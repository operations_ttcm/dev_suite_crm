<?php
/***CONFIGURATOR***/
$sugar_config['disable_persistent_connections'] = false;
$sugar_config['lead_conv_activity_opt'] = 'copy';
$sugar_config['default_module_favicon'] = false;
$sugar_config['dashlet_auto_refresh_min'] = '30';
$sugar_config['stack_trace_errors'] = false;
$sugar_config['passwordsetting']['SystemGeneratedPasswordON'] = '0';
$sugar_config['passwordsetting']['systexpiration'] = '0';
$sugar_config['passwordsetting']['systexpirationtime'] = '';
$sugar_config['passwordsetting']['oneupper'] = '0';
$sugar_config['passwordsetting']['onelower'] = '0';
$sugar_config['passwordsetting']['onenumber'] = '0';
$sugar_config['passwordsetting']['onespecial'] = '0';
$sugar_config['passwordsetting']['forgotpasswordON'] = '0';
$sugar_config['passwordsetting']['linkexpirationtime'] = '30';
$sugar_config['passwordsetting']['generatepasswordtmpl'] = '';
$sugar_config['passwordsetting']['lostpasswordtmpl'] = '';
$sugar_config['passwordsetting']['factoremailtmpl'] = '';
$sugar_config['passwordsetting']['systexpirationtype'] = '0';
$sugar_config['SAML_loginurl'] = '';
$sugar_config['SAML_logouturl'] = '';
$sugar_config['SAML_X509Cert'] = '';
$sugar_config['authenticationClass'] = '';
$sugar_config['email_xss'] = 'YToxMzp7czo2OiJhcHBsZXQiO3M6NjoiYXBwbGV0IjtzOjQ6ImJhc2UiO3M6NDoiYmFzZSI7czo1OiJlbWJlZCI7czo1OiJlbWJlZCI7czo0OiJmb3JtIjtzOjQ6ImZvcm0iO3M6NToiZnJhbWUiO3M6NToiZnJhbWUiO3M6ODoiZnJhbWVzZXQiO3M6ODoiZnJhbWVzZXQiO3M6NjoiaWZyYW1lIjtzOjY6ImlmcmFtZSI7czo2OiJpbXBvcnQiO3M6ODoiXD9pbXBvcnQiO3M6NToibGF5ZXIiO3M6NToibGF5ZXIiO3M6NDoibGluayI7czo0OiJsaW5rIjtzOjY6Im9iamVjdCI7czo2OiJvYmplY3QiO3M6MzoieG1wIjtzOjM6InhtcCI7czo2OiJzY3JpcHQiO3M6Njoic2NyaXB0Ijt9';
$sugar_config['logger']['level'] = 'fatal';
$sugar_config['license_last_check_date'] = 1526993321;
$sugar_config['outfitters_licenses']['sweetersync'] = 'de55ad8e62d96916e77de22830d10633';
$sugar_config['outfitters_licenses']['gmsyncaddon'] = '4f04b78e94fb6b649d74021c3f7e6c18';
$sugar_config['email_default_delete_attachments'] = false;
$sugar_config['email_enable_auto_send_opt_in'] = true;
$sugar_config['aod']['enable_aod'] = false;
$sugar_config['securitysuite_inbound_email'] = false;
$sugar_config['admin_export_only'] = true;
$sugar_config['dbconfigoption']['collation'] = 'utf8_general_ci';
/***CONFIGURATOR***/