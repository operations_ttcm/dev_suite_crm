<?php
require_once('include/formbase.php');
require_once('modules/PM_ProcessManagerStage/PM_ProcessManagerStage.php');
global $current_user;
//$user_id = $current_user->id;
$focus = new PM_ProcessManagerStage();
$focus->retrieve($_POST['record']);
$return_val = array();
//Get the posted values

$name =($_POST['name']);
$start_delay_minutes =($_POST['start_delay_minutes']);
$start_delay_hours =($_POST['start_delay_hours']);
$start_delay_days = ($_POST['start_delay_days']);
$start_delay_months = ($_POST['start_delay_months']);
$start_delay_years = ($_POST['start_delay_years']);
$description = ($_POST['description']);
$assignedUserID = ($_POST['assigned_user_id']);

$focus->name = $name;
$focus->start_delay_minutes = $start_delay_minutes;
$focus->start_delay_hours = $start_delay_hours;
$focus->start_delay_days = $start_delay_days;
$focus->start_delay_months = $start_delay_months;
$focus->start_delay_years = $start_delay_years;
$focus->description = $description;
$focus->assigned_user_id = $assignedUserID;
$focus->save();

$return_action = 'DetailView';
$return_module = 'PM_ProcessManagerStage';
$record = ($_POST['record']);
if ($record == '') {
	$record = $focus->id;
}

//Now redirect
header("Location: index.php?action=$return_action&module=$return_module&record=$record");
?>