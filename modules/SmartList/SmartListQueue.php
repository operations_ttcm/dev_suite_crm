<?php






class SmartListQueue extends Basic 
{
    var $new_schema = true;
    var $module_dir = 'SmartList';
    var $object_name = 'SmartListQueue';
    var $table_name = 'smartlist_queue';

    var $id;
    var $name;
    var $module;
    var $record;
    
    function bean_implements($interface){
        switch($interface){
            case 'ACL': return true;
        }
        return false;
    }
}
