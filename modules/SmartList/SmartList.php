<?php






require_once('modules/SmartList/includes/classes/SmartList/Field.php');
require_once('modules/SmartList/includes/classes/SmartList/Logger.php');

class SmartList extends Basic 
{
    var $new_schema = true;
    var $module_dir = 'SmartList';
    var $object_name = 'SmartList';
    var $table_name = 'smartlist';

    var $id;

    // chad 5/26/2017 - I don't think this actually gets used unless you're manually messing with a smartlist bean
    // when you save your smartlist settings in SC setup step 4, all of these will be defined
    // with the exception of accounts if accounts are not enabled
    public $default_metadata = array(
        'Contacts' => array(
            'mode' => 'none', // none, all, smart, sql
            'none' => array(),
            'all' => array(),
            'smart' => array(),
            'sql' => array(),
        ),
        'Prospects' => array(
            'mode' => 'none', // none, all, smart, sql
            'none' => array(),
            'all' => array(),
            'smart' => array(),
            'sql' => array(),
        ),
        'Leads' => array(
            'mode' => 'none', // none, all, smart, sql
            'none' => array(),
            'all' => array(),
            'smart' => array(),
            'sql' => array(),
        ),
        'Accounts' => array(
            'mode' => 'none', // none, all, smart, sql
            'none' => array(),
            'all' => array(),
            'smart' => array(),
            'sql' => array(),
        ),
    );

    public static $default_account_metadata = array(
        'mode' => 'none',
        'sql' => array(
            'initial_sql' => 'SELECT id FROM accounts WHERE type=\'Customer\' ',
        ),
    );

    function bean_implements($interface){
        switch($interface){
            case 'ACL': return true;
        }
        return false;
    }

    // override save function to handle packing the metadata
    function save($check_notify=false)
    {
        $this->active = $this->get_active_state($this->metadata);

        $this->prepare_for_save($this->metadata);

        return parent::save($check_notify);
    }

    // check to see if there is at least module with smartlist turned on
    // needs to be all/smart/sql, it is turned off in 'none' mode
    function get_active_state($metadata)
    {
        if (empty($metadata) or !is_array($metadata))
        {
            SmartList_Logger::log('warning','SmartList get_active_state: metadata not provided');
            return false;
        }

        $active_modes = array('all','smart','sql');

        $is_active = false;
        foreach ($metadata as $module => $data)
        {
            if (!empty($metadata[$module]['mode']) && in_array($metadata[$module]['mode'],$active_modes))
            {
                SmartList_Logger::log('debug','SmartList get_active_state: smartlist enabled for '.$module);
                $is_active = true;
                break;
            }
        }

        if ($is_active === true)
        {
            SmartList_Logger::log('debug','SmartList get_active_state: smartlist is active');
            return '1';
        }
        else
        {
            SmartList_Logger::log('debug','SmartList get_active_state: smartlist is not active');
            return '0';
        }
    }

    public function prepare_for_save($metadata=false)
    {
        // smartlist-metadata[Contacts][mode]
        // smartlist-metadata[Contacts][all]
        // smartlist-metadata[Contacts][none]
        // smartlist-metadata[Contacts][smart][filters][0][field]
        // smartlist-metadata[Contacts][smart][filters][0][operand]
        // smartlist-metadata[Contacts][smart][filters][0][value]
        // smartlist-metadata[Contacts][smart][filters][0][type]
        // smartlist-metadata[Contacts][sql][sql]
        // smartlist-metadata[Leads][mode]
        // smartlist-metadata[Leads][all]
        // smartlist-metadata[Leads][none]
        // smartlist-metadata[Leads][smart][filters][0][field]
        // smartlist-metadata[Leads][smart][filters][0][operand]
        // smartlist-metadata[Leads][smart][filters][0][value]
        // smartlist-metadata[Leads][smart][filters][0][type]
        // smartlist-metadata[Leads][sql][sql]

        if (empty($metadata) or !is_array($metadata))
        {
            SmartList_Logger::log('warning','SmartList->prepare_for_save: there was no module data sent');
            $metadata = $this->default_metadata;
        }

        foreach ($metadata as $module => $data)
        {
            if (empty($metadata[$module]['mode']))
            {
                SmartList_Logger::log('warning','SmartList->prepare_for_save: no mode set for module: '.print_r($module,true));
                continue;
            }

            $mode = $metadata[$module]['mode'];

            $mode_metadata = array();
            if (isset($metadata[$module][$mode]))
            {
                $mode_metadata = $metadata[$module][$mode];
            }
            else
            {
                SmartList_Logger::log('debug','SmartList->prepare_for_save: no metadata for module '.print_r($module,true).' and mode '.print_r($mode,true));
            }

            $mode_object = SmartList_Mode::forge($mode,$module,$mode_metadata);
            
            $prepared_metadata = array();
            if (!empty($mode_object))
            {
                $prepared_metadata = $mode_object->prepare_metadata();
            }

            if (empty($prepared_metadata))
            {
                SmartList_Logger::log('warning','SmartList->prepare_for_save: prepared metadata was empty for module '.print_r($module,true).' and mode '.print_r($mode,true));
            }
            else
            {
                $metadata[$module][$mode] = $prepared_metadata;
            }
        }

        $this->metadata = $this->pack_metadata($metadata);

        return $this->metadata;
    }

    public function unpack_metadata($metadata)
    {
        if ($this->is_metadata_unpacked($metadata) === true)
        {
            return $metadata;
        }

        return unserialize(base64_decode($metadata));
    }

    public function pack_metadata($metadata)
    {
        if ($this->is_metadata_unpacked($metadata) !== true)
        {
            return $metadata;
        }

        return base64_encode(serialize($metadata));
    }

    // if metadata is an array, it is unpacked
    // otherwise it is a string and needs to be unpacked
    public function is_metadata_unpacked($metadata)
    {
        return is_array($metadata);
    }

    public function get_smartlist_by_prospect_list_id($prospect_list_id)
    {
        if (empty($prospect_list_id))
        {
            return false;
        }

        global $db;

        $sql = "SELECT id FROM smartlist WHERE prospect_list_id='".$db->quote($prospect_list_id)."'";
        $result = $db->query($sql);
        $row = $db->fetchByAssoc($result);

        if (empty($row['id']))
        {
            return false;
        }

        $smartlist_id = $row['id'];

        return BeanFactory::getBean('SmartList',$smartlist_id,array('disable_row_level_security'=>true));
    }

    public function get_active_smartlists($return_beans=true)
    {
        global $db;

        $sql = "SELECT smartlist.id AS id FROM smartlist
                INNER JOIN prospect_lists ON smartlist.prospect_list_id=prospect_lists.id
                WHERE prospect_lists.deleted=0 AND smartlist.deleted=0 AND smartlist.active=1";

        SmartList_Logger::log('debug','SmartList get_active_smartlists: active smartlist query: '.$sql);

        if (!$result = $db->query($sql))
        {
            SmartList_Logger::log('fatal','SmartList get_active_smartlists: active smartlist query failed: '.$sql);
            return false;
        }
        
        $smartlist_ids = array();
        while ($row = $db->fetchByAssoc($result))
        {
            $smartlist_ids []= $row['id'];
        }

        SmartList_Logger::log('debug','SmartList get_active_smartlists: smartlist_ids: '.print_r($smartlist_ids,true));

        if ($return_beans === false)
        {
            return $smartlist_ids;
        }

        $smartlists = array();
        foreach ($smartlist_ids as $id)
        {
            SmartList_Logger::log('debug','SmartList get_active_smartlists: get smartlist bean for id '.$id);
            $smartlists[$id] = BeanFactory::getBean('SmartList',$id,array('disable_row_level_security'=>true));
        }

        return $smartlists;
    }

    public function queue_checks($modules=array())
    {
        // if no modules were passed in, check all of them
        $check_all_modules = false;
        if (empty($modules) || !is_array($modules))
        {
            $check_all_modules = true;
        }

        // if one of these modes are selected
        // a queue of all records for the module is needed
        $queue_modes = array(
            'smart',
            'all',
            'sql',
        );

        $metadata = $this->metadata;

        // check if the metadata is ready to use
        if ($this->is_metadata_unpacked($metadata) !== true)
        {
            // metadata is not unpacked, unpack it
            $metadata = $this->unpack_metadata($metadata);
        }

        foreach ($metadata as $module => $data)
        {
            if ($check_all_modules === false && !in_array($module, $modules))
            {
                SmartList_Logger::log('debug','SmartList queue_check: not queueing check for module '.print_r($module,true));
                continue;
            }

            if (empty($data['mode']) || !in_array($data['mode'],$queue_modes))
            {
                SmartList_Logger::log('debug','SmartList queue_check: not queueable mode for module '.print_r($module,true));
                continue;
            }

            SmartList_Logger::log('debug','SmartList queue_check: queuing check for module '.print_r($module,true));
            $this->queue_check($module);
        }

        return true;
    }

    public function queue_check($module)
    {
        if (empty($module))
        {
            SmartList_Logger::log('debug','SmartList queue_check: module is empty, but it is required');
            return false;
        }

        // get us a bean for the module
        $bean = BeanFactory::getBean($module);

        if (empty($bean))
        {
            SmartList_Logger::log('debug','SmartList queue_check: could not get a bean for module '.print_r($module,true));
            return false;
        }

        if (empty($bean->module_dir))
        {
            SmartList_Logger::log('debug','SmartList queue_check: bean->module_dir is empty, but it is required');
            return false;
        }

        $module_name = $bean->module_dir;
        SmartList_Logger::log('debug','SmartList queue_check: que');

        if (empty($bean->table_name))
        {
            SmartList_Logger::log('debug','SmartList queue_check: bean->table_name is empty, but it is required');
            return false;
        }

        $table_name = $bean->table_name;

        global $db;

        $sql = "INSERT INTO smartlist_queue (id,module,record,date_entered,date_modified,deleted,name,modified_user_id,created_by,description) 
                SELECT ".$db->getGuidSQL().",'".$db->quote($module_name)."',id,".$db->now().",".$db->now().",0,NULL,NULL,NULL,NULL 
                FROM ".$db->quote($table_name)." WHERE ".$db->quote($table_name).".deleted=0";
        SmartList_Logger::log('debug','SmartList queue_check: sql: '.print_r($sql,true));

        if (!$result = $db->query($sql))
        {
            SmartList_Logger::log('debug','SmartList queue_check: query failed: '.print_r($sql,true));
            return false;
        }

        return true;
    }
}
