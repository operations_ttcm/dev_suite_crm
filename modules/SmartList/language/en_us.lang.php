<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






$mod_strings = array (
    'LBL_SMARTLIST' => 'SmartList',
    'LNK_SETUP' => 'Setup',
    'LBL_PARAM1' => 'Parameter 1',
    'LBL_PARAM2' => 'Parameter 2',
    'LBL_PARAM3' => 'Parameter 3',
    'LBL_PARAM4' => 'Parameter 4',
    'LBL_PARAM5' => 'Parameter 5',
    'STATUS_CONFIRMING_SCHEDULER' => 'Confirming your Scheduler configuration...',
    'STATUS_LOADING_SMARTLIST_SETTINGS' => 'Loading SmartList Setting Data...',
    'STATUS_SAVING_SMARTLIST_SETTINGS' => 'Saving SmartList Settings...',
    'STATUS_LOADING_UPGRADE_OPTIONS' => 'Loading Upgrade Options...',
    'STATUS_UPGRADING_LICENSE' => 'Upgrading Your License...',
    'STATUS_CANCELLING_UPGRADE_TRIAL' => 'Cancelling Upgrade Trial...',    
    'ERR_MISSING_PARAMETER_FIELD' => 'Missing a parameter.',
    'ERR_TRY_AGAIN' => 'Please try again.',
    'ERR_LICENSE_CONNECTION' => 'Cannot connect to the License Server',
    'SUCCESS_LOGIN_TEST_TITLE' => 'Success!',
    'HEALTH_STATUS_LNK' => 'Health Status',
    'SETUP_LNK' => 'SmartList Configuration',
    'SMARTLIST_LICENSE_LNK' => 'License Configuration',
    'SMARTLIST_LNK' => 'SmartList Setup',
    'LBL_SMARTLIST_DASHLET_TITLE' => 'MailChimp List Settings',
    'LBL_SMARTLIST_DASHLET_DESCRIPTION' => 'Quick view of your synced MailChimp List settings.',            
    'LBL_QUEUEING_SYNC' => 'Queueing Sync...',
    'LBL_QUEUE_SYNC_COMPLETE' => 'Re-sync Successfully Queued',
    'LBL_LOG_LVL_SAVED' => 'SmartList Logging Level Saved',
    'LBL_CHECK' => 'is Checked',
    'LBL_NOTCHECK' => 'is not Checked',
    'LBL_EMPTY' => 'is Empty',
    'LBL_NOTEMPTY' => 'is not Empty',
    'LBL_GREATERTHAN' => 'is Greater Than',
    'LBL_GREATERTHANEQUAL' => 'is Greater Than or Equal',
    'LBL_LESSTHANEQUAL' => 'is Less Than or Equal',
    'LBL_LESSTHAN' => 'is Less Than',
    'LBL_EQUAL' => 'Equals',
    'LBL_NOTEQUAL' => 'Does not Equal',
    'LBL_CONTAIN' => 'Contains',
    'LBL_NOTCONTAIN' => 'Does not Contain',
    'LBL_STARTSWITH' => 'Starts With',
    'LBL_ENDSWITH' => 'Ends With',
    'LBL_ONEOF' => 'is One Of',
    'LBL_NOTONEOF' => 'is not One Of',
    'SMARTLIST_INVALID_SQL' => 'Invalid query: check the SmartList_Custom_SQL_Guide_Link for more details.',
);

