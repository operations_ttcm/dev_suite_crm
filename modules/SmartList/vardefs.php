<?php






require_once('include/SugarObjects/VardefManager.php');

$dictionary['SmartList'] = array (
    'table' => 'smartlist',
    'fields' => array(
        'id' => array(
            'name' =>'id',
            'type' =>'id'
        ),
        'prospect_list_id' => array(
            'name'=>'prospect_list_id',
            'type' => 'relate',
            'dbType' => 'id',
            'rname' => 'id',
            'module' => 'ProspectList',
            'id_name' => 'prospect_list_id',
        ),
        'prospect_list' => array(
            'name' => 'prospect_list',
            'type' => 'link',
            'relationship' => 'prospect_list_smartlist',
            'link_type'=>'one',
            'side'=>'right',
            'source'=>'non-db',
            'vname'=>'LBL_PROSPECT_LISTS',
        ),
        'active' => array(
            'name' => 'active',
            'type' => 'bool',
            'default' => '1',
        ),
        'type' => array(
            'name'=>'type',
            'type'=>'varchar',
            'dbType' => 'varchar',
            'len'=>255,
        ),
        'metadata' => array(
            'name' => 'metadata',
            'type' => 'text',
            'rows' => 6,
            'cols' => 80,
            'duplicate_on_record_copy' => 'always',
        ),
        'query' => array(
            'name' => 'query',
            'type' => 'text',
            'rows' => 6,
            'cols' => 80,
            'duplicate_on_record_copy' => 'always',
        ),
        'date_entered' => array (
            'name' => 'date_entered',
            'type' => 'datetime'
        ),
        'date_modified' => array (
            'name' => 'date_modified',
            'type' => 'datetime'
        ),
        'deleted' => array(
            'name' => 'deleted',
            'type' => 'bool',
            'len'  => '1',
            'default'   => '0',
            'required'  => false
        ),
    ),
);

VardefManager::createVardef('SmartList','SmartList', array('basic'));

$dictionary['SmartListQueue'] = array (
    'table' => 'smartlist_queue',
    'fields' => array(
        'id' => array(
            'name' =>'id',
            'type' =>'id'
        ),
        'module' => array(
            'name'=>'module',
            'type'=>'varchar',
            'dbType' => 'varchar',
            'len'=>255,
        ),
        'record' => array(
            'name' =>'record',
            'type' =>'id'
        ),
        'date_entered' => array (
            'name' => 'date_entered',
            'type' => 'datetime'
        ),
        'date_modified' => array (
            'name' => 'date_modified',
            'type' => 'datetime'
        ),
        'deleted' => array(
            'name' => 'deleted',
            'type' => 'bool',
            'len'  => '1',
            'default'   => '0',
            'required'  => false
        ),
    ),
);

VardefManager::createVardef('SmartListQueue','SmartListQueue', array('basic'));


