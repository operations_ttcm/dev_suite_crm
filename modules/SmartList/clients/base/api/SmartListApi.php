<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






global $sugar_version;

// only load the real SugarApi for sugar 6.7.*, 7, and above users
// otherwise, load a fake one so it can still be extended and not throw error in CE
if(preg_match( "/^6.[0-6]/", $sugar_version))
{
    require_once('modules/SugarChimp/includes/classes/CEHackSugarApi.php');
}
else
{
    require_once('include/api/SugarApi.php');
}

require_once('data/BeanFactory.php');
require_once('modules/SmartList/includes/classes/SmartList/Scheduler.php');
require_once('modules/SmartList/includes/classes/SmartList/Setting.php');


class SmartListApi extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'test' => array(
                'reqType' => 'GET',
                'path' => array('SmartList','test','?','?','?','?','?'),
                'pathVars' => array('','','param1','param2','param3','param4','param5'),
                'method' => 'test',
                'shortHelp' => 'Test method to test stuff. Duh.',
            ),
            'getHealth' => array(
                'reqType' => 'GET',
                'path' => array('SmartList', 'health'),
                'pathVars' => array('',''),
                'method' => 'getHealth',
                'shortHelp' => 'Misc stats about current SmartList state.',
            ),
            'updateSetting' => array(
                'reqType'   => 'GET',
                'path'      => array('SmartList','setting','?','?'),
                'pathVars'  => array('','','key','value'),
                'method'    => 'updateSetting',
                'shortHelp' => 'Update a sugarcrm setting key to a given value.',
            ),
            'getSetting' => array(
                'reqType'   => 'GET',
                'path'      => array('SmartList','setting','?'),
                'pathVars'  => array('','','key'),
                'method'    => 'getSetting',
                'shortHelp' => 'Get a sugarcrm setting key value.',
            ),
        );
    }
    
    public function test($api, array $args)
    {
        $param1 = empty($args['param1']) ? 0 : $args['param1'];
        $param2 = empty($args['param2']) ? 0 : $args['param2'];
        $param3 = empty($args['param3']) ? 0 : $args['param3'];
        $param4 = empty($args['param4']) ? 0 : $args['param4'];
        $param5 = empty($args['param5']) ? 0 : $args['param5'];
        
        
        return array('success' => true, 'result' => $result);
    }
    
    public function getHealth($api, array $args)
    {
        global $db;
        
        $scheduler_status = SmartList_Scheduler::getHealth();

        if(!empty($scheduler_status))
        {
            SmartList_Logger::log('debug','$scheduler_status: ' . print_r($scheduler_status, true));
            return $scheduler_status;
        }
        else
        {
            SmartList_Logger::log('debug','$scheduler_status is empty');
            return false;
        }
    }

    public function updateSetting($api,$args)
    {
        if (empty($args['key']))
        {
            SmartList_Logger::log('fatal','SmartListAPI->updateSetting a key is required to update the setting.');
            return array('success'=>false,'message'=>'A key is required to update a setting.');
        }
        
        if (empty($args['value']))
        {
            $args['value'] = '';
            SmartList_Logger::log('warning','SmartListAPI->updateSetting value is empty. will set '.$key.' to empty value.');
        }
        
        $key = $args['key'];
        $value = $args['value'];

        require_once('modules/SmartList/includes/classes/SmartList/Setting.php');
        $result = SmartList_Setting::set($key,$value);

        if ($result !== true)
        {
            return array(
                'success' => false,
                'message' => 'The setting could not be saved.',
            );
        }

        return array(
            'success' => true,
            'message' => 'The setting was successfully updated.',
        );
    }

    public function getSetting($api,$args)
    {
        if (empty($args['key']))
        {
            SmartList_Logger::log('fatal','SmartListAPI->updateSetting a key is required to update the setting.');
            return array('success'=>false,'message'=>'A key is required to update a setting.');
        }
        
        $key = $args['key'];
        
        require_once('modules/SmartList/includes/classes/SmartList/Setting.php');
        $result = SmartList_Setting::retrieve($key);

        if ($result === false)
        {
            return array(
                'success' => false,
                'message' => 'The setting could not be retrieved.',
                'value' => $result,
            );
        }

        return array(
            'success' => true,
            'message' => 'The setting was retrieved successfully.',
            'value' => $result,
        );
    }
}