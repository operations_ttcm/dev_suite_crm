<?php






require_once('modules/SmartList/includes/classes/SmartList/Field/Driver.php');
require_once('modules/SmartList/includes/classes/SmartList/Logger.php');

class SmartList_Field_Int extends SmartList_Field_Driver
{
	public static $operands = array(
		'equal',
		'notequal',
		'empty',
        'notempty',
        'lessthan',
        'lessthanequal',
        'greaterthan',
        'greaterthanequal'
	);
    public static $field_filters = array(
    	'null_value' => "0"
    );
}