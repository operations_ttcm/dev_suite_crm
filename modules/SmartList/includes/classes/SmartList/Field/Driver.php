<?php






require_once('modules/SmartList/includes/classes/SmartList/Logger.php');
require_once('modules/SmartList/includes/classes/SmartList/Operand.php');

abstract class SmartList_Field_Driver
{
	public static $operands = array();
	public static $template = '@sql_field@ @sql_operand@ @value@';
	public static $prefix = array();
	public static $postfix = array();
	public static $field_filters = array();

    public static function forge()
    {
        return new static();
    }

	public function get_operands()
	{
		return $operands;
	}

	public function prepare_for_client($field_string)
	{
		$data = array(
			'all_operands' => static::$operands,
			'template' => static::$template,
		);
		$data['operands'] = array();
		foreach ($data['all_operands'] as $operand)
		{
			$data['operands'][$operand] = SmartList_Operand::forge($operand)->prepare_for_client($field_string);
		}

		return $data;
	}

	// 	Renders sql for custom filters
	//
	//	Call Stack to get here:
	//	SmartList_Field::get_wheres()
	//	SmartList_Operand_Driver::render_sql()
	// 
	// **********************************
	//	ex: first_name does not Contain 'abc'
	// 
	// 	$filter = array(
	//		sql_field => 'first_name',
	//		sql_operand => 'NOT LIKE',
	//		value => 'abc'
	//	)
	//
	//	*****$prefixes and $postfixes are only set by the operand at this point
	//	*****so no quotes for text fields yet
	//
	//	$prefix = array(
	//		value=>'%'
	//	)
	//	$postfix = array(
	//		value=>'%'
	//	)
	//
	//	*****$append_template is for operands that require more than 'field' 'operand' 'value'
	//	***** Currently only used by 'notContain' and 'notEqual'
	//
	//	$append_template=" OR @sql_field@ IS NULL"
	//	
	// ***********************************
	public function render_sql($filter, $op_prefix, $op_postfix, $append_template=""){
		
		// validate that field / operand combo is allowed
        $valid = static::is_valid_operand($filter['operand']);
        if(empty($valid))
    	{
    		SmartList_Logger::log('fatal','Custom filter: Operand is not valid for text fields');    		
    		return false;
    	}

    	// append static template
    	$template = static::$template . $append_template;
		SmartList_Logger::log('debug','Custom filter: appended template: ' . static::$template);    		
		// add field's pre and postfix outside of op's pre and postfix
		$prefix = $this->add_prefixes($op_prefix);
		$postfix = $this->add_postfixes($op_postfix);
		$sql = $this->replace_holders($filter, $prefix, $postfix, $template);

		$sql = '(' . $sql . ')';
    	SmartList_Logger::log('debug','Custom filters sql: ' . print_r($sql,true));

    	return $sql;
	}

	//	check that $operand is found within static::$operands
	public static function is_valid_operand($operand)
	{
		return in_array($operand,static::$operands);
	}

	// add field's pre and postfix outside of op's pre and postfix
	public function add_prefixes($op_prefix)
	{
		SmartList_Logger::log('debug','Old Prefix :' . print_r(static::$prefix,true));
		SmartList_Logger::log('debug','Operand Prefix :' . print_r($op_prefix,true));

		$prefix = $op_prefix;
		
		foreach(static::$prefix as $key => $value) 
		{
			if(isset($op_prefix[$key]) && is_string($op_prefix[$key])) 
			{
				SmartList_Logger::log('debug','Operand Prefix appended to Field Prefix for: ' . print_r($op_prefix[$key],true));
				$prefix[$key] = str_replace('@op@', $op_prefix[$key], $value);
			}
			else 
			{
				SmartList_Logger::log('debug','No Operand Prefix, remove @op@ from ' . print_r($value,true));
				$prefix[$key] = str_replace('@op@', '', $value);
			}
		}

		SmartList_Logger::log('debug','New Prefix :' . print_r($prefix,true));

		return $prefix;
	}

	// add field's pre and postfix outside of op's pre and postfix
	public function add_postfixes($op_postfix)
	{
		SmartList_Logger::log('debug','Old Postfix :' . print_r(static::$postfix,true));
		SmartList_Logger::log('debug','Operand Prefix :' . print_r($op_postfix,true));

		$postfix = $op_postfix;

		foreach(static::$postfix as $key => $value) 
		{
			if(isset($op_postfix[$key]) && is_string($op_postfix[$key])) 
			{
				SmartList_Logger::log('debug','Operand Prefix appended to Field Prefix for: ' . print_r($key,true));
				$postfix[$key] = str_replace('@op@', $op_postfix[$key], $value);
			}
			else {
				SmartList_Logger::log('debug','Operand Prefix with no Field Prefix for: ' . print_r($key,true));
				$postfix[$key] = str_replace('@op@', '', $value);
			}
		}

		SmartList_Logger::log('debug','New Postfix :' . print_r($postfix,true));

		return $postfix;
	}

	//replaces ** @sql_value@ ** with ** prefix['value'] . filter['value'] . postfix['value'] inside of $str
	//replaces all other @key@ with prefixes and filter['key']
	public function replace_holders($filter, $prefix, $postfix, $template)
	{
		global $db;

		// adds prefixes
		// ex: field = first_name   operand = Contains
		// would do this:
		// @sql_value@ -> '%@sql_value@
		foreach($prefix as $key => $value) 
		{
			if(isset($filter[$key]) && is_string($filter[$key])) 
			{
				$filter[$key] = $value . $filter[$key];
			}
			else 
			{
				SmartList_Logger::log('warning','Key value not set or not a string in filter for: ' . print_r($key,true));
			}
		}

		// adds postfixes
		// ex: field_type = text   sql_operand = Contains
		// would do this:
		// '%@value@ -> '%@value@%'
		foreach($postfix as $key => $value) 
		{
			if(isset($filter[$key]) && is_string($filter[$key])) 
			{
				$filter[$key] = $filter[$key] . $value;
			}
			else 
			{
				SmartList_Logger::log('warning','Key value not set in filter for: ' . print_r($key,true));
			}
		}

		// replaces the @key@ with filter['key']
		// ex: value = 'abc'    sql_operand = Contains
		// would do this:
		// '%@value@%' -> '%abc%'
		$sql = $template;
		foreach($filter as $key => $value) 
		{
			if(!is_string($filter[$key])) 
			{
				SmartList_Logger::log('warning','Cannot replace : ' . print_r($key,true));
			}

			$sql = str_replace('@'. $key .'@', $value, $sql);
		}

		// replace any extra field filters that are included with this specific field type
		// ex. null_value where we need to know what value should be represented when a null value is encountered
		foreach(static::$field_filters as $key => $value) 
		{
			if(!is_string($field_filters[$key])) 
			{
				SmartList_Logger::log('warning','Cannot replace : ' . print_r($key,true));
			}

			$sql = str_replace('@'. $key .'@', $value, $sql);
		}

		return $sql;
	}
}