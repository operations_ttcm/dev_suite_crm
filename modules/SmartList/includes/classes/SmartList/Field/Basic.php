<?php






require_once('modules/SmartList/includes/classes/SmartList/Field/Driver.php');
require_once('modules/SmartList/includes/classes/SmartList/Logger.php');

class SmartList_Field_Basic extends SmartList_Field_Driver
{
    public static $operands = array(
        'equal',
        'notequal',
        'empty',
        'notempty'
    );
    public static $prefix = array(
        'value' => "'@op@",
    );
    public static $postfix = array(
        'value' => "@op@'",
    );
    public static $field_filters = array(
        'null_value' => "''"
    );
}