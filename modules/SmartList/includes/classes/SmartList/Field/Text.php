<?php






require_once('modules/SmartList/includes/classes/SmartList/Field/Basic.php');
require_once('modules/SmartList/includes/classes/SmartList/Logger.php');

class SmartList_Field_Text extends SmartList_Field_Basic
{
	public static $operands = array(
		'equal',
		'notequal',
		'contain',
		'notcontain',
		'startswith',
		'endswith',
		'empty',
        'notempty'
		);
}