<?php






require_once('modules/SmartList/includes/classes/SmartList/Field/Text.php');

class SmartList_Field_Enum extends SmartList_Field_Text
{
	public static $operands = array(
        'equal',
        'notequal',
        'empty',
        'notempty',
        'oneof',
        'notoneof'
        );
}