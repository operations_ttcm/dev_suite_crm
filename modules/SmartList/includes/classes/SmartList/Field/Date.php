<?php






require_once('modules/SmartList/includes/classes/SmartList/Field/Basic.php');
require_once('modules/SmartList/includes/classes/SmartList/Logger.php');

class SmartList_Field_Date extends SmartList_Field_Basic
{
	public static $operands = array(
		'equal',
		'notequal',
		'empty',
        'notempty',
        'lessthan',
        'lessthanequal',
        'greaterthan',
        'greaterthanequal'
	);
}