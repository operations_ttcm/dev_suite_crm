<?php






require_once('modules/SmartList/includes/classes/SmartList/Field/Driver.php');
require_once('modules/SmartList/includes/classes/SmartList/Logger.php');

class SmartList_Field_Bool extends SmartList_Field_Driver
{
	public static $operands = array(
		'check',
        'notcheck',
		'empty',
        'notempty'
	);
    public static $field_filters = array(
    	'null_value' => "0"
    );
}