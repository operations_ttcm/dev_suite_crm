<?php






require_once('modules/SmartList/includes/classes/SmartList/Logger.php');
require_once('modules/SmartList/includes/classes/SmartList/Field.php');

abstract class SmartList_Operand_Driver 
{
	public static $name = 'driver';
	public static $vname = 'LBL_DRIVER';
	public static $sql_operand = '';
	public static $prefix = array();
	public static $postfix = array();
	public static $display_type = array('default' => 'input'); //input, multiselect, dropdown, empty
	public static $append_template = ''; //for abnormal operands (like !=), string to append to template
	
	public static function forge()
    {
        return new static();
    }


    // 	****Overridden by operands that do not depend on field_type****
    //	Renders sql for custom filters
	//	Proper Operand Class is forged to get static prefix/postfixes
	//
	//	Call Stack to get here:
	//	SmartList_Field::get_wheres()
	// 
	// 	**********************************
	//	ex: first_name does not Contain 'abc'
	// 
	// 	$filter = array(
	//		sql_field => 'first_name',
	//		sql_operand => 'NOT LIKE',
	//		value => 'abc'
	//	)
    //
    //  will send $filter along with 
    //	$prefix['value'] = '%'  and   $postfix['value'] = '%'
    // 	to SmartList_Field_Driver::render_sql()
    //
	// 	***********************************
	public function render_sql($filter)
	{
		// //multienum
		// (industry LIKE '%^tech^%' OR industry LIKE '%^auto^%' OR industry LIKE '%^finance^%')
		// //enum
		// (industry='tech' OR industry='auto' OR industry='finance')
		$filter['sql_operand'] = static::$sql_operand;

		if(is_array($filter['value']))
		{
			$passing_filter = $filter;
			$sql = ' ( ';
			$first = true;
			foreach($filter['value'] as $value)
			{
				$passing_filter['value'] = $value;
				if(empty($first))
				{
					$sql .= ' ' . static::$conjunction . ' ';
				}

				$sql .= SmartList_Field::forge($filter['type'])->render_sql($passing_filter, static::$prefix, static::$postfix);

				$first = false;
			}
			$sql .= ' ) ';
			return $sql;
		}

		return SmartList_Field::forge($filter['type'])->render_sql($filter, static::$prefix, static::$postfix, static::$append_template);
	}

	public function prepare_for_client($field)
	{
		$display_type = empty(static::$display_type[$field]) ? static::$display_type['default'] : static::$display_type[$field];
		$label = empty(static::$vname) ? static::$name : translate(static::$vname, 'SugarChimp');
		$label = ($label == static::$vname) ? static::$name : $label;
		
		return array(
			'name' => static::$name,
			'label' => $label,
			'vname' => static::$vname,
			'sql_operand' => static::$sql_operand,
			'prefix' => static::$prefix,
			'postfix' => static::$postfix,
			'display_type' => $display_type,
		);
	}

	public function get_label()
	{
		$label = empty(static::$vname) ? static::$name : translate(static::$vname, 'SugarChimp');
		return ($label == static::$vname) ? static::$name : $label;
	}
}
