<?php






require_once('modules/SmartList/includes/classes/SmartList/Operand/Driver.php');

class SmartList_Operand_Endswith extends SmartList_Operand_Driver 
{
	public static $name = 'endswith';
	public static $vname = 'LBL_ENDSWITH';
	public static $sql_operand = 'LIKE';
	public static $prefix = array(
		'value' => '%',
		'sql_field' => 'IFNULL(',
	);
	public static $postfix = array(
		'sql_field' => ",'')",
	);
}