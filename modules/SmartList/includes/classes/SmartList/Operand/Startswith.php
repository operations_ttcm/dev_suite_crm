<?php






require_once('modules/SmartList/includes/classes/SmartList/Operand/Driver.php');

class SmartList_Operand_Startswith extends SmartList_Operand_Driver 
{
	public static $name = 'startswith';
	public static $vname = 'LBL_STARTSWITH';
	public static $sql_operand = 'LIKE';
	public static $prefix = array(
		'sql_field' => 'IFNULL(',
	);
	public static $postfix = array(
		'value' => '%',
		'sql_field' => ",'')",
	);
}