<?php






require_once('modules/SmartList/includes/classes/SmartList/Operand/Driver.php');

class SmartList_Operand_Empty extends SmartList_Operand_Driver 
{
	public static $name = 'empty';
	public static $vname = 'LBL_EMPTY';
	public static $sql_operand = '=';
	public static $prefix = array();
	public static $postfix = array();
	public static $display_type = array(
		'default' => 'empty'
	);

	// empty must handle itself, instead of passing to field to render
	// by overriding render_sql, it will never call the field type render_sql
	// it's not needed in this scenario because we know the value side
	// and it's the same for all field types
	public function render_sql($filter)
	{
		global $db;
		
		return " ( " . $db->quote($filter['sql_field']) . " ='' OR " . $db->quote($filter['sql_field']) . " IS NULL" . " ) ";
	}
}