<?php






require_once('modules/SmartList/includes/classes/SmartList/Operand/Driver.php');

class SmartList_Operand_Notoneof extends SmartList_Operand_Driver 
{
	public static $name = 'notoneof';
	public static $vname = 'LBL_NOTONEOF';
	public static $sql_operand = 'NOT LIKE';
	public static $conjunction = 'AND';
	public static $prefix = array(
		'value' => '%',
		'sql_field' => 'IFNULL(',
	);
	public static $postfix = array(
		'value' => '%',
		'sql_field' => ",'')",
	);
	public static $display_type = array(
		'default' => 'multiselect',
	);
}