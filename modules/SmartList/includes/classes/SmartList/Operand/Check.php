<?php






require_once('modules/SmartList/includes/classes/SmartList/Operand/Equal.php');

class SmartList_Operand_Check extends SmartList_Operand_Equal 
{
	public static $name = 'check';
	public static $vname = 'LBL_CHECK';
	public static $display_type = array(
		'default' => 'empty'
	);

	// by overriding render_sql, it will never call the field type render_sql
	// it's not needed in this scenario because we know the value side
	// and it's the same for all field types
	public function render_sql($filter)
	{
		global $db;
		
		return " ( IFNULL(" . $db->quote($filter['sql_field']) . ",0) =1" . " ) ";
	}
}