<?php






require_once('modules/SmartList/includes/classes/SmartList/Operand/Driver.php');

class SmartList_Operand_Notcontain extends SmartList_Operand_Driver 
{
	public static $name = 'notcontain';
	public static $vname = 'LBL_NOTCONTAIN';
	public static $sql_operand = 'NOT LIKE';
	public static $prefix = array(
		'value' => '%',
		'sql_field' => 'IFNULL(',
	);
	public static $postfix = array(
		'value' => '%',
		'sql_field' => ",'')",
	);
}