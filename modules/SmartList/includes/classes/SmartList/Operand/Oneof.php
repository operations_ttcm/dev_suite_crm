<?php






require_once('modules/SmartList/includes/classes/SmartList/Operand/Driver.php');

class SmartList_Operand_Oneof extends SmartList_Operand_Driver 
{
	public static $name = 'oneof';
	public static $vname = 'LBL_ONEOF';
	public static $sql_operand = 'LIKE';
	public static $conjunction = 'OR';
	public static $prefix = array(
		'value' => '%',
		'sql_field' => 'IFNULL(',
	);
	public static $postfix = array(
		'value' => '%',
		'sql_field' => ",'')",
	);
	public static $display_type = array(
		'default' => 'multiselect',
	);
}