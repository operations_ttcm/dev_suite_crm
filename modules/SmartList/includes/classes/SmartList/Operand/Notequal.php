<?php






require_once('modules/SmartList/includes/classes/SmartList/Operand/Driver.php');

class SmartList_Operand_Notequal extends SmartList_Operand_Driver 
{
	public static $name = 'notequal';
	public static $vname = 'LBL_NOTEQUAL';
	public static $sql_operand = '!=';
	public static $prefix = array(
		'sql_field' => 'IFNULL(',
	);
	public static $postfix = array(
		'sql_field' => ",@null_value@)",
	);
}