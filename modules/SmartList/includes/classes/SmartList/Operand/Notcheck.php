<?php






require_once('modules/SmartList/includes/classes/SmartList/Operand/Notequal.php');

class SmartList_Operand_Notcheck extends SmartList_Operand_Notequal 
{
	public static $name = 'notcheck';
	public static $vname = 'LBL_NOTCHECK';
	public static $display_type = array(
		'default' => 'empty'
	);
	
	// by overriding render_sql, it will never call the field type render_sql
	// it's not needed in this scenario because we know the value side
	// and it's the same for all field types
	public function render_sql($filter)
	{
		global $db;

		return " ( IFNULL(" . $db->quote($filter['sql_field']) . ",0) !=1" . " ) ";
	}
}