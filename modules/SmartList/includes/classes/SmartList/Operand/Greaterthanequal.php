<?php






require_once('modules/SmartList/includes/classes/SmartList/Operand/Driver.php');

class SmartList_Operand_Greaterthanequal extends SmartList_Operand_Driver 
{
	public static $name = 'greaterthanequal';
	public static $vname = 'LBL_GREATERTHANEQUAL';
	public static $sql_operand = '>=';
	public static $prefix = array(
		'sql_field' => 'IFNULL(',
	);
	public static $postfix = array(
		'sql_field' => ",0)",
	);
}