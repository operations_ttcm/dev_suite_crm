<?php






require_once('modules/SmartList/includes/classes/SmartList/Operand/Driver.php');

class SmartList_Operand_Equal extends SmartList_Operand_Driver 
{
	public static $name = 'equal';
	public static $vname = 'LBL_EQUAL';
	public static $sql_operand = '=';
	public static $conjunction = 'AND';
	public static $prefix = array(
		'sql_field' => 'IFNULL(',
	);
	public static $postfix = array(
		'sql_field' => ",@null_value@)",
	);
	public static $display_type = array(
		'default' => 'input',
		'enum' => 'dropdown',
		'multienum' => 'multiselect',
	);
}
