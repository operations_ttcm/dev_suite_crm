<?php






require_once('modules/SmartList/includes/classes/SmartList/Operand/Driver.php');

class SmartList_Operand_Contain extends SmartList_Operand_Driver 
{
	public static $name = 'contain';
	public static $vname = 'LBL_CONTAIN';
	public static $sql_operand = 'LIKE';
	public static $prefix = array(
		'value' => '%',
		'sql_field' => 'IFNULL(',
	);
	public static $postfix = array(
		'value' => '%',
		'sql_field' => ",'')",
	);
}