<?php






require_once('modules/SmartList/includes/classes/SmartList/Mode/Driver.php');
require_once('modules/SmartList/includes/classes/SmartList/Setting.php');

class SmartList_Mode_Sql extends SmartList_Mode_Driver 
{
	protected $supported_sugarchimp_plans = array(
		'ultimate',
	);

	public function prepare_metadata()
	{
		// since we support parent related modules, ex. contacts->accounts->industry = 'automotive'
		// we need to be able to quickly determine who needs to be rechecked when records change
		// finding the related_modules in the provided sql on smartlist save will save time from 
		// having to do it when the actual logic hook hits
        $related_modules_in_sql = array();

        if (!empty($this->metadata['initial_sql']))
        {
            $initial_sql = $this->metadata['initial_sql'];
            $end_where_position = $this->find_end_position_of_where_keyword($initial_sql);
            SmartList_Logger::log('debug','SmartList_Mode_Sql prepare_metadata: end_where_position: '.print_r($end_where_position,true));

            $module_table_mapping = SmartList_Setting::$module_table_mapping;

            if (!empty($module_table_mapping) and is_array($module_table_mapping))
            {
                if ($end_where_position !== false)
                {
                	// to find the modules, we take the part of the sql query before the 'WHERE'
                	// find any table names and infer the module from there
                    $before_where_sql = substr($initial_sql, 0, $end_where_position);
                    SmartList_Logger::log('debug','SmartList_Mode_Sql prepare_metadata: before_where_sql: '.print_r($where_sql,true));

                    foreach ($module_table_mapping as $related_module => $table)
                    {
                        if (strpos($before_where_sql," ".$table." ") !== false)
                        {
                            // we found the table name in the query
                            // we need to lookup lookup based on this related module
                            $related_modules_in_sql[$related_module] = $related_module;
                        }
                    }
                }
            }
        }
        else
        {
            SmartList_Logger::log('debug','SmartList_Mode_Sql prepare_metadata: there is no initial_sql to work with');
        }

        $this->metadata['related_modules_in_sql'] = $related_modules_in_sql;

        return $this->metadata;
	}

	public function get_test_sql()
	{
		if (empty($this->metadata['initial_sql']))
		{
			SmartList_Logger::log('warning','SmartList_Mode_Sql get_test_sql: initial_sql from metadata is empty');
			return false;
		}

		$sql = $this->prepare_initial_sql($this->metadata['initial_sql']);

		$bean = BeanFactory::getBean($this->module);
		$module_table = $bean->getTableName();

		// go ahead and add the deleted check here so that a reliable number
		// can be returned to the user whenever they test their query
		// we do this by wrapping the user-provided query and putting the .deleted=0 check on the outside
		// if they use an alias, we cannot simply do {table}.deleted=0, the query will fail
		$sql = "SELECT ".$module_table.".id AS ".SmartList_Mode::$sql_key_id." FROM ".$module_table." WHERE ".$module_table.".id IN (".$sql.") AND ".$this->get_where_deleted_statement($module_table);

		return $sql;
	}

	public function get_inclusive_sql($off_list=array())
	{
		if (empty($off_list))
		{
			SmartList_Logger::log('warning','SmartList_Mode_Sql get_inclusive_sql: off list is empty');
			return false;
		}

		if (empty($this->metadata['initial_sql']))
		{
			SmartList_Logger::log('warning','SmartList_Mode_Sql get_inclusive_sql: initial_sql from metadata is empty');
			return false;
		}

		$sql = $this->prepare_initial_sql($this->metadata['initial_sql']);

		$sql = $this->enclose_original_where($sql);

		$in_field = $this->get_field_by_alias($sql,SmartList_Mode::$sql_key_id);

		$sql = $this->append_in_statement($sql,$off_list,$in_field);

		$bean = BeanFactory::getBean($this->module);
		$module_table = $bean->getTableName();

		// go ahead and add the deleted check here so that a reliable number
		// can be returned to the user whenever they test their query
		// we do this by wrapping the user-provided query and putting the .deleted=0 check on the outside
		// if they use an alias, we cannot simply do {table}.deleted=0, the query will fail
		$sql = "SELECT ".$module_table.".id AS ".SmartList_Mode::$sql_key_id." FROM ".$module_table." WHERE ".$module_table.".id IN (".$sql.") AND ".$this->get_where_deleted_statement($module_table);

		return $sql;
	}

	public function get_exclusive_sql($on_list=array())
	{
		if (empty($on_list))
		{
			SmartList_Logger::log('warning','SmartList_Mode_Sql get_exclusive_sql: on list is empty');
			return false;
		}

		if (empty($this->metadata['initial_sql']))
		{
			SmartList_Logger::log('warning','SmartList_Mode_Sql get_exclusive_sql: initial_sql from metadata is empty');
			return false;
		}

		$sql = $this->prepare_initial_sql($this->metadata['initial_sql']);

		$sql = $this->enclose_original_where($sql,true);

		$in_field = $this->get_field_by_alias($sql,SmartList_Mode::$sql_key_id);

		$sql = $this->append_in_statement($sql,$on_list,$in_field);

		$bean = BeanFactory::getBean($this->module);
		$module_table = $bean->getTableName();

		// go ahead and add the deleted check here so that a reliable number
		// can be returned to the user whenever they test their query
		// we do this by wrapping the user-provided query and putting the .deleted=0 check on the outside
		// if they use an alias, we cannot simply do {table}.deleted=0, the query will fail
		$sql = "SELECT ".$module_table.".id AS ".SmartList_Mode::$sql_key_id." FROM ".$module_table." WHERE ".$module_table.".id IN (".$sql.") AND ".$this->get_where_deleted_statement($module_table);

		return $sql;
	}

	public function get_affected_records($bean)
	{
		if (empty($bean->module_dir))
		{
			SmartList_Logger::log('debug','SmartList_Mode_Sql::get_affected_records: bean->module_dir is empty');
			return false;
		}

		$parent_module = $bean->module_dir;
		
		if (empty($this->metadata['related_modules_in_sql']) or !is_array($this->metadata['related_modules_in_sql'])) 
		{
			SmartList_Logger::log('debug','SmartList_Mode_Sql::get_affected_records: related_modules_in_sql is empty or not an array');
			return array();
		}

		$related_modules_in_sql = $this->metadata['related_modules_in_sql'];
		SmartList_Logger::log('debug','SmartList_Mode_Sql::get_affected_records: related_modules_in_sql: '.print_r($related_modules_in_sql,true));

		if (!in_array($parent_module,$related_modules_in_sql))
		{
			SmartList_Logger::log('debug','SmartList_Mode_Sql::get_affected_records: parent_module is not in related_modules_in_sql, do not need to worry about queueing related people.');
			return array();
		}

		if (empty(SmartList_Setting::$supported_parent_related_modules) or !is_array(SmartList_Setting::$supported_parent_related_modules))
		{
			SmartList_Logger::log('debug','SmartList_Mode_Sql::get_affected_records: supported_parent_related_modules is empty or not an array');
			return array();
		}

        $supported_parent_related_modules = SmartList_Setting::$supported_parent_related_modules;
		SmartList_Logger::log('debug','SmartList_Mode_Sql::get_affected_records: supported_parent_related_modules: '.print_r($supported_parent_related_modules,true));

		if (empty($supported_parent_related_modules[$parent_module][$this->module]))
		{
			SmartList_Logger::log('debug','SmartList_Mode_Sql::get_affected_records: could not find link field for module or base module');
			return array();
		}

		// get link field
		$link_field = $supported_parent_related_modules[$parent_module][$this->module];

		return $this->get_records_from_link($bean,$link_field,$this->module);
	}
}