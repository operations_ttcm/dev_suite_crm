<?php






require_once('modules/SmartList/includes/classes/SmartList/Mode/Driver.php');
require_once('modules/SmartList/includes/classes/SmartList/Filter.php');
require_once('modules/SmartList/includes/classes/SmartList/Logger.php');

class SmartList_Mode_None extends SmartList_Mode_Driver 
{
	protected $supported_sugarchimp_plans = array(
		'basic',
		'professional',
		'ultimate',
	);

	public function prepare_metadata()
	{
		return $this->metadata;
	}

	public function get_inclusive_sql($off_list=array())
	{
		return false;
	}

	public function get_exclusive_sql($on_list=array())
	{
		return false;
	}
}