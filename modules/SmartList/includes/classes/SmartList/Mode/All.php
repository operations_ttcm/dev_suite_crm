<?php






require_once('modules/SmartList/includes/classes/SmartList/Mode/Driver.php');
require_once('modules/SmartList/includes/classes/SmartList/Mode.php');
require_once('modules/SmartList/includes/classes/SmartList/Logger.php');

class SmartList_Mode_All extends SmartList_Mode_Driver 
{
	protected $supported_sugarchimp_plans = array(
		'professional',
		'ultimate',
	);

	public function prepare_metadata()
	{
		if (!is_array($this->metadata))
		{
			SmartList_Logger::log('warning','SmartList_Mode_All prepare metdata: metadata was not an array, setting to empty array');
			$this->metadata = array();
		}

		$this->metadata['initial_sql'] = $this->get_initial_sql();

		return $this->metadata;
	}

	public function get_inclusive_sql($off_list=array())
	{
		if (empty($off_list))
		{
			SmartList_Logger::log('warning','SmartList_Mode_All get_inclusive_sql: off list is empty');
			return false;
		}

		if (empty($this->metadata['initial_sql']))
		{
			SmartList_Logger::log('warning','SmartList_Mode_All get_inclusive_sql: initial_sql from metadata is empty');
			return false;
		}

		$sql = $this->prepare_initial_sql($this->metadata['initial_sql']);

		$in_field = $this->get_field_by_alias($sql,SmartList_Mode::$sql_key_id);

		$sql = $this->append_in_statement($sql,$off_list,$in_field);

		return $sql;
	}

	public function get_exclusive_sql($on_list=array())
	{
		// everyone is already on the list
		// do not run an inclusive query
		return false;

		/**

		If people need to have specific people not included on these lists
		all they need to do is mark the Contact/Target/Lead/Accounts email address
		as invalid or opted-out in Sugar. Contacts/Targets/Leads/Accounts with opted-out
		or invalid email addresses can be added to the Sugar Target Lists,
		but they will not be sent to MailChimp because they are marked as
		opted-out or invalid.
		
		*/
	}

	protected function get_initial_sql()
	{
		if (empty($this->module))
		{
			SmartList_Logger::log('fatal','SmartList_Mode_All get_initial_sql: cannot generate query, this->module is empty');
		}

		if (empty($this->metadata))
		{
			SmartList_Logger::log('fatal','SmartList_Mode_All get_initial_sql: cannot generate query, this->metadata is empty');
		}

		$bean = BeanFactory::newBean($this->module);

		$table = $bean->getTableName();

		$sql = "SELECT {$table}.id AS ".SmartList_Mode::$sql_key_id." FROM {$table} WHERE deleted=0";

		SmartList_Logger::log('debug','SmartList_Mode_All get_initial_sql: query: '.$sql);

		return $sql;
	}
}










