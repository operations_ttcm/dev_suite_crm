<?php






require_once('modules/SmartList/includes/classes/SmartList/Mode/Driver.php');
require_once('modules/SmartList/includes/classes/SmartList/Filter.php');
require_once('modules/SmartList/includes/classes/SmartList/Field.php');
require_once('modules/SmartList/includes/classes/SmartList/Logger.php');

class SmartList_Mode_Smart extends SmartList_Mode_Driver 
{
	protected $supported_sugarchimp_plans = array(
		'ultimate',
	);

	public function prepare_metadata()
	{
		if (empty($this->metadata))
		{
			SmartList_Logger::log('warning','SmartList_Mode_Smart prepare metdata: there is no metadata');
			return false;
		}

        if (!empty($this->metadata['filters']))
        {
            $this->metadata['initial_sql'] = SmartList_Filter::get_initial_sql($this->module,$this->metadata);
        }

        // since smart filters support parent related modules, we need a fast way to 
        // evaluate who needs to be added to the queue. in the smartlist save we gather
        // the parent related modules that are in the filters so that when we the logic hooks
        // hit we can make our check fast
        $parent_related_modules_in_filters = array();

        // is this module in a filter
        if (!empty($this->metadata['filters']) && is_array($this->metadata['filters']))
        {
            foreach ($this->metadata['filters'] as $filter)
            {
                SmartList_Logger::log('debug','SmartList_Mode_Smart prepare_metadata: filter: '.print_r($filter,true));

                if (empty($filter['field'])) 
                {
                    SmartList_Logger::log('debug','SmartList_Mode_Smart prepare_metadata: filter does not have a field');
                    continue;
                }
                if (!SmartList_Field::is_related_field($filter['field'])) 
                {
                    SmartList_Logger::log('debug','SmartList_Mode_Smart prepare_metadata: field is not a related field');
                    continue;
                }

                $parts = SmartList_Field::get_related_field_parts($filter['field']);
                SmartList_Logger::log('debug','SmartList_Mode_Smart prepare_metadata: parts: '.print_r($parts,true));

                if (empty($parts['link_field_name']) or empty($parts['parent_field_name'])) 
                {
                    SmartList_Logger::log('debug','SmartList_Mode_Smart prepare_metadata: parts does not have what we need');
                    continue;
                }

                $link_field_name = $parts['link_field_name'];

                $bean = BeanFactory::getBean($this->module);
                $found = $bean->load_relationship($link_field_name);

                if ($found !== true)
                {
                    // failed load
                    SugarChimp_Logger::log('fatal','SmartList_Mode_Smart prepare_metadata: no relationship provided for module '.print_r($this->module,true).' and field '.print_r($link_field_name,true));
                    continue;
                }

                if (!method_exists($bean->$link_field_name,'getRelatedModuleName'))
                {
                    SmartList_Logger::log('debug','SmartList_Mode_Smart prepare_metadata: cannot call getRelatedModuleName for link: '.print_r($link_field_name,true));
                    continue;
                }

                $parent_module = $bean->$link_field_name->getRelatedModuleName();

                if (!method_exists($bean->$link_field_name,'getRelatedModuleLinkName'))
                {
                    SmartList_Logger::log('debug','SmartList_Mode_Smart prepare_metadata: cannot call getRelatedModuleLinkName for link: '.print_r($link_field_name,true));
                    continue;
                }
                
                $parent_link = $bean->$link_field_name->getRelatedModuleLinkName();

                SmartList_Logger::log('debug','SmartList_Mode_Smart prepare_metadata: parent_module: '.print_r($parent_module,true));
                SmartList_Logger::log('debug','SmartList_Mode_Smart prepare_metadata: parent_link: '.print_r($parent_link,true));

                $parent_related_modules_in_filters[$parent_module] = $parent_link;
            }
        }

        SmartList_Logger::log('debug','SmartList_Mode_Smart prepare_metadata: parent_related_modules_in_filters: '.print_r($parent_related_modules_in_filters,true));

        $this->metadata['parent_related_modules_in_filters'] = $parent_related_modules_in_filters;

        return $this->metadata;
	}

	public function get_inclusive_sql($off_list=array())
	{
		if (empty($off_list))
		{
			SmartList_Logger::log('warning','SmartList_Mode_Smart get_inclusive_sql: off list is empty');
			return false;
		}

		if (empty($this->metadata['initial_sql']))
		{
			SmartList_Logger::log('warning','SmartList_Mode_Smart get_inclusive_sql: initial_sql from metadata is empty');
			return false;
		}

		$sql = $this->prepare_initial_sql($this->metadata['initial_sql']);

		$in_field = $this->get_field_by_alias($sql,SmartList_Mode::$sql_key_id);

		$sql = $this->append_in_statement($sql,$off_list,$in_field);

		return $sql;
	}

	public function get_exclusive_sql($on_list=array())
	{
		if (empty($on_list))
		{
			SmartList_Logger::log('warning','SmartList_Mode_Smart get_exclusive_sql: on list is empty');
			return false;
		}

		if (empty($this->metadata['initial_sql']))
		{
			SmartList_Logger::log('warning','SmartList_Mode_Smart get_exclusive_sql: initial_sql from metadata is empty');
			return false;
		}

		// regenerate query to not the where for exclusion sql
		$sql = SmartList_Filter::get_initial_sql($this->module,$this->metadata,false,true);

		$sql = $this->prepare_initial_sql($sql);

		$in_field = $this->get_field_by_alias($sql,SmartList_Mode::$sql_key_id);

		$sql = $this->append_in_statement($sql,$on_list,$in_field);

		return $sql;
	}

	public function get_affected_records($bean)
	{
		if (empty($bean->module_dir))
		{
			SmartList_Logger::log('debug','SmartList_Mode_Smart::get_affected_records: bean is empty');
			return false;
		}

		$parent_module = $bean->module_dir;

		if (empty($this->metadata['parent_related_modules_in_filters']) or !is_array($this->metadata['parent_related_modules_in_filters'])) 
		{
			SmartList_Logger::log('debug','SmartList_Mode_Smart::get_affected_records: parent_related_modules_in_filters is empty or not an array');
			return false;
		}

		// check to see if changing module is one of the parent_related_modules_in_filters modules
		if (!array_key_exists($parent_module, $this->metadata['parent_related_modules_in_filters'])) 
		{
			SmartList_Logger::log('debug','SmartList_Mode_Smart::get_affected_records: parent_related_modules_in_filters module is not in parent_related_modules_in_filters');
			return false;
		}
		
		// this module is a parent related module, queue the related children records to be processed
		$link_field = $this->metadata['parent_related_modules_in_filters'][$parent_module];
		SmartList_Logger::log('debug','SmartList_Mode_Smart::get_affected_records: link field: '.print_r($link_field,true));

		return $this->get_records_from_link($bean,$link_field,$this->module);
	}
}