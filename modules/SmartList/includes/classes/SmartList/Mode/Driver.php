<?php






require_once('modules/SmartList/includes/classes/SmartList/Logger.php');
require_once('modules/SmartList/includes/classes/SmartList/Mode.php');
require_once('modules/SmartList/includes/classes/SmartList/Setting.php');

abstract class SmartList_Mode_Driver 
{
	protected $mode = false;
	protected $module = false;
	protected $metadata = array();
	protected $supported_sugarchimp_plans = array();

	public static function forge($mode=false,$module=false,$metadata=array())
    {
        return new static($mode,$module,$metadata);
    }

    function __construct($mode=false,$module=false,$metadata=array())
    {
    	$this->mode = $mode;
    	$this->module = $module;
    	$this->metadata = $metadata;
    }

    protected function append_in_statement($sql,$in_array,$in_field,$in_operand='IN')
    {
    	if (empty($sql))
    	{
    		SmartList_Logger::log('warning','SmartList_Mode_Driver append_in_statement: sql is empty');
    		return false;
    	}

    	if (empty($in_array))
    	{
    		SmartList_Logger::log('warning','SmartList_Mode_Driver append_in_statement: in_array is empty');
    		return false;
    	}

    	if (empty($in_field))
    	{
    		SmartList_Logger::log('warning','SmartList_Mode_Driver append_in_statement: in_field is empty');
    		return false;
    	}

		$where_pos = $this->find_end_position_of_where_keyword($sql);

		if ($where_pos === false)
		{
			// there's no where part
			$sql .= ' WHERE ';
		}
		else
		{
			// a where exists, just need to append the AND part
    		$sql .= " AND ";
		}

		// add the rest of the IN piece
		$sql .= " {$in_field} {$in_operand} ('" . implode("','",$in_array) . "') ";

    	SmartList_Logger::log('debug','SmartList_Mode_Driver append_in_statement: new sql: '.$sql);

    	return $sql;
    }

    protected function get_where_deleted_statement($table)
    {
    	if (empty($table))
    	{
    		SmartList_Logger::log('warning','SmartList_Mode_Driver get_where_deleted_statement: table is empty');
    		return false;
    	}

    	global $db;

    	$str = $db->quote($table) . ".deleted=0";
    	SmartList_Logger::log('debug','SmartList_Mode_Driver get_where_deleted_statement: str: '.print_r($str,true));

    	return $str;
    }

	// take something like "select contacts.id AS myid from contacts where deleted=0"
	// find the field in SELECT that has alias 'myid' and return contacts.id
	protected function get_field_by_alias($sql,$alias)
	{
		if (empty($sql))
		{
			SmartList_Logger::log('warning','SmartList_Mode_Driver get_field_by_alias: sql is empty');
			return false;
		}

		if (empty($alias))
		{
			SmartList_Logger::log('warning','SmartList_Mode_Driver get_field_by_alias: alias is empty');
			return false;
		}

		SmartList_Logger::log('debug','SmartList_Mode_Driver get_field_by_alias: sql: '.print_r($sql,true));
		SmartList_Logger::log('debug','SmartList_Mode_Driver get_field_by_alias: alias: '.print_r($alias,true));

		preg_match('/SELECT\s(.*?)\sFROM/si', $sql, $display);

		if (empty($display[1]))
		{
			SmartList_Logger::log('warning','SmartList_Mode_Driver get_field_by_alias: could not find in_field in sql: '.print_r($sql,true));
			return false;
		}

		$select_fields = $display[1];

		$select_fields = explode(',',$select_fields);

		foreach ($select_fields as $field)
		{
			$as_pos = stripos($field, ' as ');

			if ($as_pos === false)
			{
				// this field does not have an alias, go to the next
				continue;
			}

			$actual_name = substr($field, 0, $as_pos);

			$actual_name = trim($actual_name);

			SmartList_Logger::log('debug','SmartList_Mode_Driver get_field_by_alias: found name for alias: '.print_r($actual_name,true));			
			return $actual_name;
		}

		SmartList_Logger::log('debug','SmartList_Mode_Driver get_field_by_alias: did not find the alias');
		return false;
	}

	// do the things that all queries need
	// like make sure the ID alias we are selecting is the name we're expecting
	public function prepare_initial_sql($sql)
	{
		if (empty($sql))
		{
			SmartList_Logger::log('warning','SmartList_Mode_Driver prepare_initial_sql: sql is empty, it is a required parameter');
			return false;
		}

		// since we started using subqueries, at one point I thought the following code was not necessary
		// it absolutely is necessary for optimization purposes
		// the "where id in (array)" part needs to be within the subquery, this way the entire table isn't search for each query

		// replace all new line characters to spaces
		$sql = trim(preg_replace('/\s+/', ' ', $sql));

		// see if "AS SmartList_Mode::$sql_key_id" is in the query
		// if it is, you're good
		// if not replace stuff
		preg_match('/SELECT\s+.+\s+AS\s+'.SmartList_Mode::$sql_key_id.'\s+FROM/si', $sql, $display);

		// if we could not find 'SELECT field AS id FROM', we need to add it
		if (empty($display))
		{
			preg_match('/SELECT\s+.+\s+AS\s+.+\s+FROM/si', $sql, $display);

			if (empty($dislay))
			{
				// there is no 'AS' in the select area, need to add ' AS SmartList_Mode::$sql_key_id '
				// find ' FROM ' and add ' AS SmartList_Mode::$sql_key_id ' before it
				$pos = stripos($sql, ' from ');

				if ($pos === false)
				{
					SmartList_Logger::log('warning','SmartList_Mode_Driver prepare_initial_sql: cannot find " from " in sql: '.print_r($sql,true));
					return false;
				}

				$sql_length = strlen($sql);

				$begin = substr($sql, 0, $pos);
				$end = substr($sql, $pos, $sql_length);

				$sql = $begin . " AS ".SmartList_Mode::$sql_key_id." " . $end;
				SmartList_Logger::log('debug','SmartList_Mode_Driver prepare_initial_sql: " as " not in sql, adding it: '.print_r($sql,true));
			}
			else
			{
				// there is an 'AS', need to change the alias to SmartList_Mode::$sql_key_id
				// find ' AS ' AND ' FROM ' and add ' SmartList_Mode::$sql_key_id ' between them
				// todo: this is a lazy way to handle this, will not work if there are multiple selected fields
				// todo: ' AS ' could show up many times in a normal query
				$from_pos = stripos($sql, ' from ');

				if ($from_pos === false)
				{
					SmartList_Logger::log('warning','SmartList_Mode_Driver prepare_initial_sql: cannot find " from " in sql: '.print_r($sql,true));
					return false;
				}

				$as_pos = stripos($sql, ' as ');

				if ($as_pos === false)
				{
					SmartList_Logger::log('warning','SmartList_Mode_Driver prepare_initial_sql: cannot find " as " in sql: '.print_r($sql,true));
					return false;
				}

				$sql_length = strlen($sql);

				$begin = substr($sql, 0, ($as_pos+4));
				$end = substr($sql, $from_pos, $sql_length);

				$sql = $begin . " AS ".SmartList_Mode::$sql_key_id." " . $end;
				SmartList_Logger::log('debug','SmartList_Mode_Driver prepare_initial_sql: " as " in sql but wrong alias provided, adding it: '.print_r($sql,true));
			}
		}

		// check to make sure main table .deleted is being checked
		// find if deleted = 0 is somewhere in the query

		// remove semicolon if at the very end of the query
		$sql = trim($sql);
		$last_char = substr($sql, -1);
		if ($last_char == ';')
		{
			$sql = substr($sql,0,-1);
		}

		return $sql;
	}

	// add a WHRE NOT (...) to a given sql query
	// smart mode queries handle this on their own
	// sql mode queries need this
    public function enclose_original_where($sql,$not_where=false)
    {
        if (empty($sql))
        {
            SmartList_Logger::log('warning','SmartList_Mode_Driver enclose_original_where: sql is empty');
            return false;
        }

        SmartList_Logger::log('debug','SmartList_Mode_Driver enclose_original_where: sql: '.print_r($sql,true));

        // find the where and start there
        // todo: Have to do something far more complex to support more complex queries
        // todo: we need to be able to include all crazy queries
        $where_pos = $this->find_end_position_of_where_keyword($sql);

        if ($where_pos === false)
        {
            // there's nothing to NOT, there is no WHERE statement
            return $sql;
        }

        $not = '';
        if ($not_where === true)
        {
            $not = ' NOT ';
        }

        $sql = substr($sql,0,($where_pos+1)) . $not . ' ( ' . substr($sql,($where_pos + 1)) . ' ) ';
        
        SmartList_Logger::log('debug','SmartList_Mode_Driver enclose_original_where: new sql: '.$sql);

        return $sql;
    }

	// super simple method right now
	// add 7 chars if yuo 
	public function find_end_position_of_where_keyword($sql)
	{
		if (empty($sql))
		{
			SmartList_Logger::log('warning','SmartList_Mode_Driver find_end_position_of_where_keyword: sql is empty');
			return false;
		}

		preg_match('/^.+FROM\s+.+\s+(WHERE)\s+/siU', $sql, $found,PREG_OFFSET_CAPTURE);

		if (empty($found[1]))
		{
			// where is not in the query
			SmartList_Logger::log('debug','SmartList_Mode_Driver find_end_position_of_where_keyword: where is not in sql');
			return false;
		}

		if (empty($found[1][1]))
		{
			// where is not in the query
			SmartList_Logger::log('debug','SmartList_Mode_Driver find_end_position_of_where_keyword: where was found, but no char position was provided');
			return false;
		}
		
		// the '+5' is for the length of the word 'where' plus a space
		$end_pos = $found[1][1] + 5;

		SmartList_Logger::log('debug','SmartList_Mode_Driver find_end_position_of_where_keyword: end_pos: '.print_r($end_pos,true));

		return $end_pos;
	}

	public function get_affected_records($bean)
	{
		return array();
	}

	public function get_records_from_link($bean,$link_field,$module)
	{
		if (empty($bean))
		{
			SmartList_Logger::log('debug','SmartList_Mode_Driver get_records_from_link: parent_module is empty');
			return false;
		}

		if (empty($link_field))
		{
			SmartList_Logger::log('debug','SmartList_Mode_Driver get_records_from_link: link_field is empty');
			return false;
		}

		if (empty($module))
		{
			SmartList_Logger::log('debug','SmartList_Mode_Driver get_records_from_link: module is empty');
			return false;
		}

		SmartList_Logger::log('debug','SmartList_Mode_Driver get_records_from_link: bean module: '.print_r($bean->module_dir,true));
		SmartList_Logger::log('debug','SmartList_Mode_Driver get_records_from_link: link_field: '.print_r($link_field,true));
		SmartList_Logger::log('debug','SmartList_Mode_Driver get_records_from_link: module: '.print_r($module,true));
			
		if (!method_exists($bean, 'load_relationship'))
		{
			SmartList_Logger::log('debug','SmartList_Mode_Driver get_records_from_link: bean cannot call load_relationship');
			return false;
		}
				
		$bean->load_relationship($link_field);				

		if (!method_exists($bean->$link_field, 'getBeans'))
		{
			SmartList_Logger::log('debug','SmartList_Mode_Driver get_records_from_link: bean->link cannot call getBeans');
			return false;
		}
				
		$records = array();
		foreach ($bean->$link_field->getBeans() as $child_bean) 
		{
    		$records[$child_bean->id] = $module;
		}

		return $records;
	}

	public function is_valid_mode_for_plan($plan)
	{
		if (empty($plan))
		{
			SmartList_Logger::log('debug','SmartList_Mode_Driver is_valid_for_plan: plan is empty, cannot make check');
			return false;
		}

		if (in_array($plan,$this->supported_sugarchimp_plans))
		{
			SmartList_Logger::log('debug','SmartList_Mode_Driver is_valid_for_plan: mode is supported by plan. mode: '.print_r($this->mode,true).' plan: '.print_r($plan,true));
			return true;
		}
		else
		{
			SmartList_Logger::log('debug','SmartList_Mode_Driver is_valid_for_plan: mode is not supported by plan. mode: '.print_r($this->mode,true).' plan: '.print_r($plan,true));
			return false;
		}
	}

    // each mode needs to prepare the smartlist metadata in different ways
    // this method is where that happens
    // this is fired as the smartlist record gets saved
    abstract public function prepare_metadata();

    // if they are currently OFF the list
    // this query will see if they need to be included
    abstract public function get_inclusive_sql();

    // if they are currently ON the list
    // this query will see if they need to be excluded
    abstract public function get_exclusive_sql();
}
