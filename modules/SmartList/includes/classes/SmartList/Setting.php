<?php






require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');

require_once('modules/SmartList/includes/classes/SmartList/Loader.php');
SmartList_Loader::load('Core/Setting');

use Fanatical\Core\v1a\Setting as Setting;

class SmartList_Setting extends Setting
{
    protected static $setting_group = 'smartlist';
    protected static $default_logger_value = 'normal';
    protected static $default_logging_enabled = 'true';

    // this represents the base modules that can be added to target lists with smartlists
    public static $supported_modules = array(
    	'Contacts',
    	'Prospects',
    	'Leads',
    );

    // these are the parent related modules that are supported for the base modules
    // the keys represent the parent related module and the array is the base modules
    // that the parent related module supports
    public static $supported_parent_related_modules = array(
    	'Accounts' => array(
    		'Contacts' => 'contacts',
    		'Leads' => 'leads',
    	),
    	'Users' => array(
    		'Contacts' => 'contacts',
    		'Prospects' => 'prospects',
    		'Leads' => 'leads',
    	),
    	'Contacts' => array(
    		'Contacts' => 'direct_reports',
    		'Leads' => 'leads',
    	),
    	'Teams' => array(
    		'Contacts' => 'contacts',
    		'Prospects' => 'prospects',
    		'Leads' => 'leads',
            'Accounts' => 'accounts',
    	),
    	'Leads' => array(
    		'Leads' => 'reportees',
    	),
    	'Opportunities' => array(
    		'Leads' => 'leads',
    	),
    );

    // for determining which records need to be processed by the smartlist processor
    // when dealing with sql queries we have to infer that based on the sql query
    // this is a basic way to look it up, based on tables names
    // the keys in the array are the parent related modules, the value is their table name
    public static $module_table_mapping = array(
    	'Accounts' => 'accounts',
    	'Contacts' => 'contacts',
    	'Users' => 'users',
    	'Prospects' => 'prospects',
    	'Leads' => 'leads',
    	'Teams' => 'teams',
    	'Opportunities' => 'opportunities'
    );

    // return array of supported modules, accounts module not supported by default
    // if SugarChimp account_syncing_enabled setting is true, Accounts will be added to the array
    public static function get_supported_modules()
    {
        $supported_modules = static::$supported_modules;

        // check if account syncing is enabled
        $account_syncing_enabled = SugarChimp_Setting::retrieve('account_syncing_enabled');

        // if it is enabled, add it to the supported modules array
        if (!empty($account_syncing_enabled))
        {
            $supported_modules []= "Accounts";
        }

        return $supported_modules;
    }
}