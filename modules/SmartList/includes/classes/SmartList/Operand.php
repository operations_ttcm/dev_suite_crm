<?php






require_once('modules/SmartList/includes/classes/SmartList/Operand/Check.php');
require_once('modules/SmartList/includes/classes/SmartList/Operand/Notcheck.php');
require_once('modules/SmartList/includes/classes/SmartList/Operand/Empty.php');
require_once('modules/SmartList/includes/classes/SmartList/Operand/Notempty.php');
require_once('modules/SmartList/includes/classes/SmartList/Operand/Greaterthan.php');
require_once('modules/SmartList/includes/classes/SmartList/Operand/Lessthan.php');
require_once('modules/SmartList/includes/classes/SmartList/Operand/Greaterthanequal.php');
require_once('modules/SmartList/includes/classes/SmartList/Operand/Lessthanequal.php');
require_once('modules/SmartList/includes/classes/SmartList/Operand/Equal.php');
require_once('modules/SmartList/includes/classes/SmartList/Operand/Notequal.php');
require_once('modules/SmartList/includes/classes/SmartList/Operand/Contain.php');
require_once('modules/SmartList/includes/classes/SmartList/Operand/Notcontain.php');
require_once('modules/SmartList/includes/classes/SmartList/Operand/Startswith.php');
require_once('modules/SmartList/includes/classes/SmartList/Operand/Endswith.php');
require_once('modules/SmartList/includes/classes/SmartList/Operand/Oneof.php');
require_once('modules/SmartList/includes/classes/SmartList/Operand/Notoneof.php');
require_once('modules/SmartList/includes/classes/SmartList/Logger.php');


class SmartList_Operand
{
    public static function forge($driver)
    {
        if (empty($driver))
        {
            return false;
        }
        
        $driver = "SmartList_Operand_" . ucfirst($driver);
        
        if(!class_exists($driver))
        {
            SmartList_Logger::log('warning','SmartList_Operand_' . $driver . ': Invalid Operand.');
            return false;            
        }
        return $driver::forge();
    }
}
