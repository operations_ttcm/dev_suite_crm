<?php






require_once('modules/SmartList/includes/classes/SmartList/Operand.php');
require_once('modules/SmartList/includes/classes/SmartList/Logger.php');
require_once('modules/SmartList/includes/classes/SmartList/Field.php');
require_once('modules/SmartList/includes/classes/SmartList/Mode.php');

class SmartList_Filter
{
	public static $alias_counter = 0;
	public static $table_alias_reference = array();
	public static $related_table_alias_reference = array();

	// assumes each filter contains 4 things
	// field
	// type
	// operand
	// value array();
	public static function get_initial_sql($module,$smart,$return_count=false,$not_where=false)
	{
		if (empty($module))
		{
			SmartList_Logger::log('warning','SmartList_Filter get_initial_sql: module is empty');
			return false;
		}

		if (empty($smart) or !is_array($smart))
		{
			SmartList_Logger::log('warning','SmartList_Filter get_initial_sql: smart is empty or not an array');
			return false;
		}

		$smart = static::preprocess_filters($module,$smart);

		SmartList_Logger::log('debug','SmartList_Filter get_initial_sql: smart filters: '.print_r($smart['filters'],true));

		$select = static::get_select($smart['primary_table_alias'],$return_count);
		$from = static::get_from($smart['primary_table'],$smart['primary_table_alias']);
		$join = static::get_joins($smart);
		$where = static::get_wheres($smart,true,$not_where);

		$sql = $select . $from . $join . $where;

		SmartList_Logger::log('debug','SmartList_Filter get_initial_sql: sql: '.print_r($sql,true));

		// if we're dealing with mssql, replace the mysql functions with the mssql counterpart
		// this is a quick workaround until a future more robust solution
		// currently the only things filters use that doesn't work in mssql is ifnull, which is a simply change to isnull
		global $db;
		if ($db->dbType == 'mssql')
		{
			$sql = static::replace_for_mssql($sql);
		}

		return $sql;
	}

	public static function preprocess_filters($module,$smart)
	{
		if (empty($module))
		{
			SmartList_Logger::log('warning','SmartList_Filter preprocess_filters: module is empty');
			return false;
		}

		if (empty($smart) or !is_array($smart))
		{
			SmartList_Logger::log('warning','SmartList_Filter preprocess_filters: smart is empty or not an array');
			return false;
		}

		if (empty($smart['filters']) or !is_array($smart['filters']))
		{
			SmartList_Logger::log('warning','SmartList_Filter preprocess_filters: there are no smart filters, nothing to process. smart: '.print_r($smart,true));
			return $smart;
		}

		$bean = BeanFactory::newBean($module);

		if (empty($bean))
		{
			SmartList_Logger::log('warning','SmartList_Filter preprocess_filters: could not get a bean for '.print_r($module,true));
			return false;
		}

		// set the primary module
		$smart['primary_module'] = $module;
		$smart['primary_table'] = $bean->getTableName();
		$smart['primary_table_alias'] = self::get_table_alias($smart['primary_table']);
		$smart['custom_table'] = $bean->get_custom_table_name();
		$smart['custom_table_alias'] = self::get_table_alias($smart['custom_table']);
		$smart['has_custom_field'] = false; // will get set to true if custom field is included in a filter
		$smart['has_related_field'] = false; // will get set to true if related field is included in a filter

		// get all the module field defs
		$smart['field_defs'] = array();
		$smart['field_defs'][$module] = SmartList_Field::get_module_fields($module);

		// preprocess each filter
		foreach ($smart['filters'] as $key => $filter)
		{
			// db->quote() the values here
			global $db;
			if(!is_array($filter['value']))
			{
				SmartList_Logger::log('debug','db->quoted value: ' .print_r($db->quote($filter['value']),true));
				$filter['value'] = $db->quote($filter['value']);
				$smart['filters'][$key]['value'] = $filter['value'];
			}

			// set the table name for the bean
			// if source = table or empty, $bean->getTableName();
			// if source = custom_fields, $bean->get_custom_table_name()
			// if source = parent_relationship, get relationship, check 
			$data_source = empty($smart['field_defs'][$module][$filter['field']]['source']) ? false : $smart['field_defs'][$module][$filter['field']]['source'];
			
			// determine if field is in related module
			if (SmartList_Field::is_related_field($filter['field']))
			{
				SmartList_Logger::log('debug','SmartList_Filter preprocess_filters: smartlist filter is related field '.print_r($filter['field'],true));
				
				// smart filters include a related field
				$smart['has_related_field'] = true;

				// it's a related field
				$smart['filters'][$key]['related_field'] = true;

				// lookup link field and get relationship
				// only support one level of depth
				$tokens = explode('.',$filter['field']);

				if (!is_array($tokens) or count($tokens)!=2)
				{
					SmartList_Logger::log('debug','SmartList_Filter preprocess_filters: filter is related field but does not have two tokens '.print_r($filter,true));
					continue;
				}

				$link_field = $tokens[0];
				$related_field = $tokens[1];

				if (empty($smart['field_defs'][$module][$link_field]))
				{
					SmartList_Logger::log('debug','SmartList_Filter preprocess_filters: link_field does not exist in field defs '.print_r($link_field,true));
					continue;
				}

				$link_field_defs = $smart['field_defs'][$module][$link_field];
				$smart['filters'][$key]['link_field_defs'] = $link_field_defs;

				if (empty($link_field_defs['relationship']))
				{
					SmartList_Logger::log('debug','SmartList_Filter preprocess_filters: no relationship definition for link_field_defs '.print_r($link_field_defs,true));
					continue;
				}

				$link_field_relationship = SmartList_Field::get_relationship_definition($link_field_defs['relationship']);
				$smart['filters'][$key]['link_field_relationship'] = $link_field_relationship;

				if (empty($link_field_relationship))
				{
					SmartList_Logger::log('debug','SmartList_Filter preprocess_filters: could not find a relationship definition for link_field_defs relationship '.print_r($link_field_defs['relationship'],true));
					continue;
				}

				// load up the field defs for the custom module
				$related_module = ($module == $link_field_relationship['lhs_module']) ? $link_field_relationship['rhs_module'] : $link_field_relationship['lhs_module'];
				$smart['filters'][$key]['module'] = $related_module;
				
				SmartList_Logger::log('debug','SmartList_Filter preprocess_filters: related module '.print_r($related_module,true));

				if (empty($smart['field_defs'][$related_module]))
				{
					SmartList_Logger::log('debug','SmartList_Filter preprocess_filters: Looking up field defs for related module '.print_r($related_module,true));
					$smart['field_defs'][$related_module] = SmartList_Field::get_module_fields($related_module);
				}	

				if (empty($smart['field_defs'][$related_module][$related_field]))
				{
					SmartList_Logger::log('debug','SmartList_Filter preprocess_filters: could not find a field def for '.print_r($related_field,true));
					continue;
				}

				$related_bean = BeanFactory::newBean($related_module);

				if (empty($related_bean))
				{
					SmartList_Logger::log('warning','SmartList_Filter preprocess_filters: could not get a related_bean for '.print_r($related_module,true));
					continue;
				}

				$related_data_source = empty($smart['field_defs'][$related_module][$related_field]['source']) ? false : $smart['field_defs'][$related_module][$related_field]['source'];

				if (empty($related_data_source) || $related_data_source == 'table')
				{
					$smart['filters'][$key]['table'] = $related_bean->getTableName();
					$smart['filters'][$key]['table_alias'] = self::get_table_alias($smart['filters'][$key]['table'],true);
					$smart['filters'][$key]['sql_field'] = $smart['filters'][$key]['table_alias'] . '.' . $related_field;
				}
				else if ($related_data_source == 'custom_fields')
				{
					// mark this field as custom
					$smart['filters'][$key]['custom_field'] = true;

					$smart['filters'][$key]['table'] = $related_bean->get_custom_table_name();
					$smart['filters'][$key]['table_alias'] = self::get_table_alias($smart['filters'][$key]['table'],true);
					$smart['filters'][$key]['sql_field'] = $smart['filters'][$key]['table_alias'] . '.' . $related_field;
				}
			}
			else if (empty($data_source) || $data_source == 'table')
			{
				$smart['filters'][$key]['custom_field'] = false;
				$smart['filters'][$key]['related_field'] = false;

				$smart['filters'][$key]['table'] = $smart['primary_table'];
				$smart['filters'][$key]['table_alias'] = $smart['primary_table_alias'];
				$smart['filters'][$key]['sql_field'] = $smart['primary_table_alias'] . '.' . $filter['field'];
			}
			else if ($data_source == 'custom_fields')
			{
				// we have a custom field from the primary module
				$smart['has_custom_field'] = true;
				
				// mark this field as custom
				$smart['filters'][$key]['custom_field'] = true;

				$smart['filters'][$key]['related_field'] = false;

				$smart['filters'][$key]['table'] = $smart['custom_table'];
				$smart['filters'][$key]['table_alias'] = $smart['custom_table_alias'];
				$smart['filters'][$key]['sql_field'] = $smart['custom_table_alias'] . '.' . $filter['field'];
			}
		}

		return $smart;
	}

	public static function get_select($table,$return_count=false)
	{
		if ($return_count === true)
		{
			return " SELECT COUNT({$table}.id) AS number_records ";
		}
		else
		{
			return " SELECT {$table}.id AS ".SmartList_Mode::$sql_key_id." ";
		}
	}

	public static function get_from($table,$table_alias)
	{
		return " FROM {$table} {$table_alias} ";
	}

	public static function get_joins($smart,$return_sql_string=true)
	{
		if (empty($smart) or !is_array($smart))
		{
			SmartList_Logger::log('warning','SmartList_Filter get_joins: smart is empty or not an array');
			return false;
		}

		$joins = array();

		if ($smart['has_custom_field'] === true)
		{
			// return " INNER JOIN {$smart['custom_table']} ON {$smart['custom_table']}.id_c={$smart['primary_table']}.id ";
			$joins[$smart['custom_table_alias']] = array(
				'join_type' => 'LEFT JOIN',
				'join_table' => $smart['custom_table'],
				'join_table_alias' => $smart['custom_table_alias'],
				'join_on' => array(
					array(
						'left_field' => $smart['custom_table_alias'] . '.id_c',
						'operand' => '=',
						'right_field' => $smart['primary_table_alias'] . '.id',
					),
				),
			);
		}

		if ($smart['has_related_field'] === true)
		{
			// find the related fields and setup their join data
			foreach ($smart['filters'] as $filter)
			{
				if ($filter['related_field'] === true)
				{
					// we have a related field, setup the join data
					if (empty($filter['link_field_relationship']['join_table']) || $filter['link_field_relationship']['relationship_type']=='one-to-many')
					{
						if (empty($filter['link_field_relationship']['lhs_table']) || empty($filter['link_field_relationship']['lhs_key']) || empty($filter['link_field_relationship']['rhs_table']) || empty($filter['link_field_relationship']['rhs_key']))
						{
							SmartList_Logger::log('warning','SmartList_Filter get_joins: could not get join data for related custom field: '.print_r($filter,true));
							continue;
						}

						$lhs_table_alias = self::get_table_alias($filter['link_field_relationship']['lhs_table'],true);
						$rhs_table_alias = self::get_table_alias($filter['link_field_relationship']['rhs_table']);

						// the join id is in the root table
						// inner join {relationship.lhs_table} on 
						// {relationship.lhs_table}.{relationship.lhs_key}
						// 	={relationship.rhs_table}.{relationship.rhs_key}
						$joins[$lhs_table_alias] = array(
							'join_type' => 'LEFT JOIN',
							'join_table' => $filter['link_field_relationship']['lhs_table'],
							'join_table_alias' => $lhs_table_alias,
							'join_on' => array(
								array(
									'left_field' => $lhs_table_alias.'.'.$filter['link_field_relationship']['lhs_key'],
									'operand' => '=',
									'right_field' => $rhs_table_alias.'.'.$filter['link_field_relationship']['rhs_key'],
								),
							),
						);
					}
					else
					{
						if (empty($filter['link_field_relationship']['join_table']) || empty($filter['link_field_relationship']['lhs_table']) || empty($filter['link_field_relationship']['lhs_key']) || empty($filter['link_field_relationship']['rhs_table']) || empty($filter['link_field_relationship']['rhs_key']))
						{
							SmartList_Logger::log('warning','SmartList_Filter get_joins: could not get join data for related custom field: '.print_r($filter,true));
							continue;
						}

						$join_table_alias = self::get_table_alias($filter['link_field_relationship']['join_table'],true);
						$rhs_table_alias = self::get_table_alias($filter['link_field_relationship']['rhs_table']);
						$lhs_table_alias = self::get_table_alias($filter['link_field_relationship']['lhs_table'],true);

						// gotta join the join table
						// inner join {relationship.join_table} on 
						// {relationship.join_table}.{relationship.lhs_key}
						// 	={relationship.rhs_table}.{relationship.rhs_key}
						$joins[$join_table_alias] = array(
							'join_type' => 'LEFT JOIN',
							'join_table' => $filter['link_field_relationship']['join_table'],
							'join_table_alias' => $join_table_alias,
							'join_on' => array(
								array(
									'left_field' => $join_table_alias.'.'.$filter['link_field_relationship']['join_key_rhs'],
									'operand' => '=',
									'right_field' => $rhs_table_alias.'.'.$filter['link_field_relationship']['rhs_key'],
								),
								array(
									'left_field' => $join_table_alias.'.deleted',
									'operand' => '=',
									'right_field' => '0',
								),
							),
						);

						// then gotta join the data table
						// inner join {relationship.lhs_table} on
						// {relationship.join_table}.{relationship.lhs_key}
						// 	={relationship.lhs_table}.{relationship.lhs_key}
						$joins[$lhs_table_alias] = array(
							'join_type' => 'LEFT JOIN',
							'join_table' => $filter['link_field_relationship']['lhs_table'],
							'join_table_alias' => $lhs_table_alias,
							'join_on' => array(
								array(
									'left_field' => $join_table_alias.'.'.$filter['link_field_relationship']['join_key_lhs'],
									'operand' => '=',
									'right_field' => $lhs_table_alias.'.'.$filter['link_field_relationship']['lhs_key'],
								),
								array(
									'left_field' => $join_table_alias.'.deleted',
									'operand' => '=',
									'right_field' => '0',
								),
							),
						);
					}

					if ($filter['custom_field'] === true)
					{
						if (empty($filter['module']) || empty($filter['table']))
						{
							SmartList_Logger::log('warning','SmartList_Filter get_joins: could not get join data for related custom field: '.print_r($filter,true));
							continue;
						}

						$temp_bean = BeanFactory::getBean($filter['module']);
						$parent_table = $temp_bean->getTableName();
						$parent_table_alias = self::get_table_alias($parent_table,true);
						$parent_custom_table = $temp_bean->get_custom_table_name();
						$parent_custom_table_alias = self::get_table_alias($parent_custom_table,true);
						
						// join in the custom related table
						$joins[$parent_custom_table_alias] = array(
							'join_type' => 'LEFT JOIN',
							'join_table' => $parent_custom_table,
							'join_table_alias' => $parent_custom_table_alias,
							'join_on' => array(
								array(
									'left_field' => $parent_custom_table_alias.'.id_c',
									'operand' => '=',
									'right_field' => $parent_table_alias.'.id',
								),
							),
						);
					}
				}	
			}
		}

		SmartList_Logger::log('debug','SmartList_Filter get_joins: joins: '.print_r($joins,true));

		if ($return_sql_string === false)
		{
			return $joins;
		}

		$join_sql = '';
		foreach ($joins as $join)
		{
			$join_sql .= ' '. $join['join_type'] . ' ' . $join['join_table'] . ' ' . $join['join_table_alias'] . ' ON ';
			$first = true;
			foreach ($join['join_on'] as $on)
			{
				if ($first === false)
				{
					$join_sql .= ' AND ';
				}

				$join_sql .= ' '.$on['left_field'] . ' ' . $on['operand'] . ' ' . $on['right_field'] . ' ';
				$first = false;
			}
		}

		return $join_sql;
	}

	public static function get_table_alias($table_name,$is_related=false)
	{
		if (empty($table_name))
		{
			SmartList_Logger::log('debug','SmartList_Filter get_table_alias: table_name is empty, cannot generate alias');
			return false;
		}

		SmartList_Logger::log('debug','SmartList_Filter get_table_alias: table_name: '.print_r($table_name,true));
		SmartList_Logger::log('debug','SmartList_Filter get_table_alias: is_related: '.print_r($is_related,true));
		SmartList_Logger::log('debug','SmartList_Filter get_table_alias: table_alias_reference: '.print_r(self::$table_alias_reference,true));
		SmartList_Logger::log('debug','SmartList_Filter get_table_alias: related_table_alias_reference: '.print_r(self::$related_table_alias_reference,true));

		if ($is_related === false)
		{
			$existing_alias = array_search($table_name,self::$table_alias_reference);
		
			if ($existing_alias !== false)
			{
				return $existing_alias;
			}

			$alias = $table_name . self::$alias_counter;

			self::$table_alias_reference[$alias] = $table_name;
		}
		else
		{
			$existing_alias = array_search($table_name,self::$related_table_alias_reference);
		
			if ($existing_alias !== false)
			{
				return $existing_alias;
			}

			// add "r" for related
			$alias = $table_name . 'r' . self::$alias_counter;

			self::$related_table_alias_reference[$alias] = $table_name;
		}

		self::$alias_counter++;

		SmartList_Logger::log('debug','SmartList_Filter get_table_alias: table_alias_reference: '.print_r(self::$table_alias_reference,true));
		SmartList_Logger::log('debug','SmartList_Filter get_table_alias: related_table_alias_reference: '.print_r(self::$related_table_alias_reference,true));

		return $alias;
	}

	public static function get_wheres($smart,$return_sql_string=true,$not_where=false)
	{
		if (empty($smart) or !is_array($smart))
		{
			SmartList_Logger::log('warning','SmartList_Filter get_wheres: smart is empty or not an array');
			return false;
		}

		// SmartList_Logger::log('debug','SmartList_Filter get_wheres: smart: '.print_r($smart,true));

		$join = 'OR';
		if (!empty($smart['join']))
		{
			$join = $smart['join'];
		}

		$where = array();

		if (!empty($smart['filters']))
		{
			foreach ($smart['filters'] as $filter)
			{
				SmartList_Logger::log('debug','SmartList_Filter get_wheres: filter: '.print_r($filter,true));
				$where []= SmartList_Operand::forge($filter['operand'])->render_sql($filter);
			}
		}

		if ($return_sql_string === true)
		{
			$sql = ' WHERE ';

			// optional to not the filter where for exclusion query
			if ($not_where === true)
			{
				$sql .= ' NOT ';
			}

			$sql .= '( ';
			$first = true;
			foreach ($where as $w)
			{
				if (empty($first))
				{
					$sql .= ' ' . $join . ' ';
				}

				$sql .= $w;

				$first = false;
			}
			$sql .= " ) AND {$smart['primary_table_alias']}.deleted=0";
			return $sql;
		}
		else
		{
			return $where;
		}
	}

	public static function replace_for_mssql($sql)
	{
		if (empty($sql))
		{
			SmartList_Logger::log('warning','SmartList_Filter replace_for_mssql: sql is empty');
			return false;
		}		

		$sql = str_ireplace('IFNULL(', 'ISNULL(', $sql);

		SmartList_Logger::log('debug','SmartList_Filter::replace_for_mssql: sql after mssql replacements: '.print_r($sql,true));

		return $sql;
	}
}