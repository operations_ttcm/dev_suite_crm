<?php






require_once('modules/SmartList/includes/classes/SmartList/Mode/All.php');
require_once('modules/SmartList/includes/classes/SmartList/Mode/Smart.php');
require_once('modules/SmartList/includes/classes/SmartList/Mode/Sql.php');
require_once('modules/SmartList/includes/classes/SmartList/Mode/None.php');
require_once('modules/SmartList/includes/classes/SmartList/Logger.php');


class SmartList_Mode
{
    public static $sql_key_id = 'smartlist_lookup_id';
    public static $sql_unallowed = array(
        'LIMIT',
        'ORDER BY',
        'GROUP BY',
        'HAVING',
    );

    public static function forge($driver,$module=false,$metadata=array())
    {
        if (empty($driver))
        {
        	SmartList_Logger::log('fatal','SmartList_Mode forge: driver name was empty, cannot forge object');
            return false;
        }
        
        $driver = "SmartList_Mode_" . ucfirst($driver);
        
        if(!class_exists($driver))
        {
            SmartList_Logger::log('warning','SmartList_Mode_' . $driver . ': Invalid Operand.');
            return false;            
        }

        SmartList_Logger::log('debug',"Instantiate SmartList_Mode Driver {$driver}");
        return $driver::forge($driver,$module,$metadata);
    }

    // checks that the sql starts with select
    // checks that the sql does not contain invalid words
    public static function is_valid_sql($sql)
    {
        // making things all upper case for easy checking
        // trim it to catch spaces on the beginning/end
        $sql = strtoupper(trim($sql));
        
        // must start with SELECT
        if(substr($sql,0,6) != 'SELECT')
        {
            SmartList_Logger::log('debug','SQL did not contain a SELECT query: ' . print_r($sql,true));
            return array(
                'success'=>false,
                'message'=>'Invalid Query: must be a SELECT query.'
            );
        }
        
        foreach(static::$sql_unallowed as $word)
        {
            if(strpos($sql,$word) !== false)
            {
                SmartList_Logger::log('debug','SugarChimpApi::testSQLQuery: SQL contains invalid keyword: ' . print_r($word,true));
                return array(
                    'success'=>false,
                    'message'=>'SQL contains invalid keyword: ' . $word
                );
            }
        }

        return array(
            'success'=>true,
            'message'=>'Valid Query'
        );
    }
}
