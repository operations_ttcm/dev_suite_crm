<?php






require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SmartList/includes/classes/SmartList/Helper.php');
require_once('modules/SmartList/includes/classes/SmartList/Logger.php');
require_once('modules/SmartList/includes/classes/SmartList/Mode.php');
require_once('modules/SmartList/includes/classes/SmartList/List.php');
require_once('modules/SmartList/includes/classes/SmartList/Setting.php');
require_once('modules/SmartList/includes/classes/SmartList/Field.php');

require_once('modules/SmartList/includes/classes/SmartList/Loader.php');
SmartList_Loader::load('Core/Queue');

use Fanatical\Core\v1a\Queue as Queue;
use BeanFactory as BeanFactory;

class SmartList_Queue extends Queue
{
    protected $max_records_to_process = 100;
    protected $queue_table_name = 'smartlist_queue';

	public static function queue($data)
	{
		if (empty($data['module']) or empty($data['id']))
		{
			SmartList_Logger::log('warning',"queue - module or id was empty");
			return false;
		}

		SmartList_Logger::log('debug',"queue - module: {$data['module']}");
		SmartList_Logger::log('debug',"queue - id: {$data['id']}");

		$smartlist = BeanFactory::newBean('SmartListQueue');
		$smartlist->module = $data['module'];
		$smartlist->record = $data['id'];
		$smartlist->save();

		SmartList_Logger::log('debug',"queue - created SmartListQueue entry");
		return true;
	}

	public static function process_record_change($bean)
    {
    	if (empty($bean->module_dir) or empty($bean->id))
    	{
    		SmartList_Logger::log('warning','SmartList_Queue::process_record_change: could not process change of bean, empty module_dir or ID');
    		return false;
    	}

        $module = $bean->module_dir;
        $record = $bean->id;

        // want to see all debug data here except for scheduler jobs module
        // these get modified a lot as they are auto updated for scheduler stuff
  		// if ($module != 'SchedulersJobs')
  		// {
		// 		SmartList_Logger::log('debug','SmartList_Queue::process_record_change: module: '.print_r($module,true));
		// 		SmartList_Logger::log('debug','SmartList_Queue::process_record_change: record: '.print_r($record,true));
		// }

        $supported_modules = SmartList_Setting::get_supported_modules();

        if (empty($supported_modules) or !is_array($supported_modules))
        {
        	SmartList_Logger::log('fatal','SmartList_Queue::process_record_change: there are no supported modules');
        	return false;
        }

        if (in_array($module,$supported_modules))
        {
            SmartList_Logger::log('debug','SmartList_Queue::process_record_change: supported module detected');
    		
    		static::queue(array(
                'module' => $module,
                'id' => $record
            ));
        }

        return true;
	}


	public static function process_related_record_change($bean)
	{
    	if (empty($bean->module_dir) or empty($bean->id))
    	{
    		SmartList_Logger::log('warning','SmartList_Queue::process_related_record_change: could not process change of bean, empty module_dir or ID');
    		return false;
    	}

        $module = $bean->module_dir;
        $record = $bean->id;

		// want to see all debug data here except for scheduler jobs module
		// these get modified a lot as they are auto updated for scheduler stuff
		// if ($module != 'SchedulersJobs')
		// {
		// 	SmartList_Logger::log('debug','SmartList_Queue::process_related_record_change: module: '.print_r($module,true));
		// 	SmartList_Logger::log('debug','SmartList_Queue::process_related_record_change: record: '.print_r($record,true));
		// }
		
		if (empty(SmartList_Setting::$supported_parent_related_modules) or !is_array(SmartList_Setting::$supported_parent_related_modules))
		{
			SmartList_Logger::log('debug','SmartList_Queue::process_related_record_change: supported_parent_related_modules is empty or not an array');
			return false;
		}

        $supported_parent_related_modules = SmartList_Setting::$supported_parent_related_modules;

        // this log gets a little noisy...
		// SmartList_Logger::log('debug','SmartList_Queue::process_related_record_change: supported_parent_related_modules: '.print_r($supported_parent_related_modules,true));
            
        if (!array_key_exists($module, $supported_parent_related_modules))
        {
        	// SmartList_Logger::log('debug','SmartList_Queue::process_related_record_change: this is not a supported parent related module');
        	return true;
        }
            
        SmartList_Logger::log('debug','SmartList_Queue::process_related_record_change: supported parent related module detected');
		
		// get smartlists' metadata
		$smartlist = BeanFactory::newBean('SmartList');
		$smartlists = $smartlist->get_active_smartlists();

		if (empty($smartlists))
		{
			SmartList_Logger::log('debug','SmartList_Queue::process_related_record_change: there are no smartlists saved');
			return true;
		}

		SmartList_Logger::log('debug','SmartList_Queue::process_related_record_change: start to find parent related filters');
		$children_to_queue = array();

		foreach ($smartlists as $smartlist)
		{
			foreach ($smartlist->metadata as $base_module => $smart_def)
			{
				SmartList_Logger::log('debug','SmartList_Queue::process_related_record_change: smartlist id: '.print_r($smartlist->id,true));
				SmartList_Logger::log('debug','SmartList_Queue::process_related_record_change: base module: '.print_r($base_module,true));
				SmartList_Logger::log('debug','SmartList_Queue::process_related_record_change: smart_def: '.print_r($smart_def,true));
				
				if (empty($smart_def['mode'])) continue;

				$mode = $smart_def['mode'];

				if (empty($smart_def[$mode])) continue;

				$mode_metadata = $smart_def[$mode];

				$mode_object = SmartList_Mode::forge($mode,$base_module,$mode_metadata);

				$affected_records = $mode_object->get_affected_records($bean);

				if (!empty($affected_records) and is_array($affected_records))
				{
					foreach ($affected_records as $child_id => $child_module)
					{
		        		static::queue(array(
			                'module' => $child_module,
			                'id' => $child_id,
			            ));
					}
				}
			}
		}

		SmartList_Logger::log('debug','SmartList_Queue::process_related_record_change: children to queue: '.print_r($children_to_queue,true));

        return true;
    }

	public function process()
	{
		SmartList_Logger::log('debug','SmartList_Queue::process() - start process.');
		global $db;

		$smartlist = BeanFactory::newBean('SmartList');
		$smartlists = $smartlist->get_active_smartlists();

		if (empty($smartlists))
		{
			SmartList_Logger::log('debug','SmartList_Queue process: There are no active smartlists. Cleaning Queue and Ending Process.');
			// if there are no smartlists to check against, clear the queue
			// it will get filled back up whenever new smartlists are created/enabled
			$this->empty_queue();
			return true;
		}

		$current_plan = SugarChimp_Helper::get_current_plan();

		if (empty($current_plan))
		{
			// there is not an active sugarchimp subscription, can't run smartlist
			SmartList_Logger::log('fatal','SmartList_Queue process: there is not an active sugarchimp subscription. current_plan: '.print_r($current_plan,true));
			return true;
		}

		while ($this->check_timer() && $chunk = $this->get_chunk())
		{
			SmartList_Logger::log('debug','SmartList_Queue process: found a chunk.');
			// $chunk => array(
			// 	'Contacts' => array(
			// 		'{queue_id}' => '{record_id}',
			// 	)
			// )

			// the ids array keeps track of the id of the record in the smartlist_queue that gets processed
			// they are then passed to the delete chunk function for removal
			$ids = array();

			foreach ($chunk as $module => $people)
			{
				// get the ids for this module chunk
				$ids = array_merge($ids,array_keys($people));

				SmartList_Logger::log('debug','SmartList_Queue::process module: '.$module);
				foreach ($smartlists as $smartlist)
		        {
		        	static::process_module_for_smartlist($smartlist,$module,$people);
	    		}
	        }

	        $this->delete_chunk($ids);

	        $this->update_timer();
		}

		SmartList_Logger::log('debug','SmartList_Queue process: ending process');
		return true;
	}
	// smartlist - bean
	// module = 'Contacts'
	// people = array(
	// 		'unqiue_smartlist_queue_id'=>'module_record_id'
	//	)
	public static function process_module_for_smartlist($smartlist, $module, $people)
	{
		global $db;
		SmartList_Logger::log('debug','SmartList_Queue::process_module_for_smartlist: smartlist: '.$smartlist->id);
		$current_plan = SugarChimp_Helper::get_current_plan();

    	if (empty($smartlist->metadata[$module]) || !is_array($smartlist->metadata[$module]))
    	{
    		SmartList_Logger::log('warning','SmartList_Queue::process_module_for_smartlist: there is no smartlist metadata for module: '.print_r($module,true));
    		return false;
    	}

    	if (empty($smartlist->metadata[$module]['mode']))
    	{
    		SmartList_Logger::log('fatal','SmartList_Queue::process_module_for_smartlist there is no smartlist mode for this module: '.print_r($module,true));
    		return false;
    	}

    	// get the data we need from the metadata 
    	$module_metadata = $smartlist->metadata[$module];
    	$module_mode = $module_metadata['mode'];
    	$mode_metadata = array();

    	if ($module_mode == 'none')
    	{
    		SmartList_Logger::log('debug','SmartList_Queue::process_module_for_smartlist: smartlist disabled: '.print_r($smartlist->metadata[$module],true));
    		return false;
    	}

    	// if the metadata for the specific mode isset
    	// let's use it
    	if (isset($module_metadata[$module_mode]))
    	{
    		$mode_metadata = $module_metadata[$module_mode];
    	}

    	// get the mode object
    	$mode = SmartList_Mode::forge($module_mode,$module,$mode_metadata);

    	if ($mode->is_valid_mode_for_plan($current_plan) === false)
    	{
    		// this mode is not supported by the current sugarchimp subscription.
			SmartList_Logger::log('fatal','SmartList_Queue::process_module_for_smartlist: Current plan: '.print_r($current_plan,true));
    		SmartList_Logger::log('fatal','SmartList_Queue::process_module_for_smartlist: This mode is not supported by your current SugarChimp subscription. Please contact support@sugarchimp.com. Current mode: '.print_r($mode,true));

    		$error_data = array(
    			'module_mode' => $module_mode,
    			'module' => $module,
    			'prospect_list_id' => $smartlist->prospect_list_id,
    		);
    		SmartList_Helper::send_mismatch_plan_email($error_data);
    		return false;
    	}

    	if (empty($smartlist->prospect_list_id))
    	{
    		SmartList_Logger::log('warning','SmartList_Queue::process_module_for_smartlist: smartlist does not contain a target list id');
    		return false;
    	}

    	$target_list_id = $smartlist->prospect_list_id;
    	
    	if (empty($target_list_id))
    	{
    		SmartList_Logger::log('warning','SmartList_Queue::process_module_for_smartlist: there is no target list id for this smart list');
    		return false;
    	}

    	// select everyone that is ON the list AND in our chunk of people
    	$sql = "SELECT related_id AS id
    			FROM prospect_lists_prospects
    			WHERE 
    				prospect_list_id='".$db->quote($target_list_id)."' AND
    				related_id IN ('" . implode("','",$people) . "') AND
    				deleted=0";

		if (!$result = $db->query($sql))
		{
			SmartList_Logger::log('fatal','SmartList_Queue::process_module_for_smartlist: get on list query failed: '.$sql);
			return false;
		}

		// put the people on the list in the on_list array
		$on_list = array();
		while ($row = $db->fetchByAssoc($result))
        {
            $on_list []= $row['id'];
        }

        // didn't run a separate query for the off list because if the prospect_lists_prospects
        // table is huge, it would run slower than just processing it on the php side
        if (empty($on_list) || !is_array($on_list))
        {
        	$off_list = array_values($people);
        }
        else
        {
        	// can do a diff on the array values to get the opposite of the on list
        	$off_list = array_diff(array_values($people),$on_list);
        }

        SmartList_Logger::log('debug','SmartList_Queue::process_module_for_smartlist on list: '.print_r($on_list,true));
        SmartList_Logger::log('debug','SmartList_Queue::process_module_for_smartlist off list: '.print_r($off_list,true));

        //
        // $on_list and $off_list values contain their respective IDs
        //

    	$inclusive_sql = $mode->get_inclusive_sql($off_list);
		// SmartList_Logger::log('debug','SmartList_Queue process: inclusive_sql: '.print_r($inclusive_sql,true));

		$exclusive_sql = $mode->get_exclusive_sql($on_list);
		// SmartList_Logger::log('debug','SmartList_Queue process: exclusive_sql: '.print_r($exclusive_sql,true));


		if (!empty($inclusive_sql))
		{
			// run the inclusive query
			if (!$result = $db->query($inclusive_sql))
			{
				SmartList_Logger::log('fatal','SmartList_Queue::process_module_for_smartlist: inclusive sql failed: '.print_r($inclusive_sql,true));
			}
			else
			{
				$add_to_list = array();
				while ($row = $db->fetchByAssoc($result))
		        {
		            $add_to_list []= $row[SmartList_Mode::$sql_key_id];
		        }

		        if (!empty($add_to_list))
		        {
		        	SmartList_List::add_to_list($target_list_id,$module,$add_to_list);
		        }
		        else
		        {
		        	SmartList_Logger::log('debug','SmartList_Queue::process_module_for_smartlist: add_to_list is empty');
		        }
			}
	    }

	    if (!empty($exclusive_sql))
	    {
			// run the exclusive query
			if (!$result = $db->query($exclusive_sql))
			{
				SmartList_Logger::log('fatal','SmartList_Queue::process_module_for_smartlist: inclusive sql failed: '.print_r($inclusive_sql,true));
			}
			else
			{
				$remove_from_list = array();
				while ($row = $db->fetchByAssoc($result))
		        {
		            $remove_from_list []= $row[SmartList_Mode::$sql_key_id];
		        }

		        if (!empty($remove_from_list))
		        {
		        	
		        	SmartList_List::remove_from_list($target_list_id,$module,$remove_from_list);
		        }
		        else
		        {
		        	SmartList_Logger::log('debug','SmartList_Queue::process_module_for_smartlist: remove_from_list is empty');
		        }
		    }
		}
		return true;
	}
	public function get_chunk($table=false,$max_records_to_process=false)
	{
    	if (empty($table) && empty($this->queue_table_name))
		{
			SmartList_Logger::log('warning','SmartList_Queue get_chunk: table is empty');
			return false;
		}

    	if (empty($max_records_to_process) && empty($this->max_records_to_process))
		{
			SmartList_Logger::log('warning','SmartList_Queue get_chunk: max_records_to_process is empty');
			return false;
		}

        $table = (!empty($table)) ? $table : $this->queue_table_name;
		$max_records_to_process = (!empty($max_records_to_process)) ? $max_records_to_process : $this->max_records_to_process;

		global $db;

		$sql = "SELECT id AS id, module AS module, record AS record FROM " . $db->quote($table) . " ORDER BY date_entered ASC";

		// use sugar db tools to create the limit query
		$sql = $db->limitQuery($sql,0,$db->quote($max_records_to_process),false,'',false);

		// SmartList_Logger::log('debug','SmartList_Queue get_chunk: query: '.$sql);

		/**

		76dev todo-chad: Make sure this works on SQL Server

		*/

		if (!$result = $db->query($sql))
		{
			SmartList_Logger::log('fatal','SmartList_Queue get_chunk: Query failed: '.$sql);
			return false;
		}

		$chunk = array();
		while ($row = $db->fetchByAssoc($result))
        {
            $chunk[$row['module']][$row['id']] = $row['record'];
        }

        // if there are no chunks, return false
        if (count($chunk) == 0)
        {
        	return false;
        }

		return $chunk;
	}

	public function delete_chunk($ids=array(),$table=false,$max_records_to_process=false)
	{
    	if (empty($table) && empty($this->queue_table_name))
		{
			SmartList_Logger::log('warning','SmartList_Queue delete_chunk: table is empty');
			return false;
		}

    	if (empty($max_records_to_process) && empty($this->max_records_to_process))
		{
			SmartList_Logger::log('warning','SmartList_Queue delete_chunk: max_records_to_process is empty');
			return false;
		}

        if (empty($ids) || !is_array($ids))
		{
			SmartList_Logger::log('warning','SmartList_Queue delete_chunk: ids is not an array or is empty');
			return false;
		}

        $table = (!empty($table)) ? $table : $this->queue_table_name;
		$max_records_to_process = (!empty($max_records_to_process)) ? $max_records_to_process : $this->max_records_to_process;

		global $db;

		// $sql = "DELETE FROM " . $db->quote($table) . " ORDER BY date_entered ASC LIMIT " . $db->quote($max_records_to_process);
		$sql = "DELETE FROM " . $db->quote($table) . " WHERE id IN ('".implode("','", $ids)."')";
		// SmartList_Logger::log('debug','SmartList_Queue delete_chunk: query: '.$sql);

		/**

		76dev todo: Make sure this works on SQL Server

		*/

		if (!$result = $db->query($sql))
		{
			SmartList_Logger::log('fatal','SmartList_Queue delete_chunk: Query failed: '.$sql);
			return false;
		}

		return true;
	}

	public function empty_queue($table=false)
    {
    	if (empty($table) && empty($this->queue_table_name))
        {
        	SmartList_Logger::log('debug','SmartList_Queue empty_queue: No table name available to empty queue with.');
        	return false;
        }

        $table = (!empty($table)) ? $table : $this->queue_table_name;

        global $db;

        $sql = "TRUNCATE TABLE {$table}";
                
        SmartList_Logger::log('debug','SmartList_Queue empty_queue: sql: '.$sql);

        if (!$result = $db->query($sql))
        {
        	SmartList_Logger::log('fatal','SmartList_Queue empty_queue: sql failed: '.$sql);
        	return false;
        }

    	SmartList_Logger::log('debug','SmartList_Queue empty_queue: Queue cleared successfully.');
    	return true;
    }
}