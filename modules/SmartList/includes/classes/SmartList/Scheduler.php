<?php






require_once('modules/SmartList/includes/classes/SmartList/Logger.php');
require_once('modules/SmartList/includes/classes/SmartList/Queue.php');
require_once('modules/SmartList/includes/classes/SmartList/Loader.php');
SmartList_Loader::load('Core/Scheduler');

use Fanatical\Core\v1a\Scheduler as Scheduler;

class SmartList_Scheduler extends Scheduler
{
	public static $job_function = "SmartList";
	
	public static function run()
	{
		SmartList_Logger::log('debug','SmartList_Scheduler run: Starting SmartList Scheduler.');
		SmartList_Queue::forge()->start();
	}
}