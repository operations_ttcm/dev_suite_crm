<?php






require_once('modules/SmartList/includes/classes/SmartList/Loader.php');
require_once('modules/SmartList/includes/classes/SmartList/Setting.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');

SmartList_Loader::load('Core/Logger');


use Fanatical\Core\v1a\Logger as Logger;

class SmartList_Logger extends Logger
{
	protected static $prefix = "SmartList";

	//If you do not want to include location information on log,
	//Then set $format to false
	//Otherwise, parent logger will include that information
	public static function log($level, $message = '', $format = true)
	{
		//If logging_enabled has been set to false
        $logging_enabled = SmartList_Setting::retrieve('logging_enabled');
        if(empty($logging_enabled))
        {
            return;
        }
        
        // smartlist_logger mirrors sugarchimp logger
        $smartlist_logger = SmartList_Setting::retrieve('logger');

        // if 'debug', all smartlist messages are logged at 'fatal' level
        // this allows you to run sugarchimp on debug mode and the rest of the system at any other level
        // if not 'debug', run on level provided in call
        if (!empty($smartlist_logger) && $smartlist_logger == 'debug')
        {
            $level = 'fatal';
        }

		parent::log($level, $message, $format);
	}
}