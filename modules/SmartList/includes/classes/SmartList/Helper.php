<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/License.php');
require_once('modules/SugarChimp/license/OutfittersLicense.php');

require_once('modules/SmartList/includes/classes/SmartList/Logger.php');
require_once('modules/SmartList/includes/classes/SmartList/Mode.php');
require_once('modules/SmartList/includes/classes/SmartList/Queue.php');
require_once('modules/SmartList/includes/classes/SmartList/List.php');
require_once('modules/SmartList/includes/classes/SmartList/Setting.php');
require_once('modules/SmartList/includes/classes/SmartList/Field.php');
require_once('modules/SmartList/includes/classes/SmartList/Loader.php');
SmartList_Loader::load('Core/Queue');

use Fanatical\Core\v1a\Queue as Queue;
use BeanFactory as BeanFactory;

class SmartList_Helper
{

	// if no smartlist exist, or all modes are set to none, returns false
    public static function process_beans_for_list($target_list, $beans)
    {
    	$sl_bean = BeanFactory::newBean('SmartList');
        $smartlist = $sl_bean->get_smartlist_by_prospect_list_id($target_list->id);

        if(empty($smartlist))
    	{
    		//no smartlist exist for this target list, unable to process
    		return false;
    	}
        SmartList_Logger::log('debug','SmartList_Helper::process_beans_for_list found smartlist-'. print_r($smartlist->id, true));

    	$chunk = array();
    	foreach($beans as $bean)
    	{
    		if(empty($chunk[$bean->module_dir]))
    		{
    			$chunk[$bean->module_dir] = array();
    		}
			SmartList_Logger::log('debug',"SmartList_Helper::process_beans_for_list add to chunk: {$bean->module_dir}-{$bean->id}");
    		$chunk[$bean->module_dir][$bean->id] = $bean->id;
    	}
    	$action_done=false;
    	foreach($chunk as $module => $people)
    	{
    		$single_action = SmartList_Queue::process_module_for_smartlist($smartlist,$module,$people);
			SmartList_Logger::log('debug',"SmartList_Helper::process_beans_for_list process {$module} action: {$single_action}");

    		$action_done = $action_done || $single_action;
    	}

    	return $action_done;

    }
    public static function send_mismatch_plan_email($error_data)
    {
        global $sugar_config, $sugar_version, $sugar_flavor;

        $error_email = SugarChimp_Helper::get_error_email_address();

        $email_to = $error_email;
        $subject =  'SugarChimp Has Stopped Syncing';
        $bodyHTML= "You recently tried upgraded features of SugarChimp, however your trial for these features have ended. The upgraded features will no longer work, however you still have them selected in your settings. You must go to Admin > SugarChimp Configuration > Step 4 and remove the select settings that are no longer supported by your SugarChimp Edition to continue your syncing.

If you have any questions about this, please email SugarChimp Support and we will help you out: support@sugarchimp.com

Site: ".$sugar_config['site_url']."
SugarCRM Version: ".$sugar_version."
SugarCRM Edition: ".$sugar_flavor."
SugarChimp Version: ".SugarChimp_Helper::get_sugarchimp_version();

        $license_key = SugarChimp_License::get_license_key();
        if (!empty($license_key))
        {
            $bodyHTML .= "\n\nSugarChimp License: ".$license_key;
        }
        else
        {
            $bodyHTML .= "\n\nSugarChimp License: unknown - could not retrieve";
        }

		SugarChimp_Helper::send_email($email_to,$subject,$bodyHTML,false,'help+error-plan-mismatch@sugarchimp.com');

    }
}