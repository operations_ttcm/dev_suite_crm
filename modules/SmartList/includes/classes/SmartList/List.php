<?php






require_once('modules/SmartList/includes/classes/SmartList/Loader.php');
SmartList_Loader::load('Core/fList');

use Fanatical\Core\v1a\fList as fList;

require_once('modules/SmartList/includes/classes/SmartList/Logger.php');
require_once('modules/SmartList/includes/classes/SmartList/LogicHook.php');

class SmartList_List extends fList
{
    // parameters below follow the same signature as SugarBean->get_list(...)
    // order by - what field to order by
    // where - add a where clause to the sql query
    // row_offset - which record to start at
    // $limit - how many to query (defaults to max list count config variable)
    // max - max amount to query
    // show_deleted - also pull deleted target lists?
    // select_fields - fields to be selected during the query
    //
    // returns array(
    //     'row_count' => 5,
    //     'next_offset' => 20,
    //     'previous_offset' => -20,
    //     'current_offset' => 0,
    //     'list' => array(
    //         'asdf-asdf-asdf-asdf-asdf' => array(
    //             'id' => 'asdf-asdf-asdf-asdf-asdf',
    //             'name' => 'name of the list',
    //         )
    //     )
    // )
    public static function get_smartlists($order_by='name',$where='',$row_offset=0,$limit=-1,$max=-1,$show_deleted=0,$single_select=false,$select_fields=array(),$prepare_response=true)
    {
        $smartlist_bean = BeanFactory::getBean('SmartList');
        $smartlist_bean->disable_row_level_security = true;
        
        $smartlists = $smartlist_bean->get_list($order_by,$where,$row_offset,$limit,$max,$show_deleted,$single_select,$select_fields); //Sugar will use list_max_entries_per_page if set to -1
        
        if ($prepare_response === false)
        {
        	return $smartlists;
        }

        $data = array();
        $data['row_count'] = $smartlists['row_count'];
        $data['next_offset'] = $smartlists['next_offset'];
        $data['previous_offset'] = $smartlists['previous_offset'];
        $data['current_offset'] = $smartlists['current_offset'];

        $data['list'] = array();
        if (!empty($smartlists) and is_array($smartlists))
        {
            foreach($smartlists['list'] as $key => $list) 
            {
                if (empty($list->id))
                {
                    SmartList_Logger::log('warning','SmartList_List::get_smartlists list->id is empty for key '.print_r($key,true));
                    continue;
                }

                $data['list'][$list->id] = static::prepare_smartlist_for_client($list);
            }
        }

        return $data;
    }

    // same parameters and return as get_smartlists
    // returns only those smartlists that are active
    public static function get_active_smartlists($order_by='name',$where='',$row_offset=0,$limit=-1,$max=-1,$show_deleted=0,$single_select=false,$select_fields=array())
    {
        // setup the filter to get only those sugar target lists that are synced to a mailchimp list
        $is_synced_where =  " (active=1) ";
        if (!empty($where) and is_string($where))
        {
            $where .= " AND " . $is_synced_where;
        }
        else
        {
            $where = $is_synced_where;
        }

        return static::get_smartlists($order_by,$where,$row_offset,$limit,$max,$show_deleted,$single_select,$select_fields);
    }

    public static function prepare_smartlist_for_client($list,$unpack_metadata = true)
    {
        if (empty($list))
        {
            return false;
        }

        $data = array(
            'id' => isset($list->id) ? $list->id : false,
            'prospect_list_id' => isset($list->prospect_list_id) ? $list->prospect_list_id : false,
            'active' => isset($list->active) ? $list->active : false,
            'metadata' => isset($list->metadata) ? $list->metadata : false,
        );

        return $data;
    }

    public static function add_to_list($target_list_id=false,$module=false,$ids=false)
    {
        return static::mass_list_action($target_list_id,$module,$ids,'add');
    }

    public static function remove_from_list($target_list_id=false,$module=false,$ids=false)
    {
        return static::mass_list_action($target_list_id,$module,$ids,'delete');
    }

    protected static function mass_list_action($target_list_id,$module,$ids,$action)
    {
        if(empty($target_list_id)) 
        {
            SmartList_Logger::log('warning','SmartList_List mass_list_action: target list id is empty');
            return false;
        }

        if(empty($module)) 
        {
            SmartList_Logger::log('warning','SmartList_List mass_list_action: module is empty');
            return false;
        }

        if(empty($ids) || !is_array($ids)) 
        {
            SmartList_Logger::log('warning','SmartList_List mass_list_action: ids is empty or not an array');
            return false;
        }

        if(empty($action)) 
        {
            SmartList_Logger::log('warning','SmartList_List mass_list_action: action is empty or not an array');
            return false;
        }

        $bean = BeanFactory::newBean($module);
        $results = array();
        $relationship = '';

        foreach ($bean->get_linked_fields() as $field => $def) 
        {
            if ($bean->load_relationship($field)) 
            {
                if ($bean->$field->getRelatedModuleName() == 'ProspectLists') 
                {
                    $relationship = $field;
                    break;
                }
            }
        }

        if (empty($relationship)) 
        {
            SmartList_Logger::log('fatal','SmartList_List mass_list_action: could not find relationship with target list and module '.print_r($module,true));
            return false;
        }

        SmartList_Logger::log('debug','SmartList_List mass_list_action: '.print_r($action,true).' '.count($ids).' '.print_r($module,true).' to/from list '.print_r($target_list_id,true).':'.print_r($ids,true));


        foreach ($ids as $id) 
        {
            $bean = BeanFactory::getBean($module,$id);

            if (empty($bean)) 
            {
                SmartList_Logger::log('fatal','SmartList_List mass_list_action: could not find bean by module '.print_r($module,true).' and id '.print_r($id,true).' cannot be added to list '.print_r($target_list_id,true));
                continue;
            } 

            $bean->load_relationship($relationship);

            SmartList_LogicHook::disable_logic_hooks(array($bean));

            SmartList_Logger::log('debug','SmartList_List mass_list_action: action '.print_r($action,true).' '.print_r($module,true).' -'.print_r($id,true).' to/from list '.print_r($target_list_id,true));
            if ($action == 'add')
            {
                $bean->prospect_lists->add($target_list_id);
            }
            else if ($action == 'delete')
            {
                $bean->prospect_lists->delete($id,$target_list_id);
                // update 10/31/17
                // Issue where SmartList removs a record from Target List
                // but SugarChimp has an update job already
                // Both jobs are queued and processed
                // Depending on order, subscriber was re-subscribed to MailChimp incorrectly
                
                // use bean to remove 
                // use mc list as filter as well
                //  need mc list id, job name, bean module/id

                $prospect_list_bean = BeanFactory::getBean('ProspectLists', $target_list_id);
                
                if (empty($prospect_list_bean)) 
                {
                    SmartList_Logger::log('fatal','SmartList_List mass_list_action: could not find prospect list bean with id '.print_r($target_list_id,true).'. Unable to clear Update jobs from SugarChimp queue.');
                    SmartList_LogicHook::enable_logic_hooks(array($bean));
                    continue;
                }

                $mc_list_id = $prospect_list_bean->mailchimp_list_name_c;
                
                if(empty($mc_list_id))
                {
                    SmartList_Logger::log('fatal','SmartList_List mass_list_action: unable to retrieve mailchimp_list_name_c from Prospect List with id: '.print_r($target_list_id,true).'. Cannot clear Update jobs from SugarChimp queue.');
                    SmartList_LogicHook::enable_logic_hooks(array($bean));
                    continue;
                }
                
                global $db;

                $sql = "DELETE FROM sugarchimp WHERE 
                    (name = 'UpdateMailChimpSubscriber' OR 
                        name = 'UpdateMailChimpSubscriberGroup') AND 
                    param1 = '".$db->quote($bean->module_dir)."' AND 
                    param2 = '".$db->quote($bean->id)."' AND 
                    mailchimp_list_id= '".$db->quote($mc_list_id) ."'";                if (!$result = $db->query($sql))
                {
                    SmartList_Logger::log('fatal','SmartList_Queue delete_chunk: Query failed: '.$sql);
                    continue;
                }

            }

            SmartList_LogicHook::enable_logic_hooks(array($bean));

            // Sugar 6 BeanFactory class does not have a clearCache method
            // memory usage can get high here, so found a way to clear out the
            // cache in Sugar 6 by extending BeanFactory
            // this should probably be in the generic Fanatical library 
            global $sugar_version;
            if(preg_match( "/^6.*/", $sugar_version))
            {
                // if sugar 6, use the workaround
                SugarChimpBeanFactory::clearCache();
            }
            else
            {
                // if sugar 7, use beanfactory method
                BeanFactory::clearCache();
            }
        }

        return true;
    }
}

class SugarChimpBeanFactory extends BeanFactory
{
    public static function clearCache()
    {
        self::$loadedBeans = array();
        self::$total = 0;
        self::$hits = 0;
    }
}