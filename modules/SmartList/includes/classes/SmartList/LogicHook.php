<?php






require_once('modules/SmartList/includes/classes/SmartList/Queue.php');
require_once('modules/SmartList/includes/classes/SmartList/Logger.php');

class SmartList_LogicHook
{
    public static $beans_with_logic_hooks_disabled = array();

/**
new thinking is, only need to check parent related changes in after_save hook
after_relationship_add and after_relatinsihp_delte only queue supported_modules
this for sure applies to smartlist, probably applies for field mapping
*/

    function PersonUpdateAfterSave($bean, $event, $arguments) 
    {
        if (static::logic_hooks_enabled($bean) === false) return;
        SmartList_Queue::process_record_change($bean);
        SmartList_Queue::process_related_record_change($bean);
    }

    function PersonAfterRelationshipAdd($bean, $event, $arguments)
    {
        if (static::logic_hooks_enabled($bean) === false) return;
        SmartList_Queue::process_record_change($bean);
    }

    function PersonAfterRelationshipDelete($bean, $event, $arguments)
    {
        if (static::logic_hooks_enabled($bean) === false) return;
        SmartList_Queue::process_record_change($bean);
    }

    function SmartListAfterRetrieve($bean, $event, $arguments)
    {
        // SmartList_Logger::log('debug','LogicHook SmartList After Retrieve');

        // unpack the smartlist metadata on retrieval
        $bean->metadata = $bean->unpack_metadata($bean->metadata);
    }

    public static function disable_logic_hooks($beans)
    {
        SmartList_Logger::log('debug','SmartList_LogicHook::disable_logic_hook begin');
        if (is_array($beans))
        {
            foreach ($beans as $bean)
            {
                if ($bean instanceOf SugarBean)
                {
                    SmartList_Logger::log('debug',"SmartList_LogicHook::disable_logic_hook for {$bean->module_dir} {$bean->id}");
                    static::$beans_with_logic_hooks_disabled [$bean->id]= $bean->id;
                }
            }
        }
        SmartList_Logger::log('debug','SmartList_LogicHook::disable_logic_hook end');
    }
    
    public static function enable_logic_hooks($beans)
    {
        SmartList_Logger::log('debug','SmartList_LogicHook::enable_logic_hook begin');
        if (is_array($beans))
        {
            foreach ($beans as $bean)
            {
                if ($bean instanceOf SugarBean)
                {
                    SmartList_Logger::log('debug',"SmartList_LogicHook::enable_logic_hook for {$bean->module_dir} {$bean->id}");
                    unset(static::$beans_with_logic_hooks_disabled[$bean->id]);
                }
            }
        }
        SmartList_Logger::log('debug','SmartList_LogicHook::enable_logic_hook end');
    }
    
    public static function logic_hooks_enabled($bean)
    {
        return !in_array($bean->id,static::$beans_with_logic_hooks_disabled);
    }
}