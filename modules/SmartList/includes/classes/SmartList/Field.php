<?php






require_once('modules/SugarChimp/includes/classes/SugarChimp/FieldMap.php');
require_once('modules/SmartList/includes/classes/SmartList/Logger.php');
require_once('modules/SmartList/includes/classes/SmartList/Field/Phone.php');
require_once('modules/SmartList/includes/classes/SmartList/Field/Name.php');
require_once('modules/SmartList/includes/classes/SmartList/Field/Id.php');
require_once('modules/SmartList/includes/classes/SmartList/Field/Text.php');
require_once('modules/SmartList/includes/classes/SmartList/Field/Email.php');
require_once('modules/SmartList/includes/classes/SmartList/Field/Number.php');
require_once('modules/SmartList/includes/classes/SmartList/Field/Bool.php');
require_once('modules/SmartList/includes/classes/SmartList/Field/Date.php');
require_once('modules/SmartList/includes/classes/SmartList/Field/Datetime.php');
require_once('modules/SmartList/includes/classes/SmartList/Field/Mailchimprating.php');
require_once('modules/SmartList/includes/classes/SmartList/Field/Varchar.php');
require_once('modules/SmartList/includes/classes/SmartList/Field/Basic.php');
require_once('modules/SmartList/includes/classes/SmartList/Field/Enum.php');
require_once('modules/SmartList/includes/classes/SmartList/Field/Multienum.php');
require_once('modules/SmartList/includes/classes/SmartList/Field/Decimal.php');

require_once('modules/SmartList/includes/classes/SmartList/Loader.php');
require_once('modules/SmartList/includes/classes/SmartList/Logger.php');

SmartList_Loader::load('Core/Field');

use Fanatical\Core\v1a\Field as Field;

class SmartList_Field extends Field
{
    //as this grows, you must also add to the includes
    public static $supported_fields = array(
        'basic',
        'bool',
        'date',
        'datetime',
        'decimal',
        'email',
        'enum',
        'id',
        'int',
        'mailchimprating',
        'multienum',
        'name',
        'number',
        'phone',
        'text',
        'varchar',
    );

    public static function forge($driver)
    {
        if (empty($driver))
        {
            return false;
        }

        $driver = "SmartList_Field_" . ucfirst($driver);
        if(!class_exists($driver))
        {
            SmartList_Logger::log('warning','SmartList_Field::field not found: ' . $driver);
            $driver = 'SmartList_Field_Varchar';
        }
        return $driver::forge();
    }

    // not implementing datetime and date to start with due to time
    // whenever we remove the type exclude filter to include date/datetime
    // we can remove this method completely
    // there is a get_regular_database_fields method in Core Field
    public static function get_regular_database_fields($module,$include_fields=array(),$exclude_fields=array())
    {
        if (empty($module))
        {
            SmartList_Logger::log('warning','SmartList_Field::get_regular_database_fields module is empty');
            return false;
        }

        $fields = self::get_module_fields($module,
            array(
                'source' => array(''),
            ),
            array(
                'dbType' => array('id'),
                // not implementing datetime and date to start with due to time
                // whenever we remove the type filter, we can remove this method completely
                // there is a get_regular_database_fields method in Core Field
                'type' => array('datetime','date'),
            )
        );

        if (empty($fields))
        {
            Logger::log('warning','SmartList_Field::get_regular_database_fields there were no module fields for '.$module);
            return array();
        }

        return $fields;
    }

    public static function get_fields_info($fields=array())
    {
        $fields = (empty($fields) || !is_array($fields)) ? self::$supported_fields : $fields;

        $data = array();
        foreach($fields as $field)
        {
            $data[$field] = self::forge($field)->prepare_for_client($field);
        }

        return $data;
    }
}