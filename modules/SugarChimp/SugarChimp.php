<?php







require_once('modules/SugarChimp/SugarChimp_sugar.php');

class SugarChimp extends SugarChimp_sugar {

    public static function getMailChimpLists()
    {
        static $lists = null;
        if (!$lists)
        {
            require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
            $mclists = SugarChimp_Helper::get_mailchimp_lists(true);
            $lists = array(''=>'');
            foreach($mclists as $key=>$val) 
            {
                $lists[$key] = $val;
            }
        }

        return $lists;
    }

    public static function getSugarChimpMCLists()
    {
        //changed to only get Lists from SugarChimpMCList table 1/30/17
        static $lists = null;
        if (!$lists)
        {
            require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
            $mclists = SugarChimp_Helper::get_sugarchimpmclists();
            $lists = array(''=>'');
            foreach($mclists as $key=>$val) 
            {
                $lists[$key] = $val;
            }
        }

        return $lists;
    }

    public static function getSugarChimpMCCampaigns()
    {
        //changed to only get Campaigns from SugarChimpMCCampaign table 1/30/17
        static $campaigns = null;
        if (!$campaigns)
        {
            require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
            $mccampaigns = SugarChimp_Helper::get_sugarchimpmccampaigns();
            $campaigns = array(''=>'');
            foreach($mccampaigns as $key=>$val) 
            {
                $campaigns[$key] = $val;
            }
        }

        return $campaigns;
    }

    public static function getMailChimpCampaigns()
    {
        static $campaigns = null;
        if (!$campaigns)
        {
            require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
            $mccampaigns = SugarChimp_Helper::get_mailchimp_campaigns(true);
            $campaigns = array(''=>'');
            foreach($mccampaigns as $key=>$val) 
            {
                $campaigns[$key] = $val;
            }
        }

        return $campaigns;
    }

    public static function getMailChimpDefaultModules()
    {
        static $defaultModules = null;
        if (!$defaultModules){
            require_once('modules/SugarChimp/includes/classes/SugarChimp/FieldMap.php');
            global $app_list_strings;

            $defaultModules = array(''=>'');
            $supported_modules = SugarChimp_FieldMap::get_supported_modules();
            foreach ($supported_modules as $supported_mod)
            {
                $defaultModules[$supported_mod] = $app_list_strings['moduleList'][$supported_mod];
            }
        }

        return $defaultModules;
    }
}
