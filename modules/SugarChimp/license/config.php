<?php





$outfitters_config = array(
    'name' => 'SugarChimp',
    'shortname' => 'SugarChimp', //The short name of the Add-on. e.g. For the url https://store.suitecrm.com/addons/sugaroutfitters the shortname would be sugaroutfitters
    'api_url' => 'https://store.suitecrm.com/api/v2',
    'validate_users' => false,
    'validation_frequency' => 'daily', //default: weekly options: hourly, daily, weekly
    'continue_url' => '#SugarChimp/layout/setup', //[optional] Will show a button after license validation that will redirect to this page. Could be used to redirect to a configuration page such as index.php?module=MyCustomModule&action=config

    // includes all public keys in this group
    'public_key' => 'e1e8fe2db6c591ca253a0f744c3bc32e,52170ea5741fe7cad206d467bf0fd2f0,add168b9679fa779ab7669b7b7498c7d,1aab37a6285036741ef9a97669aebd31,98aecf4e397ab1444ca1f386051241f3,59df68ef9dd43fe421768643927baa92,386258a93a5fe9f5ca74f575c74cd308,5746fafb12e15a6e154e72e83ddbef04,344b80c5977fd489a0b56600705c426c,bdecfdb304cf25a55a29ad6f12e0559a,a21e351b78acd8099fe0c3a79af7fadf',
    'plans' => array(
        '1e3f5d842e7af817ce58f7f41d02fdd5' => 'starter',
        '083de98c13112a8b5aefb6e7accb08fc' => 'starter',
        '1aab37a6285036741ef9a97669aebd31' => 'basic',
        '98aecf4e397ab1444ca1f386051241f3' => 'basic',
        '59df68ef9dd43fe421768643927baa92' => 'basic',
        '386258a93a5fe9f5ca74f575c74cd308' => 'basic',
        'e1e8fe2db6c591ca253a0f744c3bc32e' => 'professional',
        '52170ea5741fe7cad206d467bf0fd2f0' => 'professional',
        'add168b9679fa779ab7669b7b7498c7d' => 'ultimate',
        '5746fafb12e15a6e154e72e83ddbef04' => 'ultimate',
        '344b80c5977fd489a0b56600705c426c' => 'ultimate',
        'bdecfdb304cf25a55a29ad6f12e0559a' => 'ultimate', //sugaroutfitters & suite store monthly priority
        'a21e351b78acd8099fe0c3a79af7fadf' => 'ultimate', //sugaroutfitters & suite store yearly priority
    ),
);



global $sugar_version;

if(preg_match( "/^6.*/", $sugar_version) ) {
    $outfitters_config['continue_url'] = 'index.php?module=SugarChimp&action=setup';
}
