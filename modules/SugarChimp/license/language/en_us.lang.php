<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');







$license_strings = array (
    'LBL_STEPS_TO_LOCATE_KEY_TITLE' => 'To Locate Your Key',
    'LBL_STEPS_TO_LOCATE_KEY' => '1. Login to <a href="https://store.suitecrm.com" target="_blank">SugarOutfitters or store.SuiteCRM.com</a><br/>2. From the "My Account" menu, go to <a href="https://store.suitecrm.com/orders" target="_blank">Purchases</a></br>3. Locate the key for the purchase of this add-on<br/>4. Paste into the License Key box below<br/>5. Hit "Validate"',
    'LBL_LICENSE_KEY' => 'License Key',
    'LBL_CURRENT_USERS' => 'Current User Count',
    'LBL_LICENSED_USERS' => 'Licensed User Count',
    'LBL_VALIDATE_LABEL' => 'Validate',
    'LBL_VALIDATED_LABEL' => 'Validated',
);

