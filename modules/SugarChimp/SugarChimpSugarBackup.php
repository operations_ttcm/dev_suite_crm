<?php







class SugarChimpSugarBackup extends Basic 
{
    var $new_schema = true;
    var $module_dir = 'SugarChimp';
    var $object_name = 'SugarChimpSugarBackup';
    var $table_name = 'sugarchimp_sugar_backup';

    var $id;
    var $record;
    var $module;
    var $field;
    var $value;
    var $date_entered;
    
    function bean_implements($interface){
        switch($interface){
            case 'ACL': return true;
        }
        return false;
    }
}
