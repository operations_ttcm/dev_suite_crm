<?php







class SugarChimpLock extends Basic 
{
    var $new_schema = true;
    var $module_dir = 'SugarChimp';
    var $object_name = 'SugarChimpLock';
    var $table_name = 'sugarchimp_lock';

    var $id;
    var $email;
    
    function bean_implements($interface){
        switch($interface){
            case 'ACL': return true;
        }
        return false;
    }
}
