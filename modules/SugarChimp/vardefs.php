<?php





require_once('include/SugarObjects/VardefManager.php');

$dictionary['SugarChimp'] = array(
    'table'=>'sugarchimp',
    'audited'=>false,
    'fields'=>array (
        'mailchimp_list_id' => array(
            'required' => false,
            'name' => 'mailchimp_list_id',
            'vname' => 'LBL_MAILCHIMP_LIST_ID',
            'type' => 'enum',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => false,
            'reportable' => true,
            'len' => 255,
            'size' => '20',
            'dbType' => 'varchar',
            'studio' => 'hidden',
        ),
        'param1' => array(
            'required' => false,
            'name' => 'param1',
            'vname' => 'LBL_PARAM1',
            'type' => 'enum',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => false,
            'reportable' => true,
            'len' => 255,
            'size' => '20',
            'dbType' => 'varchar',
            'studio' => 'hidden',
        ),
        'param2' => array(
            'required' => false,
            'name' => 'param2',
            'vname' => 'LBL_PARAM2',
            'type' => 'enum',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => false,
            'reportable' => true,
            'len' => 255,
            'size' => '20',
            'dbType' => 'varchar',
            'studio' => 'hidden',
        ),
        'param3' => array(
            'required' => false,
            'name' => 'param3',
            'vname' => 'LBL_PARAM3',
            'type' => 'enum',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => false,
            'reportable' => true,
            'len' => 255,
            'size' => '20',
            'dbType' => 'varchar',
            'studio' => 'hidden',
        ),
        'param4' => array(
            'required' => false,
            'name' => 'param4',
            'vname' => 'LBL_PARAM4',
            'type' => 'enum',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => false,
            'reportable' => true,
            'len' => 255,
            'size' => '20',
            'dbType' => 'varchar',
            'studio' => 'hidden',
        ),
        'param5' => array(
            'required' => false,
            'name' => 'param5',
            'vname' => 'LBL_PARAM5',
            'type' => 'enum',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => false,
            'reportable' => true,
            'len' => 255,
            'size' => '20',
            'dbType' => 'varchar',
            'studio' => 'hidden',
        ),
    ),
    'relationships'=>array (
    ),
    'optimistic_lock'=>true,
);

VardefManager::createVardef('SugarChimp','SugarChimp', array('basic'));



$dictionary['SugarChimpMC'] = array (
    'table' => 'sugarchimp_mc',
    'fields' => array (
        'id' => array(
            'name' =>'id',
            'type' =>'id'
        ),
        'mailchimp_list_id' => array(
            'required' => false,
            'name' => 'mailchimp_list_id',
            'vname' => 'LBL_MAILCHIMP_LIST_ID',
            'type' => 'enum',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => false,
            'reportable' => true,
            'len' => 255,
            'size' => '20',
            'dbType' => 'varchar',
            'studio' => 'hidden',
        ),
        'data' => array (
            'name' => 'data',
            'vname' => 'LBL_DATA',
            'type' => 'longtext',
            'required' => false,
            'reportable' => true,
        ),
        'date_entered' => array (
            'name' => 'date_entered',
            'type' => 'datetime'
        ),
        'date_modified' => array (
            'name' => 'date_modified',
            'type' => 'datetime'
        ),
        'deleted' => array(
            'name' => 'deleted',
            'type' => 'bool',
            'len'  => '1',
            'default'   => '0',
            'required'  => false
        ),
    ),
);

VardefManager::createVardef('SugarChimpMC','SugarChimpMC', array('basic'));

// table that holds the backup queue
$dictionary['SugarChimpBackupQueue'] = array (
    'table' => 'sugarchimp_backup_queue',
    'fields' => array (
        'id' => array(
            'name' =>'id',
            'type' =>'id'
        ),
        'name' => array(
            'name'=>'name',
            'vname'=> 'LBL_NAME',
            'type'=>'name',
            'link' => true,
            'dbType' => 'varchar',
            'len'=>255,
        ),
        'mailchimp_list_id' => array(
            'required' => false,
            'name' => 'mailchimp_list_id',
            'vname' => 'LBL_MAILCHIMP_LIST_ID',
            'type' => 'enum',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => false,
            'reportable' => true,
            'len' => 255,
            'size' => '20',
            'dbType' => 'varchar',
            'studio' => 'hidden',
        ),
        'data' => array (
            'name' => 'data',
            'vname' => 'LBL_DATA',
            'type' => 'longtext',
            'required' => false,
            'reportable' => true,
        ),
        'date_entered' => array (
            'name' => 'date_entered',
            'type' => 'datetime'
        ),
        'date_modified' => array (
            'name' => 'date_modified',
            'type' => 'datetime'
        ),
        'deleted' => array(
            'name' => 'deleted',
            'type' => 'bool',
            'len'  => 1,
            'default'   => '0',
            'required'  => false
        ),
    ),
);

VardefManager::createVardef('SugarChimpBackupQueue','SugarChimpBackupQueue', array('basic'));

// this is the table the stores the MC backups
// it looks just like the sugarchimp_mc table, just is never processed
$dictionary['SugarChimpMCBackup'] = array (
    'table' => 'sugarchimp_mailchimp_backup',
    'fields' => array (
        'id' => array(
            'name' =>'id',
            'type' =>'id'
        ),
        'mailchimp_list_id' => array(
            'required' => false,
            'name' => 'mailchimp_list_id',
            'vname' => 'LBL_MAILCHIMP_LIST_ID',
            'type' => 'enum',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => false,
            'reportable' => true,
            'len' => 255,
            'size' => '20',
            'dbType' => 'varchar',
            'studio' => 'hidden',
        ),
        'data' => array (
            'name' => 'data',
            'vname' => 'LBL_DATA',
            'type' => 'longtext',
            'required' => false,
            'reportable' => true,
        ),
        'date_entered' => array (
            'name' => 'date_entered',
            'type' => 'datetime'
        ),
        'date_modified' => array (
            'name' => 'date_modified',
            'type' => 'datetime'
        ),
        'deleted' => array(
            'name' => 'deleted',
            'type' => 'bool',
            'len'  => '1',
            'default'   => '0',
            'required'  => false
        ),
    ),
);

VardefManager::createVardef('SugarChimpMCBackup','SugarChimpMCBackup', array('basic'));

$dictionary['SugarChimpSugarBackup'] = array (
    'table' => 'sugarchimp_sugar_backup',
    'fields' => array (
        'id' => array(
            'name' =>'id',
            'type' =>'id'
        ),
        'record' => array(
            'name' =>'record',
            'type' =>'id'
        ),
        'module' => array(
            'name'=>'module',
            'type'=>'varchar',
            'dbType' => 'varchar',
            'len'=>255,
        ),
        'data' => array (
            'name' => 'data',
            'vname' => 'LBL_DATA',
            'type' => 'longtext',
            'required' => false,
            'reportable' => true,
        ),
        'date_entered' => array (
            'name' => 'date_entered',
            'type' => 'datetime'
        ),
    ),
);

VardefManager::createVardef('SugarChimpSugarBackup','SugarChimpSugarBackup', array('basic'));

$dictionary['SugarChimpMCPeople'] = array (
    'table' => 'sugarchimp_mc_people',
    'fields' => array (
        'id' => array(
            'name' =>'id',
            'type' =>'id'
        ),
        'mailchimp_list_id' => array(
            'required' => false,
            'name' => 'mailchimp_list_id',
            'vname' => 'LBL_MAILCHIMP_LIST_ID',
            'type' => 'enum',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => false,
            'reportable' => true,
            'len' => 255,
            'size' => '20',
            'dbType' => 'varchar',
            'studio' => 'hidden',
        ),
        'data' => array (
            'name' => 'data',
            'vname' => 'LBL_DATA',
            'type' => 'longtext',
            'required' => false,
            'reportable' => true,
        ),
        'date_entered' => array (
            'name' => 'date_entered',
            'type' => 'datetime'
        ),
        'date_modified' => array (
            'name' => 'date_modified',
            'type' => 'datetime'
        ),
        'deleted' => array(
            'name' => 'deleted',
            'type' => 'bool',
            'len'  => '1',
            'default'   => '0',
            'required'  => false
        ),
    ),
);

VardefManager::createVardef('SugarChimpMCPeople','SugarChimpMCPeople', array('basic'));

$dictionary['SugarChimpLock'] = array (
    'table' => 'sugarchimp_lock',
    'fields' => array (
        'id' => array(
            'name' =>'id',
            'type' =>'id'
        ),
        'email' => array(
            'required' => false,
            'name' => 'email',
            'type' => 'enum',
            'len' => 255,
            'size' => '20',
        ),
        'date_entered' => array (
            'name' => 'date_entered',
            'type' => 'datetime'
        ),
    ),
);

VardefManager::createVardef('SugarChimpLock','SugarChimpLock', array('basic'));

$dictionary['SugarChimpMCCleanedEmail'] = array (
    'table' => 'sugarchimp_mc_cleaned_email',
    'fields' => array (
        'mailchimp_list_id' => array(
            'required' => false,
            'name' => 'mailchimp_list_id',
            'vname' => 'LBL_MAILCHIMP_LIST_ID',
            'type' => 'enum',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => false,
            'reportable' => true,
            'len' => 255,
            'size' => '20',
            'dbType' => 'varchar',
            'studio' => 'hidden',
        ),
        'email' => array(
            'name'=>'email',
            'vname'=> 'LBL_EMAIL',
            'type'=>'varchar',
            'link' => true,
            'dbType' => 'varchar',
            'len'=>255,
        ),
    ),
);

VardefManager::createVardef('SugarChimpMCCleanedEmail','SugarChimpMCCleanedEmail', array('basic'));




$dictionary['SugarChimpMCActivity'] = array (
    'table' => 'sugarchimp_mc_activity',
    'fields' => array (
        'id' => array(
            'name' =>'id',
            'type' =>'id'
        ),
        'mailchimp_campaign_id' => array(
            'required' => false,
            'name' => 'mailchimp_campaign_id',
            'vname' => 'LBL_MAILCHIMP_CAMPAIGN_ID',
            'type' => 'enum',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => false,
            'reportable' => true,
            'len' => 255,
            'size' => '20',
            'dbType' => 'varchar',
            'studio' => 'hidden',
        ),
        'mailchimp_list_id' => array(
            'required' => false,
            'name' => 'mailchimp_list_id',
            'vname' => 'LBL_MAILCHIMP_LIST_ID',
            'type' => 'enum',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => false,
            'reportable' => true,
            'len' => 255,
            'size' => '20',
            'dbType' => 'varchar',
            'studio' => 'hidden',
        ),
        'include_empty' => array(
            'name' => 'include_empty',
            'type' => 'bool',
            'len'  => '1',
            'default'   => '0',
            'required'  => false
        ),
        'data' => array (
            'name' => 'data',
            'vname' => 'LBL_DATA',
            'type' => 'longtext',
            'required' => false,
            'reportable' => true,
        ),
        'date_entered' => array (
            'name' => 'date_entered',
            'type' => 'datetime'
        ),
        'date_modified' => array (
            'name' => 'date_modified',
            'type' => 'datetime'
        ),
        'deleted' => array(
            'name' => 'deleted',
            'type' => 'bool',
            'len'  => '1',
            'default'   => '0',
            'required'  => false
        ),
    ),
    'indices' => array(
        array(
            'name' =>'sugarchimp_mc_activity_next_mc_list_id', 
            'type' =>'index', 
            'fields'=>array('mailchimp_list_id','date_entered')
        ),
    )
);

VardefManager::createVardef('SugarChimpMCActivity','SugarChimpMCActivity', array('basic'));


// table that holds the backup queue
$dictionary['SugarChimpOptoutTracker'] = array (
    'table' => 'sugarchimp_optout_tracker',
    'fields' => array (
        'id' => array(
            'name' =>'id',
            'type' =>'id'
        ),
        'name' => array(
            'name'=>'name',
            'vname'=> 'LBL_NAME',
            'type'=>'name',
            'link' => true,
            'dbType' => 'varchar',
            'len'=>255,
        ),
        'email' => array(
            'name'=>'email',
            'vname'=> 'LBL_EMAIL',
            'type'=>'varchar',
            'link' => true,
            'dbType' => 'varchar',
            'len'=>255,
        ),
        'mailchimp_list_id' => array(
            'required' => false,
            'name' => 'mailchimp_list_id',
            'vname' => 'LBL_MAILCHIMP_LIST_ID',
            'type' => 'enum',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => false,
            'reportable' => true,
            'len' => 255,
            'size' => '20',
            'dbType' => 'varchar',
            'studio' => 'hidden',
        ),
        'date_entered' => array (
            'name' => 'date_entered',
            'type' => 'datetime'
        ),
        'date_modified' => array (
            'name' => 'date_modified',
            'type' => 'datetime'
        ),
        'deleted' => array(
            'name' => 'deleted',
            'type' => 'bool',
            'len'  => '1',
            'default'   => '0',
            'required'  => false
        ),
    ),
    // 'indices' => array(
    //     array(
    //         'name' =>'sugarchimp_optout_tracker_lookup', 
    //         'type' =>'key', 
    //         'fields'=>array('email','mailchimp_list_id'),
    //     ),
    // )
);

VardefManager::createVardef('SugarChimpOptoutTracker','SugarChimpOptoutTracker', array('basic'));


// table that holds the all MailChimp Lists
$dictionary['SugarChimpMCList'] = array (
    'table' => 'sugarchimp_mc_list',
    'fields' => array (
        'id' => array(
            'name' =>'id',
            'type' =>'id'
        ),
        'name' => array(
            'name'=>'name',
            'vname'=> 'LBL_MAILCHIMP_LIST_NAME',
            'type'=>'name',
            'link' => true,
            'dbType' => 'varchar',
            'len'=>255,
        ),
        'mailchimp_list_id' => array(
            'required' => false,
            'name' => 'mailchimp_list_id',
            'vname' => 'LBL_MAILCHIMP_LIST_ID',
            'type' => 'varchar',
            'audited' => false,
            'reportable' => true,
            'len' => 255,
        ),
        'deleted_from_mailchimp' => array(
            'name' => 'deleted_from_mailchimp',
            'vname' => 'LBL_DELETED_FROM_MAILCHIMP',
            'type' => 'bool',
            'len'  => '1',
            'default'   => '0',
            'required'  => false
        ),
        'optout_tracker_processed' => array(
            'name' => 'optout_tracker_processed',
            'vname' => 'LBL_OPTOUT_TRACKER_PROCESSED',
            'type' => 'bool',
            'default' => '0',
            'reportable' => false,
        ),
        'optout_tracker_offset' => array(
            'name' => 'optout_tracker_offset',
            'vname' => 'LBL_OPTOUT_TRACKER_OFFSET',
            'type' => 'int',
            'default' => '0',
            'reportable' => false,
        ),
        'date_entered' => array (
            'name' => 'date_entered',
            'type' => 'datetime'
        ),
        'date_modified' => array (
            'name' => 'date_modified',
            'type' => 'datetime'
        ),
        'deleted' => array(
            'name' => 'deleted',
            'type' => 'bool',
            'len'  => '1',
            'default'   => '0',
            'required'  => false
        ),
        'last_cleaned_email_check' => array(
            'name' => 'last_cleaned_email_check',
            'vname' => 'LBL_LAST_SYNC',
            'type' => 'datetime',
            'comment' => 'The datetime the cleaned emails for this list was checked.',
            'enable_range_search' => true,
            'options' => 'date_range_search_dom',
            'reportable' => false,
        ),
    ),
    'relationships'=> array (
    ),
);

VardefManager::createVardef('SugarChimpMCList','SugarChimpMCList', array('basic'));

// table that holds the all MailChimp Campaigns
$dictionary['SugarChimpMCCampaign'] = array (
    'table' => 'sugarchimp_mc_campaign',
    'fields' => array (
        'id' => array(
            'name' =>'id',
            'type' =>'id'
        ),
        'name' => array(
            'name'=>'name',
            'vname'=> 'LBL_NAME',
            'type'=>'name',
            'link' => true,
            'dbType' => 'varchar',
            'len'=>255,
        ),
        'subject' => array(
            'name'=>'subject',
            'vname'=> 'LBL_MAILCHIMP_CAMPAIGN_SUBJECT',
            'type'=>'varchar',
            'link' => true,
            'dbType' => 'varchar',
            'len'=>255,
        ),
        'mailchimp_campaign_id' => array(
            'required' => false,
            'name' => 'mailchimp_campaign_id',
            'vname' => 'LBL_MAILCHIMP_CAMPAIGN_ID',
            'type' => 'varchar',
            'audited' => false,
            'reportable' => true,
            'len' => 255,
        ),
        'mailchimp_list_id' => array(
            'required' => false,
            'name' => 'mailchimp_list_id',
            'vname' => 'LBL_MAILCHIMP_LIST',
            'type' => 'enum',
            'options' => 'mailchimp_lists_list',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => false,
            'reportable' => true,
            'len' => 255,
            'size' => '20',
            'dbType' => 'varchar',
        ),
        'type' => array(
            'required' => false,
            'name' => 'type',
            'vname' => 'LBL_MAILCHIMP_CAMPAIGN_TYPE',
            'type' => 'enum',
            'options' => 'mailchimp_campaign_type_list',
            'len' => 50,
            'duplicate_on_record_copy' => 'always',
            'audited' => false,
            'reportable' => true,
        ),
        'send_time' => array (
            'name' => 'send_time',
            'vname' => 'LBL_MAILCHIMP_CAMPAIGN_SEND_TIME',
            'type' => 'datetime',
            // 'enable_range_search' => true,
            // 'options' => 'date_range_search_dom',
        ),
        'date_entered' => array (
            'name' => 'date_entered',
            'type' => 'datetime',
        ),
        'date_modified' => array (
            'name' => 'date_modified',
            'type' => 'datetime',
        ),
        'deleted' => array(
            'name' => 'deleted',
            'type' => 'bool',
            'len'  => '1',
            'default'   => '0',
            'required'  => false
        ),
    ),
    'relationships'=> array (
    ),
);

VardefManager::createVardef('SugarChimpMCCampaign','SugarChimpMCCampaign', array('basic'));

// table that holds the backup queue
$dictionary['SugarChimpBatches'] = array (
    'table' => 'sugarchimp_batches',
    'fields' => array (
        'id' => array(
            'name' =>'id',
            'type' =>'id'
        ),
        'batch_id' => array(
            'name'=>'batch_id',
            'vname'=> 'LBL_BATCH_ID',
            'type'=>'enum',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => false,
            'reportable' => true,
            'len' => 255,
            'size' => '20',
            'dbType' => 'varchar',
            'studio' => 'hidden',
        ),
        'mailchimp_list_id' => array(
            'required' => false,
            'name' => 'mailchimp_list_id',
            'vname' => 'LBL_MAILCHIMP_LIST_ID',
            'type' => 'enum',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => false,
            'reportable' => true,
            'len' => 255,
            'size' => '20',
            'dbType' => 'varchar',
            'studio' => 'hidden',
        ),
        'date_entered' => array (
            'name' => 'date_entered',
            'type' => 'datetime'
        ),
        'date_modified' => array (
            'name' => 'date_modified',
            'type' => 'datetime'
        ),
        'deleted' => array(
            'name' => 'deleted',
            'type' => 'bool',
            'len'  => '1',
            'default'   => '0',
            'required'  => false
        ),
    ),
);

VardefManager::createVardef('SugarChimpBatches','SugarChimpBatches', array('basic'));
