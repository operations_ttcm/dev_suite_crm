<?php







class SugarChimpMCCampaign extends Basic 
{
    var $new_schema = true;
    var $module_dir = 'SugarChimp';
    var $object_name = 'SugarChimpMCCampaign';
    var $table_name = 'sugarchimp_mc_campaign';
    
    var $id;
    var $name;
    var $mailchimp_campaign_id;
    var $mailchimp_list_id;
    var $type;
    
    function bean_implements($interface){
        switch($interface){
            case 'ACL': return true;
        }
        return false;
    }
}