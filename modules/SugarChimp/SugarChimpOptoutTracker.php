<?php







class SugarChimpOptoutTracker extends Basic 
{
    var $new_schema = true;
    var $module_dir = 'SugarChimp';
    var $object_name = 'SugarChimpOptoutTracker';
    var $table_name = 'sugarchimp_optout_tracker';

    var $id;
    var $name; // not used
    var $email;
    var $mailchimp_list_id;
    var $date_entered;
    
    function bean_implements($interface){
        switch($interface){
            case 'ACL': return true;
        }
        return false;
    }
}