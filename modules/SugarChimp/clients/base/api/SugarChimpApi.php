<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');





global $sugar_version;

// only load the real SugarApi for sugar 6.7.*, 7, and above users
// otherwise, load a fake one so it can still be extended and not throw error in CE
if(preg_match( "/^6.[0-6]/", $sugar_version))
{
    require_once('modules/SugarChimp/includes/classes/CEHackSugarApi.php');
}
else
{
    require_once('include/api/SugarApi.php');
}

require_once('data/BeanFactory.php');

require_once('modules/SugarChimp/includes/vendor/Mailchimp/Mailchimp.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Campaign.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/List.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/FieldMap.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Field.php');

require_once('modules/SugarChimp/license/OutfittersLicense.php');
require_once('modules/SugarChimp/license/config.php');

require_once('modules/SmartList/clients/base/api/SmartListApi.php');

require_once('modules/SmartList/includes/classes/SmartList/Field.php');
require_once('modules/SmartList/includes/classes/SmartList/List.php');
require_once('modules/SmartList/includes/classes/SmartList/Filter.php');
require_once('modules/SmartList/includes/classes/SmartList/Mode.php');

class SugarChimpApi extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'test' => array(
                'reqType' => 'GET',
                'path' => array('SugarChimp','test','?','?','?','?','?'),
                'pathVars' => array('','','param1','param2','param3','param4','param5'),
                'method' => 'test',
                'shortHelp' => 'Test method to test stuff. Duh.',
            ),
            'test_post' => array(
                'reqType' => 'POST',
                'path' => array('SugarChimp','test_post'),
                'pathVars' => array('',''),
                'method' => 'test_post',
                'shortHelp' => 'Test POST method to test stuff. Duh.',
            ),
            'getApiKey' => array(
                'reqType' => 'GET',
                'path' => array('SugarChimp', 'apikey'),
                'pathVars' => array(''),
                'method' => 'getApiKey',
                'shortHelp' => 'Retrieves the MailChimp API key',
            ),
            'updateApiKey' => array(
                'reqType'   => 'POST',
                'path'      => array('SugarChimp','apikey','update'),
                'pathVars'  => array(''),
                'method'    => 'updateApiKey',
                'shortHelp' => 'This method saves the MailChimp API key.',
            ),            
            'getPHP' => array(
                'reqType' => 'GET',
                'path' => array('SugarChimp', 'php'),
                'pathVars' => array(''),
                'method' => 'getPHP',
                'shortHelp' => 'Retrieves the active PHP version',
            ),
            'isValidPHP' => array(
                'reqType' => 'GET',
                'path' => array('SugarChimp', 'apikey'),
                'pathVars' => array(''),
                'method' => 'isValidPHP',
                'shortHelp' => 'Returns rather getPHP is a valid PHP option',
            ),
            'getLicenseValidation' => array(
                'reqType' => 'GET',
                'path' => array('SugarChimp', 'license','get'),
                'pathVars' => array(''),
                'method' => 'getLicenseValidation',
                'shortHelp' => 'Returns rather the license is valid or not',
            ),
            'validateLicense' => array(
                'reqType'   => 'POST',
                'path'      => array('SugarChimp','license','validate'),
                'pathVars'  => array('','',''),
                'method'    => 'validateLicense',
                'shortHelp' => 'This method saves the SugarChimp License key.',
            ),
            'getSwitchablePlans' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','license','options'),
                'pathVars'  => array('','',''),
                'method'    => 'getSwitchablePlans',
                'shortHelp' => 'This method switchable plans for current SugarChimp license.',
            ),
            'switchPlan' => array(
                'reqType'   => 'POST',
                'path'      => array('SugarChimp','license','upgrade'),
                'pathVars'  => array('','',''),
                'method'    => 'switchPlan',
                'shortHelp' => 'This method switches plans for the SugarChimp License key.',
            ),
            'cancelUpgrade' => array(
                'reqType'   => 'POST',
                'path'      => array('SugarChimp','license','upgrade','cancel'),
                'pathVars'  => array('','','',''),
                'method'    => 'cancelUpgrade',
                'shortHelp' => 'This method cancels the upgrade trial for the SugarChimp License key.',
            ),
            'preChecksScheduler' => array(
                'reqType' => 'GET',
                'path' => array('SugarChimp', 'preChecks','scheduler'),
                'pathVars' => array(''),
                'method' => 'preChecksScheduler',
                'shortHelp' => 'Checks to see if the scheduler has been set up, and PHP is compatible.',
            ),
            'preChecksNetwork' => array(
                'reqType' => 'GET',
                'path' => array('SugarChimp', 'preChecks','network'),
                'pathVars' => array(''),
                'method' => 'preChecksNetwork',
                'shortHelp' => 'Checks to see if the network has been set up properly, and cURL is enabled.',
            ),
            'getMailChimpListsCount' => array(
                'reqType' => 'GET',
                'path' => array('SugarChimp', 'mailchimp','lists','count'),
                'pathVars' => array('','','',''),
                'method' => 'getMailChimpListsCount',
                'shortHelp' => 'Get basic count of how many MailChimp lists there are.',
            ),
            'getMailChimpLists' => array(
                'reqType' => 'GET',
                'path' => array('SugarChimp', 'mailchimp','lists'),
                'pathVars' => array('','',''),
                'method' => 'getMailChimpLists',
                'shortHelp' => 'Get all MailChimp lists that have not been synced.',
            ),
            // 'getAllMailChimpLists' => array(
            //     'reqType' => 'GET',
            //     'path' => array('SugarChimp', 'mailchimp','all_lists'),
            //     'pathVars' => array('','',''),
            //     'method' => 'getAllMailChimpLists',
            //     'shortHelp' => 'Get all MailChimp lists.',
            // ),
            'getSyncedMailChimpLists' => array(
                'reqType' => 'GET',
                'path' => array('SugarChimp', 'mailchimp','synced_lists'),
                'pathVars' => array('','',''),
                'method' => 'getSyncedMailChimpLists',
                'shortHelp' => 'Get all synced MailChimp lists.',
            ),
            'saveMailChimpLists' => array(
                'reqType'   => 'POST',
                'path'      => array('SugarChimp','mailchimp','lists','save'),
                'pathVars'  => array('','','',''),
                'method'    => 'saveMailChimpLists',
                'shortHelp' => 'Save MailChimp list preferences.',
            ),
            'getSugarCRMLists' => array(
                'reqType' => 'GET',
                'path' => array('SugarChimp', 'sugarcrm','lists'),
                'pathVars' => array('','',''),
                'method' => 'getSugarCRMLists',
                'shortHelp' => 'Get all SugarCRM lists.',
            ),
            'saveSugarCRMLists' => array(
                'reqType'   => 'POST',
                'path'      => array('SugarChimp','sugarcrm','lists','save'),
                'pathVars'  => array('','','',''),
                'method'    => 'saveSugarCRMLists',
                'shortHelp' => 'Save SugarCRM list preferences.',
            ),
            'getHealth' => array(
                'reqType' => 'GET',
                'path' => array('SugarChimp', 'health'),
                'pathVars' => array('',''),
                'method' => 'getHealth',
                'shortHelp' => 'Misc stats about current SugarChimp state.',
            ),
            'saveConfigLogger' => array(
                'reqType'   => 'POST',
                'path'      => array('SugarChimp','config','logger','save'),
                'pathVars'  => array('','','',''),
                'method'    => 'saveConfigLogger',
                'shortHelp' => 'Save SugarChimp logger preference.',
            ),
            'dashletPersonRecordData' => array(
                'reqType'   => 'POST',
                'path'      => array('SugarChimp','dashlet','person','record'),
                'pathVars'  => array('','','',''),
                'method'    => 'dashletPersonRecordData',
                'shortHelp' => 'Get data for SugarChimp Dashlet for Person (Contact, Lead, Target) Record view',
            ),
            'dashletAccountRecordData' => array(
                'reqType'   => 'POST',
                'path'      => array('SugarChimp','dashlet','account','record'),
                'pathVars'  => array('','','',''),
                'method'    => 'dashletAccountRecordData',
                'shortHelp' => 'Get data for SugarChimp Dashlet for Account Record view',
            ),
            'getMappingOptions' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','mapping','?'),
                'pathVars'  => array('','','list_id'),
                'method'    => 'getMappingOptions',
                'shortHelp' => 'Get data required to map MailChimp List Fields with SugarCRM fields',
            ),
            'saveMappingOptions' => array(
                'reqType'   => 'POST',
                'path'      => array('SugarChimp','mapping','save'),
                'pathVars'  => array('','',''),
                'method'    => 'saveMappingOptions',
                'shortHelp' => 'Save data required to map MailChimp List Fields with SugarCRM fields',
            ),
            'getAdminData' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','admin'),
                'pathVars'  => array('',''),
                'method'    => 'getAdminData',
                'shortHelp' => 'Simple getter for records in the sugarchimp table.',
            ),
            'getAdminBackupData' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','adminbackups'),
                'pathVars'  => array('',''),
                'method'    => 'getAdminBackupData',
                'shortHelp' => 'Simple getter for records in the sugarchimp_backup_queue table.',
            ),
            'getAdminSugarBackupData' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','adminsugarbackups'),
                'pathVars'  => array('',''),
                'method'    => 'getAdminSugarBackupData',
                'shortHelp' => 'Simple getter for records in the sugarchimp_sugar_backup table.',
            ),
            'getAdminMailchimpBackupData' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','adminmailchimpbackups'),
                'pathVars'  => array('',''),
                'method'    => 'getAdminMailchimpBackupData',
                'shortHelp' => 'Simple getter for records in the sugarchimp_mailchimp_backup table.',
            ),
            'getAdminmcData' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','adminmc'),
                'pathVars'  => array('',''),
                'method'    => 'getAdminmcData',
                'shortHelp' => 'Simple getter for records in the sugarchimp_mc table.',
            ),
            'getAdminactivitytrackerData' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','adminactivitytracker'),
                'pathVars'  => array('',''),
                'method'    => 'getAdminactivitytrackerData',
                'shortHelp' => 'Simple getter for records in the sugarchimp_activity_tracker table.',
            ),
            'getAdminactivitiesmcData' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','adminactivitiesmc'),
                'pathVars'  => array('',''),
                'method'    => 'getAdminactivitiesmcData',
                'shortHelp' => 'Simple getter for records in the sugarchimp_mc_activity table.',
            ),
            'getAdminmcpeopleData' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','adminmcpeople'),
                'pathVars'  => array('',''),
                'method'    => 'getAdminmcpeopleData',
                'shortHelp' => 'Simple getter for records in the sugarchimp_mc_people table.',
            ),
            'getAdminmclistData' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','adminmclist'),
                'pathVars'  => array('',''),
                'method'    => 'getAdminmclistData',
                'shortHelp' => 'Simple getter for records in the sugarchimp_mc_list table.',
            ),
            'getAdminmccleanedemailData' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','adminmccleanedemail'),
                'pathVars'  => array('',''),
                'method'    => 'getAdminmccleanedemailData',
                'shortHelp' => 'Simple getter for records in the sugarchimp_mc_cleaned_email table.',
            ),
            'getAdminactivitiesData' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','adminactivities'),
                'pathVars'  => array('',''),
                'method'    => 'getAdminactivitiesData',
                'shortHelp' => 'Simple getter for records in the sugarchimpactivity table.',
            ),
            'getAdminOptoutTrackerData' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','adminoptouttracker'),
                'pathVars'  => array('',''),
                'method'    => 'getAdminOptoutTrackerData',
                'shortHelp' => 'Simple getter for records in the sugarchimp_optout_tracker table.',
            ),
            'getAdminBatchesData' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','adminbatches'),
                'pathVars'  => array('',''),
                'method'    => 'getAdminBatchesData',
                'shortHelp' => 'Simple getter for records in the sugarchimp_batches table.',
            ),
            'getBatchData' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','batch','?'),
                'pathVars'  => array('','','batch_id'),
                'method'    => 'getBatchData',
                'shortHelp' => 'Calls MailChimp for batch data.',
            ),
            'getAdminSmartlistQueueData' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','adminsmartlistqueue'),
                'pathVars'  => array('',''),
                'method'    => 'getAdminSmartlistQueueData',
                'shortHelp' => 'Simple getter for records in the smartlist_queue table.',
            ),
            'getAdminSmartlistData' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','adminsmartlist'),
                'pathVars'  => array('',''),
                'method'    => 'getAdminSmartlistData',
                'shortHelp' => 'Simple getter for records in the smartlist table.',
            ),
            'emptyTable' => array(
                'reqType'   => 'POST',
                'path'      => array('SugarChimp','empty_table'),
                'pathVars'  => array('',''),
                'method'    => 'emptyTable',
                'shortHelp' => 'Simple action to empty sugarchimp or sugarchimp_mc table.',
            ),
            'countTable' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','count_table','?'),
                'pathVars'  => array('','','table_name'),
                'method'    => 'countTable',
                'shortHelp' => 'Simple action to return the number of records on a table.',
            ),
            'removeRecord' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','remove_record','?','?'),
                'pathVars'  => array('','','table_name','record'),
                'method'    => 'removeRecord',
                'shortHelp' => 'Simple action to remove single record from sugarchimp or sugarchimp_mc table.',
            ),
            'updateSetting' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','setting','?','?'),
                'pathVars'  => array('','','key','value'),
                'method'    => 'updateSetting',
                'shortHelp' => 'Update a sugarcrm setting key to a given value.',
            ),
            'getSetting' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','setting','?'),
                'pathVars'  => array('','','key'),
                'method'    => 'getSetting',
                'shortHelp' => 'Get a sugarcrm setting key value.',
            ),
            'getMailChimpCampaign' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','mailchimp','campaign','?'),
                'pathVars'  => array('','','','id'),
                'method'    => 'getMailChimpCampaign',
                'shortHelp' => 'Get the details of a single MailChimp Campaign.',
            ),
            'getMailChimpReport' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','mailchimp','report','?'),
                'pathVars'  => array('','','','id'),
                'method'    => 'getMailChimpReport',
                'shortHelp' => 'Get the report details of a single MailChimp Campaign.',
            ),
            'getMailChimpCampaigns' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','mailchimp','campaigns'),
                'pathVars'  => array('','',''),
                'method'    => 'getMailChimpCampaigns',
                'shortHelp' => 'Get list of MailChimp Campaigns.',
            ),
            'resetSugarChimp' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','reset'),
                'pathVars'  => array('',''),
                'method'    => 'resetSugarChimp',
                'shortHelp' => 'This will empty the sugarchimp table and will unset the mailchimp_list_name_c field for all target lists',
            ),
            'getSugarChimpSchedulers' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','getSchedulers'),
                'pathVars'  => array('',''),
                'method'    => 'getSugarChimpSchedulers',
                'shortHelp' => 'This will empty return the last 100 job_queue records of sugarchimp relaeted jobs.',
            ),
            'resetSugarChimpSchedulers' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','resetSchedulers'),
                'pathVars'  => array('',''),
                'method'    => 'resetSugarChimpSchedulers',
                'shortHelp' => 'This will empty the job_queue table of sugarchimp relaeted jobs.',
            ),
            'optInEveryone' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','optin'),
                'pathVars'  => array('',''),
                'method'    => 'optInEveryone',
                'shortHelp' => 'This will set all email addresses to be opted-in in the sugar database',
            ),
            'optInEmails' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','optinemails'),
                'pathVars'  => array('',''),
                'method'    => 'optInEmails',
                'shortHelp' => 'This will set an array of email addresses to be opted-in in the sugar database',
            ),
            'killScheduler' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','killScheduler'),
                'pathVars'  => array('',''),
                'method'    => 'killScheduler',
                'shortHelp' => 'This will remove SugarChimp jobs from job_queue where status is running',
            ),
            
            'queueSync' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','queue-sync','?','?'),
                'pathVars'  => array('','','target_list_id','sync_action'),
                'method'    => 'queueSync',
                'shortHelp' => 'This will queue a manual re-sync for a synced target list',
            ),
            'smartlistSetup' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','smartlist','setup'),
                'pathVars'  => array('','',''),
                'method'    => 'smartlistSetup',
                'shortHelp' => 'Get the data to show the SugarChimp smartlist setup page',
            ),
            'smartlistDashlet' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','smartlist','dashlet','?'),
                'pathVars'  => array('','','','mailchimp_list_id'),
                'method'    => 'smartlistDashlet',
                'shortHelp' => 'Get the data to show the SugarChimp smartlist dashlet',
            ),
            'createSugarList' => array(
                'reqType'   => 'POST',
                'path'      => array('SugarChimp','sugar','list'),
                'pathVars'  => array('','',''),
                'method'    => 'createSugarList',
                'shortHelp' => 'Given an array of sugar target list field values, create and return a new Sugar Target List',
            ),
            'createMailChimpList' => array(
                'reqType'   => 'POST',
                'path'      => array('SugarChimp','mailchimp','list'),
                'pathVars'  => array('','',''),
                'method'    => 'createMailChimpList',
                'shortHelp' => 'Given an array of mailchimp list field values, create and return a new MailChimp List',
            ),
            'smartlistSave' => array(
                'reqType'   => 'POST',
                'path'      => array('SugarChimp','smartlist','save'),
                'pathVars'  => array('','',''),
                'method'    => 'smartlistSave',
                'shortHelp' => 'Save the SmartList',
            ),
            'testFiltersQuery' => array(
                'reqType'   => 'POST',
                'path'      => array('SugarChimp','smartlist','test','filters'),
                'pathVars'  => array('','',''),
                'method'    => 'testFiltersQuery',
                'shortHelp' => 'Test the query generated by the filters.',
            ),
            'testSQLQuery' => array(
                'reqType'   => 'POST',
                'path'      => array('SugarChimp','smartlist','test','sql'),
                'pathVars'  => array('','',''),
                'method'    => 'testSQLQuery',
                'shortHelp' => 'Test the query generated by the filters.',
            ),
            'getCurrentPlan' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','plan'),
                'pathVars'  => array('',''),
                'method'    => 'getCurrentPlan',
                'shortHelp' => 'Get the current subscription plan SugarChimp is running on.',
            ),
            'enableAccountSyncing' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','accountsyncing','enable'),
                'pathVars'  => array('','',''),
                'method'    => 'enableAccountSyncing',
                'shortHelp' => 'Enable account syncing',
            ),
            'disableAccountSyncing' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','accountsyncing','disable'),
                'pathVars'  => array('','',''),
                'method'    => 'disableAccountSyncing',
                'shortHelp' => 'Disable account syncing',
            ),
            'removeListSyncing' => array(
                'reqType'   => 'GET',
                'path'      => array('SugarChimp','listsyncing','remove','?'),
                'pathVars'  => array('','','','target_list_id'),
                'method'    => 'removeListSyncing',
                'shortHelp' => 'Get the data to show the SugarChimp smartlist dashlet',
            ),
            'erase' => array(
                'reqType'   => 'POST',
                'path'      => array('SugarChimp','erase'),
                'pathVars'  => array('',''),
                'method'    => 'erase',
                'shortHelp' => 'Given an email address, erase the data in SuagrChimp and MailChimp',
            ),
        );
    }
    
    public function test($api, array $args)
    {
        $module = empty($args['param1']) ? 0 : $args['param1'];
        // 'ae07b928-4988-fb03-7866-55ae4a8bb9c8';
        $bean_id = empty($args['param2']) ? 0 : $args['param2'];
        // '577'
        $list_id = empty($args['param3']) ? 0 : $args['param3'];

        $bean = BeanFactory::getBean($module,$bean_id);
        $bean->load_relationship('prospect_lists');

        if (!isset($bean->prospect_lists) || !method_exists($bean->prospect_lists,'delete'))
        {
            $GLOBALS['log']->fatal("Relationshp Test - Cannot remove {$bean->module_dir} {$bean->id} from target list {$target_list->id}");
            return false;
        }

        $GLOBALS['log']->fatal("SugarChimp_Helper::remove_beans_from_list - Attempting to Remove {$bean->module_dir} {$bean->id} from target list {$target_list->id}");

        $bean->prospect_lists->delete($bean->id,$target_list->id);

        $GLOBALS['log']->fatal("SugarChimp_Helper::remove_beans_from_list - Removal Complete for {$bean->module_dir} {$bean->id} from target list {$target_list->id}");
        return true;

        //DELETE QUERY TEST FUNCTION
        $param1 = empty($args['param1']) ? 0 : $args['param1'];
        $param2 = empty($args['param2']) ? 0 : $args['param2'];
        $param3 = empty($args['param3']) ? 0 : $args['param3'];
        $param4 = empty($args['param4']) ? 0 : $args['param4'];
        $param5 = empty($args['param5']) ? 0 : $args['param5'];

        $sql = "DELETE FROM ".$db->quote($param1)." WHERE ".$db->quote($param2)."='".$db->quote($param3)."'";
        if (!$result = $db->query($sql))
        {
            return array('success'=>false,'message'=>'query failed: '.print_r($sql,true));
        }

        $row = $db->fetchByAssoc($result);

        return array('success'=>true,'data'=>$row);

        $campaign = BeanFactory::newBean('SugarChimpMCCampaign');
        $campaign->mailchimp_campaign_id = $param1;
        $campaign->name = $param2;
        $campaign->type = $param3;
        //$campaign-> = "chad+email{$label_id}@sugarchimp.com";
        $campaign->save();

        return array('success' => true, 'result' => $result);

        $create_count = $param1;
        $check_count = $param2;
        $start_label = $param3;

        $start_time = time();
        SugarChimp_Helper::log('fatal','SugarChimpApi::test start: '.$start_time);
        
    }

    public function test_post($api, array $args)
    {
        $emails = empty($args['emails']) ? false : $args['emails'];
        global $db;

        $i = 0;
        $arr = array();

        foreach ($emails as $email)
        {
            $arr []= strtoupper($email);
            $i++;

            if ($i==100)
            {
                $sql = "UPDATE email_addresses SET invalid_email=0,opt_out=0 WHERE email_address_caps IN ('".implode("','",$arr)."')";
                SugarChimp_Helper::log('debug','SugarChimpApi::testpost sql: '.print_r($sql,true));

                if(!$result = $db->query($sql))
                {
                    SugarChimp_Helper::log('fatal','SugarChimpApi::testpost query failed: '.print_r($sql,true));
                }

                $arr = array();
                $i=0;
            }
        }

        $sql = "UPDATE email_addresses SET invalid_email=0,opt_out=0 WHERE email_address_caps IN ('".implode("','",$arr)."')";
        SugarChimp_Helper::log('debug','SugarChimpApi::testpost sql: '.print_r($sql,true));

        if(!$result = $db->query($sql))
        {
            SugarChimp_Helper::log('fatal','SugarChimpApi::testpost query failed: '.print_r($sql,true));
        }

        return true;
    }

    public function getApiKey($api, array $args)
    {
        $apikey = SugarChimp_Setting::retrieve('apikey');
        return array('apikey'=>$apikey);
    }
    
    public function updateApiKey($api, array $args)
    {
        if (empty($args['apikey'])) {
            throw new SugarApiExceptionMissingParameter('ERR_MISSING_PARAMETER_FIELD', array('apikey'), 'SugarChimp');
        }

        $args['apikey'] = trim($args['apikey']);

        //test the key
        try 
        {
            $ping_response = SugarChimp_Helper::ping($args['apikey']);
        }
        catch (Exception $e)
        {
            return array('success'=>false,'message'=>$e->getMessage());
        }

        if (empty($ping_response))
        {
            SugarChimp_Helper::log('warning',"SugarChimp Error: could not access MailChimp server");
            //throw new SugarApiExceptionError($e->getMessage(), null, 'SugarChimp');
            //want to pass the error message so return 200
            return array('success'=>false,'message'=>"Could not access MailChimp server");
        }
        
        // sugar 6 has a weird issue where saveSetting returns 0 if the setting doesn't change
        // if apikey in config is what's provided, return success
        global $sugar_version;
        if(preg_match( "/^6.*/", $sugar_version))
        {
            $current_apikey = $this->getApiKey($api,array());
            if (!empty($current_apikey['apikey']) && $current_apikey['apikey']==$args['apikey'])
            {
                return array('success' => true,'message'=>'Saving key was successful.');
            }
        }
        
        $admin = BeanFactory::getBean('Administration');
        $return = $admin->saveSetting('sugarchimp', 'apikey', $args['apikey']);
        
        return array('success' => (!empty($return)),'message'=>'Saving key was '.(!empty($return)?'successful':'unsuccessful').'.');
    }
    public function validateLicense($api, array $args)
    {
        if (empty($args['key'])) 
        {
            throw new SugarApiExceptionMissingParameter('ERR_MISSING_PARAMETER_FIELD', array('apikey'), 'SugarChimp');
        }

        $args['module'] = 'SugarChimp';
        $args['key'] = trim($args['key']);

        return SugarChimpOutfittersLicense::validate($args);
    }
    
    public function getLicenseValidation($api, array $args)
    {
        $module = 'SugarChimp';
        $private_key = SugarChimpOutfittersLicense::getKey($module);
        $last = SugarChimpOutfittersLicense::get_last_response($module);
        SugarChimp_Helper::log('debug','SugarChimpAPI::getLicenseValidation: last: ' . print_r($last,true));
        $data = SugarChimpOutfittersLicense::get_last_response($module);
        $trial_time_remaining = null;

        SugarChimp_Helper::log('debug','SugarChimpAPI:getLicenseValidation: data: ' . print_r($data,true));
        
        if (empty($data['last_result']) or !is_array($data['last_result']) or 
            empty($data['last_result']['result']) or !is_array($data['last_result']['result'])
            )
        {
            // bad data in last response
            SugarChimp_Helper::log('fatal','SugarChimpAPI:getLicenseValidation: bad data in last response: ' . print_r($last,true));
            return array(
                'success'=> false,
                'message'=> 'Invalid Last Response from SugarOutfitters.',
            );
        }

        $result = $data['last_result']['result'];
        if(!empty($result['trial_ends']))
        {
            $trial_time_remaining = $result['trial_ends'] - time();
            if($trial_time_remaining < 15)
            {
                $result['in_trial'] = false;
                $result['in_upgrade_trial'] = false;
            }
        }
        
        return array(
            'success' => $result['validated'],
            'private_key' => $private_key,
            'result' => $result,
            'trial_time_remaining' =>$trial_time_remaining,
        );
    }

    public function getSwitchablePlans($api, array $args)
    {
        $response = SugarChimpOutfittersLicense::get_switchable_plans('SugarChimp');
        SugarChimp_Helper::log('debug','SugarChimpAPI::getSwitchablePlans array: ' . $response);
        
        return $response;
    }
    
    public function switchPlan($api, array $args)
    {
        if(empty($args['plan_id']))
        {
            SugarChimp_Helper::log('debug',"SugarChimp::switchPlan: No plan_id found.");
            return array(
                'success'=>false,
                'message'=>'No plan is entered.',
            );
        }
        // call SOLicense with switchPlans plan_id
        $response = SugarChimpOutfittersLicense::switch_plan('SugarChimp',$args['plan_id']);
        SugarChimp_Helper::log('debug',"SugarChimp::switchPlan response: " . print_r($response, true));
        
        // return response
        return $response;
    }
    public function cancelUpgrade($api, array $args)
    {
        // call SOLicense to cancel upgrade
        $response = SugarChimpOutfittersLicense::cancel_upgrade('SugarChimp');
        SugarChimp_Helper::log('debug',"SugarChimp::cancelUpgrade response: " . print_r($response, true));
        // return response
        return $response;
    }
    public function preChecksScheduler($api, array $args)
    {
        global $sugar_flavor;

        $php_compatible = version_compare(PHP_VERSION, '5.3.0') >= 0;

        $ondemand = ($sugar_flavor != 'CE' && SugarChimp_Helper::is_ondemand_instance() === true);

        // 761dev todo-jon : expand scheduler check to find things in the last 24 hours only
        $scheduler_ran = false;
        $instructions = '';
        
        $scheduler = BeanFactory::getBean('Schedulers');
        $scheduler_list = $scheduler->get_list('','last_run is not null');
        
        if(!empty($scheduler_list) && $scheduler_list['row_count'] > 0) {
            $scheduler_ran = true;
        }
        
        if (!isset($_SERVER['Path'])) {
            $_SERVER['Path'] = getenv('Path');
        }

        $success = $scheduler_ran && $php_compatible;

        return array(
            'ondemand'=>$ondemand,
            'scheduler_ran'=>$scheduler_ran,
            'is_windows'=>is_windows(),
            'realpath'=>SUGAR_PATH,
            'php_compatible'=>$php_compatible,
            'php_version'=>PHP_VERSION,
            'success'=>$success 
            );
    }
    public function preChecksNetwork($api, array $args)
    {
        SugarChimp_Helper::log('debug','preCheckNetwork: start');
        global $sugar_flavor;

        $network_success = false;
        $message = "cURL must be enabled to test your network settings.";
        $curl_enabled = function_exists('curl_init');
        SugarChimp_Helper::log('debug','preCheckNetwork: curl enabled '.print_r($curl_enabled,true));
        
        $outbound_proxy = SugarChimp_Setting::retrieve('outbound_proxy');

        //$curl_enabled = false;
        if($curl_enabled){
            SugarChimp_Helper::log('debug',"SugarChimp::preChecksNetwork: cURL is enabled, reaching out.");

            $curl = SugarChimp_Helper::initialize_curl();
            $url = "https://mailchimp.com";
            
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HEADER, true);    // we want headers
            curl_setopt($curl, CURLOPT_NOBODY, true);    // we don't need body
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($curl, CURLOPT_TIMEOUT,10);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,false);
            
            $result = curl_exec($curl);
            SugarChimp_Helper::log('debug',"SugarChimp::preChecksNetwork: result received: " . print_r($result, true));
            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if(curl_error($curl))
                $err = curl_error($curl);
            SugarChimp_Helper::log('debug',"SugarChimp::preChecksNetwork: curl code: " . print_r($code, true));
            curl_close($curl);

            if($code > 0 && $code < 400)
            {
                SugarChimp_Helper::log('debug',"SugarChimp::preChecksNetwork: successful code received: " . print_r($code,true));
                $network_success = true;
                $message = " Network is Ready, code: " . print_r($code,true);
            }
            else
            {
                SugarChimp_Helper::log('fatal','preCheckNetwork: invalid cURL response code: ' . print_r($code,true));
                $message = " SugarChimp is unable to reach MailChimp, code " . print_r($code,true) . ": ".print_r($err,true);
            }
        }
        $success = $network_success && $curl_enabled;
        SugarChimp_Helper::log('debug','preCheckNetwork: success value: '.print_r($success,true));
        
        return array(
            'network_success'=>$network_success,
            'curl_enabled'=>$curl_enabled,
            'outbound_proxy'=>$outbound_proxy,
            'message'=>$message,
            'success'=>$success 
        );
    }
    
    public function getMailChimpListsCount($api, array $args)
    {
        $apikey = SugarChimp_Setting::retrieve('apikey');
        
        if(empty($apikey)) {
            //todo: lang string
            return array('success'=>false,
                'message'=>'No API key was found. Please go back and enter your MailChimp API Key.');
        }
        
        $total_list_count = 0; //includes those synced already
        $list_count = 0; //only if not synced
        $subscriber_count = 0;
        try {
            $result = MailChimp_API::get_lists(array('fields'=>'lists.id,lists.stats.member_count'));
            
            //candidate to be removed if performance is an issue
            if(!empty($result['lists']) and is_array($result['lists'])) {
                $list_count = count($result['lists']);
                $total_list_count = $list_count;
            
                foreach($result['lists'] as $list) {
                    $subscriber_count += $list['stats']['member_count'];
                }
            }
        } catch(Exception $e) {
            SugarChimp_Helper::log('warning',"SugarChimp Error: ".$e->getMessage());
            
            //want to pass the error message so return 200
            return array('success'=>false,
                'message'=>$e->getMessage()
                );
        }

        return array(
            'success'=> true, 
            'total_list_count'=>$total_list_count, 
            'list_count'=>$list_count, 
            'subscriber_count'=>$subscriber_count
            );
    }
    
    public function getMailChimpLists($api, array $args)
    {
        $sync_count = 0;
        $synced_list_ids = array();
        
        $prospect_list_bean = BeanFactory::getBean('ProspectLists');
        $prospect_lists = $prospect_list_bean->get_list('name','mailchimp_list_name_c is not null',0,99999,99999,0,false,array('id','name','mailchimp_list_name_c')); //Sugar will use list_max_entries_per_page if set to -1
        
        foreach($prospect_lists['list'] as $list) {
            //to be safe just in case saving in db as empty string
            if(!empty($list->mailchimp_list_name_c)) {
                $sync_count += 1;
                $synced_list_ids[] = $list->mailchimp_list_name_c;
            }
        }
        
        $list_count = 0;
        $lists = array();
        try {
            // $mc = new Mailchimp($apikey);
            // $result = $mc->lists->getList(array(),0,100,'name','ASC'); //MC api limit of 100
            $result = MailChimp_API::get_lists(array('fields'=>'lists.id,lists.name,lists.stats.member_count'));

            $list_count = $result['total'];
            //candidate to be removed if performance is an issue
            if(!empty($result['lists']) and is_array($result['lists'])) {
                foreach($result['lists'] as $list) {
                    if(in_array($list['id'], $synced_list_ids)) {
                        continue; //skip if already synced...may want to display this list in the future
                    }
                    $lists[] = array(
                        'id' => $list['id'],
                        // 'web_id' => $list['web_id'], // not available in v3 api
                        'name' => $list['name'],
                        'member_count' => $list['stats']['member_count'],
                    );
                }
            }
        } catch(Exception $e) {
            SugarChimp_Helper::log('warning',"SugarChimp Error: ".$e->getMessage());
            
            //want to pass the error message so return 200
            return array('success'=>false,'message'=>$e->getMessage());
        }

        return array('success'=> true, 'list_count'=>$list_count, 'lists'=>$lists, 'sync_count'=>$sync_count);
    }

    // nothing is using this as of 12/14/2016
    // deprecating for SC 7.8
    // public function getAllMailChimpLists($api, array $args)
    // {
    //     $apikey = SugarChimp_Setting::retrieve('apikey');
        
    //     if(empty($apikey)) {
    //         //todo: lang string
    //         return array('success'=>false,'message'=>'No API key was found. Please go back and enter your MailChimp API Key.');
    //     }

    //     try {
    //         $mc = new Mailchimp($apikey);
    //         $results = $mc->lists->getList(array(),0,100,'name','ASC'); //MC api limit of 100
    //     } catch(Exception $e) {
    //         SugarChimp_Helper::log('warning',"SugarChimp Error: ".$e->getMessage());
    //         return array('success'=>false,'message'=>$e->getMessage());
    //     }

    //     return array('success'=> true, 'lists'=>$results);
    // }
    
    public function getSyncedMailChimpLists($api, array $args)
    {
        $apikey = SugarChimp_Setting::retrieve('apikey');
        
        if(empty($apikey)) 
        {
            //todo: lang string
            return array('success'=>false,'message'=>'No API key was found. Please go back and enter your MailChimp API Key.');
        }

        try 
        {
            $all_synced_lists = SugarChimp_Helper::get_synced_lists();

            if (empty($all_synced_lists))
            {
                // query failed
                SugarChimp_Helper::log('fatal','SugarChimpApi->getSyncedMailChimpLists there are no sugar lists synced to mailchimp lists');
                throw new Exception('SugarChimpApi->getSyncedMailChimpLists there are no sugar lists synced to mailchimp lists');
            }

            $synced_lists = array();
            foreach ($all_synced_lists as $list)
            {
                // $synced_lists[mailchimp_list_id] = sugar_list_id
                $synced_lists[$list['mailchimp_list_id']] = $list['id'];
            }

            if (empty($synced_lists))
            {
                // there are no synced lists
                return array(
                    'success'=> true, 
                    'lists'=> array(
                        'data' => array(),
                        'errors' => array(),
                        'total' => 0,
                    ),
                );
            }

            $results = MailChimp_API::get_lists();

            if (!empty($results['lists']))
            {
                foreach ($results['lists'] as $key => $list)
                {
                    if (array_key_exists($list['id'],$synced_lists))
                    {
                        $results['lists'][$key]['sugar_target_list_id'] = $synced_lists[$list['id']];
                    }
                    else
                    {
                        unset($results['lists'][$key]);
                    }
                }
            }
            // reset the keys so it starts a 0
            $results['lists'] = array_values($results['lists']);

            // set the new total
            $results['total'] = count($results['lists']);
        } 
        catch(Exception $e)
        {
            SugarChimp_Helper::log('warning',"SugarChimp Error: ".$e->getMessage());
            return array('success'=>false,'message'=>$e->getMessage());
        }
        // SugarChimp_Helper::log('fatal',"SugarChimpApi::getSyncedMailChimpLists original data ". print_r($results,true));

        //sort by date_created
        // because we can't use usort in On-demand...
        // add a key based on date_created timestamps to temp array
        // if multiple timestamps, add seconds until available.
        foreach ($results['lists'] as $key => $list)
        {

            // SugarChimp_Helper::log('debug',"SugarChimpAPI->getSyncedMailChimpLists set new key for list:".print_r($list,true));
            $time = strtotime($list['date_created']);
            $new_key = $time;
            
            while(isset($list[$new_key]))
            {
                // SugarChimp_Helper::log('warning',"SugarChimpAPI->getSyncedMailChimpLists timestamp key already used: ".print_r($time,true));
                $compare_to_time = strtotime($list[$new_key]['date_created']);
                if($time < $compare_to_time)
                {
                    SugarChimp_Helper::log('debug',"SugarChimpAPI->getSyncedMailChimpLists list created before other list, taking key and moving other list down.".print_r($time,true));
                    //swap activity with current temp_activity[$new_key]
                    $replace_list = $results['lists'][$new_key];
                    $results['lists'][$new_key] = $list;
                    $list = $replace_list;
                    
                    $time = $compare_to_time;
                    SugarChimp_Helper::log('debug',"SugarChimpAPI->getSyncedMailChimpLists Now Searching to place list: ".print_r($list,true));
                }
                $new_key++;
            }
            
            // set the new key
            $results['lists'][$new_key] = $list;
            
            // remove the old key reference
            unset($results['lists'][$key]);
        }

        // reverse sort the keys numerically
        krsort($results['lists'],SORT_NUMERIC);
        $results['lists'] = array_values($results['lists']);

        SugarChimp_Helper::log('debug',"SugarChimpApi::getSyncedMailChimpLists sorted data ". print_r($results,true));

        return array('success'=> true, 'data'=>$results);
    }

    
    //When saving lists to sync only process those submitted by the form. Not all lists will
    //be included if too large...sync and unsync for each list...process each list that is passed through
    //this then supports previous installs
    
    public function saveMailChimpLists($api, array $args)
    {
        /** //args coming in
            [mc_id_ec9ba14db4] => ec9ba14db4
            [mc_sync_ec9ba14db4] => 1
            [mc_module_ec9ba14db4] => Lead
        */
        $to_sync = array();
        $job_queued = false;
        
        //look for mc_id_* formats and process those keys
        //NOTE: right now all previously synced lists have been removed so only look for mc_sync
        foreach ($args as $key => $value)
        {
            if (strpos($key, 'mc_sync_') === 0)
            {
                if (!empty($value)) {
                    $list_id = substr($key,8);
                    $to_sync[$list_id] = $args['mc_module_'.$list_id];

                    SugarChimp_Helper::queue_MailChimpToSugarCRM_job($list_id,$args['mc_module_'.$list_id]);
                    $job_queued = true;
                }
            }
        }

        return array('success'=> true,'job_queued'=>$job_queued);
    }
    
    public function getSugarCRMLists($api, array $args)
    {
        $total_list_count = 0; //includes those synced already
        $list_count = 0; //only if not synced
        $sync_count = 0;
        $subscriber_count = 0;
        $lists = array();
        $synced_lists = array();
        $synced_list_ids = array();
        $member_count = 0;
        
        $prospect_list_bean = BeanFactory::getBean('ProspectLists');
        $prospect_lists = $prospect_list_bean->get_list('name','',0,99999,99999,0,false,array('id','name','mailchimp_list_name_c')); //Sugar will use list_max_entries_per_page if set to -1
        
        $total_list_count = $prospect_lists['row_count'];
        
        foreach($prospect_lists['list'] as $list) {
            
            if(!empty($list->mailchimp_list_name_c)) {
                $sync_count += 1;
                $synced_lists[] = array(
                    'id' => $list->id,
                    'name' => $list->name,
                    'member_count' => $member_count,
                );
                $synced_list_ids[] = $list->mailchimp_list_name_c;
            } else {
                $member_count = $list->get_entry_count();
                $subscriber_count += $member_count;
                $list_count += 1;
                $lists[] = array(
                    'id' => $list->id,
                    'name' => $list->name,
                    'member_count' => $member_count,
                );
            }
        }

        //only lists not already synced
        $mailchimp_lists = array();
        try {
            // $mc = new MailChimp($apikey);
            // $result = $mc->lists->getList(array(),0,100,'name','ASC'); //MC api limit of 100
            $result = MailChimp_API::get_lists(array('fields'=>'lists.id,lists.name'));

            //candidate to be removed if performance is an issue
            if(!empty($result['lists']) and is_array($result['lists'])) {
                foreach($result['lists'] as $list) {
                    if(in_array($list['id'], $synced_list_ids)) {
                        continue; //skip if already synced...may want to display this list in the future
                    }
                    $mailchimp_lists[] = array(
                        'id' => $list['id'],
                        'name' => $list['name'],
                    );
                }
            }
        } catch(Exception $e) {
            SugarChimp_Helper::log('warning',"SugarChimp Error: ".$e->getMessage());
            
            //want to pass the error message so return 200
            return array('success'=>false,'message'=>$e->getMessage());
        }
        
        return array(
            'success'=> true,
            'total_list_count'=>$total_list_count,
            'list_count'=>$list_count,
            'subscriber_count'=>$subscriber_count,
            'lists'=>$lists,
            'sync_count'=>$sync_count,
            'synced_lists'=>$synced_lists,
            'mailchimp_lists'=>$mailchimp_lists,
        );
    }

    
    public function saveSugarCRMLists($api, array $args)
    {   
        //for each checked list save the sync module to the prospect list (mailchimp_default_module_c) and create list in MC.
        //then set mailchimp_list_name_c to mailchimp id
        /** //args coming in
            [sugar_id_8e12bdc7-a26c-11e3-adef-001dd96c2083] => ec9ba14db4
            [sugar_sync_8e12bdc7-a26c-11e3-adef-001dd96c2083] => 1
            [sugar_module_8e12bdc7-a26c-11e3-adef-001dd96c2083] => Lead
        */
        //ensure each mailchimp list is selected just once...if not then throw an error or throw out the last one
        $mailchimp_lists = array();
        $to_sync = array();
            
        //NOTE: right now all previously synced lists have been removed so only look for sugar_sync_
        foreach ($args as $key => $value)
        {
            if (strpos($key, 'sugar_sync_') === 0)
            {
                if (!empty($value)) {
                    $list_id = substr($key,11);
                    SugarChimp_Helper::log('debug',"SugarChimp: saveSugarCRMLists list_id {$list_id}");

                    $prospect_list_bean = BeanFactory::getBean('ProspectLists',$list_id);
                    if (empty($prospect_list_bean->id)) {
                        SugarChimp_Helper::log('debug',"SugarChimp: Tried syncing SugarCRM list ".$prospect_list_bean->name."|".$list_id." to MailChimp, but it does not exist in SugarCRM.");
                        continue;
                    }
                    $mailchimp_list_id = $args['sugar_mailchimp_'.$list_id];
                    if (empty($mailchimp_list_id)) {
                        SugarChimp_Helper::log('debug',"SugarChimp: Skipping sync for SugarCRM list ".$prospect_list_bean->name."|".$list_id." to MailChimp as no MailChimp list was selected to sync to.");
                        continue;
                    }
                    SugarChimp_Helper::log('debug',"SugarChimp: saveSugarCRMLists mailchimp_list_id {$mailchimp_list_id}");
                    
                    //check to see if already synced...
                    if (!empty($prospect_list_bean->mailchimp_list_name_c)) {
                        SugarChimp_Helper::log('debug',"SugarChimp: Tried syncing SugarCRM list ".$prospect_list_bean->name."|".$list_id." to MailChimp list ".$mailchimp_list_id.", but it is already synced to a different list: ".$list->mailchimp_list_name_c);
                        continue;
                    }
                    if (in_array($mailchimp_list_id,$mailchimp_lists)) {
                        SugarChimp_Helper::log('debug','A MailChimp list was selected to be synced with 2 different SugarCRM lists. Please address and try again. List ID: '.$prospect_list_bean->name.'|'.$list_id);
                        return array('success'=>false,'message'=>'A MailChimp list was selected to be synced with 2 different SugarCRM lists. Please address and try again.');
                    }
                    $prospect_list_bean->mailchimp_default_module_c = $args['sugar_module_'.$list_id];
                    $prospect_list_bean->mailchimp_list_name_c = $mailchimp_list_id;
                    
                    $to_sync[$list_id] = $prospect_list_bean;
                    $mailchimp_lists[$mailchimp_list_id] = $mailchimp_list_id;
                }
            }
        }
        
        SugarChimp_Helper::log('debug',"SugarChimp: saveSugarCRMLists to_sync keys ".print_r($to_sync,true));        

        if(!empty($to_sync)) {
            //we got here so now save the sync in SugarCRM
            foreach ($to_sync as $list) {
                SugarChimp_Helper::log('debug',"SugarChimp: saveSugarCRMLists saving list {$list->id}");        
                $list->save(); //not set as synced
            }
            
            //now push the subscribers over
            SugarChimp_Helper::push_lists_to_mailchimp($to_sync);
        }

        
        return array('success'=> true);
    }
    
    public function getHealth($api, array $args)
    {
        global $db;
        
        $apikey = SugarChimp_Setting::retrieve('apikey');

        $mailchimp_configured = false;
        if (!empty($apikey)) {
            $ping_result = SugarChimp_Helper::ping();
            $mailchimp_configured = true;
            if (empty($ping_result))
            {
                $mailchimp_configured = false;
            }
        }

        $last_ran = '';
        $next_run = '';
        $interval = '';
        $scheduler_found = false;
        $scheduler_status = 'Inactive';
        $scheduler_id = '';
        $to_mailchimp_lists = 0;
        $to_mailchimp_subscribers = 0;
        $from_mailchimp_lists = 0;
        $from_mailchimp_subscribers = 0;
        $from_mailchimp_activities = 0;
        $mailchimp_initial = false;
        $mailchimp_activity_initial = false;

        $scheduler = BeanFactory::getBean('Schedulers');
        $scheduler->retrieve_by_string_fields(array('job' => 'function::SugarChimp'));        
        if (!empty($scheduler->id))
        {
            $scheduler_found = true;
            $scheduler_id = $scheduler->id;
            $scheduler_status = $scheduler->status;
            $last_ran = $scheduler->last_run;
            
            global $mod_strings, $current_language;
            $temp_mod_strings = $mod_strings;
            $mod_strings = return_module_language($current_language, 'Schedulers');  
            
            $scheduler->get_list_view_data(); //sets some vars we need
            $interval = $scheduler->intervalHumanReadable;

            $mod_strings = $temp_mod_strings;
            
            //format last_ran
            if (!empty($last_ran)) {
                global $current_user, $timedate;

                $last_ran = $timedate->to_display_date_time($last_ran, true, true, $current_user);
            }
            else
            {
                $last_ran = 'Has not run yet';
            }
            
        } else {
            $last_ran = 'The SugarChimp scheduler job is missing.';
        }
        
        //SugarCRM to MailChimp
        $result = $db->query("select count(distinct mailchimp_list_id) as list_count from sugarchimp where name != 'AddWebhook'");
        $row = $db->fetchByAssoc($result);
        if (!empty($row)) {
            $to_mailchimp_lists = $row['list_count'];
        }
        $result = $db->query("select count(id) as subscriber_count from sugarchimp where name != 'AddWebhook'");
        $row = $db->fetchByAssoc($result);
        if (!empty($row)) {
            $to_mailchimp_subscribers = $row['subscriber_count'];
        }
        
        //MailChimp to SugarCRM
        $result = $db->query('select count(distinct mailchimp_list_id) as list_count from sugarchimp_mc');
        $row = $db->fetchByAssoc($result);
        if (!empty($row)) {
            $from_mailchimp_lists = $row['list_count'];
        }
        $result = $db->query('select count(id) as subscriber_count from sugarchimp_mc');
        $row = $db->fetchByAssoc($result);
        if (!empty($row)) {
            $from_mailchimp_subscribers = $row['subscriber_count'];
        }

        // get synced lists
        $synced_lists = SugarChimp_Helper::get_synced_lists();
        
        // get names of mailchimp lists
        $mailchimp_lists = array();
        try 
        {
            // $mc = new MailChimp($apikey);
            // $result = $mc->lists->getList(array(),0,100,'name','ASC'); //MC api limit of 100
            $result = MailChimp_API::get_lists(array('fields'=>'lists.id,lists.name,lists.date_created'));

            //candidate to be removed if performance is an issue
            if(!empty($result['lists']) and is_array($result['lists'])) 
            {
                foreach($result['lists'] as $list) 
                {
                    $mailchimp_lists[$list['id']] = array(
                        'id' => $list['id'],
                        'name' => $list['name'],
                        'date_created' => $list['date_created'],
                    );
                }
            }
        } 
        catch(Exception $e) 
        {
            SugarChimp_Helper::log('warning',"SugarChimpAPI getHealth: ".$e->getMessage());
        }

        if (!empty($synced_lists) and is_array($synced_lists))
        {
            foreach ($synced_lists as $key => $list)
            {
                if (array_key_exists($list['mailchimp_list_id'],$mailchimp_lists))
                {
                    $synced_lists[$key]['mailchimp_name'] = $mailchimp_lists[$list['mailchimp_list_id']]['name'];
                    $synced_lists[$key]['date_created'] = $mailchimp_lists[$list['mailchimp_list_id']]['date_created'];
                }
                else
                {
                    $synced_lists[$key]['mailchimp_name'] = 'unknown';
                    $synced_lists[$key]['date_created'] = 0;
                }

                // get status of sync
                $status = true;
                $actions = array();

                // MailChimp to Sugar Sync Queued - check job_queue for MailChimp to Sugar where $list id is in the name
                $sql = "SELECT id FROM job_queue WHERE (resolution!='success' OR status!='done') AND name='MailChimp to SugarCRM Job - Initial Import for list ".$db->quote($list['mailchimp_list_id'])."'";
                $sql = $db->limitQuery($sql,0,1,false,'',false);
                $result = $db->query($sql);
                if ($row = $db->fetchByAssoc($result))
                {
                    $status = 'MailChimp to Sugar Sync Queued';
                }

                if ($status === true)
                {
                    // MailChimp to Sugar Sync In Progress - check sugarchimp_mc table for list_id
                    $sql = "SELECT id FROM sugarchimp_mc WHERE mailchimp_list_id='".$db->quote($list['mailchimp_list_id'])."'";
                    $sql = $db->limitQuery($sql,0,1,false,'',false);
                    $result = $db->query($sql);
                    if ($row = $db->fetchByAssoc($result))
                    {
                        $status = "MailChimp to Sugar Sync In-progress";
                    }
                }

                if ($status === true)
                {
                    // Sugar to MailChimp Sync In Progress - check sugarchimp table for list_id where name is not AddWebhook
                    $sql = "SELECT id FROM sugarchimp WHERE mailchimp_list_id='".$db->quote($list['mailchimp_list_id'])."' AND name != 'AddWebhook'";
                    $sql = $db->limitQuery($sql,0,1,false,'',false);
                    $result = $db->query($sql);
                    if ($row = $db->fetchByAssoc($result))
                    {
                        $status = "Sugar to MailChimp Sync In-progress";
                    }
                }

                if ($status === true)
                {
                    $status = 'Sync Complete';
                    $actions = array('mc2sugar','sugar2mc');
                }

                $synced_lists[$key]['status'] = $status;
                $synced_lists[$key]['actions'] = $actions;
            }

            // 3/21/18
            // jon-todo Sort the Array here by date_created in MC
            // get mailchimp name for each list
            // if multiple timestamps, add seconds until available.
            foreach ($synced_lists as $key => $list)
            {

                SugarChimp_Helper::log('debug',"SugarChimpAPI->getHealth() set new key for list:".print_r($list,true));
                $time = strtotime($list['date_created']);
                $new_key = $time;
                
                while(isset($list[$new_key]))
                {
                    SugarChimp_Helper::log('warning',"SugarChimpAPI->getHealth() timestamp key already used: ".print_r($time,true));
                    $compare_to_time = strtotime($list[$new_key]['date_created']);
                    if($time < $compare_to_time)
                    {
                        SugarChimp_Helper::log('debug',"SugarChimpAPI->getHealth() list created before other list, taking key and moving other list down.".print_r($time,true));
                        //swap activity with current temp_activity[$new_key]
                        $replace_list = $synced_lists[$new_key];
                        $synced_lists[$new_key] = $list;
                        $list = $replace_list;
                        
                        $time = $compare_to_time;
                        SugarChimp_Helper::log('debug',"SugarChimpAPI->getHealth() Now Searching to place list: ".print_r($list,true));
                    }
                    $new_key++;
                }
                
                // set the new key
                $synced_lists[$new_key] = $list;
                
                // remove the old key reference
                unset($synced_lists[$key]);
            }

            // reverse sort the keys numerically
            krsort($synced_lists,SORT_NUMERIC);
            $synced_lists = array_values($synced_lists);
        }

        //MailChimp Activity to SugarCRM
        $result = $db->query('select count(id) as activity_count from sugarchimp_mc_activity');
        $row = $db->fetchByAssoc($result);
        if (!empty($row)) {
            $from_mailchimp_activities = $row['activity_count'];
        }

        
        $sugarchimp_version = SugarChimp_Helper::get_sugarchimp_version();
        
        require_once('modules/SugarChimp/license/OutfittersLicense.php');
        $valid_license = SugarChimpOutfittersLicense::isValid('SugarChimp');
        
        if ($valid_license !== true) {
            $valid_license = false;
        }


        $result = $db->query("select count(id) as pending_count from job_queue where target = 'class::MailChimpToSugarCRM' and status != 'done' and resolution != 'success'");
        $row = $db->fetchByAssoc($result);
        if (!empty($row)) {
             if( $row['pending_count'] > 0) {
                $mailchimp_initial = true;
             }
        }
        

        $result = $db->query("select count(id) as pending_count from job_queue where target = 'class::MailChimpToSugarCRMActivity' and status != 'done' and resolution != 'success'");
        $row = $db->fetchByAssoc($result);
        if (!empty($row)) {
             if( $row['pending_count'] > 0) {
                $mailchimp_activity_initial = true;
             }
        }

        
        $sugarchimp_logger = SugarChimp_Setting::retrieve('logger');
        if (empty($sugarchimp_logger)) {
            $sugarchimp_logger = ''; //just to be safe
        }

        // ensure all lists have their webhooks properly setup
        SugarChimp_Helper::queue_add_webhooks_to_all_lists();
        
        $sugarchimp_webhook_url = SugarChimp_Helper::get_webhook_url_for_mc();
        
        //get status from SmartListAPI to populate on health-status
        $slapi = new SmartListApi();
        $smartlist_health = $slapi->getHealth(null, array());
        $smartlist_scheduler_id = $smartlist_health['id'];
        $smartlist_scheduler_last_ran  = $smartlist_health['last_ran'];
        $smartlist_scheduler_status  = $smartlist_health['status'];
        $smartlist_scheduler_interval  = $smartlist_health['interval'];
        $smartlist_scheduler_found = $smartlist_health['found'];

        // get current sugarchimp subscription
        $current_plan = SugarChimp_Helper::get_current_plan();

        return array(
            'success' => true,
            'scheduler_found' => $scheduler_found,
            'scheduler_id' => $scheduler_id,
            'last_ran' => $last_ran,
            'scheduler_status' => $scheduler_status,
            //'next_run' => $next_run,
            'interval' => $interval,
            'mailchimp_configured' => $mailchimp_configured,
            'to_mailchimp_lists' => $to_mailchimp_lists,
            'to_mailchimp_subscribers' => $to_mailchimp_subscribers,
            'from_mailchimp_lists' => $from_mailchimp_lists,
            'from_mailchimp_subscribers' => $from_mailchimp_subscribers,
            'from_mailchimp_activities' => $from_mailchimp_activities,
            'sugarchimp_version' => $sugarchimp_version,
            'valid_license' => $valid_license,
            'mailchimp_initial' => $mailchimp_initial,
            'mailchimp_activity_initial' => $mailchimp_activity_initial,
            'sugarchimp_logger' => $sugarchimp_logger,
            'sugarchimp_webhook_url' => $sugarchimp_webhook_url,
            'synced_lists' => $synced_lists,
            'smartlist_scheduler_id' => $smartlist_scheduler_id,
            'smartlist_last_ran' => $smartlist_scheduler_last_ran,
            'smartlist_interval' => $smartlist_scheduler_interval,
            'smartlist_status' => $smartlist_scheduler_status,
            'smartlist_scheduler_found' => $smartlist_scheduler_found,
            'current_plan' => $current_plan,
        );
    }
    
    public function saveConfigLogger($api, array $args)
    {
        $result = SugarChimp_Setting::set('logger',$args['sugarchimp_logger']);
        $result_smartlist = SmartList_Setting::set('logger',$args['sugarchimp_logger']);
        if ($result === true && $result_smartlist === true)
        {
            return array('success'=> true);
        }
        else
        {
            return array(
                'success' => false, 
                'message' => 'Failed to save logger settings. Try again.',
            );
        }
    }

    public function dashletPersonRecordData($api,$args)
    {
        $apikey = SugarChimp_Setting::retrieve('apikey');
        
        if(empty($apikey)) 
        {
            //todo: lang string
            return array(
                'success'=>false,
                'dataFetched'=>true,
                'message'=>'No API key was found. Please go back and enter your MailChimp API Key.');
        }

        // check for a valid SugarOutfitters license
        $license_check = SugarChimp_License::is_valid();
        if ($license_check !== true)
        {
            SugarChimp_Helper::log('fatal',"SugarChimpAPI->dashletPersonRecordData license check failed.");
            return array(
                'success'=>false,
                'dataFetched'=>true,
                'message'=>'  License Key is invalid. Contact support@sugarchimp.com for help.');
        }

        // make sure an email address exists
        if (empty($args['email']))
        {
            //todo: lang string
            return array(
                'success'=>false,
                'dataFetched'=>true,
                'message'=>'  An email address is required to have MailChimp Activity data.');
        }

        // get the mc/sugar lists for the email address
        $lists = SugarChimp_Helper::get_mc_lists_for_email($args['email']);

        // the contact is not related to any synced lists
        if (empty($lists))
        {
            $data = array();

            $data['dataFetched'] = true;
            $data['message'] = 'There is no MailChimp Activity data for this person.';

            return $data;
        }
        
        // temp array to store activities in
        $temp_activities = array();

        // go through each list id we have and get the member's activity
        foreach ($lists as $mc_id => $list_data)
        {
            try {
                $mc = new Mailchimp($apikey);
                $activities = $mc->lists->memberActivity($mc_id,array(
                    array('email' => $args['email'])
                ));
            } catch(Exception $e) {
                SugarChimp_Helper::log('warning',"SugarChimpAPI->dashletPersonRecordData for list {$mc_id} error: ".$e->getMessage());
                continue;
            }

            if (empty($activities['data'][0]['activity']))
            {
                SugarChimp_Helper::log('debug',"SugarChimpAPI->dashletPersonRecordData No activities for {$mc_id}");
                continue;
            }

            SugarChimp_Helper::log('debug',"SugarChimpAPI->dashletPersonRecordData found activities");
            
            foreach ($activities['data'][0]['activity'] as $activity)
            {
                if ($activity['action'] == 'unsub')
                {
                    $activity['list_id'] = $mc_id;
                    $activity['list_name'] = $list_data['name'];
                    $activity['title'] = $list_data['name'];
                }

                $temp_activities []= $activity;
            }
        }

        // because we can't use usort in On-demand...
        // add a key based on timestamp to temp array
        // if multiple timestamps, add seconds until available.
        foreach ($temp_activities as $key => $activity)
        {

            // SugarChimp_Helper::log('debug',"SugarChimpAPI->dashletPersonRecordData set new key for activity:".print_r($activity,true));
            $time = strtotime($activity['timestamp']);
            $new_key = $time;
            
            while(isset($temp_activities[$new_key]))
            {
                SugarChimp_Helper::log('warning',"SugarChimpAPI->dashletPersonRecordData timestamp key already used: ".print_r($time,true));
                $compare_to_time = strtotime($temp_activities[$new_key]['timestamp']);
                if($time < $compare_to_time)
                {
                    SugarChimp_Helper::log('debug',"SugarChimpAPI->dashletPersonRecordData activity occurred before other time, taking key and moving other activity down.".print_r($time,true));
                    //swap activity with current temp_activity[$new_key]
                    $replace_activity = $temp_activities[$new_key];
                    $temp_activities[$new_key] = $activity;
                    $activity = $replace_activity;
                    // do some data formatting
                    $temp_activities[$new_key]['date'] = SugarChimp_Helper::format_timestamp($time,'M j');
                    $temp_activities[$new_key]['time'] = SugarChimp_Helper::format_timestamp($time,'H:i');

                    $time = $compare_to_time;
                    SugarChimp_Helper::log('debug',"SugarChimpAPI->dashletPersonRecordData Now Searching to place activity: ".print_r($activity,true));
                }
                $new_key++;
            }
            
            // set the new key
            $temp_activities[$new_key] = $activity;
            
            // do some data formatting
            $temp_activities[$new_key]['date'] = SugarChimp_Helper::format_timestamp($time,'M j');
            $temp_activities[$new_key]['time'] = SugarChimp_Helper::format_timestamp($time,'H:i');
            
            // remove the old key reference
            unset($temp_activities[$key]);
        }

        // reverse sort the keys numerically
        krsort($temp_activities,SORT_NUMERIC);

        // remove the keys and only send values for sorting purposes once it hits javascript
        $temp_activities = array_values($temp_activities);

        $data = array();
        $data['lists'] = $lists;
        $data['activities'] = $temp_activities;
        $data['dataFetched'] = true;
        $data['success'] = true;
        
        return $data;
    }
    
    //    Jon 9/11/15: $args is an account_id
    //    post: return MailChimp activity data for all related people ordered by most recent
    //    Need to Catch a limit of 50 emails
    public function dashletAccountRecordData($api,$args)
    {
        $apikey = SugarChimp_Setting::retrieve('apikey');
        
        if(empty($apikey)) 
        {
            //todo: lang string
            return array(
                'success'=>false,
                'dataFetched'=>true,
                'message'=>'No API key was found. Please go back and enter your MailChimp API Key.');
        }

        // check for a valid SugarOutfitters license
        $license_check = SugarChimp_License::is_valid();
        if ($license_check !== true)
        {
            SugarChimp_Helper::log('fatal',"SugarChimpAPI->dashletPersonRecordData license check failed.");
            return array(
                'success'=>false,
                'dataFetched'=>true,
                'message'=>'  License Key is invalid. Contact support@sugarchimp.com for help.');
        }

        // make sure an email address exists
        if (empty($args['id']))
        {
            //todo: lang string
            SugarChimp_Helper::log('warning','SugarChimpApi->dashletAccountRecordData: Account_id was not found.');
            return array(
                'success'=>false,
                'dataFetched'=>true,
                'message'=>'The Account was not found.');
        }

        $id = $args['id'];
        $relatedBeans = SugarChimp_Helper::get_related_people_for_account($id, true);        

        if($relatedBeans ===false)
        {
            SugarChimp_Helper::log('warning','SugarChimpApi->dashletAccountRecordData: No Bean for this account.');
            return array('success'=>false,'message'=>'An Account is required to receive any MailChimp activity data.');
        }


        /*
        Either relatedBeans is empty
        There will not be any data to show
        */
        if(empty($relatedBeans))
        {
            SugarChimp_Helper::log('debug','Account_Related_Beans: none found');
            return array('success'=>true,'message'=>'No related People were found for this account.');
        }
        else
        {
            $emails = array();
            $lists = array();
            $names = array();

            //Get all the email addresses for related contacts, targets, and leads
            //Get all the Sugar names associated with those email addresses
            //Need these specifically later
            //Then get all the lists associated with those email_addresses
            foreach($relatedBeans as $bean)
            {
                if(!empty($bean->email1))
                {
                    $email = $bean->email1;

                    if(isset($names[$email]))
                    {
                        $names[$email] .= ", ".$bean->name;
                        SugarChimp_Helper::log('debug','SugarChimpApi->dashletAccountRecordData: email already found, appending name only: ' . print_r($names[$email],true));
                    }
                    else
                    {
                        $names[$email] = $bean->name;
                        $emails[] = array('email' => $email);
                        SugarChimp_Helper::log('debug','SugarChimpApi->dashletAccountRecordData: Account_Related_Bean process email: ' . $bean->email1);

                        // get the mc/sugar lists for the email address
                        $personLists = SugarChimp_Helper::get_mc_lists_for_email($email);
                        foreach($personLists as $mc_id => $list_data)
                        {
                            $lists[$mc_id] = $list_data;
                        }
                    }
                }
            }

            if (empty($emails))
            {
                $data = array();
                $data['success'] = false;
                $data['dataFetched'] = true;
                $data['message'] = 'There are no Email Addresses associated with this Account.';

                return $data;
            }

            // the contact is not related to any synced lists
            if (empty($lists))
            {
                $data = array();
                $data['success'] = false;
                $data['dataFetched'] = true;
                $data['message'] = 'There is no MailChimp data for these email addresses.';

                return $data;
            }
            

            // temp array to store activities in
            $temp_activities = array();

            // go through each list id we have and get the member's activity
            foreach ($lists as $mc_id => $list_data)
            {
                try {
                    $mc = new Mailchimp($apikey);
                    $activities = $mc->lists->memberActivity($mc_id,$emails);

                } catch(Exception $e) {
                    //jon todo - possibly run the 'mailchimp list no longer exists stuff here'
                    SugarChimp_Helper::log('fatal',"SugarChimpAPI->dashletAccountRecordData for list {$mc_id} error: ".$e->getMessage());
    
                    continue;
                }

                if (empty($activities['data'][0]['activity']))
                {
                    SugarChimp_Helper::log('debug',"SugarChimpAPI->dashletAccountRecordData No activities for list: {$mc_id}");
                    continue;
                }

                //Each activity has 'email' string and 'activity' array
                //Need to get them on the same level with email address inside activity
                foreach($activities['data'] as $activities_by_email)
                {
                    $email = $activities_by_email['email']['email'];

                    foreach($activities_by_email['activity'] as $activity)
                    {
                        if ($activity['action'] == 'unsub')
                        {
                            $activity['list_id'] = $mc_id;
                            $activity['list_name'] = $list_data['name'];
                            $activity['title'] = $list_data['name'];
                        }
                        $activity['email'] = $email;
                        $activity['name'] = $names[$email];
                        $temp_activities [] = $activity;
                    }
                }
            }

            // SugarChimp_Helper::log('debug','SugarChimpApi->dashletAccountRecordData: temp_activities1: ' . print_r($temp_activities, true));

            // because we can't use usort in On-demand...
            // add a key based on timestamp to temp array
            // if multiple timestamps, add seconds until available.
            foreach ($temp_activities as $key => $activity)
            {

                // SugarChimp_Helper::log('debug',"SugarChimpAPI->dashletAccountRecordData set new key for activity:".print_r($activity,true));
                $time = strtotime($activity['timestamp']);
                $new_key = $time;
                
                while(isset($temp_activities[$new_key]))
                {
                    SugarChimp_Helper::log('warning',"SugarChimpAPI->dashletAccountRecordData timestamp key already used: ".print_r($time,true));
                    $compare_to_time = strtotime($temp_activities[$new_key]['timestamp']);
                    if($time < $compare_to_time)
                    {
                        SugarChimp_Helper::log('debug',"SugarChimpAPI->dashletAccountRecordData activity occurred before other time, taking key and moving other activity down.".print_r($time,true));
                        //swap activity with current temp_activity[$new_key]
                        $replace_activity = $temp_activities[$new_key];
                        $temp_activities[$new_key] = $activity;
                        // do some data formatting
                        $temp_activities[$new_key]['date'] = SugarChimp_Helper::format_timestamp($time,'M j');
                        $temp_activities[$new_key]['time'] = SugarChimp_Helper::format_timestamp($time,'H:i');
                        $activity = $replace_activity;
                        $time = $compare_to_time;
                        SugarChimp_Helper::log('debug',"SugarChimpAPI->dashletAccountRecordData Now Searching to place activity: ".print_r($activity,true));
                    }
                    $new_key++;
                }
                
                // set the new key
                $temp_activities[$new_key] = $activity;
                
                // do some data formatting
                $temp_activities[$new_key]['date'] = SugarChimp_Helper::format_timestamp($time,'M j');
                $temp_activities[$new_key]['time'] = SugarChimp_Helper::format_timestamp($time,'H:i');
                
                // remove the old key reference
                unset($temp_activities[$key]);
            }

            // reverse sort the keys numerically
            krsort($temp_activities,SORT_NUMERIC);

            SugarChimp_Helper::log('debug',"SugarChimpAPI->dashletAccountRecordData all sorted activities from MailChimp: ".print_r($temp_activities,true));

            // remove the keys and only send values for sorting purposes once it hits javascript
            $temp_activities = array_values($temp_activities);

            $data = array();
            $data['lists'] = $lists;
            $data['activities'] = $temp_activities;
            $data['dataFetched'] = true;
            $data['success'] = true;
            
            return $data;
        }
    }

    public function getMappingOptions($api,$args)
    {
        if (empty($args['list_id']))
        {
            SugarChimp_Helper::log('warning','SugarChimpAPI->getMappingOptions list id is required.');
            return array('success'=>false,'message'=>'A list id is required.');
        }

        $list_id = $args['list_id'];
        
        // get mailchimp merge fields
        $merge_vars = SugarChimp_FieldMap::get_mc_list_merge_fields($list_id);
        
        if (empty($merge_vars))
        {
            return array('success'=>false,'message'=>'The list does not have any MailChimp fields.');
        }
        
        // get mailchimp merge fields, will return array of groups data, may be an empty array if no groups are on the list
        $groups = SugarChimp_FieldMap::get_mc_list_group_fields($list_id);
        
        // get mappings
        $modules = SugarChimp_FieldMap::get_supported_modules();
        
        if (empty($modules))
        {
            return array('success'=>false,'message'=>'No modules are supported.');
        }
        
        /* check for valid plan */
        // do not get parent related fields if not on ultimate
        $get_parent_related_fields = false;
        if (SugarChimp_FieldMap::is_related_field_mapping_supported() === true)
        {
            $get_parent_related_fields = true;
        }

        $mappings = array();
        foreach ($modules as $module)
        {
            // get available fields for each module
            // $mappings[$module]['available_fields'] = SugarChimp_Field::get_module_fields($module);
            $mappings[$module]['available_fields'] = SugarChimp_Field::get_available_fields($module,$get_parent_related_fields);

            // NOTE: this is here because when we added the ultimate edition, all parent-related field mappings were removed from the basic/professional edition
            // This used to be available to basic/pro editions because account_name is a default sugar field
            // we are manually adding this field to be mapped for basic/pro if it doesn't exist in get_available_fields
            if ($module == 'Contacts' && empty($mappings[$module]['available_fields']['account_name']))
            {
                $mappings[$module]['available_fields']['account_name'] = array(
                    'name' => 'account_name',
                    'vname' => 'LBL_ACCOUNT_NAME',
                    'label' => 'Account Name',
                );
            }

            // NOTE: this is here because when we added the ultimate edition, all parent-related field mappings were removed from the basic/professional edition
            // This used to be available to basic/pro editions because account_name is a default sugar field
            // we are manually adding this field to be mapped for basic/pro if it doesn't exist in get_available_fields
            if ($module == 'Contacts' && empty($mappings[$module]['available_fields']['account_id']))
            {
                $mappings[$module]['available_fields']['account_id'] = array(
                    'name' => 'account_id',
                    'vname' => 'LBL_ACCOUNT_ID',
                    'label' => 'Account ID',
                );
            }

            // NOTE: Requested by a Customer to be added, just Campaign name
            if ($module == 'Contacts' && empty($mappings[$module]['available_fields']['campaign_name']))
            {
                $mappings[$module]['available_fields']['account_id'] = array(
                    'name' => 'campaign_name',
                    'vname' => 'LBL_CAMPAIGN',
                    'label' => 'Campaign Name',
                );
            }

            // NOTE: Requested by a Customer to be added, just Campaign name
            if ($module == 'Leads' && empty($mappings[$module]['available_fields']['campaign_name']))
            {
                $mappings[$module]['available_fields']['account_id'] = array(
                    'name' => 'campaign_name',
                    'vname' => 'LBL_CAMPAIGN',
                    'label' => 'Campaign Name',
                );
            }

            // get available fields for each module
            $mappings[$module]['group_fields'] = SugarChimp_Field::get_available_fields($module,$get_parent_related_fields,array('type'=>array('enum','multienum')));

            // get mapping for module
            $temp_mapping = SugarChimp_FieldMap::get_field_mappings($list_id,$module);
            $temp_fields = array();
            if (!empty($temp_mapping))
            {
                foreach ($temp_mapping['fields'] as $field)
                {
                    $temp_fields[$field['mapping']['mailchimp_name']]= $field['mapping']['sugarcrm_name'];
                }
            }
            $mappings[$module]['mapping'] = $temp_fields;
        }
        
        // see if the MC list is set to sync with a Sugar list yet
        $sugar_target_list = BeanFactory::getBean('ProspectLists');
        $sugar_target_list->retrieve_by_string_fields(array('mailchimp_list_name_c' => $list_id));

        $sugar_target_list_id = false;
        $synced_with_sugar = false;
        if (!empty($sugar_target_list->id)) {
            $sugar_target_list_id = $sugar_target_list->id;
            $synced_with_sugar = true;
        }

        return array(
            'success' => true,
            'merge_vars' => $merge_vars,
            'groups' => $groups['categories'],
            'mappings' => $mappings,
            'modules' => $modules,
            'list_id' => $list_id,
            'synced_with_sugar' => $synced_with_sugar,
            'sugar_target_list_id' => $sugar_target_list_id,
        );
    }
    
    public function saveMappingOptions($api,$args)
    {
        if (empty($args['list_id']))
        {
            SugarChimp_Helper::log('warning','SugarChimpAPI::saveMappingOptions list_id is required.');
            return array('success'=>false,'message'=>'A list_id is required.');
        }

        $list_id = $args['list_id']; 
        SugarChimp_Helper::log('debug','SugarChimpAPI::saveMappingOptions list_id: '.print_r($list_id,true));

        $result = SugarChimp_Helper::queue_mailchimp_backup(array($list_id));
        SugarChimp_Helper::log('debug','SugarChimpAPI::saveMappingOptions queued mailchimp backup: '.print_r($result,true));

        $result = SugarChimp_Helper::queue_sugar_backup(array($list_id));
        SugarChimp_Helper::log('debug','SugarChimpAPI::saveMappingOptions queued sugar backup: '.print_r($result,true));

        // seed module_mappings with the mapped modules and fields array
        // if all fields were set to not be mapped, without this things will break
        // because below we need this array to have stuff in it to sync the email fields
        $supported_modules = SugarChimp_FieldMap::get_supported_modules();
        foreach ($supported_modules as $module)
        {
            // since email address mappings are not editable, we need to make sure they are set here
            // do it in the for loop for each module
            $module_mappings[$module]["email1"] = array(
                "mapping" => array(
                    "sugarcrm_name" => "email1",
                    "mailchimp_name" => "NEW-EMAIL", // using new email so it updates properly if email changes
                ),
                "type" => "Email",
            );


            // since mailchimp ratings mappings are not editable, we need to make sure they are set here
            // do it in the for loop for each module
            $module_mappings[$module]["mailchimp_rating_c"] = array(
                "mapping" => array(
                    "sugarcrm_name" => "mailchimp_rating_c",
                    "mailchimp_name" => "MEMBER_RATING", // using new email so it updates properly if email changes
                ),
                "type" => "Basic",
            );

        }
        
        foreach ($args as $key => $sugarcrm_name)
        {
            if (empty($sugarcrm_name)) continue;

            // key must begin with 'mapping|'
            if (strpos($key,'mapping|') !== 0) continue;
            
            // mapping|list_id|module|mailchimp_name
            $parts = explode('|',$key);
            $module = $parts[2];
            $mailchimp_name = $parts[3];
            $field_type = empty($parts[4]) ? 'Basic' : $parts[4];

            // can add more data params later as needed for custom field types
            // added initially to support the date field, param1 stores the date format from mailchimp            
            $param1 = empty($parts[5]) ? '' : $parts[5];
            
            $module_mappings[$module][$sugarcrm_name] = array(
                'mapping' => array(
                    'sugarcrm_name' => $sugarcrm_name,
                    'mailchimp_name' => $mailchimp_name,
                ),
                'type' => $field_type,
                'param1' => $param1,
            );
        }

        $results = array();
        foreach ($module_mappings as $module => $fields)
        {
            $mapping = array(
                'fields' => $fields
            );
            
            $result = SugarChimp_FieldMap::save_field_mappings($list_id,$module,$mapping);
            $results[$module] = $result;
        }
        
        SugarChimp_FieldMap::update_related_field_mappings();

        // if there's a sync action, try to queue the sync
        $sync_action = empty($args['sync_direction']) ? false : $args['sync_direction'];
        if (!empty($sync_action))
        {
            switch ($sync_action)
            {
                case "sugar2mc":
                    // sync sugar list to mailchimp
                    SugarChimp_Helper::log('debug','SugarChimpApi::saveMappingOptions attempt to queue sugar2mc sync');
                    if (empty($args['sugar_target_list_id']))
                    {
                        SugarChimp_Helper::log('fatal','SugarChimpApi::saveMappingOptions sugar target list id not available to setup sugar2mc sync');
                        break;
                    }

                    $target_list = BeanFactory::getBean('ProspectLists',$args['sugar_target_list_id']);

                    if (empty($target_list))
                    {
                        SugarChimp_Helper::log('fatal','SugarChimpApi::saveMappingOptions could not queue sugar2mc for target list id '.print_r($args['sugar_target_list_id'],true).' and list id '.print_r($list_id,true));
                        break;
                    }
            
                    SugarChimp_Helper::push_lists_to_mailchimp(array($target_list));

                    break;

                case "mc2sugar":
                    // sync mailchimp list to sugar
                    SugarChimp_Helper::log('debug','SugarChimpApi::saveMappingOptions attempt to queue mc2sugar sync');

                    $job = array('name' => 'QueueMailchimpToSugarSync');

                    SugarChimp_FieldMap::queue_jobs(array($job),array($list_id));
                    break;
            }
        }
        else
        {
            SugarChimp_Helper::log('debug','Re-sync is not being queued for this field mapping save: '.print_r($list_id,true));
        }

        return array('success'=>true,'results'=>$results);
    }
    
    public function getAdminData($api,$args)
    {
        global $db;
        
        // we don't want to filter on lists, so give us any person in the system that matches the email
        $sql = "SELECT id,name,date_entered,date_modified,modified_user_id,created_by,description,deleted,mailchimp_list_id,param1,param2,param3,param4,param5
                FROM sugarchimp
                ORDER BY date_entered ASC";
                
        SugarChimp_Helper::log('debug','SugarChimpApi::getAdminData sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The query failed: '.$sql,
            );
        }

        $records = array();

        while ($row = $db->fetchByAssoc($result))
        {
            $records []= $row;
        }

        if (empty($records))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The table is empty.',
            );
        }
        
        return array(
            'success' => true,
            'data' => $records,
            'message' => 'There are '.count($records).' records in the sugarchimp table.',
        );
    }

    public function getAdminBackupData($api,$args)
    {
        global $db;
        
        // we don't want to filter on lists, so give us any person in the system that matches the email
        $sql = "SELECT id,name,mailchimp_list_id,data,date_entered,date_modified,deleted
                FROM sugarchimp_backup_queue
                ORDER BY date_entered ASC";
                
        SugarChimp_Helper::log('debug','SugarChimpApi::getAdminBackupData sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The query failed: '.$sql,
            );
        }

        $records = array();

        while ($row = $db->fetchByAssoc($result))
        {
            $records []= $row;
        }

        if (empty($records))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The table is empty.',
            );
        }
        
        return array(
            'success' => true,
            'data' => $records,
            'message' => 'There are '.count($records).' records in the sugarchimp_backup_queue table.',
        );
    }

    public function getAdminSugarBackupData($api,$args)
    {
        global $db;
        
        // we don't want to filter on lists, so give us any person in the system that matches the email
        $sql = "SELECT id,record,module,data,date_entered
                FROM sugarchimp_sugar_backup
                ORDER BY date_entered ASC";
                
        SugarChimp_Helper::log('debug','SugarChimpApi::getAdminSugarBackupData sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The query failed: '.$sql,
            );
        }

        $records = array();

        while ($row = $db->fetchByAssoc($result))
        {
            $records []= $row;
        }

        if (empty($records))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The table is empty.',
            );
        }
        
        return array(
            'success' => true,
            'data' => $records,
            'message' => 'There are '.count($records).' records in the sugarchimp_sugar_backup table.',
        );
    }

    public function getAdminMailchimpBackupData($api,$args)
    {
        global $db;
        
        // we don't want to filter on lists, so give us any person in the system that matches the email
        $sql = "SELECT id,mailchimp_list_id,data,date_entered,date_modified,deleted
                FROM sugarchimp_mailchimp_backup
                ORDER BY date_entered ASC";
                
        SugarChimp_Helper::log('debug','SugarChimpApi::getAdminMailchimpBackupData sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The query failed: '.$sql,
            );
        }

        $records = array();

        while ($row = $db->fetchByAssoc($result))
        {
            $records []= $row;
        }

        if (empty($records))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The table is empty.',
            );
        }
        
        return array(
            'success' => true,
            'data' => $records,
            'message' => 'There are '.count($records).' records in the sugarchimp_mailchimp_backup table.',
        );
    }

    public function getAdminmcData($api,$args)
    {
        global $db;
        
        // we don't want to filter on lists, so give us any person in the system that matches the email
        $sql = "SELECT id,mailchimp_list_id,data,date_entered,date_modified,deleted
                FROM sugarchimp_mc
                ORDER BY date_entered ASC";
                
        SugarChimp_Helper::log('debug','SugarChimpApi::getAdminmcData sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The query failed: '.$sql,
            );
        }

        $records = array();

        while ($row = $db->fetchByAssoc($result))
        {
            $records []= $row;
        }

        if (empty($records))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The table is empty.',
            );
        }
        
        return array(
            'success' => true,
            'data' => $records,
            'message' => 'There are '.count($records).' records in the sugarchimp_mc table.',
        );
    }

    public function getAdminactivitytrackerData($api,$args)
    {
        global $db;
        
        // we don't want to filter on lists, so give us any person in the system that matches the email
        $sql = "SELECT id,mailchimp_list_id,mailchimp_campaign_id,initial_sync,start_date,last_sync
                FROM sugarchimp_activity_tracker";
                
        SugarChimp_Helper::log('debug','SugarChimpApi::getAdminactivitytrackerData sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The query failed: '.$sql,
            );
        }

        $records = array();

        while ($row = $db->fetchByAssoc($result))
        {
            $records []= $row;
        }

        if (empty($records))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The table is empty.',
            );
        }
        
        return array(
            'success' => true,
            'data' => $records,
            'message' => 'There are '.count($records).' records in the sugarchimp_activity_tracker table.',
        );
    }

    public function getAdminactivitiesmcData($api,$args)
    {
        global $db;

        // we don't want to filter on lists, so give us any person in the system that matches the email
        $sql = "SELECT id,mailchimp_campaign_id,mailchimp_list_id,include_empty,data,date_entered,date_modified,deleted
                FROM sugarchimp_mc_activity";

        SugarChimp_Helper::log('debug','SugarChimpApi::getAdminactivitiesmcData sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The query failed: '.$sql,
            );
        }

        $records = array();

        while ($row = $db->fetchByAssoc($result))
        {
            $records []= $row;
        }

        if (empty($records))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The table is empty.',
            );
        }

        return array(
            'success' => true,
            'data' => $records,
            'message' => 'There are '.count($records).' records in the sugarchimp_mc_activity table.',
        );
    }

    public function getAdminmcpeopleData($api,$args)
    {
        global $db;

        // we don't want to filter on lists, so give us any person in the system that matches the email
        $sql = "SELECT id,mailchimp_list_id,data,date_entered,date_modified,deleted
                FROM sugarchimp_mc_people";

        SugarChimp_Helper::log('debug','SugarChimpApi::getAdminmcpeopleData sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The query failed: '.$sql,
            );
        }

        $records = array();

        while ($row = $db->fetchByAssoc($result))
        {
            $records []= $row;
        }

        if (empty($records))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The table is empty.',
            );
        }

        return array(
            'success' => true,
            'data' => $records,
            'message' => 'There are '.count($records).' records in the sugarchimp_mc_people table.',
        );
    }

    public function getAdminmclistData($api,$args)
    {
        global $db;

        $sql = "SELECT id,name,mailchimp_list_id,optout_tracker_processed,optout_tracker_offset,deleted_from_mailchimp,deleted,date_entered,date_modified,modified_user_id,created_by,description,last_cleaned_email_check
                FROM sugarchimp_mc_list";
    
        SugarChimp_Helper::log('debug','SugarChimpApi::getAdminmclistData sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The query failed: '.$sql,
            );
        }

        $records = array();

        while ($row = $db->fetchByAssoc($result))
        {
            $records []= $row;
        }

        if (empty($records))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The table is empty.',
            );
        }

        return array(
            'success' => true,
            'data' => $records,
            'message' => 'There are '.count($records).' records in the sugarchimp_mc_list table.',
        );
    }

    public function getAdminmccleanedemailData($api,$args)
    {
        global $db;

        $sql = "SELECT id,mailchimp_list_id,email,date_entered
                FROM sugarchimp_mc_cleaned_email";

        SugarChimp_Helper::log('debug','SugarChimpApi::getAdminmccleanedemailData sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The query failed: '.$sql,
            );
        }

        $records = array();

        while ($row = $db->fetchByAssoc($result))
        {
            $records []= $row;
        }

        if (empty($records))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The table is empty.',
            );
        }

        return array(
            'success' => true,
            'data' => $records,
            'message' => 'There are '.count($records).' records in the sugarchimp_mc_cleaned_email table.',
        );
    }

    public function getAdminactivitiesData($api,$args)
    {
        global $db;

        // we don't want to filter on lists, so give us any person in the system that matches the email
        $sql = "SELECT id,mailchimp_campaign_id,mailchimp_list_id,include_empty,description,date_entered,date_modified,deleted
                FROM sugarchimpactivity";

        SugarChimp_Helper::log('debug','SugarChimpApi::getAdminactivitiesData sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The query failed: '.$sql,
            );
        }

        $records = array();

        while ($row = $db->fetchByAssoc($result))
        {
            $records []= $row;
        }

        if (empty($records))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The table is empty.',
            );
        }

        return array(
            'success' => true,
            'data' => $records,
            'message' => 'There are '.count($records).' records in the sugarchimpactivity table.',
        );
    }

    public function getAdminOptoutTrackerData($api,$args)
    {
        global $db;
        
        // we don't want to filter on lists, so give us any person in the system that matches the email
        $sql = "SELECT id,name,date_entered,date_modified,modified_user_id,created_by,description,deleted,email,mailchimp_list_id
                FROM sugarchimp_optout_tracker
                ORDER BY date_entered DESC";

        SugarChimp_Helper::log('debug','SugarChimpApi::getAdminOptoutTrackerData sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The query failed: '.$sql,
            );
        }

        $records = array();

        while ($row = $db->fetchByAssoc($result))
        {
            $records []= $row;
        }

        if (empty($records))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The table is empty.',
            );
        }
        
        return array(
            'success' => true,
            'data' => $records,
            'message' => 'There are '.count($records).' records in the sugarchimp_optout_tracker table.',
        );
    }

    public function getAdminBatchesData($api,$args)
    {
        global $db;
        
        // we don't want to filter on lists, so give us any person in the system that matches the email
        $sql = "SELECT id,batch_id,date_entered,mailchimp_list_id
                FROM sugarchimp_batches
                ORDER BY date_entered DESC";
                
        SugarChimp_Helper::log('debug','SugarChimpApi::getAdminBatchesData sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The query failed: '.$sql,
            );
        }

        $records = array();

        while ($row = $db->fetchByAssoc($result))
        {
            $records []= $row;
        }

        if (empty($records))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The table is empty.',
            );
        }
        
        return array(
            'success' => true,
            'data' => $records,
            'message' => 'There are '.count($records).' records in the sugarchimp table.',
        );
    }


    public function getBatchData($api,$args)
    {
        SugarChimp_Logger::log('debug','SugarChimpAPI::getBatchData - args: ' . print_r($args,true));
        if (empty($args['batch_id']))
        {
            SugarChimp_Logger::log('warning','SugarChimpAPI::getBatchData - no batch_id found. ');

            return array(
                'success' => false,
                'message' => 'No Batch_ID provided.',
            );
        }
        $data = MailChimp_API::get_batch($args['batch_id']);
        
        return array(
            'success' => true,
            'batch_info' => $data,
        );
    }
    public function getAdminSmartlistQueueData($api,$args)
    {
        global $db;
        
        // we don't want to filter on lists, so give us any person in the system that matches the email
        $sql = "SELECT id,module,record,date_entered,date_modified,deleted,name,modified_user_id,created_by,description
                FROM smartlist_queue
                ORDER BY date_entered DESC";

        SugarChimp_Helper::log('debug','SugarChimpApi::getAdminSmartlistQueueData sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The query failed: '.$sql,
            );
        }

        $records = array();

        while ($row = $db->fetchByAssoc($result))
        {
            $records []= $row;
        }

        if (empty($records))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The table is empty.',
            );
        }
        
        return array(
            'success' => true,
            'data' => $records,
            'message' => 'There are '.count($records).' records in the smartlist_queue table.',
        );
    }

    public function getAdminSmartlistData($api,$args)
    {
        global $db;
        
        // we don't want to filter on lists, so give us any person in the system that matches the email
        $sql = "SELECT id,prospect_list_id,active,type,metadata,query,date_entered,date_modified,deleted,name,modified_user_id,created_by,description
                FROM smartlist
                ORDER BY date_entered ASC";

        SugarChimp_Helper::log('debug','SugarChimpApi::getAdminSmartlistData sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The query failed: '.$sql,
            );
        }

        $records = array();

        while ($row = $db->fetchByAssoc($result))
        {
            $records []= $row;
        }

        if (empty($records))
        {
            return array(
                'success' => true,
                'data' => array(),
                'message' => 'The table is empty.',
            );
        }
        
        return array(
            'success' => true,
            'data' => $records,
            'message' => 'There are '.count($records).' records in the smartlist table.',
        );
    }

    public function emptyTable($api,$args)
    {
        global $db;
        
        if (empty($args['table_name']))
        {
            SugarChimp_Helper::log('warning','SugarChimpAPI->emptyTable table_name is required.');
            return array('success'=>false,'message'=>'A table name is required.');
        }
        
        if(!empty($args['where']))
        {
            $where = $args['where']; 
        }

        $table_name = $args['table_name'];
        
        $valid_tables = array(
            'sugarchimp',
            'sugarchimp_mc',
            'sugarchimp_activity_tracker',
            'sugarchimpactivity',
            'sugarchimpactivity_contacts',
            'sugarchimpactivity_leads',
            'sugarchimpactivity_accounts',
            'sugarchimpactivity_prospects',
            'sugarchimp_mc_activity',
            'sugarchimp_backup_queue',
            'sugarchimp_sugar_backup',
            'sugarchimp_mailchimp_backup',
            'sugarchimp_mc_people',
            'smartlist',
            'smartlist_queue',
            'sugarchimp_optout_tracker',
            'sugarchimp_mc_list',
            'sugarchimp_batches',
            'sugarchimp_lock',
            'sugarchimp_mc_cleaned_email',
        );
        
        if (!in_array($table_name,$valid_tables))
        {
            SugarChimp_Helper::log('warning','SugarChimpAPI->emptyTable You cannot empty that table: '.$table_name);
            return array('success'=>false,'message'=>'You cannot empty that table: '.$table_name);
        }
        if(empty($where))
        {
            $sql = "TRUNCATE TABLE {$table_name}";
        }
        else
        {
            $sql = "DELETE FROM {$table_name} WHERE ".$db->quote($where);
        }

        SugarChimp_Helper::log('debug','SugarChimpApi::emptyTable sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            return array(
                'success' => false,
                'message' => 'The query failed: '.$sql,
            );
        }

        return array(
            'success' => true,
            'message' => 'The table was emptied: '.$table_name,
        );
    }

    public function countTable($api,$args)
    {
        global $db;
        
        if (empty($args['table_name']))
        {
            SugarChimp_Helper::log('warning','SugarChimpAPI->emptyTable table_name is required.');
            return array('success'=>false,'message'=>'A table name is required.');
        }
        
        $table_name = $args['table_name'];
        
        $valid_tables = array(
            'sugarchimp',
            'sugarchimp_mc',
            'sugarchimp_activity_tracker',
            'sugarchimpactivity',
            'sugarchimp_mc_activity',
            'sugarchimp_backup_queue',
            'sugarchimp_sugar_backup',
            'sugarchimp_mailchimp_backup',
            'sugarchimp_mc_people',
            'smartlist',
            'smartlist_queue',
            'sugarchimp_optout_tracker',
            'sugarchimp_mc_list',
            'sugarchimp_batches',
            'sugarchimp_lock',
            'sugarchimp_mc_cleaned_email',
        );
        
        if (!in_array($table_name,$valid_tables))
        {
            SugarChimp_Helper::log('warning','SugarChimpAPI->countTable You cannot count that table: '.$table_name);
            return array('success'=>false,'message'=>'You cannot empty that table: '.$table_name);
        }
        
        $sql = "SELECT count(id) as records FROM ".$db->quote($table_name);
        
        SugarChimp_Helper::log('debug','SugarChimpApi::countTable sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            return array(
                'success' => false,
                'message' => 'The query failed: '.$sql,
            );
        }
        $row = $db->fetchByAssoc($result);

        $num = !empty($row['records']) ? $row['records'] : 0;

        return array(
            'success' => true,
            'message' => 'The table has : '.$num.' records',
        );
    }

    public function removeRecord($api,$args)
    {
        global $db;
        
        if (empty($args['table_name']))
        {
            SugarChimp_Helper::log('warning','SugarChimpAPI->removeRecord table_name is required.');
            return array('success'=>false,'message'=>'A table name is required.');
        }
        
        if (empty($args['record']))
        {
            SugarChimp_Helper::log('warning','SugarChimpAPI->removeRecord record is required.');
            return array('success'=>false,'message'=>'A record id is required.');
        }
        
        $table_name = $args['table_name'];
        $record = $args['record'];
        
        $valid_tables = array(
            'sugarchimp',
            'sugarchimp_mc',
            'sugarchimp_activity_tracker',
            'sugarchimpactivity',
            'sugarchimp_backup_queue',
            'sugarchimp_sugar_backup',
            'sugarchimp_mailchimp_backup',
            'sugarchimp_mc_people',
            'smartlist',
            'smartlist_queue',
        );
        
        if (!in_array($table_name,$valid_tables))
        {
            SugarChimp_Helper::log('warning','SugarChimpAPI->removeRecord You cannot empty that table: '.$table_name);
            return array('success'=>false,'message'=>'You cannot empty that table: '.$table_name);
        }
        
        // we don't want to filter on lists, so give us any person in the system that matches the email
        $sql = "DELETE FROM ".$db->quote($table_name)." WHERE id='".$db->quote($record)."'";
                
        SugarChimp_Helper::log('debug','SugarChimpApi::removeRecord sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            return array(
                'success' => false,
                'message' => 'The query failed: '.$sql,
            );
        }

        return array(
            'success' => true,
            'message' => 'The record was removed: '.$table_name,
        );
    }
    
    public function updateSetting($api,$args)
    {
        if (empty($args['key']))
        {
            SugarChimp_Helper::log('fatal','SugarChimpAPI->updateSetting a key is required to update the setting.');
            return array('success'=>false,'message'=>'A key is required to update a setting.');
        }
        
        if (empty($args['value']))
        {
            $args['value'] = '';
            SugarChimp_Helper::log('warning','SugarChimpAPI->updateSetting value is empty. will set '.$key.' to empty value.');
        }
        
        $key = $args['key'];
        $value = $args['value'];
        
        $result = SugarChimp_Setting::set($key,$value);

        if ($result !== true)
        {
            return array(
                'success' => false,
                'message' => 'The setting could not be saved.',
            );
        }

        return array(
            'success' => true,
            'message' => 'The setting was successfully updated.',
        );
    }

    public function getSetting($api,$args)
    {
        if (empty($args['key']))
        {
            SugarChimp_Helper::log('fatal','SugarChimpAPI->updateSetting a key is required to update the setting.');
            return array('success'=>false,'message'=>'A key is required to update a setting.');
        }
        
        $key = $args['key'];
        
        require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');
        $result = SugarChimp_Setting::retrieve($key);

        if ($result === false)
        {
            return array(
                'success' => false,
                'message' => 'The setting could not be retrieved.',
                'value' => $result,
            );
        }

        return array(
            'success' => true,
            'message' => 'The setting was retrieved successfully.',
            'value' => $result,
        );
    }

    public function getMailChimpCampaign($api, array $args)
    {
        if (empty($args['id']))
        {
            return array('success'=>false,'message'=>'No Campaign ID was provided.');
        }

        $campaign_id = $args['id'];

        try 
        {
            $result = MailChimp_API::get_campaign($campaign_id);
        } 
        catch(Exception $e) 
        {
            SugarChimp_Helper::log('warning',"SugarChimp Error: ".$e->getMessage());
            
            //want to pass the error message so return 200
            return array('success'=>false,'message'=>$e->getMessage());
        }

        return array('success'=> true, 'result'=>$result);
    }
    
    public function getMailChimpReport($api, array $args)
    {
        if (empty($args['id']))
        {
            return array('success'=>false,'message'=>'No Campaign/Report ID was provided.');
        }

        $process_stats = true;
        if (!empty($_REQUEST['process_stats']))
        {
            // stat calculations and formatting will be done automatically unless this is set
            $process_stats = false;
        }
        
        $campaign_id = $args['id'];

        try 
        {
            $result = MailChimp_API::get_report($campaign_id);
        } 
        catch(Exception $e) 
        {
            SugarChimp_Helper::log('warning',"SugarChimp Error: ".$e->getMessage());
            
            //want to pass the error message so return 200
            return array('success'=>false,'message'=>$e->getMessage());
        }

        if ($process_stats === true)
        {
            $result = SugarChimp_Campaign::process_stats($result);
        }

        return array('success'=> true, 'result'=>$result);
    }
    
    public function getMailChimpCampaigns($api, array $args)
    {
        $apikey = SugarChimp_Setting::retrieve('apikey');
        
        if(empty($apikey)) 
        {
            //todo: lang string
            return array('success'=>false,'message'=>'No API key was found. Please go back and enter your MailChimp API Key.');
        }

        try 
        {
            // $mc = new Mailchimp($apikey);
            // $result = $mc->campaigns->getList(); //MC api limit of 100
            // todo: support getting more than 100 campaign MC limit
            // would need to get numbers of campaigns and loop through calls until all are retrieved
            $result = MailChimp_API::get_campaigns(array('sort_field'=>'send_time','sort_dir'=>'DESC','fields'=>'campaigns.id,campaigns.settings.title,campaigns.status','count'=>2000));
        }
        catch(Exception $e) 
        {
            SugarChimp_Helper::log('warning',"SugarChimp Error: ".$e->getMessage());
            
            //want to pass the error message so return 200
            return array('success'=>false,'message'=>$e->getMessage());
        }

        return array('success'=> true, 'result'=>$result);
    }

    public function resetSugarChimp($api, array $args)
    {
        $apikey = SugarChimp_Setting::retrieve('apikey');
        
        if(empty($apikey)) 
        {
            //todo: lang string
            return array('success'=>false,'message'=>'No API key was found. Please go back and enter your MailChimp API Key.');
        }

        global $db;
        
        // empty the sugarchimp table
        $sql = "DELETE FROM sugarchimp";
        SugarChimp_Helper::log('debug','SugarChimpApi::resetSugarChimp sql: '.$sql);
        $db->query($sql);
        
        $sql = "DELETE FROM sugarchimp_mc";
        SugarChimp_Helper::log('debug','SugarChimpApi::resetSugarChimp sql: '.$sql);
        $db->query($sql);
        
        $sql = "DELETE FROM sugarchimp_mc_activity";
        SugarChimp_Helper::log('debug','SugarChimpApi::resetSugarChimp sql: '.$sql);
        $db->query($sql);
        
        $sql = "DELETE FROM sugarchimp_mc_people";
        SugarChimp_Helper::log('debug','SugarChimpApi::resetSugarChimp sql: '.$sql);
        $db->query($sql);
        
        // unset all synced target lists
        $sql = "UPDATE prospect_lists SET mailchimp_list_name_c = NULL";
        SugarChimp_Helper::log('debug','SugarChimpApi::resetSugarChimp sql: '.$sql);
        $db->query($sql);

        // delete sugarchimp runs from the job queue
        $sql = "DELETE FROM job_queue WHERE name='SugarChimp'";
        SugarChimp_Helper::log('debug','SugarChimpApi::resetSugarChimp sql: '.$sql);
        $db->query($sql);        

        // delete MC to sugar runs
        $sql = "DELETE FROM job_queue WHERE target='class::MailChimpToSugarCRM'";
        SugarChimp_Helper::log('debug','SugarChimpApi::resetSugarChimp sql: '.$sql);
        $db->query($sql);        

        // delete MC activity to sugar runs
        $sql = "DELETE FROM job_queue WHERE target='class::MailChimpToSugarCRMActivity'";
        SugarChimp_Helper::log('debug','SugarChimpApi::resetSugarChimp sql: '.$sql);
        $db->query($sql);        

        return array('success'=> true);
    }

    public function getSugarChimpSchedulers($api, array $args)
    {
        global $db;
        
        $sugarchimp_records = array();
        $sql = "SELECT * FROM job_queue WHERE name='SugarChimp'";
        $sql = $db->limitQuery($sql,0,100,false,'',false);
        SugarChimp_Helper::log('debug','SugarChimpApi::getSugarChimpSchedulers sql: '.$sql);
        $result = $db->query($sql);

        while ($row = $db->fetchByAssoc($result))
        {
            $sugarchimp_records []= $row;
        }

        $mailchimp_to_sugar_records = array();
        $sql = "SELECT * FROM job_queue WHERE target='class::MailChimpToSugarCRM'";
        $sql = $db->limitQuery($sql,0,100,false,'',false);
        SugarChimp_Helper::log('debug','SugarChimpApi::getSugarChimpSchedulers sql: '.$sql);
        $result = $db->query($sql);

        while ($row = $db->fetchByAssoc($result))
        {
            $mailchimp_to_sugar_records []= $row;
        }

        $mailchimp_to_sugar_activity_records = array();
        $sql = "SELECT * FROM job_queue WHERE target='class::MailChimpToSugarCRMActivity'";
        $sql = $db->limitQuery($sql,0,100,false,'',false);
        SugarChimp_Helper::log('debug','SugarChimpApi::getSugarChimpSchedulers sql: '.$sql);
        $result = $db->query($sql);

        while ($row = $db->fetchByAssoc($result))
        {
            $mailchimp_to_sugar_activity_records []= $row;
        }

        return array(
            'success' => true,
            'data' => array(
                'sugarchimp_records' => $sugarchimp_records,
                'mailchimp_to_sugar_records' => $mailchimp_to_sugar_records,
                'mailchimp_to_sugar_activity_records' => $mailchimp_to_sugar_activity_records,
            ),
            'message' => 'Data loaded.',
        );
    }

    public function resetSugarChimpSchedulers($api, array $args)
    {
        global $db;

        // delete sugarchimp runs from the job queue
        $sql = "DELETE FROM job_queue WHERE name='SugarChimp'";
        SugarChimp_Helper::log('debug','SugarChimpApi::resetSugarChimp sql: '.$sql);
        $db->query($sql);        

        // delete MC to sugar runs
        $sql = "DELETE FROM job_queue WHERE target='class::MailChimpToSugarCRM'";
        SugarChimp_Helper::log('debug','SugarChimpApi::resetSugarChimp sql: '.$sql);
        $db->query($sql);        

        // delete MC activity to sugar runs
        $sql = "DELETE FROM job_queue WHERE target='class::MailChimpToSugarCRMActivity'";
        SugarChimp_Helper::log('debug','SugarChimpApi::resetSugarChimp sql: '.$sql);
        $db->query($sql);

        return array('success'=> true);
    }

    public function optInEveryone($api, array $args)
    {
        $apikey = SugarChimp_Setting::retrieve('apikey');
        
        if(empty($apikey)) 
        {
            //todo: lang string
            return array('success'=>false,'message'=>'No API key was found. Please go back and enter your MailChimp API Key.');
        }

        global $db;
        
        // empty the sugarchimp table
        $sql = "UPDATE email_addresses SET opt_out=0";
        SugarChimp_Helper::log('debug','SugarChimpApi::optInEveryone sql: '.$sql);
        $db->query($sql);

        return array('success'=> true);
    }

    public function optInEmails($api, array $args)
    {
        //add email addresses to be opted in
        $emails = array(
        );
        $apikey = SugarChimp_Setting::retrieve('apikey');
        
        if(empty($apikey)) 
        {
            //todo: lang string
            return array('success'=>false,'message'=>'No API key was found. Please go back and enter your MailChimp API Key.');
        }

        global $db;
        
        // opt in sql
        $sql = "UPDATE email_addresses SET opt_out=0 WHERE email_address IN ('".implode("','", $emails)."')";
        SugarChimp_Helper::log('debug','SugarChimpApi::optInEmails sql: '.$sql);
        $db->query($sql);

        return array('success'=> true);
    }

    public function killScheduler($api, array $args)
    {
        global $db;

        $apikey = SugarChimp_Setting::retrieve('apikey');
        
        if(empty($apikey)) 
        {
            return array('success'=>false,'message'=>'No API key was found. Please go back and enter your MailChimp API Key.');
        }

        // delete job sql
        $sql = "DELETE from job_queue WHERE name='sugarchimp' AND (status='queued'OR status='running')";
        SugarChimp_Helper::log('debug','SugarChimpApi::killScheduler sql: '. $sql);
        
        if (!$result = $db->query($sql))
        {
            return array(
                'success' => false,
                'message' => 'The query failed: '.$sql,
            );
        }

        return array(
            'success' => true,
            'message' => 'The record was removed: ',
        );

        return array('success'=> true);
    }

    public function queueSync($api, array $args)
    {
        $target_list_id = empty($args['target_list_id']) ? false : $args['target_list_id'];
        $sync_action = empty($args['sync_action']) ? false : $args['sync_action'];

        $target_list = BeanFactory::getBean('ProspectLists',$target_list_id);

        if (empty($target_list))
        {
            // error, no target list by id target_list_id
            return array(
                'success'=> false,
                'message'=>'Cannot queue sync. A valid Sugar Target List could not be found.',
            );
        }

        switch ($sync_action)
        {
            case "sugar2mc":
                // sync sugar list to mailchimp
                SugarChimp_Helper::push_lists_to_mailchimp(array($target_list));

                return array(
                    'success'=> true,
                    'message' => 'List has been successfully queued to re-sync.',
                );

                break;

            case "mc2sugar":
                // sync mailchimp list to sugar
                if (empty($target_list->mailchimp_list_name_c))
                {
                    // error, no MC list id with target list
                    return array(
                        'success'=> false,
                        'message'=>'Cannot perform MailChimp to Sugar sync. A valid MailChimp List ID is not available.',
                    );

                }

                $result = SugarChimp_Helper::queue_MailChimpToSugarCRM_job($target_list->mailchimp_list_name_c,false,false);

                if (empty($result))
                {
                    return array(
                        'success'=> false,
                        'message' => 'MailChimp to Sugar Syncing is Disabled. Contact SugarChimp Support to enable.',
                    );
                }

                return array(
                    'success'=> true,
                    'message' => 'List has been successfully queued to re-sync.',
                );

                break;
        }

        return array(
            'success'=> false,
            'message'=>'Sync was not queued. That action is not available.',
        );
    }

    public function smartlistSetup($api, array $args)
    {
        $apikey = SugarChimp_Setting::retrieve('apikey');

        if(empty($apikey)) {
            //todo: lang string
            return array('success'=>false,'message'=>'No API key was found. Please go back and enter your MailChimp API Key.');
        }

        // get all sugar target lists
        // get all mailchimp lists
        // which list are you wanting to setup?
        // Which option is smart list option is setup?
        // previous values for selection
        // -- filters
        // -- query
        // filter field setup

        // select all sugar lists
        $sugar_lists = SugarChimp_List::get_sugar_lists();

        $new_sugar_list = false;
        if (empty($sugar_lists['list']))
        {
            $default_empty_name = "Master SugarChimp List";

            // there are no Sugar Target Lists
            // in this case we create one for them behind the scenes to setup the smartlist filters
            // name is only required field for sugar target list
            $new_sugar_list = SugarChimp_List::create_sugar_list(array(
                'name' => !empty($args['name']) ? $args['name'] : $default_empty_name,
            ));

            if (empty($new_sugar_list))
            {
                SugarChimp_Helper::log('fatal','SugarChimpApi::smartlistSetup could not create sugar target list automatically.');
            }

            // get all the sugar lists again
            $sugar_lists = SugarChimp_List::get_sugar_lists();
        }

        // select all mailchimp lists
        $mailchimp_lists = SugarChimp_List::get_mailchimp_lists();

        $new_mailchimp_list = false;
        if (empty($mailchimp_lists['lists']))
        {
            // there are no MailChimp Lists
            // in this case we create one for them behind the scenes to setup the smartlist filters
            $new_mailchimp_list = SugarChimp_List::create_mailchimp_list(array(
                'name' => 'Master SugarChimp List', // MailChimp List From Sugar (created ' . date("F j, Y, g:i a") . ')',
            ));

            if (empty($new_mailchimp_list))
            {
                SugarChimp_Helper::log('fatal','SugarChimpApi::smartlistSetup could not create mailchimp list automatically.');
            }

            $mailchimp_lists = SugarChimp_List::get_mailchimp_lists();
        }

        $modules = SugarChimp_FieldMap::get_supported_modules();

        // get available fields for each syncable module
        $available_fields = SmartList_Field::get_all_available_fields($modules);
            
        // get the other details about the fields (operands, values, etc.)
        $fields_info = SmartList_Field::get_fields_info();

        // get all existing smartlists
        $smartlists = SmartList_List::get_smartlists();

        // add the found smartlists to the list of sugar lists
        if (!empty($smartlists) && !empty($sugar_lists['list']))
        {
            foreach ($smartlists as $smartlist_id => $smartlist)
            {
                if (empty($smartlist['prospect_list_id']) or empty($sugar_lists['list'])) continue;
                
                if (array_key_exists($smartlist['prospect_list_id'],$sugar_lists['list']))
                {
                    $sugar_lists['list'][$smartlist['prospect_list_id']]['smartlist'] = $smartlist;
                }
            }
        }

        $default_modules = SugarChimp::getMailChimpDefaultModules();

        $current_plan = SugarChimp_Helper::get_current_plan();

        return array(
            'sugar_lists' => $sugar_lists,
            'mailchimp_lists' => $mailchimp_lists,
            'new_sugar_list' => $new_sugar_list,
            'new_mailchimp_list' => $new_mailchimp_list,
            'available_fields' => $available_fields,
            'fields_info' => $fields_info,
            'sugarchimp_modules' => $default_modules,
            'current_plan' => $current_plan,
        );
    }

    // this function started as a copy of the smartlistSetup function
    // with the new v3 api the getLists response from mailchimp changed
    // so we need to change the get list call specifically for the dashlet so 
    // that it does not slow down the smartlist setup calls
    public function smartlistDashlet($api, array $args)
    {
        $apikey = SugarChimp_Setting::retrieve('apikey');

        if(empty($apikey)) {
            //todo: lang string
            return array('success'=>false,'message'=>'No API key was found. Please go back and enter your MailChimp API Key.');
        }

        $mailchimp_list_id = empty($args['mailchimp_list_id']) ? false : $args['mailchimp_list_id'];

        if(empty($mailchimp_list_id)) {
            //todo: lang string
            return array('success'=>false,'message'=>'The Sugar Target List is not synced to a MailChimp List.');
        }

        // get all sugar target lists
        // get all mailchimp lists
        // which list are you wanting to setup?
        // Which option is smart list option is setup?
        // previous values for selection
        // -- filters
        // -- query
        // filter field setup

        // select all sugar lists
        $sugar_lists = SugarChimp_List::get_sugar_lists();

        // select all mailchimp lists
        $mailchimp_list = MailChimp_API::get_list($mailchimp_list_id);

        if (empty($mailchimp_list['id']))
        {
            return array('success'=>false,'message'=>'Could not find that list in MailChimp.');
        }

        $mailchimp_lists = array($mailchimp_list['id'] => $mailchimp_list);

        $modules = SugarChimp_FieldMap::get_supported_modules();

        // get available fields for each syncable module
        $available_fields = SmartList_Field::get_all_available_fields($modules);
            
        // get the other details about the fields (operands, values, etc.)
        $fields_info = SmartList_Field::get_fields_info();

        // get all existing smartlists
        $smartlists = SmartList_List::get_smartlists();

        // add the found smartlists to the list of sugar lists
        if (!empty($smartlists) && !empty($sugar_lists['list']))
        {
            foreach ($smartlists as $smartlist_id => $smartlist)
            {
                if (empty($smartlist['prospect_list_id']) or empty($sugar_lists['list'])) continue;
                
                if (array_key_exists($smartlist['prospect_list_id'],$sugar_lists['list']))
                {
                    $sugar_lists['list'][$smartlist['prospect_list_id']]['smartlist'] = $smartlist;
                }
            }
        }

        $default_modules = SugarChimp::getMailChimpDefaultModules();

        $current_plan = SugarChimp_Helper::get_current_plan();

        return array(
            'sugar_lists' => $sugar_lists,
            'mailchimp_lists' => array('lists'=>$mailchimp_lists),
            'available_fields' => $available_fields,
            'fields_info' => $fields_info,
            'sugarchimp_modules' => $default_modules,
            'current_plan' => $current_plan,
        );
    }

    public function createSugarList($api, array $args)
    {
        // name is only required field for sugar target list
        if (empty($args['name']))
        {
            return array(
                'success'=> false,
                'message'=>'A name is required to create the Sugar Target List.',
            );
        }

        $list = SugarChimp_List::create_sugar_list(array(
            'name' => $args['name'],
        ));

        if (empty($list))
        {
            return array(
                'success'=> false,
                'message'=>'Sugar Target List failed to create.',
            );
        }

        return array(
            'success'=> true,
            'list' => $list,
            'message'=>'Sugar Target List created successfully',
        );
    }

    public function createMailChimpList($api, array $args)
    {
        // make sure required params are there
        // name is only required field, other fields have defaults
        if (empty($args['name']))
        {
            return array(
                'success'=> false,
                'message'=>'A name is required to create the Sugar Target List.',
            );
        }

        $list = SugarChimp_List::create_mailchimp_list(array(
            'name' => $args['name'],
        ));

        if (empty($list))
        {
            return array(
                'success'=> false,
                'message'=>'MailChimp List failed to create.',
            );
        }

        return array(
            'success'=> true,
            'list' => $list,
        );
    }

    public function smartlistSave($api, array $args)
    {
        SugarChimp_Helper::log('debug','SugarChimpApi::smartlistSave: Saving Smartlist, args: '.print_r($args,true));
        
        // before we can parse the form data provided, we must
        // decode values that might be encoded before proceeding with save
        $args['formData'] = html_entity_decode($args['formData'],ENT_QUOTES);
        SugarChimp_Helper::log('debug','SugarChimpApi::smartlistSave: args after decode: '.print_r($args,true));

        // we send as a GET request string, parse_str properly parses this 
        // jquery/javascript doesn't have a way to easily pass fields with names like smartlist[module][field][filters] etc.
        // field names with multiple brackets get weird, but passing as a GET request string and parsing it gets around this
        parse_str($args['formData'],$form_data);

        if (empty($form_data['smartlist-sugar-list-id']))
        {
            SugarChimp_Helper::log('fatal','SugarChimpApi::smartlistSave: No Sugar Target List ID was passed to the server. Looking for smartlist-sugar-list-id.');
            return array(
                'form_data' => $form_data,
                'success'=> false,
                'message' => "No Sugar Target List ID was passed to the server. Looking for 'smartlist-sugar-list-id'.",
            );
        }

        if (empty($form_data['smartlist-mailchimp-list-id']))
        {
            SugarChimp_Helper::log('fatal','SugarChimpApi::smartlistSave: No MailChimp List ID was passed to the server. Looking for smartlist-mailchimp-list-id.');
            return array(
                'form_data' => $form_data,
                'success'=> false,
                'message' => "No Sugar Target List ID was passed to the server. Looking for 'smartlist-mailchimp-list-id'.",
            );
        }

        if (empty($form_data['smartlist-metadata']) or !is_array($form_data['smartlist-metadata']))
        {
            SugarChimp_Helper::log('fatal','SugarChimpApi::smartlistSave: No SmartList metadata was passed to the server. Looking for smartlist-metadata.');
            return array(
                'form_data' => $form_data,
                'success'=> false,
                'message' => "No SmartList metadata data was passed to the server. Looking for 'smartlist-metadata'.",
            );
        }

        $metadata = $form_data['smartlist-metadata'];
        SugarChimp_Helper::log('debug','SugarChimpApi::smartlistSave: metadata: '.print_r($metadata,true));

        // need to determine if new or old smartlist record
        // either use newBean or getBean
        $sugar_list_id = $form_data['smartlist-sugar-list-id'];
        $mailchimp_list_id = $form_data['smartlist-mailchimp-list-id'];

        SugarChimp_Logger::log('debug','SugarChimpApi::smartlistSave: sugar list id '.print_r($sugar_list_id,true));
        SugarChimp_Logger::log('debug','SugarChimpApi::smartlistSave: mailchimp list id '.print_r($mailchimp_list_id,true));
        
        $sugar_list = BeanFactory::getBean('ProspectLists',$sugar_list_id);

        if (empty($sugar_list))
        {
            SugarChimp_Helper::log('fatal','SugarChimpApi::smartlistSave: Could not retrieve Sugar Target List by id '.$sugar_list_id);
            return array(
                'form_data' => $form_data,
                'success'=> false,
                'message' => "Could not find Sugar Target List: {$sugar_list_id}",
            );
        }

        // try to find sugar list by the provided mailchimp list id
        // if a sugar list already exists with this mailchimp list id
        // remove the mailchimp list id from that target list
        $synced_sugar_lists = SugarChimp_List::get_synced_sugar_lists("name","mailchimp_list_name_c='".$mailchimp_list_id."'");
        SugarChimp_Logger::log('debug','SugarChimpApi::smartlistSave: synced sugar lists '.print_r($synced_sugar_lists,true));

        if (!empty($synced_sugar_lists['list']) && is_array($synced_sugar_lists['list']))
        {
            foreach($synced_sugar_lists['list'] as $synced_sugar_list)
            {
                $synced_sugar_list_id = !empty($synced_sugar_list['id']) ? $synced_sugar_list['id'] : false;

                if (empty($synced_sugar_list_id)) 
                {
                    SugarChimp_Logger::log('debug','SugarChimpApi::smartlistSave: no sugar id for mc list '.print_r($mailchimp_list_id,true).' synced sugar list data: '.print_r($synced_sugar_list,true));
                    continue;
                }

                if ($synced_sugar_list_id == $sugar_list_id)
                {
                    // sanity check, make sure the target list found is not the same list already synced
                    // if it is, don't do anything
                    SugarChimp_Logger::log('debug','SugarChimpApi::smartlistSave: this is the same list being saved, carry on.');
                    continue;
                }

                $synced_sugar_list_bean = BeanFactory::getBean('ProspectLists',$synced_sugar_list_id);

                if (empty($synced_sugar_list_bean)) 
                {
                    SugarChimp_Logger::log('debug','SugarChimpApi::smartlistSave: could not remove link by mc list '.print_r($mailchimp_list_id,true).' synced sugar list data: '.print_r($synced_sugar_list,true));
                    continue;
                }

                $synced_sugar_list_bean->mailchimp_list_name_c = '';
                $synced_sugar_list_bean->save();

                SugarChimp_Logger::log('debug','SugarChimpApi::smartlistSave: removed link between sugar list '.print_r($synced_sugar_list_id,true).' and mc list '.print_r($mailchimp_list_id,true));
            }
        }

        // set the select sugar list to sync to the selected mailchimp list
        // this will queue a sugar to mailchimp sync via logic hook
        if ($sugar_list->mailchimp_list_name_c != $mailchimp_list_id)
        {
            SugarChimp_Logger::log('debug','SugarChimpApi::smartlistSave there is a new mailchimp_list_id, add it to target list');
            $sugar_list->mailchimp_list_name_c = $mailchimp_list_id;
        }

        if (empty($form_data['create-subscriber-as']))
        {
            SugarChimp_Logger::log('debug','SugarChimpApi::smartlistSave there is no create-subscriber-as value, getting default module for create subscriber as');
            $create_subscriber_as = SugarChimp_FieldMap::get_default_module();
            $sugar_list->mailchimp_default_module_c = $create_subscriber_as;
        }
        else if (SugarChimp_FieldMap::is_supported_module($form_data['create-subscriber-as']) === true)
        {
            SugarChimp_Logger::log('debug','SugarChimpApi::smartlistSave setting create subscriber as: '.print_r($form_data['create-subscriber-as'],true));
            $create_subscriber_as = $form_data['create-subscriber-as'];
            $sugar_list->mailchimp_default_module_c = $create_subscriber_as;
        }
        else
        {
            SugarChimp_Logger::log('debug','SugarChimpApi::smartlistSave create subscriber as is not a valid module, setting to default. '.print_r($form_data['create-subscriber-as'],true));
            $create_subscriber_as = SugarChimp_FieldMap::get_default_module();
            $sugar_list->mailchimp_default_module_c = $create_subscriber_as;
        }

        $sugar_list->save();

        // handle queueing mailchimp to sugar sync if checkbox checked
        // if smartlist-sync-from-mailchimp not empty, the checkbox to do MC to sugar was checked
        if (!empty($form_data['smartlist-sync-from-mailchimp']))
        {
            // queue MC to Sugar import job for mailchimp list
            // third param false tells it not to create a new target list
            SugarChimp_Logger::log('debug','SugarChimpApi::smartlistSave queueing MailChimpToSugarCRM job for mc list: '.print_r($mailchimp_list_id,true));
            SugarChimp_Helper::queue_MailChimpToSugarCRM_job($mailchimp_list_id,$create_subscriber_as,false);
        }

        // handle saving the smartlist record
        $smartlist = BeanFactory::newBean('SmartList');
        $existing_smartlist = $smartlist->get_smartlist_by_prospect_list_id($sugar_list_id);

        if (!empty($existing_smartlist))
        {
            // instead of using the new $smartlist object above
            // set $smartlist to be the previously saved smartlist object
            // this should prevent multiple uneeded records each time a smartlist setting changes
            SugarChimp_Helper::log('debug','SugarChimpApi::smartlistSave: A smartlist record exists for this target list, edit this one instead of creating a new one.');
            $smartlist = $existing_smartlist;
        }

        $smartlist->prospect_list_id = $sugar_list_id;
        $smartlist->metadata = $metadata;
        $smartlist->save();

        $smartlist->queue_checks();

        return array(
            'success'=> true,
            'metadata' => $smartlist->unpack_metadata($smartlist->metadata),
        );
    }
    public function testSQLQuery($api, array $args)
    {
        if (empty($args['sql']))
        {
            SugarChimp_Helper::log('fatal','SugarChimpApi::testSQLQuery: No SQL was provided.');
            return array(
                'success'=> false,
                'message' => "No query was provided to Test.",
            );
        }

        if (empty($args['sql_module']))
        {
            SugarChimp_Helper::log('fatal','SugarChimpApi::testSQLQuery: No module was provided.');
            return array(
                'success'=> false,
                'message' => "No module was provided to test against.",
            );
        }

        $valid_response = SmartList_Mode::is_valid_sql($args['sql']);
        if(!$valid_response['success'])
        {
            return $valid_response;
        }

        global $db;
        
        // before we can use the sql query provided, we must
        // decode values that might be encoded before proceeding with test
        $sql = html_entity_decode($args['sql'],ENT_QUOTES);
        $module = $args['sql_module'];
        
        // forge a sql mode object so that we can get us a test query
        $mode = SmartList_Mode::forge('sql',$module,array('initial_sql'=>$sql));
        
        $test_sql = $mode->get_test_sql();
        SugarChimp_Helper::log('debug','SugarChimpApi::testSQLQuery: test_sql: '.print_r($test_sql,true));

        if (empty($test_sql))
        {
            SugarChimp_Helper::log('fatal','SugarChimpApi::testSQLQuery: could not get a query to test against, test_sql: '.print_r($test_sql,true));
            return array(
                'success'=> false,
                'message' => "Could not generate a test query.",
            );
        }
        
        // query failed
        if (!$result = $db->query($test_sql))
        {         
            return array(
                    'success'=> false,
                    'message' => "Test failed, Your query returned error: " . $db->lastDbError(),
                );
        }

        $count = $db->getRowCount($result);

        return array(
            'success'=> true,
            'count'=> $count,
        );
    }

    public function testFiltersQuery($api, array $args)
    {
        SugarChimp_Helper::log('debug','SugarChimpApi::testFilterQuery: args: '.print_r($args,true));
        
        // before we can parse the form data provided, we must
        // decode values that might be encoded before proceeding with save
        $args['formData'] = html_entity_decode($args['formData'],ENT_QUOTES);
        SugarChimp_Helper::log('debug','SugarChimpApi::testFiltersQuery: args after decode: '.print_r($args,true));

        // we send as a GET request string, parse_str properly parses this 
        // jquery/javascript doesn't have a way to easily pass fields with names like smartlist[module][field][filters] etc.
        // field names with multiple brackets get weird, but passing as a GET request string and parsing it gets around this
        parse_str($args['formData'],$form_data);
        
        if (empty($args['module']))
        {
            SugarChimp_Helper::log('fatal','SugarChimpApi::testFilterQuery: No module was provided.');
            return array(
                'success'=> false,
                'message' => "No module was provided to Test the Query.",
            );
        }
        $module = $args['module'];
        if (empty($form_data['smartlist-metadata']) or !is_array($form_data['smartlist-metadata']))
        {
            SugarChimp_Helper::log('fatal','SugarChimpApi::testFilterQuery: No SmartList metadata was passed to the server. Looking for smartlist-metadata.');
            return array(
                'form_data' => $form_data,
                'success'=> false,
                'message' => "No SmartList metadata data was passed to the server. Looking for 'smartlist-metadata'.",
            );
        }
        global $db;

        $metadata = $form_data['smartlist-metadata'];
        SugarChimp_Helper::log('debug','SugarChimpApi::testFiltersQuery: metadata: '.print_r($metadata,true));

        if (empty($metadata[$module]['smart']['filters']))
        {
            SugarChimp_Helper::log('debug','SugarChimpApi::testFilterQuery: No Filters were passed to the server.');
            return array(
                'form_data' => $form_data,
                'success'=> false,
                'message' => 'Test failed. You do not have any filters. Click "Add Filter" to create a filter and try again.',
            );
        }

        $sql = SmartList_Filter::get_initial_sql($module,$metadata[$module]['smart'],true);
        $result = $db->query($sql);

        if (!$result)
        {
            // query failed
            SugarChimp_Helper::log('fatal','SugarChimpApi->testFilterQuery, Query failed: '.print_r($sql,true));
            return array(
                'form_data' => $form_data,
                'success'=> false,
                'message' => "Test failed, check your filters for incomplete data. Error: " + $db->lastDbError(),
            );
        }

        $row = $db->fetchByAssoc($result);
        if (!empty($row)) {
            $count = $row['number_records'];
        }
        else{
            $count = 0;
        }
        return array(
            'success'=> true,
            'count'=> $count,
        );
    }

    public function getCurrentPlan($api, array $args)
    {
        $plan = SugarChimp_Helper::get_current_plan();

        return array(
            'success' => true,
            'plan' => $plan,
        );
    }

    // more than the setting flag needs to change when enabling account syncing
    // we also need to set the field mapping and smartlist metadata properly
    public function enableAccountSyncing($api, array $args)
    {
        global $db;

        // check to make sure you're on pro or above
        $current_plan = SugarChimp_Helper::get_current_plan();

        if (empty($current_plan) || ($current_plan != 'professional' && $current_plan != 'ultimate'))
        {
            return array(
                'success'=>false,
                'log'=>array("You cannot use Account Syncing unless you are using SugarChimp Professional or SugarChimp Ultimate. Contact support@sugarchimp.com to learn how to try out the Professional Plan for free."),
            );
        }

        $success = true;
        $log = array();

        // set the account syncing flag to true
        SugarChimp_Setting::set('account_syncing_enabled',1);
        $log []= "Set account_syncing_enabled to true";

        // add default account mapping to all synced lists
        $default_account_mapping = SugarChimp_FieldMap::$default_account_mapping;
        $log []= "Adding accounts field mapping to existing lists";

        if (!empty($default_account_mapping) and is_array($default_account_mapping))
        {
            // get all mapping config names that looks somethign like 'asdfasdf|asdfasdf'
            $sql = "SELECT name AS name FROM config WHERE category='sugarchimp' AND name LIKE '%|%'";
            $result = $db->query($sql);
            $log []= "Get all configs with '%|%'. result: ".print_r($result,true);

            $list_ids = array();
            
            // go through each item we find, it'll be "{list_id}|{module_name}"
            // put list_id as the key, that holds an array of modules with mapings
            while ($row = $db->fetchByAssoc($result))
            {
                if (!empty($row['name']))
                {
                    $parts = explode("|",$row['name']);
                    if (!empty($parts[0]) and !empty($parts[1]))
                    {
                        if (!isset($list_ids[$parts[0]]))
                        {
                            $list_ids[$parts[0]] = array();
                        }

                        $list_ids[$parts[0]][$parts[1]]= $parts[1];
                    }
                }
                else
                {
                    $log []= "row did not contain anything. row: ".print_r($row,true);
                }
            }

            // go through each of the arrays, if there is not an account mapping for a list, add it
            if (!empty($list_ids) and is_array($list_ids))
            {
                foreach ($list_ids as $list_id => $mapped_modules)
                {
                    $has_account_mapping = false;
                    if (!empty($mapped_modules) and is_array($mapped_modules))
                    {
                        foreach ($mapped_modules as $mapped_module)
                        {
                            if ($mapped_module == 'Accounts')
                            {
                                $has_account_mapping = true;
                                break;
                            }
                        }
                    }
                    if ($has_account_mapping === false)
                    {
                        $log []= "Added default account field mapping for list: ".print_r($list_id,true);
                        // we need to add default account mapping
                        SugarChimp_FieldMap::save_field_mappings($list_id,'Accounts',$default_account_mapping);
                    }
                    else
                    {
                        $log []= "Account field mapping already existed for list. Nothing to do. list id: ".print_r($list_id,true);
                    }
                }
            }
            else
            {
                $log []= "There were no synced lists to process. Nothing to do.";
            }
        }
        else
        {
            // cannot set the default account mapping, default account mapping is empty
            $success = false;
            $log []= "ERROR: cannot set the default account mapping, default account mapping is empty";
        }

        // add accounts metadata to current smartlists
        // get all smartlists
        $sql = "SELECT id AS id FROM smartlist WHERE deleted=0";
        $result = $db->query($sql);

        require_once('modules/SmartList/SmartList.php');
        $default_account_metadata = SmartList::$default_account_metadata;
        $log []= "Adding accounts metadata to existing smartlists";

        if (!empty($default_account_metadata) and is_array($default_account_metadata))
        {
            while ($row = $db->fetchByAssoc($result))
            {
                if (!empty($row['id']))
                {
                    $smartlist = BeanFactory::getBean('SmartList',$row['id']);
                    if (empty($smartlist->metadata['Accounts']))
                    {
                        $log []= "Added accounts metadata to smartlist id: ".print_r($smartlist->id,true);
                        $smartlist->metadata['Accounts'] = $default_account_metadata;
                        $smartlist->save();
                    }
                    else
                    {
                        $log []= "Accounts metadata already exists for smartlist. nothing to do. id: ".print_r($smartlist->id,true);
                    }
                }
                else
                {
                    $log []= "There was an empty row. row: ".print_r($row,true);
                }
            }
        }
        else
        {
            // cannot set the default account metadata, default account metadata is empty
            $success = false;
            $log []= "ERROR: cannot set the default account metadata, default account metadata is empty";
        }

        return array(
            'success'=>$success,
            'log'=>$log
        );
    }
    
    // more than the setting flag needs to change when disabling account syncing
    // we also need to set the field mapping and smartlist metadata properly
    public function disableAccountSyncing($api, array $args)
    {
        global $db;

        $success = true;
        $log = array();

        // set the account syncing flag to false
        SugarChimp_Setting::set('account_syncing_enabled',0);
        $log []= "Set account_syncing_enabled flag to false";


        // update any target list with default module as Accounts, change to Contacts
        // first get the affected lists, so we can log them
        $sql = "SELECT id AS id FROM prospect_lists WHERE mailchimp_default_module_c='Accounts'";
        $result = $db->query($sql);
        $target_list_ids = array();
        while ($row = $db->fetchByAssoc($result))
        {
            $target_list_ids []= $row['id'];
            $log []= "Target List had 'Accounts' as default module. Changing to 'Contacts': ".print_r($row['id'],true);
        }

        if (!empty($target_list_ids) && is_array($target_list_ids))
        {
            $sql = "UPDATE prospect_lists SET mailchimp_default_module_c='Contacts' WHERE mailchimp_default_module_c='Accounts'";
            $result = $db->query($sql);
        }

        // remove account mappings
        // delete any sugarchimp config item with "%|Accounts"
        $sql = "DELETE FROM config WHERE category='sugarchimp' AND name LIKE '%|Accounts'";
        $result = $db->query($sql);

        $log []= "Delete all configs ending with '|Accounts'. result: ".print_r($result,true);

        // remove accounts metadata from current smartlists
        $sql = "SELECT id AS id FROM smartlist WHERE deleted=0";
        $result = $db->query($sql);

        $log []= "Get all smartlists. result: ".print_r($result,true);

        require_once('modules/SmartList/SmartList.php');

        while ($row = $db->fetchByAssoc($result))
        {
            if (!empty($row['id']))
            {
                $smartlist = BeanFactory::getBean('SmartList',$row['id']);
                if (!empty($smartlist->metadata['Accounts']))
                {
                    $log []= "Removed accounts metadata from smartlist id: ".print_r($smartlist->id,true);
                    unset($smartlist->metadata['Accounts']);
                    $smartlist->save();
                }
                else
                {
                    $log []= "There were no accounts metadata from smartlist, nothing to do. id: ".print_r($smartlist->id,true);
                }
            }
            else
            {
                $log []= "There was an empty row. row: ".print_r($row,true);
            }
        }

        return array(
            'success'=>$success,
            'log'=>$log
        );
    }

    public function removeListSyncing($api, array $args)
    {
        if(empty($args['target_list_id']))
        {
            SugarChimp_Helper::log('debug','SugarChimpApi::removeListSyncing: no Target List id was provided.');
            return array(
                'success'=>false,
                'message'=> "Must provide a Target List id.",
            );
        }
        
        $list_id = $args['target_list_id'];
        
        // get that Target List Bean
        $prospect_list_bean = BeanFactory::getBean('ProspectLists',$list_id);
        
        if (empty($prospect_list_bean->id)) {
            SugarChimp_Helper::log('debug',"SugarChimpApi::removeListSyncing - No Target List found with id: ".print_r($list_id,true));
            return array(
                'success'=>false,
                'message'=> "No Target List found with that ID.",
            );
        }
        
        if (empty($prospect_list_bean->mailchimp_list_name_c)) {
            SugarChimp_Helper::log('debug',"SugarChimpApi::removeListSyncing - Target List is not currently synced to a MailChimp List.");
            return array(
                'success'=>false,
                'message'=> "Target List is not currently synced to a MailChimp List.",
            );
        }
        
        // set its mailchimp_list_name_c to blank,
        // the logic_hook will do the rest of the cleanup of queues
        $prospect_list_bean->mailchimp_list_name_c = '';
        $prospect_list_bean->mailchimp_default_module_c = '';
        $prospect_list_bean->save();

        return array(
            'success'=>true,
            'message'=> "List Syncing removed from Target List - ".print_r($list_id,true),
        );
    }

    public function erase($api, array $args)
    {
        if (empty($args['email']))
        {
            SugarChimp_Helper::log('fatal','SugarChimpApi::erase: No email was provided.');
            return array(
                'success'=> false,
                'messages' => "No email was provided to erase.",
            );
        }

        // make sure email is a valid email
        if (filter_var($args['email'], FILTER_VALIDATE_EMAIL) === false)
        {
            return array(
                "success" => false,
                "messages" => "That is not a valid email address."
            );
        }

        require_once('modules/SugarChimp/includes/classes/SugarChimp/DataPrivacy.php');
        
        $result = SugarChimp_DataPrivacy::remove_by_email($args['email']);
        
        if (!empty($result['messages']) and is_array($result['messages']))
        {
            $result['messages'] = implode("<br>",$result['messages']);
        }

        return $result;
    }
}