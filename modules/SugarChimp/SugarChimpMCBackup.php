<?php







class SugarChimpMCBackup extends Basic 
{
    var $new_schema = true;
    var $module_dir = 'SugarChimp';
    var $object_name = 'SugarChimpMCBackup';
    var $table_name = 'sugarchimp_mailchimp_backup';

    var $id;
    var $name;
    var $mailchimp_list_id;
    var $data;
    
    function bean_implements($interface){
        switch($interface){
            case 'ACL': return true;
        }
        return false;
    }
}
