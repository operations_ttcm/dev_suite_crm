<?php







class SugarChimpBackupQueue extends Basic 
{
    var $new_schema = true;
    var $module_dir = 'SugarChimp';
    var $object_name = 'SugarChimpBackupQueue';
    var $table_name = 'sugarchimp_backup_queue';

    var $id;
    var $name;
    var $mailchimp_list_id;
    
    function bean_implements($interface){
        switch($interface){
            case 'ACL': return true;
        }
        return false;
    }
}