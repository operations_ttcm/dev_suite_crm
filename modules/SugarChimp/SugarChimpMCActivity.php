<?php







class SugarChimpMCActivity extends Basic 
{
    var $new_schema = true;
    var $module_dir = 'SugarChimp';
    var $object_name = 'SugarChimpMCActivity';
    var $table_name = 'sugarchimp_mc_activity';

    var $id;
    var $name;
    var $mailchimp_list_id;
    var $mailchimp_campaign_id;
    var $include_empty;
    var $data;
    
    function bean_implements($interface){
        switch($interface){
            case 'ACL': return true;
        }
        return false;
    }
}
