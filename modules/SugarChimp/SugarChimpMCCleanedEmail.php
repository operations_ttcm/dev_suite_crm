<?php







class SugarChimpMCCleanedEmail extends Basic 
{
    var $new_schema = true;
    var $module_dir = 'SugarChimp';
    var $object_name = 'SugarChimpMCCleanedEmail';
    var $table_name = 'sugarchimp_mc_cleaned_email';

    var $id;
    var $mailchimp_list_id;
    var $email;
    
    function bean_implements($interface){
        switch($interface){
            case 'ACL': return true;
        }
        return false;
    }
}
