




<div class="setup-body">
    <h1 class="tcenter">
        SugarChimp Activities to Sync Table
    </h1>
    <hr/>
    
    {if $message}
        <h3>{$message}</h3>
    {/if}
    
    {if count($data)>0}
        <table class="table status-table" id="sugarchimp-adminactivities">
            <tbody>
                <tr>
                    <td>id</td>
                    <td>mailchimp_campaign_id</td>
                    <td>mailchimp_list_id</td>
                    <td>include_empty</td>
                    <td>data</td>
                    <td>date_entered</td>
                    <td>date_modified</td>
                    <td>deleted</td>
                </tr>
                {foreach from=$data key=data_key item=data_item}
                    <tr>
                        <td>{$data_item.id}</td>
                        <td>{$data_item.mailchimp_campaign_id}</td>
                        <td>{$data_item.mailchimp_list_id}</td>
                        <td>{$data_item.include_empty}</td>
                        <td>{$data_item.data}</td>
                        <td>{$data_item.date_entered}</td>
                        <td>{$data_item.date_modified}</td>
                        <td>{$data_item.deleted}</td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    {else}
        <p>There is not data in the SugarChimp Activities to Sync table.</p>
    {/if}
</div>