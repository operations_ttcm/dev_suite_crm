




<link rel="stylesheet" href="modules/SugarChimp/includes/assets/css/sugarchimpactivity-dashlet.css?8.1.1a" />

{literal}
<script type="text/javascript">
$(document).ready(function(){
    {/literal}
    {if !empty($campaign_id)}
        get_campaign_details('{$campaign_id}');
    {else}
        get_campaign_details($('#sugarchimpactivity-campaign-selector').val());
    {/if}
    {literal}
    
    $('#sugarchimpactivity-campaign-selector').change(function(){
        get_campaign_details($(this).val());
    });
});

function get_campaign_details(campaign_id){
    if (!campaign_id){
        alert('You must select a campaign.');
    }
    
    $('.sugarchimpactivity-campaign-summary').html('');
    $('.sugarchimpactivity-loading').show();
    
    // get mapping
    $.ajax({
        url: 'index.php',
        method: 'post',
        dataType: 'json',
        data: {
            module: 'SugarChimp',
            action: 'get_campaign',
            campaign_id: campaign_id,
            to_pdf: 1,
        },
        success: function(data){
            $('.sugarchimpactivity-loading').hide();
            if (data && data.success && data.success == true){
                $('.sugarchimpactivity-campaign-summary').html(data.html);
            } else {
                if (data && data.message){
                    alert(data.message);
                } else {
                    alert('Campaign summary could not be loaded for the campaign. Check sugarcrm logs.');
                }
            }
        },
        error: function(data){
            $('.sugarchimpactivity-loading').hide();
            alert('Campaign summary could not be loaded for the campaign. Check sugarcrm logs.');
        },
    });
}
</script>
{/literal}

<div class="sugarchimpactivity-dashlet" style="max-width:600px">
    {foreach from=$errors key=error_key item=error_message}
        <div class="setup-section error">
            <p>{$error_message}</p>
        </div>
    {/foreach}
    <div class="sugarchimpactivity-loading">
        <img src="modules/SugarChimp/includes/assets/img/loading.gif" height="26" width="26" />
    </div>
    {if $campaigns}
        <div class="sugarchimpactivity-campaign-selector-div">
            <select id="sugarchimpactivity-campaign-selector">
                {foreach from=$campaigns key=campaign_key item=campaign}
                    <option value="{$campaign.id}" {if !empty($campaign_id) && $campaign_id == $campaign.id}selected="selected"{/if}>{$campaign.settings.title}</option>
                {/foreach}
            </select>
            <hr>
        </div>
        <div class="sugarchimpactivity-campaign-summary" style="clear:both;"></div>
    {else}
        <p>You have not sent any campaigns.</p>
    {/if}
</div>        
        
        
        
        
