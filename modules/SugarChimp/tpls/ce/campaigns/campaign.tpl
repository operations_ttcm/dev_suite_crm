




<h2>
     <span title="View" class="sugarchimpactivity-active-color">{$campaign.emails_sent}</span> Recipients
</h2>

<ul>
    <li>
         <strong>List: </strong> {$campaign.list_name}
    </li>
    <li>
         <strong>Subject: </strong> {$campaign.subject_line}
    </li>
    <li>
         <strong>Delivered: </strong> {$campaign.send_time_formatted}
    </li>
    {if $campaign.archive_url}
    <li>
         <a href="{$campaign.archive_url}" target="_new" class="sugarchimpactivity-active-color">View Email</a>
    </li>
    {/if}
</ul>

<h3>Open Rate: <span class="sugarchimpactivity-active-color">{$campaign.opens.open_rate_formatted}</span></h3>

<div class="sugarchimpactivity-meter">
    <span style="width:{$campaign.opens.open_rate_formatted};"></span>
</div>

<ul>
    <li>
         Industry avg (<span class="sugarchimpactivity-active-color">{$campaign.industry_stats.type}</span>) {$campaign.industry_stats.open_rate_formatted}
    </li>
</ul>

<h3>Click Rate: <span class="sugarchimpactivity-active-color">{$campaign.clicks.click_rate_formatted}</span></h3>

<div class="sugarchimpactivity-meter">
    <span style="width:{$campaign.clicks.click_rate_formatted};"></span>
</div>

<ul>
    <li>
         Industry avg (<span class="sugarchimpactivity-active-color">{$campaign.industry_stats.type}</span>) {$campaign.industry_stats.click_rate_formatted}
    </li>
</ul>

<div id="DIV_58" class="sugarchimpactivity-rounded-box">
    <div id="DIV_59">
        <h2 class="sugarchimpactivity-active-color">{$campaign.opens.unique_opens}</h2>
        <p>{if $campaign.opens.opens_total}Opens{else}Tracking disabled{/if}</p>
    </div>
    <div id="DIV_63">
        <h2 class="sugarchimpactivity-active-color">{$campaign.clicks.unique_clicks}</h2>
        <p>Clicked</p>
    </div>
    <div id="DIV_67">
        <h2>{$campaign.bounces.total_bounces}</h2>
        <p>Bounced</p>
    </div>
    <div id="DIV_70">
        <h2>{$campaign.unsubscribed}</h2>
        <p>Unsubscribed</p>
    </div>
</div>

<ul>
    <li>
         <strong>Successful Deliveries: </strong> {$campaign.successful_deliveries} ({$campaign.successful_delivery_rate_formatted})
    </li>
    <li>
         <strong>Total Opens: </strong> {if $campaign.opens.opens_total}{$campaign.opens.opens_total}{else}Tracking disabled{/if}
    </li>
    <li>
         <strong>Last Opened: </strong> {$campaign.opens.last_open_formatted}
    </li>
    <li>
         <strong>Forwarded: </strong> {$campaign.forwards.forwards_count} ({$campaign.forwards.forwards_opens} forwards opened)
    </li>
</ul>

<ul>
    <li>
        <strong>Clicks per unique open: </strong> {$campaign.clicks.clicks_per_opens_formatted}
    </li>
    <li>
        <strong>Total Clicks: </strong> {$campaign.clicks.clicks_total}
    </li>
    <li>
        <strong>Last Clicked: </strong> {$campaign.clicks.last_click_formatted}
    </li>
    <li>
        <strong>Abuse Reports: </strong> {$campaign.abuse_reports}
    </li>
</ul>
