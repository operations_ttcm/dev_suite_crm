




<script src="modules/SugarChimp/includes/assets/js/admin.js"></script>
<div class="setup-body">
    <h1 class="tcenter">
        MailChimp to Sugar Queued Records <button class="sc-empty-table" id="sugarchimp_mc">Empty Table</button>
    </h1>
    <hr/>
    
    {if $message}
        <h3>{$message}</h3>
    {/if}
    
    {if count($data)>0}
        <table class="table status-table" id="sugarchimp-adminmc">
            <tbody>
                <tr>
                    <td>id</td>
                    <td>date_entered</td>
                    <td>date_modified</td>
                    <td>mailchimp_list_id</td>
                    <td>data</td>
                    <td>deleted</td>
                </tr>
                {foreach from=$data key=data_key item=data_item}
                    <tr>
                        <td>{$data_item.id}</td>
                        <td>{$data_item.date_entered}</td>
                        <td>{$data_item.date_modified}</td>
                        <td>{$data_item.mailchimp_list_id}</td>
                        <td>{$data_item.data}</td>
                        <td>{$data_item.deleted}</td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    {else}
        <p>There is not data in the SugarChimp_MC table.</p>
    {/if}
</div>