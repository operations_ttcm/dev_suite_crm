




{include file="modules/SugarChimp/tpls/ce/setup/header.tpl"}
<script type="text/javascript" src="modules/SugarChimp/includes/assets/js/ce/setup/step3.js"></script>

<div class="setup-body tcenter">
    <div id="sugarchimp-error-section" class="setup-section error" style="display:none">
        <p></p>
    </div>
    <div id="apikey-group" class="row-fluid control-group license-spacing">
        <p>
            <h3>What is your MailChimp API Key?</h3>
        </p>
        <div class="control-label span3">
            
            <label for="apikey">MailChimp API Key:</label>
        </div>
        <div class="controls span8 input-append">
            <input type="text" id="apikey" class="span7" style="float: left" />
            <button class="testLogin btn btn-primary span4 pull-right" title="Connect to MailChimp">Connect</button>
            <span class="help-block pull-left mailchimp-unsuccess" style="display: none">No API Key Entered</span>
        </div>
        <div class="clearfix"></div>
    </div>
    
    <div id="mailchimp-info-unsuccess" class="mailchimp-unsuccess tcenter info-box" style="display: none">
        <h4>Don't have a MailChimp Account, yet?</h4>
        
        <p><a href="http://mailchimp.com/signup?utm_source=SugarOutfitters&utm_medium=partnerships&utm_campaign=pid&pid=sugarchimp&source=website" target="_blank">Create one now.</a></p>

        <h4>Need help locating your API key?</h4>

        <p>Check out <a href="http://kb.mailchimp.com/article/where-can-i-find-my-api-key/" target="_blank">Where can I find my API key?</a> on MailChimp.com</p>
    </div>
    <div id="mailchimp-info-success" class="mailchimp-success tcenter info-box" style="display: none">
        <h4>MailChimp API Key Registered Successfully!</h4>
        
        <p>Continue to step 4 to Sync your first List!</p>
        <button class="nextStep btn btn-primary btn-large" data-step="4">Next</button>
    </div>
    <div id="mailchimp-info-change" class="mailchimp-change tcenter info-box" style="display: none">
        <h4>Warning!</h4>
        <p>
            You have attempted to change your MailChimp API Key. Your previous API key is still active. If you have done this by accident, click refresh to view your active API key.
        </p>
        <button class="goToStep btn btn-primary btn-large" data-step="3">Refresh</button><br>

    </div>
</div>

{include file="modules/SugarChimp/tpls/ce/setup/footer.tpl"}