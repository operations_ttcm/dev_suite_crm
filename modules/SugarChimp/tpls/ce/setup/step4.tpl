




{include file="modules/SugarChimp/tpls/ce/setup/header.tpl"}
<script type="text/javascript" src="modules/SugarChimp/includes/assets/js/ce/setup/step4.js"></script>

<div class="setup-body tcenter">
    <div id="sugarchimp-error-section" class="setup-section error" style="display:none">
        <p></p>
    </div>
	<h2 class="tcenter">Sync a List!</h2>
    <div>
    	<p class="info-box header-info">
    		MailChimp uses Lists to organize your subscribers. In Sugar, we use Target Lists to do the same. SugarChimp pairs one Target List in Sugar, with one MailChimp List. From here, changes to people on that Target List in Sugar will be reflected on the corresponding subscriber in MailChimp, and vice versa.
    	</p>
    </div>
	<form id="smarlist-setup-form">
		
	</form>
</div>
{include file="modules/SugarChimp/tpls/ce/setup/footer.tpl"}
{literal}
<script id="hbs-render-step4" type="text/x-handlebars-template">
	<div class="row step-header-row">
		<div id="step1-header" class="span6 step1">
			<h3 class="step-header tleft"><strong>Step 1.</strong> Choose a Target List</h3>
			<input type="hidden" name="set-sugar-list-id" id="set-sugar-list-id" value="">
		</div>
		<div id="step2-header" class="span6 step2">
				<h3 class="step-header tright">Step 2. Choose a MailChimp List</h3>
			<input type="hidden" name="set-mailchimp-list-id" id="set-mailchimp-list-id" value="">
		</div>
	</div>
	<div class="row description-row">
		<div id="step1-description" class="span6 step1">
			<div class="info-box step-description">
				<p id="has-target-list" class="step1-description target-list " style="display:none">
					You already have Target Lists in Sugar. If you want to sync an existing Sugar Target List to MailChimp, select the List from the dropdown.<br> 
				   	Or, select “Create New Sugar List” to start from scratch.
				</p>
				<p id="no-target-list" class="step1-description no-target-list" style="display:none">
					We’ve created an empty Target List for you called ‘Master SugarChimp List’. SugarChimp will use this list to copy people over to MailChimp!
				</p>
			</div>
		</div>
		<div id="step2-description" class="span6 step2">
			<div class="info-box step-description">
				<p id="no-mailchimp-list" class="step2-description" style="display:none">
					We’ve created a MailChimp List for you called ‘Master SugarChimp List’ to get you started.
				</p>
				<p id="has-mailchimp-list" class="step2-description" style="display:none">
					You already have Lists created on your MailChimp Account. Which MailChimp List would you like to connect to the Sugar Target List from step 1? <br>
					Or select “Create New MailChimp List” to create a new one and start from scratch.
				</p>
				<p id="synced-mailchimp-list" class="step2-description" style="display:none">
					These Lists have already been paired. You can change which MailChimp List is paired in the dropdown below.
				</p>
				<p class="step1-description">
					<em><strong>NOTE:</strong> Actual syncing will not take place until later in the setup process</em>
				</p>
			</div>
		</div>
	</div>
	<div class="row dropdown-row">
		<div class="span4 step1">
			<h4 class="list-dropdown-title tleft">Sugar List</h4>
			<div id="step1-dropdown" class="pull-left space-left">
			</div>
		</div>
		<div class="span4 step2">
			<img id="two-way-arrows" class="syncing-arrows" style="display:none" src="modules/SugarChimp/includes/assets/img/setup/two-way-arrows-color-large.png">
			<img id="one-way-arrow" class="syncing-arrows" style="display:none" src="modules/SugarChimp/includes/assets/img/setup/one-way-arrow-color-large.png">
		</div>
		<div class="span4 step2 clearfix">
			<h4 class="list-dropdown-title tright">MailChimp List</h4>
			<div id="step2-dropdown" class="pull-right space-right">
			</div>
		</div>
	</div>
	<div class="row dropdown-row">
		<div class="span6 step1">
			<div id="smartlist-create-new-sugar-list" class="info-box step-description create-list"style="display:none">
				<p>Enter a name for the new Sugar Target List: <input type="text" name="smartlist-create-new-sugar-list-name" id="smartlist-create-new-sugar-list-name" value="Master SugarChimp List"> <br>(this can be changed later)</p>
				<button id="smartlist-create-new-sugar-list-button" class="btn btn-small btn-primary">Create List</button>
				<button id="smartlist-cancel-new-sugar-list-button" class="btn btn-small btn-danger pull-right">Cancel</button>
			</div>
		</div>
		<div class="span6 step2">
			<div id="smartlist-create-new-mailchimp-list" class="info-box step-description create-list smartlist-has-mailchimp-lists-check "  style="display:none">
				<p>
					Enter a name for the new MailChimp List:
					<input type="text" name="smartlist-create-new-mailchimp-list-name" id="smartlist-create-new-mailchimp-list-name" value="Master SugarChimp List"> <br>(this can be changed later)
				</p>
				<button id="smartlist-create-new-mailchimp-list-button" class="btn btn-small btn-primary">Create List</button>
				<button id="smartlist-cancel-new-mailchimp-list-button" class="btn btn-small btn-danger pull-right">Cancel</button>
			</div>
			<div id="smartlist-sync-from-mailchimp-div" class="info-box step-description not-create-list"  style="display:none">
				<p>If you have subscribers on this MailChimp List, you can sync them all to Sugar immediately. Would you like to do that?</p>
				<input type="checkbox" name="smartlist-sync-from-mailchimp" id="smartlist-sync-from-mailchimp" class="" unchecked />Yes, sync these MailChimp subscribers to Sugar <br><br>
				<p class="clearfix">
					<em><strong>NOTE:</strong> We will setup how Sugar handles them later</em>
				</p>
			</div>
		</div>
	</div>
	<br><hr><br>
	<div id="step3-start" class="tcenter" style="display:none">
    	<button class="btn btn-large btn-primary center30 smartlist-show smartlist-hide" data-hide="#step3-start" data-show=".step3">Continue to Step 3</button>
        <p class="tcenter clearfix"style="color:black;">
			<em><strong>NOTE:</strong>The following settings will only be applied to these two lists!</em>
		</p>
	</div>
	<div id="hideable-step-3" class="smartlist-hideable step3" style="display:none">
		{{#if supported_modules}}
		<div id="smartlist-options">
        	<h3 class="step-header tleft">Step 3. Who do you want to send to MailChimp?</h3>
        	<div id="step3-description" class="info-box header-info">
				<p>
					You can choose to send whoever you want from Sugar to MailChimp. Some companies send all of their Sugar Contacts to MailChimp, some send only a few.   
{{#eq current_plan 'basic'}}
  					<span id="plan-specific-insert">
						SugarChimp allows you to sync Contacts, Targets, and Leads over to MailChimp. <a href="https://store.suitecrm.com/docs/SugarChimp/try-ultimate"><strong style="color:#007ACC">You can try Pro or Ultimate for free</strong></a> to gain more advanced list management features such as:
					</span>
					<ul class="feature-list">
						<li>
							Automatically sync all of your Contacts, Targets or Leads to the list
						</li>
						<li>
							Create unlimited custom filters for each module
						</li>
						<li>
							Write custom SQL for each module
						</li>
					</ul>
				</p>
{{/eq}}
{{#eq current_plan 'professional'}}
  					<span id="plan-specific-insert">
						SugarChimp wants to make your life easy! You have the option to automatically keep all Contacts, Targets or Leads on your List! <a target="_blank" href="https://store.suitecrm.com/docs/SugarChimp/try-ultimate"><strong style="color:#007ACC">You can try Ultimate for free</strong></a> to gain more advanced list management features such as:
						<ul class="feature-list">
							<li>
								Create unlimited custom filters for each module
							</li>
							<li>
								Write custom SQL for each module
							</li>
						</ul>
					</span>
				</p>
{{/eq}}
{{#eq current_plan 'ultimate'}}
  					<span id="plan-specific-insert">
						SugarChimp wants to make your life easy! You have multiple options to allow SugarChimp to manage your lists for you! You will get more details on each option as you select it.
					</span>
				</p>
{{/eq}}
				<p>
					<em><strong>NOTE:</strong> As your company uses Sugar, new people will be created. This step will help you get those people onto the MailChimp List automatically. Choose from the options below how you want to manage this.
					</em>
				</p>
        	</div>
			<ul class="nav nav-tabs smartlist-nav">
				{{#each supported_modules}}
					<li{{#if @first}} class="active"{{/if}}><a id="tab-{{this}}">{{check this}}</a></li>
				{{/each}}
			</ul>
			<div class="tab-content syncing-tab">
				{{#each supported_modules}}
					<div class="smartlist-tab tab-pane{{#if @first}} active{{/if}}" id="smartlist-div-tab-{{this}}">								
						{{!-- none - no syncing --}}
						<div class="smartlist-mode-div clearfix">
							<input type="radio" checked="checked" name="smartlist-metadata[{{this}}][mode]" value='none' class="smartlist-mode-selection" id="smartlist-{{this}}-mode-none" data-module="{{this}}"> 
							Only sync {{check this}} added to the Target List over to MailChimp. 
							<p class="info-box sync-option-details"><em>
								This option will let you maintain the {{check this}} on your list manually. Any {{check this}} you add to the list in Sugar will be added to the list in MailChimp. You can add to the list manually by going to the Target List and adding from the subpanel or adding from a Sugar Report.
							</em></p>
							<div style="display:none" class="smartlist-type-{{this}}-div sync-option-details-div" id="smartlist-type-{{this}}-div-none">
							</div>
						</div>
						<br>
						{{!-- all - sync all records --}}
{{#eq ../current_plan 'basic'}}
						<div class="smartlist-mode-div clearfix disabled-text">
							<input type="radio" name="smartlist-metadata[{{this}}][mode]" value='all' class="smartlist-mode-selection" id="smartlist-{{this}}-mode-all" data-module="{{this}}" disabled>
							Sync All {{check this}} from Sugar automatically to the MailChimp List. <a href="javascript:;" data-plan="professional" class="sc-upgrade-link"><span class="upgrade">(try Professional out for free!)</span></a>
{{else}}
						<div class="smartlist-mode-div clearfix">
							<input type="radio" name="smartlist-metadata[{{this}}][mode]" value='all' class="smartlist-mode-selection" id="smartlist-{{this}}-mode-all" data-module="{{this}}">
							Sync All {{check this}} from Sugar automatically to the MailChimp List.
{{/eq}}
							<p class="alert alert-danger smartlist-type-{{this}}-div-mode-errors" id="smartlist-type-{{this}}-div-all-errors" style="display:none"></p>
							<p class="info-box sync-option-details"><em>
								This option will sync all of your Sugar {{check this}} to MailChimp. As new {{check this}} are created, they will be also added to your Sugar Target List and synced to the MailChimp List.
							</em></p>
							<div style="display:none" class="smartlist-type-{{this}}-div sync-option-details-div" id="smartlist-type-{{this}}-div-all">
							</div>
						</div>
						<br>
						{{!-- smart - sync using smartlist filters --}}
{{#eq ../current_plan 'basic'}}
						<div class="smartlist-mode-div clearfix disabled-text">
							<input type="radio" name="smartlist-metadata[{{this}}][mode]" value='smart' class="smartlist-mode-selection" id="smartlist-{{this}}-mode-smart" data-module="{{this}}" disabled>
							Create Filters to automatically sync {{check this}} to MailChimp. <a href="javascript:;" data-plan="ultimate" class="sc-upgrade-link"><span class="upgrade">(try Ultimate out for free!)</span></a>
{{/eq}}
{{#eq ../current_plan 'professional'}}
						<div class="smartlist-mode-div clearfix disabled-text">
							<input type="radio" name="smartlist-metadata[{{this}}][mode]" value='smart' class="smartlist-mode-selection" id="smartlist-{{this}}-mode-smart" data-module="{{this}}" disabled>
							Create Filters to automatically sync {{check this}} to MailChimp. <a href="javascript:;" data-plan="ultimate" class="sc-upgrade-link"><span class="upgrade">(try Ultimate out for free!)</span></a>
{{/eq}}
{{#eq ../current_plan 'ultimate'}}
						<div class="smartlist-mode-div clearfix">
							<input type="radio" name="smartlist-metadata[{{this}}][mode]" value='smart' class="smartlist-mode-selection" id="smartlist-{{this}}-mode-smart" data-module="{{this}}">
							Create Filters to automatically sync {{check this}} to MailChimp.
{{/eq}}
							<p class="alert alert-danger smartlist-type-{{this}}-div-mode-errors" id="smartlist-type-{{this}}-div-smart-errors" style="display:none"></p>
							<p class="info-box sync-option-details"><em>
								You can create custom filters for which {{check this}} you want on the list. SugarChimp will automatically add {{check this}} that meet the criteria, as well as remove those {{check this}} that no longer meet the criteria.
							</em></p>
							<div style="display:none" class="smartlist-type-{{this}}-div sync-option-details-div" id="smartlist-type-{{this}}-div-smart">
{{#eq ../current_plan 'ultimate'}}							
								<table id="smartlist-filters-table-{{this}}" width="95%" class="smartlist-filters-table">
									<tr id="smartlist-filters-header-{{this}}" class="smartlist-filters-header">
										   <th></th>
										   <th>Field</th>
										   <th>Operand</th>
										   <th>Value</th>
									</tr>
								</table>
								<div>
									<hr>
									<button class="createNewFilterRow btn" data-module="{{this}}">Add Filter</button>
									<button id="{{this}}-test-query-filter" class="btn test-filters" data-type="filters" data-module="{{this}}">Test Filters</button>
									<em><p id="{{this}}-test-query-count-filters"></p></em>
								</div>
{{/eq}}					
							</div>						
						</div>
						<br>
						{{!-- sql - sync using a straight sql query --}}
{{#eq ../current_plan 'basic'}}
						<div class="smartlist-mode-div clearfix disabled-text">
							<input type="radio" name="smartlist-metadata[{{this}}][mode]" value='sql' class="smartlist-mode-selection" id="smartlist-{{this}}-mode-sql" data-module="{{this}}" disabled>
							Write your own SQL Query to automatically sync {{check this}} to MailChimp. <a href="javascript:;" data-plan="ultimate" class="sc-upgrade-link"><span class="upgrade">(try Ultimate out for free!)</span></a>
{{/eq}}
{{#eq ../current_plan 'professional'}}
						<div class="smartlist-mode-div clearfix disabled-text">
							<input type="radio" name="smartlist-metadata[{{this}}][mode]" value='sql' class="smartlist-mode-selection" id="smartlist-{{this}}-mode-sql" data-module="{{this}}" disabled>
							Write your own SQL Query to automatically sync {{check this}} to MailChimp. <a href="javascript:;" data-plan="ultimate" class="sc-upgrade-link"><span class="upgrade">(try Ultimate out for free!)</span></a>
{{/eq}}
{{#eq ../current_plan 'ultimate'}}
						<div class="smartlist-mode-div clearfix">
							<input type="radio" name="smartlist-metadata[{{this}}][mode]" value='sql' class="smartlist-mode-selection" id="smartlist-{{this}}-mode-sql" data-module="{{this}}">
							Write your own SQL Query to automatically sync {{check this}} to MailChimp.	
{{/eq}}
							<p class="alert alert-danger smartlist-type-{{this}}-div-mode-errors" id="smartlist-type-{{this}}-div-sql-errors" style="display:none"></p>
							<p class="info-box sync-option-details"><em>
								<strong>[Advanced Customers Only]</strong> If you have a complex way to determine who needs to be synced with MailChimp, you can write a sql query below. You will need to follow our <a href="https://store.suitecrm.com/docs/SugarChimp/smartlist-custom-sql-guide" target="_blank">Custom SQL Guide</a>.
							</em></p>
							<div style="display:none" class="smartlist-type-{{this}}-div sync-option-details-div" id="smartlist-type-{{this}}-div-sql">
{{#eq ../current_plan 'ultimate'}}
								<textarea name="smartlist-metadata[{{this}}][sql][initial_sql]" id="smartlist-custom-sql-{{this}}" class="sync-option-details smartlist-custom-sql-box"></textarea>
								<div class="test-query-row">
									<button id="{{this}}-test-query-sql" class="span2 btn test-query" data-type="sql" data-module="{{this}}">Test Query</button>
									<em><p id="{{this}}-test-query-count-sql" class="span10"></p></em>
								</div>
{{/eq}}
							</div>	
						</div>
					</div>				
				{{/each}}
			</div>
		</div>
	{{else}}
		<div><p class="tcenter">Loading Sync Options. . .</p></div>
	{{/if}}
		<br>
		<hr>
		<br>
		<div id="step4-start" class="step3 tcenter">
        	<button id="continue-step-4" class="btn btn-large btn-primary center30 smartlist-show smartlist-hide" data-hide="#step4-start" data-show="#hideable-step-4">Continue to Step 4</button>
            <p class="tcenter clearfix"style="color:black;">
				<em><strong>NOTE: </strong>Only one more step before we sync!</em>
			</p>
		</div>
	</div>
	<div id="hideable-step-4" class="hideable-step step4" style="display:none" data-step="4">
        <h3 class="step-header tleft">Step 4. How should we handle new subscribers from MailChimp?</h3>
        <div id="step4-header"class="info-box header-info">
        	<p>
        		There will be times that new people are brought into Sugar from this MailChimp List. For example, if you use MailChimp web forms to get new subscribers. When new subscribers are created in MailChimp, they will be imported to Sugar. What type of record would you like to be created when a new subscriber is added to Sugar?
        	</p><br>
        	<div id="create-subscriber-dropdown">
        	</div>
        </div>
		<hr>
		<br>
        <div id="step5-start" class="tcenter">
        	<div class="row">
            	<div class="centered" style="clear: both;">
	      				<button id="premapping-sync" class="btn btn-large btn-primary center30">Save and Sync</button>
					<p class="tcenter">
						<em><strong>NOTE:</strong>This will start the syncing process!</em>
					</p>
				</div>
			</div>
		</div>
    </div>
</script>
{/literal}
{include file="modules/SugarChimp/includes/assets/handlebars/filter-dropdown.hbs"}
{include file="modules/SugarChimp/includes/assets/handlebars/filter-multiselect.hbs"}
{include file="modules/SugarChimp/includes/assets/handlebars/filter-empty.hbs"}
{include file="modules/SugarChimp/includes/assets/handlebars/filter-input.hbs"}
{include file="modules/SugarChimp/includes/assets/handlebars/filter-row.hbs"}
{include file="modules/SugarChimp/includes/assets/handlebars/generic-dropdown.hbs"}
{include file="modules/SugarChimp/includes/assets/handlebars/mailchimp-lists.hbs"}
{include file="modules/SugarChimp/includes/assets/handlebars/sugar-lists.hbs"}