




{literal}
<script type="text/javascript">
    var sugarChimpSelf = {
        confirmed: function(){},
        cancelled: function(){}
    };
</script>
{/literal}

<link rel="stylesheet" type="text/css" href="modules/SugarChimp/includes/assets/css/ce/bootstrap.css?8.1.1a">
<link rel="stylesheet" type="text/css" href="modules/SugarChimp/includes/assets/css/sugarchimp.css?8.1.1a">
<link rel="stylesheet" type="text/css" href="modules/SugarChimp/includes/assets/css/ce/sugarchimp_sugar6.css?8.1.1a">
<div id="include-ce-css"></div>
<script type="text/javascript" src="modules/SugarChimp/includes/assets/js/ce/handlebars.js"></script>
<script type="text/javascript" src="modules/SugarChimp/includes/assets/js/ce/setup/setup.js"></script>
<div class="sugarchimp-bootstrap sugarchimp-setup">
    <div class="setup-container container-fluid main-panel">
        <div class="row-fluid setup-steps">
            <a id="sugarchimp-setup-step1" class="span3 {if $step_count == 1}current{/if}" data-step="1" href="index.php?module=SugarChimp&action=step1"><span class="badge">1)</span> Pre-Checks</a>
            <a id="sugarchimp-setup-step2" class="span3 {if $step_count == 2}current{/if}" data-step="2" href="index.php?module=SugarChimp&action=step2"><span class="badge">2)</span> License Key</a>
            <a id="sugarchimp-setup-step3" class="span3 {if $step_count == 3}current{/if}" data-step="3" href="index.php?module=SugarChimp&action=step3"><span class="badge">3)</span> Connect</a>
            <a id="sugarchimp-setup-step4" class="span3 {if $step_count == 4}current{/if}" data-step="4" href="index.php?module=SugarChimp&action=step4"><span class="badge">4)</span> Sync a List</a> 
        </div>
        <div class="row-fluid warning-panel tcenter" style="display:none">
            <div class="warning-box info-box">
                <h3 id="warning-header" class="tcenter">This is a Warning Title!</h3>
                <p id="warning-body">This is a Warning Message</p>
                <button class="btn btn-primary" id="warning-confirm">confirm</button>
                <button class="btn btn-danger" id="warning-cancel">cancel</button>
            </div>
        </div>
        <div class="row-fluid loading-panel tcenter" style="display:none">
            <p id="loading-body"></p>
            <hr>
        </div>
        <div class="row-fluid content-panel">
            <div class="span12">