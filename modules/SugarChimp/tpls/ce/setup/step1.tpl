




{include file="modules/SugarChimp/tpls/ce/setup/header.tpl"}
<script type="text/javascript" src="modules/SugarChimp/includes/assets/js/ce/setup/step1.js"></script>

<div class="setup-body">
    
    <div id="sugarchimp-error-section" class="setup-section error" style="display:none">
        <p></p>
    </div>
    <div id="welcome-message" class="welcome-message tcenter">
        <h2 class="license-spacing">Welcome to SugarChimp!</h2>
        <div class="info-box extra-padding">
            <p>
                This guide will walk you through our brief setup process. We will start by making sure your system has everything it needs to allow SugarChimp to function properly! Before you know it, you'll be a SugarChimp Master!
            </p>
            <button id="show-checks" class="btn btn-primary btn-large btn-margins">Let's get Started!</button><br>
        </div>
    </div>
    <div id="prechecks" style="display:none">
        <h3 class="tcenter">SugarChimp Essentials</h3>
        <div class="info-box extra-padding">
            <p>
                This Page outlines the 4 essential network and server settings that SugarChimp needs to function properly. 
                <span class="precheck-success">
                    <br><br>It looks as though everything is setup and ready to continue!
                </span>
                <span class="precheck-fail">
                    The items in <span class="alert label-large alert-danger"><strong>red</strong></span> are in need of attention before you continue. Follow the given instructions to get SugarChimp running in no time!
                </span> 
            </p>
        </div>
        <table class="table status-table">
            <tbody>
                <tr>
                    <td colspan="2">
                    </td>
                <tr>
                    <td width="45%" class="precheck tright vmiddle">PHP >= 5.3 :</td>
                    <td>
                        <div id="php-box-success" class="php-success" style="display:none">
                            <img src="modules/SugarChimp/includes/assets/img/green-check.png"><span id="php-success-details"></span> 
                        </div>
                        <div id="php-box-fail" class="php-fail" style="display:none">
                            <img src="modules/SugarChimp/includes/assets/img/red-cross.png">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <div class="php-fail alert label-large alert-danger">
                            <div id="php-fail-help">
                            </div>
                            <div class="php-fail">
                                <em><strong>NOTE: </strong>If you are working with a Sugar consultant/developer, they will know how to set this up.
                                If you want to try to address the issues yourself, please use the link(s) below: </em>
                                <ul>
                                    <li>
                                        <a href="https://store.suitecrm.com/docs/SugarChimp/failed-prechecks#php" target="_blank">How to update PHP</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="45%" class="precheck tright vmiddle">cURL Enabled :</td>
                    <td>
                        <div id="curl-box-success" class="curl-success" style="display:none">
                            <img src="modules/SugarChimp/includes/assets/img/green-check.png" >
                        </div>
                        <div id="curl-box-fail" class="curl-fail" style="display:none">
                            <img src="modules/SugarChimp/includes/assets/img/red-cross.png">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <div id="curl-fail-help" class="curl-fail alert label-large alert-danger">
                            <p>
                                cURL is a PHP extension that allows you to connect to different servers. SugarChimp uses cURL to communicate with MailChimp. Your cURL Extension is currently not enabled.
                            </p>
                            <em><strong>NOTE: </strong>
                                If you are working with a Sugar consultant/developer, they will know how to set this up.
                                If you want to try to address the issues yourself, please use the link(s) below: 
                            </em>
                            <ul>
                                <li>
                                    <a href="https://store.suitecrm.com/docs/SugarChimp/failed-prechecks#curl" target="_blank">How to enable cURL</a>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="45%" class="precheck tright vmiddle">Network Setup :</td>
                    <td>
                        <div id="network-box-success" class="network-success" style="display:none">
                            <img src="modules/SugarChimp/includes/assets/img/green-check.png">
                        </div>
                        <div id="network-box-fail" class="network-fail" style="display:none">
                            <img src="modules/SugarChimp/includes/assets/img/red-cross.png">
                        </div>
                        <div id="network-box-unknown" class="network-unknown" style="display:none">
                            Unknown
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <div class="network-fail network-unknown alert label-large alert-danger">
                            <p id="network-fail-help"></p>
                            <p class="network-fail">
                                <em><strong>NOTE: </strong>
                                    If you are working with a Sugar consultant/developer, they will know how to set this up.
                                    If you want to try to address the issues yourself, please use the link(s) below: 
                                </em>
                                <ul>
                                    <li class="curl-success">
                                        <a href="https://store.suitecrm.com/docs/SugarChimp/failed-prechecks#network" target="_blank">Network Problems with SugarChimp</a>
                                    </li>
                                </ul>
                            </p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="45%" class="precheck tright vmiddle">Scheduler Configured :</td>
                    <td>
                        <div id="scheduler-box-success" class="scheduler-success" style="display:none">
                            <img src="modules/SugarChimp/includes/assets/img/green-check.png">
                        </div>
                        <div id="scheduler-box-fail" class="scheduler-fail" style="display:none">
                            <img src="modules/SugarChimp/includes/assets/img/red-cross.png">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <div id="scheduler-fail-help" class="scheduler-fail alert label-large alert-danger">
                            <p>
                                The Sugar Scheduler is a tool that comes with Sugar and allows you to run recurring processes on a specific schedule. We noticed that yours has never run. SugarChimp needs this tool to function properly.
                            </p><br>
                            <em><strong>NOTE: </strong>
                                If you are working with a Sugar consultant/developer, they will know how to set this up.
                                If you want to try to address the issues yourself, please use the link below: 
                            </em>
                            <ul>
                                <li>
                                    <a href="https://store.suitecrm.com/docs/SugarChimp/failed-prechecks#scheduler" target="_blank">Sugar Scheduler is not Working</a>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="precheck-success setup-success tcenter info-box" style="display:none">
            <h3>Great, your system is ready!</h3>
            <p>Continue to step 2 to validate your SugarChimp license!</p>
            <button class="nextStep btn btn-primary btn-large" data-step="2">Next</button>
        </div>
        <div class="precheck-fail setup-fail tcenter info-box" style="display:none">
            <h3>Warning!</h3>
            <p>SugarChimp found issues. Use the ReCheck button to see if the issues are cleared!</p>
            <button class="reCheck btn btn-primary btn-large">ReCheck</button><br>
            <p id="recheck-support" class="label-large">
                <em>If you are trying to setup the integration on your own, contact <a href="mailto:support@sugarchimp.com">support</a> for more help.</em>
            </p>
            <div id="continue-anyway" style="display:none">
                <hr>
                <p>You may continue to setup your integration, but it will not function properly until all of the essentials are met.</p>
                <button class="nextStep btn btn-danger btn-large" data-step="2">Next</button>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

{include file="modules/SugarChimp/tpls/ce/setup/footer.tpl"}