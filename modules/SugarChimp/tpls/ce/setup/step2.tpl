




{include file="modules/SugarChimp/tpls/ce/setup/header.tpl"}
<script type="text/javascript" src="modules/SugarChimp/includes/assets/js/ce/setup/step2.js"></script>

<div class="setup-body tcenter">
    <div id="sugarchimp-error-section" class="setup-section error" style="display:none">
        <p></p>
    </div>
    <div id="insert-upgrade-details">
        
    </div>
    <div id="license-group" class="row-fluid control-group license-spacing">
        <p>
            <h3>SugarChimp License Validation</h3>
        </p>
        <div class="control-label span3 tright">
            <label for="licenseKey">License Key:</label>
        </div>
        <div class="controls span8 input-append">
            <input type="text" id="licenseKey" class="span8" style="float: left" />
            <button class="validateLicense btn btn-primary span4 pull-right" title="Validate SugarChimp License">Validate</button>
            <span class="help-block pull-left" style="display: none">No key entered</span>
        </div>
        <div class="clearfix"></div>
    </div>
    <div id ="license-info-fail" class="info-box" style="display: none">
        <h4>How to Validate Your SugarChimp License</h4>
        <ol>
            <li>Login to <a href="https://store.suitecrm.com" target="_blank">SugarOutfitters</a><br/></li>
            <li>From the "My Account" menu, go to <a href="https://store.suitecrm.com/orders" target="_blank">Purchases</a></li>
            <li>Locate the key for the purchase of this add-on</li>
            <li>Paste the key into the License Key box below</li>
            <li>Hit "Validate"</li>
        </ol>
    </div>
    <div id ="license-info-success" class="info-box" style="display: none">
        <h3>Validation Success!</h3>
        <p>Continue to step 3 to register your MailChimp API key.</p>
        <button class="nextStep btn btn-primary btn-large" data-step="3">Next</button>
    </div>
</div>

{include file="modules/SugarChimp/tpls/ce/setup/footer.tpl"}
{literal}
<script id="hbs-render-step2" type="text/x-handlebars-template">
    <div class="trial-period license-validated">
        <p class="trial-ended" style="display:none">Your trialing period has ended.</p>
        <p class="countdown-timer" style="display:none"><em>
            <span id="in-upgrade-trial" class="" style="display:none">Upgrade </span>Trial ends in 
            <span id="exact-time" class="" style="display:none">
                <strong><span id="countdown-days"> </span></strong>
                <strong><span id="countdown-hours"></span></strong>
                <strong><span id="countdown-minutes"></span></strong>
            </span>
            <span id="minimal-time" class="" style="display:none">less than an hour!</span>
        </em></p>
    </div>
    <div id="plan-switching" class="license-validated " style="display:none">
        <h3 id="plan-switching-header"><a href="javascript:;" class="hideToggle" data-toggle="#collapsible-plan-switching">Try a new Plan!</a></h3>
        <div id="collapsible-plan-switching" class="collapse in">
            <p id="current-plan-p" class="">You are currently <span id="trialing-or-on"></span> <span id="current-plan-label"></span></p>
            <div id="plan-switching-options">

            </div>
            <div id="all-plan-buttons" class="">
                <div id="plan-switching-button" class="" style="display:none">
                    <button id="initiate-switch-button" class="btn btn-primary btn-small plan-change-accept smartlist-show" data-show="#plan-switching-terms">Try it out Now!</button>
                    <p class="button-details"><em>free for 14 days!</em></p>
                </div>
                <div id="switch-upgrade" class="pull-left" style="display:none">
                    <button id="switch-upgrade-button" class="btn btn-primary btn-small plan-change-accept smartlist-show" data-show="#plan-switching-terms">Switch Plan</button>
                    <span class="button-details"><em>switch freely throughout the trial!</em></span>
                </div>
                <div id="cancel-upgrade" class="pull-right " style="display:none">
                    <button id="cancel-upgrade-button" class="btn btn-danger btn-small">Cancel Upgrade</button>
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="plan-switching-terms" class="" style="display:none">
                <p class="terms-header">Terms of Plan Switching</p>
                <textarea id="plan-switching-terms-textbox" class="sugarchimp-terms-box" style="display:none" readonly>
&#8226;Your upgrade trial will last 14 days from the initial upgrade
&#8226;You can switch plans freely at any time during those 14 days by coming back to this page
&#8226;You can cancel the upgrade at any time within the 14 day trialing period to remain on your current subscription
&#8226;At the conclusion of your trial, your subscription will automatically be changed to whichever plan you are trialing
&#8226;When a subscription change occurs, you will be charged a prorated amount for that billing period
&#8226;To get more details on the prorated subscription amount, or change between monthly and yearly billing options, contact support@sugarchimp.com
                </textarea>
                <textarea id="in-trial-terms-textbox" class="sugarchimp-terms-box"  style="display:none" readonly>
&#8226;You can switch plans freely until the end of your SugarChimp trial
&#8226;At the conclusion of your trial, your subscription will automatically be changed to whichever plan you are trialing
&#8226;If you would like to change plans after your initial trial period, you will have one free upgrade period of 14 days to try out more advanced features at a later time.
&#8226;To get more details or help during your trial, contact support@sugarchimp.com
                </textarea>
                <div id="terms-buttons">
                    <button id="accept-terms" class="btn btn-primary btn-small smartlist-hide" data-hide="#collapsible-plan-switching">Accept</button>
                    <button id="cancel-terms" class="btn btn-danger btn-small smartlist-show smartlist-hide" data-toggle="hide" data-show="#all-plan-buttons" data-hide="#plan-switching-terms">cancel</button>
                </div>
            </div>
        </div>
    </div>
</script>
{/literal}
{include file="modules/SugarChimp/includes/assets/handlebars/plan-switch-option.hbs"}