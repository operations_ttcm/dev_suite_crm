




{literal}
<script>
    $(document).ready(function(){
        
        {/literal}
        {if !empty($list_id)}
            get_field_mapping_details('{$list_id}');
        {else}
            get_field_mapping_details($('#list_id').val());
        {/if}
        {literal}
        
        $('#list_id').change(function(){
            get_field_mapping_details($(this).val());
        });
    });
    
    function get_field_mapping_details(list_id){
        if (list_id==''){
            alert('You must select a list.');
        }
        
        $('#field_mapping_div').html('');
        $('#field_mapping_div_loading').show();
        
        // get mapping
        $.ajax({
            url: 'index.php',
            method: 'post',
            dataType: 'json',
            data: {
                module: 'SugarChimp',
                action: 'field_mapping_details',
                list_id: list_id,
                to_pdf: 1,
            },
            success: function(data){
                $('#field_mapping_div_loading').hide();
                if (data && data.success && data.success == true){
                    $('#field_mapping_div').html(data.html);
                } else {
                    if (data.message){
                        alert(data.message);
                    } else {
                        alert('Mapping could not be loaded for the list. Check sugarcrm logs.');
                    }
                }
            },
            error: function(data){
                $('#field_mapping_div_loading').hide();
                alert('Mapping could not be loaded for the list. Check sugarcrm logs.');
            },
        });
    }
</script>
{/literal}

{foreach from=$errors key=error_key item=error_message}
    <div class="setup-section error">
        <p>{$error_message}</p>
    </div>
{/foreach}

<h1 class="tcenter">
    Field Mapping
</h1>
<hr/>

<h3>Map your MailChimp List fields, merge tags and groups with your SugarCRM Contact, Target and Lead fields.</h3>
<p>Each MailChimp List has its own set of custom fields. SugarChimp allows you to easily define exactly what you would like to sync. To setup your custom field mapping, use the following steps:</p>
<ol>
    <li>Using the first drop down below, select the MailChimp list you would like to customize the field mapping for. (Note: You can only map fields for lists that are currently set to sync)</li>
    <li>After the table loads, the left column will contain the MailChimp List fields, merge tags and groups. The columns on the right will contain the possible SugarCRM Modules and Fields you can sync with.</li>
    <li>Go through each row and mapping the fields using the drop downs. When you are finished, click the "Continue" button at the bottom.</li>
    <li>You'll be taken to the next step that has more options for updating your lists based on your new mapping.</li>
</ol>
<p>And that's it! From now on, the mapped fields below will be used when changes are made in MailChimp or SugarCRM.</p>
<p>To create new fields on your MailChimp List, you need to login to your MailChimp account and follow these instructions: <a href="http://kb.mailchimp.com/article/getting-started-with-merge-tags">http://kb.mailchimp.com/article/getting-started-with-merge-tags</a></p>
<hr/>

{if $lists}
    <p>Select a MailChimp list to map SugarCRM fields to: 
        <select name="list_id" id="list_id">
            {foreach from=$lists key=list_key item=list}
                <option value="{$list.id}" {if !empty($list_id) && $list_id == $list.id}selected="selected"{/if}>{$list.name}</option>
            {/foreach}
        </select>
    </p>
    <div id="mapping">
        
    </div>
{else}
    <p>You do not have any MailChimp lists.</p>
{/if}

<hr>

<div id="field_mapping_div"></div>
<div id="field_mapping_div_loading" style="display:none;">
    <p><img src="modules/SugarChimp/includes/assets/img/loading.gif"></p>
</div>
