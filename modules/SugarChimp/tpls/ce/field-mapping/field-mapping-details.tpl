




{literal}
<script type="text/javascript">
    $(document).ready(function(){
        $('#field-mapping-continue-button').click(function(){
            $('#field-mapping-continue-div').show();
        });

        $('#field-mapping-cancel-button').click(function(){
            $('#field-mapping-continue-div').hide();
            $('#field-mapping-sync-direction').css('border','inherit').css('color','inherit');
        });

        $('.sugarchimp-field-mapping-save-button').click(function(){
            $(this).html('<img src="modules/SugarChimp/includes/assets/img/loading.gif" width="16" height="16">');
            $(this).attr('clicked','true');
        });

        $('#field_mapping_details_form').submit(function(){
            $('.sugarchimp-field-mapping-save-button').attr('disabled','disabled');
            $('.sugarchimp-field-mapping-save-button').addClass('disabled');
            if ($('.sugarchimp-field-mapping-save-button[clicked=true]').val() == 'save_and_sync' && $('#field-mapping-sync-direction').val()==''){
                // trying to setup a sync, but sync type is not selected
                $('#field-mapping-save-and-sync-button').html('Save Mapping & Start Re-sync');
                $('.sugarchimp-field-mapping-save-button').removeAttr('disabled','disabled');
                $('.sugarchimp-field-mapping-save-button').removeClass('disabled');
                $('#field-mapping-sync-direction').css('border','1px solid red').css('color','red');
                alert('To Save Mapping and Start a Re-sync, please make a selection from the "Re-sync Options" dropdown.');
                return false;
            }
            return true;
        });
    });
</script>
{/literal}

<form action="index.php" method="post" id="field_mapping_details_form">
    <input type="hidden" name="module" value="SugarChimp">
    <input type="hidden" name="action" value="save_field_mapping">
    <input type="hidden" name="sugar_target_list_id" value="{$mappings.sugar_target_list_id}">
    <input type="hidden" name="list_id" value="{$list_id}">
    <table cellspacing="10">
        <thead>
            <tr>
                <th>MailChimp List Field</th>
                {foreach from=$mappings.modules key=module_key item=module}
                    <th>{if $module == 'Prospects'}Targets{else}{$module}{/if}</th>
                {/foreach}
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Email Address (EMAIL)</td>
                <td align="center"><em>Email Address (email1)</em></td>
                <td align="center"><em>Email Address (email1)</em></td>
                <td align="center"><em>Email Address (email1)</em></td>
            </tr>
            {foreach from=$mappings.merge_vars key=merge_var_key item=merge_var}
                {if $merge_var.tag != 'EMAIL' && $merge_var.tag != 'NEW-EMAIL'}
                    <tr>
                        <td>{$merge_var.name} ({$merge_var.tag})</td>
                        {foreach from=$mappings.modules key=module_key item=module}
                            <td>
                                <select name="mapping|{$mappings.list_id}|{$module}|{$merge_var.tag}{if $merge_var.type=='date'}|Date{if !empty($merge_var.options.date_format)}|{$merge_var.options.date_format}{/if}{/if}" id="mapping|{$mappings.list_id}|{$module}|{$merge_var.tag}" class="mapping">
                                    <option value="">Do Not Sync</option>
                                    {foreach from=$mappings.mappings.$module.available_fields key=available_field_key item=available_field}
                                        <option value="{$available_field.name}" {if $mappings.mappings.$module.mapping[$merge_var.tag] == $available_field.name}selected="selected"{/if}>{$available_field.label} ({$available_field.name})</option>
                                    {/foreach}
                                </select>
                            </td>
                        {/foreach}
                    </tr>
                {/if}
            {/foreach}
            {if !empty($mappings.groups)}
                {foreach from=$mappings.groups key=group_key item=group}
                    <tr>
                        <td>{$group.title} (Group #{$group.id})</td>
                        {foreach from=$mappings.modules key=module_key item=module}
                            <td>
                                <select name="mapping|{$mappings.list_id}|{$module}|{$group.id}|Group" id="mapping|{$mappings.list_id}|{$module}|{$group.id}|Group" class="mapping">
                                    <option value="">Do Not Sync</option>
                                    {foreach from=$mappings.mappings.$module.group_fields key=group_field_key item=group_field}
                                        <option value="{$group_field.name}" {if $mappings.mappings.$module.mapping[$group.id] == $group_field.name}selected="selected"{/if}>{$group_field.label} ({$group_field.name})</option>
                                    {/foreach}
                                </select>
                            </td>
                        {/foreach}
                    </tr>
                {/foreach}
            {/if}
        </tbody>
    </table>
    <button type="button" id="field-mapping-continue-button">Continue</button>

    <div id="field-mapping-continue-div" style="background:#eee; border:1px solid #ddd; padding:10px; margin:10px 0; display:none;">
        <h1>You are about to change your Field Mapping Settings!</h1><br>
        <p>The new Field Mapping Settings will not take affect until you re-sync your list.</p><br>
        
        {if !empty($mappings.groups)}
            <p style="color:red;font-weight:bold">You are syncing a MailChimp Group field. Before continuing, do you know which side has the most up-to-date data for the Group Field? Is it MailChimp or Sugar? If you do not know, please find out before continuing, otherwise data-loss may occur.</p><br>

            <p style="color:red">If MailChimp has the most up-to-date group data, please select MailChimp to Sugar from the “Re-sync Options” dropdown below and click the "Save Mapping & Start Re-sync" button.</p><br>

            <p style="color:red">If Sugar has the most up-to-date group data, please select Sugar to MailChimp from the “Re-sync Options” dropdown below and click the "Save Mapping & Start Re-sync" button.</p><br>
        {/if}

        <p>If you have any questions before moving forward, please email <a href="mailto:support@sugarchimp.com">support@sugarchimp.com</a></p><br>

        <br>
        <hr>

        <h1>What type of re-sync would you like to perform? Please make a selection before continuing.</h1><br>

        <select id="field-mapping-sync-direction" name="sync_direction">
            <option value="">Re-sync Options</option>
            <option value="mc2sugar">Queue Re-sync from MailChimp to Sugar</option>
            <option value="sugar2mc">Queue Re-sync from Sugar to MailChimp</option>
        </select>
        <button type="submit" name="field_mapping_action" value="save_and_sync" id="field-mapping-save-and-sync-button" class="btn btn-primary sugarchimp-field-mapping-save-button">Save Mapping & Start Re-sync</button>

        <br><br><hr>

        <h1>Other Options:</h1><br>

        <button type="button" class="pull-left btn" id="field-mapping-cancel-button">Cancel & Go Back</button>
        
        <br><br>

        <button type="submit" name="field_mapping_action" value="save_no_sync" class="pull-left btn btn-primary sugarchimp-field-mapping-save-button" id="field-mapping-save-no-sync-button">Save Mapping & Do NOT Start Re-sync</button>
        
        <br><br>
        <hr>

        <p>"Save Mapping & Start Re-sync" - This button will save your field mapping changes and queue a re-sync for the list in the direction that you select from the dropdown.</p><br>

        <p>"Cancel & Go Back" - This button will prevent any changes from being made to your mapping and take you back to the Field Mapping page.</p><br>

        <p>"Save Mapping & Do NOT Start Re-sync" - This button will save your field mapping but will NOT queue a re-sync for your list. If you are syncing MailChimp Groups with a Sugar Field, this could cause potential for a loss of data.</p><br>
    </div>
</form>