




<script src="modules/SugarChimp/includes/assets/js/admin.js"></script>
<div class="setup-body">
    <h1 class="tcenter">
        SugarChimp Queued Records <button class="sc-empty-table" id="sugarchimp">Empty Table</button>
    </h1>
    <hr/>
    
    {if $message}
        <h3>{$message}</h3>
    {/if}
    
    {if count($data)>0}
        <table class="table status-table" id="sugarchimp-admin">
            <tbody>
                <tr>
                    <td>created_by</td>
                    <td>date_entered</td>
                    <td>date_modified</td>
                    <td>deleted</td>
                    <td>description</td>
                    <td>id</td>
                    <td>mailchimp_list_id</td>
                    <td>modified_user_id</td>
                    <td>name</td>
                    <td>param1</td>
                    <td>param2</td>
                    <td>param3</td>
                    <td>param4</td>
                    <td>param5</td>
                </tr>
                {foreach from=$data key=data_key item=data_item}
                    <tr>
                        <td>{$data_item.created_by}</td>
                        <td>{$data_item.date_entered}</td>
                        <td>{$data_item.date_modified}</td>
                        <td>{$data_item.deleted}</td>
                        <td>{$data_item.description}</td>
                        <td>{$data_item.id}</td>
                        <td>{$data_item.mailchimp_list_id}</td>
                        <td>{$data_item.modified_user_id}</td>
                        <td>{$data_item.name}</td>
                        <td>{$data_item.param1}</td>
                        <td>{$data_item.param2}</td>
                        <td>{$data_item.param3}</td>
                        <td>{$data_item.param4}</td>
                        <td>{$data_item.param5}</td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    {else}
        <p>There is not data in the SugarChimp table.</p>
    {/if}
</div>