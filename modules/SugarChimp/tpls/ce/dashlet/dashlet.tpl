




<link rel="stylesheet" href="modules/SugarChimp/includes/assets/css/sugarchimp-dashlet.css?8.1.1a" />

<div class="sugarchimp-dashlet sugarchimp-dashlet-ce">
    {if !empty($data.dataFetched)}
        {if $data.success == true}
            {if !empty($data.lists)}
                <div id="mailchimp-lists">
                    <h2>MailChimp Lists:</h2>
                    <ul class="lists">
                        {foreach from=$data.lists key=list_key item=this}
                            <li><strong>{$this.name}</strong> ({$this.status})</li>
                        {/foreach}
                    </ul>
                </div>
            {else}
                <p>This person is not subscribed to any lists in MailChimp.</p>
            {/if}
            {if !empty($data.activities)}
                <ul>
                    {foreach from=$data.activities key=activities_key item=this}
                        <li class="activity">
                            <div class="activity-timestamp">
                                <div class="activity-date">{$this.date}</div>
                                <div class="activity-time">{$this.time}</div>
                                <div class="activity-action">
                                    {if !empty($this.action)}
                                        {if $this.action == "open"}
                                            <img src="modules/SugarChimp/includes/assets/img/dashlet/activity-opened.png">
                                        {/if}
                                        {if $this.action == "click"}
                                            <img src="modules/SugarChimp/includes/assets/img/dashlet/activity-clicked.png">
                                        {/if}
                                        {if $this.action == "bounce"}
                                            <img src="modules/SugarChimp/includes/assets/img/dashlet/activity-bounced.png">
                                        {/if}
                                        {if $this.action == "unsub"}
                                            <img src="modules/SugarChimp/includes/assets/img/dashlet/activity-unsubscribe.png">
                                        {/if}
                                        {if $this.action == "abuse"}
                                            <img src="modules/SugarChimp/includes/assets/img/dashlet/activity-abuse.png">
                                        {/if}
                                        {if $this.action == "sent"}
                                            <img src="modules/SugarChimp/includes/assets/img/dashlet/activity-sent.png">
                                        {/if}
                                        {if $this.action == "queued"}
                                            <img src="modules/SugarChimp/includes/assets/img/dashlet/activity-sent.png">
                                        {/if}
                                        {if $this.action == "ecomm"}
                                            <img src="modules/SugarChimp/includes/assets/img/dashlet/activity-ecomm.png">
                                        {/if}
                                        {if $this.action == "mandrill_send"}
                                            <img src="modules/SugarChimp/includes/assets/img/dashlet/activity-sent.png">
                                        {/if}
                                        {if $this.action == "mandrill_hard_bounce"}
                                            <img src="modules/SugarChimp/includes/assets/img/dashlet/activity-bounced.png">
                                        {/if}
                                        {if $this.action == "mandrill_soft_bounce"}
                                            <img src="modules/SugarChimp/includes/assets/img/dashlet/activity-bounced.png">
                                        {/if}
                                        {if $this.action == "mandrill_open"}
                                            <img src="modules/SugarChimp/includes/assets/img/dashlet/activity-opened.png">
                                        {/if}
                                        {if $this.action == "mandrill_click"}
                                            <img src="modules/SugarChimp/includes/assets/img/dashlet/activity-clicked.png">
                                        {/if}
                                        {if $this.action == "mandrill_spam"}
                                            <img src="modules/SugarChimp/includes/assets/img/dashlet/activity-abuse.png">
                                        {/if}
                                        {if $this.action == "mandrill_unsub"}
                                            <img src="modules/SugarChimp/includes/assets/img/dashlet/activity-unsubscribe.png">
                                        {/if}
                                        {if $this.action == "mandrill_abuse"}
                                            <img src="modules/SugarChimp/includes/assets/img/dashlet/activity-abuse.png">
                                        {/if}
                                    {/if}
                                </div>
                            </div>
                            <div class="activity-info">
                                <h4 class="activity-title">
                                    {if !empty($this.action)}
                                        {if $this.action == "open"}
                                            Opened
                                        {/if}
                                        {if $this.action == "click"}
                                            Clicked
                                        {/if}
                                        {if $this.action == "bounce"}
                                            Bounced
                                        {/if}
                                        {if $this.action == "unsub"}
                                            Unsubscribed
                                        {/if}
                                        {if $this.action == "abuse"}
                                            Abuse
                                        {/if}
                                        {if $this.action == "sent"}
                                            Sent
                                        {/if}
                                        {if $this.action == "queued"}
                                            Queued
                                        {/if}
                                        {if $this.action == "ecomm"}
                                            Purchase
                                        {/if}
                                        {if $this.action == "mandrill_send"}
                                            Send
                                        {/if}
                                        {if $this.action == "mandrill_hard_bounce"}
                                            Hard Bounce
                                        {/if}
                                        {if $this.action == "mandrill_soft_bounce"}
                                            Soft Bounce
                                        {/if}
                                        {if $this.action == "mandrill_open"}
                                            Open
                                        {/if}
                                        {if $this.action == "mandrill_click"}
                                            Clicked
                                        {/if}
                                        {if $this.action == "mandrill_spam"}
                                            Spam
                                        {/if}
                                        {if $this.action == "mandrill_unsub"}
                                            Unsubscribe
                                        {/if}
                                        {if $this.action == "mandrill_abuse"}
                                            Abuse
                                        {/if}
                                    {/if}
                                </h4>
                                <p class="activity-campaign">
                                    {$this.title}
                                </p>
                                {if $this.url}
                                    <p class="activity-email">
                                        <a href="{$this.url}" class="activity-email-link">{$this.url}</a>
                                    </p>
                                {else}
                                    {if $this.campaign_data.archive_url}
                                        <p class="activity-email">
                                            View email: <a href="{$this.campaign_data.archive_url}" class="activity-email-link">{$this.campaign_data.archive_url}</a>
                                        </p>
                                    {/if}
                                {/if}
                            </div>
                        </li>
                    {/foreach}
                </ul>
            {else}
                <div class="block-footer">No data available.</div>
            {/if}
        {else}
            {if !empty($data.message)}
                <p>{$data.message}</p>
            {/if}
        {/if}
    {else}
        <div style="margin:10px; font-weight: bold; text-align: center; color: #58595b;">Loading...</div>
    {/if}
</div>