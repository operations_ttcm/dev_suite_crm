




<link rel="stylesheet" type="text/css" href="modules/SugarChimp/includes/assets/css/ce/bootstrap.css?8.1.1a">
<link rel="stylesheet" type="text/css" href="modules/SugarChimp/includes/assets/css/sugarchimp.css?8.1.1a">
<link rel="stylesheet" type="text/css" href="modules/SugarChimp/includes/assets/css/ce/sugarchimp_sugar6.css?8.1.1a">

<script src="modules/SugarChimp/includes/assets/js/ce/data-privacy.js"></script>
<div class="setup-body sugarchimp-setup" style="background-color:#fff;padding:15px;">
    <div class="row-fluid control-group verticalspace">
        <div class="vertical-scroll" style="background-color:white;">
            <h1 class="tcenter">Data Privacy</h1>
            <p>This tool allows you to completely remove personally identifiable information (PII) that is stored in the SugarChimp modules required to make SugarCRM and MailChimp work together.</p>
            <hr/>
            <p>Enter an email address into the text box and click the "Erase Data" button. After doing this, the following will happen:</p>
            <ul>
                <li>The email address will be removed from SugarChimp-related database tables</li>
                <li>MailChimp Activity records will have the email address field updated with the "erased" value</li>
                <li>In MailChimp, the email address and associated subscriber will be deleted from any MailChimp List they are on</li>
            </ul>
            <br>
            <input type="text" name="sc-erase-email-address" id="sc-erase-email-address" style="width:50%"><button class="button primary sc-erase-data-button">Erase Data</button>
            <div style="width:90%; margin:5px; padding:5px; display:none" id="sc-erase-results" class="info-box"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
