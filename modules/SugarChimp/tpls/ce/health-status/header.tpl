




{literal}
<script type="text/javascript">
    var sugarChimpSelf = {
        confirmed: function(){},
        cancelled: function(){}
    };
</script>
{/literal}

<link rel="stylesheet" type="text/css" href="modules/SugarChimp/includes/assets/css/ce/bootstrap.css?8.1.1a">
<link rel="stylesheet" type="text/css" href="modules/SugarChimp/includes/assets/css/sugarchimp.css?8.1.1a">
<link rel="stylesheet" type="text/css" href="modules/SugarChimp/includes/assets/css/ce/sugarchimp_sugar6.css?8.1.1a">
<div id="include-ce-css"></div>
<script type="text/javascript" src="modules/SugarChimp/includes/assets/js/ce/handlebars.js"></script>
<script type="text/javascript" src="modules/SugarChimp/includes/assets/js/ce/health-status/health-status.js"></script>
<div class="sugarchimp-bootstrap">
	<div class="health-status-container container-fluid main-panel">
		<h1 class="tcenter">
            SugarChimp Health Status  
        </h1>
        <hr/>
        <div class="row-fluid warning-panel tcenter" style="display:none">
            <div class="warning-box info-box">
                <h3 id="warning-header" class="tcenter">This is a Warning Title!</h3>
                <p id="warning-body">This is a Warning Message</p>
                <button class="btn btn-primary" id="warning-confirm">confirm</button>
                <button class="btn btn-danger" id="warning-cancel">cancel</button>
            </div>
        </div>
        <div class="row-fluid loading-panel tcenter" style="display:none">
            <p id="loading-body"></p>
            <hr>
        </div>
	    <div class="row-fluid content-panel">
	        <div class="span12">
