


<link rel="stylesheet" type="text/css" href="modules/SugarChimp/includes/assets/css/ce/bootstrap.css?8.1.1a">
<link rel="stylesheet" type="text/css" href="modules/SugarChimp/includes/assets/css/sugarchimp.css?8.1.1a">
<link rel="stylesheet" type="text/css" href="modules/SugarChimp/includes/assets/css/ce/sugarchimp_sugar6.css?8.1.1a">
<div id="include-ce-css"></div>

{include file="modules/SugarChimp/tpls/ce/health-status/header.tpl"}

<div class="setup-body">
    {foreach from=$errors key=error_key item=error_message}
        <div class="setup-section error">
            <p>{$error_message}</p>
        </div>
    {/foreach}
    <div id="health-main-rendered"class="setup-section">
        
    </div>
</div>

{include file="modules/SugarChimp/tpls/ce/health-status/footer.tpl"}
{literal}
<script id="hbs-render-health-status" type="text/x-handlebars-template">
        <div id='show-sugarchimp-health-status'>
            <p class="alert alert-success health-status-welcome">
                This is where you can find the current state of SugarChimp including 
                what's in queue to sync over next and when. 
                You can find this page by going to Admin - SugarChimp - Health Status at any time.
            </p>
            <table class="table status-table">
                <tbody>
                    <tr>
                        <td width="35%" class="tright">MailChimp Connection:</td>
                        <td>
                            <div id="status-mailchimp-configured-good" style="display:none">
                                 <img src="modules/SugarChimp/includes/assets/img/green-check.png">
                            </div>
                            <div id="status-mailchimp-configured-bad" style="display:none">
                                <img src="modules/SugarChimp/includes/assets/img/red-cross.png">
                                <a href="index.php?module=SugarChimp&action=step3"> Enter MailChimp API Key</a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="tright">SugarChimp License:</td>
                        <td>
                            <div id="status-valid-license-good" style="display:none">
                                 <img src="modules/SugarChimp/includes/assets/img/green-check.png">
                            </div>
                            <div id="status-valid-license-bad" style="display:none">
                                <img src="modules/SugarChimp/includes/assets/img/red-cross.png">
                                <a href="index.php?module=SugarChimp&action=step2"> Revalidate License</a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="tright">SugarChimp Version:</td>
                        <td>
                            <span id="sugarchimp-version"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tright">SugarChimp Edition:</td>
                        <td>
                            <span id="sugarchimp-edition"></span>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table class="table status-table">
                <tbody>
                    <tr>
                        <td width="35%" class="tright" width="40%">Last Ran:</td>
                        <td id="row-status-last-ran">
                            <i class="icon-check hide"></i> <span id="status-last-ran"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tright">Interval:</td>
                        <td>
                            <span id="status-interval"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tright">Scheduler Status:</td>
                        <td id="row-status-scheduler-status">
                            <i class="icon-check hide"></i> <span id="status-scheduler-status"></span>
                            <a href="#" class="scheduler-link">view scheduler
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td class="tright">Your Webhook URL:</td>
                        <td>
                            <span id="sugarchimp-webhook-url"></span>
                            <div class="sugarchimp-webhook-help">
                                <em>
                                This is how MailChimp sends back updates to SugarCRM.
                                Make sure that the URL <strong>is publicly accessible</strong> so that your subscriber updates are sent to SugarCRM.
                                If it already is then there is nothing else that you need to do.
                                The URL is based on the site_url in your config.php.
                                </em>
                            </div>
                        </td>
                    </tr>
                    <tr class="status-summary">
                        <td class="tright">Pending to MailChimp</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="tright">Lists:</td>
                        <td>
                            <span id="to-mailchimp-lists"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tright">Targets/Leads/Contacts:</td>
                        <td>
                            <span id="to-mailchimp-subscribers"></span>
                        </td>
                    </tr>
                    <tr class="status-summary">
                        <td class="tright">Pending from MailChimp</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="tright">Lists:</td>
                        <td>
                            <span id="from-mailchimp-lists"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tright">Subscribers:</td>
                        <td>
                            <span id="from-mailchimp-subscribers"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tright">Initial Import:</td>
                        <td>
                            <span id="mailchimp-initial" class="hide">Job queued and pending</span>
                            <span id="mailchimp-initial-done" class="hide">Done</span>
                        </td>
                    </tr>
                </tbody>
            </table>

    {{#eq current_plan "basic"}}
        <div class="alert alert-danger health-status-welcome">
            <p>
                <strong>You are currently using the Basic Edition of SugarChimp.</strong>
                The Information below is a sample of what's provided in the Professional and Ultimate Editions of SugarChimp.
                The Professional and Ultimate versions of SugarChimp contain the added ability to do the following:
            </p>
            <ul>
                <li>Automatically Sync all Contacts, Targets, or Leads to a Target List</li>
                <li>Sync MailChimp Campaign Activity directly to Sugar</li>
                <li>Allows Reporting on MailChimp Activity</li>
                <li>Sample Report included</li>
            </ul>
            <p><a href="https://store.suitecrm.com/docs/SugarChimp/try-ultimate"><strong>You can try Pro or Ultimate for free.</strong></a></p>
        </div>
    {{/eq}}

            <table class="table status-table">
                <tbody>
                    <tr class="status-summary">
                        <td width="35%" class="tright">Pending Activities from MailChimp</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="tright">Activities to Process:</td>
                        <td>
                            <span id="from-mailchimp-activities"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tright">Pending Activity Import Job:</td>
                        <td>
                            <span id="mailchimp-activity-initial" class="hide">Job queued and pending</span>
                            <span id="mailchimp-activity-initial-done" class="hide">Done</span>
                        </td>
                    </tr>
                    <tr class="status-summary">
                        <td class="tright">SmartList Scheduler Status</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="35%" class="tright" width="40%">Last Ran:</td>
                        <td id="row-status-smartlist-last-ran">
                            <i class="icon-check hide"></i> <span id="status-smartlist-last-ran"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tright">Interval:</td>
                        <td>
                            <span id="status-smartlist-interval"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tright">Scheduler Status:</td>
                        <td id="row-status-smartlist-scheduler-status">
                            <i class="icon-check hide"></i> <span id="status-smartlist-scheduler-status"></span>
                            <a href="#" class="smartlist-scheduler-link">view scheduler</a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="table status-table">
                <tbody>
                    <tr class="status-summary">
                        <td width="35%" class="tright">General Configuration</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="tright">SugarChimp Logger Level:</td>
                        <td>
                            <select name="sugarchimp_logger" id="sugarchimp_logger" class="width-inherit">
                                <option value="normal">Normal</option>
                                <option value="debug">Debug</option>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>

        <style type="text/css">
        #health-status-synced-lists td.list-data {
            font-size: 11px;
        }
        #health-status-synced-lists td.light-note {
            color: #333;
            font-size: 11px;
        }
        </style>

            <table class="table status-table" style="margin-bottom:20px;">
                <tbody>
                    <tr class="status-summary">
                        <td width="35%" class="tright">List Sync Status</td>
                        <td colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4" class="light-note">If your Sugar Target List and MailChimp List are below, this means it's currently setup to be automatically synced. Changes made on one side will be updated on the other. The Sync Actions below are only if you need to manually force a re-sync. This should not be a common task. If you would like to force a re-sync in either direction, use the action dropdowns below next to your list.</td>
                    </tr>
                </tbody>
            </table>

            <table class="table status-table table-striped" id="health-status-synced-lists">
                <tbody></tbody>
            </table>
        </div>
</script>
{/literal}
{include file="modules/SugarChimp/includes/assets/handlebars/synced-lists.hbs"}