




<div class="setup-body">
    <h1 class="tcenter">
        SugarChimp Activity Tracker Table
    </h1>
    <hr/>
    
    {if $message}
        <h3>{$message}</h3>
    {/if}
    
    {if count($data)>0}
        <table class="table status-table" id="sugarchimp-adminactivitytracker">
            <tbody>
                <tr>
                    <td>id</td>
                    <td>mailchimp_list_id</td>
                    <td>mailchimp_campaign_id</td>
                    <td>initial_sync</td>
                    <td>start_date</td>
                    <td>last_sync</td>
                </tr>
                {foreach from=$data key=data_key item=data_item}
                    <tr>
                        <td>{$data_item.id}</td>
                        <td>{$data_item.mailchimp_list_id}</td>
                        <td>{$data_item.mailchimp_campaign_id}</td>
                        <td>{$data_item.initial_sync}</td>
                        <td>{$data_item.start_date}</td>
                        <td>{$data_item.last_sync}</td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    {else}
        <p>There is not data in the SugarChimp Activity Tracker table.</p>
    {/if}
</div>