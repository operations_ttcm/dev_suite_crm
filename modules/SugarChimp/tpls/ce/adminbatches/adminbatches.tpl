




<script src="modules/SugarChimp/includes/assets/js/ce/adminbatches.js"></script>
<div class="setup-body">
    <h1 class="tcenter">
        Batches sent in the last 7 days
    </h1>
    <hr/>
    
    {if $message}
        <h3>{$message}</h3>
    {/if}
    
    {if count($data)>0}
        <table class="table status-table" id="sugarchimp-admin">
            <tbody>
                <tr>
                    <td>&nbsp;</td>
                    <td>id</td>
                    <td>date_entered</td>
                    <td>batch_id</td>
                    <td>mailchimp_list_id</td>
                </tr>
                {foreach from=$data key=data_key item=data_item}
                    <tr>
                        <td><button class="sc-get-batch" id="{$data_item.batch_id}">Show</button></td>
                        <td>1{$data_item.id}</td>
                        <td>2{$data_item.date_entered}</td>
                        <td>3{$data_item.batch_id}</td>
                        <td>4{$data_item.mailchimp_list_id}</td>
                    </tr>
                    <tr>
                        <td colspan="5" id="dump-{$data_item.batch_id}" class="tcenter" style="display:none">
                        </td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    {else}
        <p>No batches in the last 7 days.</p>
    {/if}
</div>