




$(document).ready(function(){
	//declare click events
	// 'click .smartlist-nav li a':'smartlistTabToggle',
	$('#smarlist-setup-form').on("keypress", ":input:not(textarea)", function(event) {
    	return event.keyCode != 13;
	});

	// 'click #smartlist-cancel-new-mailchimp-list-button':'smartlistCancelNewMailChimpListButton',
	$('#smarlist-setup-form').on('click','.sc-upgrade-link',function(evt){
		switchToUpgradeOptions(evt);
		return false;
	});

	$('#smarlist-setup-form').on('click','.smartlist-nav li a',function(evt){
		smartlistTabToggle(evt);
		return false;
	});

    // 'click #smartlist-create-new-sugar-list-button':'smartlistCreateNewSugarListButton',
	$('#smarlist-setup-form').on('click','#smartlist-create-new-sugar-list-button',function(evt){
		smartlistCreateNewSugarListButton(evt);
		return false;
	});    
	// 'click #smartlist-create-new-mailchimp-list-button':'smartlistCreateNewMailChimpListButton',
	$('#smarlist-setup-form').on('click','#smartlist-create-new-mailchimp-list-button',function(evt){
		smartlistCreateNewMailChimpListButton(evt);
		return false;
	});
    // 'click .createNewFilterRow':'createNewFilterRow',
	$('#smarlist-setup-form').on('click','.createNewFilterRow',function(evt){
		createNewFilterRow(evt);
		return false;
	});
	// 'click #smartlist-cancel-new-sugar-list-button':'smartlistCancelNewSugarListButton',
	$('#smarlist-setup-form').on('click','#smartlist-cancel-new-sugar-list-button',function(evt){
		smartlistCancelNewSugarListButton(evt);
		return false;
	});
	// 'click #smartlist-cancel-new-mailchimp-list-button':'smartlistCancelNewMailChimpListButton',
	$('#smarlist-setup-form').on('click','#smartlist-cancel-new-mailchimp-list-button',function(evt){
		smartlistCancelNewMailChimpListButton(evt);
		return false;
	});
	// 'click #premapping-sync':'preMappingSync',
	$('#smarlist-setup-form').on('click','#premapping-sync',function(evt){
		preMappingSync(evt);
		return false;
	});
	// 'click .remove-filter':'removeFilter',
	$('#smarlist-setup-form').on('click','.remove-filter',function(evt){
		removeFilter(evt);
		return false;
	});
	// 'click .test-query':'testQuery',
	$('#smarlist-setup-form').on('click','.test-query',function(evt){
		testQuery(evt);
		return false;
	});
	// 'click .test-filters':'testFilters',
	$('#smarlist-setup-form').on('click','.test-filters',function(evt){
		testQuery(evt);
		return false;
	});
    // 'click .smartlist-hide':'smartListHide',
	$('#smarlist-setup-form').on('click','.smartlist-hide',function(evt){
		smartListHide(evt);
		return false;
	});
    // 'click .smartlist-show':'smartListShow',
	$('#smarlist-setup-form').on('click','.smartlist-show',function(evt){
		smartListShow(evt);
		return false;
	});

	//declare change events
	// 'change .join-dropdown':'smartlistFilterJoinDropDownChange',
	$('#smarlist-setup-form').on('change','.join-dropdown',function(evt){
		smartlistFilterJoinDropDownChange(evt);
		return false;
	});

	// 'change .smartlist-filter-available-fields-dropdown':'smartlistFilterAvailableFieldsDropDownChange',
	$('#smarlist-setup-form').on('change','.smartlist-filter-available-fields-dropdown',function(evt){
		smartlistFilterAvailableFieldsDropDownChange(evt);
		return false;
	});

	// 'change .smartlist-filter-operands-dropdown':'smartlistFilterOperandsDropDownChange',
	$('#smarlist-setup-form').on('change','.smartlist-filter-operands-dropdown',function(evt){
		smartlistFilterOperandsDropDownChange(evt);
		return false;
	});

	// 'change input:radio[class=smartlist-mode-selection]':'smartlistModuleModeToggle',
	$('#smarlist-setup-form').on('change','input:radio[class=smartlist-mode-selection]',function(evt){
		smartlistModuleModeToggle(evt);
		return false;
	});

	// 'change #smartlist-sugar-list-id':'smartlistSugarListChange',
	$('#smarlist-setup-form').on('change','#smartlist-sugar-list-id',function(evt){
		smartlistSugarListChange(evt);
		return false;
	});

	// 'change #smartlist-mailchimp-list-id':'smartlistMailChimpListChange',
	$('#smarlist-setup-form').on('change','#smartlist-mailchimp-list-id',function(evt){
		smartlistMailChimpListChange(evt);
		return false;
	});
	initializeStep();
});
/**************************************
    SmartList Initialization Functions
**************************************/
	function initializeStep() {
		// get data for smartlist view
		showLoadingBox("Loading SmartList Settings ");
	    $('.step1').hide();
	    // set some default member variables
	    sugarChimpSelf.sugarListId = false;
	    sugarChimpSelf.removeSugarPairWarned = false;
	    sugarChimpSelf.removeMailChimpPairWarned = false;
	    sugarChimpSelf.mailChimpListId = false;
	    sugarChimpSelf.supported_modules = [];
	    sugarChimpSelf.current_plan = false;
	    sugarChimpSelf.data = {};
	    $.ajax({
		    url: 'index.php',
		    method: 'GET',
		    dataType: 'json',
		    data: {
		        module: 'SugarChimp',
		        action: 'smartlist_setup',
		    },
	        success: function(data,response) {
	        	hideLoadingBox();
	            sugarChimpSelf.data = data;
	            for(var key in sugarChimpSelf.data.available_fields){
	                sugarChimpSelf.supported_modules.push(key);
	            }

				if (data.current_plan){
					sugarChimpSelf.current_plan = data.current_plan;
				}

	            renderStep();
	        }
	    }, {});
	}
	function renderStep() {
		var step4_html = renderViewData("hbs-render-step4",null,sugarChimpSelf);
		$('#hbs-render-step4').remove();
		$("#smarlist-setup-form").html(step4_html);

		$('#sugarchimp-setup-status').html(sugarChimpSelf.message);
        $('div.disabled-text div.sync-option-details-div').css("display", "block");

	    // make sure we have data before doing anything with the data
	    if (sugarChimpSelf.data){
	        // render the dyanmic dropdowns
	        $('.step1').show();
	        renderSugarListDropdown(sugarChimpSelf.data);
	        renderMailChimpListDropdown(sugarChimpSelf.data);
	        renderCreateSubscribersDropdown(sugarChimpSelf.supported_modules);
	        $('.step2').hide();
	        $('.smartlist-has-sugar-lists-check').hide();
	        if (sugarChimpSelf.data.new_sugar_list){
	            // there were no sugar lists, but we auto-created one for them
	            $('#no-target-list').show();

	            // we're going to sync with the new list we just created
	            // start showing the next step with the mailchimp lists
	            if (sugarChimpSelf.data.new_sugar_list.id){
	                smartlistSugarListChange(null,sugarChimpSelf.data.new_sugar_list.id);
	            }else{
	                // problems
	            }
	        }else if(sugarChimpSelf.data.sugar_lists && sugarChimpSelf.data.sugar_lists.row_count && sugarChimpSelf.data.sugar_lists.row_count>0){
	            // there were existing sugar lists
	            $('#has-target-list').show();
	        }else{
	            // this is the bail out case
	            // if there are no sugar lists and auto-creating a sugar list fails (should rarely happen)
	            // give the ability to create a list
	            $('#no-target-list').show();
	            $('#smartlist-create-new-sugar-list').show();
	        }
	    }else{
	    	//no data yet
	    }
	}


/**************************************
    SmartList General Functions
**************************************/

	//remove starter, uncollapse, and show step class
	function smartListShow(evt, select){
	    if(!select){
	        var $el;
	        $el = this.$(evt.currentTarget);
	        select = $el.attr('data-show');
	        if(select ==='.step3');
            {
                $('div.disabled-text div.sync-option-details-div').css("display", "block");
            }
	    }
	    $(select).show();
	    return false;
	}
	function smartListHide(evt, select) {
	    if(!select){
	        var $el;
	        $el = this.$(evt.currentTarget);
	        var select = $el.attr('data-hide');
	    }
		$(select).hide();
	    return false;
	}
	function goBackTo(step){
	    // implement the ability to go back to
	    // steps other than 1 or 2 here
	    if(step > 0 && step < 6)
	    {
	        for(var i = 5; i > 2; i--)
	        {
	            select = '#hideable-step-' + i;
	            smartListHide(null, select);
	        }
	        //$('#step' + step + "-start").show();
	    }
	}
	function switchToUpgradeOptions(evt) {
        var $el;
        $el = this.$(evt.currentTarget);
        var plan = $el.attr('data-plan');
		goToStep(2);
	    return false;
	}
/**************************************
		SmartList Rendering
**************************************/

	function renderSugarListDropdown(data,defaultValue){
	    $('#smartlist-sugar-list-id').remove();
	    data.id="smartlist-sugar-list-id";
	    data.name="smartlist-sugar-list-id";
	    var html = renderViewData('hbs-sugar-lists','SugarChimp',data);
	    $('#step1-dropdown').append(html);
	    if(defaultValue){
	        smartlistSetSugarList(defaultValue);
	        $('#smartlist-sugar-list-id').val(defaultValue);
	    }
	}
	function renderMailChimpListDropdown(data,defaultValue){
	    $('#smartlist-mailchimp-list-id').remove();
	    data.id="smartlist-mailchimp-list-id";
	    data.name="smartlist-mailchimp-list-id";
	    var html = renderViewData('hbs-mailchimp-lists','SugarChimp',data);
	    $('#step2-dropdown').append(html);
	    if(defaultValue){
	        smartlistSetMailChimpList(defaultValue);
	        $('#smartlist-mailchimp-list-id').val(defaultValue);
	    }
	}

/**************************************
SmartList Steps 1 and 2 
**************************************/

	function smartlistSugarListChange(evt,id){
	    var currentVal;
	    var $el;
	    if (id){
	        currentVal = id;
	    }else{
	        $el = this.$(evt.currentTarget);
	        currentVal = $el.val();
	        //warn if any changes could be lost?


	        // hide all mailchimp list selection stuff, a sugar list must be selected first
	        sugarChimpSelf.removeSugarPairWarned = false;
	        sugarChimpSelf.removeMailChimpPairWarned = false;
	        goBackTo(1);
	    }
	    //confirmationAlert('sugar_change_'+id,'SugarList Changed','Are you good with that?');
	    // hide create new sugar list dialog and all step2
	    smartListHide(null,'#smartlist-create-new-sugar-list');
	    smartListHide(null,'.step2');
	    
	    // hide step3 starter button
	    smartListHide(null,'#step3-start');
	    
	    // reset the mailchimp list dropdown, it will get set to the proper value later
	    smartlistSetMailChimpList('');

	    if (currentVal == ''){
	        // nothing selected
	    }else if (currentVal == 'new'){
	        // create new list option is selected, make the list
	        $('#smartlist-create-new-sugar-list').show();
	        $('#smartlist-create-new-sugar-list-name').focus().select();
	    }else{
	        // specific sugar list selected
	        smartlistSetSugarList(currentVal);

	        // depending on the mailchimp list situation, do something
	        var min_height = $('#step1-description > p').height();

	        // $('.step2').hide();
	        $('.syncing-arrows').hide();
	        $('#step2-description > p').css({'height': min_height});
	        // see if the select resulted in a synced sugar/mc list
	        var mcListId = (((((sugarChimpSelf||{}).data||{}).sugar_lists||{}).list||{})[currentVal]||{}).mailchimp_list_name_c;
	        var mcList = (((((sugarChimpSelf||{}).data||{}).mailchimp_lists||{}).lists||{})[mcListId]||{});
            var subscribers = (((((sugarChimpSelf||{}).data||{}).sugar_lists||{}).list||{})[currentVal]||{}).mailchimp_default_module_c;

	        if (mcListId && mcListId==mcList.id){
	            // we have a valid mc list id and the mailchimp list object data
	            // the linked MC list exists, show the MC lists and select the synced mailchimp list
	            $('#synced-mailchimp-list').show();
	            $('#has-mailchimp-list').hide();
	            $('#no-mailchimp-list').hide();
	            $('#two-way-arrows').show();
	            $('one-way-arrow').hide();
                setCreateSubscriberAsDropdown(subscribers);
	            smartlistMailChimpListChange(null,mcListId);
	        }else if (sugarChimpSelf.data.new_mailchimp_list){
	            // if no mailchimp lists existed and one was created
	            $('#synced-mailchimp-list').hide();
	            $('#has-mailchimp-list').hide();
	            $('#no-mailchimp-list').show();
	            $('one-way-arrow').hide();
	            $('#two-way-arrows').show();
	            smartlistMailChimpListChange(null,sugarChimpSelf.data.new_mailchimp_list.id);
	        }else if(sugarChimpSelf.data.mailchimp_lists != false){
	            // if there were existing mailchimp lists
	            $('#synced-mailchimp-list').hide();
	            $('#has-mailchimp-list').show();
	            $('#no-mailchimp-list').hide();
	            $('#one-way-arrow').show();
	            $('#two-way-arrows').hide();
	        }else if(sugarChimpSelf.data.mailchimp_lists===false){
	            // there was an error grabbing the lists, maybe the connction between Sugar/MailChimp didn't work.
	            showLoadingBox("Error Grabbing MailChimp Lists ");
	        }else{
	            // this is the bail out case
	            // if there are no mailchimp lists and auto-creating a mailchimp list fails (should rarely happen)
	            // give the ability to create a list
	        }
	        smartListShow(null, '.step2');
	    }
	}
	function smartlistMailChimpListChange(evt,id){
	    var currentVal;
	    var $el;
	    if (id){
	        currentVal = id;
	        confirmMailChimpListChange(currentVal,false);
	    }else{
	        $el = this.$(evt.currentTarget);
	        currentVal = $el.val();
	        
	        // Check if we need to warn user
	        // 1: Is this Sugar List already paired with a MC list?
	        var sugar_warn = sugarChimpSelf.data.sugar_lists.list[sugarChimpSelf.sugarListId] && 
	            sugarChimpSelf.data.sugar_lists.list[sugarChimpSelf.sugarListId].mailchimp_list_name_c &&  
	            sugarChimpSelf.data.sugar_lists.list[sugarChimpSelf.sugarListId].mailchimp_list_name_c != currentVal &&       
	            sugarChimpSelf.removeSugarPairWarned === false;
	        // Check if we need to warn user
	        // 2: Is this MC List already paired with a different Sugar list?
	        if(sugar_warn){
	            sugarChimpSelf.removeSugarPairWarned = true;
	            smartlistShowSugarWarning(currentVal);
	            //let Box decide from here
	            return;
	        }
	        else if(getSugarListPairedWithMailChimp(currentVal) &&
	                getSugarListPairedWithMailChimp(currentVal) != sugarChimpSelf.sugarListId){
	            sugarChimpSelf.removeMailChimpPairWarned = true;
	            smartlistShowMailChimpWarning(currentVal);
	            //let Box decide from here
	            return;
	        }
	        else{
	            confirmMailChimpListChange(currentVal,true);
	        }
	    }
	}
	function countMailChimpSubscribers(id){
	    if(sugarChimpSelf.data.mailchimp_lists.lists &&
	    	sugarChimpSelf.data.mailchimp_lists.lists[id] && 
	    	sugarChimpSelf.data.mailchimp_lists.lists[id].stats &&
	    	sugarChimpSelf.data.mailchimp_lists.lists[id].stats.member_count) {
	        return sugarChimpSelf.data.mailchimp_lists.lists[id].stats.member_count;
	    }
	    return 0;
	}
	function confirmMailChimpListChange(id, goBack){
	    if(goBack === true){
	        goBackTo(2);
	    }
	    smartListHide(null,'#smartlist-create-new-mailchimp-list');
	    smartListHide(null,'#smartlist-sync-from-mailchimp-div');        
	    smartListHide(null,'#synced-mailchimp-list'); 
	    smartListHide(null,'#step3-start');
	    smartListShow(null,'#has-mailchimp-list'); 
	    
	    if (id == ''){
	        // nothing selected
	        $('#one-way-arrow').show();
	        $('#two-way-arrows').hide();
	    }else if (id == 'new'){
	        // create new list option is selected, make the list
	        $('#smartlist-create-new-mailchimp-list').show();
	        $('#smartlist-create-new-mailchimp-list-name').focus().select();
	        $('#two-way-arrows').hide();
	        $('#one-way-arrow').show();
	    }else{
	        // specific mc list selected

	        smartlistSetMailChimpList(id);
	        var subs = countMailChimpSubscribers(id);

            var already_synced = (sugarChimpSelf.data.sugar_lists.list[sugarChimpSelf.sugarListId] && 
              					  sugarChimpSelf.data.sugar_lists.list[sugarChimpSelf.sugarListId].mailchimp_list_name_c)

            $('#smartlist-sync-from-mailchimp-div').show();
	        $('#one-way-arrow').hide();
	        $('#two-way-arrows').show();
	        smartListShow(null,'#step3-start');
	    }
	}
	function smartlistShowMailChimpWarning(id){
	    confirmationAlert(
	        //confirm function
	        function(){
	            confirmMailChimpListChange(id,true);
	        }.bind(sugarChimpSelf),
	        //cancel function
	        function(){
	            smartlistSetMailChimpList(sugarChimpSelf.mailChimpListId);
	        }.bind(sugarChimpSelf),
	        //id
	        'mc_changed',
	        //title
	        'Warning!',
	        //message
	        'This <span style="font-weight: 800">MailChimp List</span> has already been paired with a different Sugar Target List. Choosing this MailChimp List will remove its previous pairing. Do you still want to continue? <br>(<em>no final change takes place until you save after step 4</em>)'
	        );
	}
	function smartlistShowSugarWarning(id){
	    confirmationAlert(
	        //confirm function
	        function(){
	            if(getSugarListPairedWithMailChimp(id) &&
	                getSugarListPairedWithMailChimp(id) != sugarChimpSelf.sugarListId){
	                smartlistShowMailChimpWarning(id);
	            }
	            else{
	                confirmMailChimpListChange(id);
	            }
	        }.bind(sugarChimpSelf),
	        //cancel function
	        function(){
	            smartlistSetMailChimpList(sugarChimpSelf.mailChimpListId);
	        }.bind(sugarChimpSelf),
	        //id
	        'mc_changed',
	        //title
	        'Warning!',
	        //message
	        'This <span style="font-weight: 800">Sugar List</span> is already paired with a MailChimp List. Changing this box will remove the previous pairing. <br>Do you still want to continue? <br>(<em>no final change takes place until you save after step 4</em>)'
	        );
	}
	function smartlistSetSugarList(id){
	    $('#smartlist-sugar-list-id').val(id); // this will not fire the dropdown change event listener
	    $('#set-sugar-list-id').val(id);
	    sugarChimpSelf.sugarListId = id;
	    // populates the SmartList Settings and filters
	        renderSavedMode(id);
	        renderSavedFilters(id);
	        renderSavedSQL(id);
	}
	function smartlistSetMailChimpList(id){
	    $('#smartlist-mailchimp-list-id').val(id); // this will not fire the dropdown change event listener
	    $('#set-mailchimp-list-id').val(id);
	    sugarChimpSelf.mailChimpListId = id;
	}
	function smartlistCreateNewSugarListButton(evt){
	    $('#smartlist-create-new-sugar-list-name').removeClass('error');
	    var listName = $('#smartlist-create-new-sugar-list-name').val();
	    
	    // list name is required
	    if (!listName){
	        $('#smartlist-create-new-sugar-list-name').addClass('error');
	        // Handle Error Messages
	        // 'You must add a name to create the List'
	        return false;
	    }
	    // create the list, reload dropdowns, select the newly created list in the dropdown
	    // let user know what just happened
		showLoadingBox("Create new Sugar List ");	    

		$.ajax({
		    url: 'index.php',
		    method: 'GET',
		    dataType: 'json',
		    data: {
		    	// createSugarList in API
		        module: 'SugarChimp',
		        action: 'create_sugar_list',
		        listName: listName
		    },
	        success: function(data,response) {
	        	hideLoadingBox();
	            if (data && data.success){
	                //add a 'success' message
	                //"List Created Successfully"

	                // list created
	                // add new list to sugar target list dropdown
	                sugarChimpSelf.data.sugar_lists.list[data.list.id] = data.list;
	                sugarChimpSelf.data.sugar_lists.row_count++;

	                // rebuild sugar dropdown and select it from the dropdown
	                renderSugarListDropdown(sugarChimpSelf.data,data.list.id);
	                smartlistSugarListChange(null,data.list.id);
	            } else {
	                // list failed to create
	                
	                // Handle SugarChimp Save Error
	                
	            }
	        }
	    });

	    // do not submit form
	    return false;
	}
	function smartlistCreateNewMailChimpListButton(evt){
	    $('#smartlist-create-new-mailchimp-list-name').removeClass('error');
	    var listName = $('#smartlist-create-new-mailchimp-list-name').val();
	    
	    // list name is required
	    if (!listName){
	        $('#smartlist-create-new-mailchimp-list-name').addClass('error');
	        // Handle Error
	        // 'No MailChimp Name'
	        return false;
	    }

	    // create the list, reload dropdowns, select the newly created list in the dropdown
	    // let user know what just happened
		showLoadingBox("Saving MailChimp List ");

		$.ajax({
		    url: 'index.php',
		    method: 'GET',
		    dataType: 'json',
		    data: {
		    	// createMailChimpList in API
		        module: 'SugarChimp',
		        action: 'create_mailchimp_list',
		        listName: listName
		    },
	        success: function(data,response) {
	            hideLoadingBox();
	            if (data && data.success){
	                //add a 'success' message
	                //"List Created Successfully"

	                // list created
	                // add new list to sugar target list dropdown
	                sugarChimpSelf.data.mailchimp_lists.lists[data.list.id] = data.list;
	                sugarChimpSelf.data.mailchimp_lists.total_items++;

	                // rebuild mailchimp dropdown and select it from the dropdown
	                renderMailChimpListDropdown(sugarChimpSelf.data,data.list.id);
	                smartlistMailChimpListChange(null,data.list.id);
	            } else {
	                // list failed to create
	                
	                // Handle MailChimp list creation Error
	            }
	        }
	    });

	    // do not submit form
	    return false;
	}
	function smartlistCancelNewSugarListButton(evt){
	    $('#smartlist-create-new-sugar-list').hide();
	    smartlistSetSugarList('');
	    // do not submit form
	    return false;
	}
	function smartlistCancelNewMailChimpListButton(evt){
	    $('#smartlist-create-new-mailchimp-list').hide();
	    if(sugarChimpSelf.mailChimpListId && 
	            sugarChimpSelf.mailChimpListId != ''){
	        confirmMailChimpListChange(sugarChimpSelf.mailChimpListId,false);
	    }
	    // do not submit form
	    return false;
	}
	function getSugarListPairedWithMailChimp(id){
	    for(var sugar_id in sugarChimpSelf.data.sugar_lists.list)
	    {
	        if(sugarChimpSelf.data.sugar_lists.list[sugar_id].mailchimp_list_name_c == id){
	            return sugar_id;
	        }
	    }
	    return false;
	}

/**************************************
SmartList Step 3 Custom Filter
**************************************/

	//currently only focuses for input boxes
	function filterFocusHandler(evt) {
	    var $el;
	    $el = this.$(evt.currentTarget);
	    $el.select();
	}
	function getNumberOfFilters(module) {
	    return $('.smartlist-filter-row-' + module).length;
	}
	function renderSavedMode(list_id) {
	    // foreach supported module
	    for(i=0; i < sugarChimpSelf.supported_modules.length; i++)
	    {
	        var module = sugarChimpSelf.supported_modules[i];
	        //leave it as default mode if none is set
	        var mode = 'none';
	        if(sugarChimpSelf.data.sugar_lists.list[list_id] && 
	            sugarChimpSelf.data.sugar_lists.list[list_id].smartlist &&
	            sugarChimpSelf.data.sugar_lists.list[list_id].smartlist.metadata &&
	            sugarChimpSelf.data.sugar_lists.list[list_id].smartlist.metadata[module] &&
	            sugarChimpSelf.data.sugar_lists.list[list_id].smartlist.metadata[module].mode){
	            //set mode if it is preset
	            var mode = sugarChimpSelf.data.sugar_lists.list[list_id].smartlist.metadata[module].mode;
	        }

	        $('#smartlist-'+module+'-mode-'+mode).prop('checked', true);   
	        smartlistModuleModeToggle(null,$('#smartlist-'+module+'-mode-'+mode));
	    }
	}
	function renderSavedFilters(list_id) {
	    // foreach supported module
	    for(i=0; i < sugarChimpSelf.supported_modules.length; i++)
	    {
	        var module = sugarChimpSelf.supported_modules[i];
	        // need to clear all current filters first
	        clearCustomFilters(module);
	        // then
	        // add saved filters
	        if(sugarChimpSelf.data.sugar_lists.list[list_id] && 
	            sugarChimpSelf.data.sugar_lists.list[list_id].smartlist &&
	            sugarChimpSelf.data.sugar_lists.list[list_id].smartlist.metadata &&
	            sugarChimpSelf.data.sugar_lists.list[list_id].smartlist.metadata[module] &&
	            sugarChimpSelf.data.sugar_lists.list[list_id].smartlist.metadata[module].smart &&
	            sugarChimpSelf.data.sugar_lists.list[list_id].smartlist.metadata[module].smart.filters){
	            //add a filter row for each saved filter
	            for(filter in sugarChimpSelf.data.sugar_lists.list[list_id].smartlist.metadata[module].smart.filters){
	                createNewFilterRow(null, module, sugarChimpSelf.data.sugar_lists.list[list_id].smartlist.metadata[module].smart.filters[filter]);
	            }
	        }
	        // if no saved filters, add a default one
	        if(getNumberOfFilters(module) == 0){
	            createNewFilterRow(null, module);
	        }
	        else if(getNumberOfFilters(module) > 1){
	            smartlistFilterJoinDropDownChange(null,module,sugarChimpSelf.data.sugar_lists.list[list_id].smartlist.metadata[module].smart.join);
	        }
	    }
	}
	function renderSavedSQL(list_id) {
	    // foreach supported module
	    for(i=0; i < sugarChimpSelf.supported_modules.length; i++)
	    {
	        var module = sugarChimpSelf.supported_modules[i];            
	        // then
	        // add saved sql
	        if(sugarChimpSelf.data.sugar_lists.list[list_id] && 
	            sugarChimpSelf.data.sugar_lists.list[list_id].smartlist &&
	            sugarChimpSelf.data.sugar_lists.list[list_id].smartlist.metadata &&
	            sugarChimpSelf.data.sugar_lists.list[list_id].smartlist.metadata[module] &&
	            sugarChimpSelf.data.sugar_lists.list[list_id].smartlist.metadata[module].sql &&
	            sugarChimpSelf.data.sugar_lists.list[list_id].smartlist.metadata[module].sql.initial_sql){
	            //populate sql box
	            var sql = sugarChimpSelf.data.sugar_lists.list[list_id].smartlist.metadata[module].sql.initial_sql;
	            $("#smartlist-custom-sql-"+module).val(sql);
	        }
            else{
                //no saved sql
                sql = getGenericSQL(module);
                $("#smartlist-custom-sql-"+module).val(sql);
            }
	    }
	}
	function getGenericSQL(module)
    {
        module = module.toLowerCase();
        var sql = 'SELECT id FROM ' + module + ' WHERE ';
        if(module === 'leads')
        {
            sql += "lead_source='Cold Call' ";
        }
        else
        {
            sql += "title='CEO' ";
        }
        return sql;
    }
	function renderCreateSubscribersDropdown(supported_modules)
	{
	    if(supported_modules)
	    {
	        def_modules = {};
	        for (var i = 0; i < supported_modules.length; i++) {
	            var module = supported_modules[i];
	            def_modules[module] = {
	                name: module,
	                label: module,
	            };
	            if(module === "Prospects"){
	                def_modules[module].label = 'Targets';
	            }
	        }
	        var data = {
	            label: {
	                class: "",
	                id: "create-subscriber-dropdown-label",
	                text: "Create New Subscribers as: ",
	            },
	            select: {
	                class: "",
	                id: "create-subscriber-as-dropdown",
	                name: 'create-subscriber-as',
	            },
	            options: def_modules,
	        };
	        var drop_html = renderViewData('hbs-generic-dropdown','SugarChimp',data);
	        $("#create-subscriber-dropdown").append(drop_html);
	    }
	}
	function clearCustomFilters(module){
	    if(!module || module =='') {
	       return false;
	    }
	    var table_id = '#smartlist-filters-table-'+module;
	    var $table = $(table_id);
	    $table.children( 'tr:not(:first)' ).remove();
	}
	function createNewFilterRow(evt, module, presetValues) {
	    if(evt){
	        var $el;
	        $el = this.$(evt.currentTarget);
	        module = $el.attr('data-module');
	        presetValues = null;
	    }
	    smartlistFilterNewRow(module, presetValues);
	    // do not submit the form on click
	    return false;
	}
	function smartlistFilterNewRow(module, presetValues) {
	    //add filter-row.hbs into filters div

	    if(!module || module =='') {
	       return false;
	    }
	    var table_id = '#smartlist-filters-table-'+module;
	    var $table = $(table_id);
	    var id = '0';
	    var add_join = false;
	    if(getNumberOfFilters(module) != 0){
	        var high_id = parseInt($(table_id + ' tr:last').attr('data-id'));
	        id = high_id + 1;
	        if($('select.join-dropdown').length > 0)
	        {
	            add_join = $('select.join-dropdown').val()
	        }
	        else
	        {
	            add_join = 'or';
	        }
	    }

	    if(!presetValues) {
	        presetValues = null;
	    }
	    var data = {
	        id: id,
	        module: module,
	        join: add_join,
	    };

	    var row_html = renderViewData('hbs-filter-row','SugarChimp',data);
	    $table.append(row_html);
	    smartlistFilterCreateAvailableFieldsDropDown(module, id, presetValues);
	}
	function smartlistFilterJoinDropDownChange(evt,module, join){
	    if (evt){
	        $el = this.$(evt.currentTarget);
	        module = $el.attr('data-module');
	        join = $el.val();
	    }
	    $('.join-dropdown-' + module).val(join);
	}
	// smartlist-filter-available-fields-dropdown
	function smartlistFilterCreateAvailableFieldsDropDown(module, id, presetValues) {
	    if(!module || module =='' || !isInt(id)) {
	       return false;
	    }

	    var data = {
	        defaultlabel: 'Choose a Field',
	        module: module,
	        id: id,
	        column: 'available-fields',
	        columnName: 'field',            
	        options: sugarChimpSelf.data.available_fields[module],
	    };

	    if(presetValues && presetValues['field']!=''){
	        var preset_field = presetValues['field'];
	        data.value = preset_field;

	        if (sugarChimpSelf.data.available_fields[module] && sugarChimpSelf.data.available_fields[module][preset_field] && sugarChimpSelf.data.available_fields[module][preset_field].type){
	            // what if the saved field does not have an operand
	            type = sugarChimpSelf.data.available_fields[module][preset_field].type;
	            setSelectedFieldType(module,id,type);
	            smartlistFilterCreateOperandsDropDown(module, id, preset_field, type, presetValues);
	        }
	    }

	    var field_html = renderViewData('hbs-filter-dropdown','SugarChimp',data);
	    var row_id = '#smartlist-filter-row-' + module + '-' + id;
	    $(row_id + '> #smartlist-filter-td-available-fields').append(field_html);
	}
	//smartlist-filter-{{column}}-dropdown-{{data.module}}-{{data.id}}   
	function smartlistFilterSetAvailableFieldsDropDown(module, id, value){
	    $('#smartlist-filter-available-fields-dropdown-' + module + '-' + id).val(value);
	}
	function smartlistFilterAvailableFieldsDropDownChange(evt,field){

	    var currentVal;
	    var $el;
	    if (field){
	        currentVal = field;
	    }else{
	        $el = this.$(evt.currentTarget);
	        field = $el.val();
            var module = $el.attr('data-module');
            var id = $el.attr('data-rowid');

	    	if(field){
	            var type = sugarChimpSelf.data.available_fields[module][field].type;
	            setSelectedFieldType(module,id,type);
	            smartlistFilterCreateOperandsDropDown(module, id, field, type);
	        }
	        else{
	            smartlistFilterClearOperandsDropDown(module,id);
	        }
	        smartlistFilterClearValueElement(module,id);
	    }
	}
	function setSelectedFieldType(module,id,type){
	    if (!module || !id || !type) return false;
	    $('#smartlist-filter-type-'+module+'-'+id).val(type);
	}
	// smartlist-filter-operands-dropdown
	function smartlistFilterCreateOperandsDropDown(module, id, field, field_type, presetValues) {
	    var no; 
	    if(!module || module =='' || !isInt(id)) {
	         return false;
	    }
	    
	    if(!sugarChimpSelf.data.fields_info[field_type] || !sugarChimpSelf.data.fields_info[field_type].operands) {
            field_type = 'varchar';
        }

	    var data = {
	        defaultlabel: 'Choose an Operand',
	        module: module,
	        id: id,
	        column: 'operands',
	        columnName: 'operand',
	        fieldname: field,
	        options: sugarChimpSelf.data.fields_info[field_type].operands
	    };

	    if(presetValues && presetValues['operand']!=''){
	        var preset_operand = presetValues['operand'];
	        data.value = preset_operand;

	        if (field && field_type && sugarChimpSelf.data.fields_info[field_type] && 
	        	sugarChimpSelf.data.fields_info[field_type]['operands'] && 
	        	sugarChimpSelf.data.fields_info[field_type]['operands'][preset_operand] && 
	        	sugarChimpSelf.data.fields_info[field_type]['operands'][preset_operand].display_type){
	            // if the empty value or default empty value field is selected, we don't want it to select anyhi
	            var display_type = sugarChimpSelf.data.fields_info[field_type]['operands'][preset_operand].display_type;
	            smartlistFilterCreateValueElement(module, id, field, display_type, presetValues);
	        }
	    }

	    var ops_html =  renderViewData('hbs-filter-dropdown','SugarChimp',data);

	    var row_id = '#smartlist-filter-row-' + module + '-' + id;
	    
	    $(row_id + '> #smartlist-filter-td-operands').empty();
	    $(row_id + '> #smartlist-filter-td-operands').append(ops_html);
	}
	// smartlist-filter-operands-dropdown
	function smartlistFilterClearOperandsDropDown(module, id) {
	    if(!module || module =='' || !isInt(id)) {
	         return false;
	    }
	    var row_id = '#smartlist-filter-row-' + module + '-' + id;
	    
	    $(row_id + '> #smartlist-filter-td-operands').empty();
	    $(row_id + '> #smartlist-filter-td-value').empty();
	}
	//module, id
	function smartlistFilterSetOperandDropDown(module, id, value)
	{
	    $('#smartlist-filter-operand-dropdown-' + module + '-' + id).val(value);
	}
	function smartlistFilterOperandsDropDownChange(evt,operand){
	    var currentVal;
	    var $el;
	    if (operand){
	        currentVal = operand;
	    }else{
	        $el = this.$(evt.currentTarget);
	        var operand = $el.val();
            var module = $el.attr('data-module');
            var id = $el.attr('data-rowid');
            if(operand){
	            var field = $el.attr('data-field');
	            
	            var field_type = sugarChimpSelf.data.available_fields[module][field].type;
                if(!sugarChimpSelf.data.fields_info[field_type] || !sugarChimpSelf.data.fields_info[field_type].operands) {
                    field_type = 'varchar';
                }   

                var display_type = sugarChimpSelf.data.fields_info[field_type]['operands'][operand].display_type;;
                if(!sugarChimpSelf.data.fields_info[field_type]['operands'][operand].display_type) {
                    display_type = 'input';
                }	            
	            smartlistFilterCreateValueElement(module, id, field, display_type);
	        }
	        else{
	            smartlistFilterClearValueElement(module,id);
	        }
	    }
	}
	//create empty, input, dropdown, or multiselect
	function smartlistFilterCreateValueElement(module, id, field, display_type, presetValues) {
	    if(!module || module =='' || !isInt(id) || !display_type || display_type =='') {
	        return false;
	    }

	    var options = [];
	    if(display_type == 'dropdown' || display_type == 'multiselect') {
	        options = sugarChimpSelf.data.available_fields[module][field].options.options;
	    }

	    var data = {
	        value: '',
	        defaultlabel: 'Enter a Value',
	        module: module,
	        id: id,
	        column: 'value',
	        options: options
	    };
	    if(presetValues && typeof presetValues.value != 'undefined')
	    {
	        if(display_type == 'input')
	        {
	            data.value = presetValues.value;
	        }
	        else if(display_type == 'dropdown') 
	        {
	            data.value = presetValues.value;
	        }
	        else if(display_type == 'multiselect')
	        {
	            data.value = presetValues.value;
	        }
	        else
	        {
	            data.value = 'No Value Needed for that Operand';
	        }
	    }

	    // display_types: input, dropdown, multiselect, empty
	    var val_html =  renderViewData('hbs-filter-' + display_type,'SugarChimp',data);
	    var row_id = '#smartlist-filter-row-' + module + '-' + id;
	    
	    $(row_id + '> #smartlist-filter-td-value').empty();
	    $(row_id + '> #smartlist-filter-td-value').append(val_html);
	}
	// smartlist-filter-operands-dropdown
	function smartlistFilterClearValueElement(module, id) {
	    if(!module || module =='' || !isInt(id)) {
	         return false;
	    }

	    var row_id = '#smartlist-filter-row-' + module + '-' + id;        
	    $(row_id + '> #smartlist-filter-td-value').empty();
	}
	function smartlistFilterSetValueElement(module,id,value){
	    // unimplemented for now
	}
	// remove filter button pressed
	function removeFilter(evt,module,id,addFilterIfNone){
	    if (evt){
	        $el = this.$(evt.currentTarget);
	        module = $el.attr('data-module');
	        id = $el.attr('data-id');
	    }

	    // the param has to be exactly true or exactly false
	    if (addFilterIfNone!==false){
	        addFilterIfNone = true;
	    }

	    var $row = $('#smartlist-filter-row-' + module + '-' + id);
	    $row.remove();
	    if(getNumberOfFilters(module) < 1 && addFilterIfNone===true){
	        createNewFilterRow(null, module);
	    }
	}

/**************************************
SmartList Step3 Toggles
**************************************/

	function smartlistModuleModeToggle(evt,$el){
	    if (typeof $el != 'object'){
	        $el = this.$(evt.currentTarget);
	    }
	    var module = $el.attr('data-module');
	    $('.smartlist-type-'+module+'-div').hide();
	    var mode = $el.val();
	    $('#smartlist-type-'+module+'-div-'+mode).show();

	            // hide any mode errors
        $('.smartlist-type-'+module+'-div-mode-errors').hide();

        var plan_mode_support = {
            basic: ['none'],
            professional: ['none','all'],
            ultimate: ['none','all','smart','sql'],
        };

        // we have a plan, check to see if current plan supports the selected mode
        if (sugarChimpSelf.data && sugarChimpSelf.data.current_plan && plan_mode_support[sugarChimpSelf.data.current_plan]){
            var supported_modes = plan_mode_support[sugarChimpSelf.data.current_plan];
            if (supported_modes.indexOf && supported_modes.indexOf(mode)<0){
                // mode is not supported on plan, show warning.
                $('#smartlist-type-'+module+'-div-'+mode+'-errors').html('This mode is not supported in your SugarChimp subscription. Please select another option.');
                $('#smartlist-type-'+module+'-div-'+mode+'-errors').show();
            }
        }
	}
	function smartlistTabToggle(evt){
        $('.smartlist-tab').hide();
        var $el = this.$(evt.currentTarget);
        $('.smartlist-nav li').removeClass('active');
        $('.smartlist-tab').removeClass('active');
        $el.parent().addClass('active');
        var divId = $el.attr('id');
        $('#smartlist-div-'+divId).addClass('active');
        $('#smartlist-div-'+divId).show(); 
    }

/**************************************
SmartList Step 3 Testing
**************************************/

	//called by Test Query and Test Filter buttons
	function testQuery(evt) {
	    var $el;
	    
	    $el = this.$(evt.currentTarget);
	    module = $el.attr('data-module');
	    type = $el.attr('data-type');
	    
	    if(type == "sql"){
	        testSQLQuery(module);
	        //showTestQueryResults(module,type,count)
	    }
	    else{
	        testFiltersQuery(module);
	        //showTestQueryResults(module,type,count)    
	    }
	    // do not submit the form on click
	    return false;
	}
	function testFiltersQuery(module){
	    // get data for smartlist view
	    showLoadingBox('Testing Filters');

	    // make sure any smartlist filters are complete
	    // keeping this simple for now, make sure a non-empty field and operand is selected
	    // if one is not selected, remove the row
	    removeIncompleteFilters([module]);

	    $.ajax({
		    url: 'index.php',
		    type: 'POST',
		    dataType: 'json',
		    data: {
		    	// testFiltersQuery in API
		        module: 'SugarChimp',
		        action: 'test_filters_query',
		        formData: $('#smarlist-setup-form').serialize(),
		        to_pdf: 1,
	        	module_type: module,
		    },
	        success: function(data,response) {
	            hideLoadingBox();
	            if(data.success){
	                count = data.count;
	                showTestQueryResults(module,'filters',count);
	            }
	            else{
	                showTestQueryResults(module,'filters',0,data.message);
	            }
	        }
	    });
	    return false;
	}
	function testSQLQuery(module){
	    showLoadingBox('Testing Query');

	    var sql_query = $("#smartlist-custom-sql-" +module).val();

	    $.ajax({
		    url: 'index.php',
		    type: 'POST',
		    dataType: 'json',
		    data: {
		    	// testSQLQuery in API
		        module: 'SugarChimp',
		        action: 'test_sql_query',
		        sql: sql_query,
		        to_pdf: 1,
		        sql_module: module
		    },
	        success: function(data,response) {
	            hideLoadingBox();
	            if(data.success){
	                count = data.count;
	                showTestQueryResults(module,'sql',count);
	            }
	            else{
	                showTestQueryResults(module,'sql',0,data.message);
	            }
	        }
	    });
	    return false;
	}
	function showTestQueryResults(module,type,count,error){
	    //sentence id 
	    var p_id = "#" + module + "-test-query-count-" + type;
	    var html;
	    if(error){
	        html = error;
	    }
	    else{    
	        //to be shown if no results are returned
	        var help_msg;
	        
	        if(type == "sql"){
	            type_str = "query";
	        }
	        else{
	            type_str = "SmartList filters";
	        }
	        
	        if(count > 0){
	            html = "The current "+ type_str +" would allow <b>"+ count +"<b> "+ module +" on the list.";
	        }
	        else{
	            html = "No "+ module +" match that criteria. Check your " + type_str + " and try again.";
	        }
	    }

	    //add count sentence next to button.
	    $(p_id).empty();
	    $(p_id).append(html);
	    return false;
	}

/**************************************
SmartList Saving and Validation
**************************************/
	function isValidRow($row){
	    var module = $row.attr('data-module');
	    var id = $row.attr('data-id');

	    var fieldVal = $('#smartlist-filter-available-fields-'+module+'-'+id).val();
	    var operandVal = $('#smartlist-filter-operands-'+module+'-'+id).val();

	    // the only time the value column can be empty is if the 
	    // empty, notempty, check, notcheck operands are selected
	    // otherwise a value is required
	    var valueVal = true;
	    if (operandVal!='empty' && operandVal!='notempty' && operandVal!='check' && operandVal!='notcheck')
	    {
	        valueVal = $('#smartlist-filter-value-'+module+'-'+id).val();
	    }

	    // make sure all values that need a value have a value
	    if (fieldVal && fieldVal!='' && operandVal && operandVal!='' && valueVal && valueVal!=''){
	        return true;
	    } else {
	        return false;
	    }
	}
	function isInt(value) {
	    return !isNaN(value) && parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
	}
	function removeIncompleteFilters(modules){
	    if (!modules) return false;

	    for (var i=0;i<modules.length;i++){
	        var module = modules[i];
	        $('.smartlist-filter-row-'+module).each(function(index){
	            if (!isValidRow($(this))){
	                var id = $(this).attr('data-id');
	                removeFilter(null,module,id,false);
	            }
	        });
	    }
	}
	function setCreateSubscriberAsDropdown(val) {
        $('#create-subscriber-as-dropdown').val(val);
    }
	function preMappingSync(evt){
	    // get data for smartlist view
	    showLoadingBox('Saving SmartList Settings');

	    // make sure any smartlist filters are complete
	    // keeping this simple for now, make sure a non-empty field and operand is selected
	    // if one is not selected, remove the row
	    removeIncompleteFilters(sugarChimpSelf.supported_modules);
	    
	    // make the save call!
	    $.ajax({
		    url: 'index.php',
		    type: 'POST',
		    dataType: 'json',
		    data: {
		    	// smartlistSave in API
		        module: 'SugarChimp',
		        action: 'smartlist_save',
		        to_pdf: 1,
		        formData: $('#smarlist-setup-form').serialize()
		    },
	        success: function(data,response){
	            hideLoadingBox();
	            if (data && data.success){
	                //add a 'success' message
	                //"SugarChimp Settings Saved for this List"

                    window.location.href = 'index.php?module=SugarChimp&action=health_status';


	            } 
	            else{
	                // list failed to create
	                
	                // Handle Error
	                // Something didn't save correctly
	            }
	        }
	    });
	    // do not submit the form on click
	    return false;
	}