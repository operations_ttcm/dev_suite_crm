




$(document).ready(function(){
    sugarChimpSelf.hasMailChimpAPI = false;
    sugarChimpSelf.data = {};
    /*
    'click .testLogin':'testLogin',
    */
    //declare events
    $(".testLogin").click(function(){
        testLogin();
        return false;
    }.bind(sugarChimpSelf));
    
    initializeStep();
});

function initializeStep() {
    //show Loading Scheduler. . .
    showLoadingBox('Checking API Key');
    $.ajax({
        url: 'index.php',
        method: 'GET',
        dataType: 'json',
        data: {
            module: 'SugarChimp',
            action: 'get_apikey',
        },
        success: function(data,response) {
            hideLoadingBox();
            if (!data.apikey) {
                data.apikey = '';
                $('.mailchimp-unsuccess').show();
                $('.mailchimp-success').hide();
                $('#apikey-group .help-block').hide();
            } else {
                sugarChimpSelf.hasMailChimpAPI = true;
                $('#apikey').val(data.apikey);
                $('.mailchimp-unsuccess').hide();
                $('.mailchimp-success').show();
            }
        }
    });
}

function testLogin(data,response) {
    $('#apikey-group').removeClass('error');
    $('#apikey-group .help-block').hide();
        
    //check to see if apikey is input into the box
    var apikey = $('#apikey').val();
    if(!$.trim(apikey).length) {
        $('#apikey-group').addClass('error');
        $('#apikey-group .help-block').show();
        $('.setup-section.error p').html('No API key entered.');
        $('#sugarchimp-error-section').show();
        return;
    }
    if(sugarChimpSelf.hasMailChimpAPI){
        showMailChimpAPIWarning(apikey);
        return;
    }
    confirmMailChimpAPIChange(apikey);
}

function confirmMailChimpAPIChange(apikey){
    showLoadingBox('Saving API Key');
    $.ajax({
        url: 'index.php',
        method: 'GET',
        dataType: 'json',
        data: {
            module: 'SugarChimp',
            action: 'update_apikey',
            apikey: apikey
        },
        success: function(data,response) {
            hideLoadingBox();
            if(data.success){
                alert('API key Registered Successfully!');
                $('#apikey-group').removeClass('error');
                $('.mailchimp-unsuccess').hide();
                $('.mailchimp-success').show();
                $('.setup-section.error p').empty();
                $("#mailchimp-info-change").hide();
                //move on to next step
                // goToStep(4);
            } else {
                if(sugarChimpSelf.hasMailChimpAPI){
                    $("#mailchimp-info-success").hide();
                    $("#mailchimp-info-change").show();
                }
                alert('Unable to register that API key');
                $('#apikey-group').addClass('error');
                $('#apikey-group .help-block').show();
                $('.setup-section.error p').html(data.message);
                $('#sugarchimp-error-section').show();
            }
        }
    });
}
function showMailChimpAPIWarning(apikey){
    confirmationAlert(
        //confirm function
        function(){
            confirmMailChimpAPIChange(apikey);
        }.bind(sugarChimpSelf),
        //cancel function
        function(){
        }.bind(sugarChimpSelf),
        //id
        'mc_api_changed',
        //title
        'Warning!',
        //message
        'SugarChimp can only have 1 active API key. Changing this key will remove the sync with your current MailChimp Account. Do you wish to continue?'
        );
}