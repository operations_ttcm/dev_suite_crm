




sugarChimpSelf.scheduler_count =0;
$(document).ready(function(){
	sugarChimpSelf.data = {};
	
	//declare events
	$("#show-checks").click(function(){
		showChecks();
		return false;
	}.bind(sugarChimpSelf));
	$(".reCheck").click(function(){
		reCheck();
		return false;
	}.bind(sugarChimpSelf));

	initializeStep();
});

function showChecks() {
    $('#welcome-message').hide();
    $("#prechecks").show();
    sugarChimpSelf.welcome_complete = true;
}
function reCheck() {
    sugarChimpSelf.rechecked = true;
    $("#continue-anyway").show();
    $("#recheck-support").show();
    initializeStep();
    return false;
}
function initializeStep() {
	showLoadingBox('Checking Scheduler. . .');
	$.ajax({
	    url: 'index.php',
	    method: 'GET',
	    dataType: 'json',
	    data: {
	        module: 'SugarChimp',
	        action: 'prechecks_scheduler',
	    },
	    success: function(data,response) {
	    	hideLoadingBox();
	        if(data.php_compatible){
	            $('#php-success-details').html(" You are currently running " + data.php_version);
	            $('.php-fail').hide();
	            $('.php-success').show();
	        }
	        else{
	            $('#php-fail-help').append("SugarChimp requires PHP version 5.3 or greater. You are currently running " + data.php_version);
	            $('.php-fail').show();
	            $('.php-success').hide();
	        }
	        if (data.scheduler_ran){
	            $('.scheduler-fail').hide();
	            $('.scheduler-success').show();
	        }
	        else{
	            $('.scheduler-fail').show();
	            $('.scheduler-success').hide();
	        }
	        var old_data = data;
	        showLoadingBox('Checking Network. . .');
	        $.ajax({
			    url: 'index.php',
			    method: 'GET',
			    dataType: 'json',
			    data: {
			        module: 'SugarChimp',
			        action: 'prechecks_network',
			    },
			    success: function(data){
			    	hideLoadingBox();
			    	//new api call for cURL and Network
	            	// dismiss loading. . .
	                $('#network-fail-help').append(data.message);
	                
	                if(data.curl_enabled){
	                    $('.curl-fail').hide();
	                    $('.curl-success').show();
	                    if (data.network_success){
	                        $('.network-fail').hide();
	                        $('.network-unknown').hide();
	                        $('.network-success').show();
	                    }
	                    else{
	                        $('.network-unknown').hide();
	                        $('.network-success').hide();
	                        $('.network-fail').show();
	                    }
	                }
	                else{
	                    $('.curl-fail').show();
	                    $('.curl-success').hide();
	                    
	                    //network cannot be checked
	                    $('.network-success').hide();
	                    $('.network-fail').hide();
	                    $('.network-unknown').show();
	                }
	                
	                if(data.success && old_data.success){
	                    $('.precheck-success').show();
	                    $('.precheck-fail').hide();
	                }
	                else{
	                    $('.precheck-success').hide();
	                    $('.precheck-fail').show();
	                }
	            }
	        });
	    }
	});
}