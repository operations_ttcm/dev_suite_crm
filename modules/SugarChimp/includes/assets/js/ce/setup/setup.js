




$(document).ready(function(){
    Handlebars.registerHelper("valueInArray",function(val,arr,str){
        if (typeof arr == 'object' && arr.length > 0){
            for (var i=0;i<arr.length;i++){
                if (val == arr[i]) return new Handlebars.SafeString(str);
            }
        }
        return '';
    });
    Handlebars.registerHelper("check", function(str)
    {
        if(typeof(str) === 'undefined' || str === null)
        {
            return "";
        }
        if(str === "Prospects")
        {
            return "Targets";
        }
        return str;
    });
    Handlebars.registerHelper("eq",function(val1,val2,options)
    {
        return val1==val2?options.fn(this):options.inverse(this);
    });
    Handlebars.registerHelper("toLowerCase", function(str)
    {
        if(typeof(str) === 'undefined' || str === null)
        {
            return "";
        }
        return str.toLowerCase();
    });
    /*
    //Unimplemented Sugar7 functions for all steps
        'click .setupComplete':'setupComplete',
        'click .howtoListToggleLink':'howtoListToggleLink', 
    */
    $("#warning-confirm").click(function(evt){
        onWarningConfirmed();
        return false;
    }.bind(self));
    $("#warning-cancel").click(function(evt){
        onWarningCancelled();
        return false;
    }.bind(self));
    $(".nextStep").click(function(evt){
        var $el;
        $el = this.$(evt.currentTarget);
        var step = $el.attr('data-step');
        goToStep(step);
        return false;
    }.bind(self));
    $(".goToStep").click(function(evt){
        var $el;
        $el = this.$(evt.currentTarget);
        var step = $el.attr('data-step');
        goToStep(step);
        return false;
    }.bind(self));
    //make call to check in suite or ce
    $.ajax({
        url:'index.php',
        dataType: 'json',
        data: {
            module: 'SugarChimp',
            action: 'checkForSuiteCRM',
        },
        type:'GET',
        success: function(response)
        {
            if (response && !response.is_suitecrm){
                addCEcss();
            }
        }
    });
});
function addCEcss()
{
    var css_link = "<link rel='stylesheet' type='text/css' href='modules/SugarChimp/includes/assets/css/ce/sugarchimp_ce_only.css?8.1.1a'>";
    $('#include-ce-css').html(css_link);
}
function goToStep(step)
{
    window.location.href = 'index.php?module=SugarChimp&action=step'+step;
}
function hideLoadingBox()
{
    $('.loading-panel').hide();
}
function showLoadingBox(text)
{
    var gif = "<img id='loading-gif' src='modules/SugarChimp/includes/assets/img/loading.gif'>";
    if(!text || text == '')
    {
        text = "Loading";
    }
    var html = gif + text + ". . .";
    $('#loading-body').html(html);
    $('.loading-panel').show();
}
function confirmationAlert(confirm, cancel, id,title,message,confirm_text,cancel_text)
{
    if(confirm)
    {
        sugarChimpSelf.confirmed = confirm;
    }
    if(cancel)
    {
        sugarChimpSelf.cancelled = cancel;
    }
    if(!title || title == '')
    {
        title = "Title";
    }
    if(!message || message == '')
    {
        message = "Are you Sure?";
    }
    if(!id || id == '')
    {
        id = 'confirm_action';
    }
    if(!confirm_text || confirm_text == '')
    {
        confirm_text = "confirm";
    }
    if(!cancel_text || cancel_text == '')
    {
        cancel_text = "cancel";
    }
    $('#warning-header').html(title);
    $('#warning-body').html(message);
    $('#warning-confirm').html(confirm_text);
    $('#warning-cancel').html(cancel_text);
    //show warning-box div with two buttons who call those functions
    showWarningBox();
    //hide main div
}
function onWarningConfirmed()
{
    hideWarningBox();
    sugarChimpSelf.confirmed();
}
function onWarningCancelled()
{
    hideWarningBox();
    sugarChimpSelf.cancelled();
}
function showWarningBox()
{
    $('.warning-panel').show();
    $('.content-panel').hide();
}
function hideWarningBox()
{
    $('.warning-panel').hide();
    $('.content-panel').show();
}
// left module in so that we can use the renderViewData Sugar7 calls
function renderViewData(template,module,data){
    if (!template || !data){ return false;}
    var source = $("#"+template).html();
    var template = Handlebars.compile(source);
    return template(data);
}