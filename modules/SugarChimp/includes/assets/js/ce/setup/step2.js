




$(document).ready(function(){
	/*
		'click .validateLicense': 'validateLicense',
        'change input:radio[class=switch-to-option]':'planSwitchToggle',
        'click #accept-terms':'switchPlan',
        'click .hideToggle': 'hideToggle',
        'click .plan-change-accept': 'showTerms',
        'click #cancel-upgrade-button':'showUpgradeCancelWarning',	
	*/
	//declare events
	$(".validateLicense").click(function(){
		validateLicense();
		return false;
	}.bind(sugarChimpSelf));
	$('#insert-upgrade-details').on('change','input:radio[class=switch-to-option]',function(evt){
    	planSwitchToggle(evt);
		return false;
	});
	$('#insert-upgrade-details').on('click','#accept-terms',function(evt){
    	switchPlan(evt);
		return false;
	});
	$('#insert-upgrade-details').on('click','.hideToggle',function(evt){
    	hideToggle(evt);
		return false;
	});
	$('#insert-upgrade-details').on('click','.plan-change-accept',function(evt){
    	showTerms(evt);
		return false;
	});
	$('#insert-upgrade-details').on('click','#cancel-upgrade-button',function(evt){
    	showUpgradeCancelWarning(evt);
		return false;
	});
    // 'click .smartlist-hide':'smartListHide',
    $('#insert-upgrade-details').on('click','.smartlist-hide',function(evt){
        smartListHide(evt);
        return false;
    });
    // 'click .smartlist-show':'smartListShow',
    $('#insert-upgrade-details').on('click','.smartlist-show',function(evt){
        smartListShow(evt);
        return false;
    });
	initializeStep();
});

function validateLicense(evt,key){
    $('#license-group').removeClass('error');
    $('#license-group .help-block').hide();

    if(key && key != null)
    {
        //validate from switched plan
        var fromPlan = true;
    }
    else
    {
        //check to see if licenseKey is set
        var key = $('#licenseKey').val();
        if(!$.trim(key).length) {
            //  empty license key error
            //
            $('#license-group').addClass('error');
            $('#license-group .help-block').show();
            $('.setup-section.error p').html('No License Key entered.');
            $('#sugarchimp-error-section').show();
            return;
        }
    }



    var sugarChimpSelf = this;
    showLoadingBox('Validating License');
    $.ajax({
        url: 'index.php',
        method: 'GET',
        dataType: 'json',
        data: {
            module: 'SugarChimp',
            action: 'validate_license',
            licensekey: key
        },
        success: function(data,response) {
            hideLoadingBox();
            if(data.success) {
                //move on to next step
                //goToStep(3);
                if(fromPlan) {
                    //reload step 2
                    goToStep(2);
                } else {
                    alert('License Successfully Validated!');
                    $('#license-info-fail').hide();
                    $('#license-info-success').show();
                    $('#license-group').addClass('textbox-success');
                    $('.setup-section.error p').empty();
                    $('#plan-switching').hide();
                    $('.trial-period').hide();
                }
            } else {
                $('#license-info-success').hide();
                $('#license-info-fail').show();
                $('#license-group').addClass('error');
                $('#license-group .help-block').show();
                //show error messages
                $('.setup-section.error p').html(data.message);
                $('#sugarchimp-error-section').show();
            }
        },
        failure: function(data,response) {
            alert('Unable to Validate License. Contact support@sugarchimp.com');

        }
    });
}

function initializeStep() {
	showLoadingBox('Loading License Key');
	$.ajax({
	    url: 'index.php',
	    method: 'GET',
	    dataType: 'json',
	    data: {
	        module: 'SugarChimp',
	        action: 'get_license',
	    },
	    success: function(data,response) {
            hideLoadingBox();
            if (!data.success){
                // License Key Population
                data.private_key = '';
                $('#license-group').removeClass('textbox-success');
                $('.license-validated').hide();
                $('#license-info-fail').show();

            }else{
                sugarChimpSelf.license = data.result;
                sugarChimpSelf.license.trial_time_remaining = data.trial_time_remaining;
                sugarChimpSelf.license.key = data.private_key;

                // License Key Population
                $('#licenseKey').val(data.private_key);
                $('#license-info-fail').hide();
                $('#license-info-success').show();
                $('#license-group').addClass('textbox-success');

                // plan switching logic
                // show switch unless these scenarios:
                // 1. They are not on a switchable plan
                // 2. They are already on the highest_plan ('ultimate')
                // 3. They have already finished the Upgrade Trial (removed all for now)
                // 
                
                if (sugarChimpSelf.license.is_switchable && (sugarChimpSelf.license.in_trial || sugarChimpSelf.license.plan_details.name != 'Ultimate'))
                {
            		showLoadingBox('Loading Upgrade Options');
            		//call for switchable plans
            		$.ajax({
					    url: 'index.php',
					    method: 'GET',
					    dataType: 'json',
					    data: {
					        module: 'SugarChimp',
					        action: 'get_switchable_plans',
					    },
					    success: function(data,response) {
				            hideLoadingBox();
                            var freq = '';
                            
                            if(sugarChimpSelf.license.plan_details.id && data[sugarChimpSelf.license.plan_details.id] && data[sugarChimpSelf.license.plan_details.id].frequency)
                            {
                                freq = data[sugarChimpSelf.license.plan_details.id].frequency;
                            }
                            var grandfathered =  sugarChimpSelf.license.license_created <= 1457740800;

                            // basic id = 4,5 / 183,6
                            // pro id = 286,9 / 287,10
                            // ult id = 541,214 / 542,215

                            // set available plans
                            sugarChimpSelf.available_plans = {};
                            for(var key in data){
                                if(data[key].frequency == freq)
                                {
                                    if(grandfathered)
                                    {
                                        var price = 44;
                                        if(data[key].id == '286' || data[key].id == '287' 
                                            || data[key].id == '9' || data[key].id == '10')
                                            price = 99;
                                        else if(data[key].id == '541' || data[key].id == '542' 
                                            || data[key].id == '214' || data[key].id == '215')
                                            price = 199;

                                        if(freq == 'monthly')
                                        {
                                            data[key].price_formatted = '$' + price + "/month";
                                        }
                                        else
                                        {
                                            price *=10;
                                            data[key].price_formatted = '$' + price + "/year";
                                        }
                                    }
                                    sugarChimpSelf.available_plans[key] = data[key];
                                }
                            }
                            renderStep();
                        }
                    });
                }
                else
                {
                    //on ultimate or not switchable, do not show switching
                }
            }
	    }
	});
}
/**************************************
   		Upgrade Functions
**************************************/
	function renderStep() {
		var upgrade_html = renderViewData("hbs-render-step2",null,sugarChimpSelf);
		$('#hbs-render-step2').remove();
		$("#insert-upgrade-details").html(upgrade_html);
		renderPlanOptions();
		// currently in a trial or switch
	    if (sugarChimpSelf.license.in_trial || sugarChimpSelf.license.in_upgrade_trial)
	    {
	        showTrialGUI();
	        $('#trialing-or-on').html('trialing');
	    }
	    //not in a trial
	    else
	    {
	        // jon-todo edit if all trial changes are permanent.
	        $('#trialing-or-on').html('on');
	        $('.trial-ended').hide();
	    }
	    //trying something other than what you are paying for
	    if(sugarChimpSelf.license.trial_plan_details && 
	        sugarChimpSelf.license.trial_plan_details!=null && 
	        typeof sugarChimpSelf.license.trial_plan_details == 'object')
	    {
	        $('#current-plan-label').html(sugarChimpSelf.license.trial_plan_details.name);
	        $("input[value='" + sugarChimpSelf.license.trial_plan_details.id+ "']").prop("checked", true);
	        $('#switch-to-'+sugarChimpSelf.license.trial_plan_details.public_key).addClass('active');
	    }
	    else
	    {
	        $('#current-plan-label').html(sugarChimpSelf.license.plan_details.name);
	        $("input[value='" + sugarChimpSelf.license.plan_details.id+ "']").prop("checked", true);
	        $('#switch-to-'+sugarChimpSelf.license.plan_details.public_key).addClass('active');
	    }
	    // You are paying for Pro
	    if(!sugarChimpSelf.license.in_trial && sugarChimpSelf.license.plan_details.name!='Basic')
	    {
	        // hide each basic plan option
	        for(var i=0; i<sugarChimpSelf.basic_keys.length; i++) {
	            if(sugarChimpSelf.basic_keys[i] != null)
	            {
	                $("#switch-to-" + sugarChimpSelf.basic_keys[i]).hide();
	            }
	        }
	    }

		$('#plan-switching').show();

	}
	function renderPlanOptions()
    {
        sugarChimpSelf.basic_keys = [];
        // add div option for each available plan
        for(var key in sugarChimpSelf.available_plans){
            var plan_info = sugarChimpSelf.available_plans[key];
            if(plan_info.id == '5' || plan_info.id == '6' 
                || plan_info.id == '4' || plan_info.id == '183')
            {
                sugarChimpSelf.basic_keys.push(plan_info.public_key);
            }
            else if(plan_info.id == '541' || plan_info.id == '542' 
                || plan_info.id == '214' || plan_info.id == '215')
            {
                plan_info.link = 'https://store.suitecrm.com/docs/SugarChimp/try-ultimate/';
                plan_info.link_label = '(see features)';
            }
                
            var html = renderViewData('hbs-plan-switch-option','SugarChimp',plan_info);
            $('#plan-switching-options').prepend(html);
        }
    }

	function hideToggle(evt, select) {
        if(evt && evt != null)
        {
            var $el;
            $el = this.$(evt.currentTarget);
            select = $el.attr('data-toggle');
        }
        $(select).toggle();
        return false;
    }

    function planSwitchToggle(evt, on_paid, on_trial) {
        if(evt && evt != null)
        {
            var $el = this.$(evt.currentTarget);
            var id = $el.val();
            on_paid = sugarChimpSelf.license && sugarChimpSelf.license.plan_details && sugarChimpSelf.license.plan_details.id === id;
            on_trial = sugarChimpSelf.license && sugarChimpSelf.license.trial_plan_details && sugarChimpSelf.license.trial_plan_details.id === id;
        }

        // make sure license terms are collapsed and proper buttons showing
        $('#plan-switching-terms').hide();
        $('#all-plan-buttons').show();


        //not in a trial
        if(!sugarChimpSelf.license.in_upgrade_trial && !sugarChimpSelf.license.in_trial)
        {
            if(on_paid)
            {
                //disable try now button
                $('#plan-switching-button').hide();
            }
            else
            {
                $('#plan-switching-button').show();
                // enable try now button
            }
        }
        //in upgrade trial outside of initial_trial
        else if(sugarChimpSelf.license.in_upgrade_trial && !sugarChimpSelf.license.in_trial)
        {
            $('#cancel-upgrade').show();
            if(on_trial)
            {
                //disable try now button
                $('#switch-upgrade').hide();
            }
            else
            {
                $('#switch-upgrade').show();
            }
        }
        //in initial trial, show plan switching with no cancel button
        else
        {
            $('#switch-upgrade').show();
            if(sugarChimpSelf.license.in_upgrade_trial && on_trial)
            {
                $('#switch-upgrade').hide();
            }
            else if(!sugarChimpSelf.license.in_upgrade_trial && on_paid)
            {
                $('#switch-upgrade').hide();
            }
        }
    }
    function showTrialGUI() {
        var seconds = 0;
        planSwitchToggle(null,true,true);
        if(sugarChimpSelf.license.in_trial)
        {
            $('#plan-switching-terms-textbox').hide();
            $('#in-trial-terms-textbox').show();
        }
        else
        {
            $('#in-upgrade-trial').show();
        }

        if(sugarChimpSelf.license.trial_time_remaining > 3600)
        {
            seconds = sugarChimpSelf.license.trial_time_remaining;
            $('#minimal-time').show();
            $('.countdown-timer').show();
            var days = Math.floor( seconds/(60*60*24) );
            seconds -= days * (60*60*24);
            var hours = Math.floor( (seconds/(60*60)) % 24 );
            seconds -= hours * (60*60);
            var minutes = Math.floor( (seconds/60) % 60 );
            
            if(days > 1)
                $("#countdown-days").html(days + ' days ');
            else if(days > 0)
                $("#countdown-days").html(days + ' day ');
            if(hours > 1)
            $("#countdown-hours").html(hours + ' hours ');
            else if(hours > 0)
                $("#countdown-hours").html(days + ' hour ');

            if(minutes > 1)
            $("#countdown-minutes").html(minutes + ' minutes ');
            else if(minutes > 0)
                $("#countdown-minutes").html(days + ' minute ');

            $('#exact-time').show();
            $('#minimal-time').hide();
        }
        else
        {
            $('#minimal-time').show();
            $('#exact-time').hide();
        }
        $('.countdown-timer').show();
    }
    function showTerms(evt) {
        $('#all-plan-buttons').hide();
        if(sugarChimpSelf.license.in_trial)
        {
            $('#in-trial-terms-textbox').show();
            $('#plan-switching-terms-textbox').hide();
        }
        else
        {
            $('#in-trial-terms-textbox').hide();
            $('#plan-switching-terms-textbox').show();
        }
    }
    function showUpgradeCancelWarning(evt) {
        confirmationAlert(
            //confirm function
            function(){
                confirmUpgradeCancel();
            }.bind(sugarChimpSelf),
            //cancel function
            function(){
                
            }.bind(sugarChimpSelf),
            //id
            'cancel_upgrade',
            //title
            'Are you sure?',
            //message
            'Cancelling the upgrade will move you back to your original plan. Any features that you are currently using on an upgraded plan will no longer function.',
            //confirm_button
            'Yes, Cancel Upgrade',
            //cancel_button
            'No, Keep Trialing'
        );
    }
    function confirmUpgradeCancel() {
        //show SugarChimp_save_inprocess
        showLoadingBox('Cancelling Upgrade')

		// make the save call!
	    $.ajax({
		    url: 'index.php',
		    type: 'POST',
		    dataType: 'json',
		    data: {
		    	module: 'SugarChimp',
		        action: 'cancel_upgrade',
		    },        
            success: function(data,response) {
            	hideLoadingBox();
                if(data.cancelled) {
                    alert('Upgrade Cancelled Successfully.');
                    $('#plan-switching').hide();
                    $('.trial-period').hide();
                    validateLicense(null, sugarChimpSelf.license.key);
                } else {
                    alert('Unable to cancel the upgrade trial. contact support@sugarchimp.com');
                }
            }
        });
    }
    function switchPlan(evt) {
        var switchTo = $('#plan-switching-options input[class=switch-to-option]:checked').val();
        //show SugarChimp_save_inprocess
        showLoadingBox('Switching Plans');
        $.ajax({
		    url: 'index.php',
		    type: 'POST',
		    dataType: 'json',
		    data: {
		    	module: 'SugarChimp',
		        action: 'switch_plan',
		    	plan_id: switchTo
		    },
            success: function(data,response) {
            	hideLoadingBox();
                if(data.switched) {
                    alert('Plan Changed Successfully!');
                    // revalidate the license, and reload the page.
                    validateLicense(null, sugarChimpSelf.license.key);

                } else {
                    alert('Unable to change plans. Contact support@sugarchimp.com');
                }
            },
        });
    }
    function smartListShow(evt, select){
        if(!select){
            var $el;
            $el = this.$(evt.currentTarget);
            select = $el.attr('data-show');
            if(select ==='.step3');
            {
                $('div.disabled-text div.sync-option-details-div').css("display", "block");
            }
        }
        $(select).show();
        return false;
    }
    function smartListHide(evt, select) {
        if(!select){
            var $el;
            $el = this.$(evt.currentTarget);
            var select = $el.attr('data-hide');
        }
        $(select).hide();
        return false;
    }