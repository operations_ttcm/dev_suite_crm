




$(document).ready(function(){
    Handlebars.registerHelper("eq",function(val1,val2,options)
    {
        return val1==val2?options.fn(this):options.inverse(this);
    });

    loadHealthData();
    // update logger level when dropdown changes
    $('#health-main-rendered').on('change','#sugarchimp_logger',function(evt){
        var val = $(this).val();
        $.ajax({
            url: 'index.php?module=SugarChimp&action=save_config_logger&to_pdf=1&sugarchimp_logger='+val,
            dataType: 'json',
            success: function(response){
                if (response.success === true){
                    alert('Logger level saved!');
                } else {
                    alert('Logger level could not be saved.');
                }
            },
            error: function(response){
                alert('Logger level could not be saved.');
            }
        });
    });

    $('#health-main-rendered').on('click','.health-status-queue-sync', function(evt){
        // health-status-queue-sync-{guid}
        var list_id = $(this).attr('id').substring(25);
        var syncaction = $('#list-sync-options-'+list_id).val();
        
        if (!syncaction || syncaction==''){
            alert('Select a Sync Option from the Dropdown.');
            return false;
        }

        if (!list_id || list_id==''){
            alert('SugarChimp was unable to start the sync.');
            return false;
        }

        $.ajax({
            url: 'index.php?module=SugarChimp&action=queue_sync&to_pdf=1&list_id='+list_id+'&syncaction='+syncaction,
            dataType: 'json',
            success: function(response){
                alert('The list re-sync has been queued.');
                $('#health-status-queue-sync-'+list_id).hide(); // hide button
                $('#list-sync-options-'+list_id).hide(); // hide list
                $('#synced-list-status-'+list_id).html('Sync Queued'); // update status
            },
            error: function(response){
                alert('The list re-sync could not be queue.');
            }
        });
    });
    //'click .health-status-remove-sync':'showRemoveSyncWarning',
    $('#health-main-rendered').on('click','.health-status-remove-sync', function(evt){
        showRemoveSyncWarning(evt);
        return false;
    });
    $("#warning-confirm").click(function(evt){
        onWarningConfirmed();
        return false;
    }.bind(self));
    $("#warning-cancel").click(function(evt){
        onWarningCancelled();
        return false;
    }.bind(self));

});
function loadHealthData() {
    showLoadingBox();
    $.ajax({
        url: 'index.php',
        method: 'GET',
        dataType: 'json',
        data: 
        {
            module: 'SugarChimp',
            action: 'get_health_data'
        },
        success: function(data,response) 
        {
            hideLoadingBox();
            var health_html = renderViewData("hbs-render-health-status",null,data);
            $("div.setup-section").html(health_html);

            if(data.last_ran && data.scheduler_found) {
                $('#row-status-last-ran i').addClass('status-good');
            } else {
                $('#row-status-last-ran i').addClass('status-bad');
            }
            $('#row-status-last-ran i').show();
            
            if(data.last_ran) {
                $('#status-last-ran').html(data.last_ran);
            } else {
                $('#status-last-ran').html('Has not ran yet');
            }

            
            if(data.scheduler_status == 'Active') {
                $('#row-status-scheduler-status i').addClass('status-good');
            } else {
                $('#row-status-scheduler-status i').addClass('status-bad');
            }
            $('#row-status-scheduler-status i').show();
            $('#status-scheduler-status').html(data.scheduler_status);
            
            $('#status-interval').html(data.interval);
            
            if(data.mailchimp_configured) {
                $('#status-mailchimp-configured-good').show();
            } else {
                $('#status-mailchimp-configured-bad').show();
            }
            
            $('#to-mailchimp-lists').html(data.to_mailchimp_lists);
            $('#to-mailchimp-subscribers').html(data.to_mailchimp_subscribers);
            $('#from-mailchimp-lists').html(data.from_mailchimp_lists);
            $('#from-mailchimp-subscribers').html(data.from_mailchimp_subscribers);
            $('#from-mailchimp-activities').html(data.from_mailchimp_activities);
            
            if(data.scheduler_found) {
                $('.scheduler-link').attr('href','index.php?module=Schedulers&action=DetailView&record='+data.scheduler_id);
                $('.scheduler-link').show();
            } else {
                $('.scheduler-link').hide();
            }
            
            if (data.sugarchimp_version){
                $('#sugarchimp-version').html(data.sugarchimp_version);
            } else {
                $('#sugarchimp-version').html('unknown');
            }

            if (data.current_plan){
                $('#sugarchimp-edition').html(data.current_plan);
            } else {
                $('#sugarchimp-edition').html('unknown');
            }

            if(data.valid_license) {
                $('#status-valid-license-good').show();
            } else {
                $('#status-valid-license-bad').show();
            }
            
            if(data.mailchimp_initial) {
                $('#mailchimp-initial').show();
                $('#from-mailchimp-lists').html('* job pending');
                $('#from-mailchimp-subscribers').html('* job pending');
            } else {
                $('#mailchimp-initial-done').show();
            }
            
            if(data.mailchimp_activity_initial) {
                $('#mailchimp-activity-initial').show();
            } else {
                $('#mailchimp-activity-initial-done').show();
            }
            //SmartList Data Handling
            if(data.smartlist_last_ran) {
                $('#row-status-smartlist-last-ran i').addClass('status-good');
                $('#status-smartlist-last-ran').html(data.smartlist_last_ran);
            } else {
                $('#row-status-smartlist-last-ran i').addClass('status-bad');
                $('#status-smartlist-last-ran').html('Has not run yet');
            }
            $('#row-status-smartlist-last-ran i').show();

            if(data.smartlist_status == 'Active') {
                $('#row-status-smartlist-scheduler-status i').addClass('status-good');
            } else {
                $('#row-status-smartlist-scheduler-status i').addClass('status-bad');
            }
            $('#row-status-smartlist-scheduler-status i').show();
            $('#status-smartlist-scheduler-status').html(data.smartlist_status);
            
            $('#status-smartlist-interval').html(data.interval);
            
            if(data.smartlist_scheduler_found) {
                $('.smartlist-scheduler-link').attr('href','index.php?module=Schedulers&action=DetailView&record='+data.smartlist_scheduler_id);
                $('.smartlist-scheduler-link').show();
            } else {
                $('.smartlist-scheduler-link').hide();
            }

            if(data.sugarchimp_logger) {
                $('#sugarchimp_logger').val(data.sugarchimp_logger);
            }
            
            $('#sugarchimp-webhook-url').html(data.sugarchimp_webhook_url);

            var row_template = renderViewData('hbs-render-synced-lists', null, data);
            $('#health-status-synced-lists tbody').append(row_template);
        },
    });
}

function addCEcss()
{
    var css_link = "<link rel='stylesheet' type='text/css' href='modules/SugarChimp/includes/assets/css/ce/sugarchimp_ce_only.css?7.11.2h'>";
    $('#include-ce-css').html(css_link);
}
function hideLoadingBox()
{
    $('.loading-panel').hide();
}
function showLoadingBox(text)
{
    var gif = "<img id='loading-gif' src='modules/SugarChimp/includes/assets/img/loading.gif'>";
    if(!text || text == '')
    {
        text = "Loading";
    }
    var html = gif + text + ". . .";
    $('#loading-body').html(html);
    $('.loading-panel').show();
}
// left module in so that we can use the renderViewData Sugar7 calls
function renderViewData(template,module,data){
    if (!template || !data){ return false;}
    var source = $("#"+template).html();
    var template = Handlebars.compile(source);
    return template(data);
}

function removeSync(list_id) {
    console.log("Confirm Removal of Target List id: "+list_id);

    if (!list_id || list_id==''){
        return false;
    }
    //show alert for removing list
    showLoadingBox('Attempting to Remove the Sync. . .');
    
    $.ajax({
        url: 'index.php',
        method: 'GET',
        dataType: 'json',
        data: 
        {
            module: 'SugarChimp',
            action: 'removeListSyncing',
            target_list_id: list_id,
        },
        success: function(data) 
        {
            //dismiss save message
            hideLoadingBox();
            if(data.success){
                $('#synced-list-row-'+list_id).hide();
            }
        },
    });
}
function showRemoveSyncWarning(evt) {
    var sugarChimpSelf = this;
    var $el = this.$(evt.currentTarget);

    var list_id = $el.attr('id').substring(15);
    var s_name = $el.attr('sugar-name');
    var m_name = $el.attr('mailchimp-name');

    confirmationAlert(
        //confirm function
        function(){
            removeSync(list_id);
        }.bind(sugarChimpSelf),
        //cancel function
        function(){
            
        }.bind(sugarChimpSelf),
        //id
        'remove_sync_warning',
        //title
        'Are you sure?',
        //message
        'This will remove the sync between <b>'+s_name+'</b> (Target List) and <b>'+m_name+'</b> (MailChimp List).',
        //confirm_button
        'Yes, Remove the Sync',
        //cancel_button
        'No, Keep the Sync'
    );
}
function confirmationAlert(confirm, cancel, id,title,message,confirm_text,cancel_text)
{
    if(confirm)
    {
        sugarChimpSelf.confirmed = confirm;
    }
    if(cancel)
    {
        sugarChimpSelf.cancelled = cancel;
    }
    if(!title || title == '')
    {
        title = "Title";
    }
    if(!message || message == '')
    {
        message = "Are you Sure?";
    }
    if(!id || id == '')
    {
        id = 'confirm_action';
    }
    if(!confirm_text || confirm_text == '')
    {
        confirm_text = "confirm";
    }
    if(!cancel_text || cancel_text == '')
    {
        cancel_text = "cancel";
    }
    $('#warning-header').html(title);
    $('#warning-body').html(message);
    $('#warning-confirm').html(confirm_text);
    $('#warning-cancel').html(cancel_text);
    //show warning-box div with two buttons who call those functions
    showWarningBox();
    //hide main div
}
function onWarningConfirmed()
{
    hideWarningBox();
    sugarChimpSelf.confirmed();
}
function onWarningCancelled()
{
    hideWarningBox();
    sugarChimpSelf.cancelled();
}
function showWarningBox()
{
    $('.warning-panel').show();
    $('.content-panel').hide();
}
function hideWarningBox()
{
    $('.warning-panel').hide();
    $('.content-panel').show();
}