






//NOT IMPLEMENTED CURRENTLY
$(document).ready(function(){
    $('#field-mapping-continue-button').click(function(){
        $('#field-mapping-continue-div').show();
    });

    $('#field-mapping-cancel-button').click(function(){
        $('#field-mapping-continue-div').hide();
        $('#field-mapping-sync-direction').css('border','inherit').css('color','inherit');
    });

    $('.sugarchimp-field-mapping-save-button').click(function(){
        $(this).html('<img src="modules/SugarChimp/includes/assets/img/loading.gif" width="16" height="16">');
        $(this).attr('clicked','true');
    });

    $('#field_mapping_details_form').submit(function(){
        $('.sugarchimp-field-mapping-save-button').attr('disabled','disabled');
        $('.sugarchimp-field-mapping-save-button').addClass('disabled');
        if ($('.sugarchimp-field-mapping-save-button[clicked=true]').val() == 'save_and_sync' && $('#field-mapping-sync-direction').val()==''){
            // trying to setup a sync, but sync type is not selected
            $('#field-mapping-save-and-sync-button').html('Save Mapping & Start Re-sync');
            $('.sugarchimp-field-mapping-save-button').removeAttr('disabled','disabled');
            $('.sugarchimp-field-mapping-save-button').removeClass('disabled');
            $('#field-mapping-sync-direction').css('border','1px solid red').css('color','red');
            alert('To Save Mapping and Start a Re-sync, please make a selection from the "Re-sync Options" dropdown.');
            return false;
        }
        return true;
    });
    /*
        {if !empty($list_id)}
            get_field_mapping_details('{$list_id}');
        {else}
            get_field_mapping_details($('#list_id').val());
        {/if}
        {literal}
        
        $('#list_id').change(function(){
            get_field_mapping_details($(this).val());
        });
    */
});
    
function get_field_mapping_details(list_id){
    if (list_id==''){
        alert('You must select a list.');
    }
    
    $('#field_mapping_div').html('');
    $('#field_mapping_div_loading').show();
    
    // get mapping
    $.ajax({
        url: 'index.php',
        method: 'post',
        dataType: 'json',
        data: {
            module: 'SugarChimp',
            action: 'field_mapping_details',
            list_id: list_id,
            to_pdf: 1,
        },
        success: function(data){
            $('#field_mapping_div_loading').hide();
            if (data && data.success && data.success == true){
                $('#field_mapping_div').html(data.html);
            } else {
                if (data.message){
                    alert(data.message);
                } else {
                    alert('Mapping could not be loaded for the list. Check sugarcrm logs.');
                }
            }
        },
        error: function(data){
            $('#field_mapping_div_loading').hide();
            alert('Mapping could not be loaded for the list. Check sugarcrm logs.');
        },
    });
}