





$(document).ready(function(){
    $('button.sc-empty-table').click(function(){
        var table_name = $(this).attr('id');
        if (table_name=='sugarchimp' || table_name=='sugarchimp_mc'){
            if (confirm("Are you sure you want to empty the "+table_name+" table?")){
                sc_empty_table(table_name)
            }
        }else{
            alert("Are you crazy? You can't empty that table.");
        }
    });
    $('button.sc-get-batch').click(function(){
        var batch_id = $(this).attr('id');
        getBatchData(batch_id);
    });

});

function getBatchData(batch_id){
    
    if(typeof batch_id !== 'string'){
        return false;
    }

    $.ajax({
        url: 'index.php',
        method: 'get',
        dataType: 'json',
        data: {
            module: 'SugarChimp',
            action: 'get_batch_data',
            batch_id: batch_id,
        },
        success: function(data){
            if (data.success===true){
                showBatchData(data.batch_info);
            } else {
                alert('Could not retrieve Batch Data from MailChimp.')
            }
        }
    });
}

function showBatchData(batch_info){
    if(typeof batch_info !== 'object' || typeof batch_info.id !== 'string'){
        return false;
    }
    var batch_id = batch_info.id;
    
    var td = '#dump-'+batch_id;
    var text = 'No Link ';
    if(batch_info.response_body_url){
        text = "<strong><a href='"+batch_info.response_body_url+"'>Download Link</a></strong> ";
    }
    if(batch_info.status) {
        text += "&nbsp; <strong>Status:</strong> "+batch_info.status;
    }
    if(batch_info.submitted_at) {
        text += "&nbsp; <strong>Submitted at:</strong> "+ batch_info.submitted_at;
    }
    if(batch_info.completed_at) {
        text += "&nbsp; <strong>Completed at:</strong> "+ batch_info.completed_at;
    }
    if(batch_info.total_operations != null) {
        text += "&nbsp; <strong>Total operations:</strong> "+batch_info.total_operations;
    }
    if(batch_info.total_operations != null) {
        text += "&nbsp; <span style='color:#0CC264'><strong>Finished operations:</strong> "+batch_info.finished_operations+"</span>";
    }
    if(batch_info.total_operations != null) {
        text += "&nbsp; <span style='color:#C20C21'><strong>Errored operations:</strong> "+batch_info.errored_operations+"</span>";
    }

    $(td).html(text);
    $(td).show();
}

function sc_empty_table(table_name){
    if (table_name!='sugarchimp' && table_name!='sugarchimp_mc' && table_name!='sugarchimp_batches'){
        return false;
    }
    
    $.ajax({
        url: 'index.php',
        method: 'post',
        dataType: 'json',
        data: {
            module: 'SugarChimp',
            action: 'empty_table',
            table_name: table_name,
        },
        success: function(data){
            if (data.success===true){
                alert("Table emptied! Refresh the page to verify the table is empty.");
            } else {
                alert("Could not empty the table. Sorry.");
            }
        }
    });
}