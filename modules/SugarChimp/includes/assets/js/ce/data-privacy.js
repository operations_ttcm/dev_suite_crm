





$(document).ready(function(){
	$('button.sc-erase-data-button').click(function(evt){
		eraseData(evt);
	});
});

function eraseData(evt){
    var email = $('#sc-erase-email-address').val();

    if (!email){
        alert('Please insert an email address into the box.');
    }else{
        if (confirm("Are you sure you want to erase SugarChimp data for "+email+"?")){
            this.eraseDataConfirm(email,evt)
        }
    }
}

function eraseDataConfirm(email,evt){

	ajaxStatus.hideStatus();
	ajaxStatus.flashStatus("Loading...");

    $('#sc-erase-results').hide();

    if (!email) return;

	$.ajax({
        url: 'index.php',
        method: 'get',
        dataType: 'json',
        data: {
            module: 'SugarChimp',
            action: 'erase',
            email: email
        },
        success: function(data){
            if (data.success===true){
                ajaxStatus.hideStatus();

                $('#sc-erase-results').removeClass('success-box');
	            $('#sc-erase-results').removeClass('fail-box');

	            if (data.success){
	                $('#sc-erase-results').addClass('success-box');
	                if (data.messages){
	                    $('#sc-erase-results').html(data.messages);
	                }else{
	                    $('#sc-erase-results').html("The data was removed successfully.");
	                }
	            }else{
	                $('#sc-erase-results').addClass('fail-box');   
	                if (data.messages){
	                    $('#sc-erase-results').html(data.messages);
	                }else{
	                    $('#sc-erase-results').html("Data removal failed. Please check the SugarCRM logs.");
	                }
	            }

	            $('#sc-erase-results').show();
            } else {
                alert('Could not erase data.');
            }
        }
    });
}
