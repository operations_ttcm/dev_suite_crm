





$(document).ready(function(){
    $('button.sc-empty-table').click(function(){
        var table_name = $(this).attr('id');
        if (table_name=='sugarchimp' || table_name=='sugarchimp_mc'){
            if (confirm("Are you sure you want to empty the "+table_name+" table?")){
                sc_empty_table(table_name)
            }
        }else{
            alert("Are you crazy? You can't empty that table.");
        }
    });
});

function sc_empty_table(table_name){
    if (table_name!='sugarchimp' && table_name!='sugarchimp_mc'){
        return false;
    }
    
    $.ajax({
        url: 'index.php',
        method: 'post',
        dataType: 'json',
        data: {
            module: 'SugarChimp',
            action: 'empty_table',
            table_name: table_name,
        },
        success: function(data){
            if (data.success===true){
                alert("Table emptied! Refresh the page to verify the table is empty.");
            } else {
                alert("Could not empty the table. Sorry.");
            }
        }
    });
}