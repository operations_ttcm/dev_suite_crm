<?php







class Mailchimp_Export {
    
    var $root = 'https://api.mailchimp.com/export/1.0';
    
    public function __construct(Mailchimp $master) {
        $this->master = $master;
        $this->root = $this->master->generateRoot($this->root);
    }

    /**
     * Exports/dumps members of a list and all of their associated details. This is a very similar to exporting via the web interface.
     * list is a reserved word, so using getList() instead, this is how Mailchimp_List works
     * @param string id
     *     - the list id to get members from (can be gathered using lists())
     * @param string status (optional)
     *     - the status to get members for - one of (subscribed, unsubscribed, cleaned), defaults to subscribed
     * @param array segment (optional)
     *     - pull only a certain Segment of your list. For help with what this array should contain: see campaignSegmentTest(). It's also suggested that you test your options against campaignSegmentTest().
     * @param string since (optional)
     *     - only return member whose data has changed since a GMT timestamp – in YYYY-MM-DD HH:mm:ss format
     * @param string hashed (optional)
     *     - if, instead of full list data, you'd prefer a hashed list of email addresses, set this to the hashing algorithm you expect. Currently only "sha256" is supported.
     * @return text
     *     - a plain text dump of JSON objects. The first row is a header row. Each additional row returned is an individual JSON object. Rows are delimited using a newline (\n) marker, so implementations can read in a single line at a time, handle it, and move on.
     */
    public function getList($callback, $id, $status=false, $segment=false, $since=false, $hashed=false) {
        $_params = array(
            "id" => $id
        );
        // the export api doesn't like 'false' for optional params, only set the param if it !== false
        if ($status!==false) $_params['status'] = $status;
        if ($segment!==false) $_params['segment'] = $segment;
        if ($since!==false) $_params['since'] = $since;
        if ($hashed!==false) $_params['hashed'] = $hashed;
        // use this->root and non-json call type
        return $this->master->call_stream('list/', $_params, $callback, $this->root);
    }

    /**
     * Exports/dumps all Ecommerce Orders for an account.
     * @param string since (optional)
     *     - only return orders with order dates since a GMT timestamp – in YYYY-MM-DD HH:mm:ss format
     * @return text
     *     - a plain text dump of JSON objects. Each line/row returned is an individual JSON object. Rows are delimited using a newline (\n) marker so implementations can read in a single line at a time, handle it, and move on.
     */
    public function ecommOrders($callback, $since=false) {
        $_params = array();
        // the export api doesn't like 'false' for optional params, only set the param if it !== false
        if ($since!==false) $_params['since'] = $since;
        // use this->root and non-json call type
        return $this->master->call_stream('ecommOrders/', $_params, $callback, $this->root);
    }

    /**
     * Exports/dumps all Subscriber Activity for the requested campaign.
     * @param string id
     *     - the list id to get members from (can be gathered using lists())
     * @param include_empty (optional)
     *     - if set to "true" a record for every email address sent to will be returned even if there is no activity data. defaults to "false"     * @param string since (optional)
     * @param string since (optional)
     *     - only return member whose data has changed since a GMT timestamp – in YYYY-MM-DD HH:mm:ss format
     * @return text
     *     - a plain text dump of JSON objects. Each line/row returned is an individual JSON object. Rows are delimited using a newline (\n) marker so implementations can read in a single line at a time, handle it, and move on.
     */
    public function campaignSubscriberActivity($callback, $id, $include_empty=false, $since=false) {
        $_params = array("id" => $id);
        // the export api doesn't like 'false' for optional params, only set the param if it !== false
        if ($include_empty!==false) $_params['include_empty'] = $include_empty;
        if ($since!==false) $_params['since'] = $since;
        // use this->root and non-json call type
        return $this->master->call_stream('campaignSubscriberActivity/', $_params, $callback, $this->root);
    }
}