<?php







require_once 'Mailchimp/Folders.php';
require_once 'Mailchimp/Templates.php';
require_once 'Mailchimp/Users.php';
require_once 'Mailchimp/Helper.php';
require_once 'Mailchimp/Mobile.php';
require_once 'Mailchimp/Ecomm.php';
require_once 'Mailchimp/Neapolitan.php';
require_once 'Mailchimp/Lists.php';
require_once 'Mailchimp/Campaigns.php';
require_once 'Mailchimp/Vip.php';
require_once 'Mailchimp/Reports.php';
require_once 'Mailchimp/Gallery.php';
require_once 'Mailchimp/Exceptions.php';
require_once 'Mailchimp/Export.php';

class Mailchimp {

    /**
     * Placeholder attribute for Mailchimp_Folders class
     *
     * @var Mailchimp_Folders
     * @access public
     */
    var $folders;
    /**
     * Placeholder attribute for Mailchimp_Templates class
     *
     * @var Mailchimp_Templates
     * @access public
     */
    var $templates;
    /**
     * Placeholder attribute for Mailchimp_Users class
     *
     * @var Mailchimp_Users
     * @access public
     */
    var $users;
    /**
     * Placeholder attribute for Mailchimp_Helper class
     *
     * @var Mailchimp_Helper
     * @access public
     */
    var $helper;
    /**
     * Placeholder attribute for Mailchimp_Mobile class
     *
     * @var Mailchimp_Mobile
     * @access public
     */
    var $mobile;
    /**
     * Placeholder attribute for Mailchimp_Ecomm class
     *
     * @var Mailchimp_Ecomm
     * @access public
     */
    var $ecomm;
    /**
     * Placeholder attribute for Mailchimp_Neapolitan class
     *
     * @var Mailchimp_Neapolitan
     * @access public
     */
    var $neapolitan;
    /**
     * Placeholder attribute for Mailchimp_Lists class
     *
     * @var Mailchimp_Lists
     * @access public
     */
    var $lists;
    /**
     * Placeholder attribute for Mailchimp_Campaigns class
     *
     * @var Mailchimp_Campaigns
     * @access public
     */
    var $campaigns;
    /**
     * Placeholder attribute for Mailchimp_Vip class
     *
     * @var Mailchimp_Vip
     * @access public
     */
    var $vip;
    /**
     * Placeholder attribute for Mailchimp_Reports class
     *
     * @var Mailchimp_Reports
     * @access public
     */
    var $reports;
    /**
     * Placeholder attribute for Mailchimp_Gallery class
     *
     * @var Mailchimp_Gallery
     * @access public
     */
    var $gallery;
    /**
     * Placeholder attribute for Mailchimp_Export class
     *
     * @var Mailchimp_Export
     * @access public
     */
    var $export;

    /**
     * CURLOPT_SSL_VERIFYPEER setting
     * @var  bool
     */
    public $ssl_verifypeer = false;
    /**
     * CURLOPT_SSL_VERIFYHOST setting
     * @var  bool
     */
    public $ssl_verifyhost = false;
    /**
     * CURLOPT_CAINFO
     * @var  string
     */
    public $ssl_cainfo = null;

    /**
     * the api key in use
     * @var  string
     */
    public $apikey;
    public $ch;
    public $root = 'https://api.mailchimp.com/2.0';
    /**
     * whether debug mode is enabled
     * @var  bool
     */
    public $debug = false;

    public static $error_map = array(
        "ValidationError" => "Mailchimp_ValidationError",
        "ServerError_MethodUnknown" => "Mailchimp_ServerError_MethodUnknown",
        "ServerError_InvalidParameters" => "Mailchimp_ServerError_InvalidParameters",
        "Unknown_Exception" => "Mailchimp_Unknown_Exception",
        "Request_TimedOut" => "Mailchimp_Request_TimedOut",
        "Zend_Uri_Exception" => "Mailchimp_Zend_Uri_Exception",
        "PDOException" => "Mailchimp_PDOException",
        "Avesta_Db_Exception" => "Mailchimp_Avesta_Db_Exception",
        "XML_RPC2_Exception" => "Mailchimp_XML_RPC2_Exception",
        "XML_RPC2_FaultException" => "Mailchimp_XML_RPC2_FaultException",
        "Too_Many_Connections" => "Mailchimp_Too_Many_Connections",
        "Parse_Exception" => "Mailchimp_Parse_Exception",
        "User_Unknown" => "Mailchimp_User_Unknown",
        "User_Disabled" => "Mailchimp_User_Disabled",
        "User_DoesNotExist" => "Mailchimp_User_DoesNotExist",
        "User_NotApproved" => "Mailchimp_User_NotApproved",
        "Invalid_ApiKey" => "Mailchimp_Invalid_ApiKey",
        "User_UnderMaintenance" => "Mailchimp_User_UnderMaintenance",
        "Invalid_AppKey" => "Mailchimp_Invalid_AppKey",
        "Invalid_IP" => "Mailchimp_Invalid_IP",
        "User_DoesExist" => "Mailchimp_User_DoesExist",
        "User_InvalidRole" => "Mailchimp_User_InvalidRole",
        "User_InvalidAction" => "Mailchimp_User_InvalidAction",
        "User_MissingEmail" => "Mailchimp_User_MissingEmail",
        "User_CannotSendCampaign" => "Mailchimp_User_CannotSendCampaign",
        "User_MissingModuleOutbox" => "Mailchimp_User_MissingModuleOutbox",
        "User_ModuleAlreadyPurchased" => "Mailchimp_User_ModuleAlreadyPurchased",
        "User_ModuleNotPurchased" => "Mailchimp_User_ModuleNotPurchased",
        "User_NotEnoughCredit" => "Mailchimp_User_NotEnoughCredit",
        "MC_InvalidPayment" => "Mailchimp_MC_InvalidPayment",
        "List_DoesNotExist" => "Mailchimp_List_DoesNotExist",
        "List_InvalidInterestFieldType" => "Mailchimp_List_InvalidInterestFieldType",
        "List_InvalidOption" => "Mailchimp_List_InvalidOption",
        "List_InvalidUnsubMember" => "Mailchimp_List_InvalidUnsubMember",
        "List_InvalidBounceMember" => "Mailchimp_List_InvalidBounceMember",
        "List_AlreadySubscribed" => "Mailchimp_List_AlreadySubscribed",
        "List_NotSubscribed" => "Mailchimp_List_NotSubscribed",
        "List_InvalidImport" => "Mailchimp_List_InvalidImport",
        "MC_PastedList_Duplicate" => "Mailchimp_MC_PastedList_Duplicate",
        "MC_PastedList_InvalidImport" => "Mailchimp_MC_PastedList_InvalidImport",
        "Email_AlreadySubscribed" => "Mailchimp_Email_AlreadySubscribed",
        "Email_AlreadyUnsubscribed" => "Mailchimp_Email_AlreadyUnsubscribed",
        "Email_NotExists" => "Mailchimp_Email_NotExists",
        "Email_NotSubscribed" => "Mailchimp_Email_NotSubscribed",
        "List_MergeFieldRequired" => "Mailchimp_List_MergeFieldRequired",
        "List_CannotRemoveEmailMerge" => "Mailchimp_List_CannotRemoveEmailMerge",
        "List_Merge_InvalidMergeID" => "Mailchimp_List_Merge_InvalidMergeID",
        "List_TooManyMergeFields" => "Mailchimp_List_TooManyMergeFields",
        "List_InvalidMergeField" => "Mailchimp_List_InvalidMergeField",
        "List_InvalidInterestGroup" => "Mailchimp_List_InvalidInterestGroup",
        "List_TooManyInterestGroups" => "Mailchimp_List_TooManyInterestGroups",
        "Campaign_DoesNotExist" => "Mailchimp_Campaign_DoesNotExist",
        "Campaign_StatsNotAvailable" => "Mailchimp_Campaign_StatsNotAvailable",
        "Campaign_InvalidAbsplit" => "Mailchimp_Campaign_InvalidAbsplit",
        "Campaign_InvalidContent" => "Mailchimp_Campaign_InvalidContent",
        "Campaign_InvalidOption" => "Mailchimp_Campaign_InvalidOption",
        "Campaign_InvalidStatus" => "Mailchimp_Campaign_InvalidStatus",
        "Campaign_NotSaved" => "Mailchimp_Campaign_NotSaved",
        "Campaign_InvalidSegment" => "Mailchimp_Campaign_InvalidSegment",
        "Campaign_InvalidRss" => "Mailchimp_Campaign_InvalidRss",
        "Campaign_InvalidAuto" => "Mailchimp_Campaign_InvalidAuto",
        "MC_ContentImport_InvalidArchive" => "Mailchimp_MC_ContentImport_InvalidArchive",
        "Campaign_BounceMissing" => "Mailchimp_Campaign_BounceMissing",
        "Campaign_InvalidTemplate" => "Mailchimp_Campaign_InvalidTemplate",
        "Invalid_EcommOrder" => "Mailchimp_Invalid_EcommOrder",
        "Absplit_UnknownError" => "Mailchimp_Absplit_UnknownError",
        "Absplit_UnknownSplitTest" => "Mailchimp_Absplit_UnknownSplitTest",
        "Absplit_UnknownTestType" => "Mailchimp_Absplit_UnknownTestType",
        "Absplit_UnknownWaitUnit" => "Mailchimp_Absplit_UnknownWaitUnit",
        "Absplit_UnknownWinnerType" => "Mailchimp_Absplit_UnknownWinnerType",
        "Absplit_WinnerNotSelected" => "Mailchimp_Absplit_WinnerNotSelected",
        "Invalid_Analytics" => "Mailchimp_Invalid_Analytics",
        "Invalid_DateTime" => "Mailchimp_Invalid_DateTime",
        "Invalid_Email" => "Mailchimp_Invalid_Email",
        "Invalid_SendType" => "Mailchimp_Invalid_SendType",
        "Invalid_Template" => "Mailchimp_Invalid_Template",
        "Invalid_TrackingOptions" => "Mailchimp_Invalid_TrackingOptions",
        "Invalid_Options" => "Mailchimp_Invalid_Options",
        "Invalid_Folder" => "Mailchimp_Invalid_Folder",
        "Invalid_URL" => "Mailchimp_Invalid_URL",
        "Module_Unknown" => "Mailchimp_Module_Unknown",
        "MonthlyPlan_Unknown" => "Mailchimp_MonthlyPlan_Unknown",
        "Order_TypeUnknown" => "Mailchimp_Order_TypeUnknown",
        "Invalid_PagingLimit" => "Mailchimp_Invalid_PagingLimit",
        "Invalid_PagingStart" => "Mailchimp_Invalid_PagingStart",
        "Max_Size_Reached" => "Mailchimp_Max_Size_Reached",
        "MC_SearchException" => "Mailchimp_MC_SearchException"
    );
    
    public function __construct($apikey=null, $opts=array()) {
        if(!$apikey) $apikey = getenv('MAILCHIMP_APIKEY');
        if(!$apikey) throw new Mailchimp_Error('You must provide a MailChimp API key');
        $this->apikey = $apikey;
        
        $this->root = $this->generateRoot($this->root);

        if (!isset($opts['timeout']) || !is_int($opts['timeout'])){
            $opts['timeout']=0;
        }
        if (isset($opts['debug'])){
            $this->debug = true;
        }
        if (isset($opts['ssl_verifypeer'])){
            $this->ssl_verifypeer = $opts['ssl_verifypeer'];
        }
        if (isset($opts['ssl_verifyhost'])){
            $this->ssl_verifyhost = $opts['ssl_verifyhost'];
        }
        if (isset($opts['ssl_cainfo'])){
            $this->ssl_cainfo = $opts['ssl_cainfo'];
        }

        require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');

        $this->ch = SugarChimp_Helper::initialize_curl();
        curl_setopt($this->ch, CURLOPT_USERAGENT, 'MailChimp-PHP/2.0.4');
        curl_setopt($this->ch, CURLOPT_POST, true);
        curl_setopt($this->ch, CURLOPT_HEADER, false);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, $opts['timeout']);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, false);

        // chad - 8/4/2015
        // customers on Sugar OD 7.5.2.1 and 7.5.2.2 started seeing a weird error
        // "Received problem 2 in the chunky parser"
        // after some googling, this is suppose to be the fix
        // if this doesn't do it, it could be related to an encoding issue and the following should be tried
        // curl_setopt HTTPHEADER Content-type: text/html; charset=utf-8
        // reference: http://stackoverflow.com/questions/2392041/why-is-this-warning-being-shown-received-problem-2-in-the-chunky-parser
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);

        // chad - 4/30/2015
        // this one causes issues in some ondemand instances, but not actually needed
        // anything that tries to connect to mailchimp will throw 500 error due to a curl error
        // Wed Apr 29 07:43:11 2015 [867][f4122fddeb7f6000a7aa16d18a4dc766][ERROR] A PHP error occurred: Warning: curl_setopt() [function.curl-setopt]: CURLOPT_FOLLOWLOCATION cannot be activated when safe_mode is enabled or an open_basedir is set occurred in /mnt/od2/store19/sugarondemand.com/nxirakia212223/modules/SugarChimp/includes/vendor/Mailchimp/Mailchimp.php on line 263 [2015-04-29 07:43:11]
        // 
        // curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);

        $this->folders = new Mailchimp_Folders($this);
        $this->templates = new Mailchimp_Templates($this);
        $this->users = new Mailchimp_Users($this);
        $this->helper = new Mailchimp_Helper($this);
        $this->mobile = new Mailchimp_Mobile($this);
        $this->ecomm = new Mailchimp_Ecomm($this);
        $this->neapolitan = new Mailchimp_Neapolitan($this);
        $this->lists = new Mailchimp_Lists($this);
        $this->campaigns = new Mailchimp_Campaigns($this);
        $this->vip = new Mailchimp_Vip($this);
        $this->reports = new Mailchimp_Reports($this);
        $this->gallery = new Mailchimp_Gallery($this);
        $this->export = new Mailchimp_Export($this);
    }

    public function __destruct() {
        curl_close($this->ch);
    }

    // support for a call to a streaming api
    // foreach line provided in the stream, the callback is called
    // support for custom api path provided through $root param (needed for export api as it has a different path)
    public function call_stream($url,$params,$callback,$root=false)
    {
        global $mailchimp_call_stream_params;
        $mailchimp_call_stream_params = $params;

        $params['apikey'] = $this->apikey;
        $params = http_build_query($params);
        $ch = $this->ch;

        if ($root === false) {
            // if no root is specified, use the root on the Mailchimp object
            $root = $this->root;
        } else {
            // else, generate a one-time root based on what's provided
            // this is needed as the export api is on a different root url
            $root = $this->generateRoot($root);
        }
        $uri = $root . $url;

        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        
        // SSL Options
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->ssl_verifypeer);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $this->ssl_verifyhost);
        if ($this->ssl_cainfo) curl_setopt($ch, CURLOPT_CAINFO, $this->ssl_cainfo);
        
        require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
        require_once('modules/SugarChimp/includes/classes/SugarChimp/Queue.php');
        
        SugarChimp_Helper::log('debug','MailChimp->call_stream Import Process: uri: '.print_r($uri,true));
        SugarChimp_Helper::log('debug','MailChimp->call_stream Import Process: params: '.print_r($params,true));
        
        // Callback stuff
        if ($url == 'list/')
        {
            curl_setopt($ch, CURLOPT_WRITEFUNCTION, function($ch, $str){
                global $mailchimp_incomplete_data, $mailchimp_call_stream_params;
            
                $length = strlen($str);

                $str = trim($str);
                $last_char = substr($str,-1);

                // if there's incomplete data saved, prepend it to what's been provided
                if (!empty($mailchimp_incomplete_data))
                {
                    $str = $mailchimp_incomplete_data . $str;
                    $mailchimp_incomplete_data = '';
                }

                // if it ends with ] we're good, otherwise handle it properly
                if ($last_char != ']')
                {
                    // find where the previous \n[ is and save the incomplete string
                    $beginning_position = strrpos($str,"\n[");
                
                    if ($beginning_position !== false)
                    {
                        $mailchimp_incomplete_data = substr($str,$beginning_position+1);
                        $str = substr($str,0,$beginning_position+1);
                    }
                    else
                    {
                        // if we can't find it, save the entire string for the next loop
                        $mailchimp_incomplete_data = $str;
                        return $length;
                    }
                }

                $lines = explode("\n",$str);

                if (!empty($lines))
                {
                    SugarChimp_Helper::log('debug','MailChimp->call_stream Import Process: number lines: '.count($lines));

                    if (!empty($mailchimp_call_stream_params['status']) && $mailchimp_call_stream_params['status']=='cleaned')
                    {
                        SugarChimp_Queue::queue_mailchimp_cleaned_emails($lines);
                    }
                    else
                    {
                        SugarChimp_Queue::queue_mailchimp_subscribers($lines);
                    }
                }

                return $length;
            });
        }
        else if ($url == 'campaignSubscriberActivity/')
        {
            curl_setopt($ch, CURLOPT_WRITEFUNCTION, function($ch, $str){
                global $mailchimp_incomplete_data;
            
                $length = strlen($str);

                $str = trim($str);
                $last_char = substr($str,-2);

                // if there's incomplete data saved, prepend it to what's been provided
                if (!empty($mailchimp_incomplete_data))
                {
                    $str = $mailchimp_incomplete_data . $str;
                    $mailchimp_incomplete_data = '';
                }

                // if it ends with ]} we're good, otherwise handle it properly
                if ($last_char != ']}')
                {
                    // find where the previous \n{ is and save the incomplete string
                    $beginning_position = strrpos($str,"\n{");
                
                    if ($beginning_position !== false)
                    {
                        $mailchimp_incomplete_data = substr($str,$beginning_position+1);
                        $str = substr($str,0,$beginning_position+1);
                    }
                    else
                    {
                        // if we can't find it, save the entire string for the next loop
                        $mailchimp_incomplete_data = $str;
                        return $length;
                    }
                }

                $lines = explode("\n",$str);
            
                if (!empty($lines))
                {
                    foreach ($lines as $line)
                    {
                        $line = $line;
                        SugarChimp_Helper::log('debug',$line);
                        SugarChimp_Queue::queue_mailchimp_activity(trim($line));
                    }
                }

                return $length;
            });
        }
        
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);

        if(curl_error($ch)) 
        {
            throw new Mailchimp_HttpError("API call to $url failed: " . curl_error($ch));
        }
        
        curl_close($ch);
    }

    public function call($url, $params) {
        $params['apikey'] = $this->apikey;
        $params = json_encode($params);
        $ch = $this->ch;

        curl_setopt($ch, CURLOPT_URL, $this->root . $url . '.json');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_VERBOSE, $this->debug);
        // SSL Options
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->ssl_verifypeer);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $this->ssl_verifyhost);
        if ($this->ssl_cainfo) curl_setopt($ch, CURLOPT_CAINFO, $this->ssl_cainfo);

        $response_body = curl_exec($ch);
        $info = curl_getinfo($ch);

        if(curl_error($ch)) {
            throw new Mailchimp_HttpError("API call to $url failed: " . curl_error($ch));
        }
        $result = json_decode($response_body, true);

        require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
        
        SugarChimp_Helper::log('debug','MailChimp->call(v2) result: '.print_r($result,true));

        if(floor($info['http_code'] / 100) >= 4) {
            throw $this->castError($result);
        }

        return $result;
    }

    public function castError($result) {
        if($result['status'] !== 'error' || !$result['name']) throw new Mailchimp_Error('We received an unexpected error: ' . json_encode($result));

        $class = (isset(self::$error_map[$result['name']])) ? self::$error_map[$result['name']] : 'Mailchimp_Error';
        
        if (isset(self::$error_map[$result['name']]))
        {
            switch ($result['name'])
            {
                case "ValidationError": 
                    return new Mailchimp_ValidationError($result['error'], $result['code']); break;
                case "ServerError_MethodUnknown":
                    return new Mailchimp_ServerError_MethodUnknown($result['error'], $result['code']); break;
                case "ServerError_InvalidParameters":
                    return new Mailchimp_ServerError_InvalidParameters($result['error'], $result['code']); break;
                case "Unknown_Exception":
                    return new Mailchimp_Unknown_Exception($result['error'], $result['code']); break;
                case "Request_TimedOut":
                    return new Mailchimp_Request_TimedOut($result['error'], $result['code']); break;
                case "Zend_Uri_Exception":
                    return new Mailchimp_Zend_Uri_Exception($result['error'], $result['code']); break;
                case "PDOException":
                    return new Mailchimp_PDOException($result['error'], $result['code']); break;
                case "Avesta_Db_Exception":
                    return new Mailchimp_Avesta_Db_Exception($result['error'], $result['code']); break;
                case "XML_RPC2_Exception":
                    return new Mailchimp_XML_RPC2_Exception($result['error'], $result['code']); break;
                case "XML_RPC2_FaultException":
                    return new Mailchimp_XML_RPC2_FaultException($result['error'], $result['code']); break;
                case "Too_Many_Connections":
                    return new Mailchimp_Too_Many_Connections($result['error'], $result['code']); break;
                case "Parse_Exception":
                    return new Mailchimp_Parse_Exception($result['error'], $result['code']); break;
                case "User_Unknown":
                    return new Mailchimp_User_Unknown($result['error'], $result['code']); break;
                case "User_Disabled":
                    return new Mailchimp_User_Disabled($result['error'], $result['code']); break;
                case "User_DoesNotExist":
                    return new Mailchimp_User_DoesNotExist($result['error'], $result['code']); break;
                case "User_NotApproved":
                    return new Mailchimp_User_NotApproved($result['error'], $result['code']); break;
                case "Invalid_ApiKey":
                    return new Mailchimp_Invalid_ApiKey($result['error'], $result['code']); break;
                case "User_UnderMaintenance":
                    return new Mailchimp_User_UnderMaintenance($result['error'], $result['code']); break;
                case "Invalid_AppKey":
                    return new Mailchimp_Invalid_AppKey($result['error'], $result['code']); break;
                case "Invalid_IP":
                    return new Mailchimp_Invalid_IP($result['error'], $result['code']); break;
                case "User_DoesExist":
                    return new Mailchimp_User_DoesExist($result['error'], $result['code']); break;
                case "User_InvalidRole":
                    return new Mailchimp_User_InvalidRole($result['error'], $result['code']); break;
                case "User_InvalidAction":
                    return new Mailchimp_User_InvalidAction($result['error'], $result['code']); break;
                case "User_MissingEmail":
                    return new Mailchimp_User_MissingEmail($result['error'], $result['code']); break;
                case "User_CannotSendCampaign":
                    return new Mailchimp_User_CannotSendCampaign($result['error'], $result['code']); break;
                case "User_MissingModuleOutbox":
                    return new Mailchimp_User_MissingModuleOutbox($result['error'], $result['code']); break;
                case "User_ModuleAlreadyPurchased":
                    return new Mailchimp_User_ModuleAlreadyPurchased($result['error'], $result['code']); break;
                case "User_ModuleNotPurchased":
                    return new Mailchimp_User_ModuleNotPurchased($result['error'], $result['code']); break;
                case "User_NotEnoughCredit":
                    return new Mailchimp_User_NotEnoughCredit($result['error'], $result['code']); break;
                case "MC_InvalidPayment":
                    return new Mailchimp_MC_InvalidPayment($result['error'], $result['code']); break;
                case "List_DoesNotExist":
                    return new Mailchimp_List_DoesNotExist($result['error'], $result['code']); break;
                case "List_InvalidInterestFieldType":
                    return new Mailchimp_List_InvalidInterestFieldType($result['error'], $result['code']); break;
                case "List_InvalidOption":
                    return new Mailchimp_List_InvalidOption($result['error'], $result['code']); break;
                case "List_InvalidUnsubMember":
                    return new Mailchimp_List_InvalidUnsubMember($result['error'], $result['code']); break;
                case "List_InvalidBounceMember":
                    return new Mailchimp_List_InvalidBounceMember($result['error'], $result['code']); break;
                case "List_AlreadySubscribed":
                    return new Mailchimp_List_AlreadySubscribed($result['error'], $result['code']); break;
                case "List_NotSubscribed":
                    return new Mailchimp_List_NotSubscribed($result['error'], $result['code']); break;
                case "List_InvalidImport":
                    return new Mailchimp_List_InvalidImport($result['error'], $result['code']); break;
                case "MC_PastedList_Duplicate":
                    return new Mailchimp_MC_PastedList_Duplicate($result['error'], $result['code']); break;
                case "MC_PastedList_InvalidImport":
                    return new Mailchimp_MC_PastedList_InvalidImport($result['error'], $result['code']); break;
                case "Email_AlreadySubscribed":
                    return new Mailchimp_Email_AlreadySubscribed($result['error'], $result['code']); break;
                case "Email_AlreadyUnsubscribed":
                    return new Mailchimp_Email_AlreadyUnsubscribed($result['error'], $result['code']); break;
                case "Email_NotExists":
                    return new Mailchimp_Email_NotExists($result['error'], $result['code']); break;
                case "Email_NotSubscribed":
                    return new Mailchimp_Email_NotSubscribed($result['error'], $result['code']); break;
                case "List_MergeFieldRequired":
                    return new Mailchimp_List_MergeFieldRequired($result['error'], $result['code']); break;
                case "List_CannotRemoveEmailMerge":
                    return new Mailchimp_List_CannotRemoveEmailMerge($result['error'], $result['code']); break;
                case "List_Merge_InvalidMergeID":
                    return new Mailchimp_List_Merge_InvalidMergeID($result['error'], $result['code']); break;
                case "List_TooManyMergeFields":
                    return new Mailchimp_List_TooManyMergeFields($result['error'], $result['code']); break;
                case "List_InvalidMergeField":
                    return new Mailchimp_List_InvalidMergeField($result['error'], $result['code']); break;
                case "List_InvalidInterestGroup":
                    return new Mailchimp_List_InvalidInterestGroup($result['error'], $result['code']); break;
                case "List_TooManyInterestGroups":
                    return new Mailchimp_List_TooManyInterestGroups($result['error'], $result['code']); break;
                case "Campaign_DoesNotExist":
                    return new Mailchimp_Campaign_DoesNotExist($result['error'], $result['code']); break;
                case "Campaign_StatsNotAvailable":
                    return new Mailchimp_Campaign_StatsNotAvailable($result['error'], $result['code']); break;
                case "Campaign_InvalidAbsplit":
                    return new Mailchimp_Campaign_InvalidAbsplit($result['error'], $result['code']); break;
                case "Campaign_InvalidContent":
                    return new Mailchimp_Campaign_InvalidContent($result['error'], $result['code']); break;
                case "Campaign_InvalidOption":
                    return new Mailchimp_Campaign_InvalidOption($result['error'], $result['code']); break;
                case "Campaign_InvalidStatus":
                    return new Mailchimp_Campaign_InvalidStatus($result['error'], $result['code']); break;
                case "Campaign_NotSaved":
                    return new Mailchimp_Campaign_NotSaved($result['error'], $result['code']); break;
                case "Campaign_InvalidSegment":
                    return new Mailchimp_Campaign_InvalidSegment($result['error'], $result['code']); break;
                case "Campaign_InvalidRss":
                    return new Mailchimp_Campaign_InvalidRss($result['error'], $result['code']); break;
                case "Campaign_InvalidAuto":
                    return new Mailchimp_Campaign_InvalidAuto($result['error'], $result['code']); break;
                case "MC_ContentImport_InvalidArchive":
                    return new Mailchimp_MC_ContentImport_InvalidArchive($result['error'], $result['code']); break;
                case "Campaign_BounceMissing":
                    return new Mailchimp_Campaign_BounceMissing($result['error'], $result['code']); break;
                case "Campaign_InvalidTemplate":
                    return new Mailchimp_Campaign_InvalidTemplate($result['error'], $result['code']); break;
                case "Invalid_EcommOrder":
                    return new Mailchimp_Invalid_EcommOrder($result['error'], $result['code']); break;
                case "Absplit_UnknownError":
                    return new Mailchimp_Absplit_UnknownError($result['error'], $result['code']); break;
                case "Absplit_UnknownSplitTest":
                    return new Mailchimp_Absplit_UnknownSplitTest($result['error'], $result['code']); break;
                case "Absplit_UnknownTestType":
                    return new Mailchimp_Absplit_UnknownTestType($result['error'], $result['code']); break;
                case "Absplit_UnknownWaitUnit":
                    return new Mailchimp_Absplit_UnknownWaitUnit($result['error'], $result['code']); break;
                case "Absplit_UnknownWinnerType":
                    return new Mailchimp_Absplit_UnknownWinnerType($result['error'], $result['code']); break;
                case "Absplit_WinnerNotSelected":
                    return new Mailchimp_Absplit_WinnerNotSelected($result['error'], $result['code']); break;
                case "Invalid_Analytics":
                    return new Mailchimp_Invalid_Analytics($result['error'], $result['code']); break;
                case "Invalid_DateTime":
                    return new Mailchimp_Invalid_DateTime($result['error'], $result['code']); break;
                case "Invalid_Email":
                    return new Mailchimp_Invalid_Email($result['error'], $result['code']); break;
                case "Invalid_SendType":
                    return new Mailchimp_Invalid_SendType($result['error'], $result['code']); break;
                case "Invalid_Template":
                    return new Mailchimp_Invalid_Template($result['error'], $result['code']); break;
                case "Invalid_TrackingOptions":
                    return new Mailchimp_Invalid_TrackingOptions($result['error'], $result['code']); break;
                case "Invalid_Options":
                    return new Mailchimp_Invalid_Options($result['error'], $result['code']); break;
                case "Invalid_Folder":
                    return new Mailchimp_Invalid_Folder($result['error'], $result['code']); break;
                case "Invalid_URL":
                    return new Mailchimp_Invalid_URL($result['error'], $result['code']); break;
                case "Module_Unknown":
                    return new Mailchimp_Module_Unknown($result['error'], $result['code']); break;
                case "MonthlyPlan_Unknown":
                    return new Mailchimp_MonthlyPlan_Unknown($result['error'], $result['code']); break;
                case "Order_TypeUnknown":
                    return new Mailchimp_Order_TypeUnknown($result['error'], $result['code']); break;
                case "Invalid_PagingLimit":
                    return new Mailchimp_Invalid_PagingLimit($result['error'], $result['code']); break;
                case "Invalid_PagingStart":
                    return new Mailchimp_Invalid_PagingStart($result['error'], $result['code']); break;
                case "Max_Size_Reached":
                    return new Mailchimp_Max_Size_Reached($result['error'], $result['code']); break;
                case "MC_SearchException":
                    return new Mailchimp_MC_SearchException($result['error'], $result['code']); break;
            }
        }
        
        return new Mailchimp_Error($result['error'], $result['code']);
    }

    // generate a root url on the fly
    public function generateRoot($root)
    {
        $dc = "us1";
        if (strstr($this->apikey,"-")){
            list($key, $dc) = explode("-",$this->apikey,2);
            if (!$dc) $dc = "us1";
        }
        $root = str_replace('https://api', 'https://'.$dc.'.api', $root);
        $root = rtrim($root, '/') . '/';
        
        return $root;
    }
}