<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Logger.php');
require_once('modules/SugarChimp/includes/vendor/Mailchimp/MailchimpV3.php');

/**

// calls that need to be migrated from v2 to v3 api
//** $mc->lists->getList(array(),0,100,'name','ASC');
//** $mc->lists->memberInfo($mailchimp_list_id,$email_data);
//** $mc->lists->mergeVars(array($list_id));
//** $mc->lists->interestGroupings($list_id);
//** $mc->lists->interestGroupAdd($list_id,$group_name,$grouping_id); - removed the old methods as no longer being used
//** $mc->lists->batchSubscribe($this->list_id,$batch,false,true,false);
//** $mc->lists->webhookAdd($list_id,$webhook_url);
//** $mc->helper->ping();
//** $mc->helper->searchMembers($email); - pushed to 7.8.1, see asana task: https://app.asana.com/0/196642389843064/198516398828679
//** $mc->campaigns->getList($filter); - used combination of get_campaigns, get_campaigns, get_report

// we don't have to mess with these because the export API will still be supported
//** $mc->export->getList('queue_mailchimp_subscriber',$mailchimp_list_id);
//** $mc->export->campaignSubscriberActivity('queue_mailchimp_activity',$mailchimp_campaign_id,$include_empty,$since);

*/

class MailChimp_API 
{
	public static function ping($apikey=false,$args=array())
	{
		if (empty($apikey))
		{
			$apikey = SugarChimp_Setting::retrieve('apikey');
		}

		// if STILL empty
		if (empty($apikey))
		{
			return false;
		}

		$mc = new MailChimpV3($apikey);
		$result = $mc->getRequest('/',$args);

        SugarChimp_Logger::log('debug','MailChimp_API: ping result: '.print_r($result,true));

		return $result;
	}

	// In order to make a 3.0 Create a List call, these are required:
	// ["name", "permission_reminder", "email_type_option", "contact", "campaign_defaults"]
	public static function create_list($params)
	{
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->post('lists',$params);

        SugarChimp_Logger::log('debug','MailChimp_API: create_list result: '.print_r($result,true));

		return $result;
	}

	public static function get_list($id, $args=array())
	{
		if(empty($args['fields'])){
			// returning 10 by default
			$args['fields'] = 'id,name';
		}
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->getRequest('lists/'.$id);

        SugarChimp_Logger::log('debug','MailChimp_API: get_list result: '.print_r($result,true));
        
		return $result;
	}

	public static function delete_list($id)
	{
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->delete('lists/'.$id);

        SugarChimp_Logger::log('debug','MailChimp_API: delete_list result: '.print_r($result,true));
        
		return $result;
	}

	/**
     * Retrieve all of the lists defined for your user account
     * $args optional array
     *     - fields - string : A comma-separated list of fields to return. Reference parameters of sub-objects with dot notation.
     *	   - exclude_fields - string : A comma-separated list of fields to exclude. Reference parameters of sub-objects with dot notation.
     *     - count int - how many lists to include
     *     - before_date_created string - Restrict response to lists created before the set date - 24 hour format in <strong>GMT</strong>, eg "2013-12-30 20:30:00"
     *     - since_date_created string - Restrict response to lists created after the set date - 24 hour format in <strong>GMT</strong>, eg "2013-12-30 20:30:00"
     *     - email string - only include lists with this email in them
    */
	public static function get_lists($args=array())
	{
		if(empty($args['count'])){
			// returning 10 by default
			$args['count'] = 1000;
		}
		if(empty($args['fields'])){
			// adding member stats greatly increases response time
			$args['fields'] = 'lists.id,lists.name,lists.date_created,total_items';
		}


		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->getRequest('lists',$args);
		SugarChimp_Logger::log('debug','MailChimp_API::get_lists result: '.print_r($result,true));

		return $result;
	}

	public static function get_merge_fields($list_id, $args=array())
	{
		if (empty($args['count']))
		{
			// returning 10 by default
			$args['count'] = 100;
		}

		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->getRequest("lists/{$list_id}/merge-fields",$args);

		SugarChimp_Logger::log('debug','MailChimp_API: get_merge_fields result: '.print_r($result,true));
        
		return $result;
	}

	// params['name','type','options[default_country,phone_format,date_format,choices array, size]']
	public static function add_merge_field($list_id, $params)
	{
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->post("lists/{$list_id}/merge-fields",$params);

		SugarChimp_Logger::log('debug','MailChimp_API: add_merge_fields result: '.print_r($result,true));
        
		return $result;
	}
	public static function get_interest_categories($list_id, $args=array())
	{
		if (empty($args['count']))
		{
			$args['count'] = 100;
		}

		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->getRequest('lists/'.$list_id.'/interest-categories',$args);

        SugarChimp_Logger::log('debug','MailChimp_API: get_interest_categories result: '.print_r($result,true));
        
		return $result;
	}
	// $title = name of Interest Category, shown on Signup forms
	// $type = one of [checkboxes,dropdown,radio,hidden]
	public static function add_interest_category($list_id, $title, $type)
	{
		$params = array(
			'title'=>$title,
			'type'=>$type
		);
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->post('lists/'.$list_id.'/interest-categories',$params);

        SugarChimp_Logger::log('debug','MailChimp_API: add_interest_category result: '.print_r($result,true));
        
		return $result;
	}

	public static function get_interests($list_id,$category_id, $args=array())
	{
		if (empty($args['count']))
		{
			$args['count'] = 100;
		}

		$ext = 'lists/'.$list_id.'/interest-categories/'.$category_id.'/interests';
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->getRequest($ext,$args);

        SugarChimp_Logger::log('debug','MailChimp_API: get_interests result: '.print_r($result,true));
        
		return $result;
	}

	public static function add_interest($list_id, $interest_category_id, $name)
	{
		$params = array(
			'name'=>$name
		);
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->post("lists/{$list_id}/interest-categories/{$interest_category_id}/interests", $params);

        SugarChimp_Logger::log('debug','MailChimp_API: add_interest result: '.print_r($result,true));
        
		return $result;
	}

	public static function delete_interest($list_id, $interest_category_id, $interest_id)
	{
		$params = array();
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->delete("lists/{$list_id}/interest-categories/{$interest_category_id}/interests/{$interest_id}", $params);

        SugarChimp_Logger::log('debug','MailChimp_API: delete_interest result: '.print_r($result,true));
        
		return $result;
	}

	// get unsubscribed and cleaned members
	public static function get_unsubscribed_list_members($list_id,$args=array())
	{
		if (empty($args['count']))
		{
			$args['count'] = 100;
		}

		if (empty($args['offset']))
		{
			$args['offset'] = 0;
		}

		$args['status'] = 'unsubscribed,cleaned';

		$ext = 'lists/'.$list_id.'/members';
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->getRequest($ext,$args);

		SugarChimp_Logger::log('debug','MailChimp_API: get_unsubscribed_list_members result: '.print_r($result,true));

		return $result;
	}

	public static function search_members($query,$args=array())
	{
		$args['query'] = $query;
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->getRequest('/search-members',$args);

        SugarChimp_Logger::log('debug','MailChimp_API: search_members result: '.print_r($result,true));

		return $result;
	}

	public static function get_campaigns($args=array())
	{
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->getRequest('/campaigns',$args);

        SugarChimp_Logger::log('debug','MailChimp_API: get_campaigns result: '.print_r($result,true));

		return $result;
	}

	public static function get_campaign($id,$args=array())
	{
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->getRequest('/campaigns/'.$id,$args);

        SugarChimp_Logger::log('debug','MailChimp_API: get_campaign result: '.print_r($result,true));

		return $result;
	}

	public static function get_report($id,$args=array())
	{
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->getRequest('/reports/'.$id,$args);

        SugarChimp_Logger::log('debug','MailChimp_API: get_report result: '.print_r($result,true));

		return $result;
	}

	public static function batch_update_subscribers($list_id,$subscribers)
	{
		if (empty($list_id))
		{
			SugarChimp_Logger::log('warning','MailChimp_API batch_subscribe: list_id is empty');
			return false;
		}

		if (empty($subscribers) or !is_array($subscribers))
		{
			SugarChimp_Logger::log('warning','MailChimp_API batch_subscribe: subscribers is empty or not an array');
			return false;
		}

		$operations = array();
		$type = "PUT";
		$url = "lists/{$list_id}/members/";

		foreach ($subscribers as $key => $subscriber)
		{
			if (empty($subscriber['id']))
			{
				// id is required
				SugarChimp_Logger::log('warning','MailChimp_API batch_subscribe: no id for subscriber '.print_r($subscriber,true));
				continue;
			}

			if (empty($subscriber['email_address']))
			{
				// email address required
				SugarChimp_Logger::log('warning','MailChimp_API batch_subscribe: no email_address for subscriber '.print_r($subscriber,true));
				continue;
			}

			// add the ability to send hashable email for 
			// updating subscriber email address in v3
			$email_hash = self::get_email_hash($subscriber['email_address']);
			if (!empty($subscriber['hashable_email'])){
				$email_hash = self::get_email_hash($subscriber['hashable_email']);
			}
			
			$path = $url . $email_hash;

			$operation = array(
				"method" => $type,
				"path" => $path,
				"operation_id" => $subscriber['id'],
				"body" => json_encode($subscriber),
			);

			SugarChimp_Logger::log('debug','MailChimp_API operation: '.print_r($operation,true));
			$operations []= $operation;
		}

		return self::post_batches($operations,$list_id);
	}

	// method specifically used to unsubscribe/clean members from list
	// we only send the 'status' field in the body
	public static function batch_remove_subscribers($list_id,$subscribers)
	{
		if (empty($list_id))
		{
			SugarChimp_Logger::log('warning','MailChimp_API batch_subscribe: list_id is empty');
			return false;
		}

		if (empty($subscribers) or !is_array($subscribers))
		{
			SugarChimp_Logger::log('warning','MailChimp_API batch_subscribe: subscribers is empty or not an array');
			return false;
		}

		$operations = array();
		$type = "PUT";
		$url = "lists/{$list_id}/members/";

		foreach ($subscribers as $key => $subscriber)
		{
			if (empty($subscriber['id']))
			{
				// id is required
				SugarChimp_Logger::log('warning','MailChimp_API batch_subscribe: no id for subscriber '.print_r($subscriber,true));
				continue;
			}

			if (empty($subscriber['email_address']))
			{
				// email address required
				SugarChimp_Logger::log('warning','MailChimp_API batch_subscribe: no email_address for subscriber '.print_r($subscriber,true));
				continue;
			}

			// add the ability to send hashable email for 
			// updating subscriber email address in v3
			$email_hash = self::get_email_hash($subscriber['email_address']);
			if (!empty($subscriber['hashable_email'])){
				$email_hash = self::get_email_hash($subscriber['hashable_email']);
			}
			
			$path = $url . $email_hash;

			$oid = $subscriber['id'];
			
			$subscriber_data = array();

			if (!empty($subscriber['status']))
			{
				$subscriber_data['status'] = $subscriber['status'];
			}

            $operation = array(
                "method" => $type,
                "path" => $path,
                "operation_id" => $oid,
                "body" => json_encode($subscriber_data),
            );

			SugarChimp_Logger::log('debug','MailChimp_API operation: '.print_r($operation,true));
			$operations []= $operation;
		}

		return self::post_batches($operations,$list_id);
	}

	// this method actually DELETES the subscribers from lists
	// different from unsubscribing/cleaning
	// subscribers is array of email keys and arrays of list ids
	// ex. array(
	// 	'email1@a.com' => array('list1id','list2is','list3id'),
	// 	'email2@a.com' => array('list2id','list4is','list6id'),
	// )
	// this would send a batch of 6 delete requests
	public static function batch_delete_subscribers($subscribers)
	{
		if (empty($subscribers) or !is_array($subscribers))
		{
			SugarChimp_Logger::log('warning','MailChimp_API batch_delete: subscribers is empty or not an array');
			return false;
		}

		$operations = array();
		
		foreach ($subscribers as $email => $list_ids)
		{
			if (empty($email))
			{
				// email is required
				SugarChimp_Logger::log('warning','MailChimp_API batch_delete: no email for subscriber '.print_r($subscriber,true));
				continue;
			}

			if (empty($list_ids) or !is_array($list_ids))
			{
				// list_ids array required
				SugarChimp_Logger::log('warning','MailChimp_API batch_delete: no list_ids for eamil '.print_r($subscriber,true));
				continue;
			}

			// add the ability to send hashable email for 
			$email_hash = self::get_email_hash($email);
			
			foreach($list_ids as $list_id)
			{
				$path = "lists/" . $list_id . "/members/" . $email_hash;

				$operation = array(
					"method" => "DELETE",
					"path" => $path,
					"operation_id" => $email,
				);
				SugarChimp_Logger::log('debug','MailChimp_API::batch_delete_subscribers: operation: '.print_r($operation,true));

				$operations []= $operation;
			}

			$operations []= $operation;
		}

		return self::post_batches($operations);
	}

	public static function post_batches($operations, $list_id=false)
	{
		if (empty($operations) or !is_array($operations))
		{
			return false;
		}

		$data = array(
			"operations" => $operations
		);
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->post('batches',$data);

		SugarChimp_Logger::log('debug','MailChimp_API post_batches: '.print_r($result,true));

		if (!empty($result['id']))
		{
			//create SugarChimpBatches obj from response
			$scbatch = BeanFactory::newBean('SugarChimpBatches');
			$scbatch->batch_id = $result['id'];
	   		$scbatch->mailchimp_list_id = $list_id;
	   		$scbatch->save();
	   	}
	   	else
	   	{
			SugarChimp_Logger::log('fatal','MailChimp_API post_batches: The batch was not able to be successfully submitted. result: '.print_r($result,true));
		}

		return $result;
	}

	public static function get_batch($batch_id)
	{
		if (empty($batch_id))
		{
			return false;
		}
		$data = array();
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->getRequest("batches/{$batch_id}",$data);

		SugarChimp_Logger::log('debug','MailChimp_API get_batch: '.print_r($result,true));

		return $result;
	}

	public static function get_email_hash($email)
	{
		if (empty($email))
		{
			SugarChimp_Logger::log('warning','MailChimp_API get_email_hash: no email provided');
			return false;
		}

		return md5(strtolower(trim($email)));
	}

	public static function get_member($list_id,$email,$params=array())
	{
		if (empty($params))
		{
			$params = array(
				'fields'=>array(
					'id',
					'email_address',
					'unique_email_id',
					'status',
					'merge_fields',
					'interests',
					'member_rating',
					'list_id'
				),
			);
		}
		
		$email_hash = self::get_email_hash($email);

		$ext = "lists/{$list_id}/members/{$email_hash}";
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->getRequest("lists/{$list_id}/members/{$email_hash}",$params);

		SugarChimp_Logger::log('debug','MailChimp_API get_member: '.print_r($result,true));

		return $result;
	}

	public static function get_member_activity($list_id,$email)
	{
		$email_hash = self::get_email_hash($email);

		$ext = 'lists/'.$list_id."/members/{$email_hash}/activity";
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->getRequest($ext);

		SugarChimp_Logger::log('debug','MailChimp_API get_member_activity: '.print_r($result,true));

		return $result;
	}

	public static function get_webhooks($list_id, $args=array())
	{
		if (empty($args['count']))
		{
			$args['count'] = 100;
		}

		$ext = 'lists/'.$list_id.'/webhooks';
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->getRequest($ext,$args);

        SugarChimp_Logger::log('debug','MailChimp_API: get_webhooks result: '.print_r($result,true));
        
		return $result;
	}

	public static function add_webhook($list_id, $url, $events=array(), $sources=array())
	{
		if (empty($events))
		{
			$events = array(
				"subscribe"=>true,
				"unsubscribe"=>true,
				"profile"=>true,
				"cleaned"=>false,
				"upemail"=>true,
				"campaign"=>true
			);
		}

		if (empty($sources))
		{
			$sources = array(
				"user"=>true,
				"admin"=>true,
				"api"=>true,
			);
		}

		$params = array(
			'url' => $url,
			'events' => $events,
			'sources' => $sources,
		);
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->post("lists/{$list_id}/webhooks", $params);

        SugarChimp_Logger::log('debug','MailChimp_API: add_webhook result: '.print_r($result,true));
        
		return $result;
	}

	public static function update_webhook($list_id, $webhook_id, $webhook_url=false, $events=false, $sources=false)
	{
		$args = array();

		if ($webhook_url !== false) $args['url'] = $webhook_url;
		if ($events !== false) $args['events'] = $events;
		if ($sources !== false) $args['sources'] = $sources;
		
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$mc = new MailChimpV3($apikey);
		$result = $mc->patch("lists/{$list_id}/webhooks/{$webhook_id}", $args);

        SugarChimp_Logger::log('debug','MailChimp_API: update_webhook result: '.print_r($result,true));
        
		return $result;
	}

	/*
	// the base API url is auto populated
	// assuming json is being used goint to MC and returned from them
	// $ext is used to set the extension
	// example: call_API('POST', 'lists/'
	public static function call_API($request_type, $ext='', $body='')
	{
		$apikey = SugarChimp_Setting::retrieve('apikey');
		$split = explode('-',$apikey);
		$dc = $split[1];
		$url = $dc . '.api.mailchimp.com/3.0/' . $ext;
		$curl = curl_init();
		$opts = array(
			CURLOPT_HTTPHEADER => array('Authorization: apikey ' . $apikey),
		    CURLOPT_URL => $url,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_CUSTOMREQUEST => $request_type,
			CURLOPT_SSL_VERIFYPEER => false,
        	CURLOPT_SSL_VERIFYHOST => false,
	    );

		switch ($request_type)
        {
        	case 'GET':
        		if (!empty($body))
        		{
        			$query = http_build_query($body, '', '&');
                	$opts[CURLOPT_URL] = $url . '?' . $query;
                }
        		break;
            case 'POST':
            case 'DELETE':
			    $opts[CURLOPT_POST] = 1;
			    $opts[CURLOPT_POSTFIELDS] = json_encode($body);
			    break;
            case 'PATCH':
            case 'PUT':
			    $opts[CURLOPT_POSTFIELDS] = json_encode($body);
                break;
		}
		
		curl_setopt_array($curl, $opts);
		$result = curl_exec($curl);

		SugarChimp_Logger::log('debug','MailChimp_API call_API: '.print_r($result,true));

		if (empty($result))
		{
			$error_number = curl_errno($curl);
			$error_message = curl_error($curl);
			SugarChimp_Logger::log('fatal','MailChimp_API call_API: There was a curl error: '.print_r($error_number,true).' - '.print_r($error_message,true));
			SugarChimp_Logger::log('fatal','MailChimp_API call_API: request_type'.print_r($request_type,true));
			SugarChimp_Logger::log('fatal','MailChimp_API call_API: ext'.print_r($ext,true));
			SugarChimp_Logger::log('fatal','MailChimp_API call_API: body'.print_r($body,true));
		}

		curl_close($curl);

		$data = json_decode($result);
		
		if (empty($data))
		{
			SugarChimp_Logger::log('fatal','MailChimp_API call_API: response could not be json decoded or there was no response: '.print_r($result,true));
		}

		return $data;
	}
	*/
}