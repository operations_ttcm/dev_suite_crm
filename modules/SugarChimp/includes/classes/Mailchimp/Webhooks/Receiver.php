<?php







/*

To test a MailChimp webhook locally, do this:
1. Create a http://requestb.in/ link
2. Add the link to the MailChimp list webhooks
3. Do whatever it is you want to test on the MailChimp side
4. Requestb.in will catch the webhook, copy the "RAW BODY"
5. Go to terminal, and use the line below. Paste in the raw body.

curl -d 'module=SugarChimp&entryPoint=SugarChimpWebhook&{RAW BODY GOES HERE}' http://path-to-sugar.dev/index.php
example: 
curl -d 'module=SugarChimp&entryPoint=SugarChimpWebhook&type=profile&fired_at=2016-01-14+04%3A00%3A51&data%5Bid%5D=33ef44d2df&data%5Bemail%5D=hutchins.chad%2Btest4%40gmail.com&data%5Bemail_type%5D=html&data%5Bip_opt%5D=50.26.190.146&data%5Bweb_id%5D=240611949&data%5Bmerges%5D%5BEMAIL%5D=hutchins.chad%2Btest4%40gmail.com&data%5Bmerges%5D%5BFNAME%5D=Chad+44&data%5Bmerges%5D%5BLNAME%5D=Hutchins+4&data%5Bmerges%5D%5BMMERGE3%5D=Chad+Hutchins&data%5Bmerges%5D%5BMMERGE4%5D=2016-06-24&data%5Bmerges%5D%5BMMERGE6%5D=2016-07-25&data%5Bmerges%5D%5BMMERGE7%5D=2016-08-26&data%5Bmerges%5D%5BINTERESTS%5D=&data%5Bmerges%5D%5BGROUPINGS%5D%5B0%5D%5Bid%5D=15677&data%5Bmerges%5D%5BGROUPINGS%5D%5B0%5D%5Bname%5D=Which+emails+do+you+want+to+receive%3F&data%5Bmerges%5D%5BGROUPINGS%5D%5B0%5D%5Bgroups%5D=&data%5Blist_id%5D=f2082ab659' http://6522.test1.dev/index.php
// Regular Campaign Send request
curl -d 'module=SugarChimp&entryPoint=SugarChimpWebhook&type=campaign&fired_at=2017-02-20+21%3A17%3A33&data%5Bid%5D=776684e8ca&data%5Bsubject%5D=This+is+My+Subject+of+Matter&data%5Bstatus%5D=sent&data%5Breason%5D=&data%5Blist_id%5D=f681d24081' http://7800a.pro.loc/index.php
// Automation Campaign Send request
curl -d 'module=SugarChimp&entryPoint=SugarChimpWebhook&type=campaign&fired_at=2017-02-20+21%3A09%3A37&data%5Bid%5D=ab60ee87fa&data%5Bsubject%5D=Welcome+to+SugarChimp%21&data%5Bstatus%5D=sent&data%5Breason%5D=&data%5Blist_id%5D=f681d24081' http://7800a.pro.loc/index.php

// Subscribe to List 1 
curl -d 'module=SugarChimp&entryPoint=SugarChimpWebhook&type=subscribe&fired_at=2017-05-09+16%3A58%3A13&data%5Bid%5D=59bdb16c63&data%5Bemail%5D=jon%2Bbobbyg3%40sugarchimp.com&data%5Bemail_type%5D=html&data%5Bip_opt%5D=98.200.13.123&data%5Bweb_id%5D=258213333&data%5Bmerges%5D%5BEMAIL%5D=jon%2Bbobbyg3%40sugarchimp.com&data%5Bmerges%5D%5BFNAME%5D=Bobby&data%5Bmerges%5D%5BLNAME%5D=Gen3&data%5Bmerges%5D%5BMMERGE3%5D=&data%5Bmerges%5D%5BMMERGE4%5D=&data%5Bmerges%5D%5BMMERGE6%5D=&data%5Bmerges%5D%5BMMERGE7%5D=&data%5Bmerges%5D%5BMMERGE5%5D=&data%5Bmerges%5D%5BMMERGE8%5D=&data%5Bmerges%5D%5BMMERGE9%5D=&data%5Bmerges%5D%5BMMERGE10%5D=&data%5Bmerges%5D%5BMMERGE11%5D=&data%5Bmerges%5D%5BMMERGE12%5D=&data%5Bmerges%5D%5BMMERGE13%5D=&data%5Bmerges%5D%5BMMERGE14%5D=&data%5Bmerges%5D%5BMMERGE15%5D=&data%5Bmerges%5D%5BMMERGE16%5D=First+Choice&data%5Bmerges%5D%5BINTERESTS%5D=&data%5Bmerges%5D%5BGROUPINGS%5D%5B0%5D%5Bid%5D=15677&data%5Bmerges%5D%5BGROUPINGS%5D%5B0%5D%5Bunique_id%5D=a11fb03709&data%5Bmerges%5D%5BGROUPINGS%5D%5B0%5D%5Bname%5D=Which+emails+do+you+want+to+receive%3F&data%5Bmerges%5D%5BGROUPINGS%5D%5B0%5D%5Bgroups%5D=&data%5Bmerges%5D%5BGROUPINGS%5D%5B1%5D%5Bid%5D=17153&data%5Bmerges%5D%5BGROUPINGS%5D%5B1%5D%5Bunique_id%5D=4451cc0bdb&data%5Bmerges%5D%5BGROUPINGS%5D%5B1%5D%5Bname%5D=What+are+your+favorite+colors%3F&data%5Bmerges%5D%5BGROUPINGS%5D%5B1%5D%5Bgroups%5D=&data%5Blist_id%5D=f2082ab659' http://7801b.pro.loc/index.php
// Subscribe to List 2
curl -d 'module=SugarChimp&entryPoint=SugarChimpWebhook&type=subscribe&fired_at=2017-05-09+16%3A58%3A40&data%5Bid%5D=59bdb16c63&data%5Bemail%5D=jon%2Bbobbyg3%40sugarchimp.com&data%5Bemail_type%5D=html&data%5Bip_opt%5D=98.200.13.123&data%5Bweb_id%5D=258213337&data%5Bmerges%5D%5BEMAIL%5D=jon%2Bbobbyg3%40sugarchimp.com&data%5Bmerges%5D%5BFNAME%5D=Bobby&data%5Bmerges%5D%5BLNAME%5D=Gen3&data%5Bmerges%5D%5BACCTNAME%5D=&data%5Bmerges%5D%5BSTATUS%5D=&data%5Bmerges%5D%5BCITY%5D=&data%5Bmerges%5D%5BSTATE%5D=&data%5Bmerges%5D%5BZIPCODE%5D=&data%5Bmerges%5D%5BACCTYPE%5D=&data%5Bmerges%5D%5BOPPAMT%5D=&data%5Bmerges%5D%5BMMERGE11%5D=&data%5Bmerges%5D%5BMMERGE12%5D=&data%5Bmerges%5D%5BLIST%5D=&data%5Bmerges%5D%5BINTERESTS%5D=&data%5Bmerges%5D%5BGROUPINGS%5D%5B0%5D%5Bid%5D=16993&data%5Bmerges%5D%5BGROUPINGS%5D%5B0%5D%5Bunique_id%5D=7224df4dea&data%5Bmerges%5D%5BGROUPINGS%5D%5B0%5D%5Bname%5D=Which+Emails+to+Receive&data%5Bmerges%5D%5BGROUPINGS%5D%5B0%5D%5Bgroups%5D=&data%5Bmerges%5D%5BGROUPINGS%5D%5B1%5D%5Bid%5D=17729&data%5Bmerges%5D%5BGROUPINGS%5D%5B1%5D%5Bunique_id%5D=c58badde1f&data%5Bmerges%5D%5BGROUPINGS%5D%5B1%5D%5Bname%5D=Fruit&data%5Bmerges%5D%5BGROUPINGS%5D%5B1%5D%5Bgroups%5D=&data%5Blist_id%5D=f681d24081' http://7801b.pro.loc/index.php
// Subscribe to List 3 
curl -d 'module=SugarChimp&entryPoint=SugarChimpWebhook&type=subscribe&fired_at=2017-05-09+16%3A59%3A11&data%5Bid%5D=59bdb16c63&data%5Bemail%5D=jon%2Bbobbyg3%40sugarchimp.com&data%5Bemail_type%5D=html&data%5Bip_opt%5D=98.200.13.123&data%5Bweb_id%5D=258213341&data%5Bmerges%5D%5BEMAIL%5D=jon%2Bbobbyg3%40sugarchimp.com&data%5Bmerges%5D%5BFNAME%5D=Bobby&data%5Bmerges%5D%5BLNAME%5D=Gen3&data%5Bmerges%5D%5BMMERGE3%5D=&data%5Blist_id%5D=665c8d2799' http://7801b.pro.loc/index.php
// Subscribe to List 4 
curl -d 'module=SugarChimp&entryPoint=SugarChimpWebhook&type=subscribe&fired_at=2017-05-09+16%3A59%3A31&data%5Bid%5D=59bdb16c63&data%5Bemail%5D=jon%2Bbobbyg3%40sugarchimp.com&data%5Bemail_type%5D=html&data%5Bip_opt%5D=98.200.13.123&data%5Bweb_id%5D=258213345&data%5Bmerges%5D%5BEMAIL%5D=jon%2Bbobbyg3%40sugarchimp.com&data%5Bmerges%5D%5BFNAME%5D=Bobby&data%5Bmerges%5D%5BLNAME%5D=Gen3&data%5Bmerges%5D%5BMMERGE3%5D=&data%5Bmerges%5D%5BINTERESTS%5D=&data%5Bmerges%5D%5BGROUPINGS%5D%5B0%5D%5Bid%5D=17133&data%5Bmerges%5D%5BGROUPINGS%5D%5B0%5D%5Bunique_id%5D=69ef084327&data%5Bmerges%5D%5BGROUPINGS%5D%5B0%5D%5Bname%5D=The+Groups&data%5Bmerges%5D%5BGROUPINGS%5D%5B0%5D%5Bgroups%5D=&data%5Blist_id%5D=be3b3f8cbc' http://7801b.pro.loc/index.php


*/

echo "SugarChimp Webhook Receiver is setup properly!";

require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/FieldMap.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Activity.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Queue.php');

// could probably use a class to route the webhook stuff below
// but it's not really necessary at this point
// the webhook implementations are pretty straightforward
class Mailchimp_Webhooks_Receiver {}

// SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::post: '.print_r($_POST,true));
// SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::get: '.print_r($_GET,true));
// SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::request: '.print_r($_REQUEST,true));

$type = $_POST['type'];
$data = $_POST['data'];

SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver:: type: '.print_r($type,true));
SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver:: data: '.print_r($data,true));

// lookup target list by provided mailchimp list id $data['list_id']
if (empty($data['list_id']))
{
    SugarChimp_Helper::log('fatal',"Mailchimp_Webhooks_Receiver cannot process webhook, no list id is provided {$type} ".print_r($data,true));
    return;
}

global $current_user;
$current_user = BeanFactory::getBean('Users');
$current_user->getSystemUser();

$target_list = BeanFactory::newBean('ProspectLists');
$target_list->retrieve_by_string_fields(array('mailchimp_list_name_c' => $data['list_id']));

if (empty($target_list) || empty($target_list->id))
{
    SugarChimp_Helper::log('warning',"Mailchimp_Webhooks_Receiver cannot process webhook because a related list was not found {$type} ".print_r($data,true));
    return;
}

$default_module = $target_list->mailchimp_default_module_c;

if (empty($default_module))
{
    // set to default, default module
    $default_module = SugarChimp_FieldMap::get_default_module();
}
else
{
    // make sure it's in supported modules array, if not set to default, default module
    $supported_modules = SugarChimp_FieldMap::get_supported_modules();
    if (!in_array($default_module,$supported_modules) === true)
    {
        // if it's not, set it to the default
        $default_module = SugarChimp_FieldMap::get_default_module();
    }
}

switch ($type)
{
    case "campaign":
        SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::campaign start');
        // the method fires when a campaign has been successfully sent
        // we need to fire the initial campaign import job
        
        if (empty($data['id']))
        {
            // campaign id is required
            SugarChimp_Helper::log('fatal','Mailchimp_Webhooks_Receiver::campaign id (campaign id) is required, but it was empty');
            break;
        }
        
        if (empty($data['list_id']))
        {
            // list_id is required
            SugarChimp_Helper::log('fatal','Mailchimp_Webhooks_Receiver::list id is required, but it was empty');
            break;
        }

        $mailchimp_list_id = $data['list_id'];
        $mailchimp_campaign_id = $data['id'];

        // If not already there, add List to sugarchimp_mc_list Table
        // also adds to the maintained dropdown menu
        SugarChimp_Helper::add_list_to_table($mailchimp_list_id,false,false);

        // If not already there, add Campaign to sugarchimp_mc_campaign Table
        // also adds to the maintained dropdown menu
        SugarChimp_Helper::add_campaign_to_table($mailchimp_campaign_id);

        // add it to the tracking table
        // the activity data will be downloaded once it's turn is up
        // this is governed by the MailchimpActivityToSugarCRM
        $include_send_activities = SugarChimp_Setting::retrieve('include_send_activities');
        
        // 8/15/2018 issue where Automated Campaigns are not continuing to grab activities after their initial send time. 
        // As they are sent, we need to update the send_dates on the activity tracker table to allow them to continue to be tracked
        // changed last boolean to 'true' which should allow that data to be overwritten on the table

        SugarChimp_Activity::track_campaign_activity($mailchimp_list_id,$mailchimp_campaign_id, date("Y-m-d H:i:s"),false,$include_send_activities,true);
        
        SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::campaign finish');
        break;
        
    case "subscribe":
        SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::subscribe start');
        // this method runs when a person subscribes to a list in mailchimp
        // sugarchimp will try to find existing contacts in CRM or create it based on the default_module
        // it will then update the records with the data provided from mailchimp
        // and lastly it will add the records to the target list

        if (empty($data['list_id']))
        {
            // list_id is required
            SugarChimp_Helper::log('fatal','Mailchimp_Webhooks_Receiver::unsubscribe - list id is required, but it was empty');
            break;
        }
        
        if (empty($data['email']))
        {
            // list_id is required
            SugarChimp_Helper::log('fatal','Mailchimp_Webhooks_Receiver::unsubcsribe - email is required, but it was empty');
            break;
        }
        
        $result = SugarChimp_Setting::remove_from_optout_tracking_table($data['email'],array($data['list_id']));
        
        if (empty($result))
        {
            // could not remove email to optout tracking table
            SugarChimp_Helper::log('warning','Mailchimp_Webhooks_Receiver::subscribe - Could not remove email from optout tracking table. email: '.print_r($data['email'],true).' list_id: '.print_r($data['list_id'],true));
        }

        $subscriber_sync_enabled = SugarChimp_Setting::retrieve('subscriber_sync_enabled');
        if(empty($subscriber_sync_enabled))
        {
            SugarChimp_Helper::log('warning',"Mailchimp_Webhooks_Receiver::subscribe - All MailChimp to Sugar Subscriber Syncing is Disabled. Ignoring Webhook.");
            break;
        }
        
        $webhook_subscribe_enabled = SugarChimp_Setting::retrieve('webhook_subscribe_enabled');
        if(empty($webhook_subscribe_enabled))
        {
            SugarChimp_Helper::log('warning',"Mailchimp_Webhooks_Receiver::subscribe - MailChimp to Sugar 'subscribe' Webhook is disabled. Ignoring Webhook.");
            break;
        }
        
        // if email already exists in Sugar, make sure it is marked as optin and valid
        SugarChimp_Helper::mark_email_valid($data['email']);
        SugarChimp_Helper::mark_email_optin($data['email']);
        
        // update beans with this email address on the list
        $beans_to_update = SugarChimp_Helper::find_beans_by_email($data['email'],array($data['list_id']));
        
        // if no beans on the list
        // add appropriate beans to the list
        // does not update their data, only adds them to the list
        if (empty($beans_to_update))
        {
            SugarChimp_Helper::log('debug',"Mailchimp_Webhooks_Receiver::subscribe - no beans with the email ".print_r($data['email'],true)." on the list.");
            $result = SugarChimp_Helper::add_to_list_by_email($data['email'],$target_list,$default_module);
            $beans_to_update = $result['beans'];
            $data['create_new'] = $result['create_new'];
            

            if(empty($beans_to_update))
            {
                //based on settings, need to create a new default module record
                if($data['create_new'] === true)
                {
                    SugarChimp_Helper::log('debug',"Mailchimp_Webhooks_Receiver::subscribe - no beans added to list. Creating new {$default_module} bean.");
                    $beans_to_update = array();
                    $beans_to_update[]= SugarChimp_Helper::get_empty_bean($default_module);
                }
                else
                {
                    SugarChimp_Helper::log('debug',"Mailchimp_Webhooks_Receiver::subscribe - no appropriate beans to add to list. Exiting.");
                    break;
                }
            }
        }
        
        // need to pass beans to the update_bean function as we have previously
        // if it is a newly created bean, then this function will also add it to the target list
        // all other beans have already been related to the target list
        $updated_beans = SugarChimp_Helper::update_beans_from_webhook($target_list,$beans_to_update,$data);

        SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::subscribe finish');
        break;
        
    case "unsubscribe":
        
        global $db;
        
        SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::unsubscribe start');
        // this method runs when a person unsubscribes from a list in mailchimp
        // sugarchimp will try to find existing contacts in CRM
        // if they exist, it will then remove those beans from the list

        if (empty($data['list_id']))
        {
            // list_id is required
            SugarChimp_Helper::log('fatal','Mailchimp_Webhooks_Receiver::unsubcsribe - list id is required, but it was empty');
            break;
        }
        
        if (empty($data['email']))
        {
            // list_id is required
            SugarChimp_Helper::log('fatal','Mailchimp_Webhooks_Receiver::unsubcsribe - email is required, but it was empty');
            break;
        }

        // updated in 7.8.3m (5/10/17) to only lock on RemoveMailChimpSubscriber jobs
        // if via the API is checked, we should receive the unsubscribe webhook
        // if unsubscribe came from us, then email should be locked
        // otherwise, unsubscribe is legit and needs to be processed
        SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver we have an email: '.print_r($data['email'],true));
        $locked = SugarChimp_Helper::is_email_locked($data['email']);
        SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver locked: '.print_r($locked,true));
            
        // if it's locked, clear the lock and exit
        if ($locked === true)
        {
            SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver the email is locked, clear the lock and quit '.print_r($data['email'],true));
            if (SugarChimp_Helper::unlock_email($data['email']) !== true)
            {
                SugarChimp_Helper::log('fatal','Mailchimp_Webhooks_Receiver the email is locked and could not be unlocked');
            }

            return;
        }

        $subscriber_sync_enabled = SugarChimp_Setting::retrieve('subscriber_sync_enabled');
        if(empty($subscriber_sync_enabled))
        {
            SugarChimp_Helper::log('warning',"Mailchimp_Webhooks_Receiver::unsubscribe - All MailChimp to Sugar Subscriber Syncing is Disabled. Ignoring Webhook.");
            break;
        }

        $webhook_unsubscribe_enabled = SugarChimp_Setting::retrieve('webhook_unsubscribe_enabled');
        if(empty($webhook_unsubscribe_enabled))
        {
            SugarChimp_Helper::log('warning',"Mailchimp_Webhooks_Receiver::unsubscribe - MailChimp to Sugar 'unsubscribe' Webhook is disabled. Ignoring Webhook.");
            break;
        }

        // jon-todo make sure person is on list before marking them as opted out.
        // attempt to lookup crm record with $data['email']
        $beans = SugarChimp_Helper::find_beans_by_email($data['email']);
        
        $optout_email = true;
        if (!empty($data['reason']) && $data['reason']!='manual')
        {
            // flag to later mark the email as invalid instead of opted out
            $optout_email = false;
        }
        
        // if we found beans, remove them from target list
        if (!empty($beans))
        {
            // disable logic hooks for the beans and target list
            SugarChimp_Helper::disable_logic_hooks($beans);
            SugarChimp_Helper::disable_logic_hooks(array($target_list));

            // get all synced lists for beans
            $lists = SugarChimp_Helper::get_lists_for_beans($beans);
            
            // check the globaloptout setting to see what to do
            $globaloptout = SugarChimp_Setting::retrieve('globaloptout');
            
            if (empty($globaloptout))
            {
                // If the email address is only on 1 synced Target List, 
                // then mark it as invalid or opted out. 
                // Otherwise, do not update the email address
                SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::optout global opt out is disabled');
                
                // only opt out email in sugar if email address is synced to exactly 1 list
                if (count($lists)===1)
                {
                    if ($optout_email === true)
                    {
                        SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::optout attempting to optout '.$data['email']);
                        $marked = SugarChimp_Helper::mark_email_optout($data['email']);
                    }
                    else
                    {
                        SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::optout attempting to mark invalid '.$data['email']);
                        $marked = SugarChimp_Helper::mark_email_invalid($data['email']);
                    }
                    
                    if ($marked !== true)
                    {
                        SugarChimp_Helper::log('fatal','Mailchimp_Webhooks_Receiver::optout '.$data['email'].' failed to be opted out. marked: '.print_r($marked,true));
                    }
                    else
                    {
                        SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::optout '.$data['email'].' was opted out.');
                    }
                }
            }
            else if ($globaloptout == 1)
            {
                // global opt out is enabled and set to true
                // This is default Behavior
                SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::optout global opt out is enabled');

                // opt out the email address in Sugar
                if ($optout_email === true)
                {
                    SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::optout attempting to optout '.$data['email']);
                    $marked = SugarChimp_Helper::mark_email_optout($data['email']);
                }
                else
                {
                    SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::optout attempting to mark invalid '.$data['email']);
                    $marked = SugarChimp_Helper::mark_email_invalid($data['email']);
                }

                if ($marked !== true)
                {
                    SugarChimp_Helper::log('fatal','Mailchimp_Webhooks_Receiver::optout '.$data['email'].' failed to be opted out. marked: '.print_r($marked,true));
                }
                else
                {
                    SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::optout '.$data['email'].' was opted out.');
                }
                
                if (count($lists)>1)
                {
                    // remove the original target list from the list, the beans will be removed from it later
                    unset($lists[$target_list->id]);
                    
                    // we want the logic hooks to fire for the beans when they are removed from the list
                    SugarChimp_Helper::enable_logic_hooks($beans);
                    
                    foreach ($lists as $list)
                    {
                        // remove beans from the list
                        SugarChimp_Helper::remove_beans_from_list($list,$beans);
                    }
                    
                    // re-disable the hooks
                    SugarChimp_Helper::disable_logic_hooks($beans);
                }
            }
            else if ($globaloptout == 2)
            {
                // Completely ignore all unsubscribes from MailChimp, but not bounces
                SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::optout global opt out is set to mode 2, not marking emails as opted out.');
                
                // if it's false, it's a hard bounce, want to mark invalid
                if ($optout_email === false)
                {
                    SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::optout - mark as invalid:'.$data['email']);
                    $marked = SugarChimp_Helper::mark_email_invalid($data['email']);
                    
                    if ($marked !== true)
                    {
                        SugarChimp_Helper::log('fatal','Mailchimp_Webhooks_Receiver::optout '.$data['email'].' failed to be marked as invalid.'.print_r($marked,true));
                    }
                    else
                    {
                        SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::optout '.$data['email'].' was opted out.');
                    }
                }                
            }
            
            // remove beans from the list
            SugarChimp_Helper::remove_beans_from_list($target_list,$beans);

            // enable logic hooks for the beans and target list
            SugarChimp_Helper::enable_logic_hooks($beans);
            SugarChimp_Helper::enable_logic_hooks(array($target_list));
        }

        SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::unsubscribe finish');
        break;
        
    case "profile":
        SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::profile start');
        // this method runs when a person updates their profile in mailchimp
        // sugarchimp will try to find existing contacts in CRM that are on this list
        // it will then update the records with the data provided from mailchimp
        
        // ideally if they're updating, the creating new records and attaching them to target lists isn't necessary
        // but this is an extra step by going ahead and correcting the out of sync data by doing this here

        $subscriber_sync_enabled = SugarChimp_Setting::retrieve('subscriber_sync_enabled');
        if(empty($subscriber_sync_enabled))
        {
            SugarChimp_Helper::log('warning',"Mailchimp_Webhooks_Receiver::profile - All MailChimp to Sugar Subscriber Syncing is Disabled. Ignoring Webhook.");
            break;
        }
        
        $webhook_profile_enabled = SugarChimp_Setting::retrieve('webhook_profile_enabled');
        if(empty($webhook_profile_enabled))
        {
            SugarChimp_Helper::log('warning',"Mailchimp_Webhooks_Receiver::profile - MailChimp to Sugar 'profile' Webhook is disabled. Ignoring Webhook.");
            break;
        }

        // attempt to lookup crm record(s) with $data['email'] on this mc list
        // find beans on list only, update them regardless of setting
        $beans_on_list = SugarChimp_Helper::find_beans_by_email($data['email'],array($data['list_id']));

        // if no beans, nothing matches on the sugar side, exit out of update
        if (empty($beans_on_list))
        {
            SugarChimp_Helper::log('debug',"Mailchimp_Webhooks_Receiver::profile - MailChimp to Sugar 'profile' Webhook did not find a matching record. Ignoring Webhook.");
            break;
        }

        $updated_beans = SugarChimp_Helper::update_beans_from_webhook($target_list, $beans_on_list, $data);

        SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::profile finish');
        break;
        
    case "upemail":
        SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::upemail start');
        // this method runs when a person updates their email address in mailchimp
        // sugarchimp will try to find existing contacts in CRM or create it based on the default_module
        // it will then update the records with the data provided from mailchimp
        // and lastly it will add the records to the target list

        $subscriber_sync_enabled = SugarChimp_Setting::retrieve('subscriber_sync_enabled');
        if(empty($subscriber_sync_enabled))
        {
            SugarChimp_Helper::log('warning',"Mailchimp_Webhooks_Receiver::upemail - All MailChimp to Sugar Subscriber Syncing is Disabled. Ignoring Webhook.");
            break;
        }
        
        $webhook_upemail_enabled = SugarChimp_Setting::retrieve('webhook_upemail_enabled');
        if(empty($webhook_upemail_enabled))
        {
            SugarChimp_Helper::log('warning',"Mailchimp_Webhooks_Receiver::upemail - MailChimp to Sugar 'upemail' Webhook is disabled. Ignoring Webhook.");
            break;
        }
                
        if (empty($data['new_email']))
        {
            SugarChimp_Helper::log('fatal','Mailchimp_Webhooks_Receiver::upemail new_email is empty');
            break;
        }

        // make sure we have good data
        if (empty($data['old_email']))
        {
            SugarChimp_Helper::log('fatal','Mailchimp_Webhooks_Receiver::upemail old_email is empty');
            break;
        }

        // find beans with the old email address
        // attempt to lookup crm record(s) with $data['old_email']
        $beans = SugarChimp_Helper::find_beans_by_email($data['old_email']);

        // if no beans, nothing matches on the sugar side, so let it go
        if (empty($beans))
        {
            SugarChimp_Helper::log('warning','Mailchimp_Webhooks_Receiver::upemail it is possible this originated from an email address change in Sugar.');
            SugarChimp_Helper::log('warning','Mailchimp_Webhooks_Receiver::upemail could not find any records with email '.$data['old_email']);
            break;
        }

        // disable logic hooks for the beans
        SugarChimp_Helper::disable_logic_hooks($beans);

        // update the email addresses
        foreach ($beans as $bean)
        {
            SugarChimp_Helper::log('debug',"Mailchimp_Webhooks_Receiver::upemail changing {$bean->module_dir} {$bean->id} from {$bean->email1} to {$data['new_email']}");
            $bean->email1 = $data['new_email'];
            $bean->save();
        }

        // enable logic hooks for the beans
        SugarChimp_Helper::enable_logic_hooks($beans);

        SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::upemail finish');
        break; 
}