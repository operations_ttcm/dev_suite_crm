<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Queue.php');

abstract class SugarChimp_Job_Driver
{
    public $name;
    public $list_id;
    public $jobs_data;
    
    public static function forge($list_id,$jobs_data=array())
    {
        return new static($list_id,$jobs_data);
    }
    
    // thought about serializing the SugarChimp_Job object here, but we'd lose control off
    // simply looking at the database to see what's in the queue
    // we can always grab the data and reinstantiate the SugarChimp_Job object later
    // instruct the action how to be queued
    public function queue()
    {
        if (SugarChimp_Queue::queue($this->name,$this->list_id,$this->jobs_data)!==true)
        {
            // it failed to queue
            SugarChimp_Helper::log('fatal',"A {$this->name} action for mailchimp list {$this->list_id} failed to be queued.");
        }
        
        return true;
    }
    
    // what to do when the job is run
    abstract public function run();
}