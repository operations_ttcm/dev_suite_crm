<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Job/Driver.php');
require_once('modules/SugarChimp/includes/vendor/Mailchimp/Mailchimp.php');

class SugarChimp_Job_QueueMailchimpToSugarSync extends SugarChimp_Job_Driver
{
    function __construct($list_id,$jobs_data=array())
    {
        $this->name = 'QueueMailchimpToSugarSync';
        $this->list_id = $list_id;
        $this->jobs_data = $jobs_data;
    }

    public function run()
    {
        SugarChimp_Helper::log('debug',"Run SugarChimp_Job_QueueMailchimpToSugarSync Start");
        SugarChimp_Helper::log('debug',"Run {$this->name} action for list {$this->list_id} with params: ".print_r($this->jobs_data,true));
        
        SugarChimp_Helper::queue_MailChimpToSugarCRM_job($this->list_id,false,false);
        SugarChimp_Helper::log('debug',"SugarChimp_Job_QueueMailchimpToSugarSync after queue call");

        return true;
    }
}


