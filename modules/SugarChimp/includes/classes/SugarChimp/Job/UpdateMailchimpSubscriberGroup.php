<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Job/UpdateMailchimpSubscriber.php');
require_once('modules/SugarChimp/includes/classes/Mailchimp/API.php');

class SugarChimp_Job_UpdateMailchimpSubscriberGroup extends SugarChimp_Job_UpdateMailchimpSubscriber
{
    function __construct($list_id,$jobs_data=array())
    {
        $this->name = 'UpdateMailchimpSubscriberGroup';
        $this->list_id = $list_id;
        $this->jobs_data = $jobs_data;
    }

    function get_merge_var_data($list_id,$bean)
    {
        // include groups data
        return SugarChimp_FieldMap::get_merge_var_update_array_from_bean($list_id,$bean,true);
    }

    function send_updates_to_mailchimp($list_id,$batch)
    {
        // go through the batch array and properly set the interests
        // array to be sent to mailchimp for interest updates
        foreach ($batch as $id => $person)
        {
            $interests = array();
            if (isset($person['merge_fields']['interests']))
            {
                if (!empty($person['merge_fields']['interests']) && is_array($person['merge_fields']['interests']))
                {
                    $interests = $person['merge_fields']['interests'];
                }
                unset($batch[$id]['merge_fields']['interests']);
            }

            if (!empty($interests) and is_array($interests))
            {
                $batch[$id]['interests'] = $interests;
            }
        }

        return MailChimp_API::batch_update_subscribers($list_id,$batch);
    }
}