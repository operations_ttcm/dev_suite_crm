<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Job/Driver.php');
require_once('modules/SugarChimp/includes/classes/Mailchimp/API.php');

class SugarChimp_Job_RemoveMailchimpSubscriber extends SugarChimp_Job_Driver
{
    function __construct($list_id,$jobs_data=array())
    {
        $this->name = 'RemoveMailchimpSubscriber';
        $this->list_id = $list_id;
        $this->jobs_data = $jobs_data;
    }
    
    // remove mailchimp subscribers from mailchimp lists
    // the method first checks for other sugar persons with the same email address
    // if 
    public function run()
    {
        SugarChimp_Helper::log('debug',"Run SugarChimp_Job_RemoveMailchimpSubscriber Start");
        SugarChimp_Helper::log('debug',"Run {$this->name} action for list {$this->list_id} with params: ".print_r($this->jobs_data,true));

        $admin = BeanFactory::getBean('Administration');
        $admin->retrieveSettings('sugarchimp');
        $apikey = $admin->settings['sugarchimp_apikey'];

        if (empty($apikey))
        {
            SugarChimp_Helper::log('fatal',"Run SugarChimp_Job_RemoveMailchimpSubscriber Mailchimp API Key is empty!");
            return false;
        }

        // array of data to send to mailchimp
        $batch = array();
        
        foreach ($this->jobs_data as $data)
        {
            // example data
            // $data = array(
            //     'id', // id of the job in sugarchimp table
            //     'param1' => // bean module
            //     'param2' => // bean id
            //     'param3' => // old email
            //     'param4' => // reason being removed ("invalid" or "optout")
            //     'param5' => // empty
            // )
            
            if ((empty($data['param1']) || empty($data['param2'])) && $data['param4'] != 'EmailChange')
            {
                // if the data is empty, we can't do anything, go to the next item
                continue;
            }
            
            $module = $data['param1'];
            $id = empty($data['param2']) ? 'SugarChimpBatchRemoval' : $data['param2'];
            $email = $data['param3'];
            $reason = $data['param4'];

            // the person was removed from the list without optout or invalid reasoning
            // so the reason for the removal is probably due to deleted record, record removed from target list or record's email address went from something to nothing
            // we need to check to see if there are any other records on the target list with the email address
            // if so, we want to make sure they stay in MailChimp by not queue the removal job
            if (empty($reason))
            {
                $beans = SugarChimp_Helper::find_beans_by_email($email,array($this->list_id),false);

                // if we found some beans still on the list
                if (!empty($beans))
                {
                    // there is someone still on the list with this email, do not remove the email from MC
                    continue;
                }
            }

            $mailchimp_reason = 'unsubscribed';
            if ($reason == 'invalid')
            {
                $mailchimp_reason = 'cleaned';
            }
            
            SugarChimp_Helper::log('debug',"SugarChimp_Job_RemoveMailchimpSubscriber {$email} is being removed for {$reason} - do not check for other beans with same email, remove them all");
            SugarChimp_Helper::log('debug',"SugarChimp_Job_RemoveMailchimpSubscriber add {$email} to batch unsubscribe array");
            
            // updated in 7.9.0d (7/07/17) to only lock on RemoveMailChimpSubscriber jobs
            // fixing bug from 7.8.3m update
            // if via the API is checked, we should receive the unsubscribe webhook
            $locked = SugarChimp_Helper::lock_email($email);

            $batch []= array(
                'id' => $id,
                'email_address' => html_entity_decode($email,ENT_QUOTES),
                'status' => $mailchimp_reason,
            );
        }

        if (empty($batch))
        {
            // if there are no emails to delete, we can consider this a success
            SugarChimp_Helper::log('debug',"Run SugarChimp_Job_RemoveMailchimpSubscriber no emails were found needing to be removed");
            return true;
        }

        try
        {
            $response = MailChimp_API::batch_remove_subscribers($this->list_id,$batch);
            
            SugarChimp_Helper::log('debug','SugarChimp_Job_RemoveMailchimpSubscriber batchUnsubscribe batch data: '.print_r($batch,true));
            SugarChimp_Helper::log('debug','SugarChimp_Job_RemoveMailchimpSubscriber batchUnsubscribe success: '.print_r($response,true));
        }
        catch (Mailchimp_List_DoesNotExist $e)
        {
            SugarChimp_Helper::log('warning',"Run SugarChimp_Job_RemoveMailchimpSubscriber MailChimp list no longer exists. : ".$e->getMessage());
            SugarChimp_Helper::handle_mailchimp_list_does_not_exist_exception($this->list_id);
            // do not throw an error on this
            return true;
        }
        catch (Exception $e)
        {
            SugarChimp_Helper::log('fatal',"Run SugarChimp_Job_RemoveMailchimpSubscriber Everything went well until we tried to interact with Mailchimp: ".$e->getMessage());
            return false;
        }
        
        SugarChimp_Helper::log('debug',"Run SugarChimp_Job_RemoveMailchimpSubscriber Finish");
        return true;        
    }
}