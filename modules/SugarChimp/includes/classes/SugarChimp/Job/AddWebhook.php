<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Job/Driver.php');
require_once('modules/SugarChimp/includes/vendor/Mailchimp/Mailchimp.php');

class SugarChimp_Job_AddWebhook extends SugarChimp_Job_Driver
{
    function __construct($list_id,$jobs_data=array())
    {
        $this->name = 'AddWebhook';
        $this->list_id = $list_id;
        $this->jobs_data = $jobs_data;
    }

    public function run()
    {
        // check to see if this job is enabled to run
        // if not, process it as complete and let the scheduler carry on
        $job_addwebhook_enabled = SugarChimp_Setting::retrieve('job_addwebhook_enabled');
        if(empty($job_addwebhook_enabled))
        {
            SugarChimp_Helper::log('debug',"SugarChimp_Job_AddWebhook::run - job_addwebhook_enabled is false, not enabled, so skipping adding the webhook");
            return true;
        }

        // Check to see if webhook syncs are disabled temporarily.
        // Useful for dev environments
        // the is a super-legacy way of supporting this
        // I'm pretty sure this was from the Atcore code and we kept it in 
        // the best way to do this is to use the setting check above
        global $sugar_config;
        if (!empty($sugar_config['sugarchimp_disable_webhook_sync']))
        {
            SugarChimp_Helper::log('debug',"SugarChimp_Job_AddWebhook Adding Webhooks are disabled.");
            return true;
        }
        
        SugarChimp_Helper::log('debug',"Run SugarChimp_Job_AddWebhook Start");
        SugarChimp_Helper::log('debug',"Run {$this->name} action for list {$this->list_id} with params: ".print_r($this->jobs_data,true));
        
        $response = SugarChimp_Helper::add_webhooks_to_lists(array($this->list_id));
        
        if (!empty($response['error']) && is_array($response['error']) && count($response['error'])>0)
        {
            $message = "A SugarChimp webhook could not be added to a Mailchimp List {$this->list_id}. This probably occurred because your SugarCRM/SuiteCRM server is not publicly accessible. In order for MailChimp to communicate to SugarCRM/SuiteCRM, your server must be accessible by MailChimp. If you have questions about this, please email SugarChimp Support: support@sugarchimp.com";
            SugarChimp_Helper::email_support($message,$response['error']);
        }
        
        return true;
    }
}

