<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/FieldMap.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Job/Driver.php');
require_once('modules/SugarChimp/includes/classes/Mailchimp/API.php');

class SugarChimp_Job_UpdateMailchimpSubscriberEmailChange extends SugarChimp_Job_Driver
{
    function __construct($list_id,$jobs_data=array())
    {
        $this->name = 'UpdateMailchimpSubscriberEmailChange';
        $this->list_id = $list_id;
        $this->jobs_data = $jobs_data;
    }

    // Processes sugarchimp queue records named UpdateMailchimpSubscriberEmailChange
    // 
    // 
    public function run()
    {
        SugarChimp_Helper::log('debug',"Run SugarChimp_Job_UpdateMailchimpSubscriberEmailChange Start");
        SugarChimp_Helper::log('debug',"Run {$this->name} action for list {$this->list_id} with params: ".print_r($this->jobs_data,true));

        $admin = BeanFactory::getBean('Administration');
        $admin->retrieveSettings('sugarchimp');
        $apikey = $admin->settings['sugarchimp_apikey'];

        if (empty($apikey))
        {
            SugarChimp_Helper::log('fatal',"Run SugarChimp_Job_UpdateMailchimpSubscriberEmailChange Mailchimp API Key is empty!");
            return false;
        }

        // array of data to send to mailchimp
        $batch = array();
        
        foreach ($this->jobs_data as $data)
        {
            // example data
            // $data = array(
            //     'id', // id of the job in sugarchimp table
            //     'param1' => // bean module
            //     'param2' => // bean id
            //     'param3' => // old email
            //     'param4' => // new email
            //     'param5' => // old email's status as opted out or invalid
            // )
            
            if (empty($data['param1']) || empty($data['param2']) || empty($data['param3']))
            {
                // if the data is empty, we can't do anything, go to the next item
                continue;
            }
            
            $module = $data['param1'];
            $id = $data['param2'];
            $old_email = html_entity_decode($data['param3'],ENT_QUOTES);
            $new_email = html_entity_decode($data['param4'],ENT_QUOTES);
            $old_email_status = $data['param5'];

            SugarChimp_Helper::log('debug',"Run SugarChimp_Job_UpdateMailchimpSubscriberEmailChange module {$module}");
            SugarChimp_Helper::log('debug',"Run SugarChimp_Job_UpdateMailchimpSubscriberEmailChange id {$id}");
            SugarChimp_Helper::log('debug',"Run SugarChimp_Job_UpdateMailchimpSubscriberEmailChange old_email {$old_email}");
            SugarChimp_Helper::log('debug',"Run SugarChimp_Job_UpdateMailchimpSubscriberEmailChange new_email {$new_email}");

            // load up the record
            $bean = BeanFactory::getBean($module,$id);
            
            if (empty($bean))
            {
                SugarChimp_Helper::log('warning',"Run SugarChimp_Job_UpdateMailchimpSubscriberEmailChange Could not find record for {$module} {$id} - skipping and going to next item in list");
                continue;
            }
            
            if (empty($bean->email1))
            {
                SugarChimp_Helper::log('warning',"Run SugarChimp_Job_UpdateMailchimpSubscriber no email1 value for {$module} {$id} - skipping and going to next item in list");
                continue;
            }
            
            if ($bean->email_opt_out == "1" || $bean->invalid_email == "1" || SugarChimp_Setting::is_on_optout_tracking_table($new_email,$this->list_id)===true)
            {
                SugarChimp_Helper::log('warning',"Run SugarChimp_Job_UpdateMailchimpSubscriber {$bean->email1} is invalid or opted out - skipping and going to next item in list");
                continue;
            }
            
            // get merge vars for the record
            $merge_vars = SugarChimp_FieldMap::get_merge_var_update_array_from_bean($this->list_id,$bean);

            if (empty($merge_vars))
            {
                SugarChimp_Helper::log('warning',"Run SugarChimp_Job_UpdateMailchimpSubscriberEmailChange No payload data found for {$module} {$id} - skipping and going to next item in list");
                continue;
            }

            // in most cases, we want to update based on the old email address
            $email_id = $old_email;
            
            // check if the list has related targets with the old email address
            $matching_records = SugarChimp_Helper::find_beans_by_email($old_email,$this->list_id,false);
            SugarChimp_Helper::log('debug',"Run SugarChimp_Job_UpdateMailchimpSubscriberEmailChange Records on Target List with Old Email: ".print_r($matching_records,true));

            $new_matching_records = SugarChimp_Helper::find_beans_by_email($new_email,$this->list_id,false);
            SugarChimp_Helper::log('debug',"Run SugarChimp_Job_UpdateMailchimpSubscriberEmailChange Records on Target List with New Email: ".print_r($new_matching_records,true));


            // array of jobs to queue later on
            $jobs = array();

            if (empty($matching_records))
            {
                // There was exactly one record that had the old email address
                // 2.27.17
                // MailChimp has re-introduced the ability to update subscriber's email address
                // Make that call instead of deleting an old subscriber and creating a new one

                SugarChimp_Helper::log('debug',"Run SugarChimp_Job_UpdateMailchimpSubscriberEmailChange there was exactly one synced record in Sugar with the old email address {$old_email}");

                
                // There were no records on the Target List that already had the new email address
                // We can safely update the email address MailChimp record 
                if(count($new_matching_records) <2)
                {
                    SugarChimp_Helper::log('debug',"Run SugarChimp_Job_UpdateMailchimpSubscriberEmailChange there was exactly one synced record in Sugar with the new email address {$new_email}, updating this MailChimp subscriber.");
                    // add to batch data to send to MailChimp
                    // that updates to the new email address
                    // get merge vars for the record
                    
                    // set the key as bean id so there are not duplicate keys
                    $batch[$bean->id] = array(
                        'merge_fields' => $merge_vars,
                        'email_address' => $new_email,
                        'hashable_email' => $old_email,
                        'id' => $bean->id,
                        'status' => 'subscribed',
                    );
                }
                // 4/17/18
                // The email address change created a duplicate email address in SugarCRM
                // This will cause the old email address to remain subscribed in MailChimp
                // while not updating the subscriber's data. 
                //
                // We need to send an update job for the new email address
                // and a remove subscriber job for the old email address
                // Issue: We cannot queue a remove subscriber job with just email address
                // currently need a bean to have the email address as primary in order to make it work
                // 
                // Resolutions:
                // (a) Change the way we do remove subscriber jobs to potentially pass email address instead 
                else
                {
                    SugarChimp_Helper::log('debug',"Run SugarChimp_Job_UpdateMailchimpSubscriberEmailChange there was already a subscriber with this email address on the list. Queue'ing removal job for the old address, and update job for the new address.");
                    
                    $job_type = 'UpdateMailchimpSubscriber';
                    $group_fields = SugarChimp_Field_Group::get_group_fields($this->list_id,array($module));
                    if (!empty($group_fields))
                    {
                        $job_type = 'UpdateMailchimpSubscriberGroup';
                    }

                    $jobs []= array(
                        'name' => $job_type,
                        'params' => array(
                            'param1' => $module, // bean module
                            'param2' => $bean->id, // bean id
                            'param3' => '',
                            'param4' => '',
                            'param5' => '',
                        ),
                    );

                    //remove old subscriber info from mc
                    $jobs []= array(
                        'name' => 'RemoveMailchimpSubscriber',
                        'params' => array(
                            'param1' => '', // bean module
                            'param2' => '', // bean id
                            'param3' => $old_email,
                            'param4' => 'EmailChange', // reason being removed
                            'param5' => '',
                        ),
                    );
                }
            }
            // 5/19/17 - added logic where if old address was invalid
            // we need to send removal job for that address
            else
            {
                

                // Old email is still valid
                // update old email's mc subscriber and
                // create new email's mc subscriber
                if($old_email_status == '')
                {
                    // add the record that changed their email address to the $matching_records array
                    $matching_records[$id] = $module;
                    // requeue sugar to MC sync for records with old and new email
                    foreach ($matching_records as $record_id => $record_module)
                    {
                        
                        $job_type = 'UpdateMailchimpSubscriber';
                        $group_fields = SugarChimp_Field_Group::get_group_fields($this->list_id,array($record_module));
                        if (!empty($group_fields))
                        {
                            $job_type = 'UpdateMailchimpSubscriberGroup';
                        }

                        $jobs []= array(
                            'name' => $job_type,
                            'params' => array(
                                'param1' => $record_module, // bean module
                                'param2' => $record_id, // bean id
                                'param3' => '',
                                'param4' => '',
                                'param5' => '',
                            ),
                        );
                    }
                    
                }
                // Old email was invalid or opted out
                // send removal job for old email
                // create new subscriber for new email address
                else{
                    //     'id', // id of the job in sugarchimp table
                    //     'param1' => // bean module
                    //     'param2' => // bean id
                    //     'param3' => // old email
                    //     'param4' => // reason being removed ("invalid" or "optout")
                    //     'param5' => // old_email_status
                    
                    // create subscriber for new email address
                    $job_type = 'UpdateMailchimpSubscriber';
                    $group_fields = SugarChimp_Field_Group::get_group_fields($this->list_id,array($module));
                    if (!empty($group_fields))
                    {
                        $job_type = 'UpdateMailchimpSubscriberGroup';
                    }

                    $jobs []= array(
                        'name' => $job_type,
                        'params' => array(
                            'param1' => $module, // bean module
                            'param2' => $id, // bean id
                            'param3' => '',
                            'param4' => '',
                            'param5' => '',
                        ),
                    );
                    foreach ($matching_records as $record_id => $record_module)
                    {
                        //remove old subscriber info from mc
                        $jobs []= array(
                            'name' => 'RemoveMailchimpSubscriber',
                            'params' => array(
                                'param1' => $record_module, // bean module
                                'param2' => $record_id, // bean id
                                'param3' => $old_email,
                                'param4' => $old_email_status, // reason being removed
                                'param5' => '',
                            ),
                        );
                        break;
                    }
                }
            }

            if (!empty($jobs))
            {
                SugarChimp_Helper::log('debug',"Run SugarChimp_Job_UpdateMailchimpSubscriberEmailChange there are jobs to queue: ".print_r($jobs,true));
                SugarChimp_FieldMap::queue_jobs($jobs,array($this->list_id));
            }
        }

        

        // todo: when using the MC v3 api without the php client 
        //       no exceptions will be thrown, so it's going to return true everytime here
        try
        {
            $response = MailChimp_API::batch_update_subscribers($this->list_id,$batch);

            SugarChimp_Helper::log('debug','SugarChimp_Job_UpdateMailchimpSubscriber batchSubscribe batch data: '.print_r($batch,true));
            SugarChimp_Helper::log('debug','SugarChimp_Job_UpdateMailchimpSubscriber batchSubscribe mailchimp_list_id: '.print_r($this->list_id,true));
            SugarChimp_Helper::log('debug','SugarChimp_Job_UpdateMailchimpSubscriber batchSubscribe success: '.print_r($response,true));
        }
        catch (Mailchimp_List_DoesNotExist $e)
        {
            SugarChimp_Helper::log('warning',"Run SugarChimp_Job_UpdateMailchimpSubscriber MailChimp list no longer exists. : ".$e->getMessage());
            SugarChimp_Helper::handle_mailchimp_list_does_not_exist_exception($this->list_id);
            // do not throw an error on this
            return true;
        }
        catch (Exception $e)
        {
            SugarChimp_Helper::log('fatal',"Run SugarChimp_Job_UpdateMailchimpSubscriber Everything went well until we tried to interact with Mailchimp: ".$e->getMessage());
            return false;
        }

        
        SugarChimp_Helper::log('debug',"Run SugarChimp_Job_UpdateMailchimpSubscriberEmailChange Finish");
        return true;
    }
}