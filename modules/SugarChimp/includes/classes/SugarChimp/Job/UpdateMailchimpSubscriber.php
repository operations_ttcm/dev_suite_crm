<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/FieldMap.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Job/Driver.php');
require_once('modules/SugarChimp/includes/classes/Mailchimp/API.php');

class SugarChimp_Job_UpdateMailchimpSubscriber extends SugarChimp_Job_Driver
{
    function __construct($list_id,$jobs_data=array())
    {
        $this->name = 'UpdateMailchimpSubscriber';
        $this->list_id = $list_id;
        $this->jobs_data = $jobs_data;
    }

    public function run()
    {
        SugarChimp_Helper::log('debug',"Run SugarChimp_Job_UpdateMailchimpSubscriber Start");
        SugarChimp_Helper::log('debug',"Run {$this->name} action for list {$this->list_id} with params: ".print_r($this->jobs_data,true));

        // array of data to send to mailchimp
        $batch = array();
        
        foreach ($this->jobs_data as $data)
        {
            // example data
            // $data = array(
            //     'id', // id of the job in sugarchimp table
            //     'param1' => // bean module
            //     'param2' => // bean id
            //     'param3' => // empty
            //     'param4' => // empty
            //     'param5' => // empty
            // )
            
            if (empty($data['param1']) || empty($data['param2']))
            {
                // if the data is empty, we can't do anything, go to the next item
                continue;
            }
            
            $module = $data['param1'];
            $id = $data['param2'];
            
            SugarChimp_Helper::log('debug',"Run SugarChimp_Job_UpdateMailchimpSubscriber for {$data['param1']}");
            
            // load up the record
            $bean = BeanFactory::getBean($module,$id);

            

            
            if (empty($bean->email1))
            {
                SugarChimp_Helper::log('warning',"Run SugarChimp_Job_UpdateMailchimpSubscriber no email1 value for {$module} {$id} - skipping and going to next item in list");
                continue;
            }
            
            if ($bean->email_opt_out == "1" || $bean->invalid_email == "1" || SugarChimp_Setting::is_on_optout_tracking_table($bean->email1,$this->list_id)===true)
            {
                SugarChimp_Helper::log('warning',"Run SugarChimp_Job_UpdateMailchimpSubscriber {$bean->email1} is invalid or opted out - skipping and going to next item in list");
                continue;
            }
            
            if (empty($bean))
            {
                SugarChimp_Helper::log('warning',"Run SugarChimp_Job_UpdateMailchimpSubscriber Could not find record for {$module} {$id} - skipping and going to next item in list");
                continue;
            }
            
            // get merge vars for the record
            $merge_vars = $this->get_merge_var_data($this->list_id,$bean);

            if (empty($merge_vars))
            {
                SugarChimp_Helper::log('warning',"Run SugarChimp_Job_UpdateMailchimpSubscriber No payload data found for {$module} {$id} - skipping and going to next item in list");
                continue;
            }

            // add email address to the lock table
            // if someone has "via the API" checked on their webhooks it will fire a profile update
            // for each update we send to mailchimp, to ignore these, we are flagging them into the sugarchimp_lock table
            // if an email address is locked, it will ignore the profile update webhook
            // $locked = SugarChimp_Helper::lock_email($bean->email1);

            // could not lock email
            // if ($locked !== true)
            // {
            //     SugarChimp_Helper::log('fatal','Run SugarChimp_Job_UpdateMailchimpSubscriber: could not lock the email '.print_r($bean->email1,true));
            // }

            // set the key as bean id so there are not duplicate keys
            $batch[$bean->id] = array(
                'merge_fields' => $merge_vars,
                'email_address' => html_entity_decode($bean->email1,ENT_QUOTES),
                'id' => $bean->id,
                'status' => 'subscribed',
            );
        }

        // todo: when using the MC v3 api without the php client 
        //       no exceptions will be thrown, so it's going to return true everytime here
        try
        {
            $response = $this->send_updates_to_mailchimp($this->list_id,$batch);

            SugarChimp_Helper::log('debug','SugarChimp_Job_UpdateMailchimpSubscriber batchSubscribe batch data: '.print_r($batch,true));
            SugarChimp_Helper::log('debug','SugarChimp_Job_UpdateMailchimpSubscriber batchSubscribe mailchimp_list_id: '.print_r($this->list_id,true));
            SugarChimp_Helper::log('debug','SugarChimp_Job_UpdateMailchimpSubscriber batchSubscribe success: '.print_r($response,true));
        }
        catch (Mailchimp_List_DoesNotExist $e)
        {
            SugarChimp_Helper::log('warning',"Run SugarChimp_Job_UpdateMailchimpSubscriber MailChimp list no longer exists. : ".$e->getMessage());
            SugarChimp_Helper::handle_mailchimp_list_does_not_exist_exception($this->list_id);
            // do not throw an error on this
            return true;
        }
        catch (Exception $e)
        {
            SugarChimp_Helper::log('fatal',"Run SugarChimp_Job_UpdateMailchimpSubscriber Everything went well until we tried to interact with Mailchimp: ".$e->getMessage());
            return false;
        }
        
        SugarChimp_Helper::log('debug',"Run SugarChimp_Job_UpdateMailchimpSubscriber Finish");
        return true;
        
        // optional todo: check to see if there are other records with the email address attached to the list
        //                if there are, we could sync the data across the records
        //                for example, if the title field is synced, contact A has a title and contact B does not
        //                the sync would fill in the title and send that data as well instead of setting it to blank
        //                see the RemoveMailchimpSubscriber job to see how to look up the similar beans attached to list
    }

    function get_merge_var_data($list_id,$bean)
    {
        return SugarChimp_FieldMap::get_merge_var_update_array_from_bean($list_id,$bean);
    }

    function send_updates_to_mailchimp($list_id,$batch)
    {
        // send the batch subscribe/update, no double optin, yes to updates, do not overwrite groups
        // $double_optin = false;
        // $update_existing = true;
        // $replace_groups = false;

        // make the call
        return MailChimp_API::batch_update_subscribers($list_id,$batch);
    }
}