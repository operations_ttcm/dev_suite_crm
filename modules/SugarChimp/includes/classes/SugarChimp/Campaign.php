<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');

class SugarChimp_Campaign
{
    
    // process the response from a GET reports/{$campaign_id} call (MailChimp_API::get_report($id))
    // we need to calculate some numbers, format some values 
    // and put the data in the right place to be used in a template
    public static function process_stats($campaign,$include_archive_url=true)
    {
        if (empty($campaign) || !is_array($campaign))
        {
            SugarChimp_Helper::log('warning','SugarChimp_Campaign::pricess_stats result->data is empty or is not an array');
            return $result;
        }
        
        // archive_url - the link to to website that shows what the email looked like
        // if this parameter is set to true it will make the GET campaigns/{id} call to get that url
        // it is not included in the report call that this function relies on
        if ($include_archive_url === true)
        {
            $campaign_details = MailChimp_API::get_campaign($campaign['id']);
            if (!empty($campaign_details['archive_url']))
            {
                $campaign['archive_url'] = $campaign_details['archive_url'];
            }
        }

        // calculate open rate (maybe it breaks down a/b test?)
        // calculate open rate (list total)
        $open_rate = 'n/a';
        $open_rate_formatted = 'n/a';
        if (isset($campaign['opens']['open_rate']))
        {
            $open_rate = $campaign['opens']['open_rate'];
            $open_rate_formatted = self::format_percentage($open_rate);
        }
        $campaign['opens']['open_rate'] = $open_rate;
        $campaign['opens']['open_rate_formatted'] = $open_rate_formatted;

        // format send time
        $send_time_formatted = 'n/a';
        if (!empty($campaign['send_time']))
        {
            $send_time_formatted = self::format_timestamp($campaign['send_time']);
        }
        $campaign['send_time_formatted'] = $send_time_formatted;

        // format last open time
        $last_open_formatted = 'n/a';
        if (!empty($campaign['opens']['last_open']))
        {
            $last_open_formatted = self::format_timestamp($campaign['opens']['last_open']);
        }
        $campaign['opens']['last_open_formatted'] = $last_open_formatted;

        // format last click time
        $last_click_formatted = 'n/a';
        if (!empty($campaign['clicks']['last_click']))
        {
            $last_click_formatted = self::format_timestamp($campaign['clicks']['last_click']);
        }
        $campaign['clicks']['last_click_formatted'] = $last_click_formatted;

        // format industry avg open rate
        $industry_open_rate_formatted = 'n/a';
        if (isset($campaign['industry_stats']['open_rate']))
        {
            $industry_open_rate_formatted = self::format_percentage($campaign['industry_stats']['open_rate']);
        }
        $campaign['industry_stats']['open_rate_formatted'] = $industry_open_rate_formatted;

        // calculate click rate (maybe it breaks down a/b test?)
        $click_rate = 'n/a';
        $click_rate_formatted = 'n/a';
        if (isset($campaign['clicks']['click_rate']))
        {
            $click_rate_formatted = self::format_percentage($campaign['clicks']['click_rate']);
        }
        $campaign['clicks']['click_rate_formatted'] = $click_rate_formatted;
        
        // format industry avg click rate
        $industry_click_rate_formatted = 'n/a';
        if (isset($campaign['industry_stats']['click_rate']))
        {
            $industry_click_rate_formatted = self::format_percentage($campaign['industry_stats']['click_rate']);
        }
        $campaign['industry_stats']['click_rate_formatted'] = $industry_click_rate_formatted;

        // calculate total bounced (soft + hard bounce)
        $total_bounces = 'n/a';
        if (isset($campaign['bounces']['hard_bounces']) && isset($campaign['bounces']['soft_bounces']))
        {
            $total_bounces = $campaign['bounces']['hard_bounces'] + $campaign['bounces']['soft_bounces'];
        }
        $campaign['bounces']['total_bounces'] = $total_bounces;

        // calculate and format delivery percentage
        $successful_deliveries = 'n/a';
        $successful_delivery_rate = 'n/a';
        $successful_delivery_rate_formatted = 'n/a';
        if (isset($campaign['emails_sent']) && isset($campaign['bounces']['hard_bounces']) && isset($campaign['bounces']['soft_bounces']))
        {
            $successful_deliveries = $campaign['emails_sent'] - $campaign['bounces']['hard_bounces'] - $campaign['bounces']['soft_bounces'];
            $successful_delivery_rate = $successful_deliveries / $campaign['emails_sent'];
            $successful_delivery_rate_formatted = self::format_percentage($successful_delivery_rate);
        }
        $campaign['successful_deliveries'] = $successful_deliveries;
        $campaign['successful_delivery_rate'] = $successful_delivery_rate;
        $campaign['successful_delivery_rate_formatted'] = $successful_delivery_rate_formatted;
        
        // clicks per unique opens
        $clicks_per_opens = 'n/a';
        $clicks_per_opens_formatted = 'n/a';
        if (isset($campaign['clicks']['unique_subscriber_clicks']) && isset($campaign['opens']['unique_opens']))
        {
            $clicks_per_opens = $campaign['clicks']['unique_subscriber_clicks'] / $campaign['opens']['unique_opens'];
            $clicks_per_opens_formatted = self::format_percentage($clicks_per_opens);
        }
        $campaign['clicks']['clicks_per_opens'] = $clicks_per_opens;
        $campaign['clicks']['clicks_per_opens_formatted'] = $clicks_per_opens_formatted;
    
        return $campaign;
    }
    
    public static function format_percentage($value)
    {
        return round((float)$value * 100,1) . '%';
    }

    public static function format_timestamp($value)
    {
        global $current_user;
        $td = new TimeDate();

        // mailchimp started using a new time/date format. ex. 2017-01-13T13:46:11+00:00
        // going to replace the T with a space character and remove the plus sign and beyond.
        $value = str_replace('T',' ',$value);
        $value = strstr($value,'+',TRUE);

        return $td->to_display_date_time($value, true, true, $current_user);
    }
}