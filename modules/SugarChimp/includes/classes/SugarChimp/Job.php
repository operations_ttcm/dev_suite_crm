<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');

require_once('modules/SugarChimp/includes/classes/SugarChimp/Job/RemoveMailchimpSubscriber.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Job/UpdateMailchimpSubscriber.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Job/UpdateMailchimpSubscriberGroup.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Job/UpdateMailchimpSubscriberEmailChange.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Job/AddWebhook.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Job/QueueMailchimpToSugarSync.php');

class SugarChimp_Job
{
    public static function forge($driver,$list_id,$jobs_data=array())
    {
        if (empty($driver) || empty($list_id))
        {
            return false;
        }

        $driver = "SugarChimp_Job_" . $driver;
        SugarChimp_Helper::log('debug',"Instantiate SugarChimp_Job Driver {$driver}");
        return $driver::forge($list_id,$jobs_data);
    }
}
