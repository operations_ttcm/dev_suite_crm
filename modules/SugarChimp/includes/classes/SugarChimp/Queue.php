<?php





require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/FieldMap.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');

class SugarChimp_Queue
{
    public static function queue($job_name,$list_id,$job_data=array())
    {
        if (empty($job_name) || empty($list_id))
        {
            return false;
        }

        foreach ($job_data as $job)
        {
            // set the param fields
            $param1 = (empty($job['param1'])) ? null : $job['param1'];
            $param2 = (empty($job['param2'])) ? null : $job['param2'];
            $param3 = (empty($job['param3'])) ? null : $job['param3'];
            $param4 = (empty($job['param4'])) ? null : $job['param4'];
            $param5 = (empty($job['param5'])) ? null : $job['param5'];

            $sc = BeanFactory::newBean('SugarChimp');
            $sc->name = $job_name;
            $sc->mailchimp_list_id = $list_id;
            $sc->param1 = $param1;
            $sc->param2 = $param2;
            $sc->param3 = $param3;
            $sc->param4 = $param4;
            $sc->param5 = $param5;
            $sc->save();
        }

        return true;
    }

    public static function queue_mailchimp_subscribers($datas)
    {
        if (empty($datas))
        {
            return false;
        }
    
        // This method is called anonymously as needed to import all of the subscriber data that is coming in from mailchimp
        // the global variables are used to keep consistent data across these processes
        // all of the "if (!isset())" checks below initialize the data the first time the method is run so that it doesn't run multiple times
        // this is for optimization reasons so that the MC to Sugar process is as fast as possible

        global $db, $queue_mailchimp_list_id, $queue_mailchimp_count, $queue_mailchimp_columns, $queue_mailchimp_table_name;
        global $modules, $mapping, $merge_vars, $merge_var_map, $group_fields, $group_var_map, $raw_mailchimp_groups, $mailchimp_groups;

        SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers table_name: '. print_r($queue_mailchimp_table_name,true));

        $batch = array();

        // The "Email Address" label can be customized by the user, if empty emails are coming into the system, that's why
        // The Ordering of the fields can also be changed by the user, so the email address field cannot be guaranteed to be the first column
        // Pretty much have to just tell the user to make sure it is "Email Address", otherwise we'd have to work it into the field mapping for each list
        //["Email Address","First Name","Last Name","MEMBER_RATING","OPTIN_TIME","OPTIN_IP","CONFIRM_TIME","CONFIRM_IP","LATITUDE","LONGITUDE","GMTOFF","DSTOFF","TIMEZONE","CC","REGION","LAST_CHANGED","LEID","EUID"]
        //["hello@world.com","Test","Person",2,"",null,"2014-03-02 16:52:36","71.63.149.98",null,null,null,null,null,null,null,"2014-03-02 16:52:36","172359185","8c7d31797d"]

        if (!isset($modules))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers modules is not initialized, initializing');
            $modules = SugarChimp_FieldMap::get_supported_modules();
        }
        SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers modules: '. print_r($modules,true));

        if (!isset($mapping))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers mapping is not initialized, initializing');
            $mapping = array();
            foreach ($modules as $module)
            {
                $mapping[$module] = SugarChimp_FieldMap::get_field_mappings($queue_mailchimp_list_id,$module);
            }
        }
        SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers mapping: '. print_r($mapping,true));
            
        // get mailchimp merge fields
        if (!isset($merge_vars))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers merge_vars is not initialized, initializing');
            $merge_vars = SugarChimp_FieldMap::get_mc_list_merge_fields($queue_mailchimp_list_id);
        }
        SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers merge_vars: '. print_r($merge_vars,true));
        
        if (!isset($merge_var_map))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers merge_var_map is not initialized, initializing');
            $merge_var_map = array();
            foreach ($merge_vars as $var)
            {
                $merge_var_map[$var['tag']] = $var['name'];
            }


            // this field isn't included in the get_mc_list_merge_fields call
            // so need to manually add it here
            $merge_var_map['MEMBER_RATING'] = 'MEMBER_RATING';

        }
        SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers merge_var_map: '. print_r($merge_var_map,true));

        // if list has synced groups
        if (!isset($group_fields))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers group_fields is not initialized, initializing');
            $group_fields = SugarChimp_Field_Group::get_group_fields($queue_mailchimp_list_id,$modules);
        }
        SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers group_fields: '. print_r($group_fields,true));

        if (!isset($group_var_map))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers group_var_map is not initialized, initializing');
            $group_var_map = array();
            if (!empty($group_fields))
            {
                // get the groups data
                if (!isset($raw_mailchimp_groups))
                {
                    SugarChimp_Helper::log('debug','SugarChimp_Queue::raw_mailchimp_groups group_fields is not initialized, initializing');
                    $raw_mailchimp_groups = Mailchimp_API::get_interest_categories($queue_mailchimp_list_id);
                    $raw_mailchimp_groups = $raw_mailchimp_groups['categories'];
                }
                SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers raw_mailchimp_groups: '. print_r($raw_mailchimp_groups,true));

                if (!isset($mailchimp_groups))
                {
                    // basically just setting up the array to use better keys for easier lookup
                    SugarChimp_Helper::log('debug','SugarChimp_Queue::raw_mailchimp_groups mailchimp_groups is not initialized, initializing');
                    $mailchimp_groups = array();
                    foreach ($raw_mailchimp_groups as $rmg)
                    {
                        $mailchimp_groups[$rmg['id']] = $rmg;
                    }

                    foreach ($modules as $module)
                    {
                        foreach ($mapping[$module]['fields'] as $key => $gf)
                        {
                            if ($gf['type'] === 'Group')
                            {
                                $mapping[$module]['fields'][$key]['name'] = $mailchimp_groups[$gf['mapping']['mailchimp_name']]['title'];
                            }
                        }
                    }
                }
                SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers mailchimp_groups: '. print_r($mailchimp_groups,true));
            }
        }
        SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers group_var_map: '. print_r($group_var_map,true));

        foreach ($datas as $data)
        {
            SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers line data: '.print_r($data,true));
            $data = json_decode($data);

            // this takes the first line that comes in and stuffs it into a global variable
            // it represents the names of the merge columns that come from mailchimp
            if (empty($queue_mailchimp_count) || $queue_mailchimp_count === 0)
            {
                $queue_mailchimp_count = 1;
                $queue_mailchimp_columns = $data;

                //swap key/values so that we can lookup the array index quickly
                $queue_mailchimp_columns = array_flip($queue_mailchimp_columns);
                SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers: queue_mailchimp_columns: '.print_r($queue_mailchimp_columns,true));

                // check to see if the list has synced group fields
                // we need to do extra work to get the groups right
                if (!empty($group_fields))
                {
                    foreach ($modules as $module)
                    {
                        if (!empty($mapping[$module]['fields']) && is_array($mapping[$module]['fields']))
                        {
                            foreach ($mapping[$module]['fields'] as $group)
                            {
                                if ($group['type'] === 'Group')
                                {
                                    foreach ($queue_mailchimp_columns as $name => $offset)
                                    {
                                        if ($group['name'] === $name)
                                        {
                                            $group_var_map[$group['mapping']['mailchimp_name']] = $offset;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers: group var map for module '.$module.': '.print_r($group_var_map,true));
                }
                continue;
            }

            /**
            todo: need to pull 'Email Address' from a setting value
            if the email address field isn't named 'Email Address' in MailChimp
            there's no reliable way to find that column without extra work from the user
            would be a nice settings thing
            */
            if (empty($data[$queue_mailchimp_columns['Email Address']]))
            {
                // found an empty entry, ignore and continue
                continue;
            }

            //prep the data
            // the keys must correspond to what's defined in field mapper
            $subscriber_data = array(
                'list_id' => $queue_mailchimp_list_id,
                'email' => $data[$queue_mailchimp_columns['Email Address']],
                'mapping' => array()
            );

            foreach ($modules as $module)
            {
                /**
                If you change anything in this for loop
                you should also consider making the changes to 
                modules/SugarChimp/includes/classes/SugarChimp/Scheduler/UpdateSugarPersonFromMailchimp.php
                prepare_data_for_update() method
                */
                // setup the new email field
                $subscriber_data['mapping'][$module]['NEW-EMAIL'] = $subscriber_data['email'];

                // if there are groups, add the grouping array
                if (!empty($group_fields))
                {
                    $subscriber_data['mapping'][$module]['GROUPINGS'] = array();
                }

                // go through each of the mapped fields for each module and setup the data array 
                // this will match the mailchimp/sugar field with the data provided from MC to update sugar
                foreach ($mapping[$module]['fields'] as $field)
                {
                    // skip email
                    if ($field['mapping']['mailchimp_name']=='EMAIL' || $field['mapping']['mailchimp_name']=='NEW-EMAIL') continue;

                    switch ($field['type'])
                    {
                        case "Group":

                            $subscriber_data['mapping'][$module]['GROUPINGS'] []= array(
                                'id' => $field['mapping']['mailchimp_name'],
                                'name' => $field['name'],
                                'groups' => $data[$group_var_map[$field['mapping']['mailchimp_name']]],
                            );
                            break;

                        case "Basic":
                        default:

                            $subscriber_data['mapping'][$module][$field['mapping']['mailchimp_name']] = $data[$queue_mailchimp_columns[$merge_var_map[$field['mapping']['mailchimp_name']]]];
                            break;
                    }
                }
            }
            SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers: subscriber_data: '.print_r($subscriber_data,true));

            // add the subscriber data to the batch array
            // this array is used later to construct a batch insert query 
            $batch []= base64_encode(serialize($subscriber_data));
        }

        if (empty($batch) or !is_array($batch))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Queue::queue_mailchimp_subscribers: There was no batch data to import to Sugar from MailChimp for list '.print_r($queue_mailchimp_list_id,true));
            return true;
        }

        // if we have good batch data, create the query and run it!
        $sql = "INSERT INTO ".$db->quote($queue_mailchimp_table_name)." (id,mailchimp_list_id,data,date_entered,date_modified,deleted) VALUES ";
        $i = 0;
        $total = count($batch);
        foreach ($batch as $b)
        {
            $i++;
            $sql .= "(".$db->getGuidSQL().",'".$db->quote($queue_mailchimp_list_id)."','".$db->quote($b)."',".$db->now().",".$db->now().",0)";
            if ($i !== $total)
            {
                $sql .= ",";
            }
        }
        SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers: batch insert sql: '.$sql);
        $result = $db->query($sql);
        SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers: batch insert sql result: '.print_r($result,true));
        
        return true;
    }

    /**
    * If this isn't fast enough we could look at batching the insert.
    * Should work the same for both MySQL and MSSQL
    */
    public static function queue_mailchimp_activity($data)
    {
        if (empty($data))
        {
            return false;
        }

        global $queue_mailchimp_campaign_id, $queue_mailchimp_list_id, $queue_include_empty;

        // we don't need to unpack it here, just showing what it looks like
        // sometimes there are activities sometimes there are no activities
        //
        // {"ubl@generations.com":[{"action":"click","timestamp":"2014-04-10 01:08:23","url":"https:\/\/www.sugaroutfitters.com","ip":"75.168.42.98"},{"action":"open","timestamp":"2014-04-10 01:08:23","url":null,"ip":"75.168.42.98"}]}
        // $data = [ubl@generations.com] => Array(
        //     [0] => stdClass Object
        //         (
        //             [action] => click
        //             [timestamp] => 2014-04-10 01:08:23
        //             [url] => https://www.sugaroutfitters.com
        //             [ip] => 75.168.42.98
        //         )
        //  
        //     [1] => stdClass Object
        //         (
        //             [action] => open
        //             [timestamp] => 2014-04-10 01:08:23
        //             [url] => 
        //             [ip] => 75.168.42.98
        //         )
        // )
        //
        // {"tony.frebault@bluenote-systems.com":[]}
        // $data = [tony.frebault@bluenote-systems.com] => Array()

        SugarChimp_Helper::log('debug','queue_mailchimp_activity: data: '.print_r($data,true));

        $sc = BeanFactory::newBean('SugarChimpMCActivity');
        $sc->mailchimp_campaign_id = $queue_mailchimp_campaign_id;
        $sc->mailchimp_list_id = $queue_mailchimp_list_id;
        $sc->include_empty = empty($queue_include_empty) ? false : true;
        $sc->data = base64_encode(serialize($data));
        $sc->save();

        return true;
    }

    /**
    * If this isn't fast enough we could look at batching the insert.
    * Should work the same for both MySQL and MSSQL
    */
    public static function queue_mailchimp_cleaned_emails($datas)
    {
        if (empty($datas))
        {
            return false;
        }

        global $queue_mailchimp_list_id, $queue_mailchimp_count;

        foreach ($datas as $data)
        {
            SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_subscribers line data: '.print_r($data,true));
            $data = json_decode($data);

            // set the number of entries, ignore the first line
            // the first line contains the header
            if (empty($queue_mailchimp_count) || $queue_mailchimp_count === 0)
            {
                $queue_mailchimp_count = 1;
                continue;
            }

            $cleaned_email = BeanFactory::newBean('SugarChimpMCCleanedEmail');
            $cleaned_email->mailchimp_list_id = $queue_mailchimp_list_id;
            $cleaned_email->email = $data[0]; // first column is the email address
            $cleaned_email->save();
        }

        return true;
    }

    // takes a mailchimp list id and array of emails and puts them in the sugarchimp_mc_people table
    // that table will be processed by the sugarchimp scheduler, it will update their sugar records from mailchimp data
    public static function queue_mailchimp_to_sugar_person_update($list_id,$emails)
    {
        $subscriber_sync_enabled = SugarChimp_Setting::retrieve('subscriber_sync_enabled');
        if(empty($subscriber_sync_enabled))
        {
            SugarChimp_Helper::log('warning',"SugarChimp_Queue::queue_mailchimp_to_sugar_person_update - Subscriber Syncing is Disabled. Not Queueing UpdateSugarPersonFromMailchimp Job.");
            return true;
        }

        if (empty($list_id) or empty($emails) or !is_array($emails))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Queue::queue_mailchimp_to_sugar_person_update list_id or email is empty');
            SugarChimp_Helper::log('fatal','SugarChimp_Queue::queue_mailchimp_to_sugar_person_update list_id: '.print_r($list_id, true));
            SugarChimp_Helper::log('fatal','SugarChimp_Queue::queue_mailchimp_to_sugar_person_update email: '.print_r($emails, true));
            return false;
        }

        SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_to_sugar_person_update queueing mc to sugar update for list '.print_r($list_id,true).' and emails: '.print_r($emails,true));

        global $db;

        $total = count($emails);

        // if we need to batch insert these, it's ready to go
        if ($total === 1)
        {
            reset($emails);
            $email = current($emails);
            
            $sql = "INSERT INTO sugarchimp_mc_people (id,mailchimp_list_id,data,date_entered,date_modified,deleted) 
                    VALUES (".$db->getGuidSQL().",'".$db->quote($list_id)."','".$db->quote($email)."',".$db->now().",".$db->now().",0)";

            SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_to_sugar_person_update: single insert sql: '.$sql);
        }
        else
        {
            $sql = "INSERT INTO sugarchimp_mc_people (id,mailchimp_list_id,data,date_entered,date_modified,deleted) VALUES ";
            $i = 0;
            foreach ($emails as $email)
            {
                $i++;
                $sql .= "(".$db->getGuidSQL().",'".$db->quote($list_id)."','".$db->quote($email)."',".$db->now().",".$db->now().",0)";
                if ($i !== $total)
                {
                    $sql .= ",";
                }
            }
            SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_to_sugar_person_update: batch insert sql: '.$sql);
        }   

        $result = $db->query($sql);
        SugarChimp_Helper::log('debug','SugarChimp_Queue::queue_mailchimp_to_sugar_person_update: batch insert sql result: '.print_r($result,true));

        return true;
    }
}