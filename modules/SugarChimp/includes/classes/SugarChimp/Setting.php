<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Logger.php');

require_once('modules/SugarChimp/includes/classes/SugarChimp/Loader.php');
SugarChimp_Loader::load('Core/Setting');

use Fanatical\Core\v1a\Setting as Setting;

class SugarChimp_Setting extends Setting
{
    protected static $setting_group = 'sugarchimp';
    protected static $default_logger_value = 'normal';
    protected static $default_logging_enabled = 'true';

    // accept array of strings in second param
    // "hutchins.chad@gmail.com", array("asd8f6a98df","a9f7a9f677af","a90870gqhl")
    public static function add_to_optout_tracking_table($email, $mailchimp_list_ids=array())
    {
    	global $db;

    	if (empty($email))
    	{
    		// email address is required
    		SugarChimp_Logger::log('warning','SugarChimp_Setting::add_to_optout_tracking_table: email is empty and is required');
    		return false;
    	}

    	if (empty($mailchimp_list_ids) or !is_array($mailchimp_list_ids))
    	{
    		// mailchimp list ids is required
    		SugarChimp_Logger::log('warning','SugarChimp_Setting::add_to_optout_tracking_table: mailchimp_list_ids is empty and is required. expecting an array of mailchimp list ids.');
    		return false;
    	}

    	SugarChimp_Logger::log('debug','SugarChimp_Setting:add_to_optout_tracking_table: email: '.print_r($email,true));

    	// trim it and strtolower
    	$email = strtolower(trim($email));

    	SugarChimp_Logger::log('debug','SugarChimp_Setting:add_to_optout_tracking_table: mailchimp_list_ids: '.print_r($mailchimp_list_ids,true));

    	foreach ($mailchimp_list_ids as $mailchimp_list_id)
    	{
			$sql = "SELECT id AS id 
					FROM sugarchimp_optout_tracker 
					WHERE email LIKE '".$db->quote($email)."' AND mailchimp_list_id='".$db->quote($mailchimp_list_id)."'";
			SugarChimp_Logger::log('debug','SugarChimp_Setting:add_to_optout_tracking_table: sql: '.print_r($sql,true));

			$result = $db->query($sql);

			$row = $db->fetchByAssoc($result);
			SugarChimp_Logger::log('debug','SugarChimp_Setting:add_to_optout_tracking_table: row: '.print_r($row,true));

			if (!empty($row['id']))
			{
				// email/list is already tracked as unsubscribed, do not add
				SugarChimp_Logger::log('debug','SugarChimp_Setting:add_to_optout_tracking_table: email and mailchimp list id already tracked, not adding again');
				continue;
			}

			$sql = "INSERT INTO sugarchimp_optout_tracker (id,date_entered,email,mailchimp_list_id) 
					VALUES (".$db->getGuidSQL().",".$db->now().",'".$db->quote($email)."','".$db->quote($mailchimp_list_id)."')";
			SugarChimp_Logger::log('debug','SugarChimp_Setting:add_to_optout_tracking_table: sql: '.print_r($sql,true));

			// run the query
			$result = $db->query($sql);
			SugarChimp_Logger::log('debug','SugarChimp_Setting:add_to_optout_tracking_table: result: '.print_r($result,true));
    	}

    	return true;
    }

    // add multiples to optout tracker table
    // $data needs to be array like this: 
    // array(
    //     array(
    //         "email" => "one@gmail.com",
    //         "mailchimp_list_id" => "a79sf69a78f"
    //     ),
    //     array(
    //         "email" => "two@gmail.com",
    //         "mailchimp_list_id" => "a79sf69a78f"
    //     ),
    //     // etc.
    // )
    // it will not check for duplicates, add_to_output_tracking_table() will however
    public static function batch_add_to_optout_tracking_table($data)
    {
        global $db;

        if (empty($data) or !is_array($data))
        {
            // email address is required
            SugarChimp_Logger::log('warning','SugarChimp_Setting::batch_add_to_optout_tracking_table: data is empty and needs to be an array');
            return false;
        }

        $sql = "INSERT INTO sugarchimp_optout_tracker (id,date_entered,email,mailchimp_list_id) VALUES ";
        $first = true;

        foreach ($data as $item)
        {
            if (empty($item['email']) or empty($item['mailchimp_list_id']))
            {
                // can't have empty data
                continue;
            }

            // trim it and strtolower
            $email = strtolower(trim($item['email']));

            $mailchimp_list_id = $item['mailchimp_list_id'];

            // add comma if not first
            if ($first !== true) $sql .= ",";
            
            // add values end for this query
            $sql .= "(".$db->getGuidSQL().",".$db->now().",'".$db->quote($email)."','".$db->quote($mailchimp_list_id)."')";
            
            $first = false;
        }

        SugarChimp_Logger::log('debug','SugarChimp_Setting:batch_add_to_optout_tracking_table: sql: '.print_r($sql,true));

        // run the query
        if (!$result = $db->query($sql))
        {
            SugarChimp_Logger::log('fatal','SugarChimp_Setting:add_to_optout_tracking_table: query failed: '.print_r($sql,true));
            return false;
        }

        return true;
    }

    // accept array of strings in second param
    // "hutchins.chad@gmail.com", array("asd8f6a98df","a9f7a9f677af","a90870gqhl")
    public static function remove_from_optout_tracking_table($email, $mailchimp_list_ids=array())
    {
    	global $db;

    	if (empty($email))
    	{
    		// email address is required
    		SugarChimp_Logger::log('warning','SugarChimp_Setting::remove_from_optout_tracking_table: email is empty and is required');
    		return false;
    	}
    	    	
		SugarChimp_Logger::log('debug','SugarChimp_Setting:remove_from_optout_tracking_table: email: '.print_r($email,true));

    	// trim it and strtolower
    	$email = strtolower(trim($email));

    	if (empty($mailchimp_list_ids))
    	{
    		// we remove all references of email address
	    	$sql = "DELETE FROM sugarchimp_optout_tracker
	    			WHERE email='".$db->quote($email)."'";
			SugarChimp_Logger::log('debug','SugarChimp_Setting:remove_from_optout_tracking_table: sql: '.print_r($sql,true));

			$result = $db->query($sql);
			SugarChimp_Logger::log('debug','SugarChimp_Setting:remove_from_optout_tracking_table: result: '.print_r($result,true));
			return true;
    	}

    	if (!is_array($mailchimp_list_ids))
    	{
    		// needs to be an array of strings at this point
    		SugarChimp_Logger::log('warning','SugarChimp_Setting::remove_from_optout_tracking_table: at this point mailchimp_list_ids needs to be an array of strings');
    		return false;
    	}

    	SugarChimp_Logger::log('debug','SugarChimp_Setting:remove_from_optout_tracking_table: mailchimp_list_ids: '.print_r($mailchimp_list_ids,true));

    	$sql = "DELETE FROM sugarchimp_optout_tracker
    			WHERE email='".$db->quote($email)."' AND mailchimp_list_id IN ('".implode("','",$mailchimp_list_ids)."')";
		SugarChimp_Logger::log('debug','SugarChimp_Setting:remove_from_optout_tracking_table: sql: '.print_r($sql,true));

		$result = $db->query($sql);
		SugarChimp_Logger::log('debug','SugarChimp_Setting:remove_from_optout_tracking_table: result: '.print_r($result,true));

    	return true;
    }

    public static function is_on_optout_tracking_table($email, $mailchimp_list_id)
    {
        // chad & JON 1/26/2017 - quick fix here to completely ignore the optout tracking table
        return false;

    	global $db;

    	if (empty($email))
    	{
    		// email address is required
    		SugarChimp_Logger::log('warning','SugarChimp_Setting::is_on_optout_tracking_table: email is empty and is required');
    		return false;
    	}

    	if (empty($mailchimp_list_id))
    	{
    		// mailchimp list ids is required
    		SugarChimp_Logger::log('warning','SugarChimp_Setting::is_on_optout_tracking_table: mailchimp_list_id is empty and is required');
    		return false;
    	}

    	SugarChimp_Logger::log('debug','SugarChimp_Setting:is_on_optout_tracking_table: email: '.print_r($email,true));

    	// trim it and strtolower
    	$email = strtolower(trim($email));

    	SugarChimp_Logger::log('debug','SugarChimp_Setting:is_on_optout_tracking_table: mailchimp_list_id: '.print_r($mailchimp_list_id,true));

		$sql = "SELECT id AS id 
				FROM sugarchimp_optout_tracker 
				WHERE email LIKE '".$db->quote($email)."' AND mailchimp_list_id='".$db->quote($mailchimp_list_id)."'";
		SugarChimp_Logger::log('debug','SugarChimp_Setting:is_on_optout_tracking_table: sql: '.print_r($sql,true));

		$result = $db->query($sql);

		$row = $db->fetchByAssoc($result);
		SugarChimp_Logger::log('debug','SugarChimp_Setting:is_on_optout_tracking_table: row: '.print_r($row,true));

		if (empty($row['id']))
		{
			// we could not find the email/list in the table, it is NOT in the optout tracker table
			SugarChimp_Logger::log('debug','SugarChimp_Setting:is_on_optout_tracking_table: email and list id combination is NOT in the optout tracker table');
			return false;
		}
		else
		{
			// we found match. the email/list IS in the optout tracker table
			SugarChimp_Logger::log('debug','SugarChimp_Setting:is_on_optout_tracking_table: email and list id combination IS in the optout tracker table');
			return true;
		}
    }
}