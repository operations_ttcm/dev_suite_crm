<?php





require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');

require_once('modules/SugarChimp/includes/classes/SugarChimp/Scheduler/SugarCRMGroupsToMailchimp.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Scheduler/SugarCRMToMailchimp.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Scheduler/MailchimpToSugarCRM.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Scheduler/UpdateSugarPersonFromMailchimp.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Scheduler/SugarChimpBackups.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Scheduler/SugarChimpCleanup.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Scheduler/MailchimpListToSugarCRM.php');


require_once('modules/SugarChimp/includes/classes/SugarChimp/Scheduler/MailchimpActivityToSugarCRM.php');


class SugarChimp_Scheduler
{
    public static function forge($driver,$max_time_to_run=false,$max_jobs_to_process=false,$time_buffer=false)
    {
        if (empty($driver))
        {
            return false;
        }

        $driver = "SugarChimp_Scheduler_" . $driver;
        SugarChimp_Helper::log('debug',"Instantiate SugarChimp_Scheduler Driver {$driver}");
        return $driver::forge($max_time_to_run,$max_jobs_to_process,$time_buffer);
    }
}