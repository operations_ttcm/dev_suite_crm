<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Field.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Job.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/List.php');

require_once('modules/SugarChimp/includes/classes/Mailchimp/API.php');

class SugarChimp_FieldMap
{
    protected static $default_field_type = 'Basic';

    protected static $default_module = 'Prospects';
    
    public static $supported_modules = array(
        'Contacts',
        'Prospects',
        'Leads',
    );

    protected static $supported_related_modules = array(
        'Contacts',
        'Leads',
        'Accounts',
        'Users',
        'Opportunities',
    );

    // cached mappings keeps track of mappings already fetched from the database
    // it's structure looks something like this:
    // array(
    //     'list_id_1' => array(
    //         'module_a' => mapping...
    //         'module_b' => mapping...
    //     ),
    //     'list_id_2' => array(
    //         'module_a' => mapping...
    //         'module_b' => mapping...
    //     ),
    // )
    protected static $cached_mappings = array();
    protected static $cached_related_mappings = array();
    protected static $cached_merge_vars = array();

    protected static $default_mappings = array(
        'Contacts' => array(
            "fields" => array(
                "email1" => array(
                    "mapping" => array(
                        "sugarcrm_name" => "email1",
                        "mailchimp_name" => "NEW-EMAIL", // using new email so it updates properly if email changes
                    ),
                    "type" => "Email",
                ),
                "first_name" => array(
                    "mapping" => array(
                        "sugarcrm_name" => "first_name",
                        "mailchimp_name" => "FNAME",
                    ),
                    "type" => "Basic",
                ),
                "last_name" => array(
                    "mapping" => array(
                        "sugarcrm_name" => "last_name",
                        "mailchimp_name" => "LNAME",
                    ),
                    "type" => "Basic",
                ),
                "mailchimp_rating_c" => array(
                    "mapping" => array(
                        "sugarcrm_name" => "mailchimp_rating_c",
                        "mailchimp_name" => "MEMBER_RATING",
                    ),
                    "type" => "Basic",
                ),
            ),
        ),
        'Prospects' => array(
            "fields" => array(
                "email1" => array(
                    "mapping" => array(
                        "sugarcrm_name" => "email1",
                        "mailchimp_name" => "NEW-EMAIL", // using new email so it updates properly if email changes
                    ),
                    "type" => "Email",
                ),
                "first_name" => array(
                    "mapping" => array(
                        "sugarcrm_name" => "first_name",
                        "mailchimp_name" => "FNAME",
                    ),
                    "type" => "Basic",
                ),
                "last_name" => array(
                    "mapping" => array(
                        "sugarcrm_name" => "last_name",
                        "mailchimp_name" => "LNAME",
                    ),
                    "type" => "Basic",
                ),
                "mailchimp_rating_c" => array(
                    "mapping" => array(
                        "sugarcrm_name" => "mailchimp_rating_c",
                        "mailchimp_name" => "MEMBER_RATING",
                    ),
                    "type" => "Basic",
                ),
            ),
        ),
        'Leads' => array(
            "fields" => array(
                "email1" => array(
                    "mapping" => array(
                        "sugarcrm_name" => "email1",
                        "mailchimp_name" => "NEW-EMAIL", // using new email so it updates properly if email changes
                    ),
                    "type" => "Email",
                ),
                "first_name" => array(
                    "mapping" => array(
                        "sugarcrm_name" => "first_name",
                        "mailchimp_name" => "FNAME",
                    ),
                    "type" => "Basic",
                ),
                "last_name" => array(
                    "mapping" => array(
                        "sugarcrm_name" => "last_name",
                        "mailchimp_name" => "LNAME",
                    ),
                    "type" => "Basic",
                ),
                "mailchimp_rating_c" => array(
                    "mapping" => array(
                        "sugarcrm_name" => "mailchimp_rating_c",
                        "mailchimp_name" => "MEMBER_RATING",
                    ),
                    "type" => "Basic",
                ),
            ),
        ),
    );

    public static $default_account_mapping = array(
        "fields" => array(
            "email1" => array(
                "mapping" => array(
                    "sugarcrm_name" => "email1",
                    "mailchimp_name" => "NEW-EMAIL", // using new email so it updates properly if email changes
                ),
                "type" => "Email",
            ),
            "mailchimp_rating_c" => array(
                "mapping" => array(
                    "sugarcrm_name" => "mailchimp_rating_c",
                    "mailchimp_name" => "MEMBER_RATING",
                ),
                "type" => "Basic",
            ),
        ),
    );

    // $list_ids array of mailchimp list ids
    // $bean is NEW data coming in - $bean the SugarBean used in the logic hook
    // $data is OLD data already in system - $data array of old data from bean
    public static function check($list_ids,$bean,$data=array())
    {
        if (empty($bean) || empty($list_ids) || !is_array($list_ids))
        {
            SugarChimp_Helper::log('debug','SugarChimp_FieldMap::check bean or list_ids is empty');
            return false;
        }
        
        // make sure bean is of a supported module
        if (self::is_supported_module($bean->module_dir) !== true)
        {
            SugarChimp_Helper::log('debug',"SugarChimp_FieldMap::check Module is not currently a supported module {$bean->module_dir}");
            return false;
        }

        $jobs = array();

        foreach ($list_ids as $list_id)
        {        
            $mapping = self::get_field_mappings($list_id,$bean->module_dir);
        
            if (empty($mapping))
            {
                SugarChimp_Helper::log('fatal',"SugarChimp_FieldMap::check Could not get mapping for module {$bean->module_dir}");
                return false;
            }
        
            foreach ($mapping['fields'] as $field => $values)
            {
                if (empty($values['mapping']['sugarcrm_name']))
                {
                    SugarChimp_Helper::log("fatal","SugarChimp_FieldMap::check sugarcrm_name is empty for {$field}");
                    return false;
                }

                if (empty($values['mapping']['mailchimp_name']))
                {
                    SugarChimp_Helper::log("fatal","SugarChimp_FieldMap::check mailchimp_name is empty for {$field}");
                    return false;
                }

                if (SugarChimp_Field::is_related_field($values['mapping']['sugarcrm_name']))
                {
                    // we do not want to check related fields here
                    // they will get checked by another logic hook
                    SugarChimp_Logger::log('debug',"SugarChimp_FieldMap::check this is a related field, ignore these here. these are checked by another logichook/method. field: ".print_r($values,true));
                    continue;
                }

                // setup the data we need
                $field_type = empty($values['type']) ? self::$default_field_type : $values['type'];
                $sugarcrm_name = $values['mapping']['sugarcrm_name'];
                $mailchimp_name = $values['mapping']['mailchimp_name'];

                /**
                chad 4/27/2015 - in if statements below, removed 'continue' blocks as it interferred with
                empty to filled and filled to empty cases, set the values to empty so they are set later in the process
                */
                if (!isset($bean->$sugarcrm_name))
                {
                    // the field doesn't exist for some reason
                    SugarChimp_Helper::log("debug","SugarChimp_FieldMap::check Field does not exist or is null on new bean for {$bean->module_dir}->{$sugarcrm_name} record {$bean->id}");
                    // setting it to blank so the new data can override it
                    $bean->$sugarcrm_name = '';
                }
        
                if (!isset($data[$sugarcrm_name]))
                {
                    // the fetched row field doesn't exist for some reason
                    SugarChimp_Helper::log("debug","SugarChimp_FieldMap::check Field does not exist or is null on old bean for {$bean->module_dir}->{$sugarcrm_name} record {$bean->id}");
                    // setting it to blank so the new data can override it
                    $data[$sugarcrm_name] = '';
                }

                // check the field to see what action needs to be taken
                $new_jobs = SugarChimp_Field::forge($field_type)->check($sugarcrm_name,$bean,$data);

                if ($new_jobs !== false && is_array($new_jobs))
                {
                    $jobs = array_merge($jobs,$new_jobs);
                }
            }
        }
        
        $consolidated_jobs = array();
        if (is_array($jobs) && !empty($jobs))
        {
            $consolidated_jobs = self::consolidate_jobs($jobs);
        }
        
        if (is_array($consolidated_jobs) && !empty($consolidated_jobs))
        {
            self::queue_jobs($consolidated_jobs,$list_ids);
        }
        
        return true;
    }

    public static function check_related_record_changes($bean,$data=array(),$force_update_check=false)
    {
        if (empty($bean) || empty($bean->module_dir))
        {
            SugarChimp_Helper::log('warning','SugarChimp_Helper::check_related_record_changes bean is empty');
            return false;
        }
        
        // get the related mappings 
        $related_mappings = self::get_related_field_mappings();

        if (empty($related_mappings))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::check_related_record_changes there are no related mappings');
            return true;
        }

        if (empty($related_mappings[$bean->module_dir]) || !is_array($related_mappings[$bean->module_dir]))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::check_related_record_changes there are no related mappings for module '.print_r($bean->module_dir,true));
            return true;
        }

        global $db;

        // the array that will track the stuff that needs to be queued
        $related_bean_changes = array();

        // go through each parent field
        foreach ($related_mappings[$bean->module_dir] as $parent_field => $mailchimp_list_ids)
        {
            // if force update check is true, we make it find related records and queue changes
            // if not, have it check if fields were changed
            if ($force_update_check === false)
            {
                // check if the parent module / parent field changed
                $change_type = SugarChimp_Field::forge('Generic')->check($parent_field,$bean,$data);
                
                $changes_were_made = false;

                switch ($change_type)
                {
                    case 'new_record':
                    case 'empty_to_filled':
                    case 'filled_to_empty':
                    case 'value_modified':
                        // changes were made! keep going
                        $changes_were_made = true;
                }

                if ($changes_were_made === false)
                {
                    // changes were not made, go to the next item in the for loop if there is one
                    continue;
                }
            }

            // changes were made, find who needs to be queued for an update
            foreach ($mailchimp_list_ids as $mailchimp_list_id => $modules)
            {
                $lists = SugarChimp_List::get_synced_sugar_lists("name","mailchimp_list_name_c = '".$db->quote($mailchimp_list_id)."'");
                if (empty($lists['list']) or !is_array($lists['list']))
                {
                    // the list is no longer synced, maybe it was deleted
                    // go to the next list
                    continue;
                }

                if (count($lists['list'])>1)
                {
                    // more than one sugar target list has the same mailchimp list id
                    // this should never be the case, we'll carry on trying to use the first
                    SugarChimp_Logger::log('fatal','SugarChimp_FieldMap::check_related_record_changes: error found, but continued. more than one sugar target list has the same mailchimp list id: '.print_r($mailchimp_list_id,true));
                }

                // get the first list
                reset($lists['list']);
                $list = current($lists['list']);

                if (empty($list['id']))
                {
                    // we have bad data, the target list does not have an idea
                    // get out of here and go to next list to process
                    SugarChimp_Logger::log('fatal','SugarChimp_FieldMap::check_related_record_changes: target list did not have an id. the target list was found by mailchimp_list_id: '.print_r($mailchimp_list_id,true));
                    continue;
                }

                $sugar_list_id = $list['id'];

                foreach ($modules as $module => $link_fields)
                {
                    SugarChimp_Logger::log('debug','SugarChimp_FieldMap::check_related_record_changes module: '.print_r($module,true));
                            
                    foreach ($link_fields as $key => $link_field_data)
                    {
                        if (empty($link_field_data['link_field_name']))
                        {
                            // link field name isn't set, can't look stuff up
                            SugarChimp_Logger::log('fatal','SugarChimp_FieldMap::check_related_record_changes link field name is not set in link_field_data: '.print_r($link_field_data,true));
                            continue;
                        }

                        SugarChimp_Logger::log('debug','SugarChimp_FieldMap::check_related_record_changes link field name: '.print_r($link_field_data['link_field_name'],true));

                        $base_bean = BeanFactory::getBean($module);

                        if (empty($base_bean))
                        {
                            // could not load up a bean
                            SugarChimp_Logger::log('fatal','SugarChimp_FieldMap::check_related_record_changes could not load up bean for module: '.print_r($module,true));
                            continue;
                        }

                        SugarChimp_Logger::log('debug','SugarChimp_FieldMap::check_related_record_changes loaded bean for module: '.print_r($module,true));
                            
                        if (!method_exists($base_bean,'load_relationship'))
                        {
                            // got a bean but cannot load relationships
                            SugarChimp_Logger::log('fatal','SugarChimp_FieldMap::check_related_record_changes load relationship method does not exist for module: '.print_r($module,true));
                            continue;
                        }

                        SugarChimp_Logger::log('debug','SugarChimp_FieldMap::check_related_record_changes load relationship method exists for module: '.print_r($module,true));

                        $link_field_name = $link_field_data['link_field_name'];

                        if (empty($link_field_name))
                        {
                            // method does not exist, cannot look up parent rleated link name
                            SugarChimp_Logger::log('fatal','SugarChimp_FieldMap::check_related_record_changes there was no link name provided for module: '.print_r($bean->module_dir,true).' mc_list_id: '.print_r($mailchimp_list_id,true).' module: '.print_r($module,true).' key: '.print_r($key,true));
                            continue;
                        }

                        SugarChimp_Logger::log('debug','SugarChimp_FieldMap::check_related_record_changes link_field_name: '.print_r($link_field_name,true));

                        // load up the link between the primary module and the related mapped field module
                        $base_bean->load_relationship($link_field_name);

                        if (!method_exists($base_bean->$link_field_name,'getRelatedModuleLinkName'))
                        {
                            // method does not exist, cannot look up parent rleated link name
                            SugarChimp_Logger::log('fatal','SugarChimp_FieldMap::check_related_record_changes cannot load parent related module link name from module: '.print_r($module,true).' and link name: '.print_r($link_field_name,true));
                            continue;
                        }

                        SugarChimp_Logger::log('debug','SugarChimp_FieldMap::check_related_record_changes get related module link name method exists');

                        // we need the relationship loaded above to get the related link name so we can pull records from
                        // the related parent module perspective, as that is what changed
                        $parent_link = $base_bean->$link_field_name->getRelatedModuleLinkName();

                        if (empty($parent_link))
                        {
                            // could not find the parent link
                            SugarChimp_Logger::log('warning','SugarChimp_FieldMap::check_related_record_changes could not find parent link name from module: '.print_r($module,true).' and link name: '.print_r($link_field_name,true));
                            continue;
                        }

                        SugarChimp_Logger::log('debug','SugarChimp_FieldMap::check_related_record_changes parent_link: '.print_r($parent_link,true));

                        // bean is the bean that was passed to the method
                        // the related parent object

                        if (!method_exists($bean,'load_relationship'))
                        {
                            // got a bean but cannot load relationships
                            SugarChimp_Logger::log('fatal','SugarChimp_FieldMap::check_related_record_changes load relationship method does not exist for bean passed in of module: '.print_r($bean->module_dir,true));
                            continue;
                        }

                        SugarChimp_Logger::log('debug','SugarChimp_FieldMap::check_related_record_changes load relationship method does not exist for bean passed in of module: '.print_r($bean->module_dir,true));

                        $bean->load_relationship($parent_link);

                        if (!method_exists($bean->$parent_link,'getBeans'))
                        {
                            // relationship does not have getBeans method
                            SugarChimp_Logger::log('fatal','SugarChimp_FieldMap::check_related_record_changes getBeans method does not exist for relationship passed in of module: '.print_r($bean->module_dir,true).' and parent link field: '.print_r($parent_link,true));
                            continue;
                        }

                        SugarChimp_Logger::log('debug','SugarChimp_FieldMap::check_related_record_changes getBeans method exists for relationship passed in of module: '.print_r($bean->module_dir,true).' and parent link field: '.print_r($parent_link,true));

                        $job = "UpdateMailchimpSubscriber";
                        if (!empty($link_field_data['field_type']) && $link_field_data['field_type']=='Group')
                        {
                            $job = "UpdateMailchimpSubscriberGroup";
                        }

                        $related_ids = array();
                        foreach ($bean->$parent_link->getBeans() as $related_bean) 
                        {
                            $related_ids []= $related_bean->id;   
                        }
                        SugarChimp_Logger::log('debug','SugarChimp_FieldMap::check_related_record_changes found related_ids: '.print_r($related_ids,true));

                        if (!empty($related_ids) and is_array($related_ids))
                        {
                            // there were related ids
                            // find the ones that are on a related sugar target list and 
                            // add them to the array for later queueing
                                    
                            $sql = "SELECT DISTINCT plp.related_id as id
                                    FROM prospect_lists_prospects plp
                                    INNER JOIN prospect_lists pl ON pl.id=plp.prospect_list_id
                                    WHERE 
                                        plp.deleted=0 AND 
                                        pl.deleted=0 AND
                                        pl.mailchimp_list_name_c='".$db->quote($mailchimp_list_id)."' AND 
                                        plp.related_id IN ('".implode("','",$related_ids)."') AND 
                                        plp.related_type='".$db->quote($module)."'";

                            SugarChimp_Logger::log('debug','SugarChimp_FieldMap::check_related_record_changes sql: '.print_r($sql,true));

                            if (!$result = $db->query($sql))
                            {
                                SugarChimp_Logger::log('fatal','SugarChimp_FieldMap::check_related_record_changes query failed: '.print_r($sql,true));
                                continue;
                            }

                            while ($row = $db->fetchByAssoc($result))
                            {
                                if (empty($row['id']))
                                {
                                    continue;
                                }

                                if (!isset($related_bean_changes[$mailchimp_list_id]))
                                {
                                    $related_bean_changes[$mailchimp_list_id] = array();   
                                }

                                if (!isset($related_bean_changes[$mailchimp_list_id][$module]))
                                {
                                    $related_bean_changes[$mailchimp_list_id][$module] = array();   
                                }

                                if (!isset($related_bean_changes[$mailchimp_list_id][$module][$job]))
                                {
                                    $related_bean_changes[$mailchimp_list_id][$module][$job] = array();   
                                }

                                $related_bean_changes[$mailchimp_list_id][$module][$job][$row['id']] = $row['id'];
                            }
                        }
                    }
                }
            }
        }

        SugarChimp_Logger::log('debug','SugarChimp_FieldMap::check_related_record_changes related_bean_changes: '.print_r($related_bean_changes,true));

        return $related_bean_changes;
    }
    
    function consolidate_jobs($jobs)
    {
        if (!is_array($jobs) || empty($jobs))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::consolidate_jobs jobs is empty');
            return false;
        }
        
        /**
        This doesn't need to be complicated yet until we have several more types of jobs
        The primary reason for this right now (2/13/2014) is we don't want subscriber updates
        to occur if there is also a job to remove the subscriber.
        */
        $update_mailchimp_subscriber = array();
        $update_mailchimp_subscriber_group = array();
        $update_mailchimp_subscriber_email_change = array();
        $remove_mailchimp_subscriber = array();
        foreach ($jobs as $key => $job)
        {
            if (!empty($job['name']))
            {
                switch ($job['name'])
                {
                    case "UpdateMailchimpSubscriber":
                        $update_mailchimp_subscriber []= $job;
                        break;
                    case "UpdateMailchimpSubscriberGroup":
                        $update_mailchimp_subscriber_group []= $job;
                        break;
                    case "UpdateMailchimpSubscriberEmailChange":
                        $update_mailchimp_subscriber_email_change []= $job;
                        break;
                    case "RemoveMailchimpSubscriber":
                        $remove_mailchimp_subscriber []= $job;
                        break;
                }
            }
            else
            {
                SugarChimp_Helper::log('warning','SugarChimp_Helper::consolidate_jobs $job does not have a name: '.print_r($job,true));
            }
        }
        
        if (count($remove_mailchimp_subscriber) > 0)
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::consolidate_jobs returning remove_mailchimp_subscriber job');
            reset($remove_mailchimp_subscriber);
            return array(current($remove_mailchimp_subscriber));
        }
        else if (count($update_mailchimp_subscriber_email_change) > 0)
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::consolidate_jobs returning update_mailchimp_subscriber_email_change job');
            reset($update_mailchimp_subscriber_email_change);

            if (count($update_mailchimp_subscriber_group) > 0)
            {
                SugarChimp_Helper::log('debug','SugarChimp_Helper::consolidate_jobs also returning update_mailchimp_subscriber_group job as the gorups changed as well');
                reset($update_mailchimp_subscriber_group);
                return array(current($update_mailchimp_subscriber_email_change),current($update_mailchimp_subscriber_group));
            }
            else
            {
                return array(current($update_mailchimp_subscriber_email_change));
            }
        }
        else if (count($update_mailchimp_subscriber_group) > 0)
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::consolidate_jobs returning update_mailchimp_subscriber_group job');
            reset($update_mailchimp_subscriber_group);
            return array(current($update_mailchimp_subscriber_group));
        }
        else if (count($update_mailchimp_subscriber) > 0)
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::consolidate_jobs returning update_mailchimp_subscriber job');
            reset($update_mailchimp_subscriber);
            return array(current($update_mailchimp_subscriber));
        }
        else
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::consolidate_jobs returning no jobs');
            return array();
        }
    }
    
    public static function queue_jobs($jobs,$list_ids)
    {
        if (!is_array($jobs) || empty($jobs))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::queue_jobs jobs is empty');
            return false;
        }

        if (!is_array($list_ids) || empty($list_ids))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::queue_jobs list_ids is empty');
            return false;
        }

        // queue each job for each list
        foreach ($jobs as $job)
        {
            // setup default params
            $params = array(
                'param1' => '',
                'param2' => '',
                'param3' => '',
                'param4' => '',
                'param5' => '',
            );
            
            if (!empty($job['params']))
            {
                $params = $job['params'];
            }

            foreach ($list_ids as $list_id)
            {
                SugarChimp_Helper::log('debug',"SugarChimp_Helper::queue_jobs queueing {$job['name']} for {$list_id}");
                $sc_job = SugarChimp_Job::forge($job['name'],$list_id,array($params));
                if ($sc_job instanceOf SugarChimp_Job_Driver)
                {
                    $sc_job->queue();
                }
                else
                {
                    SugarChimp_Helper::log('warning',"SugarChimp_Helper::queue_jobs Could not forge job class for data: {$job['name']} for {$list_id}");
                }
            }
        }
    }
    
    public static function is_mapped_module($module)
    {
        if (empty($module))
        {
            return false;
        }
        
        $supported_modules = static::get_supported_modules();

        if (empty($supported_modules) or !is_array($supported_modules))
        {
            return false;
        }

        return in_array($module,$supported_modules);
    }
    
    public static function is_supported_module($module)
    {
        return self::is_mapped_module($module);
    }
    
    public static function is_supported_related_module($module)
    {
        if (empty($module))
        {
            return false;
        }
        
        return in_array($module,self::$supported_related_modules);
    }
    
    public static function get_field_mappings($list_id, $module)
    {
        // list_id is required
        if (empty($list_id))
        {
            SugarChimp_Helper::log('warning',"SugarChimp_FieldMap::get_field_mappings list_id is required");
            return false;
        }

        // module required
        if (empty($module))
        {
            SugarChimp_Helper::log('warning',"SugarChimp_FieldMap::get_field_mappings module is required");
            return false;
        }
        
        // make sure the module is mapped
        if (!self::is_mapped_module($module))
        {
            SugarChimp_Helper::log('warning',"SugarChimp_FieldMap::get_field_mappings {$module} is not a mapped module.");
            return false;
        }
        
        // check mapping cache first, will be expensive to check the database each time
        // if we find a match, return it
        if (!empty(self::$cached_mappings[$list_id][$module]) && is_array(self::$cached_mappings[$list_id][$module]))
        {
            return self::$cached_mappings[$list_id][$module];
        }
        
        // lookup based on database config
        $key = self::get_field_mapping_config_key($list_id,$module);
        
        if (empty($key))
        {
            SugarChimp_Helper::log('warning',"SugarChimp_FieldMap::get_field_mappings cannot lookup admin config key for {$list_id} and {$module}");
            return false;
        }

        $mapping = SugarChimp_Setting::retrieve($key);
        
        // if it can't find it, return the default
        if (empty($mapping))
        {
            if ($module == "Accounts")
            {
                SugarChimp_Helper::log('warning',"SugarChimp_FieldMap::get_field_mappings could not find mapping for key {$key}. This is for Accounts. Returning the default mapping for Accounts.");
                return self::$default_account_mapping;
            }

            SugarChimp_Helper::log('warning',"SugarChimp_FieldMap::get_field_mappings could not find mapping for key {$key}. Returning the default mapping.");
            return self::$default_mappings[$module];
        }
        
        // decode it back to php
        $mapping = unserialize(base64_decode($mapping));
      
        // set cache
        self::$cached_mappings[$list_id][$module] = $mapping;
          
        return $mapping;
    }

    public static function save_field_mappings($list_id,$module,$mapping)
    {
        // list_id is required
        if (empty($list_id))
        {
            SugarChimp_Helper::log('warning',"SugarChimp_FieldMap::save_field_mappings list_id is required");
            return false;
        }

        // module required
        if (empty($module))
        {
            SugarChimp_Helper::log('warning',"SugarChimp_FieldMap::save_field_mappings module is required");
            return false;
        }
        
        // mapping required
        if (empty($mapping) || !is_array($mapping))
        {
            SugarChimp_Helper::log('warning',"SugarChimp_FieldMap::save_field_mappings mapping is required");
            return false;
        }

        $key = self::get_field_mapping_config_key($list_id,$module);
        SugarChimp_Helper::log('debug',"SugarChimp_FieldMap::save_field_mappings key: {$key}");
        
        if (empty($key))
        {
            SugarChimp_Helper::log('warning',"SugarChimp_FieldMap::save_field_mappings cannot lookup admin config key for {$list_id} and {$module}");
            return false;
        }
        
        // set cache
        self::$cached_mappings[$list_id][$module] = $mapping;
        
        // encode it to save to db
        $mapping = base64_encode(serialize($mapping));
        
        return SugarChimp_Setting::set($key,$mapping);
    }

    // anytime the field mappings get changed, the related field mapping array needs to be updated
    // the array that gets saved here keeps track of the related fields set in field mapping
    // it is used in the logic hooks to quickly detect if changes in a parent related field were made
    // foreach ($mapped_related_fields as $parent_module => $parent_fields)
    // {
    //     foreach ($parent_fields as $parent_field => $mailchimp_list_ids)
    //     {
    //         foreach ($mailchimp_list_ids as $mailchimp_list_id => $modules)
    //         {
    //             foreach ($modules as $module => $link_fields)
    //             {
    //                 foreach ($link_fields as $key => $link_field)
    //                 {
    //                      $link_field['link_field_name'] // field to use to connect the base module to the related parent module
    //                      $link_field['field_type'] // need to know if it's a regular field or group field to sync
    //                 }
    //             }
    //         }
    //     }
    // }
    public static function update_related_field_mappings()
    {
        // first check to see if we are on a supported version that support parent related field mappings
        // if we are not, get out of here
        if (self::is_related_field_mapping_supported() !== true)
        {
            SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_related_field_mappings: current plan does not support related parent field mapping');
            return true;
        }

        // get all synced lists
        $lists = SugarChimp_Helper::get_synced_lists();

        if (empty($lists))
        {
            SugarChimp_Logger::log('debug','SugarChimp_FieldMap::update_related_field_mappings there are no synced lists to save');
            return true;
        }

        // get all their field mappings
        // foreach one, make the related array
        $mapped_related_fields = array();
        $modules = self::get_supported_modules();
        foreach ($lists as $list)
        {
            $group_fields = SugarChimp_Field_Group::get_group_fields($list['mailchimp_list_id'],$modules);
                
            foreach ($modules as $module)
            {
                $mapping = self::get_field_mappings($list['mailchimp_list_id'],$module);

                // does the list have a group field being synced
                // this information gets stored to know what type of update job needs to be run
                $group_field_is_synced = false;
                if (!empty($group_fields[$module]))
                {
                    $group_field_is_synced = true;
                }

                foreach ($mapping['fields'] as $sugarcrm_name => $field)
                {
                    // check if it's a related field
                    if (SugarChimp_Field::is_related_field($sugarcrm_name))
                    {
                        $tokens = explode('.',$sugarcrm_name);
                        $link_field_name = $tokens[0];
                        $parent_field_name = $tokens[1];

                        $bean = BeanFactory::getBean($module);
                        $found = $bean->load_relationship($link_field_name);

                        if ($found !== true)
                        {
                            // failed load
                            SugarChimp_Logger::log('fatal','SugarChimpAPI::saveMappingOptions: no relationship provided for module '.print_r($module,true).' and field '.print_r($link_field_name,true));
                            unset($module_mappings[$module][$sugarcrm_name]);
                            continue;
                        }

                        $parent_module = $bean->$link_field_name->getRelatedModuleName();

                        if (!isset($mapped_related_fields[$parent_module]))
                        {
                            $mapped_related_fields[$parent_module] = array();                    
                        }

                        if (!isset($mapped_related_fields[$parent_module][$parent_field_name]))
                        {
                            $mapped_related_fields[$parent_module][$parent_field_name] = array();                    
                        }

                        if (!isset($mapped_related_fields[$parent_module][$parent_field_name][$list['mailchimp_list_id']]))
                        {
                            $mapped_related_fields[$parent_module][$parent_field_name][$list['mailchimp_list_id']] = array();                    
                        }

                        if (!isset($mapped_related_fields[$parent_module][$parent_field_name][$list['mailchimp_list_id']][$module]))
                        {
                            $mapped_related_fields[$parent_module][$parent_field_name][$list['mailchimp_list_id']][$module] = array();                    
                        }

                        $mapped_related_fields[$parent_module][$parent_field_name][$list['mailchimp_list_id']][$module][] = array(
                            'link_field_name' => $link_field_name,
                            'field_type' => ($group_field_is_synced===true) ? 'Group' : false,
                        );
                    }
                }
            }
        }

        SugarChimp_Logger::log('debug','SugarChimp_FieldMap::update_related_field_mappings new mapping: '.print_r($mapped_related_fields,true));        

        self::$cached_related_mappings = $mapped_related_fields;

        return self::save_related_field_mappings($mapped_related_fields);
    }

    public static function get_related_field_mappings($refresh_cache=false)
    {
        // if we want to grab from the cache and it's not empty, return it
        // otherwise, go get the mapping data
        if ($refresh_cache === false && !empty(self::$cached_related_mappings))
        {
            SugarChimp_Logger::log('debug','SugarChimp_FieldMap::get_related_field_mappings: returning cached value: '.print_r(self::$cached_related_mappings,true));
            return self::$cached_related_mappings;
        }

        // get the related from the database config settings
        $related_field_mappings = SugarChimp_Setting::retrieve('related_field_mappings');

        // if nothing is there, there is nothing mapped
        // return empty array
        if (empty($related_field_mappings))
        {
            return array();
        }

        $related_field_mappings = unserialize(base64_decode($related_field_mappings));

        // set the cache
        self::$cached_related_mappings = $related_field_mappings;

        SugarChimp_Logger::log('debug','SugarChimp_FieldMap::get_related_field_mappings: getting fresh related field values from database: '.print_r(self::$cached_related_mappings,true));

        return $related_field_mappings;
    }

    public static function save_related_field_mappings($related_field_mappings=array())
    {
        // set the cache
        self::$cached_related_mappings = $related_field_mappings;

        $related_field_mappings = base64_encode(serialize($related_field_mappings));

        return SugarChimp_Setting::set('related_field_mappings',$related_field_mappings);
    }

    public static function is_mapped_related_module($module)
    {
        if (empty($module))
        {
            return false;
        }
        
        $mapping = self::get_related_field_mappings();

        if (empty($mapping))
        {
            // there are no mappings, so it's not mapped
            return false;
        }

        // if the module passed exists as a key in the array, it is being mapped
        return array_key_exists($module, $mapping);
    }

    public static function get_field_mapping_config_key($list_id,$module)
    {
        // list_id is required
        if (empty($list_id))
        {
            SugarChimp_Helper::log('fatal',"SugarChimp_FieldMap::get_field_mapping_config_key list_id is required");
            return false;
        }

        // module required
        if (empty($module))
        {
            SugarChimp_Helper::log('fatal',"SugarChimp_FieldMap::get_field_mapping_config_key module is required");
            return false;
        }
        
        return $list_id . "|" . $module;
    }
    
    // this method updates existing records or creates new records based on provided data from mailchimp
    // $bean is either a SugarBean object or a string of a valid mapped module (Contacts, Prospects, Leads, etc.)
    // $data is $data['merges'] array provided from mailchimp, usually from a mailchimp webhook
    // $interests - array of interests passed from mailchimp member call, this was interoduced in SC 7.8
    //      as they made a lot of changes to how interest categories and interests work
    //      the 'GROUPINGS' is passed back on webhook calls as of 10/17/2016, but no longer
    //      available GET /member calls, so we have to use the new interests data
    //      hopefully mailchimp will start to use the interest data array in the webhooks and export api
    //      othewise we will continue to support it all
    public static function update_bean($list_id,$bean,$data,$interests=false)
    {
        if (empty($list_id))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_FieldMap::update_bean list_id is required');
            return false;
        }
        
        if (empty($bean) || !$bean instanceOf SugarBean)
        {
            SugarChimp_Helper::log('fatal','SugarChimp_FieldMap::update_bean a bean is empty');
            return false;
        }
        
        if (empty($data) || !is_array($data))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_FieldMap::update_bean a valid data array is required');
            return false;
        }

        /**


        If we ever wanted to just not allow bean updates from MailChimp
        We could have a config check here, and return true
        We would still need to do a $bean->save() because if a new person is added from MailChimp
        If there is no matching email address, we would want them to be allowed to be added
        The question is then, if it is a new bean, do we allow it to follow the field mapping?
        Probably so


        */
        
        $mapping = self::get_field_mappings($list_id,$bean->module_dir);
        
        if (empty($mapping))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_FieldMap::update_bean a mapping could not be loaded for module: '.$bean->module_dir);
            return false;
        }
        
        // special check for email address, MailChimp may return it as NEW-EMAIL or EMAIL
        // if one is not empty and the other is empty, set it 
        if (!empty($data['NEW-EMAIL']) && empty($data['EMAIL']))
        {
            $data['EMAIL'] = $data['NEW-EMAIL'];
        }
        
        if (!empty($data['EMAIL']) && empty($data['NEW-EMAIL']))
        {
            $data['NEW-EMAIL'] = $data['EMAIL'];
        }
        
        $sugar_has_data_mailchimp_doesnt = false;

/**

If webhooks response data ever changes to reflect the new interest category / interests
this code block will likely need to change
If mailchimp ever stops returning the 'GROUPINGS' data in this format, things will
definitely have to be updated here

*/

        // if group data is present, format it in an easier way we can use it
        // this is legacy pre-SC 7.8 stuff, this data is passed from the old
        // memberInfo call as well as webhooks as of 10/17/2016
        // the new way to do things will be to use the $interests parameter
        // interests will be boolean FALSE if we are doing it the old way
        $grouping_data = array();
        if ($interests === false && !empty($data['GROUPINGS']) && is_array($data['GROUPINGS']))
        {
            foreach ($data['GROUPINGS'] as $group)
            {
                $groups_string = trim($group['groups']); 
                $groups_array = array();
                
                if (strpos($groups_string, '\,') !== FALSE)
                {
                    $groups_string = str_replace('\,','(SCCOMMA)',$groups_string); // this is really dumb
                    $groups_array = str_getcsv($groups_string);
                    foreach ($groups_array as $key => $val)
                    {
                        $groups_array[$key] = trim(str_replace('(SCCOMMA)',',',$val));
                    }
                }
                else if (!empty($groups_string))
                {
                    $groups_array = str_getcsv($groups_string);
                    foreach ($groups_array as $key => $val)
                    {
                        $groups_array[$key] = trim($val);
                    }
                }

                $grouping_data[$group['id']] = array(
                    'id' => $group['id'],
                    'name' => $group['name'],
                    'groups' => $groups_array,
                );
            }
        }

        SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean grouping_data: '.print_r($grouping_data,true));
        SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean interests: '.print_r($interests,true));
        
        $data_has_changed = false;
        foreach ($mapping['fields'] as $key => $field)
        {
            SugarChimp_Helper::log('debug',"SugarChimp_FieldMap::update_bean key ".print_r($key,true));
            SugarChimp_Helper::log('debug',"SugarChimp_FieldMap::update_bean field ".print_r($field,true));

            if (empty($field['mapping']['sugarcrm_name']) || empty($field['mapping']['mailchimp_name']))
            {
                // the required mapping fields are not set
                SugarChimp_Helper::log('warning','SugarChimp_FieldMap::update_bean the sugarcrm_name and mailchimp_name mapping fields are not provided in the mapping');
                continue;
            }
            
            SugarChimp_Helper::log('debug',"SugarChimp_FieldMap::update_bean sugarcrm_name {$field['mapping']['sugarcrm_name']}");
            SugarChimp_Helper::log('debug',"SugarChimp_FieldMap::update_bean mailchimp_name {$field['mapping']['mailchimp_name']}");
            
            if (SugarChimp_Field::is_related_field($field['mapping']['sugarcrm_name']))
            {
                // we do not update related fields 
                SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean we do not update parent related fields, going to next field. sugarcrm field name: '.print_r($field,true));
                continue;
            }
            
            // get the sugar field type
            $sugarcrm_field_type = self::get_sugarcrm_field_type($bean,$field['mapping']['sugarcrm_name']);
            
            switch($field['type'])
            {
                case "Group":
                    SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean SugarChimp field type is Group');
                    
                    // if group data is present, format it in an easier way we can use it
                    // this is legacy pre-SC 7.8 stuff, this data is passed from the old
                    // memberInfo call as well as webhooks as of 10/17/2016
                    // the new way to do things will be to use the $interests parameter
                    // interests will be boolean FALSE if we are doing it the old way
                    $values = array();
                    if ($interests === false)
                    {
                        if (empty($grouping_data[$field['mapping']['mailchimp_name']]))
                        {
                            // even though this is a synced group field, mailchimp didn't give us anything so skip it
                            // this case will happen when a groupset is deleted on the MC side, but the mapping still exists in sugar
                            SugarChimp_Helper::log('warning','SugarChimp_FieldMap::update_bean group data was not provided from mailchimp for field, maybe the group is deleted in MC but still in Sugar mapping?: '.print_r($field,true));
                            continue;
                        }

                        // get the current value from the bean
                        $values = $grouping_data[$field['mapping']['mailchimp_name']]['groups'];
                    }
                    else
                    {
                        // let's do it the new way, use the interests array
                        // if all of this is true, let's try to find what values were selected
                        if (!empty($interests) && is_array($interests) && !empty($field['param1']) && is_array($field['param1']))
                        {
                            // foreach selected interest, if the interest is true and it exists in the fields param1 array
                            // add it to the selected values array
                            foreach ($interests as $selected_interest_id => $interest_selected)
                            {
                                if ($interest_selected === true && array_key_exists($selected_interest_id,$field['param1']))
                                {
                                    $values []= $field['param1'][$selected_interest_id];
                                }
                            }
                        }
                        else
                        {
                            SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean: there must be no interests selected, so values will be empty');
                        }
                    }

                    // update these calls for php7 ...can't do below lines
                    // any object-> (some array) doesn't work the same in php7
                    // // OK for PHP5 but not for PHP7
                    // $obj->$field['f1']['f2'] = $value;

                    // // PHP7 is happy
                    // $obj->{$field['f1']['f2']} = $value;

                    // grab the label from the options to send to MC
                    $list_name = $bean->field_defs[$field['mapping']['sugarcrm_name']]['options'];
                    SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean grab label for list_name - '.print_r($list_name,true));

                    // dropdowns (enum) and multiselects (multienum) are the two supported field types for groups
                    // each need to be handled differently 
                    switch ($sugarcrm_field_type)
                    {
                        case "enum":
                            SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean Sugar field type is enum');

                            if (empty($values))
                            {
                                // all groups were removed from mailchimp subscriber
                                if(self::update_if_changed($bean->{$field['mapping']['sugarcrm_name']},''))
                                {
                                    $data_has_changed = true;
                                    $bean->{$field['mapping']['sugarcrm_name']} = '';
                                }
                            }
                            else
                            {
                                // dropdowns can only have one value, put first entry of the array in bean value (should only have one anyways)
                                $group_items = self::get_enum_keys($list_name,$values);
                                reset($group_items);
                                
                                if(self::update_if_changed($bean->{$field['mapping']['sugarcrm_name']},current($group_items)))
                                {
                                    $data_has_changed = true;
                                    $bean->{$field['mapping']['sugarcrm_name']} = current($group_items);
                                }
                            }

                            break;

                        case "multienum":
                            SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean Sugar field type is multienum');

                            if (empty($values))
                            {
                                // all groups were removed from mailchimp subscriber
                                if(self::update_if_changed($bean->{$field['mapping']['sugarcrm_name']},''))
                                {
                                    $data_has_changed = true;
                                    $bean->{$field['mapping']['sugarcrm_name']} = '';
                                }
                            }
                            else
                            {
                                // get values and encode it for sugar multienums and save it to the bean
                                $group_items = self::get_enum_keys($list_name,$values);
                                if(self::update_if_changed($bean->{$field['mapping']['sugarcrm_name']},encodeMultienumValue($group_items)))
                                {
                                    $data_has_changed = true;
                                    $bean->{$field['mapping']['sugarcrm_name']} = encodeMultienumValue($group_items);
                                }
                            }
                            
                            break;

                        default:
                            // shouldn't get here yo!
                    }

                    break;

                case "Date":
                    // handle special case for date field
                    // transform sugar date field into mailchimp date format
                    SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean: date field type start');

                    if (empty($data[$field['mapping']['mailchimp_name']]))
                    {
                        // we can't do anything for an empty date field
                        // get out of here
                        SugarChimp_Helper::log('warning','SugarChimp_FieldMap::update_bean: could not update date field, value from mailchimp was empty');
                        break;
                    }

                    $date = $data[$field['mapping']['mailchimp_name']];
                    SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean: date: '.print_r($date,true));
                    SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean: sugarcrm_field_type: '.print_r($sugarcrm_field_type,true));

                    // if the sugarcrm field type is not date or datetime
                    // we are just going to return the value
                    // it could be used by a custom text field where the customer is setting this manually
                    if ($sugarcrm_field_type != 'date' && $sugarcrm_field_type != 'datetime' && $sugarcrm_field_type != 'datetimecombo')
                    {
                        if(self::update_if_changed($bean->{$field['mapping']['sugarcrm_name']},$date))
                        {
                            $data_has_changed = true;
                            $bean->{$field['mapping']['sugarcrm_name']} = $date;
                        }
                        break;
                    }

                    // get the format from mailchimp
                    // mailchimp webhooks always return date date in YYYY-MM-DD format
                    $from_format = 'Y-m-d';
                    
                    // only need the if statement below if this ever changes to match the dateformat
                    // if ($field['param1'] == 'DD/MM/YYYY')
                    // {
                    //     $from_format = 'd/m/Y';
                    // } 
                    // else if ($field['param1'] == 'MM/DD/YYYY')
                    // {
                    //     $from_format = 'm/d/Y';
                    // }

                    SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean: from_format: '.print_r($from_format,true));

                    // get the format to sugar
                    $td = TimeDate::getInstance();
                    $to_format = false;
                    switch($sugarcrm_field_type)
                    {
                        case "date":
                            $to_format = $td->get_date_format();
                            break;

                        case "datetime":
                        case "datetimecombo":
                            $to_format = $td->get_date_time_format();
                            break;

                        default:
                            // if it's another type of field, save it in the format mailchimp saves it
                            if ($field['param1'] == 'DD/MM/YYYY')
                            {
                                $to_format = 'd/m/Y';
                            } 
                            else if ($field['param1'] == 'MM/DD/YYYY')
                            {
                                $to_format = 'm/d/Y';
                            }

                            break;
                    }
                    SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean: to_format: '.print_r($to_format,true));

                    $date_value = '';
                    if (!empty($from_format) && !empty($to_format) && !empty($date))
                    {
                        $date = DateTime::createFromFormat($from_format,$date);

                        if (method_exists($date,'format'))
                        {
                            $date_value = $date->format($to_format);
                        }
                        else
                        {
                            SugarChimp_Helper::log('fatal','SugarChimp_FieldMap::update_bean date is not a valid datetime object: '.print_r($date,true));
                        }
                    }
                    else
                    {
                        SugarChimp_Helper::log('fatal','SugarChimp_FieldMap::update_bean: date field had an empty from_format or to_format');
                    }
                    SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean: date_value: '.print_r($date_value,true));

                    if (!empty($date_value))
                    {
                        if(self::update_if_changed($bean->{$field['mapping']['sugarcrm_name']},$date_value))
                        {
                            $data_has_changed = true;
                            $bean->{$field['mapping']['sugarcrm_name']} = $date_value;
                        }
                    }
                    else
                    {
                        SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean: date_value was empty, not overwriting with empty value');
                    }

                    break;

                case "Basic":
                default: 

                    SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean SugarChimp field type is Basic');
                    // set the field with the data from mailchimp if it's non-empty
                    // set the field with the data from mailchimp if it's non-empty
                    if (isset($data[$field['mapping']['mailchimp_name']]) && trim($data[$field['mapping']['mailchimp_name']])!=='')
                    {
                        $updated_value = '';
                        if (empty($data[$field['mapping']['mailchimp_name']]))
                        {
                            $updated_value = '';
                        }
                        else
                        {
                            switch($sugarcrm_field_type)
                            {
                                case "enum":
                                    SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean Sugar field type is enum');
                                    $list_name = $bean->field_defs[$field['mapping']['sugarcrm_name']]['options'];
                                    $values = array(html_entity_decode($data[$field['mapping']['mailchimp_name']]));
                                    // dropdowns can only have one value, put first entry of the array in bean value (should only have one anyways)
                                    $labels = self::get_enum_keys($list_name,$values);
                                    reset($labels);
                                    $updated_value = current($labels);
                                    break;
                                
                                default: // text, everything else
                                    SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean Sugar field type is default');
                                    $updated_value = empty($data[$field['mapping']['mailchimp_name']]) ? '' : $data[$field['mapping']['mailchimp_name']];
                                    break;
                            }
                        }
                        
                        SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean updated value: '.print_r($updated_value,true));
                        
                        if(self::update_if_changed($bean->{$field['mapping']['sugarcrm_name']},$updated_value))
                        {
                            $data_has_changed = true;
                            $bean->{$field['mapping']['sugarcrm_name']} = $updated_value;
                        }
                    }
                    else
                    {
                        SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean Sugar field is empty from MailChimp');
                        // if it's empty on the mailchimp side, but something exists in sugar, queue it up to send data to sugar
                        // only do this check if the data provided from mailchimp has a provided entry (ex. if no value is provided for 'my_field' don't do this check)
                        // mailchimp rating c is a special read-only field, don't send extra update on that one
                        // it will cuase circular loop if "via the api" is checked on the webhooks
                        if (isset($data[$field['mapping']['mailchimp_name']]) && $field['mapping']['mailchimp_name']!='mailchimp_rating_c' && isset($bean->{$field['mapping']['sugarcrm_name']}) && trim($bean->{$field['mapping']['sugarcrm_name']})!=='')
                        {
                            SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean Sugar set sugar has data mailchimp does not flag');
                            // set the flag to true so we can sync the data to mailchimp
                            $sugar_has_data_mailchimp_doesnt = true;
                        }
                    }

                    break;
            }
        }

        if(!$data_has_changed)
        {
            SugarChimp_Helper::log('debug',"SugarChimp_FieldMap::update_bean no data updated, not saving bean.");
            return true;
        }

        SugarChimp_Helper::log('debug','SugarChimp_FieldMap::update_bean before save bean');

        $save = $bean->save();
        
        SugarChimp_Helper::log('debug',"SugarChimp_FieldMap::update_bean after save bean");
        
        if (empty($save))
        {
            SugarChimp_Helper::log('fatal',"SugarChimp_FieldMap::update_bean after save bean failed.");
            return false;
        }

        return true;
    }
    
    public static function get_enum_options($module_object_name,$field_name)
    {
        if (empty($module_object_name) || empty($field_name))
        {
            return false;
        }

        global $app_list_strings;
        $module_bean = BeanFactory::newBean($module_object_name);

        if (empty($app_list_strings[$module_bean->field_defs[$field_name]['options']]) or !is_array($app_list_strings[$module_bean->field_defs[$field_name]['options']]))
        {
            SugarChimp_Helper::log('warning',"SugarChimp_FieldMap::get_enum_options could not find dropdown options for {$module_object_name} {$field_name}");
            return false;
        }

        return array(
            'name' => $module_bean->field_defs[$field_name]['options'],
            'options' => $app_list_strings[$module_bean->field_defs[$field_name]['options']],
        );
    }

    public static function get_enum_keys($list_name,$values)
    {
        if (empty($list_name) or empty($values) or !is_array($values))
        {
            SugarChimp_Helper::log('warning',"SugarChimp_FieldMap::get_enum_keys bad data passed in list_name: ".print_r($list_name,true). " values: ".print_r($values,true));
            return false;
        }

        global $app_list_strings;

        if (empty($app_list_strings[$list_name]))
        {
            // list doesn't exist
            SugarChimp_Helper::log('warning',"SugarChimp_FieldMap::get_enum_keys could not find a list by the name ".print_r($list_name,true));
            return false;
        }

        $keys = array();

        foreach ($values as $value)
        {
            // if a vlue doesn't exist, give the key and throw a warning
            $value_key = array_search($value,$app_list_strings[$list_name]);
            if ($value_key === false)
            {
                SugarChimp_Helper::log('warning',"SugarChimp_FieldMap::get_enum_keys could not find key for value in list ".$list_name.": ".print_r($value,true));
                $keys []= $value;
            }
            else
            {
                $keys []= $value_key;
            }
        }

        return $keys;
    }

    public static function get_enum_values($list_name,$keys)
    {
        if (empty($list_name) or empty($keys) or !is_array($keys))
        {
            SugarChimp_Helper::log('warning',"SugarChimp_FieldMap::get_enum_values bad data passed in list_name: ".print_r($list_name,true). " keys: ".print_r($keys,true));
            return false;
        }

        global $app_list_strings;

        if (empty($app_list_strings[$list_name]))
        {
            // list doesn't exist
            SugarChimp_Helper::log('warning',"SugarChimp_FieldMap::get_enum_values could not find a list by the name ".print_r($list_name,true));
            return false;
        }

        $values = array();

        foreach ($keys as $key)
        {
            // if a vlue doesn't exist, give the key and throw a warning
            if (!isset($app_list_strings[$list_name][$key]))
            {
                SugarChimp_Helper::log('warning',"SugarChimp_FieldMap::get_enum_values could not find value for key ".print_r($key,true));
                $values[$key] = $key;
            }
            else
            {
                $values[$key] = $app_list_strings[$list_name][$key];
            }
        }

        return $values;
    }

    // Used to prepare the data in the Sugar to MailChimp Direction
    // take a Sugar bean, based on the field mapping, prepare the data needed to update related subscribers in Mailchimp
    public static function get_merge_var_update_array_from_bean($list_id,$bean,$include_groupings=false)
    {
        //need to check Sugar version for date syncing
        global $sugar_version;

        if (empty($bean) || !$bean instanceOf SugarBean)
        {
            SugarChimp_Helper::log('debug','SugarChimp_FieldMap::get_merge_var_update_array_from_bean a bean is empty');
            return false;
        }
        
        $mapping = self::get_field_mappings($list_id,$bean->module_dir);
        
        if (empty($mapping))
        {
            SugarChimp_Helper::log('debug','SugarChimp_FieldMap::get_merge_var_update_array_from_bean a mapping could not be loaded for module: '.$bean->module_dir);
            return false;
        }
        
        $data = array();

        // not needed for v3 api 
        // if ($include_groupings === true)
        // {
        //     $data['groupings'] = array();
        // }
        
        foreach ($mapping['fields'] as $key => $field)
        {
            if (empty($field['mapping']['sugarcrm_name']) || empty($field['mapping']['mailchimp_name']))
            {
                // the required mapping fields are not set
                SugarChimp_Helper::log('warning','SugarChimp_FieldMap::get_merge_var_update_array_from_bean the sugarcrm_name and mailchimp_name mapping fields are not provided in the mapping');
                continue;
            }
            
            // since we support mapping parent related fields, we need to be able to 
            // get the data form the parent related fields to send to MC
            // if it's a related field, set the current bean to the parent module
            $current_bean = $bean;
            $current_field = $field['mapping']['sugarcrm_name'];

            if (SugarChimp_Field::is_related_field($field['mapping']['sugarcrm_name']))
            {
                // make sure current SC subscription supports parent related field mapping
                if (self::is_related_field_mapping_supported() !== true)
                {
                    SugarChimp_Helper::log('debug','SugarChimp_FieldMap::get_merge_var_update_array_from_bean: current plan does not support related parent field mapping');
                    continue;
                }

                SugarChimp_Logger::log('debug','SugarChimp_FieldMap get_merge_var_update_array_from_bean this is a related field, get bean context for field: '.print_r($field['mapping']['sugarcrm_name'],true));
                $field_parts = SugarChimp_Field::get_related_field_parts($field['mapping']['sugarcrm_name']);
                
                if (empty($field_parts['link_field_name']))
                {
                    SugarChimp_Logger::log('fatal','SugarChimp_FieldMap get_merge_var_update_array_from_bean link_field_name is empty. Cannot sync related field: '.print_r($field['mapping']['sugarcrm_name'],true));
                    continue;
                }

                if (empty($field_parts['parent_field_name']))
                {
                    SugarChimp_Logger::log('fatal','SugarChimp_FieldMap get_merge_var_update_array_from_bean parent_field_name is empty. Cannot sync related field: '.print_r($field['mapping']['sugarcrm_name'],true));
                    continue;
                }

                $link_field_name = $field_parts['link_field_name'];
                $parent_field_name = $field_parts['parent_field_name'];

                if (!method_exists($bean, 'load_relationship'))
                {
                    // load relationship method does not exist on bean
                    SugarChimp_Logger::log('fatal','SugarChimp_FieldMap get_merge_var_update_array_from_bean load relationship method does not exit on bean');
                    SugarChimp_Logger::log('fatal','SugarChimp_FieldMap get_merge_var_update_array_from_bean Cannot sync related field: '.print_r($field['mapping']['sugarcrm_name'],true));
                    continue;
                }

                if (!$bean->load_relationship($link_field_name))
                {
                    // failed to load relationship
                    SugarChimp_Logger::log('fatal','SugarChimp_FieldMap get_merge_var_update_array_from_bean load relationship could not be called on link_field_name: '.print_r($link_field_name,true));
                    SugarChimp_Logger::log('fatal','SugarChimp_FieldMap get_merge_var_update_array_from_bean Cannot sync related field: '.print_r($field['mapping']['sugarcrm_name'],true));
                    continue;
                }

                if (!method_exists($bean->$link_field_name, 'getBeans'))
                {
                    // get beans method does not exist on bean
                    SugarChimp_Logger::log('fatal','SugarChimp_FieldMap get_merge_var_update_array_from_bean getBeans method does not exist on bean');
                    SugarChimp_Logger::log('fatal','SugarChimp_FieldMap get_merge_var_update_array_from_bean Cannot sync related field: '.print_r($field['mapping']['sugarcrm_name'],true));
                    continue;
                }

                $related_beans = $bean->$link_field_name->getBeans();

                if (empty($related_beans))
                {
                    SugarChimp_Logger::log('debug','SugarChimp_FieldMap get_merge_var_update_array_from_bean there is no parent related record, skipping. field: '.print_r($field['mapping']['sugarcrm_name'],true));
                    continue;
                }

                // if we found data, grab the first record in the array
                // since we're dealing with parent records, there should ony ever be one record
                // use the reset/current trick
                reset($related_beans);
                $current_bean = current($related_beans);
                $current_field = $parent_field_name;

                // if for some reason it's a bad value, kick it out here
                if (empty($current_bean))
                {
                    SugarChimp_Logger::log('fatal','SugarChimp_FieldMap get_merge_var_update_array_from_bean current bean is empty for an unknown reason');
                    SugarChimp_Logger::log('fatal','SugarChimp_FieldMap get_merge_var_update_array_from_bean Cannot sync related field: '.print_r($field['mapping']['sugarcrm_name'],true));
                    continue;
                }
            }

            // initialize value
            $data[$field['mapping']['mailchimp_name']] = "";
            
            // todo: the new v3 way of handling the interests will do weird things if you 
            //       have a mailchimp field that is named "interests", I think... 
            //       would need to pull out the interest gather code below and pull it separately 

            // check if member variable is on the bean, it can be empty, it just needs to exist
            if (isset($current_bean->$current_field))
            {
                $sugarcrm_field_type = self::get_sugarcrm_field_type($current_bean,$current_field);
                
                switch($field['type'])
                {
                    case "Group":

                        // with the v3 api, we do not need the extra field here
                        // the groups will be passed in the interest array below
                        unset($data[$field['mapping']['mailchimp_name']]);

                        // if we're not including groups, there's nothing to do
                        if ($include_groupings === true)
                        {
                            // dropdowns (enum) and multiselects (multienum) are the two supported field types for groups
                            // each need to be handled differently 

                            $group_items = array();

                            switch ($sugarcrm_field_type)
                            {
                                case "enum":
                                    // get the current value from the bean
                                    // grab the label from the options to send to MC
                                    $list_name = $current_bean->field_defs[$current_field]['options'];
                                    $keys = array($current_bean->$current_field);
                                    $group_items = self::get_enum_values($list_name,$keys);
                                    
                                    break;

                                case "multienum":
                                    // get the current value from the bean
                                    // split into individual selections
                                    // get the label for each from the options to send to MC
                                    $list_name = $current_bean->field_defs[$current_field]['options'];
                                    $keys = unencodeMultienum($current_bean->$current_field);
                                    $group_items = self::get_enum_values($list_name,$keys);

                                    break;

                                default:
                                    // shouldn't get here yo!
                            }

                            if (empty($field['param1']))
                            {
                                SugarChimp_Logger::log('fatal','SugarChimp_FieldMap::get_merge_var_update_array_from_bean: the group does not have a v2/v3 group mapping data, cannot continue for field: '.print_r($field,true));
                                break;
                            }

                            if (empty($data['interests']))
                            {
                                $data['interests'] = array();
                            }

                            // param1 is array of mc v3 id and v2 group names
                            // 25f245g25 => Red
                            // 88w0f8y20 => White
                            // 828vwn03q => Blue
                            // we need to turn the id => name array to 
                            // true/false if the name is selected
                            foreach ($field['param1'] as $mc_id => $group_name)
                            {
                                if (in_array($group_name, $group_items))
                                {
                                    $data['interests'][$mc_id] = true;
                                }
                                else
                                {
                                    $data['interests'][$mc_id] = false;
                                }
                            }

                            SugarChimp_Logger::log('debug','SugarChimp_FieldMap::get_merge_var_update_array_from_bean group_item_mapping: '.print_r($data['interests'],true));
                        }

                        break;

                    case "Date":
                        // handle special case for date field
                        // transform sugar date field into mailchimp date format
                        SugarChimp_Helper::log('debug','SugarChimp_FieldMap::get_merge_var_update_array_from_bean: date field type start');
                        SugarChimp_Helper::log('debug','SugarChimp_FieldMap::get_merge_var_update_array_from_bean: field: '.print_r($field,true));
                        SugarChimp_Helper::log('debug','SugarChimp_FieldMap::get_merge_var_update_array_from_bean: date: '.print_r($current_bean->$current_field,true));

                        // if the sugarcrm field type is not date or datetime
                        // we are just going to return the value
                        // it could be used by a custom text field where the customer is setting this manually
                        if ($sugarcrm_field_type != 'date' && $sugarcrm_field_type != 'datetime' && $sugarcrm_field_type != 'datetimecombo')
                        {
                            $data[$field['mapping']['mailchimp_name']] = $current_bean->$current_field;
                            break;
                        }

                        // get the format from sugarcrm
                        $td = TimeDate::getInstance();
                        $from_format = false;
                        switch($sugarcrm_field_type)
                        {
                            case "date":
                                // 8/29/2018
                                // Sugar8 always gives us the database format
                                // hardcoded for now
                                if(preg_match( "/^8.*/", $sugar_version))
                                {
                                    $from_format = 'Y-m-d';
                                }
                                else
                                {
                                    $from_format = $td->get_date_format();
                                }
                                
                                break;

                            case "datetime":
                            case "datetimecombo":
                                // 8/29/2018
                                // Sugar8 always gives us the database format
                                // hardcoded for now
                                if(preg_match( "/^8.*/", $sugar_version))
                                {
                                    $from_format = 'Y-m-d H:i:s';
                                }
                                else
                                {
                                    $from_format = $td->get_date_time_format();
                                }
                                
                                break;
                        }

                        SugarChimp_Helper::log('debug','SugarChimp_FieldMap::get_merge_var_update_array_from_bean: from_format: '.print_r($from_format,true));

                        // mailchimp always wants to receive the data in m/d/Y
                        // get the format to mailchimp
                        $to_format = 'm/d/Y';
                        
                        // if this ever changes and they respect the format saved with their list
                        // use the if statement below
                        // if ($field['param1'] == 'DD/MM/YYYY')
                        // {
                        //     $to_format = 'd/m/Y';
                        // } 
                        // else if ($field['param1'] == 'MM/DD/YYYY')
                        // {
                        //     $to_format = 'm/d/Y';
                        // }

                        SugarChimp_Helper::log('debug','SugarChimp_FieldMap::get_merge_var_update_array_from_bean: to_format: '.print_r($to_format,true));

                        $date_value = '';
                        if (!empty($from_format) && !empty($to_format) && !empty($current_bean->$current_field))
                        {
                            $date = DateTime::createFromFormat($from_format,$current_bean->$current_field);
                            if (method_exists($date,'format'))
                            {
                                // make sure method exists before running the function
                                // if it's bad data, it may throw an error here and the queue get stuck
                                $date_value = $date->format($to_format);
                                SugarChimp_Helper::log('debug','SugarChimp_FieldMap::get_merge_var_update_array_from_bean: formatted to date value: '.print_r($date_value,true));

                            }
                            else
                            {
                                SugarChimp_Helper::log('fatal','SugarChimp_FieldMap::get_merge_var_update_array_from_bean date is not a valid datetime object: '.print_r($date,true));
                            }
                        }
                        else
                        {
                            SugarChimp_Helper::log('debug','SugarChimp_FieldMap::get_merge_var_update_array_from_bean: date field had an empty from_format or to_format');
                        }


                        $data[$field['mapping']['mailchimp_name']] = $date_value;

                        break;

                    case "Basic":
                    default:

                        // for all other fields, do what we always do
                        $updated_value = '';
                        switch($sugarcrm_field_type)
                        {
                            case "enum":
                                // get display label for dropdown value and send that to MC
                                global $app_list_strings;
                                if (empty($app_list_strings[$current_bean->field_defs[$current_field]['options']][$current_bean->$current_field]))
                                {
                                    $updated_value = $current_bean->$current_field;
                                }
                                else
                                {
                                    $updated_value = $app_list_strings[$current_bean->field_defs[$current_field]['options']][$current_bean->$current_field];
                                }
                                break;
                                
                            default: // text, everything else
                                $updated_value = $current_bean->$current_field;
                                break;
                        }
                        
                        // mailchimp will not decode the values, need to do that for them
                        $data[$field['mapping']['mailchimp_name']] = html_entity_decode($updated_value,ENT_QUOTES);
                        break;
                }
            }
        }
        
        return $data;
    }
    
    public static function get_sugarcrm_field_type($bean,$field)
    {
        SugarChimp_Helper::log('debug','SugarChimp_FieldMap::get_sugarcrm_field_type start');

        if (empty($bean) || !$bean instanceOf SugarBean)
        {
            SugarChimp_Helper::log('warning','SugarChimp_FieldMap::get_sugarcrm_field_type bean is empty');
            return false;
        }
        
        if (empty($field))
        {
            SugarChimp_Helper::log('warning','SugarChimp_FieldMap::get_sugarcrm_field_type field is empty, should be the field name');
            return false;
        }
        
        if (empty($bean->field_defs[$field]) || empty($bean->field_defs[$field]['type']))
        {
            SugarChimp_Helper::log('warning','SugarChimp_FieldMap::get_sugarcrm_field_type '.$field.' is not in field defs or type is not set.');
            return false;
        }

        SugarChimp_Helper::log('debug','SugarChimp_FieldMap::get_sugarcrm_field_type end');
        return $bean->field_defs[$field]['type'];
    }
    
    // return array of supported modules, accounts module not supported by default
    // if SugarChimp account_syncing_enabled setting is true, Accounts will be added to the array
    public static function get_supported_modules()
    {
        $supported_modules = static::$supported_modules;

        // check if account syncing is enabled
        $account_syncing_enabled = SugarChimp_Setting::retrieve('account_syncing_enabled');

        // if it is enabled, add it to the supported modules array
        if (!empty($account_syncing_enabled))
        {
            $supported_modules []= "Accounts";
        }

        return $supported_modules;
    }
    
    public static function get_default_module()
    {
        return self::$default_module;
    }
    
    public static function get_mc_list_merge_fields($list_id,$clear_cache=false)
    {
        if (empty($list_id))
        {
            SugarChimp_Helper::log('warning','SugarChimp_FieldMap::get_mc_list_merge_fields list_id is empty');
            return false;
        }
        
        // check cached_merge_vars cache first, will be expensive to query mailchimp each time
        // if we find a match, return it
        if (!empty(self::$cached_merge_vars[$list_id]) && $clear_cache!==true)
        {
            return self::$cached_merge_vars[$list_id];
        }

        require_once('modules/SugarChimp/includes/vendor/Mailchimp/Mailchimp.php');
        
        $apikey = SugarChimp_Setting::retrieve('apikey');

        if(empty($apikey)) 
        {
            SugarChimp_Helper::log('warning','SugarChimp_FieldMap::get_mc_list_merge_fields could not get mailchimp apikey');
            return false;
        }

        try 
        {
            // $mc = new MailChimp($apikey);
            // $result = $mc->lists->mergeVars(array($list_id));
            $result = Mailchimp_API::get_merge_fields($list_id);
        } 
        catch (Exception $e)
        {
            SugarChimp_Helper::log('warning','SugarChimp_FieldMap::get_mc_list_merge_fields could not get merge fields from MailChimp: '.$e->getMessage());
            return false;
        }
        
        if (!empty($result['errors']) || empty($result['merge_fields']))
        {
            SugarChimp_Helper::log('warning','SugarChimp_FieldMap::get_mc_list_merge_fields successful response, but MailChimp returned errors: '.print_r($result['errors'],true));
            return false;
        }
        
        // set cache
        self::$cached_merge_vars[$list_id] = $result['merge_fields'];

        return self::$cached_merge_vars[$list_id];
    }

    public static function get_mc_list_group_fields($list_id)
    {
        if (empty($list_id))
        {
            SugarChimp_Helper::log('warning','SugarChimp_FieldMap::get_mc_list_group_fields list_id is empty');
            return false;
        }

        require_once('modules/SugarChimp/includes/vendor/Mailchimp/Mailchimp.php');

        $apikey = SugarChimp_Setting::retrieve('apikey');

        if (empty($apikey))
        {
            SugarChimp_Helper::log('warning','SugarChimp_FieldMap::get_mc_list_group_fields could not get mailchimp apikey');
            return false;
        }

        try
        {
            // $mc = new MailChimp($apikey);
            // $result = $mc->lists->interestGroupings($list_id);
            $result = Mailchimp_API::get_interest_categories($list_id);
        }
        catch (Exception $e)
        {
            // if groups are not used on a mailchimp list a Mailchimp_List_InvalidOption exception is thrown
            // a simple return false is all that is needed, but could move it to its own catch block if needed later
            SugarChimp_Helper::log('warning','SugarChimp_FieldMap::get_mc_list_group_fields could not get group fields from MailChimp: '.$e->getMessage());
            return false;
        }

        if (!empty($result['error']))
        {
            SugarChimp_Helper::log('warning','SugarChimp_FieldMap::get_mc_list_group_fields successful response, but MailChimp returned errors: '.print_r($result,true));
            return false;
        }

        return $result;
    }
    
    // get the available sugarcrm fields that are mappable to MC fields
    // $field_type_filter (optional) - array of sugar field types to include (used for groupings to only select dropdowns/multiselects)
    // ignore_email1 - dont return ->email1 if it exists, defaults to true
    public static function get_available_module_fields($module,$field_type_filter=array(),$ignore_email1=true)
    {
        if (empty($module))
        {
            SugarChimp_Helper::log('warning','SugarChimp_FieldMap::get_available_module_fields module is empty');
        }
        
        $bean = BeanFactory::newBean($module);
        $fieldsData = array();

        // do we need to filter on types?
        $check_type = false;
        if (!empty($field_type_filter))
        {
            $check_type = true;
        }
        
        if (!empty($bean->field_defs))
        {
            foreach($bean->field_defs as $def) 
            {
                if ($ignore_email1 === true && $def['name'] == 'email1') continue;

                if ($check_type === true && !empty($def['type']) && !in_array($def['type'],$field_type_filter)) continue;
                
                $fieldsData[$def['name']] = $def;
                $fieldsData[$def['name']]['label'] = empty($def['vname']) ? $def['name'] : translate($def['vname'], $module);
            }
        }
        else
        {
            SugarChimp_Helper::log('warning','SugarChimp_FieldMap::get_available_module_fields there are no available fields for module '.$module);
        }
        
        // order by field name
        ksort($fieldsData);
        
        return $fieldsData;
    }

    // simple function to check if parent related field mapping is supported by active SC subscription
    public static function is_related_field_mapping_supported()
    {
        $current_plan = SugarChimp_Helper::get_current_plan();

        if ($current_plan == 'ultimate')
        {
            return true;
        }
        else
        {
            // too noisy for this sometimes
            // SugarChimp_Helper::log('debug','SugarChimp_FieldMap::is_related_field_mapping_supported: current plan does not support related parent field mapping: '.print_r($current_plan,true));
            return false;
        }
    }

    // added in 7.9.7 to increase performance on bean updates from mailchimp
    // check bean updates to see if bean needs to be saved or not
    // if no data changes, bean will not save and trigger Sugar web logic_hooks
    public static function update_if_changed($current_val, $new_val)
    {
        // empty new_val
        if(empty($new_val))
        {
            //both empty, no changes
            if(empty($current_val)){
                SugarChimp_Helper::log('debug',"SugarChimp_FieldMap::update_if_changed values are the same, do not update bean.");
                return false;
            }
            
            // empty new_val, not empty current_val   
            SugarChimp_Helper::log('debug',"SugarChimp_FieldMap::update_if_changed value has been removed, update bean.");
            return true;
        }
        // new_val has data, current_val does not
        else if(empty($current_val))
        {
            SugarChimp_Helper::log('debug',"SugarChimp_FieldMap::update_if_changed values are different, update bean.");
            return true;
        }
        // both have data, check for changes
        else
        {
            if($new_val == $current_val)
            {
                SugarChimp_Helper::log('debug',"SugarChimp_FieldMap::update_if_changed values are the same, do not update bean.");
                return false;
            }

            SugarChimp_Helper::log('debug',"SugarChimp_FieldMap::update_if_changed values are different, update bean.");

            return true;
        }


    }
}