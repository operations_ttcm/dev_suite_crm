<?php







require_once('modules/SmartList/includes/classes/SmartList/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/FieldMap.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Logger.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/List.php');
require_once('modules/SugarChimp/includes/classes/Mailchimp/API.php');
require_once('modules/SugarChimp/license/OutfittersLicense.php');

class SugarChimp_Helper
{
    // if bean id is in this array, webhooks for sugarchimp will not be run for it
    public static $beans_with_logic_hooks_disabled = array();
    public static $current_plan = false;
    public static $plans = array(
        'basic',
        'professional',
        'ultimate',
    );

    // an abstracted log method SugarChimp module will use
    // hope to be able to independently set the sugarchimp log
    public static function log($level,$message)
    {
        SugarChimp_Logger::log($level,$message, false);
    }

    // find the number of records that have the same email address
    // $email is the email address to search by
    // $list_ids - default: array() - you can filter the search by those emails that have are attached to a specific mailchimp list
    // $return_beans - default: true - if true, return the found bean objects, if false, return array of id => module pairs
    public static function find_beans_by_email($email,$list_ids=array(),$return_beans=true, $default_module=false)
    {
        if (empty($email))
        {
            self::log('debug','SugarChimp_Helper::find_beans_by_email the email is empty');
            return false;
        }
        
        global $db;
        
        $sql = "";
        $list_sql_joins = "";

        $account_syncing_enabled = SugarChimp_Setting::retrieve('account_syncing_enabled');

        // todo: if we want to allow custom module syncing we'll need to do some work here to properly join/where the custom module table
        // 
        // supported_module_sql_joins and supported_module_sql_wheres are needed to make sure
        //  we only pull supported modules and we make sure those records we're pulling are not deleted records
        if (empty($account_syncing_enabled))
        {
            // if account syncing IS NOT enabled
            $supported_module_sql_joins = " LEFT JOIN contacts c ON eabr.bean_id=c.id
                                            LEFT JOIN prospects p ON eabr.bean_id=p.id
                                            LEFT JOIN leads l ON eabr.bean_id=l.id ";

            $supported_module_sql_wheres = " AND (
                                                (eabr.bean_module='Contacts' AND c.deleted=0) OR 
                                                (eabr.bean_module='Prospects' AND p.deleted=0) OR 
                                                (eabr.bean_module='Leads' AND l.deleted=0)
                                            ) ";
        }
        else
        {
            // if account syncing IS enabled
            $supported_module_sql_joins = " LEFT JOIN contacts c ON eabr.bean_id=c.id
                                            LEFT JOIN prospects p ON eabr.bean_id=p.id
                                            LEFT JOIN leads l ON eabr.bean_id=l.id 
                                            LEFT JOIN accounts a ON eabr.bean_id=a.id ";

            $supported_module_sql_wheres = " AND (
                                                (eabr.bean_module='Contacts' AND c.deleted=0) OR 
                                                (eabr.bean_module='Prospects' AND p.deleted=0) OR 
                                                (eabr.bean_module='Leads' AND l.deleted=0) OR 
                                                (eabr.bean_module='Accounts' AND a.deleted=0)
                                            ) ";
        }

        if (!empty($list_ids))
        {
            // if we need to filter on lists
            
            $list_sql_joins = " INNER JOIN prospect_lists pl ON plp.prospect_list_id=pl.id AND pl.deleted=0 ";
            if (is_array($list_ids) && !empty($list_ids))
            {
                foreach ($list_ids as $key => $value)
                {
                    $list_ids[$key] = $db->quote($value);
                }
                $list_sql_joins .= " AND pl.mailchimp_list_name_c IN ('" . implode("','",$list_ids) . "') ";
            }
            else if (!is_array($list_ids) && !empty($list_ids))
            {
                $list_sql_joins .= " AND pl.mailchimp_list_name_c='".$db->quote($list_ids)."' ";
            }
            
            $sql = "SELECT DISTINCT plp.related_id AS id, plp.related_type AS module
                    FROM prospect_lists_prospects plp
                    INNER JOIN email_addr_bean_rel eabr ON eabr.bean_id=plp.related_id AND eabr.deleted=0 AND eabr.primary_address=1
                    INNER JOIN email_addresses ea ON ea.id=eabr.email_address_id AND ea.deleted=0 AND ea.email_address LIKE '".$db->quote($email)."'
                    {$list_sql_joins}
                    {$supported_module_sql_joins}
                    WHERE plp.deleted=0 
                    {$supported_module_sql_wheres}";
        }
        else
        {
            // we don't want to filter on lists, so give us any person in the system that matches the email
            $sql = "SELECT DISTINCT eabr.bean_id AS id, eabr.bean_module AS module
                    FROM email_addr_bean_rel eabr
                    INNER JOIN email_addresses ea ON ea.id=eabr.email_address_id AND ea.deleted=0 AND ea.email_address LIKE '".$db->quote($email)."'
                    {$supported_module_sql_joins}
                    WHERE eabr.deleted=0 AND eabr.primary_address=1
                    {$supported_module_sql_wheres}";
        }

        self::log('debug','SugarChimp_Helper::find_beans_by_email sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            self::log('fatal','SugarChimp_Helper::find_beans_by_email sql query failed. sql: '.$sql);
            return false;
        }

        $bean_ids = array();

        while ($row = $db->fetchByAssoc($result))
        {
            $bean_ids[$row['id']] = $row['module'];
        }

        self::log('debug','SugarChimp_Helper::find_beans_by_email bean_ids: '.print_r($bean_ids,true));

        if (empty($bean_ids))
        {
            return false;
        }

        // if we aren't returning beans, we have everything we need
        if ($return_beans === false)
        {
            return $bean_ids;
        }

        $beans = array();
        foreach ($bean_ids as $id => $module)
        {
            $bean = BeanFactory::getBean($module,$id);
            if (empty($bean))
            {
                continue;
            }

            $beans[$id] = $bean;
        }

        return $beans;
    }
    
    // return array of target list mailchimp_list_name_c ids that are synced with mailchimp
    public static function get_lists()
    {
        // todo: lookup the email fields from the field mapping, doing this would enable to support for custom email address fields
        global $db;
        
        $list_ids = array();

        $sql = "SELECT DISTINCT pl.mailchimp_list_name_c as mailchimp_list_id
                FROM prospect_lists pl
                WHERE 
                    pl.deleted=0 AND
                    pl.mailchimp_list_name_c IS NOT NULL AND 
                    pl.mailchimp_list_name_c != ''";

        self::log('debug','SugarChimp_Helper::get_lists sql: '.$sql);

        $result = $db->query($sql);
        while ($row = $db->fetchByAssoc($result))
        {
            $list_ids []= $row['mailchimp_list_id'];
        }

        if (empty($list_ids))
        {
            self::log('debug','SugarChimp_Helper::get_lists there are no target lists synced to mailchimp lists');
            // if it's empty, then nothing needs to be checked
            return false;
        }
        
        self::log('debug','SugarChimp_Helper::get_lists found '.count($list_ids).' synced lists with ids: '.print_r($list_ids,true));

        return $list_ids;
    }

    // get array of synced lists
    // Formatted for the bottom of Health Status page
    public static function get_synced_lists()
    {
        // todo: lookup the email fields from the field mapping, doing this would enable to support for custom email address fields
        global $db;
        
        $lists = array();

        $sql = "SELECT pl.id as id, pl.name as name, pl.mailchimp_list_name_c as mailchimp_list_id
                FROM prospect_lists pl
                WHERE 
                    pl.deleted=0 AND
                    pl.mailchimp_list_name_c IS NOT NULL AND 
                    pl.mailchimp_list_name_c != ''";

        self::log('debug','SugarChimp_Helper::get_synced_lists sql: '.$sql);

        $result = $db->query($sql);
        while ($row = $db->fetchByAssoc($result))
        {
            $lists []= array(
                'id' => $row['id'],
                'name' => $row['name'],
                'mailchimp_list_id' => $row['mailchimp_list_id'],
            );
        }

        if (empty($lists))
        {
            self::log('debug','SugarChimp_Helper::get_synced_lists there are no target lists synced to mailchimp lists');
            // if it's empty, then nothing needs to be checked
            return false;
        }
        
        self::log('debug','SugarChimp_Helper::get_synced_lists found '.count($lists).' synced lists: '.print_r($lists,true));

        return $lists;
    }

    // provided an email address
    // return array of mc list ids that email address is associated with
    // if $match_to_sugar_lists is true, it will also return the associated sugar target list ids
    //      key => value : mc_list_id => sugar_target_list_id
    // if $match_to_sugar_lists is false, it just returns array of mc list ids
    public static function get_mc_lists_for_email($email,$match_to_sugar_lists=true)
    {
        if (empty($email))
        {
            // an email address is required
            self::log('debug','SugarChimp_Helper::get_mc_lists_for_email email address is required');
            return false;
        }
        
        $apikey = SugarChimp_Setting::retrieve('apikey');
        
        if (empty($apikey))
        {
            self::log('debug','SugarChimp_Helper::get_mc_lists_for_email apikey was empty');
            return false;
        }
        
        try
        {
            $mc = new Mailchimp($apikey);
            $response = $mc->helper->searchMembers($email);
        }
        catch (Exception $e)
        {
            self::log('fatal','SugarChimp_Helper::get_mc_lists_for_email Could not search for members by email '.$email.': '.$e->getMessage());
            return false;
        }
        
        $list_ids = array();
        if (!empty($response['exact_matches']['members']) && is_array($response['exact_matches']['members']))
        {
            foreach ($response['exact_matches']['members'] as $member)
            {
                if (!empty($member['list_id']))
                {
                    if (empty($list_ids[$member['list_id']])) $list_ids[$member['list_id']] = array();
                    $list_ids[$member['list_id']]['id'] = $member['list_id'];
                    $list_ids[$member['list_id']]['status'] = $member['status'];
                    $list_ids[$member['list_id']]['name'] = $member['list_name'];
                }
                if (!empty($member['lists']) && is_array($member['lists']))
                {
                    foreach ($member['lists'] as $list)
                    {
                        if (empty($list_ids[$list['id']])) $list_ids[$list['id']] = array();
                        $list_ids[$list['id']]['id'] = $list['id'];
                        $list_ids[$list['id']]['status'] = $list['status'];
                    }
                }
            }
        }

        $unique_list_ids = array_unique(array_keys($list_ids));

        if ($match_to_sugar_lists === true && !empty($unique_list_ids))
        {
            global $db;
            
            foreach ($unique_list_ids as $key => $list_id)
            {
                $list_ids[$list_id]['sugar_target_list'] = false;
                
                $sql = "SELECT pl.id as list_id
                        FROM prospect_lists pl
                        WHERE 
                            pl.deleted=0 AND
                            pl.mailchimp_list_name_c = '".$db->quote($list_id)."'";
                $result = $db->query($sql);
                $row = $db->fetchByAssoc($result);
                
                if (!empty($row['list_id']))
                {
                    $list_ids[$list_id]['sugar_target_list'] = $row['list_id'];
                }
            }
        }
        
        return $list_ids;
    }

    public static function get_related_people_for_account($id, $include_account = false)
    {
        $relatedBeans = array();
        $links = array('contacts','prospects','leads');
        $bean = BeanFactory::getBean('Accounts',$id);
        if(empty($bean))
        {
            self::log('warning','SugarChimp_Helper::get_related_people_for_account: account_id ('. $id . ') not found.');
            return false;
        }

        foreach ($links as $link)
        {
            //If Account has related contacts, add them to relatedBeans
            if ($bean->load_relationship($link))
            {
                //Fetch related beans
                $linkBeans = $bean->$link->getBeans();
                foreach($linkBeans as $bean_id => $bean_data)
                {
                    $relatedBeans[$bean_id] = $bean_data;
                }
            }
        }

        if($include_account === true)
        {
            $relatedBeans[$id] = $bean;
        }

        return $relatedBeans;
    }

    // return array of target list mailchimp_list_name_c ids that are synced with mailchimp
    // $id param is the id of the person record to match against
    // NOTE: this does not check if the person record is deleted, if the person is deleted but the prospect lists and related records are not, it will return list ids
    public static function get_lists_for_person($id)
    {
        if (empty($id))
        {
            // a valid record $id is required
            return false;
        }
        
        self::log('debug','SugarChimp_Helper::get_lists_for_person id: '.$id);
        
        global $db;
        
        $list_ids = array();

        $sql = "SELECT DISTINCT pl.mailchimp_list_name_c as mailchimp_list_id
                FROM prospect_lists pl
                INNER JOIN prospect_lists_prospects plp ON pl.id=plp.prospect_list_id AND plp.deleted=0 AND plp.related_id='".$db->quote($id)."'
                WHERE 
                    pl.deleted=0 AND
                    pl.mailchimp_list_name_c IS NOT NULL AND 
                    pl.mailchimp_list_name_c != ''";

        self::log('debug','SugarChimp_Helper::get_lists_for_person sql: '.$sql);

        $result = $db->query($sql);
        while ($row = $db->fetchByAssoc($result))
        {
            $list_ids []= $row['mailchimp_list_id'];
        }

        if (empty($list_ids))
        {
            self::log('debug','SugarChimp_Helper::get_lists_for_person there are no target lists synced to mailchimp lists');
            // if it's empty, then nothing needs to be checked
            return false;
        }
        
        self::log('debug','SugarChimp_Helper::get_lists_for_person found '.count($list_ids).' synced lists with ids: '.print_r($list_ids,true));

        return $list_ids;
    }

    // check to see if an email address related to this record is related to a target list that is synced to mailchimp
    public static function belongs_to_list($bean,$data=array())
    {
        // todo: lookup the email fields from the field mapping, doing this would enable to support for custom email address fields
        global $db;
        
        $list_ids = array();

        // look for mailchimp lists by old and new email address depending on what exists
        $email_address_exists = false;
        $sql_email_address = "";
        if (!empty($bean->email1) AND !empty($data['email1']) AND $bean->email1 != $data['email1'])
        {
            $email_address_exists = true;
            $sql_email_address = " AND (ea.email_address LIKE '".$db->quote($bean->email1)."' OR ea.email_address LIKE '".$db->quote($data['email1'])."') ";
        }
        else if (!empty($bean->email1))
        {
            $email_address_exists = true;
            $sql_email_address = " AND ea.email_address LIKE '".$db->quote($bean->email1)."' ";
        }
        else if (!empty($data['email1']))
        {
            $email_address_exists = true;
            $sql_email_address = " AND ea.email_address LIKE '".$db->quote($data['email1'])."' ";
        }
        else
        {
            // niether email exists
            $email_address_exists = false;
        }

        // look up related lists by email
        if ($email_address_exists === true)
        {
            $sql = "SELECT DISTINCT pl.mailchimp_list_name_c as mailchimp_list_id
                    FROM email_addresses ea
                    INNER JOIN email_addr_bean_rel eabr ON ea.id=eabr.email_address_id AND eabr.deleted=0 AND eabr.primary_address=1
                    INNER JOIN prospect_lists_prospects plp ON eabr.bean_id=plp.related_id AND plp.deleted=0
                    INNER JOIN prospect_lists pl ON plp.prospect_list_id=pl.id AND pl.deleted=0
                    WHERE ea.deleted=0 AND pl.mailchimp_list_name_c != '' AND pl.mailchimp_list_name_c IS NOT NULL" . $sql_email_address;

            self::log('debug','SugarChimp_Helper::belongs_to_list sql: '.$sql);

            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result))
            {
                $list_ids []= $row['mailchimp_list_id'];
            }
        }
        
        if (!empty($bean->id))
        {
            // we also need to search by bean id to find related list ids
            // there are cases in which looking up by email address will not work
            // the most common case is the email for the contact is removed
            $sql = "SELECT DISTINCT pl.mailchimp_list_name_c as mailchimp_list_id
                    FROM ".$db->quote($bean->table_name)." b
                    INNER JOIN prospect_lists_prospects plp ON b.id=plp.related_id AND plp.deleted=0
                    INNER JOIN prospect_lists pl ON plp.prospect_list_id=pl.id AND pl.deleted=0
                    WHERE b.deleted=0 AND b.id='".$db->quote($bean->id)."' AND pl.mailchimp_list_name_c != '' AND pl.mailchimp_list_name_c IS NOT NULL";

            self::log('debug','SugarChimp_Helper::belongs_to_list sql: '.$sql);

            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result))
            {
                $list_ids []= $row['mailchimp_list_id'];
            }
        }

        if (empty($list_ids))
        {
            self::log('debug','SugarChimp_Helper::belongs_to_list there are no target lists related to the bean');
            // if it's empty, then nothing needs to be checked
            return false;
        }
        
        // make sure there aren't duplicate list ids in list_ids
        $list_ids = array_unique($list_ids);

        self::log('debug','SugarChimp_Helper::belongs_to_list found '.count($list_ids).' synced lists with ids: '.print_r($list_ids,true));

        return $list_ids;
    }
    
    // check if changes were made to a record (usually person, ex. Contact, Target, Lead, etc.)
    // $bean represents the SugarBean passed from a logic hook
    // $data is the array of fetch_row data, it holds the data from the bean before the save
    // we have to do the comparison in the after_save to handle catching email address changes
    public static function check_person_changes($bean,$data=array())
    {
        if (empty($bean))
        {
            self::log('debug','SugarChimp_Helper::check_record_changes the bean is empty');
            return false;
        }
        
        // if the person doesn't belong to a target list that is synced with mailchimp, they don't need to be checked for field changes
        $lists = self::belongs_to_list($bean,$data);
        if (empty($lists))
        {
            self::log('debug','SugarChimp_Helper::check_record_changes the bean is not related to a target list');
            return false;
        }
        
        // since we made it here, we know this record is associated to a synced mailchimp target list
        // we need to check for modified mapped fields and queue sugarchimp events to be handled
        return SugarChimp_FieldMap::check($lists,$bean,$data);
    }
    
    // check if changes were made to a record that is part of a related field mapping
    // $bean represents the SugarBean passed from a logic hook
    // $data is the array of fetch_row data, it holds the data from the bean before the save
    public static function check_related_record_changes($bean,$data=array(),$force_update_check=false)
    {
        if (empty($bean))
        {
            self::log('debug','SugarChimp_Helper::check_related_record_changes the bean is empty');
            return false;
        }

        $changes = SugarChimp_FieldMap::check_related_record_changes($bean,$data,$force_update_check);

        if (!empty($changes) && is_array($changes))
        {
            foreach ($changes as $mailchimp_list_id => $modules)
            {
                $jobs_to_queue = array();
                foreach ($modules as $module => $jobs)
                {
                    foreach ($jobs as $job => $ids)
                    {
                        foreach ($ids as $id)
                        {
                            // queue job for $job, $mailchimp_list_id, $module, $id
                            $jobs_to_queue []= array(
                                'name' => $job,
                                'params' => array(
                                    'param1' => $module, // bean module
                                    'param2' => $id, // bean id
                                    'param3' => '',
                                    'param4' => '',
                                    'param5' => '',
                                ),
                            );
                        }
                    }
                }
                SugarChimp_Logger::log('SugarChimp_Helper::check_related_record_changes: mailchimp_list_id: '.print_r($mailchimp_list_id,true).' jobs: '.print_r($jobs_to_queue,true));
                SugarChimp_FieldMap::queue_jobs($jobs_to_queue,array($mailchimp_list_id));
            }
        }
    }
    
    // queue the removal of the person from mailchimp
    // $bean is the bean from the after_delete hook
    // $list_ids are the mailchimp list ids that the bean is related to
    public static function check_person_deletes($bean,$list_ids=array())
    {
        if (empty($bean))
        {
            self::log('debug','SugarChimp_Helper::check_deletes the bean is empty');
            return false;
        }
        
        if (!is_array($list_ids) || empty($list_ids))
        {
            self::log('debug','SugarChimp_Helper::check_deletes list_ids is empty');
            return false;
        }
        
        // since we made it here, we know this record is associated to a synced mailchimp target list
        // we need to check for modified mapped fields and queue sugarchimp events to be handled
        $job = array(
            'name' => 'RemoveMailchimpSubscriber',
            'params' => array(
                'param1' => $bean->module_dir, // bean module
                'param2' => $bean->id, // bean id
                'param3' => $bean->email1, // old email
                'param4' => '',
                'param5' => '',
            ),
        );

        SugarChimp_FieldMap::queue_jobs(array($job),$list_ids);
    }
    
    // check for changes to target lists that are synced to mailchimp
    // $bean is the target list that was changed
    // $bean->mailchimp_list_name_c is empty if not synced
    // otherwise, it holds the mailchimp list id
    public static function check_list_changes($bean,$data=array())
    {
        if (empty($bean))
        {
            self::log('debug','SugarChimp_Helper::check_list_changes the bean is empty');
            return false;
        }
        
        if (!isset($bean->mailchimp_list_name_c))
        {
            self::log('fatal','SugarChimp_Helper::check_list_changes the mailchimp_list_name_c field is not on the module');
            return false;
        }
        
        SugarChimp_Field::forge('MailchimpListName')->check('mailchimp_list_name_c',$bean,$data);
    }
    
    // check to see & queue if a person needs to be added to mailchimp list
    // $bean is the record of the list someone was added to
    // $related_module is the module of the person being added
    // $related_id is the id of the person being added
    public static function check_list_person_added($bean,$related_module,$related_id)
    {
        if (empty($bean) || empty($related_module) || empty($related_id))
        {
            self::log('debug','SugarChimp_Helper::check_list_person_added the bean, related_module or related_id is empty');
            return false;
        }
        
        if (!isset($bean->mailchimp_list_name_c) || empty($bean->mailchimp_list_name_c))
        {
            self::log('debug','SugarChimp_Helper::check_list_person_added the list is not synced with mailchimp, carry on');
            return false;
        }

        // Chad 4/2/2013
        // Removing the following code as it caused performance issues when adding several persons to a target list
        // the only reason it grabs a person_bean is to check to make sure an email1 is present
        // the UpdateMailchimpSubscriber job handles this for us, so no need for the check here 
        //
        // $person_bean = BeanFactory::getBean($related_module,$related_id);
        // 
        // if (empty($person_bean->id) or !$person_bean instanceOf SugarBean)
        // {
        //     self::log('debug',"SugarChimp_Helper::check_list_person_added could not find a bean with the related data.\nrelated_module: ".print_r($related_module,true)."\n".print_r($related_id,true));
        //     return false;
        // }
        // 
        // if (empty($person_bean->email1))
        // {
        //     self::log('debug',"SugarChimp_Helper::check_list_person_added {$related_module} {$related_id} does not have an email address, no need to update Mailchimp");
        //     return false;
        // }
        
        $job_type = 'UpdateMailchimpSubscriber';
        $group_fields = SugarChimp_Field_Group::get_group_fields($bean->mailchimp_list_name_c,array($related_module));
        if (!empty($group_fields))
        {
            $job_type = 'UpdateMailchimpSubscriberGroup';
        }

        $job = array(
            'name' => $job_type,
            'params' => array(
                'param1' => $related_module, // bean module
                'param2' => $related_id, // bean id
                'param3' => '',
                'param4' => '',
                'param5' => '',
            ),
        );

        SugarChimp_FieldMap::queue_jobs(array($job),array($bean->mailchimp_list_name_c));
    }

    // check to see & queue if a person needs to be removed to mailchimp list
    // $bean is the record of the list someone was added to
    // $related_module is the module of the person being added
    // $related_id is the id of the person being added
    public static function check_list_person_deleted($bean,$related_module,$related_id)
    {
        if (empty($bean) || empty($related_module) || empty($related_id))
        {
            self::log('debug','SugarChimp_Helper::check_list_person_deleted the bean, related_module or related_id is empty');
            return false;
        }
        
        if (!isset($bean->mailchimp_list_name_c) || empty($bean->mailchimp_list_name_c))
        {
            self::log('debug','SugarChimp_Helper::check_list_person_deleted the list is not synced with mailchimp, carry on');
            return false;
        }
        
        $person_bean = BeanFactory::getBean($related_module,$related_id);
        
        if (empty($person_bean->id))
        {
            self::log('debug','SugarChimp_Helper::check_list_person_deleted could not find the related record to remove');
            return false;
        }

        $job = array(
            'name' => 'RemoveMailchimpSubscriber',
            'params' => array(
                'param1' => $related_module, // bean module
                'param2' => $related_id, // bean id
                'param3' => $person_bean->email1, // old email
                'param4' => '',
                'param5' => '',
            ),
        );

        SugarChimp_FieldMap::queue_jobs(array($job),array($bean->mailchimp_list_name_c));
    }


    // used by the process_person function in mailchimptosugarcrm job
    // person requires
    // -list_id
    // -mapping[supported_modules]
    public static function update_beans_from_mailchimp_sync($target_list, $beans=array(), $person)
    {
        if (empty($beans))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::update_beans_from_mailchimp_sync beans are required');
            return false;
        }
        if (empty($target_list))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::update_beans_from_mailchimp_sync target list is required');
            return false;
        }
        if (empty($person))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::update_beans_from_mailchimp_sync data is required');
            return false;
        }
        
        $log_beans = array();
        $num_beans = 0;
        foreach ($beans as $bean)
        {
            $num_beans++;
            $log_beans[$bean->id] = $bean->module_dir;
        }
        SugarChimp_Helper::log('debug',"SugarChimp_Helper::update_beans_from_mailchimp_sync beans to update: ".print_r($log_beans,true));
        
        if($num_beans > 1)
        {
            $update_multi_from_mc = SugarChimp_Setting::retrieve('update_multi_from_mc');
            if(empty($update_multi_from_mc))
            {
                SugarChimp_Helper::log('debug',"SugarChimp_Helper::update_beans_from_mailchimp_sync - " . print_r($num_beans,true) ." Beans found on Target List, not updating.");
                return true;
            }
        }
        
        // disable logic hooks for the beans
        SugarChimp_Helper::disable_logic_hooks($beans);
        SugarChimp_Helper::disable_logic_hooks(array($target_list));

        // need to pass beans to the update_bean function as we have previously
        // all other beans have already been related to the target list
        $updated_beans[] = array();
        foreach($beans as $bean)
        {
            if (empty($person['mapping'][$bean->module_dir]))
            {
                // no data for this person/bean type
                SugarChimp_Helper::log('fatal','SugarChimp_Helper::update_beans_from_mailchimp_sync could not find mapping/data/fields for '.$bean->module_dir);
                continue;
            }
            $result = SugarChimp_Helper::update_beans_with_mailchimp_data($person['list_id'],array($bean),$person['mapping'][$bean->module_dir]);
        }

        //this is a newly created bean, so it needs to be added to the target list
        if(!empty($person['create_new']) && $person['create_new'] === true)
        {
            // relate the record to the target list
            $result = SugarChimp_Helper::add_beans_to_list($target_list,$beans);

            if ($result !== true)
            {
                SugarChimp_Helper::log('fatal','SugarChimp_Helper::update_beans_from_mailchimp_sync update_beans_from_mailchimp failed');
                return true;
            }
        }

        // enable logic hooks for the beans
        SugarChimp_Helper::enable_logic_hooks($beans);
        SugarChimp_Helper::enable_logic_hooks(array($target_list));

        return true;
    }

    // $beans
    // $target_list
    // $data array(
    //      'email'
    //      'list_id' (mailchimp list id)
    //      'merges()' 
    // )
    // 
    public static function update_beans_from_webhook($target_list, $beans=array(), $data=array())
    {
        if (empty($beans))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::update_beans_from_webhook beans are required');
            return false;
        }
        if (empty($target_list))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::update_beans_from_webhook target list is required');
            return false;
        }
        if (empty($data))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::update_beans_from_webhook data is required');
            return false;
        }
        if(empty($data['list_id']))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::update_beans_from_webhook data[list_id] is required');
            return false;
        }
        if(empty($data['merges']))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::update_beans_from_webhook data[merges] is required');
            return false;
        }
        //check if we can update multiple beans with MailChimp data
        // defaults to 0 (no)
        $num_beans = count($beans);
        if($num_beans > 1)
        {
            $update_multi_from_mc = SugarChimp_Setting::retrieve('update_multi_from_mc');
            if(empty($update_multi_from_mc))
            {
                SugarChimp_Helper::log('debug',"SugarChimp_Helper::update_beans_from_webhook - " . print_r($num_beans,true) ." Beans found on Target List, not updating.");
                return true;
            }
        }

        // disable logic hooks for the beans
        SugarChimp_Helper::disable_logic_hooks($beans);
        SugarChimp_Helper::disable_logic_hooks(array($target_list));

        // if there are groupings, go get the updated interest array from mailchimp
        // this is because the webhooks do not pull in the new v3 api interest grouping ids
        $interests = false;
        if (!empty($data['merges']['INTERESTS']) or !empty($data['merges']['GROUPINGS']))
        {
            $member = MailChimp_API::get_member($data['list_id'],$data['email'],array(
                'fields' => 'id,interests',
            ));
            //SugarChimp_Helper::log('debug','Mailchimp_Webhooks_Receiver::subscribe get_member response: '.print_r($member,true));

            if (!empty($member['interests']))
            {
                $interests = $member['interests'];
            }
        }
        $log_beans = array();
        foreach ($beans as $bean)
        {
            $log_beans[$bean->id] = $bean->module_dir;
        }
        SugarChimp_Helper::log('debug',"SugarChimp_Helper::update_beans_from_webhook beans to update: ".print_r($log_beans,true));


        // create/update new bean(s) with data from mailchimp
        $result = SugarChimp_Helper::update_beans_with_mailchimp_data($data['list_id'],$beans,$data['merges'],$interests);
        
        if ($result !== true)
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Helper::update_beans_from_webhook update_beans_with_mailchimp_data failed');
            return false;
        }

        //this is a newly created bean, so it needs to be added to the target list
        if(!empty($data['create_new']) && $data['create_new'] === true)
        {
            // relate the record to the target list
            $result = SugarChimp_Helper::add_beans_to_list($target_list,$beans);

            if ($result !== true)
            {
                SugarChimp_Helper::log('fatal','SugarChimp_Helper::update_beans_from_webhook add_beans_to_list failed');
                return false;
            }
        }

        // enable logic hooks for the beans
        SugarChimp_Helper::enable_logic_hooks($beans);
        SugarChimp_Helper::enable_logic_hooks(array($target_list));

        return true;
    }
    // 6/1/17 update - this method assumes that beans provided 
    // are on the synced list 
    // 
    // given an array of beans, update them based on provided data
    // it uses the field mapper to do this based on the set mappings
    public static function update_beans_with_mailchimp_data($list_id,$beans,$data,$interests=false)
    {
        if (empty($list_id))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::update_beans_with_mailchimp_data list_id is required');
            return false;
        }

        if (empty($beans))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::update_beans_with_mailchimp_data beans are required');
            return false;
        }

        if (empty($data) || !is_array($data))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::update_beans_with_mailchimp_data data array is required');
            return false;
        }
        
        //check if we can update multiple beans with MailChimp data
        // defaults to 0 (no)
        $num_beans = count($beans);
        if($num_beans > 1)
        {
            $update_multi_from_mc = SugarChimp_Setting::retrieve('update_multi_from_mc');
            if(empty($update_multi_from_mc))
            {
                SugarChimp_Helper::log('debug',"SugarChimp_Helper::update_beans_with_mailchimp_data - " . print_r($num_beans,true) ." Beans found on Target List, not updating.");
                return true;
            }
        }

        // SugarChimp_Helper::log('debug',"SugarChimp_Helper::update_beans_with_mailchimp_data - " . print_r($num_beans,true) ." bean found on Target List, updating with MailChimp data.");

        foreach ($beans as $bean)
        {
            if (SugarChimp_FieldMap::update_bean($list_id,$bean,$data,$interests) !== true)
            {
                SugarChimp_Helper::log('fatal',"SugarChimp_Helper::update_beans_with_mailchimp_data bean {$bean->id} could not be updated in sugar. data: ".print_r($data,true));
            }
        }
        
        return true;
    }
    
    // given a target list object and array of beans
    // remove all the beans from said target list
    public static function remove_beans_from_list($target_list,$beans)
    {
        if (empty($target_list) || !$target_list instanceOf SugarBean)
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::remove_beans_from_list a valid target list bean is required');
            return false;
        }

        if (empty($beans) || !is_array($beans))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::remove_beans_from_list an array of beans is required');
            return false;
        }
        
        SugarChimp_Helper::log('debug',"SugarChimp_Helper::remove_beans_from_list - Remove all beans from target list {$target_list->id}.");
        
        foreach ($beans as $bean)
        {
            $bean->load_relationship('prospect_lists');
            if (!isset($bean->prospect_lists) || !method_exists($bean->prospect_lists,'delete'))
            {
                SugarChimp_Helper::log('fatal',"SugarChimp_Helper::remove_beans_from_list Cannot remove {$bean->module_dir} {$bean->id} from target list {$target_list->id}");
                continue;
            }
            
            SugarChimp_Helper::log('debug',"SugarChimp_Helper::remove_beans_from_list - Remove {$bean->module_dir} {$bean->id} from target list {$target_list->id}");
            $bean->prospect_lists->delete($bean->id,$target_list->id);
        }

        SugarChimp_Helper::log('debug',"SugarChimp_Helper::remove_beans_from_list - All beans successfully removed from target list {$target_list->id}.");
        return true;        
    }
    

    public static function add_to_list_by_email($email,$target_list,$default_module)
    {
        $webhook_add_to_list = SugarChimp_Setting::retrieve('webhook_add_to_list');
        // default_only = 
        $def_module_only = false;
        // check_smartlist = false to bypass smartlist checks
        $check_smartlist = true;

        $secondary_module_only = false;
        // Are we allowed to create new records? 
        $create_new = true;
        switch ($webhook_add_to_list)
        {
            // add everyone always
            case "1":
                SugarChimp_Helper::log('debug','SugarChimp_Helper::add_to_list_by_email - webhook_add_to_list=1, add all matching records to list.');
                $check_smartlist = false;
                break;
            // add default_module records only, do nothing if no default modules found
            case "2":
                SugarChimp_Helper::log('debug','SugarChimp_Helper::add_to_list_by_email - webhook_add_to_list=2, add default_module records only.');
                $def_module_only = true;
                $secondary_module_only = "do_nothing";
                $create_new = false;
                break;
            // add default_module only, create new default_module if none exists
            case "3":
                SugarChimp_Helper::log('debug','SugarChimp_Helper::add_to_list_by_email - webhook_add_to_list=3, add default_module records only. If none found, create new one.');
                $def_module_only = true;
                $secondary_module_only = "create_default";
                break;
            // add default_module when exists, else add other records
            case "4":
                SugarChimp_Helper::log('debug','SugarChimp_Helper::add_to_list_by_email - webhook_add_to_list=4, add default module records only. If none found, add secondary modules.');
                $def_module_only = true;
                $secondary_module_only = "add_secondary";
                break;
            // do not add records to the target list
            case "5":
                SugarChimp_Helper::log('debug','SugarChimp_Helper::add_to_list_by_email - webhook_add_to_list=5, not adding new records to list.');
                return array(
                    'beans'=>array(),
                    'create_new'=>false,
                );
            // 
            case "0":
            default :
                SugarChimp_Helper::log('debug','SugarChimp_Helper::add_to_list_by_email - webhook_add_to_list=0, checking all beans through smartlist.');
                break;
        }

        // calls the find_beans function with no parameters
        // will filter based on settings below
        $beans = SugarChimp_Helper::find_beans_by_email($email);

        if(empty($beans)){
            SugarChimp_Helper::log('debug',"SugarChimp_Helper::add_to_list_by_email - no beans with {$email} in Sugar.");
            return array(
                'beans'=>array(),
                'create_new'=>$create_new, //if setting=2, then not creating new records
            );
        }

        //filter beans
        $filtered_beans = array();
        if($def_module_only === true)
        {
            // split beans into default_beans and secondary_beans
            $secondary_beans = array();
            SugarChimp_Helper::log('debug',"SugarChimp_Helper::add_to_list_by_email check beans for default module: ".print_r($default_module,true));
            foreach ($beans as $bean)
            {
                if (!$bean instanceOf SugarBean)
                {
                    SugarChimp_Helper::log('warning',"SugarChimp_Helper::add_to_list_by_email provided bean is not a SugarBean, skipping");
                    continue;
                }
                if($bean->module_dir == $default_module)
                {
                    SugarChimp_Helper::log('debug',"SugarChimp_Helper::add_to_list_by_email add bean {$bean->module_dir}-{$bean->id} to filtered beans.");
                    $filtered_beans[]= $bean;
                }else{
                    SugarChimp_Helper::log('debug',"SugarChimp_Helper::add_to_list_by_email add bean {$bean->module_dir}-{$bean->id} to secondary beans.");
                    $secondary_beans[]= $bean;
                }
            }
            if(empty($filtered_beans))
            {
                SugarChimp_Helper::log('debug','SugarChimp_Helper::add_to_list_by_email - only secondary modules beans found.');
                if($secondary_module_only == "add_secondary")
                {
                    SugarChimp_Helper::log('debug','SugarChimp_Helper::add_to_list_by_email - adding secondary module beans to the list.');
                    $filtered_beans = $secondary_beans;
                }
                else if($secondary_module_only == "create")
                {
                    SugarChimp_Helper::log('debug','SugarChimp_Helper::add_to_list_by_email - creating new default module bean per settings.');
                    return array(
                        'beans'=>array(),
                        'create_new'=>true,
                    );
                }else{
                    SugarChimp_Helper::log('debug','SugarChimp_Helper::add_to_list_by_email - not adding secondary modules beans to list per settings.');
                    return array(
                        'beans'=>array(),
                        'create_new'=>false,
                    );
                }
            }
        }else{
            SugarChimp_Helper::log('debug',"SugarChimp_Helper::add_to_list_by_email - no filtering on default module. ");
            $filtered_beans = $beans;
        }
        
        $log_beans = array();
        foreach ($filtered_beans as $bean)
        {
            $log_beans[$bean->id] = $bean->module_dir;
        }
        SugarChimp_Helper::log('debug','SugarChimp_Helper::add_to_list_by_email - filtered beans to add: '.print_r($log_beans,true));

        // add filtered_beans to list only
        SugarChimp_Helper::add_beans_to_list($target_list,$filtered_beans,$check_for_smartlist);
        return array(
            'beans'=>$filtered_beans,
            'create_new'=>false,
        );
    }


    // Assumes the beans in the $beans array are all beans 
    // with the same primary email address. Will not work with random Beans
    // We don't use it throughout the code any other way, but I could see it being misused.
    // 
    // if any beans are already on Target List, then bypass
    // Check vs SmartList filters
    // if no smartlist filters, then add all
    public static function add_beans_to_list($target_list,$beans,$check_for_smartlist=true)
    {

        if (empty($target_list) || !$target_list instanceOf SugarBean)
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::add_beans_to_list a valid target list bean is required');
            return false;
        }
        
        if (empty($beans) || !is_array($beans))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::add_beans_to_list an array of beans is required');
            return false;
        }

        SugarChimp_Helper::log('debug',"SugarChimp_Helper::add_beans_to_list: Target List-{$target_list->id}");
        
        $already_on_list = false;
        foreach ($beans as $bean)
        {
            if (!$bean instanceOf SugarBean)
            {
                SugarChimp_Helper::log('warning',"SugarChimp_Helper::add_beans_to_list provided bean is not a SugarBean, skipping");
                continue;
            }
            
            $bean->load_relationship('prospect_lists');
            if (!isset($bean->prospect_lists))
            {
                SugarChimp_Helper::log('warning',"SugarChimp_Helper::add_beans_to_list Cannot add {$bean->module_dir} {$bean->id} to target list {$target_list->id}");
                continue;
            }
            
            $list_ids = $bean->prospect_lists->get();

            SugarChimp_Helper::log('debug',"SugarChimp_Helper::add_beans_to_list ids of target lists the bean {$bean->module_dir}-{$bean->id} already belongs to: ".print_r($list_ids,true));
            if (in_array($target_list->id,$list_ids))
            {
                // the record is already related, so break loop and move on
                SugarChimp_Helper::log('debug',"SugarChimp_Helper::add_beans_to_list bean already belongs to list, not adding any beans to list. ");
                $already_on_list = true;
                return true;
            }
        }

        if($already_on_list === false)
        {
            SugarChimp_Helper::log('debug',"SugarChimp_Helper::add_beans_to_list no beans are currently on the Target List");
            if($check_for_smartlist)
            {
                $processed_through_smartlist = SmartList_Helper::process_beans_for_list($target_list, $beans);
                if(!empty($processed_through_smartlist))
                {
                    //debug - beans processed through smartlist filters, exiting
                    SugarChimp_Helper::log('debug',"SugarChimp_Helper::add_beans_to_list: beans processed through smartlist filters, done processing");
                    return true;
                }
                //debug log - checked for smartlist filters, but none found for the list
                SugarChimp_Helper::log('debug',"SugarChimp_Helper::add_beans_to_list: No smartlist filters for these beans on this target list, continue adding all.");
            }
            
            // disable logic hooks for the beans
            SugarChimp_Helper::disable_logic_hooks($beans);
            SugarChimp_Helper::disable_logic_hooks(array($target_list));
            // no smartlist filters exist for the list
            // and nobody is already on the list
            // add all beans to the prospect_list
            foreach ($beans as $bean)
            {
                if (!$bean instanceOf SugarBean)
                {
                    SugarChimp_Helper::log('warning',"SugarChimp_Helper::add_beans_to_list provided bean is not a SugarBean, skipping");
                    continue;
                }
                
                SugarChimp_Helper::log('debug',"SugarChimp_Helper::add_beans_to_list bean {$bean->module_dir} {$bean->id}");

                $bean->load_relationship('prospect_lists');
                if (!isset($bean->prospect_lists))
                {
                    SugarChimp_Helper::log('fatal',"SugarChimp_Helper::add_beans_to_list Cannot add {$bean->module_dir} {$bean->id} to target list {$target_list->id}");
                    continue;
                }
                
                $bean->prospect_lists->add($target_list->id);
            }
            // disable logic hooks for the beans
            SugarChimp_Helper::enable_logic_hooks($beans);
            SugarChimp_Helper::enable_logic_hooks(array($target_list));
        }
        
        return true;
    }
    
    // in some cases we need to use an empty bean if something doesn't already exist in sugar
    // most of the times $module will be the default module type for a list
    // find where it's used for a better explanation
    public static function get_empty_bean($module)
    {
        $default_assigned_user = SugarChimp_Setting::retrieve('default_assigned_user');
        
        if (empty($default_assigned_user))
        {
            $default_assigned_user = '1';
        }

        if (empty($module))
        {
            $module = 'Prospects'; //todo: allow global admin default
        }
        
        if (SugarChimp_FieldMap::is_mapped_module($module) !== true)
        {
            SugarChimp_Helper::log('fatal','SugarChimp_FieldMap::update_bean $bean is not a SugarBean or a mapped module: '.print_r($module,true));
            return false;
        }

        // get a new bean instance if we're creating a record
        $bean = BeanFactory::newBean($module);
        $bean->assigned_user_id = $default_assigned_user;
        
        return $bean;
    }
    
    /**
     * Take a MailChimp list and create a SugarCRM target list
     */
    public static function create_sugarcrm_list($mailchimp_list_id,$default_module)
    {
        //check to ensure list does not yet exist in SugarCRM
        $prospect_list = BeanFactory::getBean('ProspectLists');
        $prospect_list->retrieve_by_string_fields(array('mailchimp_list_name_c' => $mailchimp_list_id));
        
        if(!empty($prospect_list->id)) {
            // queue the job to add the webhook url to the list in MailChimp
            self::queue_add_webhooks_to_lists(array($mailchimp_list_id));
            self::log('debug',"    *** Skipping creating list ".$mailchimp_list_id." from MailChimp. Already exists in SugarCRM as ".$prospect_list->name);
            return true;
        }
        
        require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
        require_once('modules/SugarChimp/includes/vendor/Mailchimp/Mailchimp.php');
        // $mc = new Mailchimp($api_key);
        // $lists = $mc->lists->getList(array('list_id'=>$mailchimp_list_id));

        $list = MailChimp_API::get_list($mailchimp_list_id);
        
        if(empty($list['id'])) {
            self::log('debug',"SugarChimp_Helper::create_sugarcrm_list failed as the list $mailchimp_list_id does not exist in MailChimp.");
            return true; //so that it doesn't requeue
        }
        
        //create the prospect list
        $prospect_list = BeanFactory::getBean('ProspectLists');
        $prospect_list->mailchimp_list_name_c = $list['id'];
        $prospect_list->mailchimp_default_module_c = $default_module;
        $prospect_list->name = $list['name'];
        $prospect_list->list_type = 'default';
        $prospect_list->save();

        // queue the job to add the webhook url to the list in MailChimp
        self::queue_add_webhooks_to_lists(array($mailchimp_list_id));
        
        return true;
    }
    
    /**
     * Grab all subscribers from a MailChimp list and try to import them into SugarCRM
     *
     * @param $mailchimp_list_id - list_id on MC side
     * @param $api_key - MailChimp's API key
     */
    public static function import_subscribers_from_mailchimp($api_key, $mailchimp_list_id, $table_name='sugarchimp_mc')
    {
        SugarChimp_Helper::log('debug',"SugarChimp_Helper::import_subscribers_from_mailchimp for ".$mailchimp_list_id);
        
        //checks in place to see if we never finish this for a list...if 3x then email us with addt info
        //for example, a 100k subscriber list that we cannot queue fast enough
        
        require_once('modules/SugarChimp/includes/vendor/Mailchimp/Mailchimp.php');
        $mc = new Mailchimp($api_key);
        
        //hack to pass to the callback
        global $queue_mailchimp_list_id, $queue_mailchimp_table_name;
        $queue_mailchimp_list_id = $mailchimp_list_id;
        $queue_mailchimp_table_name = $table_name;
        
        $mc->export->getList('queue_mailchimp_subscriber',$mailchimp_list_id);
        
        return true;
    }
    
    /**
     * Grab all cleaned emails from a MailChimp list and queue them to sugar table
     *
     * @param $mailchimp_list_id - list_id on MC side
     * @param $api_key - MailChimp's API key
     */
    public static function import_cleaned_emails_from_mailchimp($api_key, $mailchimp_list_id, $since, $table_name='sugarchimp_mc_cleaned_emails')
    {
        SugarChimp_Helper::log('debug',"SugarChimp_Helper::import_cleaned_emails_from_mailchimp for ".$mailchimp_list_id);
        
        require_once('modules/SugarChimp/includes/vendor/Mailchimp/Mailchimp.php');
        $mc = new Mailchimp($api_key);
        
        //hack to pass to the callback
        global $queue_mailchimp_list_id, $queue_mailchimp_list_since, $queue_mailchimp_table_name;
        $queue_mailchimp_list_id = $mailchimp_list_id;
        $queue_mailchimp_list_since = $since;
        $queue_mailchimp_table_name = $table_name;
        
        try
        {
            $mc->export->getList('queue_mailchimp_cleaned_emails',$mailchimp_list_id,'cleaned',false,$since);
        }
        catch (Exception $e)
        {
            SugarChimp_Logger::log('fatal','SugarChimp_Helper::import_cleaned_emails_from_mailchimp: a MailChimp exception was thrown: '.$e->getMessage()); 
            return true;
        }
        
        SugarChimp_List::mark_list_cleaned_emails_synced($queue_mailchimp_list_id);

        return true;
    }
    
    public static function queue_MailChimpToSugarCRM_job($list_id,$default_module=false,$create_list=true)
    {
        $subscriber_sync_enabled = SugarChimp_Setting::retrieve('subscriber_sync_enabled');
        if(empty($subscriber_sync_enabled))
        {
            self::log('warning',"SugarChimp_Helper::queue_MailChimpToSugarCRM_job - Subscriber Syncing is Disabled. Not Queueing MailChimpToSugarCRM Job.");
            return false;
        }

        require_once('include/SugarQueue/SugarJobQueue.php');
        
        //note: we may want to check to see if something is queued up already
        //if so then remove and add this...could cause issues...actually, lets do that

        $data = array(
            'list_id' => $list_id,
            'default_module' => $default_module,
            'create_list' => $create_list,
        );

        $jq = new SugarJobQueue();
        
        // note: if the job name ever changes the health status check in sugarchimpapi getHealth() method needs to be updated
        $job_name = 'MailChimp to SugarCRM Job - Initial Import for list '.$list_id;
        $job = new SchedulersJob();
        
        // $job->retrieve_by_string_fields(array('target' => 'class::MailChimpToSugarCRM','status' => 'queued'));
        $job->retrieve_by_string_fields(array('name' => $job_name,'status' => 'queued'));
        if (!empty($job->id)) {
            $job->deleted = 1;
            $job->save();
            $jq->deleteJob($job->id);
        }

        $job = new SchedulersJob();
        $job->assigned_user_id = 1;
        $job->name = $job_name;
        $job->data = base64_encode(serialize($data));
        $job->target = 'class::MailChimpToSugarCRM';
        $job->requeue = 1;
        $job->retry_count = 100;

        $jobid = $jq->submitJob($job);
        
        return true;
    }
    
    /**
     * Grab all subscribers from a MailChimp list and try to import them into SugarCRM
     *
     * @param $api_key - MailChimp's API key
     * @param $mailchimp_campaign_id - campaign_id on MC side
     * @param $mailchimp_list_id - list_id on MC side
     * @param $include_empty (default false) - include_empty set to true will return all email addresses, set to false will only return people with new activity
     * @param $since (default false) - return data since the timestamp on MC side
     */
    public static function import_activities_from_mailchimp($api_key, $mailchimp_campaign_id, $mailchimp_list_id, $include_empty=false, $since=false)
    {
        SugarChimp_Helper::log('debug',"SugarChimp_Helper::import_subscribers_from_mailchimp for ".$mailchimp_list_id);
        
        //checks in place to see if we never finish this for a list...if 3x then email us with addt info
        //for example, a campaign to 100k subscriber list may not be able to be queued fast enough
        
        require_once('modules/SugarChimp/includes/vendor/Mailchimp/Mailchimp.php');
        $mc = new Mailchimp($api_key);
        
        //hack to pass to the callback
        global $queue_mailchimp_campaign_id, $queue_mailchimp_list_id, $queue_include_empty;
        $queue_mailchimp_campaign_id = $mailchimp_campaign_id;
        $queue_mailchimp_list_id = $mailchimp_list_id;
        $queue_include_empty = $include_empty;
        
        SugarChimp_Helper::log('debug',"SugarChimp_Helper::import_subscribers_from_mailchimp queue_mailchimp_campaign_id: ".print_r($queue_mailchimp_campaign_id,true));
        SugarChimp_Helper::log('debug',"SugarChimp_Helper::import_subscribers_from_mailchimp queue_mailchimp_list_id: ".print_r($queue_mailchimp_list_id,true));
        SugarChimp_Helper::log('debug',"SugarChimp_Helper::import_subscribers_from_mailchimp queue_include_empty: ".print_r($queue_include_empty,true));
        
        // start the activity import
        $mc->export->campaignSubscriberActivity('queue_mailchimp_activity',$mailchimp_campaign_id,$include_empty,$since);
        
        // mark the campaign as synced
        SugarChimp_Activity::mark_campaign_synced($mailchimp_list_id,$mailchimp_campaign_id,date("Y-m-d H:i:s"));
        
        return true;
    }
    
    public static function queue_MailChimpToSugarCRMActivity_job($data)
    {
        require_once('include/SugarQueue/SugarJobQueue.php');
        
        //note: we may want to check to see if something is queued up already
        //if so then remove and add this...could cause issues...actually, lets do that

        $jq = new SugarJobQueue();
        
        $job = new SchedulersJob();
        $job->retrieve_by_string_fields(array('target' => 'class::MailChimpToSugarCRMActivity','status' => 'queued'));
        if (!empty($job->id)) 
        {
            // there's already a job in place, let it run first before queueing another
            SugarChimp_Helper::log('debug','SugarChimp_Helper::queue_MailChimpToSugarCRMActivity_job: there is already a job in the queue, we cannot queue more than one activity sync at a time: job id '.$job->id);
            return false;
        }

        // since there's not a job, let's create one. go go go 
        $job = new SchedulersJob();
        $job->assigned_user_id = 1;
        $job->name = 'MailChimp to SugarCRM Activity Job - Initial Campaign Activity Import ';
        $job->data = base64_encode(serialize($data));
        $job->target = 'class::MailChimpToSugarCRMActivity';
        $job->requeue = 1;
        $job->retry_count = 100;

        $jobid = $jq->submitJob($job);
        
        return true;
    }

    function queue_GetMailChimpCleanedEmails_job($data=array())
    {
        require_once('include/SugarQueue/SugarJobQueue.php');
        
        $jq = new SugarJobQueue();
        
        $job = new SchedulersJob();
        $job->retrieve_by_string_fields(array('target' => 'class::GetMailChimpCleanedEmails','status' => 'queued'));
        if (!empty($job->id)) 
        {
            // there's already a job in place, let it run first before queueing another
            SugarChimp_Helper::log('debug','SugarChimp_Helper::queue_GetMailChimpCleanedEmails_job: there is already a job in the queue, we cannot queue more than one activity sync at a time: job id '.$job->id);
            return false;
        }

        // since there's not a job, let's create one. go go go 
        $job = new SchedulersJob();
        $job->assigned_user_id = 1;
        $job->name = 'MailChimp List Cleaned Emails to SugarCRM Job';
        $job->data = base64_encode(serialize($data));
        $job->target = 'class::GetMailChimpCleanedEmails';
        $job->requeue = 1;
        $job->retry_count = 100;

        $jobid = $jq->submitJob($job);
        
        return true;
    }

    /**
     * Take a full list and send it to MailChimp
     *
     * @param $lists - array of ProspectLists beans
     */
    public static function push_lists_to_mailchimp($lists)
    {
        require_once('modules/SugarChimp/includes/classes/SugarChimp/Field/Group.php');

        if (empty($lists)) {
            return true;
        }
        
        global $db;
        
        $supported_modules = SugarChimp_FieldMap::get_supported_modules();

        foreach ($lists as $list)
        {           
            //now queue up all the subscribers of this list to be pushed over to MailChimp
            //todo: support custom fields in the future

            //using a direct query here for performance reasons over queuing each member one-by-one
            // this used to use the ProspectList->create_export_members_query method, but it added a "ORDER BY related_type, id, primary_address DESC"
            // which is not allowed in the sql server when create a table/view from a query
            
            // first check to see if list contains a synced group field, if it does, run UpdateMailchimpSubscriberGroup instead of the regular UpdateMailchimpSubscriber Job
            // NOTE: this query will grab all non-primary addresses, but it works out ok because only the bean id is what is stored and later processed
            
            $job_type = 'UpdateMailchimpSubscriber';
            $group_fields = SugarChimp_Field_Group::get_group_fields($list->mailchimp_list_name_c,$supported_modules);
            if (!empty($group_fields))
            {
                $job_type = 'UpdateMailchimpSubscriberGroup';
            }

            $query = "INSERT INTO sugarchimp(id,name,date_entered,date_modified,modified_user_id,created_by,deleted,mailchimp_list_id,param1,param2)
                      SELECT ".$db->getGuidSQL().",'".$job_type."',".$db->now().",".$db->now().",1,1,0,'".$list->mailchimp_list_name_c."',related_type,id 
                      FROM (";

            $leads_query = "SELECT l.id AS id, 'Leads' AS related_type, '' AS \"name\", l.first_name AS first_name, l.last_name AS last_name, l.title AS title, l.salutation AS salutation, 
                            l.primary_address_street AS primary_address_street,l.primary_address_city AS primary_address_city, l.primary_address_state AS primary_address_state, l.primary_address_postalcode AS primary_address_postalcode, l.primary_address_country AS primary_address_country,
                            l.account_name AS account_name,
                            ea.email_address AS primary_email_address, ea.invalid_email AS invalid_email, ea.opt_out AS opt_out, ea.deleted AS ea_deleted, ear.deleted AS ear_deleted, ear.primary_address AS primary_address,
                            l.do_not_call AS do_not_call, l.phone_fax AS phone_fax, l.phone_other AS phone_other, l.phone_home AS phone_home, l.phone_mobile AS phone_mobile, l.phone_work AS phone_work
                            FROM prospect_lists_prospects plp
                            INNER JOIN leads l ON plp.related_id=l.id
                            LEFT JOIN email_addr_bean_rel ear ON  ear.bean_id=l.id AND ear.deleted=0
                            LEFT JOIN email_addresses ea ON ear.email_address_id=ea.id
                            WHERE plp.prospect_list_id = '".$list->id."' AND plp.deleted=0 
                            AND l.deleted=0
                            AND (ear.deleted=0 OR ear.deleted IS NULL)";

            $contacts_query = "SELECT c.id AS id, 'Contacts' AS related_type, '' AS \"name\", c.first_name AS first_name, c.last_name AS last_name,c.title AS title, c.salutation AS salutation, 
                            c.primary_address_street AS primary_address_street,c.primary_address_city AS primary_address_city, c.primary_address_state AS primary_address_state,  c.primary_address_postalcode AS primary_address_postalcode, c.primary_address_country AS primary_address_country,
                            a.name AS account_name,
                            ea.email_address AS email_address, ea.invalid_email AS invalid_email, ea.opt_out AS opt_out, ea.deleted AS ea_deleted, ear.deleted AS ear_deleted, ear.primary_address AS primary_address,
                            c.do_not_call AS do_not_call, c.phone_fax AS phone_fax, c.phone_other AS phone_other, c.phone_home AS phone_home, c.phone_mobile AS phone_mobile, c.phone_work AS phone_work
                            FROM prospect_lists_prospects plp
                            INNER JOIN contacts c ON plp.related_id=c.id LEFT JOIN accounts_contacts ac ON ac.contact_id=c.id LEFT JOIN accounts a ON ac.account_id=a.id
                            LEFT JOIN email_addr_bean_rel ear ON ear.bean_id=c.id AND ear.deleted=0
                            LEFT JOIN email_addresses ea ON ear.email_address_id=ea.id
                            WHERE plp.prospect_list_id = '".$list->id."' AND plp.deleted=0 
                            AND c.deleted=0
                            AND (ear.deleted=0 OR ear.deleted IS NULL)";

            $prospects_query = "SELECT p.id AS id, 'Prospects' AS related_type, '' AS \"name\", p.first_name AS first_name, p.last_name AS last_name,p.title AS title, p.salutation AS salutation, 
                                p.primary_address_street AS primary_address_street,p.primary_address_city AS primary_address_city, p.primary_address_state AS primary_address_state,  p.primary_address_postalcode AS primary_address_postalcode, p.primary_address_country AS primary_address_country,
                                p.account_name AS account_name,
                                ea.email_address AS email_address, ea.invalid_email AS invalid_email, ea.opt_out AS opt_out, ea.deleted AS ea_deleted, ear.deleted AS ear_deleted, ear.primary_address AS primary_address,
                                p.do_not_call AS do_not_call, p.phone_fax AS phone_fax, p.phone_other AS phone_other, p.phone_home AS phone_home, p.phone_mobile AS phone_mobile, p.phone_work AS phone_work
                                FROM prospect_lists_prospects plp
                                INNER JOIN prospects p ON plp.related_id=p.id
                                LEFT JOIN email_addr_bean_rel ear ON  ear.bean_id=p.id AND ear.deleted=0
                                LEFT JOIN email_addresses ea ON ear.email_address_id=ea.id
                                WHERE plp.prospect_list_id = '".$list->id."'  AND plp.deleted=0 
                                AND p.deleted=0
                                AND (ear.deleted=0 OR ear.deleted IS NULL)";

            $accounts_query = "SELECT a.id AS id, 'Accounts' AS related_type, a.name AS \"name\", '' AS first_name, '' AS last_name,'' AS title, '' AS salutation, 
                                a.billing_address_street AS primary_address_street,a.billing_address_city AS primary_address_city, a.billing_address_state AS primary_address_state, a.billing_address_postalcode AS primary_address_postalcode, a.billing_address_country AS primary_address_country,
                                '' AS account_name,
                                ea.email_address AS email_address, ea.invalid_email AS invalid_email, ea.opt_out AS opt_out, ea.deleted AS ea_deleted, ear.deleted AS ear_deleted, ear.primary_address AS primary_address,
                                0 AS do_not_call, a.phone_fax as phone_fax, a.phone_alternate AS phone_other, '' AS phone_home, '' AS phone_mobile, a.phone_office AS phone_office
                                FROM prospect_lists_prospects plp
                                INNER JOIN accounts a ON plp.related_id=a.id
                                LEFT JOIN email_addr_bean_rel ear ON  ear.bean_id=a.id AND ear.deleted=0
                                LEFT JOIN email_addresses ea ON ear.email_address_id=ea.id
                                WHERE plp.prospect_list_id = '".$list->id."'  AND plp.deleted=0 
                                AND a.deleted=0
                                AND (ear.deleted=0 OR ear.deleted IS NULL)";

            $query .= "$leads_query UNION ALL $contacts_query UNION ALL $prospects_query";

            // if accounts are being synced, throw it in there
            if (in_array("Accounts",$supported_modules))
            {
                $query .= " UNION ALL $accounts_query ";
            }
            
            $query .= ") AS subscribers
                       WHERE subscribers.related_type IN ('" . implode("','", $supported_modules) . "') ";
            
            //keep this at fatal as it shouldn't be used often and when things go wrong we'll want this
            SugarChimp_Helper::log('debug',"SugarChimp::push_lists_to_mailchimp() query: ".$query);
            $db->query($query);
                
        }
      
    }
    
    //called from the Prospect List Dropdown with ($exclude_synced_lists=true)
    public static function get_mailchimp_lists($exclude_synced_lists=false)
    {
        require_once('modules/SugarChimp/includes/vendor/Mailchimp/Mailchimp.php');
        
        $synced_lists = array();
        if ($exclude_synced_lists === true)
        {
            // get the synced lists from sugar, we'll filter out this list from the mailchimp lists
            $synced_lists = self::get_lists();
            
            // just in case an error is thrown, give it a default value
            if (empty($synced_lists))
            {
                $synced_lists = array();
            }
        }
        
        $lists = array();
        
        try {
            // $mc = new Mailchimp($apikey);
            // $result = $mc->lists->getList(array(),0,100,'web','ASC'); //MC api limit of 100
            $result = MailChimp_API::get_lists(array('fields'=>'lists.id,lists.name'));
            SugarChimp_Helper::log('debug','result of get_lists call: '.print_r($result,true));
            if(!empty($result['lists']) and is_array($result['lists'])) 
            {
                foreach($result['lists'] as $list) 
                {                  
                    $lists[$list['id']] = $list['name'];
                }
            }
            
            //MailChimp only allows sorting on "created" or "web" so we sort here by name
            asort($lists);
            
            //now add an identifier for lists already synced
            // if excluding synced lists we are going to just note that it is a synced list due to limitations in ModuleApi->getEnumValues not passing the bean to getFunctionValues
            if ($exclude_synced_lists === true)
            {
                foreach ($lists as $list_id => $list_name)
                {
                    // if excluding synced lists we are going to just note that it is a synced list due to limitations in ModuleApi->getEnumValues not passing the bean to getFunctionValues
                    if (in_array($list_id,$synced_lists))
                    {
                        $lists[$list_id] = '* '.$list_name;
                    }
                }
            }
            
        } catch(Exception $e) {
            SugarChimp_Helper::log('fatal','SugarChimp_Helper::get_mailchimp_lists error: '.$e->getMessage());
        }
        
        return $lists;
    }
    
    public static function get_mailchimp_campaigns()
    {
        require_once('modules/SugarChimp/includes/vendor/Mailchimp/Mailchimp.php');
        
        $campaigns = array();

        try 
        {
            // $mc = new Mailchimp($apikey);
            // $result = $mc->campaigns->getList(); //MC api limit of 100
            // todo: support getting more than 100 campaign MC limit
            // would need to get numbers of campaigns and loop through calls until all are retrieved
            $result = MailChimp_API::get_campaigns(array('fields'=>'campaigns.id,campaigns.settings.title','count'=>1500));


            //needs to return array of ''
            if(!empty($result['campaigns']) && is_array($result['campaigns']))
            {
                foreach($result['campaigns'] as $campaign) 
                {                  
                    $campaigns[$campaign['id']] = $campaign['settings']['title'];
                }
            }
            
            //MailChimp only allows sorting on "created" or "web" so we sort here by name
            asort($campaigns);
            
        } 
        catch(Exception $e) 
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Helper::get_mailchimp_campaigns error: '.$e->getMessage());
        }
        
        return $campaigns;
    }
    
    public static function get_sugarchimp_version()
    {
        $sugarchimp_version = 'unknown';
        
        /** //fails due to no deleted column
        $upgrade_history = BeanFactory::getBean('UpgradeHistory');
        $upgrade_history->retrieve_by_string_fields(array('id_name' => 'SugarChimp'));        
        if ($upgrade_history->id)
        {
            $sugarchimp_version = $upgrade_history->version;
        }
        */
        global $db;
        $result = $db->query("select version from upgrade_history where id_name = 'SugarChimp' order by date_entered DESC");
        $row = $db->fetchByAssoc($result);
        if (!empty($row)) {
            $sugarchimp_version = $row['version'];
        }
        
        return $sugarchimp_version;
    }
    
    public static function email_support($error_message,$data)
    {
        global $sugar_config, $sugar_version, $sugar_flavor;

        $systemUser = BeanFactory::getBean("Users");
        $systemUser->getSystemUser();

        $systemUserInfo = $systemUser->getUsersNameAndEmail();

        $error_email = SugarChimp_Helper::get_error_email_address();
        
        $email_to = 'help+error-generic@sugarchimp.com';
        $subject =  'SugarChimp Support Issue Identified';
        $bodyHTML= "
Site: ".$sugar_config['site_url']."
Admin Email: ".$error_email."|".$systemUserInfo['name']."
SugarCRM Version: ".$sugar_version."
SugarCRM Edition: ".$sugar_flavor."
SugarChimp Version: ".SugarChimp_Helper::get_sugarchimp_version()."

Error: $error_message
Remaining data: ".print_r($data,true)."
";

        $license_key = SugarChimp_License::get_license_key();
        if (!empty($license_key))
        {
            $bodyHTML .= "\n\nSugarChimp License: ".$license_key;
        }
        else
        {
            $bodyHTML .= "\n\nSugarChimp License: unknown - could not retrieve";
        }
        
        return SugarChimp_Helper::send_email($email_to,$subject,$bodyHTML);
    }
    
    /**
     * @param $email_to email address to send to
     * @param $subject subject of the email
     * @param $bodyHTML HTML body content
     * @param $bcc (Optional) email address to bcc
     */
    public static function send_email($email_to,$subject,$bodyHTML,$bcc=null,$cc=null)
    {
        try {
            global $sugar_config, $sugar_version, $sugar_flavor;

            require_once("modules/Emails/Email.php");
            $body = wordwrap($bodyHTML, 900);
            $_REQUEST['sendDescription'] = htmlentities($body);
            // Email Settings
            $send_config = array(
                'sendTo' => $email_to,
                'saveToSugar' => false,
                'sendSubject' => $subject,
                'sendCc' => '',
                'sendBcc' => '',
            );
            if (!empty($bcc))
            {
                $send_config['sendBcc'] = $bcc;
            }
            if (!empty($cc))
            {
                $send_config['sendCc'] = $cc;
            }
            $email = new Email();
            $email->email2init();
            //sending email
            if (!$email->email2Send($send_config))
            {
                SugarChimp_Helper::log('fatal',"A SugarChimp error was detected. We tried to email you about the error, but Sugar is unable to send emails to ".$email_to.". Here is what we tried to send:\n\nSubject: ".$subject."\n\nBody: ".$body. "\n\nPlease send this message to SugarChimp support if you have any questions: support@sugarchimp.com");
                return false;
            }
        } catch(Exception $e) {
            SugarChimp_Helper::log('fatal',"A SugarChimp error was detected. We tried to email you about the error, but Sugar is unable to send emails to ".$email_to.". Here is what we tried to send:\n\nSubject: ".$subject."\n\nBody: ".$body. "\n\nError: ".$e->getMessage()."\n\nPlease send this message to SugarChimp support if you have any questions: support@sugarchimp.com");
            return false;
        }
        
        return true;
    }
    
    public static function disable_logic_hooks($beans)
    {
        SugarChimp_Helper::log('debug','SugarChimp_Helper::disable_logic_hook begin');
        if (is_array($beans))
        {
            foreach ($beans as $bean)
            {
                if ($bean instanceOf SugarBean)
                {
                    SugarChimp_Helper::log('debug',"SugarChimp_Helper::disable_logic_hook for {$bean->module_dir} {$bean->id}");
                    static::$beans_with_logic_hooks_disabled [$bean->id]= $bean->id;
                }
            }
        }
        SugarChimp_Helper::log('debug','SugarChimp_Helper::disable_logic_hook end');
    }
    
    public static function enable_logic_hooks($beans)
    {
        SugarChimp_Helper::log('debug','SugarChimp_Helper::enable_logic_hook begin');
        if (is_array($beans))
        {
            foreach ($beans as $bean)
            {
                if ($bean instanceOf SugarBean)
                {
                    SugarChimp_Helper::log('debug',"SugarChimp_Helper::enable_logic_hook for {$bean->module_dir} {$bean->id}");
                    unset(static::$beans_with_logic_hooks_disabled[$bean->id]);
                }
            }
        }
        SugarChimp_Helper::log('debug','SugarChimp_Helper::enable_logic_hook end');
    }
    
    public static function logic_hooks_enabled($bean)
    {
        return !in_array($bean->id,static::$beans_with_logic_hooks_disabled);
    }
    
    public static function queue_add_webhooks_to_all_lists()
    {
        $list_ids = self::get_lists();
        
        if (empty($list_ids))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::queue_add_webhooks_to_all_lists There are no target lists synced to MC in sugar');
            return false;
        }
        
        return self::queue_add_webhooks_to_lists($list_ids);
    }
    
    public static function queue_add_webhooks_to_lists($list_ids)
    {
        if (empty($list_ids) || !is_array($list_ids))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::queue_add_webhooks_to_lists list_ids is empty');
            return false;
        }
        
        $jobs = array(array(
            'name' => 'AddWebhook'
        ));
        
        SugarChimp_FieldMap::queue_jobs($jobs,$list_ids);
        
        return true;
    }
    
    public static function queue_mailchimp_backup($list_ids)
    {
        if (empty($list_ids) || !is_array($list_ids))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::queue_mailchimp_backup list_ids is empty');
            return false;
        }
        
        global $db;

        foreach ($list_ids as $list_id)
        {
            if (empty($list_id))
            {
                SugarChimp_Helper::log('fatal','SugarChimp_Helper::queue_mailchimp_backup could not queue backup. list_id is empty.');
                continue;
            }

            $sql = "INSERT INTO sugarchimp_backup_queue
                    (id, name, mailchimp_list_id, data, date_entered, date_modified, deleted) VALUES 
                    (".$db->getGuidSQL().",'MailchimpBackup','".$db->quote($list_id)."','',".$db->now().",".$db->now().",0)";

            if (!$result = $db->query($sql))
            {
                SugarChimp_Helper::log('fatal','SugarChimp_Helper::queue_mailchimp_backup could not queue backup for list '.print_r($list_id,true));
            }
        }

        return true;
    }

    public static function queue_sugar_backup($list_ids)
    {
        if (empty($list_ids) || !is_array($list_ids))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::queue_sugar_backup list_ids is empty');
            return false;
        }
        
        global $db;

        foreach ($list_ids as $list_id)
        {
            if (empty($list_id))
            {
                SugarChimp_Helper::log('fatal','SugarChimp_Helper::queue_sugar_backup could not queue backup. list_id is empty.');
                continue;
            }

            $sql = "INSERT INTO sugarchimp_backup_queue
                    (id, name, mailchimp_list_id, data, date_entered, date_modified, deleted) VALUES 
                    (".$db->getGuidSQL().",'SugarBackup','".$db->quote($list_id)."','',".$db->now().",".$db->now().",0)";

            if (!$result = $db->query($sql))
            {
                SugarChimp_Helper::log('fatal','SugarChimp_Helper::queue_sugar_backup could not queue backup for list '.print_r($list_id,true));
            }
        }

        return true;
    }

    public static function add_webhooks_to_all_lists()
    {
        $list_ids = self::get_lists();
        
        if (empty($list_ids))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::add_webhooks_to_all_lists There are no target lists synced to MC in sugar');
            return false;
        }
        
        return self::add_webhooks_to_lists($list_ids);
    }
    
    // $list_ids is an array of MC lists
    public static function add_webhooks_to_lists($list_ids)
    {
        if (empty($list_ids) || !is_array($list_ids))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::add_webhooks_to_lists list_ids is empty');
            return false;
        }
        
        $status = array(
            'success' => array(),
            'error' => array(),
        );

        $webhook_url = self::get_webhook_url_for_mc();
        
        if (empty($webhook_url))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Helper::add_webhooks_to_lists could not retrieve SugarChimp webhook url');
            return false;
        }
        
        foreach ($list_ids as $list_id)
        {
            $webhooks = MailChimp_API::get_webhooks($list_id,array('fields'=>'webhooks.id,webhooks.url'));

            $found_match = false;
            if (!empty($webhooks['webhooks']) && is_array($webhooks['webhooks']))
            {
                foreach ($webhooks['webhooks'] as $webhook)
                {
                    if ($webhook_url == $webhook['url'])
                    {
                        $found_match = true;
                    }
                }
            }

            if ($found_match === true)
            {
                // we already have this webhook added
                // go to the next list
                SugarChimp_Helper::log('debug','SugarChimp_Helper::add_webhooks_to_lists webhook already added to list: '.print_r($list_id,true).' - '.print_r($webhook,true));
                continue;
            }

            // otherwise, let's add it
            $webhook = MailChimp_API::add_webhook($list_id,$webhook_url);

            SugarChimp_Helper::log('debug','SugarChimp_Helper::add_webhooks_to_lists: webhook response: '.print_r($webhook,true));

            if (!empty($webhook['errors']) && is_array($webhook['errors']))
            {
                if (!empty($webhook['errors'][0]['message']))
                {
                    if ($webhook['errors'][0]['message'] == "Sorry, you can't set up multiple WebHooks for one URL")
                    {
                        // shouldn't get here now because we check, but just in case
                        $status['success'][$list_id]['id'] = 'exists';
                    }
                    else
                    {
                        SugarChimp_Helper::log('fatal','SugarChimp_Helper::add_webhooks_to_lists there was an error adding webhook to list '.$list_id.' error: '.print_r($webhook['errors'],true));
                        $status['error'][$list_id]['status'] = 'error';
                        $status['error'][$list_id]['code'] = '';
                        $status['error'][$list_id]['name'] = 'Exception';
                        $status['error'][$list_id]['error'] = $webhook['errors'][0]['message'];
                    }
                }
                else
                {
                    SugarChimp_Helper::log('fatal','SugarChimp_Helper::add_webhooks_to_lists there was an error adding webhook to list '.$list_id.' error: '.print_r($webhook['errors'],true));
                    $status['error'][$list_id]['status'] = 'error';
                    $status['error'][$list_id]['code'] = '';
                    $status['error'][$list_id]['name'] = 'Exception';
                    $status['error'][$list_id]['error'] = 'Unknown error.';
                }
            }
            else
            {
                SugarChimp_Helper::log('debug','SugarChimp_Helper::add_webhooks_to_lists successfully added webhook to list '.$list_id);
                $status['success'][$list_id]['id'] = $webhook['id'];
            }
        }
        
        SugarChimp_Helper::log('debug','SugarChimp_Helper::add_webhooks_to_lists status: '.print_r($status,true));
        return $status;
    }
    
    public static function get_webhook_url_for_mc()
    {
        // first check to see if the sugarchimp_site_url config is available
        // this is handy for those who have proxies setup, they can set the site_url
        // config item and webhooks will automatically be setup for them correctly
        $url = SugarChimp_Setting::retrieve('site_url');

        if (empty($url))
        {
            global $sugar_config;
            $url = $sugar_config['site_url'];
        }

        // sometimes the url will contain double slashes, for ex. if the url stored is
        // something like website.com/ then it'll be website.com//index.php.... 
        // we could fix this here, but if we do it will let us create a duplicate
        // webhook and they'll be getting hit with double webhooks, may as well
        // leave it like it is and let it be

        return $url.'/index.php?module=SugarChimp&entryPoint=SugarChimpWebhook';
    }
    
    // mark email address as invalid
    public static function mark_email_invalid($email)
    {
        if (empty($email))
        {
            self::log('debug','SugarChimp_Helper::mark_email_invalid email is empty');
            return false;
        }

        global $db;

        self::log('debug','SugarChimp_Helper::mark_email_invalid opt out: '.$email);
        
        $sql = "UPDATE email_addresses
                SET invalid_email=1, date_modified=".$db->now()." 
                WHERE email_address LIKE '".$db->quote($email)."'";
                
        self::log('debug','SugarChimp_Helper::mark_email_invalid sql to opt out: '.$sql);
        
        if (!$result = $db->query($sql))
        {
            self::log('fatal','SugarChimp_Helper::mark_email_invalid could not mark '.$email.' invalid, query failed: '.$sql);
            return false;
        }
        
        return true;
    }
    
    // mark email address as opted out
    public static function mark_email_optout($email)
    {
        if (empty($email))
        {
            self::log('fatal','SugarChimp_Helper::mark_email_optout email is empty');
            return false;
        }
        
        global $db;

        self::log('debug','SugarChimp_Helper::mark_email_optout opt out: '.$email);
        
        $sql = "UPDATE email_addresses
                SET opt_out=1, date_modified=".$db->now()." 
                WHERE email_address LIKE '".$db->quote($email)."'";
                
        self::log('debug','SugarChimp_Helper::mark_email_optout sql to opt out: '.$sql);
        
        if (!$result = $db->query($sql))
        {
            self::log('fatal','SugarChimp_Helper::mark_email_optout could not mark '.$email.' invalid, query failed: '.$sql);
            return false;
        }

        return true;
    }
    
    // mark email address as valid
    public static function mark_email_valid($email)
    {
        if (empty($email))
        {
            self::log('debug','SugarChimp_Helper::mark_email_valid email is empty');
            return false;
        }

        global $db;

        self::log('debug','SugarChimp_Helper::mark_email_valid: '.$email);
        
        $sql = "UPDATE email_addresses
                SET invalid_email=0, date_modified=".$db->now()." 
                WHERE email_address LIKE '".$db->quote($email)."'";
                
        self::log('debug','SugarChimp_Helper::mark_email_valid sql to opt out: '.$sql);
        
        if (!$result = $db->query($sql))
        {
            self::log('fatal','SugarChimp_Helper::mark_email_valid could not mark '.$email.' valid, query failed: '.$sql);
            return false;
        }
        
        return true;
    }
    
    // mark email address as opted in
    public static function mark_email_optin($email)
    {
        if (empty($email))
        {
            self::log('debug','SugarChimp_Helper::mark_email_optin email is empty');
            return false;
        }
        
        global $db;

        self::log('debug','SugarChimp_Helper::mark_email_optin: '.$email);
        
        $sql = "UPDATE email_addresses
                SET opt_out=0, date_modified=".$db->now()." 
                WHERE email_address LIKE '".$db->quote($email)."'";
                
        self::log('debug','SugarChimp_Helper::mark_email_optin sql to opt out: '.$sql);
        
        if (!$result = $db->query($sql))
        {
            self::log('fatal','SugarChimp_Helper::mark_email_optin could not mark '.$email.' optin, query failed: '.$sql);
            return false;
        }

        return true;
    }
    
    // the mailchimp list no longer exists
    // remove the old records from the sugarchimp queue
    public static function handle_mailchimp_list_does_not_exist_exception($list_id)
    {
        if (empty($list_id))
        {
            self::log('fatal','SugarChimp_Helper::handle_mailchimp_list_does_not_exist_exception list_id is empty, old sugarchimp queue data will not be removed');
            return false;
        }
        
        global $db;
        
        // remove the link on the target list
        $sql = "UPDATE prospect_lists 
                SET mailchimp_list_name_c='', date_modified=".$db->now()." 
                WHERE mailchimp_list_name_c='".$db->quote($list_id)."'";
                
        SugarChimp_Helper::log('debug','SugarChimp_Helper::handle_mailchimp_list_does_not_exist_exception sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Helper::handle_mailchimp_list_does_not_exist_exception query failed, could dissassociate sugar list from mailchimp list');
            return false;
        }
        
        self::cleanUpAfterSyncRemoval($list_id);

        return true;
    }
    
    public static function get_lists_for_beans($beans,$synced=true)
    {
        if (empty($beans))
        {
            SugarChimp_Helper::log('warning',"SugarChimp_Helper::get_lists_for_beans array of beans is required.");
            return false;
        }
        
        $lists = array();
        
        foreach ($beans as $bean)
        {
            $bean->load_relationship('prospect_lists');
            if (!isset($bean->prospect_lists))
            {
                SugarChimp_Helper::log('warning',"SugarChimp_Helper::get_lists_for_beans Cannot load prospect_lists relationship for {$bean->module_dir} {$bean->id}");
                continue;
            }

            $all_lists = $bean->prospect_lists->getBeans();
            
            if (!empty($all_lists) && is_array($all_lists))
            {
                foreach ($all_lists as $list)
                {
                    if ($synced === true)
                    {
                        // if synced param is true, only return lists that are synced with a mailchimp list
                        if (!empty($list->mailchimp_list_name_c))
                        {
                            $lists[$list->id] = $list;
                        }
                    }
                    else
                    {
                        $lists[$list->id] = $list;
                    }
                }
            }
        }
        
        return $lists;
    }
    
    public static function get_error_email_address()
    {
        $error_email = SugarChimp_Setting::retrieve('erroremail');

        if (!empty($error_email))
        {
            self::log('debug','SugarChimp_Helper::get_error_email_address found email in settings: '.$error_email);
            return $error_email;
        }

        self::log('debug','SugarChimp_Helper::get_error_email_address could not find email in settings, getting system user.');
        
        $systemUser = BeanFactory::getBean("Users");
        $systemUser->getSystemUser();
        $systemUserInfo = $systemUser->getUsersNameAndEmail();
        
        self::log('debug','SugarChimp_Helper::get_error_email_address found system email: '.$systemUserInfo['email']);
        
        SugarChimp_Setting::set('erroremail',$systemUserInfo['email']);
        
        return $systemUserInfo['email'];
    }
    
    public static function is_ondemand_instance()
    {
        global $sugar_config;
        
        if (strpos($sugar_config['site_url'],'ondemand') !== false)
        {
            return true;
        }
        
        if (strpos($sugar_config['site_url'],'sugaropencloud') !== false) 
        {
            return true;
        }
        
        return false;
    }
    // updated in 7.8.3m (5/10/17) to only lock on RemoveSubscriber Job
    // Without locking on RemoveSubscriber job, removal from a Target List
    // would result in opting out of that email address
    // 
    // Only locking/unlocking on unsubscribes because we receive 2 webhooks
    // (profile and unsubscribe) on a single removal job...thus locking 
    // was not reliable
    // 
    // adds to lock table, will be removed if unsubscribe comes in via the api
    // Cleanup Job will remove old locks (when via the api is not checked)
    public static function lock_email($email)
    {

        global $db;

        if (empty($email))
        {
            self::log('debug','SugarChimp_Helper::lock_email cannot lock email, no email provided.');
            return false;
        }

        $email = strtolower($email);

        $sql = "INSERT INTO sugarchimp_lock (id,email,date_entered) 
                VALUES (".$db->getGuidSQL().",'".$db->quote($email)."','".$GLOBALS['timedate']->nowDb()."')";

        self::log('debug','SugarChimp_Helper::lock_email sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            self::log('debug','SugarChimp_Helper::lock_email query failed, could not lock email '.print_r($email,true));
            return false;
        }

        return true;
    }

    // updated in 7.8.3m (5/10/17) to only unlock from unsubscribes webhooks
    // 
    // should only be called when via API is checked and we receive an unsubscribe
    public static function unlock_email($email)
    {
        global $db;
        
        if (empty($email))
        {
            self::log('debug','SugarChimp_Helper::unlock_email cannot lock email, no email provided.');
            return false;
        }

        $email = strtolower($email);

        $sql = "DELETE FROM sugarchimp_lock WHERE email LIKE '".$db->quote($email)."'";

        self::log('debug','SugarChimp_Helper::unlock_email sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            self::log('debug','SugarChimp_Helper::unlock_email query failed, could not unlock email '.print_r($email,true));
            return false;
        }

        return true;
    }
    // updated in 7.8.3m (5/10/17) to only check on unsubscribe webhooks
    // 
    // should only be called when via API is checked and we receive an unsubscribe
    // if unsubscribe came from us, then email should be locked
    // otherwise, unsubscribe is legit and needs to be processed
    public static function is_email_locked($email)
    {
        global $db;
        
        if (empty($email))
        {
            self::log('debug','SugarChimp_Helper::is_email_locked cannot lock email, no email provided.');
            return false;
        }

        $email = strtolower($email);

        $sql = "SELECT count(id) as number_records FROM sugarchimp_lock WHERE email LIKE '".$db->quote($email)."'";

        self::log('debug','SugarChimp_Helper::is_email_locked sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            self::log('fatal','SugarChimp_Helper::is_email_locked query failed, could not check if email is locked '.print_r($email,true));
            return false;
        }

        $row = $db->fetchByAssoc($result);

        self::log('debug','SugarChimp_Helper::is_email_locked query result '.print_r($result,true));

        if (!empty($row['number_records']) && $row['number_records'] > 0)
        {
            // email locked
            self::log('debug','SugarChimp_Helper::is_email_locked email is locked');
            return true;
        }
        else
        {
            // not locked
            self::log('debug','SugarChimp_Helper::is_email_locked email is not locked');
            return false;
        }
    }

    // check whether this is a suitecrm instance
    public static function is_suitecrm()
    {
        global $sugar_config;

        return isset($sugar_config['suitecrm_version']);
    }

    // gather array of extra metadata to send on license validation check
    public static function license_check_metadata()
    {     
        global $db;   

        $sql = "SELECT 
                    pl.id AS prospect_list_id, 
                    pl.mailchimp_list_name_c AS mailchimp_list_id, 
                    plo.related_type AS module, 
                    COUNT(plo.related_id) AS count
                FROM prospect_lists pl
                INNER JOIN prospect_lists_prospects plo ON plo.prospect_list_id=pl.id
                WHERE
                    pl.mailchimp_list_name_c IS NOT NULL AND
                    pl.mailchimp_list_name_c != '' AND
                    pl.deleted = 0 AND
                    plo.deleted = 0
                GROUP BY plo.prospect_list_id, plo.related_type";

        self::log('debug','SugarChimp_Helper::license_check_metadata sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            self::log('fatal','SugarChimp_Helper::license_check_metadata query failed, could not generate metadata for license');
            return false;
        }

        $metadata = array();
        while ($row = $db->fetchByAssoc($result))
        {
            if (!isset($metadata[$row['prospect_list_id']]))
            {
                $metadata[$row['prospect_list_id']] = array(
                    'prospect_list_id' => $row['prospect_list_id'],
                    'mailchimp_list_id' => $row['mailchimp_list_id'],
                    'counts' => array(),
                );
            }

            $metadata[$row['prospect_list_id']]['counts'][$row['module']] = $row['count'];
        }

        self::log('debug','SugarChimp_Helper::license_check_metadata metadata: '.print_r($metadata,true));

        return $metadata;
    }

    public static function get_current_plan()
    {
        // turning off debugging, gets too noisy
        
        if (!empty(static::$current_plan) && in_array(static::$current_plan,static::$plans))
        {
            // self::log('debug','SugarChimp_Helper::get_current_plan return static current plan: '.print_r(static::$current_plan,true));
            return static::$current_plan;
        }

        // if we don't have it yet, go look it up
        // self::log('debug','SugarChimp_Helper::get_current_plan go get current plan.');
        static::$current_plan = SugarChimpOutfittersLicense::get_current_plan('SugarChimp');

        // self::log('debug','SugarChimp_Helper::get_current_plan found current plan: '.print_r(static::$current_plan,true));
        return static::$current_plan;
    }

    public static function ping($apikey = false)
    {
        return MailChimp_API::ping($apikey,array('fields'=>'account_id'));
    }

    public static function get_sugarchimpmclists()
    {
        global $db;

        $sql = "SELECT id,name,mailchimp_list_id,optout_tracker_processed,optout_tracker_offset,deleted,date_entered,date_modified,modified_user_id,created_by,description,last_cleaned_email_check
                FROM sugarchimp_mc_list";

        SugarChimp_Helper::log('debug','SugarChimp_Helper::get_sugarchimpmclists sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            SugarChimp_Helper::log('error','SugarChimp_Helper::get_sugarchimpmclists query failed. sql: '.$sql);
            return array();
        }

        $records = array();

        while ($row = $db->fetchByAssoc($result))
        {
            $records []= $row;
        }

        if (empty($records))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::get_sugarchimpmclists No records found on table.');
            return array();
        }
        $lists = array();
        if(is_array($records)) 
        {
            foreach($records as $list) 
            {                  
                $lists[$list['mailchimp_list_id']] = $list['name'];
            }
        }
            
        //MailChimp only allows sorting on "created" or "web" so we sort here by name
        asort($lists);
        SugarChimp_Helper::log('debug','SugarChimp_Helper::get_sugarchimpmclists found lists: ' . print_r($lists,true));

        return $lists;
    }
    
    public static function get_sugarchimpmccampaigns()
    {
        global $db;

        $sql = "SELECT id,name,mailchimp_campaign_id FROM sugarchimp_mc_campaign";

        SugarChimp_Helper::log('debug','SugarChimp_Helper::get_sugarchimpmccampaigns sql: '.$sql);

        if (!$result = $db->query($sql))
        {
            SugarChimp_Helper::log('error','SugarChimp_Helper::get_sugarchimpmccampaigns query failed. sql: '.$sql);
            return array();
        }

        $records = array();

        while ($row = $db->fetchByAssoc($result))
        {
            $records []= $row;
        }

        if (empty($records))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Helper::get_sugarchimpmccampaigns No records found on table.');
            return array();
        }
        $campaigns = array();
        if(is_array($records)) 
        {
            foreach($records as $campaign) 
            {                  
                $campaigns[$campaign['mailchimp_campaign_id']] = $campaign['name'];
            }
        }
            
        //MailChimp only allows sorting on "created" or "web" so we sort here by name
        asort($campaigns);
        SugarChimp_Helper::log('debug','SugarChimp_Helper::get_sugarchimpmccampaigns found campaigns: ' . print_r($campaigns,true));

        return $campaigns;
    }

    function queue_GetMCCampaignsFromActivities_job($data=array())
    {
        require_once('include/SugarQueue/SugarJobQueue.php');
        
        $jq = new SugarJobQueue();
        
        $job = new SchedulersJob();
        $job->retrieve_by_string_fields(array('target' => 'class::GetMCCampaignsFromActivities','status' => 'queued'));
        if (!empty($job->id)) 
        {
            // there's already a job in place, let it run first before queueing another
            SugarChimp_Helper::log('debug','SugarChimp_Helper::queue_GetMCCampaignsFromActivities_job: there is already a job in the queue: job id '.$job->id);
            return false;
        }

        $job = new SchedulersJob();
        $job->assigned_user_id = 1;
        $job->name = 'SugarChimp: Grab campaigns from existing SugarChimpActivity records, add to table and dropdown';
        $job->data = base64_encode(serialize($data));
        $job->target = 'class::GetMCCampaignsFromActivities';
        $job->requeue = 1;
        $job->retry_count = 100;

        $jobid = $jq->submitJob($job);

        return true;
    }

    // adds MailChimp List to the sugarchimp_mc_list table
    // also adds list to the SugarChimpActivity dropdown options
    // extracted from MailChimpListtoSugarCRM job
    // included the ability to reach out to MailChimp to get the list name
    // which is used when called from the webhook
    public static function add_list_to_table($mc_list_id, $mc_list_name=false, $add_to_dropdown=true)
    {
        $mclist = BeanFactory::newBean('SugarChimpMCList');
        $mclist->retrieve_by_string_fields(array('mailchimp_list_id' => $mc_list_id));

        // check to see if it already exists in sugar
        // if it already exists, don't do anything
        if (empty($mclist) || empty($mclist->id))
        {
            // it does not exist yet, add it to the table
            // make MC get_list call for this list
            SugarChimp_Helper::log('debug',"SugarChimp_Helper::add_list_to_table - adding list to sugarchimp_mc_list table");
            
            if(empty($mc_list_name)){
                try{
                    $mc_list_data = MailChimp_API::get_list($mc_list_id);
                    $mc_list_name = empty($mc_list_data['name']) ? '' : $mc_list_data['name'];
                    if (empty($mc_list_data['id']))
                    {
                        SugarChimp_Helper::log('warning',"SugarChimp_Helper::add_list_to_table Could not find that list in MailChimp for id: ".$mc_list_id);
                        //List has probably been deleted in MailChimp
                        return array('success'=>false,'message'=>'Could not find that list in MailChimp.');
                    }
                }
                catch(Exception $e) {
                    SugarChimp_Helper::log('warning',"SugarChimp_Helper::add_list_to_table Error: ".$e->getMessage());
                    return array('success'=>false,'message'=>$e->getMessage());
                }
            }
            $mclist->mailchimp_list_id = $mc_list_id;
            $mclist->name = $mc_list_name;
            $mclist->optout_tracker_processed = 0;
            $mclist->optout_tracker_offset = 0;
            $mclist->deleted_from_mailchimp = 0;
            $mclist->save();
        }

        if ($add_to_dropdown===true)
        {
            // Also add the List to our Dropdown Menu
            $dropdown_option = array(
                'key'=> $mc_list_id,
                'label' => $mc_list_name,
            );
            SugarChimp_Helper::add_mailchimp_list_to_dropdown($dropdown_option);
        }
    }

    // adds MailChimp Campaign to the sugarchimp_mc_campaign table
    // also adds Campaign to the SugarChimpActivity dropdown options
    public static function add_campaign_to_table($mc_campaign_id)
    {
        $mccampaign = BeanFactory::newBean('SugarChimpMCCampaign');
        $mccampaign->retrieve_by_string_fields(array('mailchimp_campaign_id' => $mc_campaign_id));

        // check to see if it already exists in sugar
        // if it already exists, don't do anything
        // possibly update the send_time?
        if (empty($mccampaign) || empty($mccampaign->id))
        {
            // it does not exist yet, add it to the table
            //make MC get_report call for this list
            SugarChimp_Helper::log('debug',"SugarChimp_Helper::add_campaign_to_table - adding campaign to sugarchimp_mc_campaign table");
            try{
                $mc_campaign_data = MailChimp_API::get_report($mc_campaign_id);
                if (empty($mc_campaign_data['id']))
                {
                    SugarChimp_Helper::log('fatal',"SugarChimp_Helper::add_campaign_to_table Could not find that Campaign in MailChimp for id: ".$mc_campaign_id);
                    return array('success'=>false,'message'=>'Could not find that campaign in MailChimp.');
                }
            }
            catch(Exception $e) {
                SugarChimp_Helper::log('fatal',"SugarChimp_Helper::add_campaign_to_table Error: ".$e->getMessage());
                return array('success'=>false,'message'=>$e->getMessage());
            }
            
            
            $mccampaign->mailchimp_campaign_id = $mc_campaign_data['id'];
            $mccampaign->mailchimp_list_id = $mc_campaign_data['list_id'];
            $mccampaign->name = empty($mc_campaign_data['campaign_title']) ? '' : $mc_campaign_data['campaign_title'];
            $mccampaign->type = empty($mc_campaign_data['type']) ? '' : $mc_campaign_data['type'];
            
            $send_time = empty($mc_campaign_data['send_time']) ? '0' : $mc_campaign_data['send_time'];;
            // have to convert the time string
            // coming from MC v3 api for Sugar
            if(!empty($send_time)){
                $send_time = str_replace('T',' ',$send_time);
                $send_time = strstr($send_time,'+',TRUE);
            }
            $mccampaign->send_time = $send_time;
            
            $mccampaign->subject = empty($mc_campaign_data['subject_line']) ? '' : $mc_campaign_data['subject_line'];
            $mccampaign->save();

            /**
            8/18/2017 - this used to try to add the campaign to the dropdown list in this same method
            we had this firing on campaign webhooks, the campaign will get added on next scheduler run
            zazen had issues when sending multiple campaigns at one time from MC
            the webhooks would hit at the same exact time and cause issues when sugar writes to the options lists

            // Also add the Campaign to our Dropdown Menu
            $dropdown_option = array(
                'key'=> $mc_campaign_data['id'],
                'label' => $mc_campaign_data['campaign_title'],
            );
            SugarChimp_Helper::add_mailchimp_campaign_to_dropdown($dropdown_option);
            */
        }
        else{
            // could choose to try and update the current info instead of skipping
            SugarChimp_Helper::log('debug',"SugarChimp_Helper::add_campaign_to_table - skipping, campaign already found with id: ".$mc_campaign_id);
        }
    }

    public static function format_timestamp($timestamp,$format='M j, Y G:i')
    {
        if(empty($timestamp))
        {
            SugarChimp_Helper::log('warning',"SugarChimp_Helper::format_timestamp - timestamp: ".$timestamp);
            return "";
        }
        
        global $timedate;
        $datetime = $timedate->fromTimestamp($timestamp);
        $timedate->tzUser($datetime);
        return $datetime->format($format);
    }

    public static function add_mailchimp_campaign_to_dropdown($option)
    {
        $mod = 'SugarChimpActivity';
        $field = 'mailchimp_campaign_id';
        $result = SugarChimp_Helper::get_enum_options($mod,$field);
        if(!empty($result) && empty($result['success'])){
            $result = array(
                'name'=>'mailchimp_campaigns_list',
                'options'=> array(),
            );
            SugarChimp_Helper::log('warning',"SugarChimp_Helper::add_mailchimp_campaign_to_dropdown no dropdown options currently found, create first option." );
        }

        // SugarChimp_Helper::log('debug',"SugarChimp_Helper::add_mailchimp_campaign_to_dropdown current options: ".print_r($result,true));
        if(!empty($result) && is_array($result) && is_array($result['options']))
        {
            if(!array_key_exists ($option['key'] ,$result['options']))
            {
                SugarChimp_Helper::add_option_to_sugar_dropdown($result['name'],$option);
            }
        }
    }

    public static function add_mailchimp_list_to_dropdown($option)
    {
        $mod = 'SugarChimpActivity';
        $field = 'mailchimp_list_id';
        $result = SugarChimp_Helper::get_enum_options($mod,$field);
        
        if(!empty($result) && empty($result['success'])){
            $result = array(
                'name'=>'mailchimp_lists_list',
                'options'=> array(),
            );
            SugarChimp_Helper::log('warning',"SugarChimp_Helper::add_mailchimp_list_to_dropdown no dropdown options currently found, create first option." );
        }
        
        // SugarChimp_Helper::log('debug',"SugarChimp_Helper::add_mailchimp_list_to_dropdown current options: ".print_r($result,true));
        if(!empty($result) && is_array($result) && is_array($result['options']))
        {
            if(!array_key_exists ($option['key'] ,$result['options']))
            {
                SugarChimp_Helper::add_option_to_sugar_dropdown($result['name'],$option);
            }
        }
    }
    // borrowed from SugarCRMGroupsToMailchimp scheduler
    public static function get_enum_options($module_object_name,$field_name)
    {
        if (empty($module_object_name) || empty($field_name))
        {
            return false;
        }

        global $app_list_strings;
        $module_bean = BeanFactory::newBean($module_object_name);

        if (empty($app_list_strings[$module_bean->field_defs[$field_name]['options']]) or !is_array($app_list_strings[$module_bean->field_defs[$field_name]['options']]))
        {
            SugarChimp_Helper::log('warning',"SugarChimp_Helper::get_enum_options could not find dropdown options for {$module_object_name}->{$field_name}");
            return array(
                'success'=>false,
                'message'=>"Could not find dropdown options for {$module_object_name}->{$field_name}",
            );
        }

        return array(
            'success'=>true,
            'name' => $module_bean->field_defs[$field_name]['options'],
            'options' => $app_list_strings[$module_bean->field_defs[$field_name]['options']],
        );
    }
    // borrowed from SugarCRMGroupsToMailchimp scheduler
    public static function add_option_to_sugar_dropdown($sugar_dropdown_name,$option)
    {
        $ignore_dropdown_updates = SugarChimp_Setting::retrieve('ignore_dropdown_updates');
        if(empty($ignore_dropdown_updates))
        {
            SugarChimp_Helper::log('debug',"SugarChimp_Helper::add_option_to_sugar_dropdown ignore_dropdown_updates set to true, ignoring.");
            return false;
        }

        if (empty($sugar_dropdown_name) or empty($option))
        {
            return false;
        }

        global $sugar_version, $current_language;

        $data = array();
        $data['dropdown_name'] = $sugar_dropdown_name;
        $data['dropdown_lang'] = empty($current_language) ? 'en_us' : $current_language;
        $_REQUEST['view_package'] = 'studio';

        // always append, never overwrite or remove items from dropdown
        $data['use_push'] = true;
        
        if (preg_match( "/^7.*/", $sugar_version) OR preg_match( "/^8.*/", $sugar_version))
        {
            require_once('modules/ModuleBuilder/parsers/parser.dropdown.php');
            $dh = new ParserDropDown();

            $data['list_value'] = array();
            $data['list_value'][] = array($option['key'],$option['label']);
            $data['list_value'] = json_encode($data['list_value']);
            SugarChimp_Helper::log('debug','SugarChimp_Helper::add_option_to_sugar_dropdown() Sugar7 data: ' . print_r($data,true));
            $dh->saveDropDown($data);

            // if sugar 7, rebuild the cache that holds the dropdown data so that users will see the new options on the next page load
            // Clear out the api metadata languages cache for selected language
            require_once('include/MetaDataManager/MetaDataManager.php');
            MetaDataManager::refreshSectionCache(MetaDataManager::MM_LABELS);
            MetaDataManager::refreshSectionCache(MetaDataManager::MM_ORDEREDLABELS);
        }
        else
        {
            require_once('modules/Studio/DropDowns/DropDownHelper.php');
            $dh = new DropDownHelper();

            $i = 0;
            
            // setup data array
            $data['slot_'. $i] = $i;
            $data['key_'. $i] = htmlspecialchars_decode($option['key']);
            $data['value_'. $i] = htmlspecialchars_decode($option['label']);
            
            SugarChimp_Helper::log('debug','SugarChimp_Helper::add_option_to_sugar_dropdown() Sugar7 data: ' . print_r($data,true));
            $dh->saveDropDown($data);
        }

        return true;
    }
    
    // all outbound curl requests are routed here
    // checks config settings for proxy and applies
    // returns curl object 
    public function initialize_curl()
    {
        $ch = curl_init();

        //$outbound_proxy = '192.168.13.108:443';
        $outbound_proxy = SugarChimp_Setting::retrieve('outbound_proxy');
        
        //$proxyauth = 'user:password';

        if(!empty($outbound_proxy)){
            if($outbound_proxy === 'false' || $outbound_proxy === '0'){
                SugarChimp_Setting::set('outbound_proxy',false);
                return $ch;
            }
            SugarChimp_Helper::log('debug','SugarChimp_Helper::initialize_curl() Using Proxy setting: ' . print_r($outbound_proxy,true));
            curl_setopt($ch, CURLOPT_PROXY, $outbound_proxy);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        }
        
        // implement here if proxy auth is needed
        // if(!empty($proxyauth))
        //     curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);

        return $ch;
    }
    // called from logic_hook
    // when Target List bean's mailchimp_list_name_c is removed
    // This could happen from setting the dropdown option to blank, 
    // or the button on the Health Status page 
    public function cleanUpAfterSyncRemoval($mailchimp_list_id)
    {

        global $db;
        if(empty($mailchimp_list_id))
        {
            return array(
                'success'=>false,
                'message'=>'No mailchimp_list_id provided to remove records from sugarchimp tables.',
            );
        }
        $sql = "DELETE FROM sugarchimp WHERE mailchimp_list_id = '".$mailchimp_list_id."'";
        self::log('debug','SugarChimp_Helper::cleanUpAfterSyncRemoval sugarchimp sql: '.$sql);
        
        if (!$result = $db->query($sql))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Helper::cleanUpAfterSyncRemoval query failed, could not dissassociate sugar list from mailchimp list');
            return false;
        }

        $sql = "DELETE FROM sugarchimp_mc WHERE mailchimp_list_id = '".$mailchimp_list_id."'";
        self::log('debug','SugarChimp_Helper::cleanUpAfterSyncRemoval sugarchimp_mc sql: '.$sql);
        
        if (!$result = $db->query($sql))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Helper::cleanUpAfterSyncRemoval query failed, could not dissassociate sugar list from mailchimp list');
            return false;
        }
        
        $sql = "DELETE FROM sugarchimp_mc_people WHERE mailchimp_list_id = '".$mailchimp_list_id."'";
        self::log('debug','SugarChimp_Helper::cleanUpAfterSyncRemoval sugarchimp_mc_people sql: '.$sql);
        
        if (!$result = $db->query($sql))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Helper::cleanUpAfterSyncRemoval query failed, could not dissassociate sugar list from mailchimp list');
            return false;
        }

        self::log('debug','SugarChimp_Helper::cleanUpAfterSyncRemoval removed mailchimp list: '.print_r($mailchimp_list_id,true).' from all SugarChimp tables.');
        
        return array(
            'success'=>true,
        );
    }
}