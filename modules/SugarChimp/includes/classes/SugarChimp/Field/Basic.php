<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Field/Driver.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Job.php');

class SugarChimp_Field_Basic extends SugarChimp_Field_Driver
{
    // if it's a new record, do this
    public function new_record($bean,$data)
    {
        return array(
            array(
                'name' => 'UpdateMailchimpSubscriber',
                'params' => array(
                    'param1' => $bean->module_dir, // bean module
                    'param2' => $bean->id, // bean id
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                ),
            )
        );
    }
    
    // if it was empty and now has a value, do this
    public function empty_to_filled($bean,$data)
    {
        return array(
            array(
                'name' => 'UpdateMailchimpSubscriber',
                'params' => array(
                    'param1' => $bean->module_dir, // bean module
                    'param2' => $bean->id, // bean id
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                ),
            )
        );
    }
    
    // if it had a value but is now empty, do this
    public function filled_to_empty($bean,$data)
    {
        return array(
            array(
                'name' => 'UpdateMailchimpSubscriber',
                'params' => array(
                    'param1' => $bean->module_dir, // bean module
                    'param2' => $bean->id, // bean id
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                ),
            )
        );
    }
    
    // if the value changed, do this
    public function value_modified($bean,$data)
    {
        return array(
            array(
                'name' => 'UpdateMailchimpSubscriber',
                'params' => array(
                    'param1' => $bean->module_dir, // bean module
                    'param2' => $bean->id, // bean id
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                ),
            )
        );
    }

    // if the value did not change, do this
    public function no_change($bean,$data)
    {
        // don't do anything
        // return false so it continues to check any fields that are left
        return false;
    }
}