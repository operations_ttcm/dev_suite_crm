<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');

abstract class SugarChimp_Field_Driver
{
    public static function forge()
    {
        return new static();
    }
    
    // $bean is NEW data coming in
    // $data is OLD data already in system
    function check($field,$bean,$data=array())
    {
        if (empty($bean))
        {
            SugarChimp_Helper::log("fatal","Tried to check a bean in SugarChimp_Field_Driver but the bean provided is not valid.");
            return false;
        }
        
        SugarChimp_Helper::log("debug","SugarChimp_Field_Driver::check Check {$bean->module_dir} {$bean->id} {$field}");
        
        if (!empty($data) && isset($data[$field]))
        {
            SugarChimp_Helper::log("debug","SugarChimp_Field_Driver::check Old Value: {$data[$field]}");
        }
        
        if (isset($bean->$field) && is_string($bean->$field))
        {
            SugarChimp_Helper::log("debug","SugarChimp_Field_Driver::check New Value: ".print_r($bean->$field,true));
        }
        
        if (empty($data['id']))
        {
            SugarChimp_Helper::log("debug","SugarChimp_Field_Driver::check new record");
            return $this->new_record($bean,$data);
        }
        else if ($data[$field] == '' && $bean->$field != '')
        {
            // we already know the values are set, so just need to check for actual emptiness, not empty()
            SugarChimp_Helper::log("debug","SugarChimp_Field_Driver::check empty to filled");
            return $this->empty_to_filled($bean,$data);
        }
        else if ($data[$field] != '' && $bean->$field == '')
        {
            // we already know the values are set, so just need to check for actual emptiness, not empty()
            SugarChimp_Helper::log("debug","SugarChimp_Field_Driver::check filled to empty");
            return $this->filled_to_empty($bean,$data);
        }
        else if ($data[$field] != $bean->$field)
        {
            SugarChimp_Helper::log("debug","SugarChimp_Field_Driver::check value modified");
            return $this->value_modified($bean,$data);
        }
        else
        {
            SugarChimp_Helper::log("debug","SugarChimp_Field_Driver::check no change");
            return $this->no_change($bean,$data);
        }
    }
    
    // if it's a new record, do this
    abstract public function new_record($bean,$data);
    // if it was empty and now has a value, do this
    abstract public function empty_to_filled($bean,$data);
    // if it had a value but is now empty, do this
    abstract public function filled_to_empty($bean,$data);
    // if the value changed, do this
    abstract public function value_modified($bean,$data);
    // if the value did not change, do this
    abstract public function no_change($bean,$data);
}