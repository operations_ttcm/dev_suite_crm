<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Field/Driver.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Job.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Logger.php');

class SugarChimp_Field_Email extends SugarChimp_Field_Driver
{
    // if it's a new record, do this
    public function new_record($bean,$data)
    {
        // if an email exists and it's neither opted out nor empty, add it
        if (!empty($bean->email1) && $bean->email_opt_out == "0" && $bean->invalid_email == "0")
        {
            return array(
                array(
                    'name' => 'UpdateMailchimpSubscriber',
                    'params' => array(
                        'param1' => $bean->module_dir, // bean module
                        'param2' => $bean->id, // bean id
                        'param3' => '',
                        'param4' => '',
                        'param5' => '',
                    ),
                )
            );
        }
        
        // otherwise, do nothing
        return false;
    }
    
    // if it was empty and now has a value, do this
    public function empty_to_filled($bean,$data)
    {
        global $sugarchimp_mailchimp_email_change;

        // if the email address is neither opted out nor empty, add it
        if ($bean->email_opt_out == "0" && $bean->invalid_email == "0")
        {
            return array(
                array(
                    'name' => 'UpdateMailchimpSubscriber',
                    'params' => array(
                        'param1' => $bean->module_dir, // bean module
                        'param2' => $bean->id, // bean id
                        'param3' => '',
                        'param4' => '',
                        'param5' => '',
                    ),
                )
            );
        }
        
        // otherwise, do nothing
        return false;
    }
    
    // if it had a value but is now empty, do this
    public function filled_to_empty($bean,$data)
    {
        // email address was removed from the record
        // remove this person from all related mailchimp lists
        // we could check if it was already invalid/optout'd out
        // but for good measure go ahead and attempt to remove it
        return array(
            array(
                'name' => 'RemoveMailchimpSubscriber',
                'params' => array(
                    'param1' => $bean->module_dir, // bean module
                    'param2' => $bean->id, // bean id
                    'param3' => $data['email1'], // old email
                    'param4' => '',
                    'param5' => '',
                ),
            )
        );
    }
    
    // if the value changed, do this
    public function value_modified($bean,$data)
    {
        // if the email address is neither opted out nor empty, add it
        if ($bean->email_opt_out == "0" && $bean->invalid_email == "0")
        {
            // 5/19/17 - Jon
            // added relevant param5 for old email's status of invalid or opted out
            // invalid takes precedence over opted out
            $param5 = '';
            if($data['invalid_email'] == "0"){
                $param5 = 'invalid';
            }
            else if($data['email_opt_out'] == "1"){
                $param5 = 'optout';
            }
            
            return array(
                array(
                    'name' => 'UpdateMailchimpSubscriberEmailChange',
                    'params' => array(
                        'param1' => $bean->module_dir, // bean module
                        'param2' => $bean->id, // bean id
                        'param3' => $data['email1'], // old email
                        'param4' => $bean->email1, // new email
                        'param5' => $param5,
                    ),
                )
            );
        }

        if ($bean->email_opt_out == "1")
        {
            return array(
                array(
                    'name' => 'RemoveMailchimpSubscriber',
                    'params' => array(
                        'param1' => $bean->module_dir, // bean module
                        'param2' => $bean->id, // bean id
                        'param3' => $data['email1'], // old email
                        'param4' => 'optout',
                        'param5' => '',
                    ),
                )
            );
        }
        
        if ($bean->invalid_email == "1")
        {
            return array(
                array(
                    'name' => 'RemoveMailchimpSubscriber',
                    'params' => array(
                        'param1' => $bean->module_dir, // bean module
                        'param2' => $bean->id, // bean id
                        'param3' => $data['email1'], // old email
                        'param4' => 'invalid',
                        'param5' => '',
                    ),
                )
            );
        }        
        
        // otherwise, do nothing
        return false;
    }

    // if the value did not change, do this
    public function no_change($bean,$data)
    {
        SugarChimp_Logger::log('debug','email field no change: $bean->email1: '.print_r($bean->email1,true));
        SugarChimp_Logger::log('debug','email field no change: $bean->email_opt_out: '.print_r($bean->email_opt_out,true));
        SugarChimp_Logger::log('debug','email field no change: $bean->invalid_email: '.print_r($bean->invalid_email,true));
        SugarChimp_Logger::log('debug','email field no change: $data[email_opt_out]: '.print_r($data['email_opt_out'],true));
        SugarChimp_Logger::log('debug','email field no change: $data[invalid_email]: '.print_r($data['invalid_email'],true));

        // if the email address is not empty and the optout status changed from 0 to 1, queue it for removal
        if (!empty($bean->email1) && ($data['email_opt_out'] == "0" && $bean->email_opt_out == "1"))
        {
            return array(
                array(
                    'name' => 'RemoveMailchimpSubscriber',
                    'params' => array(
                        'param1' => $bean->module_dir, // bean module
                        'param2' => $bean->id, // bean id
                        'param3' => $bean->email1, // email that was marked invalid or opted out
                        'param4' => 'optout',
                        'param5' => '',
                    ),
                )
            );
        }
        
        // if the email address is not empty and the invalid status changed from 0 to 1, queue it for removal
        if (!empty($bean->email1) && ($data['invalid_email'] == "0" && $bean->invalid_email == "1"))
        {
            return array(
                array(
                    'name' => 'RemoveMailchimpSubscriber',
                    'params' => array(
                        'param1' => $bean->module_dir, // bean module
                        'param2' => $bean->id, // bean id
                        'param3' => $bean->email1, // email that was marked invalid or opted out
                        'param4' => 'invalid',
                        'param5' => '',
                    ),
                )
            );
        }
        
        // if the email address is not empty and the optout status changed from 1 to 0 and invalid email is 0, queue the person to be added
        if (!empty($bean->email1) && ($data['email_opt_out'] == "1" && $bean->email_opt_out == "0" && $bean->invalid_email == "0"))
        {
            $result = SugarChimp_Setting::remove_from_optout_tracking_table($bean->email1);

            if (empty($result))
            {
                SugarChimp_Logger::log('fatal','SugarChimp_Field_Email::no_change:opted out to opted back in: email could not be removed from optout tracking table. email: '.print_r($bean->email1,true));
            }

            return array(
                array(
                    'name' => 'UpdateMailchimpSubscriber',
                    'params' => array(
                        'param1' => $bean->module_dir, // bean module
                        'param2' => $bean->id, // bean id
                        'param3' => '',
                        'param4' => '',
                        'param5' => '',
                    ),
                )
            );
        }
        
        // if the email address is not empty and the invalid status changed from 1 to 0 and email opt out is 0, queue the person to be added
        if (!empty($bean->email1) && ($data['invalid_email'] == "1" && $bean->invalid_email == "0" && $bean->email_opt_out == "0"))
        {
            $result = SugarChimp_Setting::remove_from_optout_tracking_table($bean->email1);

            if (empty($result))
            {
                SugarChimp_Logger::log('fatal','SugarChimp_Field_Email::no_change:invalid to valid: email could not be removed from optout tracking table. email: '.print_r($bean->email1,true));
            }

            return array(
                array(
                    'name' => 'UpdateMailchimpSubscriber',
                    'params' => array(
                        'param1' => $bean->module_dir, // bean module
                        'param2' => $bean->id, // bean id
                        'param3' => '',
                        'param4' => '',
                        'param5' => '',
                    ),
                )
            );
        }
        
        // otherwise, do nothing
        return false;
    }
}
