<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Field/Driver.php');

class SugarChimp_Field_MailchimpListName extends SugarChimp_Field_Driver
{
    // if it's a new record, do this
    public function new_record($bean,$data)
    {
        SugarChimp_Helper::log('debug','SugarChimp_Field_MailchimpListName new_record');
        if (!empty($bean->mailchimp_list_name_c))
        {
            SugarChimp_Helper::push_lists_to_mailchimp(array($bean));
            SugarChimp_Helper::queue_add_webhooks_to_lists(array($bean->mailchimp_list_name_c));
        }
    }
    
    // if it was empty and now has a value, do this
    public function empty_to_filled($bean,$data)
    {
        SugarChimp_Helper::log('debug','SugarChimp_Field_MailchimpListName empty_to_filled');
        if (!empty($bean->mailchimp_list_name_c))
        {
            SugarChimp_Helper::push_lists_to_mailchimp(array($bean));
            SugarChimp_Helper::queue_add_webhooks_to_lists(array($bean->mailchimp_list_name_c));
        }
    }
    
    // if it had a value but is now empty, do this
    public function filled_to_empty($bean,$data)
    {
        SugarChimp_Helper::log('debug','SugarChimp_Field_MailchimpListName filled_to_empty');
        $mc_list_id = $data['mailchimp_list_name_c'];
        SugarChimp_Helper::log('debug','SugarChimp_Field_MailchimpListName - run cleanUpAfterSyncRemoval for list: '.print_r($mc_list_id,true));
        SugarChimp_Helper::cleanUpAfterSyncRemoval($mc_list_id);
    }
    
    // if the value changed, do this
    public function value_modified($bean,$data)
    {
        SugarChimp_Helper::log('debug','SugarChimp_Field_MailchimpListName value_modified');
        if (!empty($bean->mailchimp_list_name_c))
        {
            SugarChimp_Helper::push_lists_to_mailchimp(array($bean));
            SugarChimp_Helper::queue_add_webhooks_to_lists(array($bean->mailchimp_list_name_c));

            // queue old mc list items to be removed from queues
            $mc_list_id = $data->mailchimp_list_name_c;
            SugarChimp_Helper::cleanUpAfterSyncRemoval($mc_list_id);

        }
    }

    // if the value did not change, do this
    public function no_change($bean,$data)
    {
        SugarChimp_Helper::log('debug','SugarChimp_Field_MailchimpListName no_change');
    }
}