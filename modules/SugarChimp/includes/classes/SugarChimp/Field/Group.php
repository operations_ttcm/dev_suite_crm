<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Field/Driver.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Job.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/FieldMap.php');

require_once('modules/SugarChimp/includes/classes/Mailchimp/API.php');

class SugarChimp_Field_Group extends SugarChimp_Field_Driver
{
	protected static $cached_mailchimp_groups = array();
	protected static $cached_mailchimp_groupsv2 = array();

	public static function sync_sugar_dropdowns_with_mailchimp_groups($sugar_to_mailchimp=true,$mailchimp_to_sugar=true)
	{
		// the reason we have to do all of this work is so that we 
		// 1) can guarantee that we know of all possible MC interest categories
		//    and the interests inside of the categories. We need this so that
		// 2) when we send updates to mailchimp, we are able to properly set the 
		//    values if a group is being synced

		SugarChimp_Helper::log('debug','SugarChimp_Field_Group::sync_sugar_dropdowns_with_mailchimp_groups get supported modules and all synced lists.');
		
		$supported_modules = SugarChimp_FieldMap::get_supported_modules();
		$synced_lists = SugarChimp_Helper::get_synced_lists();

		// the first part goes through each synced list and 
		// foreach group field, it will get the interests from mailchimp
		// and set the $field['param1'] field with an array of the values
		// this param1 is used later in this function as well as other
		// places throughout SC. It's important the param1 fields represents
		// the interests of an interest category in MC as closely as possible

		SugarChimp_Helper::log('debug','SugarChimp_Field_Group::sync_sugar_dropdowns_with_mailchimp_groups param1 sync 1 begin');

		foreach ($synced_lists as $synced_list)
		{
			// foreach group field, update param1
			foreach ($supported_modules as $supported_module)
			{
				$updated = self::update_mc_group_mapping($synced_list['mailchimp_list_id'],$supported_module);

				if ($updated !== true)
				{
					continue;
				}
			}
			SugarChimp_Helper::log('debug','SugarChimp_Field_Group::sync_sugar_dropdowns_with_mailchimp_groups mappings updated for mailchimp list: '.print_r($synced_list['mailchimp_list_id'],true));
		}

		SugarChimp_Helper::log('debug','SugarChimp_Field_Group::sync_sugar_dropdowns_with_mailchimp_groups compare sugar dropdowns with mailchimp groups begin');

		// we go through all mapped group fields and fill up two arrays
		// the mailchimp categories array holds all the information about a mailchimp
		// category that is synced to a sugar field and the sugar_dropdowns array
		// holds all the information about the sugar dropdowns that are synced with
		// mailchimp interest categories.

		$mailchimp_categories = array();
		$sugar_dropdowns = array();
		$mailchimp_category_queue = array();

		foreach ($synced_lists as $synced_list)
		{
			$list_id = $synced_list['mailchimp_list_id'];
			$group_fields = SugarChimp_Field_Group::get_group_fields($list_id,$supported_modules);

			foreach ($group_fields as $module => $module_fields)
			{
	            if (empty($module_fields) or !is_array($module_fields))
				{
					continue;
				}

				foreach ($module_fields as $name => $field)
				{
					SugarChimp_Helper::log('debug',$synced_list['mailchimp_list_id'] . ' : ' . $module . ' : ' . $field['mapping']['mailchimp_name']);

					$base_module = $module;
					$base_field = $field['mapping']['sugarcrm_name'];

					// make sure the right data is being checked if it's a related mapped field
					// set the proper base module and base field
					if (SugarChimp_Field::is_related_field($field['mapping']['sugarcrm_name']))
					{
		                // make sure current SC subscription supports parent related field mapping
		                if (SugarChimp_FieldMap::is_related_field_mapping_supported() !== true)
		                {
		                    SugarChimp_Helper::log('debug','SugarChimp_FieldMap::get_merge_var_update_array_from_bean: current plan does not support related parent field mapping');
		                    continue;
		                }

						$temp_bean = BeanFactory::getBean($module);

						if (empty($temp_bean))
						{
		                    SugarChimp_Helper::log('fatal','SugarChimp_Field_Group sync_sugar_dropdowns_with_mailchimp_groups could not create temp bean for module: '.print_r($module,true));
		                    continue;
						}

						$field_parts = SugarChimp_Field::get_related_field_parts($field['mapping']['sugarcrm_name']);

						if (empty($field_parts['link_field_name']))
		                {
		                    SugarChimp_Helper::log('fatal','SugarChimp_Field_Group sync_sugar_dropdowns_with_mailchimp_groups link_field_name is empty. Cannot sync related field: '.print_r($field['mapping']['sugarcrm_name'],true));
		                    continue;
		                }

		                if (empty($field_parts['parent_field_name']))
		                {
		                    SugarChimp_Helper::log('fatal','SugarChimp_Field_Group sync_sugar_dropdowns_with_mailchimp_groups parent_field_name is empty. Cannot sync related field: '.print_r($field['mapping']['sugarcrm_name'],true));
		                    continue;
		                }

		                $link_field_name = $field_parts['link_field_name'];
		                $parent_field_name = $field_parts['parent_field_name'];

		                if (!method_exists($temp_bean, 'load_relationship'))
		                {
		                    // load relationship method does not exist on bean
		                    SugarChimp_Helper::log('fatal','SugarChimp_Field_Group sync_sugar_dropdowns_with_mailchimp_groups load relationship method does not exit on bean');
		                    SugarChimp_Helper::log('fatal','SugarChimp_Field_Group sync_sugar_dropdowns_with_mailchimp_groups Cannot load options for related field: '.print_r($field['mapping']['sugarcrm_name'],true));
		                    continue;
		                }

		                if (!$temp_bean->load_relationship($link_field_name))
		                {
		                    // failed to load relationship
		                    SugarChimp_Helper::log('fatal','SugarChimp_Field_Group sync_sugar_dropdowns_with_mailchimp_groups load relationship could not be called on link_field_name: '.print_r($link_field_name,true));
		                    SugarChimp_Helper::log('fatal','SugarChimp_Field_Group sync_sugar_dropdowns_with_mailchimp_groups Cannot load options for related field: '.print_r($field['mapping']['sugarcrm_name'],true));
		                    continue;
		                }

		                if (!method_exists($temp_bean->$link_field_name, 'getRelatedModuleName'))
		                {
		                    // failed to load relationship
		                    SugarChimp_Helper::log('fatal','SugarChimp_Field_Group sync_sugar_dropdowns_with_mailchimp_groups function getRelatedModuleName does not exist on link relationship');
		                    SugarChimp_Helper::log('fatal','SugarChimp_Field_Group sync_sugar_dropdowns_with_mailchimp_groups Cannot load options for related field: '.print_r($field['mapping']['sugarcrm_name'],true));
		                    continue;
		                }

		                $related_module_name = $temp_bean->$link_field_name->getRelatedModuleName();

		                if (empty($related_module_name))
		                {
							SugarChimp_Helper::log('fatal','SugarChimp_Field_Group sync_sugar_dropdowns_with_mailchimp_groups could not find module name of related parent field');
		                    continue;
		                }

						$base_module = $related_module_name;
						$base_field = $parent_field_name;
					}

					// SugarChimp_Helper::log('debug',"SugarChimp_Field_Group::sync_sugar_dropdowns_with_mailchimp_groups base_module: ".print_r($base_module,true));
					// SugarChimp_Helper::log('debug',"SugarChimp_Field_Group::sync_sugar_dropdowns_with_mailchimp_groups base_field: ".print_r($base_field,true));
		            
		            // get the sugarcrm options for the enum or multienum
					$options = SugarChimp_FieldMap::get_enum_options($base_module,$base_field);
		            if (empty($options['options']) || !is_array($options['options']))
		            {
		                SugarChimp_Helper::log('warning',"SugarChimp_Field_Group::sync_sugar_dropdowns_with_mailchimp_groups could not retrieve dropdown options for {$module} {$field['mapping']['sugarcrm_name']} mailchimp groups could not be updated");
		                continue;
		            }

		            // SugarChimp_Helper::log('debug',"SugarChimp_Field_Group::sync_sugar_dropdowns_with_mailchimp_groups found dropdown options for {$field['mapping']['mailchimp_name']}: ".print_r($field['param1'],true));
		            // SugarChimp_Helper::log('debug',"SugarChimp_Field_Group::sync_sugar_dropdowns_with_mailchimp_groups found dropdown options for {$field['mapping']['sugarcrm_name']}: ".print_r($options,true));
		            
		            // load up the sugar dropdown array
		            // if it's already setup, just add the new mc id to the mc ids array
		            if (empty($sugar_dropdowns[$options['name']]))
		            {
			            $sugar_dropdowns[$options['name']] = array(
			            	'id' => $options['name'],
			            	'old' => $options['options'],
			            	'new' => array(),
			            	'mailchimp_ids' => array($field['mapping']['mailchimp_name']),
			            );
			        }
			        else
			        {
			        	$sugar_dropdowns[$options['name']]['mailchimp_ids'] []= $field['mapping']['mailchimp_name'];
			        }

			        // load up the mc category array
			        // if it's already setup, just add the new sugar dropdown id to the sugar dropdown ids array
		            if (empty($mailchimp_categories[$field['mapping']['mailchimp_name']]))
			        {
						$mailchimp_categories[$field['mapping']['mailchimp_name']] = array(
							'id' => $field['mapping']['mailchimp_name'],
							'old' => $field['param1'],
							'new' => array(),
							'sugar_dropdown_ids' => array($options['name']),
							'mailchimp_list_id' => $list_id,
						);
					}
					else
					{
			        	$mailchimp_categories[$field['mapping']['mailchimp_name']]['sugar_dropdown_ids'] []= $options['name'];
					}

					// seed the initial queue of things to process
					$mailchimp_category_queue [$field['mapping']['mailchimp_name']] = $field['mapping']['mailchimp_name'];
				}
			}
		}

		// now that all of the arrays have been loaded up with the proper data
		// we run through each of the items in the mailchimp category queue
		// we compare sugar values iwth mailchimp values then do the opposite
		// since the mailchimp side is unique, it will only be updated once
		// but since sugar dropdowns can be linked to multiple mailchimp groups
		// wee need to requeue the mailchimp interest group ids any time its
		// related sugar dropdown gets updated. we continue to do this until the
		// queue of mailchimp category ids is empty

		do
		{
			foreach ($mailchimp_category_queue as $mc_id)
			{
				SugarChimp_Helper::log('debug','------------------------------------------------------');
				SugarChimp_Helper::log('debug','queue: '.print_r($mailchimp_category_queue,true));
				SugarChimp_Helper::log('debug','mc_id: '.print_r($mc_id,true));
				
				$mailchimp_category = $mailchimp_categories[$mc_id];

				// go through all of the sugar dropdowns associated with this maichimp category
				foreach ($mailchimp_category['sugar_dropdown_ids'] as $sugar_id)
				{
					$sugar_dropdown = $sugar_dropdowns[$sugar_id];

					// combine the currently stored values and the new ones we know about
					$mailchimp_category_combined = array_merge($mailchimp_category['old'],$mailchimp_category['new']);
					$sugar_dropdown_combined = array_merge($sugar_dropdown['old'],$sugar_dropdown['new']);
					
					// find what exists in MailChimp that does not exist in Sugar
					$exists_in_mailchimp_not_sugar = array_diff($mailchimp_category_combined,$sugar_dropdown_combined);
					
					// find what exists in Sugar that does not exist in MailChimp
					$exists_in_sugar_not_mailchimp = array_diff($sugar_dropdown_combined,$mailchimp_category_combined);
					
					// if any exist in mailchimp but not in sugar
					// add to new sugar array and requeue related mailchimp list ids
					if (!empty($exists_in_mailchimp_not_sugar))
					{
						$sugar_dropdowns[$sugar_id]['new'] = array_merge($sugar_dropdowns[$sugar_id]['new'],$exists_in_mailchimp_not_sugar);

						// load up the queue for double checking
						foreach ($sugar_dropdown['mailchimp_ids'] as $mailchimp_id)
						{
							$mailchimp_category_queue[$mailchimp_id] = $mailchimp_id;	
						}
					}

					// if any exist in sugar that do not exist in mailchimp
					// add to new mailchimp array 
					if (!empty($exists_in_sugar_not_mailchimp))
					{
						$mailchimp_categories[$mc_id]['new'] = array_merge($mailchimp_categories[$mc_id]['new'],$exists_in_sugar_not_mailchimp);

						// load up the related mc cat ids that are related to this mailchimp category's sugar queue for double checking
						foreach ($mailchimp_category['sugar_dropdown_ids'] as $linked_sugar_id)
						{
							foreach ($sugar_dropdowns[$linked_sugar_id]['mailchimp_ids'] as $linked_mailchimp_id)
							{
								$mailchimp_category_queue[$linked_mailchimp_id] = $linked_mailchimp_id;
							}
						}
					}
				}

				// remove the already processed id from the queue
				unset($mailchimp_category_queue[$mc_id]);

				SugarChimp_Helper::log('debug','queue: '.print_r($mailchimp_category_queue,true));
			}
		}
		while (!empty($mailchimp_category_queue) && is_array($mailchimp_category_queue));

		SugarChimp_Helper::log('debug','sugar dropdowns: '.print_r($sugar_dropdowns,true));
		SugarChimp_Helper::log('debug','mailchimp cats : '.print_r($mailchimp_categories,true));

		// once the queue is empty, add the new items to the sugar dropdowns
		// and the new interests to the mailchimp list interest categories

		foreach ($sugar_dropdowns as $sugar_dropdown_id => $sugar_dropdown)
		{
			$result = SugarChimp_Field_Group::add_options_to_sugar_dropdown($sugar_dropdown_id,$sugar_dropdown['new']);
		}

		// if a mailchimp category gets updated we need to flag it so we 
		// can later re-update its param1 field on the sugar side for future syncing jobs
		$mailchimp_categories_with_updates = array();
		foreach ($mailchimp_categories as $mailchimp_id => $mailchimp_category)
		{
			foreach ($mailchimp_category['new'] as $new_interest)
			{
				if (!empty($new_interest))
				{
					$mailchimp_categories_with_updates[$mailchimp_id] = $mailchimp_id;
					$result = MailChimp_API::add_interest($mailchimp_category['mailchimp_list_id'],$mailchimp_id,$new_interest);
				}
			}
		}

		// if data changed on the mailchimp side, go re-update the param1
		// fields from mailchimp so we can be sure we have the latest data
		// if we do not do this, there may be fields that need to be synced
		// on the scheduler jobs that come after this, so need to make sure
		// we have the new interest ids for the newly created interests

		SugarChimp_Helper::log('debug','SugarChimp_Field_Group::sync_sugar_dropdowns_with_mailchimp_groups param1 sync 2 begin');

		if (!empty($mailchimp_categories_with_updates) && is_array($mailchimp_categories_with_updates))
		{
			foreach ($synced_lists as $synced_list)
			{
				// foreach group field, update param1
				foreach ($supported_modules as $supported_module)
				{
					$updated = self::update_mc_group_mapping($synced_list['mailchimp_list_id'],$supported_module,$mailchimp_categories_with_updates);

					if ($updated !== true)
					{
						continue;
					}

				}
			}
		}

		SugarChimp_Helper::log('debug','SugarChimp_Field_Group::sync_sugar_dropdowns_with_mailchimp_groups complete');

		return true;
	}

	public static function update_mc_group_mapping($list_id,$module,$mailchimp_category_ids=array())
	{
		$mapping_changed = false;
		SugarChimp_Helper::log('debug','SugarChimp_Field_Group::update_mc_group_mapping start');

		$mapping = SugarChimp_FieldMap::get_field_mappings($list_id,$module);

		if (empty($mapping['fields']) or !is_array($mapping['fields']))
		{
			SugarChimp_Helper::log('fatal','SugarChimp_Field_Group::update_mc_group_mapping: there are no fields in the mapping for list: '.print_r($synced_list,true));
			return false;
		}

		$check_for_category_id = false;
		if (!empty($mailchimp_category_ids) && is_array($mailchimp_category_ids))
		{
			$check_for_category_id = true;
		}

		foreach ($mapping['fields'] as $field_name => $field)
		{
			SugarChimp_Helper::log('debug','SugarChimp_Field_Group::update_mc_group_mapping - '.print_r($field_name,true)." => ".print_r($field,true));

			if (!empty($field['type']) && $field['type'] == 'Group' && !empty($field['mapping']['mailchimp_name']))
			{
				if ($check_for_category_id === true && !in_array($field['mapping']['mailchimp_name'],$mailchimp_category_ids))
				{
					SugarChimp_Helper::log('debug','this is a synced group field, but it is not in the mailchimp category ids array: '.print_r($field['mapping']['mailchimp_name'],true).' - '.print_r($mailchimp_category_ids,true));
					continue;
				}

				$mailchimp_interest_category_id = $field['mapping']['mailchimp_name'];
				
				// go get the interests and update param1
				$mapping_changed = true;

				$categories = MailChimp_API::get_interests($list_id,$mailchimp_interest_category_id,array('count'=>100,'fields'=>'interests.id,interests.name'));
				
				if (empty($categories['interests']) or !is_array($categories['interests']))
				{
					SugarChimp_Helper::log('warning','SugarChimp_Field_Group::update_mc_group_mapping: there are no interests or there was an error retrieving them: '.print_r($list_id,true)." : ".print_r($mailchimp_interest_category_id,true));
					continue;
				}

				SugarChimp_Helper::log('debug','SugarChimp_Field_Group::update_mc_group_mapping: categories: '.print_r($categories,true));

				$category_array = array();
				foreach ($categories['interests'] as $category)
				{
					$category_array[$category['id']] = $category['name'];
				}

				SugarChimp_Helper::log('debug','SugarChimp_Field_Group::update_mc_group_mapping: category_array: '.print_r($category_array,true));

				$mapping['fields'][$field_name]['param1'] = $category_array;
			}
		}

		if ($mapping_changed === true)
		{
			// save the mapping
			SugarChimp_FieldMap::save_field_mappings($list_id,$module,$mapping);
		}

		SugarChimp_Helper::log('debug','SugarChimp_Field_Group::update_mc_group_mapping end');

		return true;
	}

	public static function get_group_fields($list_id,$modules)
	{
		if (empty($list_id) or empty($modules))
		{
			return false;
		}

		$group_fields = array();

		foreach ($modules as $module)
		{
			$mapping = SugarChimp_FieldMap::get_field_mappings($list_id,$module);

			if (empty($mapping) or empty($mapping['fields']) or !is_array($mapping['fields']))
			{
				SugarChimp_Helper::log('debug','SugarChimp_Field_Group::get_group_fields no mappings found for module '.$module.' on list '.$list_id);
				continue;
			}

			foreach ($mapping['fields'] as $name => $field)
			{
				if (!empty($field) && !empty($field['type']) && $field['type'] == 'Group')
				{
					if (empty($group_fields[$module])) $group_fields[$module] = array();

					SugarChimp_Helper::log('debug','SugarChimp_Field_Group::get_group_fields found group field for list '.$list_id.': '.print_r(array($name=>$field),true));
					$group_fields[$module][$name] = $field;
				}
			}
		}

		return $group_fields;
	}

	public static function add_options_to_sugar_dropdown($sugar_dropdown_name,$groups_to_add)
	{
		$ignore_dropdown_updates = SugarChimp_Setting::retrieve('ignore_dropdown_updates');
        if(!empty($ignore_dropdown_updates))
        {
            SugarChimp_Helper::log('fatal',"SugarChimp_Field_Group::add_options_to_sugar_dropdown ignore_dropdown_updates set to true, possible error in syncing your Groups from MailChimp.");
            return false;
        }

		if (empty($sugar_dropdown_name) or empty($groups_to_add))
        {
            return false;
        }

        global $sugar_version, $current_language;

		$data = array();
		$data['dropdown_name'] = $sugar_dropdown_name;
		$data['dropdown_lang'] = empty($current_language) ? 'en_us' : $current_language;
		$_REQUEST['view_package'] = 'studio';

		// always append, never overwrite or remove items form dropdown
		$data['use_push'] = true;
				
		if (preg_match( "/^7.*/", $sugar_version) or preg_match( "/^8.*/", $sugar_version))
        {
	        require_once('modules/ModuleBuilder/parsers/parser.dropdown.php');
	        $dh = new ParserDropDown();

	        $i = 0;
	        $data['list_value'] = array();
			foreach ($groups_to_add as $group)
			{
				// get a safe string for the key array
				$key = SugarChimp_Field_Group::convert_string_to_array_key($group);

				if (empty($key)) continue;

				// setup data array
				$data['list_value'][] = array($key,$group);
			}

			$data['list_value'] = json_encode($data['list_value']);

			$dh->saveDropDown($data);

			// if sugar 7, rebuild the cache that holds the dropdown data so that users will see the new options on the next page load
        	// Clear out the api metadata languages cache for selected language
	        require_once('include/MetaDataManager/MetaDataManager.php');
	        MetaDataManager::refreshSectionCache(MetaDataManager::MM_LABELS);
	        MetaDataManager::refreshSectionCache(MetaDataManager::MM_ORDEREDLABELS);
		}
		else
		{
			require_once('modules/Studio/DropDowns/DropDownHelper.php');
			$dh = new DropDownHelper();

			$i = 0;
			foreach ($groups_to_add as $group)
			{
				// get a safe string for the key array
				$key = SugarChimp_Field_Group::convert_string_to_array_key($group);

				if (empty($key)) continue;

				// setup data array
				$data['slot_'. $i] = $i;
				$data['key_'. $i] = htmlspecialchars_decode($key);
				$data['value_'. $i] = htmlspecialchars_decode($group);

				$i++;
			}

			$dh->saveDropDown($data);
		}

		return true;
	}

	// generate slug-like keys for the mailchimp group to sugar option method http://stackoverflow.com/questions/2955251/php-function-to-make-slug-url-string
	// need to make sure they are a string that can be saved in a php array key
	// edge case: duplicate keys can be created if you had something like Group!!! and Group### as both would get transformed to 'group'
	// this is a lossy method of doing this, but should cover 99% of the cases, most likely issue will come up with foreign languages foreign characters
    public static function convert_string_to_array_key($string)
    {
        if (empty($string))
        {
        	SugarChimp_Helper::log('warning','SugarChimp_Field_Group::convert_string_to_array_key string is empty: '.print_r($string,true));
            return '';
        }

        $original_string = $string;

        // using method found here http://stackoverflow.com/questions/2955251/php-function-to-make-slug-url-string
        $string = preg_replace('~[^\\pL\d]+~u', '_', $string); // replace non letter or digits by _
        $string = trim($string, '-'); // trim
        $string = iconv('utf-8', 'us-ascii//TRANSLIT', $string); // transliterate        
        $string = strtolower($string); // lowercase
        $string = preg_replace('~[^-\w]+~', '', $string); // remove unwanted characters

        if (empty($string))
        {
            SugarChimp_Helper::log('warning','SugarChimp_Field_Group::convert_string_to_array_key was empty after slugging. orginal: '.print_r($original_string,true));
            return 'n-a';
        }

        SugarChimp_Helper::log('debug','SugarChimp_Field_Group::convert_string_to_array_key slugged from '.print_r($original_string,true).' to '.print_r($string,true));
        return $string;
    }

    // if it's a new record, do this
    public function new_record($bean,$data)
    {
		return array(
            array(
                'name' => 'UpdateMailchimpSubscriberGroup',
                'params' => array(
                    'param1' => $bean->module_dir, // bean module
                    'param2' => $bean->id, // bean id
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                ),
            )
        );
    }
    
    // if it was empty and now has a value, do this
    public function empty_to_filled($bean,$data)
    {
		return array(
            array(
                'name' => 'UpdateMailchimpSubscriberGroup',
                'params' => array(
                    'param1' => $bean->module_dir, // bean module
                    'param2' => $bean->id, // bean id
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                ),
            )
        );
    }
    
    // if it had a value but is now empty, do this
    public function filled_to_empty($bean,$data)
    {
		return array(
            array(
                'name' => 'UpdateMailchimpSubscriberGroup',
                'params' => array(
                    'param1' => $bean->module_dir, // bean module
                    'param2' => $bean->id, // bean id
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                ),
            )
        );
    }
    
    // if the value changed, do this
    public function value_modified($bean,$data)
    {
		return array(
            array(
                'name' => 'UpdateMailchimpSubscriberGroup',
                'params' => array(
                    'param1' => $bean->module_dir, // bean module
                    'param2' => $bean->id, // bean id
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                ),
            )
        );
    }

    // if the value did not change, do this
    public function no_change($bean,$data)
    {
    	// do nothing
        return false;
    }
}