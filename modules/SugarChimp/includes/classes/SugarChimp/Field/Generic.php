<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Field/Driver.php');

class SugarChimp_Field_Generic extends SugarChimp_Field_Driver
{
    // if it's a new record, do this
    public function new_record($bean,$data)
    {
        return 'new_record';
    }
    
    // if it was empty and now has a value, do this
    public function empty_to_filled($bean,$data)
    {
        return 'empty_to_filled';
    }
    
    // if it had a value but is now empty, do this
    public function filled_to_empty($bean,$data)
    {
        return 'filled_to_empty';
    }
    
    // if the value changed, do this
    public function value_modified($bean,$data)
    {
        return 'value_modified';
    }

    // if the value did not change, do this
    public function no_change($bean,$data)
    {
        return 'no_change';
    }
}