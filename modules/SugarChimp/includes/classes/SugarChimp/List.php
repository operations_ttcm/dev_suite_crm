<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Loader.php');
SugarChimp_Loader::load('Core/fList');

use Fanatical\Core\v1a\fList as fList;

require_once('modules/SmartList/includes/classes/SmartList/List.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Logger.php');
require_once('modules/SugarChimp/includes/classes/Mailchimp/API.php');
require_once('modules/SugarChimp/includes/vendor/Mailchimp/Mailchimp.php');

class SugarChimp_List extends fList
{
    public static $default_mailchimp_list = array(
        'permission_reminder' => 'You have subscribed to this list.',
        'email_type_option' => true,
        'contact' => array(
            "company" => 'Company',
            "address1" => 'Default Address',
            "city" => 'City',
            "state" => 'State',
            "zip" => 'Zip Code',
            "country" => 'Country'
        ),
        'campaign_defaults' => array(
            "from_name" => 'Campaigner',
            "from_email" => 'campaigner@company.com',
            "subject" => 'Campaign Subject',
            "language" => 'en'
        ),
    );

    // same parameters and return as get_sugar_lists
    // returns only those sugar lists that are synced with a mailchimp list
    public static function get_synced_sugar_lists($order_by='name',$where='',$row_offset=0,$limit=-1,$max=-1,$show_deleted=0,$single_select=false,$select_fields=array())
    {
        // setup the filter to get only those sugar target lists that are synced to a mailchimp list
        $is_synced_where =  " (mailchimp_list_name_c IS NOT NULL AND mailchimp_list_name_c != '') ";
        if (!empty($where) and is_string($where))
        {
            $where .= " AND " . $is_synced_where;
        }
        else
        {
            $where = $is_synced_where;
        }

        return static::get_sugar_lists($order_by,$where,$row_offset,$limit,$max,$show_deleted,$single_select,$select_fields);
    }
    // return only a single sugar list
    public static function get_sugar_list_by_id($list_id)
    {
        if (!empty($list_id))
        {
            $where = "(id = '". $list_id ."') ";
        }
        return static::get_sugar_lists($where);
    }

    // same parameters and return as get_sugar_lists
    // returns only those sugar lists that are NOT synced with a mailchimp list
    public static function get_not_synced_sugar_lists($order_by='name',$where='',$row_offset=0,$limit=-1,$max=-1,$show_deleted=0,$single_select=false,$select_fields=array())
    {
        // setup the filter to get only those sugar target lists that are synced to a mailchimp list
        $is_not_synced_where =  " (mailchimp_list_name_c IS NULL OR mailchimp_list_name_c = '') ";
        if (!empty($where) and is_string($where))
        {
            $where .= " AND " . $is_not_synced_where;
        }
        else
        {
            $where = $is_not_synced_where;
        }

        return static::get_sugar_lists($order_by,$where,$row_offset,$limit,$max,$show_deleted,$single_select,$select_fields);
    }

    // returns array of the following
    // 'total' => int 17 <- number of lists returned
    // 'data' => 
    // array (size=16)
    //   0 => 
    //     array (size=17)
    //       'id' => string '4afd9eb587' (length=10)
    //       'web_id' => int 462337
    //       'name' => string 'Shep first List' (length=15)
    //       'date_created' => string '2015-08-20 21:29:42' (length=19)
    //       'email_type_option' => boolean false
    //       'use_awesomebar' => boolean true
    //       'default_from_name' => string 'SugarChimp Test' (length=15)
    //       'default_from_email' => string 'jon@sugarchimp.com' (length=18)
    //       'default_subject' => string '' (length=0)
    //       'default_language' => string 'en' (length=2)
    //       'list_rating' => int 0
    //       'subscribe_url_short' => string 'http://eepurl.com/bwFXYD' (length=24)
    //       'subscribe_url_long' => string 'http://sugarchimp.us3.list-manage2.com/subscribe?u=cce4d32c4e528f0fc60c8da69&id=4afd9eb587' (length=90)
    //       'beamer_address' => string 'us3-4f589427cd-9a581b3f90@inbound.mailchimp.com' (length=47)
    //       'visibility' => string 'pub' (length=3)
    //       'stats' => 
    //         array (size=16)
    //           'member_count' => int 2
    //           'unsubscribe_count' => int 0
    //           'cleaned_count' => int 0
    //           'member_count_since_send' => int 2
    //           'unsubscribe_count_since_send' => int 0
    //           'cleaned_count_since_send' => int 0
    //           'campaign_count' => int 0
    //           'grouping_count' => int 0
    //           'group_count' => int 0
    //           'merge_var_count' => int 4
    //           'avg_sub_rate' => int 0
    //           'avg_unsub_rate' => int 0
    //           'target_sub_rate' => int 0
    //           'open_rate' => int 0
    //           'click_rate' => int 0
    //           'date_last_campaign' => null
    //       'modules' => 
    //         array (size=0)
    //           empty
    // 'errors' => array() <- array of any errors returned
    public static function get_mailchimp_lists($filters=array(), $start=0, $limit=100, $sort_field='name', $sort_dir='ASC')
    {
        $apikey = SugarChimp_Setting::retrieve('apikey');

        if(empty($apikey)) {
            //todo: lang string
            SugarChimp_Logger::log('warning','SugarChimp_List::get_mailchimp_lists MC api key is empty');
            return false;
        }

        //only lists not already synced
        $mailchimp_lists = array();
        
        try 
        {
            $result = MailChimp_API::get_lists(array('fields'=>'lists.id,lists.name'));
        } 
        catch(Exception $e) 
        {
            SugarChimp_Helper::log('warning',"SugarChimp Error: ".$e->getMessage());
            
            //want to pass the error message so return 200
            return false;
        }
        
        // set meaningful keys on the returning data array
        // set the keys to the mailchimp list id for easier lookup calls
        if (!empty($result['lists']) && is_array($result['lists']))
        {
            foreach ($result['lists'] as $key => $list)
            {
                $result['lists'][$list['id']] = $list;
                unset($result['lists'][$key]);
            }
        }

        return $result;
    }

    // return only a single sugar list
    public static function get_mailchimp_list_by_id($list_id)
    {
        if (!empty($list_id))
        {
            $where = "(id = '". $list_id ."') ";
        }
        
        return static::get_mailchimp_lists($where);
    }

    // same parameters and return as get_mailchimp_lists
    // returns only those mailchimp lists that are synced with a sugar list
    public static function get_synced_mailchimp_lists($filters=array(), $start=0, $limit=100, $sort_field='name', $sort_dir='ASC')
    {
        $synced_sugar_lists = static::get_synced_sugar_lists();

        if (empty($synced_sugar_lists['list']))
        {
            // there are no synced sugar target lists, so there are no mailchimp lists to get
            SugarChimp_Logger::log('debug','SugarChimp_List::get_synced_mailchimp_lists there are no synced sugar lists, so there are no synced mailchimp lists');
            return array();
        }

        $sugar_list_ids = array();
        foreach ($synced_sugar_lists['list'] as $key => $list)
        {
            $sugar_list_ids []= $list['mailchimp_list_name_c'];
        }

        $filters['list_id'] = implode(',',$sugar_list_ids);
        SugarChimp_Logger::log('debug','SugarChimp_List::get_synced_mailchimp_lists filters: '.print_r($filters,true));

        return static::get_mailchimp_lists($filters,$start,$limit,$sort_field,$sort_dir);
    }

    // same parameters and return as get_mailchimp_lists
    // returns only those mailchimp lists that are NOT synced with a sugar list
    public static function get_not_synced_mailchimp_lists($filters=array(), $start=0, $limit=100, $sort_field='name', $sort_dir='ASC')
    {
        $synced_sugar_lists = static::get_synced_sugar_lists();

        $all_mailchimp_lists = static::get_mailchimp_lists($filters,$start,$limit,$sort_field,$sort_dir);

        if (empty($synced_sugar_lists['list']))
        {
            SugarChimp_Logger::log('debug','SugarChimp_List::get_synced_mailchimp_lists there are no synced sugar target lists, then all mailchimp lists are NOT synced, so return them all');
            return $all_mailchimp_lists;
        }
        else
        {
            SugarChimp_Logger::log('debug','SugarChimp_List::get_synced_mailchimp_lists there ARE synced sugar target lists, so remove them from all mailchimp lists');

            // gather all the mailchimp ids we know are synced from our sugar target lists
            $synced_mailchimp_ids = array();
            foreach ($synced_sugar_lists['list'] as $list)
            {
                $synced_mailchimp_ids []= $list['mailchimp_list_name_c'];
            }

            SugarChimp_Logger::log('debug','SugarChimp_List::get_synced_mailchimp_lists mailchimp ids to remove: '.print_r($synced_mailchimp_ids,true));

            // go through each mailchimp list result and remove those that are in the synced sugar list
            foreach ($all_mailchimp_lists['data'] as $key => $list)
            {
                if (in_array($list['id'],$synced_mailchimp_ids))
                {
                    unset($all_mailchimp_lists['data'][$key]);
                }
            }

            $all_mailchimp_lists['data'] = array_values($all_mailchimp_lists['data']);
            $all_mailchimp_lists['total'] = count($all_mailchimp_lists['data']);

            return $all_mailchimp_lists;            
        }
    }

    // create a new sugar target list
    // paramters: $data needs to be an array of the fields on the 
    // array(
    //     'name' => 'Master Target List', // required
    //     'list_type' => 'default' , // a required field to create the record, but automatically supplied if not supplied
    //     'mailchimp_list_name_c' => '3ff3g5q5', // mailchimp list id if you want to sync it
    //     'description' => 'This is a target list blah blah...', //
    // )
    public static function create_sugar_list($data)
    {
        if (empty($data) or !is_array($data))
        {
            SugarChimp_Logger::log('warning','SugarChimp_List::create_sugar_list data is empty or not an array');
            return false;
        }

        if (empty($data['list_type']))
        {
            $data['list_type'] = 'default';   
        }

        $required_fields = array('name','list_type');

        foreach ($required_fields as $field)
        {
            if (empty($data[$field]))
            {
                SugarChimp_Logger::log('warning','SugarChimp_List::create_sugar_list required field not available in data. field: '.print_r($field,true).' data: '.print_r($data,true));
                return false;
            }
        }

        $list = BeanFactory::newBean('ProspectLists');

        foreach ($data as $field => $value)
        {
            if (property_exists($list,$field))
            {
                $list->$field = $value;
            }
            else
            {
                SugarChimp_Logger::log('warning','SugarChimp_List::create_sugar_list failed to create list with data: '.print_r($data,true));
            }
        }

        $list->save();

        if (empty($list->id))
        {
            SugarChimp_Logger::log('fatal','SugarChimp_List::create_sugar_list failed to create list with data: '.print_r($data,true));
            return false;
        }

        return static::prepare_sugar_list_for_client($list);
    }

    // create a new mailchimp list
    // paramters: $data needs to be an array of the fields on the 
    // array(
    //     'name' => 'Master Target List', // required
    //     'list_type' => 'default' , // a required field to create the record, but automatically supplied if not supplied
    //     'description' => 'This is a target list blah blah...', //
    // )
    public static function create_mailchimp_list($data = array())
    {
        if (empty($data['name']))
        {
            SugarChimp_Logger::log('fatal','SugarChimp_List::create_mailchimp_list a MailChimp List Name is required to create a list.');
            return false;
        }

        SugarChimp_Logger::log('debug','SugarChimp_List::create_mailchimp_list data:' . print_r($data,true));
        
        // merge in the required default fields 
        $data = self::set_array_defaults($data, self::$default_mailchimp_list);
        
        SugarChimp_Logger::log('debug','SugarChimp_List::create_mailchimp_list def_data:' . print_r($data,true));

        return MailChimp_API::create_list($data);
    }

    // Merge $arr and $def, taking $arr as priority over $def
    // $arr: Original array data
    // $def: default array data
    public static function set_array_defaults($arr,$def) 
    {
        if(empty($arr)) 
        {
            return $def;
        }

        foreach($def as $key => $value) 
        {
            if(is_array($value))
            {
                $arr[$key] = self::set_array_defaults($arr[$key],$value);
            }
            elseif(empty($arr[$key])) 
            {
                $arr[$key] = $value;
            }
        }

        return $arr;
    }

    public static function prepare_sugar_list_for_client($list,$include_subscriber_count = true,$include_smartlist = true)
    {
        if (empty($list))
        {
            return false;
        }

        $data = array(
            'id' => isset($list->id) ? $list->id : false,
            'name' => isset($list->name) ? $list->name : false,
            'description' => isset($list->description) ? $list->description : false,
            'mailchimp_list_name_c' => isset($list->mailchimp_list_name_c) ? $list->mailchimp_list_name_c : false,
            'mailchimp_default_module_c' => isset($list->mailchimp_default_module_c) ? $list->mailchimp_default_module_c : false,
        );

        if ($include_subscriber_count === true)
        {
            $data['subscriber_count'] = false;
            if (method_exists($list,'get_entry_count'))
            {
                $data['subscriber_count'] = $list->get_entry_count();
            }
        }

        if ($include_smartlist === true && !empty($list->id))
        {
            $sl = BeanFactory::newBean('SmartList');
            $smartlist = $sl->get_smartlist_by_prospect_list_id($list->id);
            $data['smartlist'] = SmartList_List::prepare_smartlist_for_client($smartlist);
        }

        return $data;
    }

    // set a list in the sugarchimp mc list module that it has been synced
    public static function mark_list_cleaned_emails_synced($mailchimp_list_id,$time_synced=false)
    {
        global $db;
        
        if (empty($mailchimp_list_id))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_List::mark_list_cleaned_emails_synced: mailchimp_list_id is empty but it is required');
            return false;
        }

        if (empty($time_synced))
        {
            // if send time is empty, default to now
            $time_synced = $db->now();
        }
        else
        {
            $time_synced = "'".$db->quote($time_synced)."'";
        }
        
        $sql = "UPDATE sugarchimp_mc_list
                SET last_cleaned_email_check = ".$time_synced."
                WHERE mailchimp_list_id='".$db->quote($mailchimp_list_id)."'";
        
        SugarChimp_Helper::log('debug','SugarChimp_List::mark_list_cleaned_emails_synced: sql for update: '.$sql);

        if (!$result = $db->query($sql))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_List::mark_list_cleaned_emails_synced: update query failed: '.$sql);
            return false;
        }
        
        return true;
    }

    // accepts $mc_list_ids
    // marks sugarchimp_mc_list records not found here as deleted_from_mailchimp
    public static function mark_other_lists_as_deleted($mc_list_ids=array())
    {
        if (empty($mc_list_ids) or !is_array($mc_list_ids))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_List::mark_other_lists_as_deleted: mc_list_id is empty but required');
            return false;
        }

        global $db;

        $sql = "UPDATE sugarchimp_mc_list
                SET deleted_from_mailchimp=1
                WHERE mailchimp_list_id NOT IN ('" . implode("','",$mc_list_ids) . "') ";
        
        SugarChimp_Helper::log('debug','SugarChimp_List::mark_other_lists_as_deleted: sql: '.print_r($sql,true));

        if (!$db->query($sql))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_List::mark_other_lists_as_deleted: query failed: '.print_r($sql,true));
            return false;
        }

        return true;
    }
}