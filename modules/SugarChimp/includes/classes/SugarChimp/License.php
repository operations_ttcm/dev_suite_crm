<?php







require_once('modules/SugarChimp/license/OutfittersLicense.php');        

class SugarChimp_License
{
    public static function is_valid()
    {
        $result = SugarChimpOutfittersLicense::isValid('SugarChimp');

        if ($result !== true)
        {
            $admin = BeanFactory::getBean('Administration');
            $admin->retrieveSettings('sugarchimp');
            $last_sent = $admin->settings['sugarchimp_licenseemail'];

            $elapsed = (60 * 60);
            
            if (empty($last_sent) || ($last_sent + $elapsed) < time())
            {
                $admin->saveSetting('sugarchimp','licenseemail',time());
                
                require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
                global $sugar_config, $sugar_version, $sugar_flavor;

                $error_email = SugarChimp_Helper::get_error_email_address();

                $email_to = $error_email;
                $subject =  '[Action Required] SugarChimp Has Stopped Syncing';
                $bodyHTML= "
The license was unable to validate. Please log into your SugarCRM instance, go to Admin->SugarChimp->License Configuration and validate your license.

If you have any questions about this, please email SugarChimp Support and we will help you out: support@sugarchimp.com

Site: ".$sugar_config['site_url']."
SugarCRM Version: ".$sugar_version."
SugarCRM Edition: ".$sugar_flavor."
SugarChimp Version: ".SugarChimp_Helper::get_sugarchimp_version();

                $license_key = SugarChimp_License::get_license_key();
                if (!empty($license_key))
                {
                    $bodyHTML .= "\n\nSugarChimp License: ".$license_key;
                }
                else
                {
                    $bodyHTML .= "\n\nSugarChimp License: unknown - could not retrieve";
                }
                
                SugarChimp_Helper::send_email($email_to,$subject,$bodyHTML,'help+error-license-failed@sugarchimp.com');
                
                return $result;
            }
        }
        
        return $result;
    }

    public static function get_license_key()
    {
        $license_key = SugarChimpOutfittersLicense::getKey('SugarChimp');
        
        if (empty($license_key))
        {
            return false;
        }

        return $license_key;
    }
}