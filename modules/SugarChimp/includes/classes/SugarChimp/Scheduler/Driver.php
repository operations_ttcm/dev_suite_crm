<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/License.php');

class SugarChimp_Scheduler_TimesUpException extends Exception {}

abstract class SugarChimp_Scheduler_Driver
{
    private $start_time = false;
    private $last_time = false;
    private $elapsed_time = false;

    private $max_time_to_run = 300; // in seconds
    private $time_buffer = 0.8; // ex. If set to .8, we're saying that we're going to stop doing work once we've used 80% of the max time
    protected $max_jobs_to_process = 200; // max number of job records to process in single call

    public static function forge($max_time_to_run=false, $max_jobs_to_process=false, $time_buffer=false)
    {
        return new static($max_time_to_run, $max_jobs_to_process, $time_buffer);
    }

    function __construct($max_time_to_run=false,$max_jobs_to_process=false, $time_buffer=false)
    {
        if (!empty($max_time_to_run))
        {
            $this->max_time_to_run = $max_time_to_run;
        }
        
        if (!empty($max_jobs_to_process))
        {
            $this->max_jobs_to_process = $max_jobs_to_process;
        }
        
        if (!empty($time_buffer))
        {
            $this->time_buffer = $time_buffer;
        }
    }
    
    public function start($elapsed_time=false)
    {
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_Driver::start');

        $this->start_timer($elapsed_time);

        // check for a valid SugarOutfitters license
        $license_check = SugarChimp_License::is_valid();
        
        if ($license_check !== true)
        {
            // there was an error with the license
            // $license_check contains the reason
            SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_Driver::start The SugarOutfitters license failed to validate: '.$license_check);
            return $this->end(array(
                "status" => "error",
                "message" => "The SugarChimp SugarOutfitters license failed to validate: {$license_check}",
                "error_type" => "license",
            ));
        }

        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_Driver::start License is good. Run it!');

        $this->update_timer();
        
        try 
        {
            $result = $this->run();
        }
        catch (SugarChimp_Scheduler_TimesUpException $e)
        {
            // time ran out for this scheduler run
            // will pick up where we left off last time
            SugarChimp_Helper::log('debug', "SugarChimp_Scheduler_Driver TimesUpException caught: ".$e->getMessage());
            return $this->end(array(
                'status' => 'success',
                'message' => $e->getMessage(),
            ));
        }
        catch (Exception $e)
        {
            // error exception caught
            SugarChimp_Helper::log('fatal', "SugarChimp_Scheduler_Driver Exception caught: ".$e->getMessage());
            return $this->end(array(
                'status' => 'error',
                'message' => $e->getMessage(),
            ));
        }
        
        return $result;
    }

    public abstract function run();

    public function end($result)
    {
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_Driver::end');
        
        // if it's boolean true, we're good
        if ($result['status'] === 'success')
        {
            // ya! we're done!
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_Driver::end The scheduler ran successfully');
        }
        else
        {
            // do something about the error
            SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_Driver::end The scheduler failed to finish: '.print_r($result,true));

            if (empty($result['error_type']) || $result['error_type'] != 'license')
            {
                global $sugar_config, $sugar_version, $sugar_flavor;

                $error_email = SugarChimp_Helper::get_error_email_address();
                
                $email_to = $error_email;
                $subject =  'Heads up! SugarChimp Sync Failed';
                $bodyHTML= "Sometimes your Sugar instance is unable to communicate with MailChimp. If you only receive this email every once in a while, there is nothing to worry about. Sugar will re-establish where it left off and continue syncing with MailChimp. 

If you are receiving this email every few minutes, please contact SugarChimp Suport so we can take a look: support@sugarchimp.com

Error: ".$result['message']."

Site: ".$sugar_config['site_url']."
SugarCRM Version: ".$sugar_version."
SugarCRM Edition: ".$sugar_flavor."
SugarChimp Version: ".SugarChimp_Helper::get_sugarchimp_version()."

Full error: ".print_r($result,true);

                $license_key = SugarChimp_License::get_license_key();
                if (!empty($license_key))
                {
                    $bodyHTML .= "\n\nSugarChimp License: ".$license_key;
                }
                else
                {
                    $bodyHTML .= "\n\nSugarChimp License: unknown - could not retrieve";
                }
        
                SugarChimp_Helper::send_email($email_to,$subject,$bodyHTML,'help+error-scheduler-failed@sugarchimp.com');
            }
        }

        $this->update_timer(false);

        // calculate and set the time remaining in seconds
        $time_used = $this->elapsed_time / $this->max_time_to_run;
        if ($time_used > $this->time_buffer)
        {
            // if we've crossed the time barrier, zero out the time remaining
            $result['elapsed_time'] = false;
        }
        else
        {
            $result['elapsed_time'] = $this->elapsed_time;
        }
        
        // always return true to signal the scheduler ran successfully
        // we'll handle error checking ourselves
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_Driver::end final result: '.print_r($result,true));

        return $result;
    }

    private function start_timer($elapsed_time=false)
    {
        $this->start_time = time();
        $this->last_time = $this->start_time;

        SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_Driver->start_timer timer started at {$this->start_time}");

        // if a $elapsed_time is provided, reset the timer to run for only the allotted time left
        if (empty($elapsed_time))
        {
            SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_Driver->start_timer elapsed time is empty");
            $this->elapsed_time = 0;
        }
        else
        {
            SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_Driver->start_timer elapsed time set to {$elapsed_time}");
            $this->elapsed_time = (int) $elapsed_time;
        }

        return true;
    }
    
    protected function update_timer($check_timer = true)
    {
        SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_Driver Original Start time {$this->start_time}");
        $current_time = time();
        SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_Driver Current time {$current_time}");
        $time_diff = $current_time - $this->last_time;
        SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_Driver Time since last check {$time_diff}");
        $this->elapsed_time += $time_diff;
        // set last_time to current_time
        $this->last_time = $current_time;
        
        if ($check_timer === true)
        {
            if ($this->check_timer() === false)
            {
                // we need to end the script
                SugarChimp_Helper::log('debug','SugarChimp_Scheduler_Driver Ending the process due to time! Will pick up where we left off next time.');
                throw new SugarChimp_Scheduler_TimesUpException('Everything ran well, but our time is up. Will continue on the next scheduler run.');
            }
        }
        
        return true;
    }
    
    private function check_timer()
    {
        // calculate the percentage of time we've used 
        $time_used = $this->elapsed_time / $this->max_time_to_run;
        SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_Driver Elapsed time {$this->elapsed_time}");
        SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_Driver Time used {$time_used}");
        // if the percentage of time used is greater thant the allowed time before
        // return false to tell the script to finish up
        if ($time_used > $this->time_buffer)
        {
            return false;
        }
        
        // otherwise, let's keep going and do something else
        return true;
    }
}