<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Field/Group.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Scheduler/Driver.php');

class SugarChimp_Scheduler_SugarChimpBackups extends SugarChimp_Scheduler_Driver 
{
    function run()
    {
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarChimpBackups->run Running...');
        $this->update_timer();

        global $db;
    
        // check to see if this job is enabled to run
        // if not, process it as complete and let the scheduler carry on
        $job_sugarchimpbackups_enabled = SugarChimp_Setting::retrieve('job_sugarchimpbackups_enabled');
        if(empty($job_sugarchimpbackups_enabled))
        {
            SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_SugarchimpBackups->run - job_sugarchimpbackups_enabled is false, not enabled. Will not perform backup.");
            SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_SugarchimpBackups->run - emptying backup table.");

            $sql = "TRUNCATE TABLE sugarchimp_backup_queue";
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarchimpBackups->run: sql: '.$sql);

            if (!$result = $db->query($sql))
            {
                SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_SugarchimpBackups->run: could not clear backups table. sql failed: '.$sql);
                return $this->end(array(
                    'status' => 'success',
                    'message' => 'SugarChimp Backup was skipped, but the table could not be emptied.',
                ));
            }

            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarchimpBackups->run: backup table cleared successfully.');
            return $this->end(array(
                'status' => 'success',
                'message' => 'SugarChimp Backups are disabled.',
            ));
        }

        // look for backup jobs in sugarchimp table
        $sql = "SELECT DISTINCT mailchimp_list_id AS mailchimp_list_id, name AS name FROM sugarchimp_backup_queue";
        $result = $db->query($sql);

        $backup_jobs = array();
        while ($row = $db->fetchByAssoc($result))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarChimpBackups->run backup job found '.$row['name'].' for list '.$row['mailchimp_list_id']);
            if (empty($backup_jobs[$row['mailchimp_list_id']])) $backup_jobs[$row['mailchimp_list_id']] = array();

            $backup_jobs[$row['mailchimp_list_id']] []= $row['name'];
        }

        foreach ($backup_jobs as $list_id => $job_names)
        {
            array_unique($job_names);
            foreach ($job_names as $type)
            {
                SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarChimpBackups starting '.$type.' job for list '.$list_id);
                switch ($type)
                {
                    case "MailchimpBackup":

                        // get mailchimp api key
                        $apikey = SugarChimp_Setting::retrieve('apikey');
                        
                        if (empty($apikey))
                        {
                            SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_SugarChimpBackups backup failed to complete due to missing MailChimp API Key for list '.print_r($list_id,true));
                            continue;
                        }

                        // start a MC List export and save to a database
                        $subscribers_completed = SugarChimp_Helper::import_subscribers_from_mailchimp($apikey, $list_id, 'sugarchimp_mailchimp_backup');
                        
                        if($subscribers_completed == false) 
                        {
                            SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_SugarChimpBackups backup failed to complete for list '.print_r($list_id,true).' result: '.print_r($subscribers_completed,true));
                        }
                        else
                        {
                            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarChimpBackups backup completed for list '.print_r($list_id,true));
                        }

                        break;

                    case "SugarBackup":
                        // start a Sugar list backup

                        // get target list id
                        $target_list = BeanFactory::getBean('ProspectLists');
                        $target_list->retrieve_by_string_fields(array('mailchimp_list_name_c' => $list_id));

                        if (empty($target_list->id)) 
                        {
                            SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_SugarChimpBackups Could not perform Sugar backup. Target List not found for MC list_id '.print_r($list_id,true));
                            break;
                        }

                        $target_list_id = $target_list->id;

                        // get beans on the target list
                        $leads_query = "SELECT DISTINCT l.id as id, 'Leads' AS module
                                        FROM prospect_lists_prospects plp
                                        INNER JOIN leads l ON plp.related_id=l.id
                                        LEFT JOIN email_addr_bean_rel ear ON  ear.bean_id=l.id AND ear.deleted=0
                                        LEFT JOIN email_addresses ea ON ear.email_address_id=ea.id
                                        WHERE plp.prospect_list_id = '".$db->quote($target_list_id)."' AND plp.deleted=0 
                                        AND l.deleted=0
                                        AND (ear.deleted=0 OR ear.deleted IS NULL)";

                        $contacts_query = "SELECT DISTINCT c.id as id, 'Contacts' AS module
                                            FROM prospect_lists_prospects plp
                                            INNER JOIN contacts c ON plp.related_id=c.id
                                            LEFT JOIN email_addr_bean_rel ear ON  ear.bean_id=c.id AND ear.deleted=0
                                            LEFT JOIN email_addresses ea ON ear.email_address_id=ea.id
                                            WHERE plp.prospect_list_id = '".$db->quote($target_list_id)."' AND plp.deleted=0 
                                            AND c.deleted=0
                                            AND (ear.deleted=0 OR ear.deleted IS NULL)";

                        $prospects_query = "SELECT DISTINCT p.id as id, 'Prospects' AS module
                                            FROM prospect_lists_prospects plp
                                            INNER JOIN prospects p ON plp.related_id=p.id
                                            LEFT JOIN email_addr_bean_rel ear ON  ear.bean_id=p.id AND ear.deleted=0
                                            LEFT JOIN email_addresses ea ON ear.email_address_id=ea.id
                                            WHERE plp.prospect_list_id = '".$db->quote($target_list_id)."' AND plp.deleted=0 
                                            AND p.deleted=0
                                            AND (ear.deleted=0 OR ear.deleted IS NULL)";

                        $accounts_query = "SELECT DISTINCT a.id as id, 'Accounts' AS module
                                            FROM prospect_lists_prospects plp
                                            INNER JOIN accounts a ON plp.related_id=a.id
                                            LEFT JOIN email_addr_bean_rel ear ON  ear.bean_id=a.id AND ear.deleted=0
                                            LEFT JOIN email_addresses ea ON ear.email_address_id=ea.id
                                            WHERE plp.prospect_list_id = '".$db->quote($target_list_id)."' AND plp.deleted=0 
                                            AND a.deleted=0
                                            AND (ear.deleted=0 OR ear.deleted IS NULL)";

                        $sql = "$leads_query UNION ALL $contacts_query UNION ALL $prospects_query UNION ALL $accounts_query";
                
                        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarChimpBackups SugarBackup query '.print_r($sql,true));
                        $result = $db->query($sql);

                        // bulk insert backup data every 100 records
                        $insert_count = 0;
                        $initial_sql = "INSERT INTO sugarchimp_sugar_backup (id,record,module,data,date_entered) VALUES ";
                        $insert_sql = "";
                        while ($row = $db->fetchByAssoc($result))
                        {
                            if ($insert_count === 0) $insert_sql = $initial_sql;

                            if (empty($row['module']) or empty($row['id'])) continue;

                            $bean = BeanFactory::getBean($row['module'],$row['id']);

                            if (empty($bean)) continue;

                            $data = SugarChimp_FieldMap::get_merge_var_update_array_from_bean($list_id,$bean,true);
                            $data = base64_encode(serialize($data));

                            if ($insert_count !== 0)
                            {
                                $insert_sql .= " , ";
                            }

                            $insert_sql .= " (".$db->getGuidSQL().",'".$db->quote($row['id'])."','".$db->quote($row['module'])."','".$data."',".$db->now().") ";
                            $insert_count++;

                            if ($insert_count == 100)
                            {
                                SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarChimpBackups SugarBackup insert sql: '.print_r($insert_sql,true));
                                if (!$insert_result = $db->query($insert_sql))
                                {
                                    SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_SugarChimpBackups SugarBackup batch query failed: '.print_r($insert_sql,true));
                                }

                                $insert_count = 0;
                                $insert_sql = "";
                            }
                        }

                        // didn't end on even 100, so run query on what's left
                        if (!empty($insert_sql))
                        {
                            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarChimpBackups SugarBackup insert sql: '.print_r($insert_sql,true));
                            if (!$result = $db->query($insert_sql))
                            {
                                SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_SugarChimpBackups SugarBackup batch query failed: '.print_r($insert_sql,true));
                            }
                        }

                        break;
                }

                // if it has run successfully, go ahead and delete other backup jobs for this list
                $sql = "DELETE FROM sugarchimp_backup_queue WHERE name='".$db->quote($type)."' AND mailchimp_list_id='".$db->quote($list_id)."'";
                if (!$result = $db->query($sql))
                {
                    SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_SugarChimpBackups query failed, could not delete '.$type.' records in the sugarchimp queue for list '.$list_id);
                }
            }
        }

        /**

        Eventually add some backup cleanup here
        if there's a newer backup or older than X days

        */
        
        return $this->end(array(
            'status' => 'success',
            'message' => 'SugarChimp Backup Queue is clear.',
        ));
    }
}