<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Field/Group.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Scheduler/Driver.php');

class SugarChimp_Scheduler_SugarCRMGroupsToMailchimp extends SugarChimp_Scheduler_Driver 
{
    function run()
    {
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarCRMGroupsToMailchimp->run Running...');
		$this->update_timer();
    
		$result = SugarChimp_Field_Group::sync_sugar_dropdowns_with_mailchimp_groups();

		if ($result !== true)
		{
			SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_SugarCRMGroupsToMailchimp->run sync_sugar_groups_to_mailchimp failed: '.print_r($result,true));
		}

        $this->update_timer();

        return $this->end(array(
            'status' => 'success',
            'message' => 'All SugarCRM Mapped Group Fields Synced With MailChimp.',
        ));
    }
}