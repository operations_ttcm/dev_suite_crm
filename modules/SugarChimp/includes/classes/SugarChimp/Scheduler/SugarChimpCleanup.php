<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Field/Group.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Scheduler/Driver.php');

class SugarChimp_Scheduler_SugarChimpCleanup extends SugarChimp_Scheduler_Driver 
{
    function run()
    {
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarChimpCleanup->run Running...');
        $this->update_timer();

        global $db;

        // remove old batches in the sugarchimp_batches table
        $remove_before_time = time() - (60*60*24*7);

        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarChimpCleanup->remove batches created before ' . print_r(date('D, d M Y H:i:s', $remove_before_time),true));
        
        $sql = "DELETE FROM sugarchimp_batches WHERE " . $db->convert('date_entered','add_date',array(7,'DAY')) . " < '".$GLOBALS['timedate']->nowDb()."'";
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarChimpCleanup sql: ' . print_r($sql,true));

        if (!$result = $db->query($sql))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_SugarChimpCleanup query failed: sql: ' . print_r($sql,true));
        }
        elseif(!empty($result->num_rows))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarChimpCleanup->removed ' . print_r($result->num_rows,true) . 'batches from the sugarchimp_batches table.');
        }

        // remove old email addresses in the lock table
        $sql = "DELETE FROM sugarchimp_lock WHERE " . $db->convert('date_entered','add_time',array(0,5)) . " < '".$GLOBALS['timedate']->nowDb()."'";
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarChimpCleanup sql: ' . print_r($sql,true));

        if (!$result = $db->query($sql))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_SugarChimpCleanup empty sugarchimp_lock table query failed: sql: ' . print_r($sql,true));
        }
        elseif(!empty($result->num_rows))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarChimpCleanup->removed ' . print_r($result->num_rows,true) . 'records from the sugarchimp_lock table removed.');
        }

        // jon todo
        //remove old SugarChimpActivities
        // remove old SUgarChimpActivity relationships
        //may leave a few orphan relationships, but should not cause major issues

        $remove_activities_after_days = SugarChimp_Setting::retrieve('remove_activities_after_days');
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarChimpCleanup remove_activities_after_days: ' . print_r($remove_activities_after_days,true));

        if(!empty($remove_activities_after_days) && $remove_activities_after_days > 1)
        {
            $activity_tables = array(
                'sugarchimpactivity_contacts',
                'sugarchimpactivity_leads',
                'sugarchimpactivity_prospects',
                'sugarchimpactivity_accounts',
                'sugarchimpactivity',
            );
            foreach($activity_tables as $table)
            {
                $sql = "DELETE FROM {$table} WHERE " . $db->convert('date_entered','add_date',array($remove_activities_after_days,'DAY')) . " < '".$GLOBALS['timedate']->nowDb()."'";
                SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarChimpCleanup remove old activities sql: ' . print_r($sql,true));

                if (!$result = $db->query($sql))
                {
                    SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_SugarChimpCleanup query failed: sql: ' . print_r($sql,true));
                }
                elseif(!empty($result->num_rows))
                {
                    SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarChimpCleanup->removed ' . print_r($result->num_rows,true) . 'batches from the sugarchimp_batches table.');
                }
            }
        }

        return $this->end(array(
            'status' => 'success',
            'message' => 'SugarChimp Cleanup is finished.',
        ));
    }
}