<?php






require_once('modules/SugarChimp/SugarChimpMCList.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');
require_once('modules/SugarChimp/includes/classes/Mailchimp/API.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Scheduler/Driver.php');

class SugarChimp_Scheduler_MailchimpListToSugarCRM extends SugarChimp_Scheduler_Driver 
{
    private $max_records_per_run = 200;
    
    function run()
    {
        global $db;

        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpListToSugarCRM->run Running...');

        $this->update_timer();

        // If Pro or Ultimate, manage dropdown menus
        $manage_dropdowns = SugarChimp_Setting::retrieve('ignore_dropdown_updates');
        if(!empty($manage_dropdowns))
        {
            $current_plan = SugarChimp_Helper::get_current_plan();
            $manage_dropdowns = (!empty($current_plan) && ($current_plan == 'professional' || $current_plan == 'ultimate'));        
        }
        else
        {
            $manage_dropdowns = false;
        }

        // every run, make sure the sugarchimp mc list table has all mc lists
        // get all of the lists in MailChimp
        $results = MailChimp_API::get_lists(array('count'=>1000,'fields'=>'lists.id,lists.name'));

// as of SC 7.7.7 the optouttracker_initial_run flag is probably unnecesary, we used to only need to do this once
// but now that we moved the cleaned email tracking to check all the time, we need to 
// make sure the sugarchimp_mc_list table has all mc lists, including new ones created on the mc side
// $optouttracker_initial_run = SugarChimp_Setting::retrieve('optouttracker_initial_run');

        if (empty($results['lists']) or !is_array($results['lists']))
        {
            // bad response data
            // don't mark optouttracker_initial_run as true because we want this to keep failing
            // they need this to run to prevent liability on our side
            SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpListToSugarCRM->run there are no lists or we hit an error. result: '.print_r($results,true));
            return $this->end(array(
                'status' => 'error',
                'message' => "Something isn't right with MailChimp: ".print_r($results,true),
            ));
        }

        $mc_list_ids = array();

        foreach ($results['lists'] as $list)
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpListToSugarCRM->run processing MC list: '.print_r($list,true));

            if (empty($list['id']))
            {
                // we have to have a list id
                SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpListToSugarCRM->run this list does not have an id, cannot use it. '.print_r($list,true));
                continue;
            }

            $mc_list_ids []= $list['id'];

            //will not manage dropdowns if on Basic level
            SugarChimp_Helper::add_list_to_table($list['id'],$list['name'], $manage_dropdowns);
        }

        if (!empty($mc_list_ids) and is_array($mc_list_ids))
        {
            SugarChimp_List::mark_other_lists_as_deleted($mc_list_ids);
        }


        $this->update_timer();

        // add all campaigns to the dropdown
        // go through each campaign and make sure it is in the campaign options list
        // this is the list that is used to display the campaign name on sugarchimp activities
        $sql = "SELECT mailchimp_campaign_id, name FROM sugarchimp_mc_campaign";
        $result = $db->query($sql);

        while ($row = $db->fetchByAssoc($result))
        {
            if (!empty($row['mailchimp_campaign_id']) && !empty($row['name']))
            {
                // Also add the Campaign to our Dropdown Menu
                $dropdown_option = array(
                    'key'=> $row['mailchimp_campaign_id'],
                    'label' => $row['name'],
                );
                SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpListToSugarCRM->run attempt to add Campaign dropdown option: '.print_r($dropdown_option,true));
                SugarChimp_Helper::add_mailchimp_campaign_to_dropdown($dropdown_option);
            }
        }

/**
        chad & JON 1/26/2017 - as of 1/26/2017 the mailchimp v3 api manages those who have actually
        unsubscribed on their side. SC is unable to resubscribe someone who has actually unsubscribed themselves.
        because of this, we no longer have to track this on the SC side, we can rely on mailchimp.

        // helper array to track if a list has failed multiple times
        // shutting things down if one fails three times
        $failed_lists = array();

        $job_optouttracker_enabled = SugarChimp_Setting::retrieve('job_optouttracker_enabled');
        if (empty($job_optouttracker_enabled))
        {
            SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_MailchimpListToSugarCRM->run() - optout tracker job is disabled");
        }
        else
        {
            // opt out tracking is enabled, do it

            // this only happens once for each list initially
            // the cleans and unsubscribes will be picked up later
            // unsubscribes via webhooks
            // cleans via   
            // go through each of the sugar mc lists that have not been processed yet
            while ($mailchimp_list = $this->get_next_mailchimp_list())
            {
                $this->update_timer();

                // SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpListToSugarCRM->run: processing list optout: '.print_r($mailchimp_list,true));

                if (isset($failed_lists[$mailchimp_list->mailchimp_list_id]) && $failed_lists[$mailchimp_list->mailchimp_list_id] >= 3)
                {
                    // there is a list constantly failing, need to see what's going on
                    return $this->end(array(
                        'status' => 'failure',
                        'message' => 'Optouts are not able to be processed for one of your MailChimp Lists: '.print_r($mailchimp_list->mailchimp_list_id,true),
                    ));
                }

                if (empty($mailchimp_list->mailchimp_list_id))
                {
                    // there is a record in the sugar mc list table that does not hav a mailchimp list id, hsouldnt be possible
                    $failed_lists[$mailchimp_list->mailchimp_list_id] += 1;
                    SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpListToSugarCRM->run: there is a record in the sugar mc list table that does not have a mailchimp list id, hsouldnt be possible. id: '.print_r($mailchimp_list->id,true));
                    continue;
                }

                $result = $this->process_mailchimp_list($mailchimp_list);

                if ($result !== true)
                {
                    if (!isset($failed_lists[$mailchimp_list->mailchimp_list_id]))
                    {
                        $failed_lists[$mailchimp_list->mailchimp_list_id] = 0;
                    }
                    
                    $failed_lists[$mailchimp_list->mailchimp_list_id] += 1;

                    // we couldn't complete the process for some reason
                    SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpListToSugarCRM->run: there was an error processing the list. '.print_r($mailchimp_list->mailchimp_list_id,true));
                    continue;
                }

                // all is good, mark the list as complete

                $this->mark_list_optout_tracker_complete($mailchimp_list);
            }
        }

        $this->update_timer();
*/
        // then check to see if there is anything in the mc cleaned email queue
        // if there is, process the queue until it is empty
        // then carry on to see if we need to check lists for cleaned emails

        $webhook_cleaned_enabled = SugarChimp_Setting::retrieve('webhook_cleaned_enabled');
        if(empty($webhook_cleaned_enabled))
        {
            // cleaned email tracking is turned off, not gonna do it
            SugarChimp_Helper::log('debug',"Mailchimp_Webhooks_Receiver::cleaned - MailChimp to Sugar 'cleaned' Webhook is disabled. Ignoring Webhook.");
        }
        else
        {
            // cleaned email tracking is on, do it
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpListToSugarCRM->run: checking sugarchimp_mc_cleaned_email queue for cleaned emails to process in queue');
            
            while ($emails = $this->get_cleaned_emails_from_queue($this->max_records_per_run))
            {
                if (empty($emails) or !is_array($emails))
                {
                    SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpListToSugarCRM->run: get_cleaned_emails_from_queue returned invalid response, expecting array of email address data. emails: '.print_r($emails,true));

                    return $this->end(array(
                        'status' => 'error',
                        'message' => "get_cleaned_emails_from_queue method returned invalid response, expecting array of email address data.",
                    ));
                }

                foreach ($emails as $email)
                {
                    SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpListToSugarCRM->run: processing email: '.print_r($email,true));

                    $response = $this->process_cleaned_email($email['sugar_record_id'],$email['email'],$email['mailchimp_list_id']);

                    if ($response !== true)
                    {
                        SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpListToSugarCRM->run: there was an error marking the email address as clean: '.print_r($email,true));
                    }

                    // remove record from the queue
                    $sql = "DELETE FROM sugarchimp_mc_cleaned_email 
                            WHERE id='".$db->quote($email['sugar_record_id'])."'";

                    if (!$result = $db->query($sql))
                    {
                        SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpListToSugarCRM->run: Could not remove record from cleaned email queue. Query failed: '.print_r($sql,true));
                    }
                }
            }

            $this->update_timer();
            
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpListToSugarCRM->run: now kickoff the next job to go get recent cleaned emails');

            $this->queue_next_mailchimp_list_to_process_cleaned_emails();
        }

        return $this->end(array(
            'status' => 'success',
            'message' => 'All MC Lists and Optout Tracking stuff is processed.',
        ));
    }

    // this function will find the next list that hasn't had cleaned emails checked in past 24 hours
    // last_cleaned_email_check is the timestamp to check
    // a scheduler job will be kicked off to run export job to get cleaned emails for the list
    // this job will use the export api to get the cleaned emails and put in clean email queue
    function queue_next_mailchimp_list_to_process_cleaned_emails()
    {
        global $db;

        // need to run a custom query for each version for this specific case
        // limit queries are weird for some reason with the order by here
        // I don't think it supports order bys that contain multiple fields
        if($db->dbType == 'mysql')
        {
            $sql = "SELECT id, last_cleaned_email_check, mailchimp_list_id
                    FROM sugarchimp_mc_list
                    WHERE deleted=0 AND
                        ((last_cleaned_email_check IS NULL OR ".$db->convert('last_cleaned_email_check','add_date',array(1,'DAY'))." < ".$db->now().") 
                            AND 
                        (deleted_from_mailchimp IS NULL OR deleted_from_mailchimp=0))
                    ORDER BY last_cleaned_email_check ASC, date_entered ASC
                    LIMIT 0,1";
        }
        else
        {
            $sql = "SELECT TOP 1 * 
                    FROM (
                        SELECT id, last_cleaned_email_check, mailchimp_list_id, ROW_NUMBER()
                        OVER (ORDER BY last_cleaned_email_check ASC, date_entered ASC) AS row_number
                        FROM sugarchimp_mc_list
                        WHERE deleted=0 AND
                            (last_cleaned_email_check IS NULL OR ".$db->convert('last_cleaned_email_check','add_date',array(1,'DAY'))." < ".$db->now().")
                                AND
                            (deleted_from_mailchimp IS NULL OR deleted_from_mailchimp=0)
                    ) AS a
                    WHERE row_number > 0";
        }

        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpToSugar->get_next_mailchimp_list_to_process_cleaned_emails get next list we need to get cleaned emails from sql: '.print_r($sql,true));
        
        if (!$result = $db->query($sql))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpToSugar->get_next_mailchimp_list_id query failed: '.print_r($sql,true));
            return false;
        }

        $row = $db->fetchByAssoc($result);

        if (empty($row))
        {
            // no lists need to be checked for cleaned emails at this time.
            return false;
        }

        if (empty($row['last_cleaned_email_check']) or $row['last_cleaned_email_check'] == '0000-00-00 00:00:00')
        {
            $since = false;
        }
        else
        {
            $since = $row['last_cleaned_email_check'];
        }

        SugarChimp_Helper::queue_GetMailChimpCleanedEmails_job(array(
            'mailchimp_list_id' => $row['mailchimp_list_id'],
            'since' => $since,
        ));

        return true;
    }

/**
    function process_mailchimp_list($mailchimp_list)
    {
        if (empty($mailchimp_list) or !$mailchimp_list instanceof SugarBean)
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpListToSugarCRM->process_mailchimp_list: mailchimp list is not a valid object');
            return false;
        }
        
        if (empty($mailchimp_list->mailchimp_list_id))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpListToSugarCRM->process_mailchimp_list: mailchimp list id is not available');
            return false;
        }
        
        $optout_tracker_offset = empty($mailchimp_list->optout_tracker_offset) ? 0 : $mailchimp_list->optout_tracker_offset;
        $total_items = false;

        // get chunk from mailchimp
        while ($optout_tracker_offset == 0 or $total_items === false or $optout_tracker_offset < $total_items)
        {
            $result = MailChimp_API::get_unsubscribed_list_members($mailchimp_list->mailchimp_list_id,$optout_tracker_offset,$this->max_records_per_run);

            if (!isset($result['total_items']))
            {
                // we don't know how many total unsubscribes/cleans there are on the list
                // probably a bad response
                SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpListToSugarCRM->process_mailchimp_list: did not receive a total_items number in our MC response. result: '.print_r($result,true));
                return false;
            }

            $total_items = $result['total_items'];

            if ($total_items == 0)
            {
                // there are no unsubscribes on the list
                // this is an acceptable response
                SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpListToSugarCRM->process_mailchimp_list: there are no unsubscribes on the mailchimp list.');
                return true;
            }

            if (!isset($result['members']))
            {
                // we didn't get any members back, so there probably are no opt outs
                SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpListToSugarCRM->process_mailchimp_list: results->members is not available. result: '.print_r($result,true));
                return false;
            }

            $data = array();
            foreach ($result['members'] as $member)
            {
                $data []= array(
                    'email' => $member['email_address'],
                    'mailchimp_list_id' => $mailchimp_list->mailchimp_list_id,
                );
            }

            if (empty($data))
            {
                // it's possible for result->members to be empty even if total_items is greater than 0
                SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpListToSugarCRM->process_mailchimp_list: data is empty, there are no unsubscribes on the mailchimp list.');
                return true;
            }

            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpListToSugarCRM->run data to add to optout tracker table: '.print_r($data,true));

            // import chunk into table
            $result = SugarChimp_Setting::batch_add_to_optout_tracking_table($data);

            if ($result !== true)
            {
                // something didn't work
                SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpListToSugarCRM->run batch add to optout tracking table failed for data: '.print_r($data,true));
                return false;
            }

            $optout_tracker_offset += $this->max_records_per_run;
            
            // update offset
            $mailchimp_list->optout_tracker_offset = $optout_tracker_offset;
            $mailchimp_list->save();
        }

        return true;
    }

    // responsible for returning the next job name in the sugarchimp mc list queue
    // this is what will be imported next
    // if there are no matches, it will return false
    function get_next_mailchimp_list()
    {
        global $db;
        
        // select the oldest job from the list
        $sql = "SELECT id AS id
                FROM sugarchimp_mc_list
                WHERE ((optout_tracker_processed IS NULL OR optout_tracker_processed!=1)
                    AND
                    (deleted_from_mailchimp IS NULL OR deleted_from_mailchimp=0))
                ORDER BY date_entered ASC";
                
        $sql = $db->limitQuery($sql,0,1,false,'',false);
        
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpToSugar->get_next_mailchimp_list_id get oldest job: '.$sql);
        
        $result = $db->query($sql);
        $row = $db->fetchByAssoc($result);
        
        if (empty($row['id']))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpToSugar->get_next_mailchimp_list_id There are no jobs to be processed for the list.');
            return false;
        }
        
        $mclist = BeanFactory::getBean('SugarChimpMCList',$row['id']);

        if (empty($mclist) or empty($mclist->id))
        {
            // this is unlikely as we just ran a query to see if this exists
            SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpToSugar->get_next_mailchimp_list_id we could not find sugar mc list record by id: '.print_r($row['id'],true));
            return false;
        }

        return $mclist;        
    }

    function mark_list_optout_tracker_complete($mailchimp_list)
    {
        if (empty($mailchimp_list) or !$mailchimp_list instanceof SugarBean)
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpListToSugarCRM->mark_list_optout_tracker_complete: mailchimp_list is empty and required');
            return false;
        }

        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpListToSugarCRM->mark_list_optout_tracker_complete mailchimp list id: '.print_r($mailchimp_list->mailchimp_list_id,true));

        global $db;

        // since this checks unsubsribes and cleans, we can mark this as checked as well
        $mailchimp_list->last_cleaned_email_check = $db->now();

        $mailchimp_list->optout_tracker_processed = 1;
        $mailchimp_list->save();

        return true;
    }
*/
    function get_cleaned_emails_from_queue($max_records_to_process=200)
    {
        global $db;

        $sql = "SELECT id AS sugar_record_id, email AS email, mailchimp_list_id AS mailchimp_list_id
                FROM sugarchimp_mc_cleaned_email
                ORDER BY date_entered ASC";

        $sql = $db->limitQuery($sql,0,$db->quote($max_records_to_process),false,'',false);

        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpListToSugarCRM->get_cleaned_emails_from_queue sql: '.print_r($sql,true));

        if (!$result = $db->query($sql))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpListToSugarCRM->get_cleaned_emails_from_queue select query failed: '.print_r($sql,true));
            return false;
        }

        $data = array();
        
        while ($row = $db->fetchByAssoc($result))
        {
            $data []= array(
                'sugar_record_id' => $row['sugar_record_id'],
                'email' => $row['email'],
                'mailchimp_list_id' => $row['mailchimp_list_id'],
            );
        }

        return $data;
    }

    function process_cleaned_email($id,$email,$list_id)
    {
        global $db;
        
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpListToSugarCRM->process_cleaned_email: start');

        if (empty($id))
        {
            // list_id is required
            SugarChimp_Helper::log('error','SugarChimp_Scheduler_MailchimpListToSugarCRM->process_cleaned_email: id is required, but it was empty');
            return false;
        }
        
        if (empty($list_id))
        {
            // list_id is required
            SugarChimp_Helper::log('error','SugarChimp_Scheduler_MailchimpListToSugarCRM->process_cleaned_email: list id is required, but it was empty');
            return false;
        }
        
        if (empty($email))
        {
            // list_id is required
            SugarChimp_Helper::log('error','SugarChimp_Scheduler_MailchimpListToSugarCRM->process_cleaned_email: email is required, but it was empty');
            return false;
        }
        
        // attempt to lookup crm record with $email
        $beans = SugarChimp_Helper::find_beans_by_email($email);
        
        // get all synced lists for beans
        $lists = SugarChimp_Helper::get_lists_for_beans($beans);
            
        // if we found beans, remove them from target list
        if (!empty($beans))
        {
            // disable logic hooks for the beans and target list
            SugarChimp_Helper::disable_logic_hooks($beans);
            SugarChimp_Helper::disable_logic_hooks($lists);

            $marked = SugarChimp_Helper::mark_email_invalid($email);
                    
            if ($marked !== true)
            {
                SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpListToSugarCRM->process_cleaned_email: '.print_r($email,true).' failed to be marked invalid. marked: '.print_r($marked,true));
            }
            else
            {
                SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpListToSugarCRM->process_cleaned_email: '.print_r($email,true).' was marked invalid.');
            }

            // remove beans from the list
            if (!empty($lists) and is_array($lists))
            {
                foreach ($lists as $list)
                {
                    SugarChimp_Helper::remove_beans_from_list($list,$beans);
                }
            }

            // enable logic hooks for the beans and target list
            SugarChimp_Helper::enable_logic_hooks($beans);
            SugarChimp_Helper::enable_logic_hooks($lists);
        }

        return true;
    }
}