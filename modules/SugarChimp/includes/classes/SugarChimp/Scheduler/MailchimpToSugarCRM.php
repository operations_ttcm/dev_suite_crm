<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Scheduler/Driver.php');

class SugarChimp_Scheduler_MailchimpToSugarCRM extends SugarChimp_Scheduler_Driver 
{
    private $max_records_per_run = 200;
    
    function run()
    {
        global $db;
        
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpToSugarCRM->run Running...');

        $this->update_timer();
        
        while ($mailchimp_list_id = $this->get_next_mailchimp_list_id())
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpToSugarCRM->run mailchimp_list_id: '.print_r($mailchimp_list_id,true));
            
            $target_list = BeanFactory::newBean('ProspectLists');
            $target_list->retrieve_by_string_fields(array('mailchimp_list_name_c' => $mailchimp_list_id));

            if (empty($target_list) || empty($target_list->id))
            {
                // could get stuck in an infinite loop if we don't error it out here
                // log the error, remove the items from the queue and move on
                SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpToSugarCRM->run There are no target lists for mailchimp list id: '.print_r($mailchimp_list_id,true));
                $sql = "DELETE FROM sugarchimp_mc WHERE mailchimp_list_id='".$db->quote($mailchimp_list_id)."'";
                $result = $db->query($sql);

                // go to the next list
                continue;
            }

            $sql = "SELECT id,data 
                    FROM sugarchimp_mc 
                    WHERE mailchimp_list_id='".$db->quote($mailchimp_list_id)."'";

            $sql = $db->limitQuery($sql,0,$db->quote($this->max_records_per_run),false,'',false);

            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpToSugarCRM->run sql: '.$sql);
                
            if (!$result = $db->query($sql))
            {
                return $this->end(array(
                    'status' => 'error',
                    'message' => "Query failure when pulling records from sugarchimp_mc table. See logs.",
                ));
            }

            $beans = array();
            while ($row = $db->fetchByAssoc($result))
            {
                $beans []= array(
                    'id' => $row['id'],
                    'data' => $row['data'],
                );
            }

            if (empty($beans))
            {
                SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpToSugarCRM->run There are no persons from Mailchimp to sync to SugarCRM, mailchimp list id: '.print_r($mailchimp_list_id,true));
                // could get stuck in an infinite loop if we don't error it out here
                return $this->end(array(
                    'status' => 'error',
                    'message' => "There are no persons from Mailchimp to sync to SugarCRM, mailchimp list id: {$mailchimp_list_id}",
                ));
            }

            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpToSugarCRM->run found people to insert/update: '.count($beans).' number people');

            // loop through each bean in the sugarchimp_mc table
            foreach ($beans as $bean)
            {
                SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpToSugarCRM->run before unserialization '.print_r($bean['data'],true));
                
                // pull out the data
                $person = unserialize(base64_decode($bean['data']));

                SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpToSugarCRM->run processing person: '.print_r($person,true));
            
                // process the data
                if ($this->process_person($target_list,$person) === true)
                {
                    // success, remove the record
                    // tried all sorts of stuff here to delete, but only straight-up query did it for some reason
                    // a combo of getting beans with get_list and cached stuff with beanfactory i think is what the issue was
                    SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_MailchimpToSugarCRM->process_person ".$bean['id']." processed successfully. Deleting.");
                    $sql = "DELETE FROM sugarchimp_mc WHERE id='".$db->quote($bean['id'])."'";
                    $result = $db->query($sql);
                }
                else 
                {
                    SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpToSugarCRM->process_person failed to process for: '.print_r($person,true));
                }
            
                $this->update_timer();
            }
        }
        
        return $this->end(array(
            'status' => 'success',
            'message' => 'All persons are processed from Mailchimp to sync to SugarCRM.',
        ));
    }
    
    // update 6/1/17 - Needs to use the Settings same as Subscribe webhook
    // need to go grab any interest groups necessary, so mimic'ing the webhook call
    // sugarchimp will try to find existing contacts in CRM or create it based on the default_module
    // it will then update the records with the data provided from mailchimp
    // and lastly it will add the records to the target list
    function process_person($target_list,$person)
    {
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpToSugarCRM->process_person start');
        
        if (empty($target_list))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpToSugarCRM->process_person target_list is empty');
            //This causes sync to get stuck 
            return false;
        }
        
        if (empty($person))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpToSugarCRM->process_person person is empty');
            //This causes sync to get stuck 
            return false;
        }

        // if email already exists in Sugar, make sure it is marked as optin and valid
        SugarChimp_Helper::mark_email_valid($person['email']);
        SugarChimp_Helper::mark_email_optin($person['email']);
        
        // update beans with this email address on the list
        $beans_to_update = SugarChimp_Helper::find_beans_by_email($person['email'],$target_list->mailchimp_list_name_c);
        
        // if no beans on the list
        // add appropriate beans to the list
        // does not update their data, only adds them to the list
        if (empty($beans_to_update))
        {
            SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_MailchimpToSugarCRM::process_person - no beans with the email ".print_r($person['email'],true)." on the list.");
            $result = SugarChimp_Helper::add_to_list_by_email($person['email'],$target_list,$target_list->mailchimp_default_module_c);
            $beans_to_update = $result['beans'];
            $person['create_new'] = $result['create_new'];
            
            if(empty($beans_to_update))
            {
                //based on settings, need to create a new default module record
                if($result['create_new'] === true)
                {
                    $default_module = $target_list->mailchimp_default_module_c;
                    SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_MailchimpToSugarCRM::process_person - no beans added to list. Creating new {$default_module} bean.");
                    $beans_to_update = array();
                    $beans_to_update[]= SugarChimp_Helper::get_empty_bean($target_list->mailchimp_default_module_c);
                    
                }
                else
                {
                    SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_MailchimpToSugarCRM::process_person - no appropriate beans to add to list. Exiting.");
                    return true;
                }
            }
        }
        
        $updated_beans = SugarChimp_Helper::update_beans_from_mailchimp_sync($target_list,$beans_to_update,$person);
        return true;
    }
    
    // responsible for returning the next job name in the sugarchimp queue for a particular list
    function get_next_mailchimp_list_id()
    {
        global $db;
        
        // select the oldest job from the list
        $sql = "SELECT mailchimp_list_id
                FROM sugarchimp_mc
                ORDER BY date_entered ASC";
        
        $sql = $db->limitQuery($sql,0,1,false,'',false);
        
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpToSugar->get_next_mailchimp_list_id get oldest job: '.$sql);
        
        $result = $db->query($sql);
        $row = $db->fetchByAssoc($result);
        
        if (empty($row['mailchimp_list_id']))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpToSugar->get_next_mailchimp_list_id There are no jobs to be processed for the list.');
            return false;
        }
        
        return $row['mailchimp_list_id'];
    }
}