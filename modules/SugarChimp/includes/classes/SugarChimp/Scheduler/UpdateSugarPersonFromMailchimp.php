<?php





require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Scheduler/Driver.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/FieldMap.php');

class SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp extends SugarChimp_Scheduler_Driver 
{
    // the main method used here to get data from mailchimp only
    // allows us to grab data for 50 subscribers at a time, so that's why there's a limitQuery
    // https://apidocs.mailchimp.com/api/2.0/lists/member-info.php
    private $max_records_per_run = 1;
    
    function run()
    {
        global $db;
        
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->run Running...');

        $this->update_timer();

        $job_updatesugarpersonmc_enabled = SugarChimp_Setting::retrieve('job_updatesugarpersonmc_enabled');
        if(empty($job_updatesugarpersonmc_enabled))
        {
            SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->run - job_updatesugarpersonmc_enabled is false, not enabled. Will not perform this job.");

            // When job is disabled, clear records from this queue           
            $sql = "TRUNCATE TABLE sugarchimp_mc_people";
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->run: emptying sugarchimp_mc_people table sql: '.$sql);

            if (!$result = $db->query($sql))
            {
                SugarChimp_Helper::log('debug','SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->run: could not clear sugarchimp_mc_people table. sql failed: '.$sql);
                return $this->end(array(
                    'status' => 'success',
                    'message' => 'Update Sugar Person From MailChimp was skipped, but the table could not be emptied.',
                ));
            }

            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->run: sugarchimp_mc_people table cleared successfully.');

            return $this->end(array(
                'status' => 'success',
                'message' => 'Update Sugar Person From MailChimp job is disabled.',
            ));
        }

        while ($data = $this->get_next_mailchimp_person())
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->run mailchimp_list_id: '.print_r($mailchimp_list_id,true));
            
            $id = $data['id'];
            $email = trim($data['email']);
            $mailchimp_list_id = $data['mailchimp_list_id'];

            $target_list = BeanFactory::newBean('ProspectLists');
            $target_list->retrieve_by_string_fields(array('mailchimp_list_name_c' => $mailchimp_list_id));

            // make sure the target list is synced
            if (empty($target_list) || empty($target_list->id))
            {
                // could get stuck in an infinite loop if we don't error it out here
                // log the error, remove the items from the queue and move on
                SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->run There are no target lists for mailchimp list id: '.print_r($mailchimp_list_id,true));
                $this->remove_from_queue($id);
                
                // go to the next person
                continue;
            }

            if (empty($email))
            {
                SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->run the email address field was empty, nothing to do here for list: '.print_r($mailchimp_list_id,true));
                // could get stuck in an infinite loop if we don't error it out here
                // log the error, remove the items from the queue and move on
                $this->remove_from_queue($id);

                // go to the next person
                continue;
            }

            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->run found person to insert/update: '.print_r($email,true));

            $response = MailChimp_API::get_member($mailchimp_list_id,$email,array(
                'fields' => 'id,merge_fields,interests,member_rating,email_address',
            ));

            SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp api::get_member response: ".print_r($response,true));

            // make sure we have good data
            if (empty($response['merge_fields']) or !is_array($response['merge_fields']))
            {
                SugarChimp_Helper::log('fatal',"SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp api::get_member could not get data for list: ".print_r($mailchimp_list_id,true)." email: ".print_r($email,true)." result: ".print_r($response,true));
            }
            else
            {
                // fill in the member rating
                $response['merge_fields']['MEMBER_RATING'] = empty($response['member_rating']) ? '0' : $response['member_rating'];
                $response['merge_fields']['NEW-EMAIL'] = $response['email_address'];

                if ($this->process_person($target_list,$response) === false)
                {
                    // failed
                    SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->process_person failed to process for: '.print_r($response,true));
                }
                else
                {
                    SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->process_person ".print_r($email,true)." processed successfully. Deleting.");
                }
            }

            // remove the record, even if it failed, don't want infinite loops
            // tried all sorts of stuff here to delete, but only straight-up query did it for some reason
            // a combo of getting beans with get_list and cached stuff with beanfactory i think is what the issue was
            $this->remove_from_queue($id);

            $this->update_timer();
        }
        
        return $this->end(array(
            'status' => 'success',
            'message' => 'All persons are processed from Mailchimp People to sync to SugarCRM.',
        ));
    }
    
    // update 6/1/17 - Needs to use the Settings same as Subscribe webhook
    // sugarchimp will try to find existing contacts in CRM or create it based on the default_module
    // it will then update the records with the data provided from mailchimp
    // and lastly it will add the records to the target list if they aren't already
    function process_person($target_list,$person)
    {
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->process_person start');
        
        if (empty($target_list))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->process_person target_list is empty');
            return false;
        }
        
        if (empty($person))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->process_person person is empty');
            return false;
        }
        $subscriber_sync_enabled = SugarChimp_Setting::retrieve('subscriber_sync_enabled');
        if(empty($subscriber_sync_enabled))
        {
            SugarChimp_Helper::log('warning',"SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->process_person - All MailChimp to Sugar Subscriber Syncing is Disabled. Ignoring Webhook.");
            return false;
        }
        
        SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->process_person target_list {$target_list->id}");
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->process_person person '.print_r($person,true));
        //new workflow
        $default_module = $target_list->mailchimp_default_module_c;

        // if email already exists in Sugar, make sure it is marked as optin and valid
        SugarChimp_Helper::mark_email_valid($person['email_address']);
        SugarChimp_Helper::mark_email_optin($person['email_address']);
        
        // update beans with this email address on the list
        $beans_to_update = SugarChimp_Helper::find_beans_by_email($person['email_address'],$target_list->mailchimp_list_name_c);
        
        // if no beans on the list
        // add appropriate beans to the list
        // does not update their data, only adds them to the list
        if (empty($beans_to_update))
        {
            SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_UpdateSugarPersonFromMailChimp->process_person - no beans with the email ".print_r($person['email_address'],true)." on the list.");
            $result = SugarChimp_Helper::add_to_list_by_email($person['email_address'],$target_list,$default_module);
            $beans_to_update = $result['beans'];
            $new_record_created = $result['create_new'];
            
            if(empty($beans_to_update))
            {
                //based on settings, need to create a new default module record
                if($result['create_new'] === true)
                {
                    SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_UpdateSugarPersonFromMailChimp->process_person - no beans added to list. Creating new {$default_module} bean.");
                    $beans_to_update = array();
                    $beans_to_update[]= SugarChimp_Helper::get_empty_bean($default_module);
                }
                else
                {
                    SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_UpdateSugarPersonFromMailChimp->process_person - no appropriate beans to add to list. Exiting.");
                    return false;
                }
            }
        }
        
        // need to pass beans to the update_bean function as we have previously
        // all other beans have already been related to the target list
        $updated_beans = SugarChimp_Helper::update_beans_with_mailchimp_data($target_list->mailchimp_list_name_c,$beans_to_update,$person['merge_fields'],$person['interests']);
        
        // if it is a newly created bean, then this function will also add it to the target list
        if($new_record_created)
        {
            $result = SugarChimp_Helper::add_beans_to_list($target_list,$beans_to_update);

            if ($result !== true)
            {
                SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_UpdateSugarPersonFromMailChimp->process_person - update_beans_from_mailchimp failed');
                return false;
            }
        }

        return true;
    }
    
    // responsible for returning the next job name in the sugarchimp queue for a particular list
    function get_next_mailchimp_person()
    {
        global $db;
        
        // select the oldest job from the list
        $sql = "SELECT id, mailchimp_list_id, data
                FROM sugarchimp_mc_people
                ORDER BY date_entered ASC";
        
        $sql = $db->limitQuery($sql,0,$db->quote($this->max_records_per_run),false,'',false);
        
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->get_next_mailchimp_person get oldest job: '.$sql);
        
        $result = $db->query($sql);
        $row = $db->fetchByAssoc($result);
        
        if (empty($row['mailchimp_list_id']))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->get_next_mailchimp_person There are no jobs to be processed for the list.');
            return false;
        }

        return array(
            'id' => $row['id'],
            'mailchimp_list_id'=>$row['mailchimp_list_id'],
            'email' => $row['data']
        );
    }

    function remove_from_queue($id)
    {
        global $db;
        
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->remove_from_queue id: '.print_r($id,true));
        
        $sql = "DELETE FROM sugarchimp_mc_people WHERE id='".$db->quote($id)."'";

        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_UpdateSugarPersonFromMailchimp->remove_from_queue sql: '.print_r($sql,true));

        $result = $db->query($sql);
    }
}