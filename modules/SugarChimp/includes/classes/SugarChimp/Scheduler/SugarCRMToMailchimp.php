<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Scheduler/Driver.php');

class SugarChimp_Scheduler_SugarCRMToMailchimp extends SugarChimp_Scheduler_Driver 
{
    protected $max_jobs_to_process = 200;

    function run()
    {
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarToMailchimp->run Running...');

        $this->update_timer();
    
        // get all mailchimp target lists
        $list_ids = SugarChimp_Helper::get_lists();
    
        if (empty($list_ids))
        {
            // there aren't any lists to sync
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarToMailchimp->run There are no Target Lists to sync with Mailchimp.');
            return $this->end(array(
                'status' => 'success',
                'message' => 'There are no Target Lists to sync with Mailchimp.',
            ));
        }

        foreach ($list_ids as $list_id)
        {
            if (empty($list_id))
            {
                // for some reason some lists on upgraded systems list_ids get set to '0' instead of emptying out
                // just ignore it at this point
                SugarChimp_Helper::log('fatal', "SugarChimp_Scheduler_SugarToMailchimp->run There's an empty list id, ignoring it and moving on, list_ids: ".print_r($list_ids,true));
                continue;
            }
            
            if ($this->process_list($list_id) !== true)
            {
                // it's unlikely we'll reach here because an exception should be thrown if error happens with a list
                // but either way, if we get here, end with error
                SugarChimp_Helper::log('fatal', "SugarChimp_Scheduler_SugarToMailchimp->run A list failed to process {$list_id}");
                return $this->end(array(
                    'status' => 'error',
                    'message' => "A list failed to process {$list_id}",
                ));
            }
        
            $this->update_timer();
        }
        
        return $this->end(array(
            'status' => 'success',
            'message' => 'There are no more jobs to run.',
        ));
    }
    
    function process_list($list_id)
    {
        if (empty($list_id))
        {
            return false;
        }
        
        // get the jobs to process
        while ($job_name = $this->get_next_job_name($list_id))
        {
            // $job_name contains the job we want to process
            // this gets the timestamp of the next event that needs to be processed
            $until_time = $this->get_next_until_time($list_id,$job_name);
            
            // get all job data we need to process for the list, job_name and until_time
            $jobs_data = $this->get_jobs_to_process($list_id,$job_name,$until_time);

            // run the job
            $job_result = SugarChimp_Job::forge($job_name,$list_id,$jobs_data)->run();
            
            if ($job_result !== true)
            {
                /**
                What should we do if stuff fails?
                move it to the back of the queue?
                notify admin and stop scheduler?
                
                we decided to leave it in the queue
                create a error count field in configurator
                increment everytime an error happens
                set to zero on success
                once it reaches a certain number (5 or 10?), email admin user
                */
                // the job failed
                SugarChimp_Helper::log('fatal',"SugarChimp_Scheduler_SugarToMailchimp->process_list {$job_name} failed to run for {$list_id}.");
                throw new Exception("SugarChimp_Scheduler_SugarToMailchimp->process_list {$job_name} failed to run for {$list_id}.");
            }
            
            // the job was successful
            // delete the jobs from the sugarchimp queue
            $this->delete_jobs($list_id,$job_name,$until_time);

            // update the timer, see if we can do another run
            $this->update_timer();
        }
        
        // end with success
        return true;
    }

    // returns all the jobs in the sugarchimp queue that need to be processed
    function delete_jobs($list_id,$job_name,$until_time=false)
    {
        global $db;
        
        $date_entered = '';
        if (!empty($until_time))
        {
            // if it's not empty, there are other jobs in the table
            // set the where
            $date_entered = " AND date_entered <= '".$db->quote($until_time)."' ";
        }
        if($db->dbType == 'mysql')
        {
            // generate mysql specific query
            // the db limitQuery method doesn't work well here
            
            $sql = "DELETE FROM sugarchimp
                    WHERE 
                        mailchimp_list_id = '".$db->quote($list_id)."' AND
                        name = '".$db->quote($job_name)."'
                        {$date_entered}
                    ORDER BY date_entered ASC
                    LIMIT ".$db->quote($this->max_jobs_to_process);
        }
        else
        {
            
            // select all jobs from the first oldest until the next oldest
            // we must manually write this query because $db->limitquery fails here
            // it doesn't let you define the columns to select after it adds the limit code
            // it simply does a select * (see includes/database/MssqlManager.php limitQuery method)
            $sql = "DELETE FROM sugarchimp WHERE id IN ( 
                        SELECT TOP ".$db->quote($this->max_jobs_to_process)." a.aid FROM (
                            SELECT id AS aid, ROW_NUMBER()
                            OVER (ORDER BY date_entered ASC) AS row_number
                            FROM sugarchimp
                            WHERE 
                                mailchimp_list_id = '".$db->quote($list_id)."' AND
                                name = '".$db->quote($job_name)."'
                        ) AS a
                        WHERE row_number > 0 
                    )";
        }

        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarToMailchimp->delete_jobs delete the jobs that we just ran: '.$sql);
        
        $jobs = array();
        
        // return those jobs
        $result = $db->query($sql);

        return true;
    }

    // responsible for returning the next job name in the sugarchimp queue for a particular list
    function get_next_job_name($list_id)
    {
        global $db;
        
        // select the oldest job from the list
        $sql = "SELECT name 
                FROM sugarchimp
                WHERE 
                    mailchimp_list_id = '".$db->quote($list_id)."'
                ORDER BY date_entered ASC";
        
        $sql = $db->limitQuery($sql,0,1,false,'',false);
        
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarToMailchimp->get_next_job_name_to_process get oldest job: '.$sql);
        
        $result = $db->query($sql);
        $row = $db->fetchByAssoc($result);
        
        if (empty($row['name']))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarToMailchimp->get_next_job_name_to_process There are no jobs to be processed for the list.');
            return false;
        }
        
        return $row['name'];
    }
    
    // responsible for getting the until timestamp needed to know how many jobs need to be run for the current list and job name
    function get_next_until_time($list_id,$job_name)
    {
        global $db;
        
        // select the next old job from the list
        // limitQuery issue doesn't let us use the $db->limitQuery method in includes/database/MssqlManager.php limitQuery method
        // it doesn't hand'e the order by properly
        // have to hard code the queries
        if($db->dbType == 'mysql')
        {
            $sql = "SELECT date_entered 
                    FROM sugarchimp
                    WHERE 
                        mailchimp_list_id = '".$db->quote($list_id)."' AND
                        name != '".$db->quote($job_name)."'
                    ORDER BY date_entered ASC
                    LIMIT 0,1";
        }
        else
        {
            $sql = "SELECT TOP 1 * 
                    FROM (
                        SELECT date_entered, ROW_NUMBER()
                        OVER (ORDER BY date_entered ASC) AS row_number
                        FROM sugarchimp
                        WHERE 
                            mailchimp_list_id = '".$db->quote($list_id)."' AND
                            name != '".$db->quote($job_name)."'
                    ) AS a
                    WHERE row_number > 0";
        }
        
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarToMailchimp->get_jobs_to_process get next oldest job: '.$sql);
        
        $result = $db->query($sql);
        $row = $db->fetchByAssoc($result);

        if (empty($row['date_entered']))
        {
            // if there's no date entered, the current job_name is the only job left to process
            return false;
        }
                
        return $row['date_entered'];
    }
    
    // returns all the jobs in the sugarchimp queue that need to be processed
    function get_jobs_to_process($list_id,$job_name,$until_time=false)
    {
        global $db;
        
        $date_entered = '';
        if (!empty($until_time))
        {
            // if it's not empty, there are other jobs in the table
            // set the where
            $date_entered = " AND date_entered <= '".$db->quote($until_time)."' ";
        }
        
        // select all jobs from the first oldest until the next oldest
        $sql = "SELECT id, param1, param2, param3, param4, param5
                FROM sugarchimp
                WHERE 
                    mailchimp_list_id = '".$db->quote($list_id)."' AND
                    name = '".$db->quote($job_name)."'
                    {$date_entered}
                ORDER BY date_entered ASC";
        
        $sql = $db->limitQuery($sql,0,$db->quote($this->max_jobs_to_process),false,'',false);
        
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarToMailchimp->get_jobs_to_process get the jobs to process: '.$sql);
        
        $jobs = array();
        
        // return those jobs
        $result = $db->query($sql);
        while ($row = $db->fetchByAssoc($result))
        {
            $jobs []= $row;
        }

        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_SugarToMailchimp->get_jobs_to_process jobs to process: '.print_r($jobs,true));

        return $jobs;
    }
}