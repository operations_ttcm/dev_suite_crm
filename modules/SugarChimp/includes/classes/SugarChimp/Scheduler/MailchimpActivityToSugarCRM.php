<?php






require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Activity.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Queue.php');
require_once('modules/SugarChimp/includes/vendor/Mailchimp/Mailchimp.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Scheduler/Driver.php');

class SugarChimp_Scheduler_MailchimpActivityToSugarCRM extends SugarChimp_Scheduler_Driver 
{
    private $max_records_per_run = 200;
    private $default_days_to_sync = 30;
    
    function run()
    {
        global $db;

        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->run Running...');

        // check to make sure you're on pro or above
        $current_plan = SugarChimp_Helper::get_current_plan();

        if (empty($current_plan) || ($current_plan != 'professional' && $current_plan != 'ultimate'))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->run: is not available for your SugarChimp subscription plan. Contact SugarChimp support for more details: support@sugarchimp.com - Current Plan: '.print_r($current_plan,true));
            return $this->end(array(
                'status' => 'success',
                'message' => 'Your SugarChimp Subscription does not support Activity Syncing. Contact SugarChimp Support for more details: support@sugarchimp.com',
            ));
        }
        
        $activity_sync_enabled = SugarChimp_Setting::retrieve('activity_sync_enabled');
        if (empty($activity_sync_enabled))
        {
            SugarChimp_Helper::log('debug',"MailChimpToSugarCRMActivity->run() - activity syncing is disabled");
            return $this->end(array(
                'status' => 'success',
                'message' => 'You have MailChimp Activity syncing turned off.',
            ));
        }
        
        // queue initial_campaign_table_fetched
        $initial_campaign_table_fetched = SugarChimp_Setting::retrieve('initial_campaign_table_fetched');
        if (empty($initial_campaign_table_fetched))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpListToSugarCRM->run: initial_campaign_table_fetched is empty, queue_GetMCCampaignsFromActivities_job');
            SugarChimp_Helper::queue_GetMCCampaignsFromActivities_job();
        }

        $this->update_timer();
        
        while ($mailchimp_list_id = $this->get_next_mailchimp_list_id())
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->run mailchimp_list_id: '.print_r($mailchimp_list_id,true));

            $sql = "SELECT id,mailchimp_campaign_id,include_empty,data
                    FROM sugarchimp_mc_activity 
                    WHERE mailchimp_list_id='".$db->quote($mailchimp_list_id)."'";
                    
            $sql = $db->limitQuery($sql,0,$db->quote($this->max_records_per_run),false,'',false);

            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->run sql: '.$sql);
                
            if (!$result = $db->query($sql))
            {
                return $this->end(array(
                    'status' => 'error',
                    'message' => "Query failure when pulling records from sugarchimp_mc_activity table. See logs.",
                ));
            }

            $activities = array();
            while ($row = $db->fetchByAssoc($result))
            {
                $activities []= array(
                    'id' => $row['id'],
                    'mailchimp_campaign_id' => $row['mailchimp_campaign_id'],
                    'include_empty' => $row['include_empty'],
                    'data' => $row['data'],
                );
            }

            if (empty($activities))
            {
                SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->run There are no activities from Mailchimp to sync to SugarCRM, mailchimp list id: '.print_r($mailchimp_list_id,true));
                // could get stuck in an infinite loop if we don't error it out here
                return $this->end(array(
                    'status' => 'error',
                    'message' => "There are no activities from Mailchimp to sync to SugarCRM, mailchimp list id: {$mailchimp_list_id}",
                ));
            }

            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->run found activities to process: '.count($activities).' number activities');

            $duplicate_check = SugarChimp_Setting::retrieve('activity_duplicate_check');

            // loop through each bean in the sugarcrm_mc table
            foreach ($activities as $activity_data)
            {
                SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->run before unserialization '.print_r($activity_data['data'],true));
                
                // pull out the data
                $activity = unserialize(base64_decode($activity_data['data']));

                SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->run processing activity '.print_r($activity,true));

                // process the data
                if ($this->process_activity($mailchimp_list_id,$activity_data['mailchimp_campaign_id'],$activity_data['include_empty'],$activity,$duplicate_check) === true)
                {
                    // success, remove the record
                    SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_MailchimpActivityToSugarCRM->run ".$activity_data['id']." processed successfully. Deleting.");
                    $sql = "DELETE FROM sugarchimp_mc_activity WHERE id='".$db->quote($activity_data['id'])."'";
                    $result = $db->query($sql);
                }
                else 
                {
                    SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->run failed to process for: '.print_r($activity,true));
                }
            
                $this->update_timer();
            }
        }
        
        // make sure there's time left
        $this->update_timer();
        
        // if there are no more activities to process
        // check if queued job exists
        // if one does, let the job run before we do anything else
        if (SugarChimp_Activity::is_activity_import_job_queued() === false)
        {
            // if we haven't done the initial campaign import, do it now
            $initial_activity_table_fetched = SugarChimp_Setting::retrieve('initial_activity_table_fetched');
            if (empty($initial_activity_table_fetched))
            {
                // get campaigns from mailchimp
                try 
                {
                    $apikey = SugarChimp_Setting::retrieve('apikey');
                    
                    $mc = new Mailchimp($apikey);

                    $days_to_sync = SugarChimp_Setting::retrieve('days_to_sync_activities');
                    
                    if (empty($days_to_sync))
                    {
                        $days_to_sync = $this->default_days_to_sync;
                    }

                    $a_year_ago = date("Y-m-d H:i:s",time() - ($days_to_sync * 24 * 60 * 60)); // now minus a year in mailchimp time
                    SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->run: fetching campaigns sent since '.$a_year_ago);
                    
                    // $filter = array(
                    //     'status' => 'sent',
                    //     'sendtime_start' => $a_year_ago,
                    // );
                    // $result = $mc->campaigns->getList($filter);
                    // SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->run: MailChimp Campaign result: '.print_r($result,true));

                    $filter = array(
                        'fields'=>'campaigns.id,campaigns.send_time,campaigns.recipients.list_id',
                        'count'=>1000,
                        'status'=>'sent',
                        'since_send_time'=>$a_year_ago
                    );

                    $result = MailChimp_API::get_campaigns($filter);

                    SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->run: get_campaigns result: '.print_r($result,true));
                }
                catch (Exception $e)
                {
                    // couldn't get campaigns and add to table
                    // need to error out
                    SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->run: could not retrieve mailchimp campaigns');
                    return $this->end(array(
                        'status' => 'error',
                        'message' => "Could not retrieve MailChimp campaigns. Exception thrown: ".$e->getMessage(),
                    ));
                }

                if(!empty($result['campaigns']) && is_array($result['campaigns'])) 
                {
                    // add them to activity tracker table
                    foreach($result['campaigns'] as $campaign) 
                    {
                        $include_send_activities = SugarChimp_Setting::retrieve('include_send_activities');

                        // add it to the activity tracker table
                        SugarChimp_Activity::track_campaign_activity($campaign['recipients']['list_id'],$campaign['id'],$campaign['send_time'],false,$include_send_activities,false);

                        // If not already there, add Campaign to sugarchimp_mc_campaign Table
                        // also adds to the maintained dropdown menu
                        SugarChimp_Helper::add_campaign_to_table($campaign['id']);
                    }
                }
                else
                {
                    SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->run: initial import, there are no MailChimp Campaigns on the account yet, nothing needs syncing');
                }
                
                // set config.initial_activity_table_fetched = true
                SugarChimp_Setting::set('initial_activity_table_fetched',true);
            }
            // initial_activity_table_fetched has already been fetched
            // grab next campaign whose activities need to be imported
            else
            {
                // queue next campaign for activity downloading
                SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->run: queue up the next import activity job');
                
                $days_to_sync = SugarChimp_Setting::retrieve('days_to_sync_activities');
                
                if (empty($days_to_sync))
                {
                    $days_to_sync = $this->default_days_to_sync;
                }
                
                // select the next campaign up
                // limitQuery issue doesn't let us use the $db->limitQuery method in includes/database/MssqlManager.php limitQuery method
                // it doesn't hand'e the order by properly
                // have to hard code the queries
                if($db->dbType == 'mysql')
                {
                    $sql = "SELECT mailchimp_campaign_id, mailchimp_list_id, initial_sync, last_sync
                            FROM sugarchimp_activity_tracker
                            WHERE
                                ".$db->convert('start_date','add_date',array($days_to_sync,'DAY'))." > ".$db->now()." AND
                                (last_sync IS NULL OR ".$db->convert('last_sync','add_date',array(1,'DAY'))." < ".$db->now().")
                            ORDER BY last_sync ASC, start_date DESC
                            LIMIT 0,1";
                }
                else
                {
                    $sql = "SELECT TOP 1 * 
                            FROM (
                                SELECT mailchimp_campaign_id, mailchimp_list_id, initial_sync, last_sync, ROW_NUMBER()
                                OVER (ORDER BY last_sync ASC, start_date DESC) AS row_number
                                FROM sugarchimp_activity_tracker
                                WHERE
                                    ".$db->convert('start_date','add_date',array($days_to_sync,'DAY'))." > ".$db->now()." AND
                                    (last_sync IS NULL OR ".$db->convert('last_sync','add_date',array(1,'DAY'))." < ".$db->now().")
                            ) AS a
                            WHERE row_number > 0";
                }
                
                SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->run: sql to get next job to queue: '.$sql);
                
                if (!$result = $db->query($sql))
                {
                    return $this->end(array(
                        'status' => 'error',
                        'message' => "Could not retrieve next MailChimp Campaign to download activities for. Query failed: ".$sql,
                    ));
                }
                
                $row = $db->fetchByAssoc($result);
                
                if (!empty($row['mailchimp_campaign_id']) && !empty($row['mailchimp_list_id']))
                {

                    if (empty($row['initial_sync']))
                    {
                        $include_empty = false;
                    }
                    else
                    {
                        $include_empty = true;
                    }
                    
                    if (empty($row['last_sync']) or $row['last_sync'] == '0000-00-00 00:00:00')
                    {
                        $since = false;
                    }
                    else
                    {
                        $since = $row['last_sync'];
                    }
                    
                    // queue it
                    SugarChimp_Helper::queue_MailChimpToSugarCRMActivity_job(array(
                        'mailchimp_campaign_id' => $row['mailchimp_campaign_id'],
                        'mailchimp_list_id' => $row['mailchimp_list_id'],
                        'include_empty' => $include_empty,
                        'since' => $since,
                    ));
                }
                else
                {
                    SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->run: activity tracker, there are no MailChimp Campaigns that need to be imported at this time.');
                }
            }
        }
        
        return $this->end(array(
            'status' => 'success',
            'message' => 'All activities are processed from Mailchimp to sync to SugarCRM.',
        ));
    }
    
    // take what activities mailchimp gave us and put them in the sugarchimp activities table
    function process_activity($mailchimp_list_id,$mailchimp_campaign_id,$include_empty,$activity_data, $duplicate_check=true)
    {
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity start');
        
        if (empty($mailchimp_list_id))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity mailchimp_list_id is empty');
            return false;
        }
        
        if (empty($mailchimp_campaign_id))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity mailchimp_campaign_id is empty');
            return false;
        }
        
        if (empty($activity_data))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity activity_data is empty');
            return false;
        }
        
        if (empty($include_empty))
        {
            $include_empty = false;
        }
        else
        {
            $include_empty = true;
        }
        
        global $db;


        SugarChimp_Helper::log('debug',"SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity mailchimp_list_id ".print_r($mailchimp_list_id,true));
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity mailchimp_campaign_id '.print_r($mailchimp_campaign_id,true));
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity include_empty '.print_r($include_empty,true));
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity activity_data '.print_r($activity_data,true));
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity duplicate_check '.print_r($duplicate_check,true));
        
        $activity = json_decode($activity_data);

        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity activity '.print_r($activity,true));

        $email = key($activity);
        $activities = current($activity);

        // Get beans associated from the email address, use later to relate them to the sugarchimpactivity
        $beans = SugarChimp_Helper::find_beans_by_email($email);

        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity found '.count($beans).' to relate to the activity.');
        
        /**
        TODO 
        
        don't do initial import every time
        need to figure out a way to fire it only on the first run
        
        */
        
        // store each of the activity record IDs to be related to $beans later
        // more efficient to do all at once instead of individually
        $activity_records = array();
        
        if ($include_empty === true)
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity initial import, add send activity for '.$email);
            
            // save the initial send
            $activity_record = SugarChimp_Activity::import(array(
                'name' => $email,
                'description' => $activity_data,
                'activity' => 'send',
                'timestamp' => substr($db->now(),1,-1), // have to use substr as now() returns it quoted, timestamp needs to come from the campaign send webhook or something if we support grabbing old campaigns
                'url' => '',
                'ip' => '',
                'mailchimp_list_id' => $mailchimp_list_id,
                'mailchimp_campaign_id' => $mailchimp_campaign_id,
                'include_empty' => $include_empty,
            ),$duplicate_check);

            if ($activity_record instanceOf SugarChimpActivity)
            {
                SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity activity record found: '.$activity_record->id);
                $activity_records []= $activity_record;
            }
            else
            {
                SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity activity failed to be imported: '.print_r($activity_record,true));
            }
        }
        
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity email '.print_r($email,true));
        if (is_array($activities) && count($activities)>0)
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity '.count($activities).' activities for this person');
            foreach ($activities as $a)
            {
                SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity activity details '.print_r($a,true));
                
                $activity_record = SugarChimp_Activity::import(array(
                    'name' => $email,
                    'description' => $activity_data,
                    'activity' => (empty($a->action)) ? '' : $a->action,
                    'timestamp' => (empty($a->timestamp)) ? '' : $a->timestamp,
                    'url' => (empty($a->url)) ? '' : $a->url,
                    'ip' => (empty($a->ip)) ? '' : $a->ip,
                    'mailchimp_list_id' => $mailchimp_list_id,
                    'mailchimp_campaign_id' => $mailchimp_campaign_id,
                    'include_empty' => $include_empty,
                ),$duplicate_check);
                
                if ($activity_record instanceOf SugarChimpActivity)
                {
                    SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity activity record found: '.$activity_record->id);
                    $activity_records []= $activity_record;
                }
                else
                {
                    SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity activity failed to be imported: '.print_r($activity_data,true));
                }

                // if the activity is an open or click and there are related beans
                // queue the email address to be processed for a MC to sugar update
                // this will update the member rating if any changes have been made
                if (!empty($beans) && ($activity_record->activity == 'open' or $activity_record->activity == 'click'))
                {
                    /**
                    THIS IS STILL SUPPORTED, we thought about not supporting in 10/17/2016. 
                    We will address this when V2 is completely turned off or stops returning what we expect.
                    Here's what we were thinking:

                    AS OF 7.8 (v3 support) We are not updating the subscribers when opens and clicks come in
                    MailChimp changed the call for how we can get profile data from individual subscribers
                    in v2 we could make a single call to get as many profile updates as we needed
                    in v3 we would need to use the batch API, however we cannot easily retrieve the batch api responses
                    because we have to unzip the stuff, and to do that we need special php libraries, b/c of OD and other
                    system specific stuff, we can't guarantee those libraries will be present or can't be added to the system.

                    The best solution would be to have a middle man (sugarchimp.com) faciliate the batch response calls, unzip the 
                    data and return the response. SC would make a call to sc.com. (Even better would be to use curl to grab the raw
                    data of the .tar.gz file, then have a php implementation of a .tar.gz unzipper that would get the response we
                    need then deal with it, all in memory)

                    If we don't do that, the next best thing would be to use the export api, have a special function
                    that does what we already do. only export the mailchimp rating field at some interval. The down side
                    is that it could be a lot of extra work and is server intensive.

                    */

                    // queue a MC to Sugar update for email address $sca->name
                    // this method is prepared to batch insert the update records if we need to in the future
                    SugarChimp_Queue::queue_mailchimp_to_sugar_person_update($mailchimp_list_id,array($activity_record->name));
                }
                else
                {
                    SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity not queueing mc to sugar person update.');   
                }
            }
        }
        else
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity 0 activities for this person');
        }
        
        // relate the activities to the beans
        if (!empty($activity_records) && is_array($activity_records) && !empty($beans) && is_array($beans))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity attempt to add activity records to beans.');
            if (SugarChimp_Activity::add_activities_to_beans($activity_records,$beans) !== true)
            {
                SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity relating activities and beans failed.');
            }
        }
        
        /** 
        
        Optional TODO - probably just need to add it to the list, then queue a MC to Sugar update 
        as we won't have all the merge data at this point in the code
        
        if we ever do this, below needs a $target_list bean to be present
        what happens if the list is not synced? 
        
        How to handle if email address doesn't exist for activity that comes in?
        See if email address has ever existed in CRM
        If yes, find all beans
        1) Could go ahead and create them based on the default 
        2) Don't create them, it could be from a previously deleted or opted out person
        
        
        // if no beans, nothing matches on the sugar side, so create the record based on target list default modue
        $new_bean_created = false;
        if (empty($beans))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity No beans found for email '.$data['email'].' - creating empty bean');
            $beans = array();
            
            ... need to get default_module from Receiver.php and the run() method in this class
            $beans []= SugarChimp_Helper::get_empty_bean($default_module);
            $new_bean_created = true;
        }
        
        // if a new bean was created because it didn't already exist (unlikely)
        // disable logic hooks on the bean and target list and add the bean to the target list
        if ($new_bean_created === true)
        {
            // disable bean/targetlist logic hooks
            SugarChimp_Helper::disable_logic_hooks($beans);
            SugarChimp_Helper::disable_logic_hooks(array($target_list));
            
            // create/update new bean with data from mailchimp
            // WE DON'T HAVE MERGE DATA HERE
            $result = SugarChimp_Helper::update_beans_from_mailchimp($data['list_id'],$beans,$data['merges']);

            if ($result !== true)
            {
                SugarChimp_Helper::log('fatal','Mailchimp_Webhooks_Receiver::subscribe update_beans_from_mailchimp failed');
                break;
            }

            // relate the record to the target list
            $result = SugarChimp_Helper::add_beans_to_list($target_list,$beans);

            if ($result !== true)
            {
                SugarChimp_Helper::log('fatal','Mailchimp_Webhooks_Receiver::subscribe update_beans_from_mailchimp failed');
                break;
            }
            
            // relate the record to the target list
            $result = SugarChimp_Helper::add_beans_to_list($target_list,$beans);

            if ($result !== true)
            {
                SugarChimp_Helper::log('fatal','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity update_beans_from_mailchimp failed');
                return false;
            }

            // enable logic hooks for the beans
            SugarChimp_Helper::enable_logic_hooks($beans);
            SugarChimp_Helper::enable_logic_hooks(array($target_list));
        }
        */
        /**        

        TODO - could probably do something useful here, things mostly seem to work though
        
        How to handle errors?
        Let them go through?
        Stop everything?

        */

        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpActivityToSugarCRM->process_activity finish');
        
        return true;
    }
    
    // responsible for returning the next job name in the sugarchimp queue for a particular list
    function get_next_mailchimp_list_id()
    {
        global $db;
        
        // select the oldest job from the list
        $sql = "SELECT mailchimp_list_id
                FROM sugarchimp_mc_activity
                ORDER BY date_entered ASC";
                
        $sql = $db->limitQuery($sql,0,1,false,'',false);
        
        SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpToSugar->get_next_mailchimp_list_id get oldest job: '.$sql);
        
        $result = $db->query($sql);
        $row = $db->fetchByAssoc($result);
        
        if (empty($row['mailchimp_list_id']))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Scheduler_MailchimpToSugar->get_next_mailchimp_list_id There are no jobs to be processed for the list.');
            return false;
        }
        
        return $row['mailchimp_list_id'];
    }
}