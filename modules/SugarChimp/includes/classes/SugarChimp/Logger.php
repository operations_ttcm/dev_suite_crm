<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Loader.php');
SugarChimp_Loader::load('Core/Logger');

require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');

use Fanatical\Core\v1a\Logger as Logger;

class SugarChimp_Logger extends Logger
{
	protected static $prefix = "SugarChimp";

	//If you do not want to include location information on log,
	//Then set $format to false
	//Otherwise, parent logger will include that information
	public static function log($level, $message = '', $format = false)
	{
		//If logging_enabled has been set to false
        $logging_enabled = SugarChimp_Setting::retrieve('logging_enabled');
        if(empty($logging_enabled))
        {
            return;
        }

		$message = 'SugarChimp:'.$level.':'.time().': '.$message;
        
        // sugarchimp_logger can be set on status page for Sugar 6
        $sugarchimp_logger = SugarChimp_Setting::retrieve('logger');
        
        // if 'debug', all sugarchimp messages are logged at 'fatal' level
        // this allows you to run sugarchimp on debug mode and the rest of the system at any other level
        // if not 'debug', run on level provided in call
        if (!empty($sugarchimp_logger) && $sugarchimp_logger == 'debug')
        {
            $level = 'fatal';
        }
		parent::log($level, $message, $format);
	}
}