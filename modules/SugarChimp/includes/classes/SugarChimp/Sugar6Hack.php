<?php






class Sugar6Hack
{
    // some additional javascript needs to be added to Sugar Reports in Sugar 6 Pro+
    // to get the mailchimp rating field to work properly
    // using the ole after ui trick for Sugar 6
    function MailchimpRatingReportHack($bean, $event, $arguments=array())
    {
        global $module;
        if ($module == "Reports" && empty($_REQUEST['to_pdf']) && empty($_REQUEST['sugar_body_only']))
        {
            echo "<script>if (typeof filter_defs!=='undefined' && filter_defs && filter_defs['int']) filter_defs['mailchimprating']=filter_defs['int'];</script>";
        }
    }
}