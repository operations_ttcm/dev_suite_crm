<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/FieldMap.php');

class SugarChimp_LogicHook
{
    protected static $fetched_row = array();
    protected static $mailchimp_list_ids = array();

    function PersonUpdateBeforeSave($bean, $event, $arguments) 
    {
        if (SugarChimp_Helper::logic_hooks_enabled($bean) === false) return;
        
        // if it's not a new record, capture the before save data
        if (!empty($bean->id)) 
        {
            SugarChimp_Helper::log('debug','SugarChimp_LogicHook Person Update Before Save');
            // opt out & invalid saving order is different in sugar 6 and 7
            // have to handle each case seperately to properly catch the changes
            global $sugar_version;
            if(preg_match( "/^6.*/", $sugar_version))
            {
                // version 6.* case
                self::$fetched_row[$bean->id] = $bean->fetched_row;

                // set default values, if we have other values they'll be marked below
                self::$fetched_row[$bean->id]['invalid_email'] = '0';
                self::$fetched_row[$bean->id]['email_opt_out'] = '0';

                // if an email address is included, populate the old invalid_email & email_opt_out status
                if (!empty($bean->email1))
                {
                    $sea = BeanFactory::getBean('EmailAddresses');
                    $emails = $sea->getAddressesByGUID($bean->id,$bean->module_dir);
                    SugarChimp_Helper::log('debug','SugarChimp_LogicHook Person Update Before Save: emails: '.print_r($emails,true));
                    if (!empty($emails) && is_array($emails))
                    {
                        // go through the email addresses
                        foreach ($emails as $email)
                        {
                            // find the primary address if one exists
                            if ($email['primary_address'] == '1')
                            {
                                // set the invalid/optout data from before save
                                self::$fetched_row[$bean->id]['invalid_email'] = $email['invalid_email'];
                                self::$fetched_row[$bean->id]['email_opt_out'] = $email['opt_out'];
                                break;
                            }
                        }
                    }
                }
            }
            else if (empty(self::$fetched_row[$bean->id]))
            {
                // version 7 case
                self::$fetched_row[$bean->id] = $bean->fetched_row;                    
                self::$fetched_row[$bean->id]['invalid_email'] = $bean->invalid_email;
                self::$fetched_row[$bean->id]['email_opt_out'] = $bean->email_opt_out;
            }
        }
    }

    function PersonUpdateAfterSave($bean, $event, $arguments) 
    {
        if (SugarChimp_Helper::logic_hooks_enabled($bean) === false) return;

        SugarChimp_Helper::log('debug','SugarChimp_LogicHook Person Update After Save');

        // use temp bean trick to get the new email address value
        // bean will now hold the new value and fetched_row will hold the old
        // without this, bean would have the old value
        $temp_bean = BeanFactory::getBean($bean->module_dir,$bean->id,array('use_cache'=>false));
        $bean->email1 = (isset($temp_bean->email1)) ? $temp_bean->email1 : false;
        
        $bean->invalid_email = (isset($temp_bean->invalid_email)) ? $temp_bean->invalid_email : false;
        $bean->email_opt_out = (isset($temp_bean->email_opt_out)) ? $temp_bean->email_opt_out : false;

        if (isset(self::$fetched_row[$bean->id]))
        {
            // edit record
            SugarChimp_Helper::log('debug',"SugarChimp_LogicHook Edit Record {$bean->module_dir} {$bean->id}");
            SugarChimp_Helper::check_person_changes($bean,self::$fetched_row[$bean->id]);
        }
        else
        {
            // new record
            SugarChimp_Helper::log('debug',"SugarChimp_LogicHook New Record {$bean->module_dir}");
            SugarChimp_Helper::check_person_changes($bean);
        }
    }
    
    function PersonBeforeDelete($bean, $event, $arguments) 
    {
        if (SugarChimp_Helper::logic_hooks_enabled($bean) === false) return;

        SugarChimp_Helper::log('debug',"SugarChimp_LogicHook Person Before Delete {$bean->module_dir} {$bean->id}");
        
        // store the linked listed to be used if we make it to the after delete
        // the queries don't work in after_delete because the deleted fields are set to 1
        self::$mailchimp_list_ids = SugarChimp_Helper::belongs_to_list($bean);
    }

    function PersonAfterDelete($bean, $event, $arguments) 
    {
        if (SugarChimp_Helper::logic_hooks_enabled($bean) === false) return;

        SugarChimp_Helper::log('debug',"SugarChimp_LogicHook Person After Delete {$bean->module_dir} {$bean->id}");
        
        // use the self::$mailchimp_list_ids member from the before_delete to get the
        // related target lists before the delete
        SugarChimp_Helper::check_person_deletes($bean,self::$mailchimp_list_ids);
    }

    function ListUpdateBeforeSave($bean, $event, $arguments) 
    {
        if (SugarChimp_Helper::logic_hooks_enabled($bean) === false) return;

        SugarChimp_Helper::log('debug','SugarChimp_LogicHook List Update Before Save');

        if (!empty($bean->id)) 
        {
            self::$fetched_row[$bean->id] = $bean->fetched_row;
        }
        
        //Check to see if the MailChimp list has been changed
        //if it has then ensure that the new list is not already syncing to another SugarCRM list
        //if it is then reset to original
        //Preference would be to throw a SugarApiExceptionInvalidParameter exception, but that causes some weird redirect
        if (!empty($bean->mailchimp_list_name_c) && $bean->mailchimp_list_name_c != $bean->fetched_row['mailchimp_list_name_c'])
        {
            //is the new selection already syncing with a different SugarCRM list?
            $prospect_list = BeanFactory::getBean('ProspectLists');
            $prospect_list->retrieve_by_string_fields(array('mailchimp_list_name_c' => $bean->mailchimp_list_name_c));

            if(!empty($prospect_list->id)) {
                $bean->mailchimp_list_name_c = $bean->fetched_row['mailchimp_list_name_c'];
                SugarChimp_Helper::log('fatal',"SugarChimp LogicHook Error: Target List ".$bean->name." cannot be synced with MC list ".$bean->mailchimp_list_name_c." as it is already synced with ".$prospect_list->name);
            }
        }
        
        // if a mailchimp list is selected but no default module is, set it to the default for them.
        if (!empty($bean->mailchimp_list_name_c) && empty($bean->mailchimp_default_module_c))
        {
            $bean->mailchimp_default_module_c = SugarChimp_FieldMap::get_default_module();
        }
    }
    
    function ListUpdateAfterSave($bean, $event, $arguments) 
    {
        if (SugarChimp_Helper::logic_hooks_enabled($bean) === false) return;

        SugarChimp_Helper::log('debug','SugarChimp_LogicHook List Update After Save');
        
        if (isset(self::$fetched_row[$bean->id]))
        {
            // edit record
            SugarChimp_Helper::log('debug',"SugarChimp_LogicHook List Update After Save {$bean->module_dir} {$bean->id}");
            SugarChimp_Helper::check_list_changes($bean,self::$fetched_row[$bean->id]);
        }
        else
        {
            // new record
            SugarChimp_Helper::log('debug',"SugarChimp_LogicHook List Update After Save {$bean->module_dir}");
            SugarChimp_Helper::check_list_changes($bean);
        }
    }

    function ListAfterAddPerson($bean, $event, $arguments) 
    {
        if (SugarChimp_Helper::logic_hooks_enabled($bean) === false) return;

        SugarChimp_Helper::log('debug',"SugarChimp_LogicHook List After Add Person {$bean->module_dir} {$bean->id}");
        
        SugarChimp_Helper::check_list_person_added($bean,$arguments['related_module'],$arguments['related_id']);
    }
    
    function ListAfterDeletePerson($bean, $event, $arguments) 
    {
        if (SugarChimp_Helper::logic_hooks_enabled($bean) === false) return;

        SugarChimp_Helper::log('debug',"SugarChimp_LogicHook List After Delete Person {$bean->module_dir} {$bean->id}");

        SugarChimp_Helper::check_list_person_deleted($bean,$arguments['related_module'],$arguments['related_id']);
    }

    function RelatedRecordUpdateBeforeSave($bean, $event, $arguments)
    {
        // we have to have the module_dir field to do anything
        if (empty($bean->module_dir)) return; 

        // check if related field mapping is supported
        if (SugarChimp_FieldMap::is_related_field_mapping_supported() !== true) return;

        // check to see if we support this module as a related mapped module
        if (SugarChimp_FieldMap::is_supported_related_module($bean->module_dir) !== true) return; 

        if (SugarChimp_Helper::logic_hooks_enabled($bean) === false) return;

        SugarChimp_Helper::log('debug',"SugarChimp_LogicHook Related Record Before Save {$bean->module_dir} {$bean->id}");

        // actually check the related mapping for changes, slower than the is_supported_related_module above
        if (SugarChimp_FieldMap::is_mapped_related_module($bean->module_dir) !== true) return;

        if (!empty($bean->id)) 
        {
            self::$fetched_row[$bean->id] = $bean->fetched_row;
        }
    }

    function RelatedRecordUpdateAfterSave($bean, $event, $arguments)
    {
        // we have to have the module_dir field to do anything
        if (empty($bean->module_dir)) return; 

        // check if related field mapping is supported
        if (SugarChimp_FieldMap::is_related_field_mapping_supported() !== true) return;

        // check to see if we support this module as a related mapped module
        if (SugarChimp_FieldMap::is_supported_related_module($bean->module_dir) !== true) return; 

        if (SugarChimp_Helper::logic_hooks_enabled($bean) === false) return;

        // actually check the related mapping for changes, slower than the is_supported_related_module above
        if (SugarChimp_FieldMap::is_mapped_related_module($bean->module_dir) !== true) return;

        if (isset(self::$fetched_row[$bean->id]))
        {
            // edit record
            SugarChimp_Helper::log('debug',"SugarChimp_LogicHook Related Record After Save Edit {$bean->module_dir} {$bean->id}");
            SugarChimp_Helper::check_related_record_changes($bean,self::$fetched_row[$bean->id]);
        }
        else
        {
            // new record
            SugarChimp_Helper::log('debug',"SugarChimp_LogicHook Related Record After Save New {$bean->module_dir}");
            SugarChimp_Helper::check_related_record_changes($bean);
        }        
    }

    function RelatedRecordAfterRelationshipAdd($bean, $event, $arguments)
    {
        // we have to have the module_dir field to do anything
        if (empty($bean->module_dir)) return; 

        // check if related field mapping is supported
        if (SugarChimp_FieldMap::is_related_field_mapping_supported() !== true) return;

        // check to see if we support this module as a related mapped module
        if (SugarChimp_FieldMap::is_supported_related_module($bean->module_dir) !== true) return; 

        if (SugarChimp_Helper::logic_hooks_enabled($bean) === false) return;

        SugarChimp_Helper::log('debug',"SugarChimp_LogicHook Related Record After Relationship Add {$bean->module_dir} {$bean->id}");

        // run the same check that happens for updated related record
        // but force the updates since we know the record is being added 
        SugarChimp_Helper::check_related_record_changes($bean,array(),true);
    }

    function RelatedRecordAfterRelationshipDelete($bean, $event, $arguments)
    {
        // we have to have the module_dir field to do anything
        if (empty($bean->module_dir)) return; 

        // check if related field mapping is supported
        if (SugarChimp_FieldMap::is_related_field_mapping_supported() !== true) return;

        // check to see if we support this module as a related mapped module
        if (SugarChimp_FieldMap::is_supported_related_module($bean->module_dir) !== true) return; 

        if (SugarChimp_Helper::logic_hooks_enabled($bean) === false) return;

        SugarChimp_Helper::log('debug',"SugarChimp_LogicHook Related Record After Relationship Delete {$bean->module_dir} {$bean->id}");
        
        // run the same check that happens for updated related record
        // but force the updates since we know the record is being removed
        SugarChimp_Helper::check_related_record_changes($bean,array(),true);
    }
    function PersonBeforeApiCall($bean, $event)
    {
        $supported_method = array(
            'Contacts'=> array(
                'updateRecord',
            ),
            'Accounts'=> array(
                'updateRecord',
            ),
            'Prospects'=> array(
                'updateRecord',
            ),
            'Leads'=> array(
                'updateRecord',
            ),
        );
        
        $req = $event['request'];
        // SugarChimp_Helper::log('debug',"SugarChimp_LogicHook earlyDetector req: " . print_r($req, true));        
        $args = $req->args;
        // SugarChimp_Helper::log('debug',"SugarChimp_LogicHook earlyDetector args:  " . print_r($args, true));
        $route = $req->route;
        $method = $route['method'];
        $module = empty($args['module']) ? false : $args['module'];
        // SugarChimp_Helper::log('debug',"SugarChimp_LogicHook earlyDetector found:  " . $method . ":" . $module);

        $supported = !empty($module) && !empty($supported_method[$module]) && in_array($method, $supported_method[$module]);

        if($supported) {
            if (!empty($args['id'])) 
            {
                $bean = BeanFactory::getBean($module,$args['id'],array('use_cache'=>false));
                // need help here
                if(!empty($bean))
                {
                    self::$fetched_row[$bean->id] = $bean->fetched_row;
                    self::$fetched_row[$bean->id]['invalid_email'] = $bean->invalid_email;
                    self::$fetched_row[$bean->id]['email_opt_out'] = $bean->email_opt_out;
                    SugarChimp_Helper::log('debug',"SugarChimp_LogicHook PersonBeforeApiCall found been, saved to fetched_row!");
                    // opt out & invalid saving order is different in sugar 6 and 7
                    // have to handle each case seperately to properly catch the changes
                }
            }
        }
    }
    /**
    // Delete hooks are not needed to catch the related record changes
    // Everytime a delete is issued, it will go through all of the related records
    // and issue it's own 'after_relationship_delete' hooks
    function RelatedRecordBeforeDelete($bean, $event, $arguments)
    {
        // // we have to have the module_dir field to do anything
        // if (empty($bean->module_dir)) return; 

        // // check to see if we support this module as a related mapped module
        // if (SugarChimp_FieldMap::is_supported_related_module($bean->module_dir) !== true) return; 

        // if (SugarChimp_Helper::logic_hooks_enabled($bean) === false) return;

        // SugarChimp_Helper::log('debug',"SugarChimp_LogicHook Related Record Before Delete {$bean->module_dir} {$bean->id}");
    }

    function RelatedRecordAfterDelete($bean, $event, $arguments)
    {
        // // we have to have the module_dir field to do anything
        // if (empty($bean->module_dir)) return; 

        // // check to see if we support this module as a related mapped module
        // if (SugarChimp_FieldMap::is_supported_related_module($bean->module_dir) !== true) return; 

        // if (SugarChimp_Helper::logic_hooks_enabled($bean) === false) return;

        // SugarChimp_Helper::log('debug',"SugarChimp_LogicHook Related Record After Delete {$bean->module_dir} {$bean->id}");
        
        // // run the same check that happens for updated related record
        // // but force the updates since we know the record is cancelled
        // // this will queue jobs to clear out the data on the MC side
        // // in the case that a parent related record is deleted
        // SugarChimp_Helper::check_related_record_changes($bean,array(),true);
    }
    */
    
    // NOTE: this method originated from modules/SugarChimp/includes/classes/SugarChip/Sugar6Hack.php
    // there was a weird issue for customer 'dremed' on OD 6.7, the after_ui logic hook that called the method in sugar6hack
    // tried to call from method SugarChimp_LogicHook, so moving it here in case another system has the same issue
    function MailchimpRatingReportHack($bean, $event, $arguments=array())
    {
        // global $module;
        // if ($module == "Reports" && empty($_REQUEST['to_pdf']) && empty($_REQUEST['sugar_body_only']))
        // {
        //     echo "<script>if (typeof filter_defs!=='undefined' && filter_defs && filter_defs['int']) filter_defs['mailchimprating']=filter_defs['int'];</script>";
        // }
    }
}