<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');

require_once('modules/SugarChimp/includes/classes/SugarChimp/Field/Generic.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Field/Basic.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Field/Date.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Field/Group.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Field/Email.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Field/MailchimpListName.php');

require_once('modules/SmartList/includes/classes/SmartList/Loader.php');
SmartList_Loader::load('Core/Field');

use Fanatical\Core\v1a\Field as Field;

class SugarChimp_Field extends Field
{
    public static function forge($driver)
    {
        if (empty($driver))
        {
            return false;
        }

        $driver = "SugarChimp_Field_" . $driver;
        SugarChimp_Helper::log('debug',"Instantiate SugarChimp_Field Driver {$driver}");
        return $driver::forge();
    }
}
