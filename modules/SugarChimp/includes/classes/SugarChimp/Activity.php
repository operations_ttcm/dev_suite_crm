<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');

class SugarChimp_Activity
{
    public static function import($activity,$check_for_duplicates=true)
    {
        SugarChimp_Helper::log('debug','SugarChimp_Activity::import start');

        if (empty($activity) || !is_array($activity))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Activity::import activity is empty or not an array');
            return false;
        }
        
        if (empty($activity['name']))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Activity::import name is required but empty');
            return false;
        }
        
        if (empty($activity['activity']))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Activity::import activity is required but empty');
            return false;
        }
        
        if (empty($activity['mailchimp_list_id']))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Activity::import mailchimp_list_id is required but empty');
            return false;
        }
        
        if (empty($activity['mailchimp_campaign_id']))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Activity::import mailchimp_campaign_id is required but empty');
            return false;
        }
        
        if ($check_for_duplicates === true)
        {
            $dupe = SugarChimp_Activity::is_duplicate($activity);
            if ($dupe !== false)
            {
                return $dupe;
            }
        }
        
        global $db;
        
        // create a new activity record
        $sca = BeanFactory::getBean('SugarChimpActivity');

        $sca->name = $activity['name']; // email address
        $sca->activity = $activity['activity'];
        $sca->mailchimp_list_id = $activity['mailchimp_list_id'];
        $sca->mailchimp_campaign_id = $activity['mailchimp_campaign_id'];

        $sca->include_empty = (empty($activity['include_empty'])) ? false : true;
        $sca->description = (empty($activity['description'])) ? '' : $activity['description'];
        $sca->timestamp = (empty($activity['timestamp'])) ? $db->now() : $activity['timestamp']; // set to current timestamp if it isn't set
        $sca->url = (empty($activity['url'])) ? '' : $activity['url'];
        $sca->ip = (empty($activity['ip'])) ? '' : $activity['ip'];

        $sca->save();
        
        return $sca;
    }
    
    public static function is_duplicate($activity,$return_dupe=true)
    {
        SugarChimp_Helper::log('debug','SugarChimp_Activity::is_duplicate start');

        if (empty($activity) || !is_array($activity))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Activity::is_duplicate activity is empty or not an array');
            return false;
        }
        
        $sql = SugarChimp_Activity::get_duplicate_query($activity,"SELECT id AS activity_id");
        
        if (empty($sql))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Activity::is_duplicate could not generate sql for activity duplicate query');
            return false;
        }

        SugarChimp_Helper::log('debug','SugarChimp_Activity::is_duplicate sql for activity duplicate query: '.$sql);

        global $db;
        
        if (!$result = $db->query($sql))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Activity::is_duplicate duplicate check sql query failed: '.$sql);
            return false;
        }
        
        $row = $db->fetchByAssoc($result);
        
        if (!empty($row) && !empty($row['activity_id']))
        {
            // we found a duplicate, mark as already in the system
            SugarChimp_Helper::log('debug','SugarChimp_Activity::is_duplicate we found a duplicate activity, carry on: '.print_r($activity,true));
            SugarChimp_Helper::log('debug','SugarChimp_Activity::is_duplicate duplicate activity id: '.print_r($row,true));
            
            // return the duplicate bean if flag set
            if ($return_dupe === true)
            {
                return BeanFactory::getBean('SugarChimpActivity',$row['activity_id']);
            }
            
            // otherwise return true
            return true;
        }

        // we didn't find nothin', so return false
        return false;
    }
    
    public static function get_duplicate_query($activity,$select=" SELECT count(id) ", $order_by=" ORDER BY date_entered DESC ")
    {
        SugarChimp_Helper::log('debug','SugarChimp_Activity::get_duplicate_query start');
        
        if (empty($activity['name']))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Activity::get_duplicate_query name is required but empty');
            return false;
        }
        
        if (empty($activity['activity']))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Activity::get_duplicate_query activity is required but empty');
            return false;
        }
        
        if (empty($activity['mailchimp_list_id']))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Activity::get_duplicate_query mailchimp_list_id is required but empty');
            return false;
        }
        
        if (empty($activity['mailchimp_campaign_id']))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Activity::get_duplicate_query mailchimp_campaign_id is required but empty');
            return false;
        }
        
        global $db;
        
        $sql = '';
        
        if ($activity['activity'] == 'send')
        {
            $sql = $db->quote($select)." FROM sugarchimpactivity 
                    WHERE 
                        name LIKE '".$db->quote($activity['name'])."' AND 
                        activity = 'send' AND
                        mailchimp_list_id = '".$db->quote($activity['mailchimp_list_id'])."' AND
                        mailchimp_campaign_id = '".$db->quote($activity['mailchimp_campaign_id'])."'
                    ".$db->quote($order_by);
        }
        else if ($activity['activity'] == 'click')
        {
            if (empty($activity['url']))
            {
                SugarChimp_Helper::log('fatal','SugarChimp_Activity::get_duplicate_query url is required but empty for click activity');
                return false;
            }
            
            if (empty($activity['timestamp']))
            {
                SugarChimp_Helper::log('fatal','SugarChimp_Activity::get_duplicate_query timestamp is required but empty for click activity');
                return false;
            }
            
            $sql = $db->quote($select)." FROM sugarchimpactivity 
                    WHERE 
                        name LIKE '".$db->quote($activity['name'])."' AND 
                        activity = 'click' AND
                        mailchimp_list_id = '".$db->quote($activity['mailchimp_list_id'])."' AND
                        mailchimp_campaign_id = '".$db->quote($activity['mailchimp_campaign_id'])."' AND
                        url = '".$db->quote($activity['url'])."' AND
                        timestamp = '".$db->quote($activity['timestamp'])."'
                    ".$db->quote($order_by);
        }
        else
        {
            if (empty($activity['timestamp']))
            {
                SugarChimp_Helper::log('fatal','SugarChimp_Activity::get_duplicate_query timestamp is required but empty for all activity checks');
                return false;
            }
            
            $sql = $db->quote($select)." FROM sugarchimpactivity 
                    WHERE 
                        name LIKE '".$db->quote($activity['name'])."' AND 
                        activity = '".$db->quote($activity['activity'])."' AND
                        mailchimp_list_id = '".$db->quote($activity['mailchimp_list_id'])."' AND
                        mailchimp_campaign_id = '".$db->quote($activity['mailchimp_campaign_id'])."' AND
                        timestamp = '".$db->quote($activity['timestamp'])."'
                    ".$db->quote($order_by);
        }
        
        return $sql;
    }
    
    // given an array of activity beans and array of beans
    // relate all the beans to activity
    public static function add_activities_to_beans($activities,$beans)
    {
        if (empty($activities) || !is_array($activities))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Activity::add_beans_to_activity a valid activity bean is required');
            return false;
        }
        
        if (empty($beans) || !is_array($beans))
        {
            SugarChimp_Helper::log('debug','SugarChimp_Activity::add_beans_to_activity an array of beans is required');
            return false;
        }

        SugarChimp_Helper::log('debug',"SugarChimp_Activity::add_beans_to_activity start");
        
        foreach ($beans as $bean)
        {
            if (!$bean instanceOf SugarBean)
            {
                SugarChimp_Helper::log('warning',"SugarChimp_Activity::add_beans_to_activity provided bean is not a SugarBean, skipping");
                continue;
            }
            
            SugarChimp_Helper::log('debug',"SugarChimp_Activity::add_beans_to_activity bean {$bean->module_dir} {$bean->id}");

            $bean->load_relationship('sugarchimpactivity');
            if (!isset($bean->sugarchimpactivity))
            {
                SugarChimp_Helper::log('fatal',"SugarChimp_Activity::add_beans_to_activity Cannot add {$bean->module_dir} {$bean->id}");
                continue;
            }

            $activity_ids = $bean->sugarchimpactivity->get();
            SugarChimp_Helper::log('debug',"SugarChimp_Activity::add_beans_to_activity activity ids already related to the bean: ".print_r($activity_ids,true));
            
            // go through each activity and add them
            foreach ($activities as $activity)
            {
                if (!$activity instanceOf SugarBean)
                {
                    SugarChimp_Helper::log('warning',"SugarChimp_Activity::add_beans_to_activity provided activity is not a SugarBean, skipping");
                    continue;
                }
                
                if (in_array($activity->id,$activity_ids))
                {
                    // the record is already related, so continue
                    SugarChimp_Helper::log('debug',"SugarChimp_Activity::add_beans_to_activity bean already belongs to list");
                    continue;
                }
            
                $bean->sugarchimpactivity->add($activity->id);
            }
        }

        return true;
    }
    
    // check to see if a import activity job is in the queue
    // if so, return true, else return false
    public static function is_activity_import_job_queued()
    {
        global $db;
        
        $result = $db->query("SELECT COUNT(id) AS pending_count FROM job_queue WHERE target = 'class::MailChimpToSugarCRMActivity' AND status = 'queued'");
        $row = $db->fetchByAssoc($result);
        if (!empty($row)) 
        {
             if( $row['pending_count'] > 0) 
             {
                 SugarChimp_Helper::log('debug','SugarChimp_Activity::is_activity_import_job_queued: there is a job pending');
                 return true;
             }
             else
             {
                 SugarChimp_Helper::log('debug','SugarChimp_Activity::is_activity_import_job_queued: there is no job pending');
                 return false;
             }
        }
        
        // no jobs queued
        SugarChimp_Helper::log('warning','SugarChimp_Activity::is_activity_import_job_queued: query failed gracefully, but we should not get here');
        return false;
    }
    
    // 
    public static function track_campaign_activity($mailchimp_list_id,$mailchimp_campaign_id,$send_time=false,$last_sync=false,$include_empty=true,$if_exists_update=true)
    {
        global $db;
        
        if (empty($mailchimp_list_id))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Activity::track_campaign_activity: mailchimp_list_id is empty but it is required');
            return false;
        }
        
        if (empty($mailchimp_campaign_id))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Activity::track_campaign_activity: mailchimp_campaign_id is empty but it is required');
            return false;
        }
        
        if (empty($send_time))
        {
            // if send time is empty, default to now
            $send_time = $db->now();
        }
        else
        {
            // db->now above adds the quotes, so to be consistent add them here
            $send_time = "'".$send_time."'";
        }
        
        if (empty($last_sync))
        {
            // default last_sync to empty
            $initial_sync = 'NULL';
        }
        else
        {
            $initial_sync = "'".$db->quote($last_sync)."'";
        }
        
        // include_empty is checkbox field, so 1 or 0 in db
        if ($include_empty === true)
        {
            $include_empty = 1;
        }
        else
        {
            $include_empty = 0;
        }
        
        $sql = "SELECT id FROM sugarchimp_activity_tracker WHERE mailchimp_list_id='".$db->quote($mailchimp_list_id)."' AND mailchimp_campaign_id='".$db->quote($mailchimp_campaign_id)."'";
        $result = $db->query($sql);
        
        if (!$result = $db->query($sql))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Activity::track_campaign_activity: lookkup query failed: '.$sql);
            return false;
        }
        
        $row = $db->fetchByAssoc($result);
        
        if (!empty($row['id']))
        {
            if ($if_exists_update === false)
            {
                SugarChimp_Helper::log('debug','SugarChimp_Activity::track_campaign_activity: match found, if_exists_update is false, not running sql: '.$sql);
                // we don't want to run the query if this flag is false
                return true;
            }
            
            // we found a match, update it
            SugarChimp_Helper::log('debug','SugarChimp_Activity::track_campaign_activity: match found, running update query: '.$row['id']);
            $sql = "UPDATE sugarchimp_activity_tracker
                    SET start_date=".$send_time.", last_sync='".$db->quote($last_sync)."', initial_sync=".$initial_sync."
                    WHERE id='".$row['id']."'";
        }
        else
        {
            // no match found, insert it
            SugarChimp_Helper::log('debug','SugarChimp_Activity::track_campaign_activity: no match found, inserting new record');
            $sql = "INSERT INTO sugarchimp_activity_tracker VALUES (
                        ".$db->getGuidSQL().", 
                        '".$db->quote($mailchimp_list_id)."', 
                        '".$db->quote($mailchimp_campaign_id)."', 
                        ".$db->quote($include_empty).",
                        ".$send_time.", 
                        ".$db->quote($initial_sync)."
                    )";
        }
        
        SugarChimp_Helper::log('debug','SugarChimp_Activity::track_campaign_activity: sql for insert/update: '.$sql);
        
        if (!$result = $db->query($sql))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Activity::track_campaign_activity: insert/update query failed: '.$sql);
            return false;
        }
        
        return true;
    }
    
    // set a campaign in the sugarchimp activity tracker table that it has been synced
    public static function mark_campaign_synced($mailchimp_list_id,$mailchimp_campaign_id,$time_synced=false)
    {
        global $db;
        
        if (empty($mailchimp_list_id))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Activity::mark_campaign_synced: mailchimp_list_id is empty but it is required');
            return false;
        }
        
        if (empty($mailchimp_campaign_id))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Activity::mark_campaign_synced: mailchimp_campaign_id is empty but it is required');
            return false;
        }
        
        if (empty($time_synced))
        {
            // if send time is empty, default to now
            $time_synced = $db->now();
        }
        else
        {
            $time_synced = "'".$db->quote($time_synced)."'";
        }
        
        $sql = "UPDATE sugarchimp_activity_tracker
                SET last_sync = ".$time_synced.", initial_sync = 0
                WHERE 
                    mailchimp_list_id='".$db->quote($mailchimp_list_id)."' AND
                    mailchimp_campaign_id='".$db->quote($mailchimp_campaign_id)."'";
        
        SugarChimp_Helper::log('debug','SugarChimp_Activity::mark_campaign_synced: sql for update: '.$sql);

        if (!$result = $db->query($sql))
        {
            SugarChimp_Helper::log('fatal','SugarChimp_Activity::mark_campaign_synced: update query failed: '.$sql);
            return false;
        }
        
        return true;
    }
}