<?php







require_once('modules/SugarChimp/includes/classes/SugarChimp/Logger.php');
require_once('modules/SugarChimp/includes/classes/Mailchimp/API.php');

class SugarChimp_DataPrivacy
{
	public static function remove_by_emails($emails)
	{
		if (empty($emails) or !is_array($emails))
		{
			SugarChimp_Logger::log('fatal','SugarChimp_DataPrivacy::remove_by_emails: emails is empty');
			return false;
		}

		SugarChimp_Logger::log('debug','SugarChimp_DataPrivacy::remove_by_emails: start removal for emails: '.print_r($emails,true));

		$email_results = array();

		foreach ($emails as $email)
		{
			$result = self::remove_by_email($email);

			$email_results[$email] = $result;
		}

		return $email_results;
	}

	// sugarchimpactivity - update sugarchimpactivity set name='erased' where where name like '$email'
	// sugarchimp_lock - delete from sugarchimp_lock where email like '$email'
	// sugarchimp_optout_tracker - delete from sugarchimp_optout_tracker where email like '$email'
	// sugarchimp_mc_cleaned_email - delete from sugarchimp_mc_cleaned_email where email LIKE '$email'
	// sugarchimp_mc_people - delete from sugarchimp_mc_people where data like '$email'
	// sugarchimp - delete from sugarchimp where param1=module and param2=record
	// sugarchimp_sugar_backup - delete from sugarchimp_sugar_backup where module=module and record=record
	// go to mailchimp and ask for lists the email address is on, then delete from MC
	// add to email to sugarchimp_data_privacy table

	// sugarchimpactivity_audit - we do not audit any PII fields
	// sugarchimp_mailchimp_backup - it is possible data is here encrypted in data field, this will be cleaned out every several days however
	// sugarchimp_mc_activity - it is possible data is here encrypted in data field
	// sugarchimp_mc - it is possible data is here encrypted in data field

    public static function remove_by_email($email)
    {
    	$messages = array();

    	if (empty($email))
		{
			$messages []= "Email address is empty. Cannot remove data. ";
			SugarChimp_Logger::log('fatal','SugarChimp_DataPrivacy::remove_by_email: Email address is empty. Cannot remove data. '.print_r($message,true));
			return self::removal_result(false,$messages);
		}

		$messages []= "Starting removal process for email ".$email.": <br>";
		SugarChimp_Logger::log('debug','SugarChimp_DataPrivacy::remove_by_email: '.print_r($message,true));
		
		global $db;

		// remove email address from SC activity records with email
		$sql = "UPDATE sugarchimpactivity SET name='erased' WHERE name LIKE '".$db->quote($email)."'";
		SugarChimp_Logger::log('debug','SugarChimp_DataPrivacy::remove_by_email: sql: '.print_r($sql,true));

		if (!$result = $db->query($sql))
		{
			SugarChimp_Logger::log('fatal','SugarChimp_DataPrivacy::remove_by_email: query to remove data from sugarchimpactivity table failed for email: '.print_r($email,true).' with query: '.print_r($sql,true));
			$messages []= "Failed to remove data from sugarchimpactivity table. ";
			return self::removal_result(false,$messages);
		}
		else
		{
			// $messages []= "Data successfully removed from sugarchimpactivity table. ";
		}

		$activities_erased = $db->getAffectedRowCount($result);

		// sugarchimp_lock - delete from sugarchimp_lock where email like '$email'
		$sql = "DELETE FROM sugarchimp_lock WHERE email LIKE '".$db->quote($email)."'";
		SugarChimp_Logger::log('debug','SugarChimp_DataPrivacy::remove_by_email: sql: '.print_r($sql,true));

		if (!$result = $db->query($sql))
		{
			SugarChimp_Logger::log('fatal','SugarChimp_DataPrivacy::remove_by_email: query to remove emails from sugarchimp_lock table failed for email: '.print_r($email,true).' with query: '.print_r($sql,true));
			$messages []= "Failed to remove data from sugarchimp_lock table. ";
			return self::removal_result(false,$messages);
		}
		else
		{
			// $messages []= "Data successfully removed from sugarchimp_lock table. ";
		}

		// sugarchimp_optout_tracker - delete from sugarchimp_optout_tracker where email like '$email'
		$sql = "DELETE FROM sugarchimp_optout_tracker WHERE email LIKE '".$db->quote($email)."'";
		SugarChimp_Logger::log('debug','SugarChimp_DataPrivacy::remove_by_email: sql: '.print_r($sql,true));

		if (!$result = $db->query($sql))
		{
			SugarChimp_Logger::log('fatal','SugarChimp_DataPrivacy::remove_by_email: query to remove emails from sugarchimp_optout_tracker table failed for email: '.print_r($email,true).' with query: '.print_r($sql,true));
			$messages []= "Failed to remove data from sugarchimp_optout_tracker table. ";
			return self::removal_result(false,$messages);
		}
		else
		{
			// $messages []= "Data successfully removed from sugarchimp_optout_tracker table. ";
		}

		// sugarchimp_mc_cleaned_email - delete from sugarchimp_mc_cleaned_email where email LIKE '$email'
		$sql = "DELETE FROM sugarchimp_mc_cleaned_email WHERE email LIKE '".$db->quote($email)."'";
		SugarChimp_Logger::log('debug','SugarChimp_DataPrivacy::remove_by_email: sql: '.print_r($sql,true));

		if (!$result = $db->query($sql))
		{
			SugarChimp_Logger::log('fatal','SugarChimp_DataPrivacy::remove_by_email: query to remove emails from sugarchimp_mc_cleaned_email table failed for email: '.print_r($email,true).' with query: '.print_r($sql,true));
			$messages []= "Failed to remove data from sugarchimp_mc_cleaned_email table. ";
			return self::removal_result(false,$messages);
		}
		else
		{
			// $messages []= "Data successfully removed from sugarchimp_mc_cleaned_email table. ";
		}

		// sugarchimp_mc_people - delete from sugarchimp_mc_people where data like '$email'
		$sql = "DELETE FROM sugarchimp_mc_people WHERE data LIKE '".$db->quote($email)."'";
		SugarChimp_Logger::log('debug','SugarChimp_DataPrivacy::remove_by_email: sql: '.print_r($sql,true));

		if (!$result = $db->query($sql))
		{
			SugarChimp_Logger::log('fatal','SugarChimp_DataPrivacy::remove_by_email: query to remove emails from sugarchimp_mc_people table failed for email: '.print_r($email,true).' with query: '.print_r($sql,true));
			$messages []= "Failed to remove data from sugarchimp_mc_people table. ";
			return self::removal_result(false,$messages);
		}
		else
		{
			// $messages []= "Data successfully removed from sugarchimp_mc_people table. ";
		}

		// sugarchimp and sugarchimp_sugar_backup need to be removed based on module name and records that have the email address
		// make a query against the email_address_rel_bean table with the sugarchimp table joining on module and record - if any matches, remove those from sugarchimp
		// sugarchimp - delete from sugarchimp where param1=module and param2=record
		// sugarchimp_sugar_backup - delete from sugarchimp_sugar_backup where module=module and record=record
		$sql = "SELECT eabr.bean_module as module, eabr.bean_id as record 
				FROM email_addr_bean_rel eabr
				INNER JOIN email_addresses ea ON eabr.email_address_id=ea.id
				WHERE ea.email_address LIKE '".$db->quote($email)."'";
		SugarChimp_Logger::log('debug','SugarChimp_DataPrivacy::remove_by_email: sql: '.print_r($sql,true));

		if (!$result = $db->query($sql))
		{
			SugarChimp_Logger::log('fatal','SugarChimp_DataPrivacy::remove_by_email: query to get modules/records form email table failed for email: '.print_r($email,true).' with query: '.print_r($sql,true));
			$messages []= "Unable to find modules and records from email table, cannot continue. ";
			return self::removal_result(false,$messages);
		}

		$records = array();
		
		while ($row = $db->fetchByAssoc($result))
		{
			SugarChimp_Logger::log('debug','SugarChimp_DataPrivacy::remove_by_email: row: '.print_r($row,true));
			if (!empty($row['module']) and !empty($row['record']))
			{
				if (!isset($records[$row['module']])) $records[$row['module']] = array();
				$records[$row['module']] []= $row['record'];
			}
		}
		SugarChimp_Logger::log('debug','SugarChimp_DataPrivacy::remove_by_email: records to process: '.print_r($records,true));

		// foreach module/record pair we found, delete from the sugarchimp and sugarchimp_sugar_backup tables
		foreach ($records as $module => $ids)
		{
			if (!empty($module) and !empty($ids) and is_array($ids))
			{
				foreach ($ids as $id)
				{
					if (!empty($id))
					{
						// sugarchimp - delete from sugarchimp where param1=module and param2=record
						$sql = "DELETE FROM sugarchimp WHERE param1='".$db->quote($module)."' and param2='".$db->quote($id)."'";
						SugarChimp_Logger::log('debug','SugarChimp_DataPrivacy::remove_by_email: sql: '.print_r($sql,true));

						if (!$result = $db->query($sql))
						{
							SugarChimp_Logger::log('fatal','SugarChimp_DataPrivacy::remove_by_email: query to remove emails from sugarchimp table failed for email: '.print_r($email,true).' with query: '.print_r($sql,true));
							$messages []= "Failed to remove data from sugarchimp table. ";
							return self::removal_result(false,$messages);
						}			
						else
						{
							// $messages []= "Data successfully removed from sugarchimp table. ";
						}

						// sugarchimp_sugar_backup - delete from sugarchimp_sugar_backup where module=module and record=record
						$sql = "DELETE FROM sugarchimp_sugar_backup WHERE module='".$db->quote($module)."' and record='".$db->quote($id)."'";
						SugarChimp_Logger::log('debug','SugarChimp_DataPrivacy::remove_by_email: sql: '.print_r($sql,true));

						if (!$result = $db->query($sql))
						{
							SugarChimp_Logger::log('fatal','SugarChimp_DataPrivacy::remove_by_email: query to remove emails from sugarchimp_sugar_backup table failed for email: '.print_r($email,true).' with query: '.print_r($sql,true));
							$messages []= "Failed to remove data from sugarchimp_sugar_backup table. ";
							return self::removal_result(false,$messages);
						}			
						else
						{
							// $messages []= "Data successfully removed from sugarchimp_sugar_backup table. ";
						}
					}
				}
			}
		}

		$messages []= "Email address removed from all SugarChimp tables. ";

		if (empty($activities_erased))
		{
			$messages []= "SugarChimp Activity records were set to 'erased'. ";
		}
		else
		{
			$messages []= $activities_erased . " SugarChimp Activity records were set to 'erased'. ";
		}

		// delete subscribers with email in MailChimp
		// make call to get all list ids for the email address
		// run a batch call of deletes for each list
		$lists = MailChimp_API::get_lists(array('fields'=>'lists.id','count'=>1000,'email'=>$email));

		if (empty($lists['lists']) or !is_array($lists['lists']))
        {
            SugarChimp_Helper::log('debug','SugarChimp_DataPrivacy::remove_by_email: email is not on any lists: '.print_r($email,true));
			$messages []= "The email does not exist on any lists in MailChimp. No actions will happen on the MailChimp side. ";
        }
        else
        {
        	// we have lists, send a batch call to delete subscribers with that email from each list
        	$delete_data = array(
        		$email=>array()
        	);

        	$list_count = 0;
        	foreach ($lists['lists'] as $list)
        	{
        		if (empty($list['id'])) continue;
        		$delete_data[$email] []= $list['id'];
        		$list_count++;
        	}

        	SugarChimp_Helper::log('debug','SugarChimp_DataPrivacy::remove_by_email: list ids to delete email from: '.print_r($delete_data,true));

        	if ($list_count>0)
        	{
	        	try
	        	{
	        		$response = MailChimp_API::batch_delete_subscribers($delete_data);
	        	}
	        	catch (Exception $e)
		        {
		            SugarChimp_Helper::log('fatal',"SugarChimp_DataPrivacy::remove_by_email: batch delete call failed with message: ".$e->getMessage()." for data: ".print_r($delete_data,true));
					$messages []= "Failed to remove email addresses from MailChimp. You will need to go to MailChimp and manually remove this email address from MailChimp. ";
					return self::removal_result(false,$messages);
		        }

				$messages []= "Email address successfully removed from ".$list_count." MailChimp lists. ";
			}
			else
			{
				$messages []= "The email does not exist on any lists in MailChimp. No actions will happen on the MailChimp side. ";
			}
        }

        $messages []= "<br>Email address successfully removed! ";
		return self::removal_result(true,$messages);
    }

    public static function removal_result($success,$messages)
    {
    	if (empty($success))
    	{
    		$success = false;
    	}

    	if (empty($messages) or !is_array($messages))
    	{
    		$messages = array("Unknown result.");
    	}

    	return array(
    		"success" => $success,
    		"messages" => $messages,
    	);
    }
}