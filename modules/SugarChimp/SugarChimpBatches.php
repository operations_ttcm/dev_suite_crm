<?php







class SugarChimpBatches extends Basic 
{
    var $new_schema = true;
    var $module_dir = 'SugarChimp';
    var $object_name = 'SugarChimpBatches';
    var $table_name = 'sugarchimp_batches';

    var $id;
    var $batch_id;
    var $mailchimp_list_id;
    
    function bean_implements($interface){
        switch($interface){
            case 'ACL': return true;
        }
        return false;
    }
}
