<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






global $mod_strings;

$module_menu[] = Array("index.php?module=SugarChimp&action=health_status", $mod_strings['HEALTH_STATUS_LNK'],"SugarChimp");
$module_menu[] = Array("index.php?module=SugarChimp&action=setup", $mod_strings['SETUP_LNK'],"SugarChimp");
$module_menu[] = Array("index.php?module=SugarChimp&action=field_mapping", $mod_strings['FIELD_MAPPING_LNK'],"SugarChimp");
$module_menu[] = Array("index.php?module=SugarChimp&action=campaigns", $mod_strings['CAMPAIGNS_REPORT_LNK'],"SugarChimp");
$module_menu[] = Array("index.php?module=SugarChimp&action=data_privacy", $mod_strings['DATA_PRIVACY_LNK'],"SugarChimp");
