<?php







class SugarChimpMCList extends Basic 
{
    var $new_schema = true;
    var $module_dir = 'SugarChimp';
    var $object_name = 'SugarChimpMCList';
    var $table_name = 'sugarchimp_mc_list';

    var $id;
    var $name;
    var $mailchimp_list_id;
    var $optout_tracker_processed;
    var $optout_tracker_offset;
    var $date_entered;
    var $deleted_from_mailchimp;
    
    function bean_implements($interface){
        switch($interface){
            case 'ACL': return true;
        }
        return false;
    }
}