<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






require_once('include/MVC/Controller/SugarController.php');
require_once('modules/SugarChimp/clients/base/api/SugarChimpApi.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');

class SugarChimpController extends SugarController
{
    function action_checkForSuiteCRM($errors=array())
    {
        $is_suitecrm = SugarChimp_Helper::is_suitecrm();

        echo json_encode(array(
            'is_suitecrm' => $is_suitecrm,
        )); exit;
    }
    
    function action_setup($errors=array())
    {
        $this->view_object_map['errors'] = $errors;
        $this->view = '_ce_only_step1';
    }
    
    function action_step1($errors=array())
    {
        $this->view_object_map['errors'] = $errors;
        $this->view = '_ce_only_step1';
    }
    
    function action_prechecks_scheduler()
    {
        $data = array();
        $scapi = new SugarChimpApi();
        $response = $scapi->preChecksScheduler(null,$data);
        
        echo json_encode($response); exit;
    }

    function action_prechecks_network()
    {
        $data = array();
        $scapi = new SugarChimpApi();
        $response = $scapi->preChecksNetwork(null,$data);
        
        echo json_encode($response); exit;
    }

    function action_step2()
    {
        $this->view = '_ce_only_step2';
    }

    function action_get_license()
    {
        $data = array();
        $scapi = new SugarChimpApi();
        $response = $scapi->getLicenseValidation(null,$data);
        echo json_encode($response); exit;
    }

    function action_get_switchable_plans()
    {
        $data = array();
        $scapi = new SugarChimpApi();
        $response = $scapi->getSwitchablePlans(null,$data);
        echo json_encode($response); exit;
    }

    function action_switch_plan()
    {
        SugarChimp_Helper::log('debug','switch_plan called with: ' . $_REQUEST['plan_id']);
        if (empty($_REQUEST['plan_id']))
        {
            return array(
                'success' => false,
                'message' => 'You must provide a plan_id to upgrade to.'
            );
        }

        SugarChimp_Helper::log('debug','plan_id: ' . $_REQUEST['plan_id']);

        $scapi = new SugarChimpApi();
        $response = $scapi->switchPlan(null,array(
            'plan_id' => $_REQUEST['plan_id']
        ));
        echo json_encode($response); exit;
    }

    function action_cancel_upgrade()
    {
        $data = array();
        $scapi = new SugarChimpApi();
        $response = $scapi->cancelUpgrade(null,$data);
        echo json_encode($response); exit;
    }

    function action_validate_license()
    {
        if (empty($_REQUEST['licensekey']))
        {
            return array(
                'success' => false,
                'message' => 'You must provide a valid SugarChimp License.'
            );
        }
        SugarChimp_Helper::log('debug','key: ' . $_REQUEST['licensekey']);

        $scapi = new SugarChimpApi();
        $response = $scapi->validateLicense(null,array(
            'key' => $_REQUEST['licensekey']
        ));
        echo json_encode($response); exit;
    }

    function action_step3()
    {
        $this->view = '_ce_only_step3';
    }

    function action_get_apikey()
    {
        $data = array();
        $scapi = new SugarChimpApi();
        $response = $scapi->getApiKey(null,$data);
        echo json_encode($response); exit;
    }
    function action_update_apikey()
    {
        if (empty($_REQUEST['apikey']))
        {
            // you must enter an apikey
            return array(
                'success' => false,
                'message' => 'No API Key entered'
                );
        }

        $data = array(
            'apikey'=> $_REQUEST['apikey']
        );
        $scapi = new SugarChimpApi();
        $response = $scapi->updateApiKey(null,$data);
        echo json_encode($response); exit;
    }

    function action_step4()
    {
        $this->view = '_ce_only_step4';
    }
    
    function action_smartlist_setup()
    {
        $data = array();
        $scapi = new SugarChimpApi();
        $response = $scapi->smartlistSetup(null,$data);
        echo json_encode($response); exit;
    }

    function action_smartlist_dashlet()
    {
        if (empty($_REQUEST['mailchimp_list_id']))
        {
            // you must enter an apikey
            return array(
                'success' => false,
                'message' => 'A MailChipm List ID is required.'
            );
        }

        $data = array(
            'mailchimp_list_id'=> $_REQUEST['mailchimp_list_id']
        );

        $scapi = new SugarChimpApi();
        $response = $scapi->smartlistDashlet(null,$data);
        echo json_encode($response); exit;
    }

    function action_create_sugar_list()
    {
        if (empty($_REQUEST['listName']))
        {
            // you must enter an listname
            return false;
        }

        $data = array(
            'name'=> $_REQUEST['listName']
        );

        $scapi = new SugarChimpApi();
        $response = $scapi->createSugarList(null,$data);
        echo json_encode($response); exit;
    }
    function action_create_mailchimp_list()
    {
        if (empty($_REQUEST['listName']))
        {
            // you must enter an listname
            return false;
        }

        $data = array(
            'name'=> $_REQUEST['listName']
        );

        $scapi = new SugarChimpApi();
        $response = $scapi->createMailChimpList(null,$data);
        echo json_encode($response); exit;
    }
    function action_test_filters_query()
    {
        if (empty($_REQUEST['formData']) || empty($_REQUEST['module']))
        {
            // you must enter an listname
            return false;
        }

        $data = array(
            'formData' => $_REQUEST['formData'],
            'module' => $_REQUEST['module_type']
        );

        $scapi = new SugarChimpApi();
        $response = $scapi->testFiltersQuery(null,$data);
        echo json_encode($response); exit;
    }
    function action_test_sql_query()
    {
        if (empty($_REQUEST['sql']))
        {
            // you must enter an listname
            return false;
        }

        if (empty($_REQUEST['sql_module']))
        {
            // you must enter an listname
            return false;
        }

        $data = array(
            'sql' => $_REQUEST['sql'],
            'sql_module' => $_REQUEST['sql_module'],
        );

        $scapi = new SugarChimpApi();
        $response = $scapi->testSQLQuery(null,$data);
        echo json_encode($response); exit;
    }
    function action_smartlist_save()
    {
        if (empty($_REQUEST['formData']))
        {

            // you must enter an apikey
            return false;
        }

        $data = array(
            'formData'=> $_REQUEST['formData']
        );

        $scapi = new SugarChimpApi();
        $response = $scapi->smartlistSave(null,$data);
        SugarChimp_Helper::log('debug',"smartlist save:: server response: " . $response);
        echo json_encode($response); exit;
    }
    function action_health_status()
    {
        $this->view = '_ce_only_health_status';
    }
    function action_get_health_data()
    {
        $scapi = new SugarChimpApi();
        $response = $scapi->getHealth(null,array());
        echo json_encode($response); exit;
    }
    function action_optin_emails()
    {
        $scapi = new SugarChimpApi();
        $response = $scapi->optInEmails(null,array());
        echo json_encode($response); exit;
    }
    function action_helper()
    {
        if (empty($_REQUEST['file']))
        {
            // you must enter an apikey
            return $this->action_step3(array('No helper path found'));
        }
    }
    function action_save_config_logger()
    {
        $scapi = new SugarChimpApi();
        
        if (empty($_REQUEST['sugarchimp_logger']))
        {
            echo json_encode(array(
                'success' => false,
                'message' => 'The logger level was empty.'
            )); exit;
        }
        
        $data = array();
        $data['sugarchimp_logger'] = $_REQUEST['sugarchimp_logger'];
        
        $response = $scapi->saveConfigLogger(null,$data);
        
        echo json_encode($response); exit;
    }

    function action_queue_sync()
    {
        $scapi = new SugarChimpApi();
        
        if (empty($_REQUEST['list_id']))
        {
            echo json_encode(array(
                'success' => false,
                'message' => 'The list id was empty.'
            )); exit;
        }
        
        if (empty($_REQUEST['syncaction']))
        {
            echo json_encode(array(
                'success' => false,
                'message' => 'The action was empty.'
            )); exit;
        }
        
        $data = array();
        $data['target_list_id'] = $_REQUEST['list_id'];
        $data['sync_action'] = $_REQUEST['syncaction'];
        
        $response = $scapi->queueSync(null,$data);
        
        echo json_encode($response); exit;
    }

    function action_DetailView()
    {
        return $this->action_health_status();
    }
    
    function action_get_email_address()
    {
        if (empty($_REQUEST['module_name']) || empty($_REQUEST['module_id']))
        {
            echo json_encode(array('success'=>false,'message'=>'Could not find the record.')); exit;
        }
        
        $bean = BeanFactory::getBean($_REQUEST['module_name'],$_REQUEST['module_id']);
        
        if (empty($bean->email1))
        {
            echo json_encode(array('success'=>false,'message'=>'There is no email address for this record.')); exit;
        }
        
        echo json_encode(array('success'=>true,'email'=>$bean->email1)); exit;
    }
    
    function action_get_dashlet_data()
    {
        if (empty($_REQUEST['module_email']) || empty($_REQUEST['module_id']))
        {
            echo json_encode(array('success'=>false,'message'=>'Could not find the record.')); exit;
        }
        
        $scapi = new SugarChimpApi();
        $data = $scapi->dashletPersonRecordData(null,array(
            'record_id' => $_REQUEST['module_id'],
            'email' => $_REQUEST['module_email'],
        ));
        
        if (empty($data['activities']))
        {
            echo json_encode(array('success'=>false,'message'=>'There are no activity records for this person in MailChimp.')); exit;
        }
        
        $this->view_object_map['dashlet_data'] = $data;
        $this->view = '_ce_only_dashlet_data';
    }

    function action_get_accounts_dashlet_data()
    {
        if (empty($_REQUEST['module_id']))
        {
            echo json_encode(array('success'=>false,'message'=>'Could not find the record.')); exit;
        }
        //SugarChimp_Helper::log('debug','accountsdashlet account_id: ' .print_r($_REQUEST['module_id'],true));

        $scapi = new SugarChimpApi();
        $data = $scapi->dashletAccountRecordData(null,array(
            'id' => $_REQUEST['module_id'],
        ));
        //SugarChimp_Helper::log('debug','accountsdashlet account_id: ' .print_r($data,true));

        if (empty($data['activities']))
        {
            echo json_encode(array('success'=>false,'message'=>'There are no activity records for this Account in MailChimp.')); exit;
        }
        
        $this->view_object_map['accounts_dashlet_data'] = $data;
        $this->view = '_ce_only_accounts_dashlet_data';
    }
    
    function action_field_mapping($list_id=false, $errors = array())
    {
        // get lists
        $scapi = new SugarChimpApi();
        $results = $scapi->getSyncedMailChimpLists(null,array());
        
        $this->view_object_map['errors'] = $errors;
        $this->view_object_map['lists'] = $results['data']['lists'];
        //$this->view_object_map['lists'] = $results;
        $this->view_object_map['list_id'] = $list_id;
        
        $this->view = '_ce_only_field_mapping';
    }
    
    function action_field_mapping_details()
    {
        if (empty($_REQUEST['list_id']))
        {
            echo json_encode(array('success'=>false,'message'=>'A list id is required.')); exit;
        }
        
        $list_id = $_REQUEST['list_id'];
        
        $scapi = new SugarChimpApi();
        $mappings = $scapi->getMappingOptions(null,array('list_id'=>$list_id));
        
        $this->view_object_map['list_id'] = $list_id;
        $this->view_object_map['mappings'] = $mappings;
        
        $this->view = '_ce_only_field_mapping_details';
    }

    function action_save_field_mapping()
    {
        if (empty($_REQUEST['list_id']))
        {
            echo json_encode(array('success'=>false,'message'=>'A list id is required.')); exit;
        }
        
        $data = array();
        
        $data['list_id'] = $list_id = $_REQUEST['list_id'];
        $data['sugar_target_list_id'] = $sugar_target_list_id = $_REQUEST['sugar_target_list_id'];
        $data['sync_direction'] = $sync_direction = empty($_REQUEST['sync_direction']) ? false : $_REQUEST['sync_direction'];

        foreach ($_REQUEST as $key => $val)
        {
            if (strpos($key,'mapping|') === 0)
            {
                $data[$key] = $val;
            }
        }
        
        $scapi = new SugarChimpApi();
        $response = $scapi->saveMappingOptions(null,$data);
        
        $errors = array();
        if (!empty($response['results']) && is_array($response['results']))
        {
            foreach ($response['results'] as $module => $result)
            {
                SugarChimp_Helper::log('debug','save result: '.print_r($result,true));
                if ($result != true)
                {
                    $errors []= "Mapping for {$module} failed to save. Refresh the page and try again.";
                }
            }
        }
        
        return $this->action_field_mapping($list_id,$errors);
    }
    
    function action_admin()
    {
        $this->view = '_ce_only_admin';
    }

    function action_adminmc()
    {
        $this->view = '_ce_only_adminmc';
    }
    
    function action_adminactivitytracker()
    {
        $this->view = '_ce_only_adminactivitytracker';
    }
    
    function action_adminactivities()
    {
        $this->view = '_ce_only_adminactivities';
    }
    
    function action_adminbatches()
    {
        $this->view = '_ce_only_adminbatches';
    }

    function action_get_batch_data()
    {
        if (empty($_REQUEST['batch_id']))
        {
            echo json_encode(array('success'=>false,'message'=>'No Batch ID Provided.')); exit;
        }
        
        $data = array();
        $data['batch_id'] = $_REQUEST['batch_id'];

        $scapi = new SugarChimpApi();
        $response = $scapi->getBatchData(null,$data);
        
        echo json_encode($response); exit;
    }

    function action_data_privacy()
    {
        $this->view = '_ce_only_data_privacy';
    }

    function action_erase()
    {
        if (empty($_REQUEST['email']))
        {
            echo json_encode(array('success'=>false,'message'=>'No Email Provided.')); exit;
        }
        
        $data = array();
        $data['email'] = $_REQUEST['email'];

        $scapi = new SugarChimpApi();
        $response = $scapi->erase(null,$data);
        
        echo json_encode($response); exit;
    }

    function action_empty_table()
    {
        if (empty($_REQUEST['table_name']))
        {
            echo json_encode(array('success'=>false,'message'=>'A table name is required.')); exit;
        }
        
        $data = array();
        $data['table_name'] = $_REQUEST['table_name'];

        $scapi = new SugarChimpApi();
        $response = $scapi->emptyTable(null,$data);
        
        echo json_encode($response); exit;
    }

    function action_update_setting()
    {
        if (empty($_REQUEST['key']))
        {
            echo 'A key is required.'; exit;
        }
        
        $data = array();
        $data['key'] = $_REQUEST['key'];

        if (empty($_REQUEST['value']))
        {
            $data['value'] = '';
        }
        else
        {
            $data['value'] = $_REQUEST['value'];
        }

        $scapi = new SugarChimpApi();
        $response = $scapi->updateSetting(null,$data);
        
        echo print_r($response,true); exit;
    }

    function action_get_setting()
    {
        if (empty($_REQUEST['key']))
        {
            echo 'A key is required.'; exit;
        }
        
        $data = array();
        $data['key'] = $_REQUEST['key'];

        $scapi = new SugarChimpApi();
        $response = $scapi->getSetting(null,$data);
        
        echo print_r($response,true); exit;
    }
    
    function action_get_mailchimp_campaign($errors = array())
    {
        if (empty($_REQUEST['id']))
        {
            echo "Campaign ID is required."; exit;
        }

        $scapi = new SugarChimpApi();
        $data = $scapi->getMailChimpCampaign(null,array(
            'id' => $_REQUEST['id']
        ));

        echo print_r($data,true); exit;
    }
    
    function action_campaigns($campaign_id=false, $errors = array())
    {
        $scapi = new SugarChimpApi();
        $campaigns = $scapi->getMailChimpCampaigns(null,array());
        
        if (!empty($campaigns['result']['campaigns']))
        {
            $campaigns = $campaigns['result']['campaigns'];
            
            // filter out non-"sent" campaigns
            foreach ($campaigns as $key => $campaign)
            {
                if ($campaign['status']!='sent')
                {
                    unset($campaigns[$key]);
                }
            }
            
            $this->view_object_map['campaigns'] = $campaigns;

            if (empty($campaign_id))
            {
                reset($campaigns);
                $first = current($campaigns);
                if (!empty($first['id']))
                {
                    $campaign_id = $first['id'];
                }
            }
        }
        else
        {
            $this->view_object_map['campaigns'] = array();
        }

        $this->view_object_map['errors'] = $errors;
        
        $this->view = '_ce_only_campaigns';
    }
    
    function action_get_campaign()
    {
        if (empty($_REQUEST['campaign_id']))
        {
            echo json_encode(array('success'=>false,'message'=>'A Campaign ID is required.')); exit;
        }
        
        $campaign_id = $_REQUEST['campaign_id'];

        $scapi = new SugarChimpApi();
        $report = $scapi->getMailChimpReport(null,array('id'=>$campaign_id));

        $this->view_object_map['campaign_id'] = $campaign_id;
        $this->view_object_map['campaign'] = $report['result'];
        $this->view = '_ce_only_get_campaign';
    }

    function action_enableAccountSyncing()
    {
        $scapi = new SugarChimpApi();
        
        $response = $scapi->enableAccountSyncing(null,array());
        
        echo json_encode($response); exit;
    }
    
    function action_disableAccountSyncing()
    {
        $scapi = new SugarChimpApi();
        
        $response = $scapi->disableAccountSyncing(null,array());
        
        echo json_encode($response); exit;
    }

    function action_removeListSyncing()
    {
        $scapi = new SugarChimpApi();
        $target_list_id = $_REQUEST['target_list_id'];
        $response = $scapi->removeListSyncing(null,array('target_list_id'=>$target_list_id));

        echo json_encode($response); exit;
    }
    
}