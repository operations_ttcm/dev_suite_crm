<?php







class SugarChimp_sugar extends Basic {
    var $new_schema = true;
    var $module_dir = 'SugarChimp';
    var $object_name = 'SugarChimp';
    var $table_name = 'sugarchimp';

    var $id;
    var $name;
    var $mailchimp_list_id;
    var $param1;
    var $param2;
    var $param3;
    var $param4;
    var $param5;
    
    function bean_implements($interface){
        switch($interface){
            case 'ACL': return true;
        }
        return false;
    }
        
}
