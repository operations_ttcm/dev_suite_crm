<?php







class SugarChimpMC extends Basic 
{
    var $new_schema = true;
    var $module_dir = 'SugarChimp';
    var $object_name = 'SugarChimpMC';
    var $table_name = 'sugarchimp_mc';

    var $id;
    var $name;
    var $mailchimp_list_id;
    var $data;
    
    function bean_implements($interface){
        switch($interface){
            case 'ACL': return true;
        }
        return false;
    }
}
