<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






require_once('include/MVC/View/SugarView.php');

class View_CE_Only_Health_Status extends SugarView
{
    public function display()
    {
        $errors = array();
        if ($data['success'] !== true)
        {
            $errors []= $data['message'];
        }
        
        $this->ss->display('modules/SugarChimp/tpls/ce/health-status/health-status.tpl');
        
    }
}
