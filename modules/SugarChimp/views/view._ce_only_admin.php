<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






require_once('include/MVC/View/SugarView.php');

class View_CE_Only_Admin extends SugarView
{
    public function display()
    {
        $scapi = new SugarChimpApi();
        $data = $scapi->getAdminData(null,array('list_id'=>$list_id));

        $this->ss->assign('message',(empty($data['message']) ? '' : $data['message']));
        $this->ss->assign('success',(empty($data['success']) ? '' : $data['success']));
        $this->ss->assign('data',(empty($data['data']) ? '' : $data['data']));
        
        $this->ss->display('modules/SugarChimp/tpls/ce/admin/admin.tpl');
    }
}


