<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






require_once('include/MVC/View/SugarView.php');
require_once('modules/SugarChimp/clients/base/api/SugarChimpApi.php');

class View_CE_Only_Field_Mapping_Details extends SugarView
{
    public function display()
    {
        $list_id = (!empty($this->view_object_map['list_id'])) ? $this->view_object_map['list_id'] : false;
        $mappings = (!empty($this->view_object_map['mappings'])) ? $this->view_object_map['mappings'] : false;
        
        $this->ss->assign('list_id',$list_id);
        $this->ss->assign('mappings',$mappings);

        $html = $this->ss->fetch('modules/SugarChimp/tpls/ce/field-mapping/field-mapping-details.tpl');
        echo json_encode(array('success'=>true,'html'=>$html)); exit;
    }
}
