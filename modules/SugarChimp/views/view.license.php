<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');







require_once('include/MVC/View/SugarView.php');

class ViewLicense extends SugarView
{

    protected function _getModuleTitleParams($browserTitle = false)
    {
        global $mod_strings;

        return array(
           "<a href='index.php?module=Administration&action=index'>".translate('LBL_MODULE_NAME','Administration')."</a>",
           translate('LBL_SUGARCHIMP','Administration').': '.translate('LBL_SUGARCHIMP_LICENSE_TITLE','Administration'),
           );
    }


    public function preDisplay()
    {
        global $current_user;

        if(!is_admin($current_user)) {
            sugar_die("Unauthorized access to administration.");
        }
    }

    public function display()
    {
        global $current_user, $app_strings, $sugar_config, $currentModule, $sugar_version;

        //load license validation config
        require_once('modules/'.$currentModule.'/license/config.php');

        echo $this->getModuleTitle();

        //$this->ss->assign("MOD", $mod_strings);
        //$this->ss->assign("APP", $app_strings);
        
        //todo: redirect appropriately
        $this->ss->assign("RETURN_MODULE", "Administration");
        $this->ss->assign("RETURN_ACTION", "index");
    
        $this->ss->assign("MODULE", $currentModule);
        $this->ss->assign("LICENSE", ViewLicense::loadLicenseStrings());

        require_once('modules/'.$currentModule.'/license/OutfittersLicense.php');
        $key = SugarChimpOutfittersLicense::getKey($currentModule);

        if(!empty($key)) {
            $this->ss->assign("license_key", $key);
        }

        $this->ss->assign("continue_url",$outfitters_config['continue_url']);

        $this->ss->assign("file_path", getJSPath("modules/".$currentModule."/license/lib/jquery-1.7.1.min.js"));

        if(preg_match( "/^6.*/", $sugar_version) ) {
            $this->ss->assign("IS_SUGAR_6",true);
        } else {
            $this->ss->assign("IS_SUGAR_6",false);
        }

        if(!function_exists('curl_init')){
            global $current_language;
            $admin_mod_strings = return_module_language($current_language, 'Administration');
            $curl_not_enabled = $admin_mod_strings['ERR_ENABLE_CURL'];
            $this->ss->assign("CURL_NOT_ENABLED",$curl_not_enabled);
        }
        
        if(isset($outfitters_config['validate_users']) && $outfitters_config['validate_users'] == true) {
            $this->ss->assign("validate_users", true);
            //get user count for all active, non-portal, non-group users
            $active_users = get_user_array(FALSE,'Active','',false,'',' AND portal_only=0 AND is_group=0');
            $this->ss->assign("current_users", count($active_users));
            $this->ss->assign("user_count_param", "user_count: '".count($active_users)."'");
        } else {
            $this->ss->assign("validate_users", false);
            $this->ss->assign("current_users", '');
            $this->ss->assign("user_count_param", '');
        }
        
        $this->ss->display('modules/'.$currentModule.'/license/tpls/license.tpl');
    }
    
    protected static function loadLicenseStrings()
    {
        global $sugar_config, $currentModule, $current_language;
        
        //load license config file....if it isn't broken don't fix it
        $default_language = $sugar_config['default_language'];

        $langs = array();
        if ($current_language != 'en_us') {
            $langs[] = 'en_us';
        }
        if ($default_language != 'en_us' && $current_language != $default_language) {
            $langs[] = $default_language;
        }
        $langs[] = $current_language;

        foreach ( $langs as $lang ) {
            $license_strings = array();
            if(!@include("modules/".$currentModule."/license/language/$lang.lang.php")) {
                //do nothing...lang file could not be found
            }

            $license_strings_array[] = $license_strings;
        }

        $license_strings = array();
        foreach ( $license_strings_array as $license_strings_item ) {
            $license_strings = sugarArrayMerge($license_strings, $license_strings_item);
        }
        
        return $license_strings;
    }
}
