<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






require_once('include/MVC/View/SugarView.php');
require_once('modules/SugarChimp/clients/base/api/SugarChimpApi.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');

class View_CE_Only_Step1 extends SugarView
{
    public function display()
    {
        $this->ss->assign('errors',$this->view_object_map['errors']);
        
        $this->ss->assign('step_count',1);
        
        $this->ss->display('modules/SugarChimp/tpls/ce/setup/step1.tpl');
    }
}
