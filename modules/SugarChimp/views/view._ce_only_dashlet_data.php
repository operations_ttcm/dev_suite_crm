<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






require_once('include/MVC/View/SugarView.php');
require_once('modules/SugarChimp/clients/base/api/SugarChimpApi.php');

class View_CE_Only_Dashlet_Data extends SugarView
{
    public function display()
    {
        $this->ss->assign('data',$this->view_object_map['dashlet_data']);
        
        $html = $this->ss->fetch('modules/SugarChimp/tpls/ce/dashlet/dashlet.tpl',null,null,false);
        echo json_encode(array('success'=>true,'html'=>$html));
    }
}
