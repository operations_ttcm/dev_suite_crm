<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






require_once('include/MVC/View/SugarView.php');

class View_CE_Only_Campaigns extends SugarView
{
    public function display()
    {
        $campaigns = (is_array($this->view_object_map['campaigns']) && !empty($this->view_object_map['campaigns'])) ? $this->view_object_map['campaigns'] : array();
        $this->ss->assign('campaigns',$campaigns);

        if (!empty($this->view_object_map['campaign_id']))
        {
            $this->ss->assign('campaign_id',$this->view_object_map['campaign_id']);
        }
        else
        {
            $this->ss->assign('campaign_id',false);
        }
        
        // assign errors passed from controller
        $this->ss->assign('errors',$this->view_object_map['errors']);
        
        $this->ss->display('modules/SugarChimp/tpls/ce/campaigns/campaigns.tpl');
    }
}
