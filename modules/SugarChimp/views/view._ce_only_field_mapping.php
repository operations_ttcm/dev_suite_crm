<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






require_once('include/MVC/View/SugarView.php');
require_once('modules/SugarChimp/clients/base/api/SugarChimpApi.php');

class View_CE_Only_Field_Mapping extends SugarView
{
    public function display()
    {
        $lists = (is_array($this->view_object_map['lists']) && !empty($this->view_object_map['lists'])) ? $this->view_object_map['lists'] : array();
        $this->ss->assign('lists',$lists);

        if (!empty($this->view_object_map['list_id'])){
            $this->ss->assign('list_id',$this->view_object_map['list_id']);
        }
        
        // assign errors passed from controller
        $this->ss->assign('errors',$this->view_object_map['errors']);
        
        $this->ss->display('modules/SugarChimp/tpls/ce/field-mapping/field-mapping.tpl');
    }
}
