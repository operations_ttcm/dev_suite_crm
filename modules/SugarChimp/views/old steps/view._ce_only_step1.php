<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






require_once('include/MVC/View/SugarView.php');
require_once('modules/SugarChimp/clients/base/api/SugarChimpApi.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');

class View_CE_Only_Step1 extends SugarView
{
    public function display()
    {
        // get MC key if it exists
        $scapi = new SugarChimpApi();
        $data = $scapi->getLicenseValidation(null,array());
        
        $this->ss->assign('validated', (empty($data['success']) ? 'false' : true));
        
        $this->ss->assign('licensekey', (empty($data['key']) ? '' : $data['key']));

        // assign errors passed from controller
        $this->ss->assign('errors',$this->view_object_map['errors']);
        
        $this->ss->assign('step_count',1);
        
        $this->ss->display('modules/SugarChimp/tpls/ce/setup/step1.tpl');
    }
}
