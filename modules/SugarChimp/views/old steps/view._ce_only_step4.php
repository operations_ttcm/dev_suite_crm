<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






require_once('include/MVC/View/SugarView.php');

class View_CE_Only_Step4 extends SugarView
{
    public function display()
    {
        $sync_options = array(
        	'master' => array(
        		'title' => 'Start from Scratch (recommended)',
        		'button_name' => 'Scratch',
        		'button_action' => 'step7',
        		'desc' => 'Starting with a blank slate, you will choose exactly who to sync over to MailChimp'
        	)
        );
        $scapi = new SugarChimpApi();
        $mc_data = $scapi->getMailChimpListsCount(null,array());
        $sugar_data = $scapi->getSugarCRMLists(null,array());

        if($mc_data['total_list_count'] != 0){
            $sync_options['mailchimp'] = array(
        		'title' => 'Start with MailChimp',
        		'button_name' => 'MailChimp',
        		'button_action' => 'step6',
        		'desc' => 'Choose this method if you have MailChimp subscribers that you would like to sync to Sugar.'
        	);
        }
            
        if($sugar_data['total_list_count'] != 0){
			$sync_options['sugar'] = array(
        		'title' => 'Start with Sugar',
        		'button_name' => 'Scratch',
        		'button_action' => 'step5',
        		'desc' => 'Choose this method if you have a Sugar Target List that you would like to sync over to MailChimp.'
        	);        
		}
		$sync_options['later'] = array(
    		'title' => 'Wait to Sync',
    		'button_name' => 'Sync Later',
    		'button_action' => 'health_status',
    		'desc' => 'Not sure what to choose? You can always come back'
    	);
		
		$this->ss->assign('sync_options',$sync_options);
        $this->ss->assign('step_count',4);
        
        $this->ss->display('modules/SugarChimp/tpls/ce/setup/step4.tpl');
    }
}
