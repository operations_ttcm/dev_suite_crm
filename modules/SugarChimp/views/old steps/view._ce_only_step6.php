<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






require_once('include/MVC/View/SugarView.php');

class View_CE_Only_Step6 extends SugarView
{
    public function display()
    {
        $scapi = new SugarChimpApi();

        $this->ss->assign('step_count',6);
        
        $this->ss->display('modules/SugarChimp/tpls/ce/setup/step6.tpl');
    }
}
