<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






require_once('include/MVC/View/SugarView.php');
require_once('modules/SugarChimp/clients/base/api/SugarChimpApi.php');

class View_CE_Only_Step2 extends SugarView
{
    public function display()
    {
        // get MC key if it exists
        $scapi = new SugarChimpApi();
        $data = $scapi->getApiKey(null,array());
        if(empty($data['apikey'])) {
            $validated = false;
            $key = '';
        }
        else {
            $validated = true;
            $apikey = $data['apikey'];
        }
        
        $this->ss->assign('validated', $validated);
        $this->ss->assign('apikey', $apikey);
        
        // assign errors passed from controller
        $this->ss->assign('errors',$this->view_object_map['errors']);
        
        $this->ss->assign('step_count',2);
        
        $this->ss->display('modules/SugarChimp/tpls/ce/setup/step2.tpl');
    }
}
