<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






require_once('include/MVC/View/SugarView.php');

class View_CE_Only_Get_Campaign extends SugarView
{
    public function display()
    {
        $campaign_id = (!empty($this->view_object_map['campaign_id'])) ? $this->view_object_map['campaign_id'] : false;
        $campaign = (!empty($this->view_object_map['campaign'])) ? $this->view_object_map['campaign'] : false;
        
        $this->ss->assign('campaign_id',$campaign_id);
        $this->ss->assign('campaign',$campaign);

        $html = $this->ss->fetch('modules/SugarChimp/tpls/ce/campaigns/campaign.tpl');
        echo json_encode(array('success'=>true,'html'=>$html)); exit;
    }
}
