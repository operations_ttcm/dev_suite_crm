<?php
require_once('include/formbase.php');
require_once('modules/PM_ProcessManager/PM_ProcessManager.php');
require_once('log4php/LoggerManager.php');
require_once('data/SugarBean.php');
global $current_user;
$user_id = $current_user->id;
$processFilterFieldArray = array();
$processFilterListArray = array();
$processFilterFieldValueArray = array();
$thisSugarBean = new SugarBean();
$focus = new PM_ProcessManager();
$focus->retrieve($_POST['record']);
$return_val = array();
//In this custom save we need to pick up the following fields and add after the sugar save

$name =($_POST['name']);
$status =($_POST['status']);
$description =($_POST['description']);
$processObject = ($_POST['process_object']);
$focus->name = $name;
$focus->status = $status;
$focus->process_object = $processObject;
$processObject =($_POST['process_object']);
$startEvent = ($_POST['start_event']);
$processOjectCancelEvent =($_POST['cancel_on_event']);
$processObjectCancelField =($_POST['process_object_cancel_field']);
$processObjectCancelFieldValue =($_POST['process_object_cancel_field_value']);
$processObjectCancelFieldOperator =($_POST['process_object_cancel_field_operator']);
$assignedUserID = ($_POST['assigned_user_id']);
$focus->assigned_user_id = $assignedUserID;
$check_delete_modify_cancel = ($_POST['check_delete_modify_cancel']);
$focus->save();

if ($processObjectCancelField == 'Please') {
	$processObjectCancelField = 'N/A';
}

//***********************************************************************
//New Code for PM 6.2 for process defs table
//***********************************************************************
$focusID = $_POST['record'];
$newID = create_guid();
$ignore_process_prev_run_check =($_POST['ignore_process_prev_run_check']);
$process_query_value =($_POST['process_query_value']);
$process_frequency =($_POST['process_frequency']);
$process_start_time_day =($_POST['process_start_time_day']);
$process_start_day_of_week =($_POST['process_start_day_of_week']);
$process_day_of_month =($_POST['process_day_of_month']);
//PM PRO 2.0
$process_last_run_date =($_POST['process_last_run_date']);
if($process_last_run_date == ''){
	$date = date('Y-m-d h:m:s');
	$process_last_run_date = $date;
}
$process_month =($_POST['process_month']);
$route_object =($_POST['route_object']);
$routing_type =($_POST['routing_type']);
$route_to_user =($_POST['route_to_user']);
$check_login_status =($_POST['check_login_status']);
$find_logged_in_user =($_POST['find_logged_in_user']);
$route_to_role =($_POST['route_to_role']);
//PM Pro v2
$route_to_team =($_POST['route_to_team']);
$relatedObject =($_POST['related_object']);
if(($relatedObject == 'Please Specify') || ($relatedObject == 'Please')) {
	$relatedObject = '';
}
$filterOnRelated = ($_POST['filter_on_related']);
if ($ignore_process_prev_run_check == '') {
	$ignore_process_prev_run_check = 0;
}
if ($route_object == '') {
	$route_object = 0;
}
if ($check_login_status == '') {
	$check_login_status = 0;
}
if ($find_logged_in_user == '') {
	$find_logged_in_user = 0;
}
$query_insert_pm_defs = "Insert into pm_process_defs set id = '" .$newID ."'";
$query_insert_pm_defs .= ", process_id = '" .$focus->id ."', ignore_process_prev_run_check = '" .$ignore_process_prev_run_check ."'";
$query_insert_pm_defs .= ", process_query_value = '$process_query_value', process_frequency = '$process_frequency'";
$query_insert_pm_defs .= ", process_start_time_day = '$process_start_time_day', process_start_day_of_week = '$process_start_day_of_week'";
$query_insert_pm_defs .= ", process_day_of_month = '$process_day_of_month', process_month = '$process_month', process_last_run_date = '$process_last_run_date'";
$query_insert_pm_defs .= ", route_object = '$route_object', routing_type = '$routing_type'";
$query_insert_pm_defs .= ", route_to_user = '$route_to_user', check_login_status = '$check_login_status'";
$query_insert_pm_defs .= ", find_logged_in_user = '$find_logged_in_user', route_to_role = '$route_to_role', route_to_team = '$route_to_team'";


$query_update_pm_defs = "Update pm_process_defs set  ignore_process_prev_run_check = '" .$ignore_process_prev_run_check ."'" ;
$query_update_pm_defs .=  ", process_query_value = '$process_query_value', process_frequency = '$process_frequency'";
$query_update_pm_defs .= ", process_start_time_day = '$process_start_time_day', process_start_day_of_week = '$process_start_day_of_week'";
$query_update_pm_defs .= ", process_day_of_month = '$process_day_of_month', process_month = '$process_month' , process_last_run_date = '$process_last_run_date'";
$query_update_pm_defs .= ", route_object = '$route_object', routing_type = '$routing_type'";
$query_update_pm_defs .= ", route_to_user = '$route_to_user', check_login_status = '$check_login_status'";
$query_update_pm_defs .= ", find_logged_in_user = '$find_logged_in_user', route_to_role = '$route_to_role', route_to_team = '$route_to_team'";
$query_update_pm_defs .= " where process_id = '" .$focusID ."'";											
if ($focusID != '') {
	$focus->db->query($query_update_pm_defs, true);
}
else{
	$focus->db->query($query_insert_pm_defs, true);
}



//Now update the record with the name
//Now see if there any filter table entries and if so then add
$return_id = $focus->id;

$queryUpdate = "Update pm_processmanager set name = '$name', process_object = '$processObject', related_object = '$relatedObject', filter_on_related = '$filterOnRelated', start_event = '$startEvent', description = '$description', "; 
$queryUpdate .= " cancel_on_event = '$processOjectCancelEvent', process_object_cancel_field = '$processObjectCancelField', "; 
$queryUpdate .= " process_object_cancel_field_value = '$processObjectCancelFieldValue', process_object_cancel_field_operator = '$processObjectCancelFieldOperator' , check_delete_modify_cancel = '$check_delete_modify_cancel'  where id = '$return_id'";

$thisSugarBean->db->query($queryUpdate, true);
//TODO - Figure out if we need cancel on field name
$cancel_on_field_name = $_POST["cancel_on_field_name"];
$cancel_on_field_value = $_POST["cancel_on_field_value"];
//If either of these two fields are not blank then we have a process for change lead status or change sales stage
//For each of the 5 filter field/value combos we insert into the filter table
$andOrFilter = $_POST['andorfilterfield'];
for ($i=1; $i<6; $i++){
	$processFilterField = "process_filter_field" .$i;
	$processFilterList = "filter_list" .$i;
	$processFilterFieldValue = "process_object_field" .$i ."_value";
		
	$process_object_field = $_POST["$processFilterField"];
	$process_filter_list = $_POST["$processFilterList"];
	$process_object_field_value = $_POST["$processFilterFieldValue"];

	if (($process_object_field_value != "") || ($process_filter_list == 'any change') || ($process_filter_list == 'date field due today') || ($process_filter_list == 'any value')|| ($process_filter_list == 'is null')) {
		$processFilterFieldArray[$i] = $process_object_field;
		$processFilterListArray[$i] = $process_filter_list;
		$processFilterFieldValueArray[$i] = $process_object_field_value;
	}
}

//Now check to see if the arrays have any values
//if so build the insert for the process filter table entry
if(count($processFilterFieldArray) > 0){
	$countOfFilterValues = count($processFilterFieldArray);
	for ($i = 1; $i <= $countOfFilterValues; $i++ ){
		$procss_object_field = $processFilterFieldArray[$i];
		$process_filter_list = $processFilterListArray[$i];
		$proces_object_field_value = $processFilterFieldValueArray[$i];
				//As long as procee object field is not null or blank then insert
		if (($proces_object_field_value != "") || ($process_filter_list == 'any change') || ($process_filter_list == 'date field due today') || ($process_filter_list == 'any value')) {
			//Need to know if we are inserting a new entry or updating an existing one
				$processFilterTableId = checkIfProcessFilterTableExists($return_id,$i,$thisSugarBean);
				$lcrmProcessFilterTableId = create_guid();
				//convert less than greater than
				if($process_filter_list == '&lt;'){
					$process_filter_list = '<';
				}
				if($process_filter_list == '&gt;'){
					$process_filter_list = '>';
				}				
				$query_insert = "Insert into pm_process_filter_table set id = '" .$lcrmProcessFilterTableId ."'";
				$query_insert .= ", process_id = '" .$focus->id ."', field_name = '" .$procss_object_field ."', field_value = '" .$proces_object_field_value ."'";
				$query_insert .= ",  field_operator = '$process_filter_list', sequence = $i, andorfilterfields = '$andOrFilter' ";
			
				
				$query_update = "Update pm_process_filter_table set  field_name = '" .$procss_object_field ."', field_value = '" .$proces_object_field_value ."'" ;
				$query_update .= ", field_operator = '$process_filter_list', andorfilterfields = '$andOrFilter' ";
				$query_update .= " where id = '" .$processFilterTableId ."' and sequence = $i";											
				if ($processFilterTableId != '') {
					$focus->db->query($query_update, true);
				}
				else{
					$focus->db->query($query_insert, true);
				}
		}

	}
	
}

/*
 * PM Pro v2 - Related Filter Entries
 * 
 */
$andOrRelatedFilter = $_POST['related_andorfilterfield'];
for ($i=1; $i<6; $i++){
	$processFilterField = "related_filter_field" .$i;
	$processFilterList = "related_filter_list" .$i;
	$processFilterFieldValue = "related_field" .$i ."_value";

	$process_object_field = $_POST["$processFilterField"];
	$process_filter_list = $_POST["$processFilterList"];
	$process_object_field_value = $_POST["$processFilterFieldValue"];

	if (($process_object_field_value != "") || ($process_filter_list == 'any change') || ($process_filter_list == 'date field due today') || ($process_filter_list == 'any value')) {
		$processRelatedFilterFieldArray[$i] = $process_object_field;
		$processRelatedFilterListArray[$i] = $process_filter_list;
		$processRelatedFilterFieldValueArray[$i] = $process_object_field_value;
	}
}

//Now check to see if the arrays have any values
//if so build the insert for the process filter table entry

if(count($processRelatedFilterFieldArray) > 0){
	$countOfFilterValues = count($processRelatedFilterFieldArray);
	for ($i = 1; $i <= $countOfFilterValues; $i++ ){
		$procss_related_object_field = $processRelatedFilterFieldArray[$i];
		$process_related_filter_list = $processRelatedFilterListArray[$i];
		$proces_related_object_field_value = $processRelatedFilterFieldValueArray[$i];
		//As long as procee object field is not null or blank then insert
		if (($procss_related_object_field != "") || ($process_related_filter_list == 'any change') || ($process_related_filter_list == 'date field due today') || ($process_related_filter_list == 'any value')) {
			//Need to know if we are inserting a new entry or updating an existing one
			$processRelatedFilterTableId = checkIfProcessRelatedFilterTableExists($return_id,$i,$thisSugarBean);
			$lcrmProcessFilterTableId = create_guid();
			//convert less than greater than
			if($process_related_filter_list == '&lt;'){
				$process_related_filter_list = '<';
			}
			if($process_related_filter_list == '&gt;'){
				$process_related_filter_list = '>';
			}
			$query_insert = "Insert into pm_process_related_filter_table set id = '" .$lcrmProcessFilterTableId ."'";
			$query_insert .= ", process_id = '" .$focus->id ."', related_field_name = '" .$procss_related_object_field ."', related_field_value = '" .$proces_related_object_field_value ."'";
			$query_insert .= ",  related_field_operator = '$process_related_filter_list', sequence = $i, andorfilterfields = '$andOrRelatedFilter' ";


			$query_update = "Update pm_process_related_filter_table set  related_field_name = '" .$procss_related_object_field ."', related_field_value = '" .$proces_related_object_field_value ."'" ;
			$query_update .= ", related_field_operator = '$process_related_filter_list', andorfilterfields = '$andOrRelatedFilter' ";
			$query_update .= " where id = '" .$processRelatedFilterTableId ."' and sequence = $i";
			if ($processRelatedFilterTableId != '') {
				$focus->db->query($query_update, true);
			}
			else{
				$focus->db->query($query_insert, true);
			}
		}

	}

}
//New code for 6.2 to make sure that the logic hook file exists for the given module
//The name of the object is in the process_object

//PM Pro 
$bean = convertTableToBean($processObject);
$moduleName = $bean->module_name;
//Convert the object to Bean

$hook = array();
$hook['module'] = $moduleName;
$hook['hook'] = 'after_save';
$hook['order'] = '99';
$hook['description'] = 'Process Manager Hook';
$hook['file'] = 'modules/PM_ProcessManager/insertIntoPmEntryTable.php';
$hook['class'] = 'insertIntoPmEntryTable';
$hook['function'] = 'setPmEntryTable';
check_logic_hook_file($hook['module'], $hook['hook'], array($hook['order'], $hook['description'],  $hook['file'], $hook['class'], $hook['function']));


handleRedirect($return_id, "PM_ProcessManager");
//Check to see if the entry in the process filter table already exists

function checkIfProcessFilterTableExists($processId,$sequence,$thisSugarBean){
	$processFilterTableId = '';
	$query = "Select id from pm_process_filter_table where process_id = '$processId' and sequence = $sequence";
	$result = $thisSugarBean->db->query($query, true);
	while($rowProcessFilterTable = $thisSugarBean->db->fetchByAssoc($result))
		{	
			$processFilterTableId = $rowProcessFilterTable['id'];	
		}

	return $processFilterTableId;
}

/*
 * PM Pro V2 
 * 
 */
function checkIfProcessRelatedFilterTableExists($processId,$sequence,$thisSugarBean){
	$processFilterTableId = '';
	$query = "Select id from pm_process_related_filter_table where process_id = '$processId' and sequence = $sequence";
	$result = $thisSugarBean->db->query($query, true);
	while($rowProcessFilterTable = $thisSugarBean->db->fetchByAssoc($result))
	{
		$processFilterTableId = $rowProcessFilterTable['id'];
	}

	return $processFilterTableId;
}


function convertTableToBean($focusObjectType){
	global $moduleList;
	global $beanList;
	$exemptModules = array();
	$exemptModules['Calendar.php'] = "Calendar.php";
	foreach ($moduleList as $key=>$value){
		$module_array = array();
		$module_array[$key]=$value;
		$module_array = convert_module_to_singular($module_array);
		if($key != 0){
			$key1 = $module_array[$key];
			$bean = $key1;
			$key1 .= ".php";
			$beanFile = "modules/$value/$key1";
			if (file_exists("$beanFile")) {
				if (!in_array($key1,$exemptModules)) {
					if ($bean == "Case") {
						$bean = "aCase";
					}
					require_once("$beanFile");
					$newbean = new $bean;
					$table = $newbean->table_name;
					if ($focusObjectType == $table) {
						return $newbean;
					}
				}
			}
		}
	}
}




?>