<?php
/*********************************************************************************
 * Copyright: SierraCRM, Inc. 2014
 * Portions created by SierraCRM are Copyright (C) SierraCRM, Inc.
 * The contents of this file are subject to the SierraCRM, Inc. End User License Agreement
 * You may not use this file except in compliance with the License. 
 * You may not rent, lease, lend, or in any way distribute or transfer any rights or this file or Process Manager
 * registrations (purchased licenses) to third parties without SierraCRM, Inc. written approval, and subject to
 * agreement by the recipient of the terms of this EULA.
 * Process Manager for SugarCRM is owned by SierraCRM, Inc. and is protected by international and local copyright laws and
 * treaties. You must not remove or alter any copyright notices on any copies of Process Manager for SugarCRM. 
 * You may not use, copy, or distribute Process Manager for SugarCRM, except as granted by SierraCRM, Inc.
 * without written authorization from SierraCRM, Inc. or its designated agents. Furthermore, this Copyright notice
 * does not grant you any rights in connection with any trademarks or service marks of SierraCRM, Inc. 
 * SierraCRM, Inc. reserves all intellectual property rights, including copyrights, and trademark rights of this software.
 ********************************************************************************/
/*********************************************************************************
 *SierraCRM, Inc
 *14563 Ward Court
 *Grass Valley, CA. 95945
 *www.sierracrm.com
 ********************************************************************************/

function checkLicense(){
    global $current_user, $currentModule;
    require_once('modules/'.$currentModule.'/license/config.php');
    if($sierracrm_config['validate_sugaroutfitters'] === true){
        require_once('modules/'.$currentModule.'/license/OutfittersLicense.php');
        $validate_license = OutfittersLicense::isValid('PM_ProcessManager');
        if($validate_license !== true) {
            if(is_admin($current_user)) {
                SugarApplication::appendErrorMessage('Process Manager is no longer active due to the following reason: '.$validate_license.' Users will have limited to no access until the issue has been addressed.');
            }
            return false;
        }
    }
    require_once('modules/'.$currentModule.'/ValidateLicenseKey.php');
    return SierraCRMLicense::checkLicense();
}

if(!checkLicense()){
	$queryParams = array(
		'module' => 'PM_ProcessManager',
		'action' => 'license',
	);
	SugarApplication::redirect('index.php?' . http_build_query($queryParams));	
}
?>
<iframe src="index.php?module=PM_ProcessManager&action=CreateV2Start&<?php if(isset($_GET['record'])): ?>record=<?php echo $_GET['record']; ?>&<?php endif; ?>to_pdf=1" width="100%" style="border: none; height: 700px;"></iframe>
