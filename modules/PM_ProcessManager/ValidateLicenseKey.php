<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * Copyright: SierraCRM, Inc. 2007
 * Portions created by SierraCRM are Copyright (C) SierraCRM, Inc.
 * The contents of this file are subject to the SierraCRM, Inc. End User License Agreement
 * You may not use this file except in compliance with the License. 
 * You may not rent, lease, lend, or in any way distribute or transfer any rights or this file or Process Manager
 * registrations (purchased licenses) to third parties without SierraCRM, Inc. written approval, and subject to
 * agreement by the recipient of the terms of this EULA.
 * Process Manager for SugarCRM is owned by SierraCRM, Inc. and is protected by international and local copyright laws and
 * treaties. You must not remove or alter any copyright notices on any copies of Process Manager for SugarCRM. 
 * You may not use, copy, or distribute Process Manager for SugarCRM, except as granted by SierraCRM, Inc.
 * without written authorization from SierraCRM, Inc. or its designated agents. Furthermore, this Copyright notice
 * does not grant you any rights in connection with any trademarks or service marks of SierraCRM, Inc. 
 * SierraCRM, Inc. reserves all intellectual property rights, including copyrights, and trademark rights of this software.
 ********************************************************************************/
/*********************************************************************************
 *SierraCRM, Inc
 *14563 Ward Court
 *Grass Valley, CA. 95945
 *www.sierracrm.com
 ********************************************************************************/

/********************
 * IMPORTANT NOTICE:
 * CHANGING THIS FILE MAY BREAK THIS PRODUCT. DOING SO IS NOT RECOMMENDED AND IS ILLEGAL.
 * PLEASE CONTACT SIERRACRM IF YOU NEED ASSISTANT
 * !!! DO NOT CHANGE THIS FILE !!!
 * *************************/


class SierraCRMLicense{
     public static function validateLicense($json = false){
        global $sugar_config;
        $params = array(
            'vlk_email' => $_POST['email'],
            'vlk_isprod' => ($_POST['sierracrm_env'] == 'prod') ? '1' : '0',
            'vlk_dbname' => $sugar_config['dbconfig']['db_name'],
            'vlk_key' => $sugar_config['unique_key'],
            'vlk_site_url' => $sugar_config['site_url'],
            'vlk_method' => 'validate',
        );
        $data = self::makeRequest($params);
        $color = 'red';
        if($data->success) $color = 'green';
        
        if($data->success){
            self::updateConfig($data->key, $_POST['sierracrm_env']);
        }

        if($json){
            echo json_encode(array('success' => $data->success, 'msg' => $data->msg));
        }else{
        }

    }

    public static function checkLicense($json = false){
    	  global $sugar_config; 
        if($row = self::getConfig()){
            if($row['name'] == 'PM_Prod_Key'){
                $is_prod = '1';
            }else{
                $is_prod = '0';
            }
            $params = array(
                'vlk' => $row['value'],
                'vlk_isprod' => $is_prod,
                'vlk_method' => 'check',
                'vlk_dbname' => $sugar_config['dbconfig']['db_name'],
            	'vlk_key' => $sugar_config['unique_key'],
            	'vlk_site_url' => $sugar_config['site_url'],
            );
            $data = self::makeRequest($params);
            $color = 'red';
            if($data->success) $color = 'green';
            if($json){
                echo json_encode(array('success' => $data->success, 'msg' => $data->msg));
            }else{
            }
            return $data->success;
        }else{
            echo json_encode(array('success' => false, 'msg' => "License Key not validated"));
            return false;
        }
    }

    public static function getConfig(){
        global $db;
        $checkSQL = "SELECT * FROM config WHERE category = 'PM_Ent' and (name = 'PM_Prod_Key' OR name = 'PM_Dev_Key')";
        $checkQuery = $db->query($checkSQL);
        if($row = $db->fetchByAssoc($checkQuery)){
            return $row;
        }else{
            return false;
        }
    }

    public static function updateConfig($vlk_key, $env){
        global $db;
        if($env == 'prod'){
            $name = 'PM_Prod_Key';
        }else{
            $name = 'PM_Dev_Key';
        }

        $sql = "INSERT INTO config (category, name, value) VALUES ('PM_Ent', '".$name."', '".$vlk_key."')";
        $query = $db->query($sql);
    }

    private function makeRequest($params){
        if(empty($thisModule)) {
            global $currentModule, $sugar_config;;
            $thisModule = $currentModule;
            
            //if still empty...then get out of here...in an odd spot in SugarCRM
            if(empty($thisModule)) {
                return true;
            }
        }

        //load license validation config
        require('modules/'.$thisModule.'/license/config.php');

        $curl = curl_init();
        //See if the server is using a Proxy Server
        if (method_exists('Administration','getSettings')) {
        //See if the server is using a Proxy Server
        $proxy_config = Administration::getSettings('proxy');
        if( !empty($proxy_config) &&
            !empty($proxy_config->settings['proxy_on']) &&
            $proxy_config->settings['proxy_on'] == 1) {

            curl_setopt($curl, CURLOPT_PROXY, $proxy_config->settings['proxy_host']);
            curl_setopt($curl, CURLOPT_PROXYPORT, $proxy_config->settings['proxy_port']);
            if (!empty($proxy_settings['proxy_auth'])) {
                curl_setopt($curl, CURLOPT_PROXYUSERPWD, $proxy_settings['proxy_username'] . ':' . $proxy_settings['proxy_password']);
            }
        }
      }
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $sierracrm_config['api_url'] . '?'. http_build_query($params),
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 5,
            CURLOPT_FAILONERROR => false
        ));
        $resp = curl_exec($curl);
        curl_close($curl);
        if(empty($resp)){
            $obj = new stdClass();
            $obj->success = false;
            $obj->msg = "Unable to connect with SierraCRM";
            return $obj;
        }
        return json_decode($resp);
    }

}

if(isset($_POST['ValidateLicenseKeySubmit']) && $_POST['ValidateLicenseKeySubmit']){
    SierraCRMLicense::validateLicense(true);
}else if(isset($_POST['CheckLicense']) && $_POST['CheckLicense']){
    SierraCRMLicense::checkLicense(true);
}
