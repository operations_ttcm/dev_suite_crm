<?php

/*
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright  2004-2013 SugarCRM Inc.  All rights reserved.
 */
$module_name = 'PM_ProcessManager';
$viewdefs[$module_name]['base']['menu']['header'] = array(
	array(
		'route' => '#bwc/index.php?module='.$module_name.'&action=CreateV2',
		'label' =>'LBL_CREATE_PROCESS',
		'acl_action'=>'create_process',
		'acl_module'=>$module_name,
		'icon' => 'icon-plus',
	),
	array(
		'route' => '#bwc/index.php?module='.$module_name.'&action=index',
		'label' =>'LBL_LIST_PROCESSES',
		'acl_action'=>'list_processes',
		'acl_module'=>$module_name,
		'icon' => 'icon-reorder',
	),
	array(
		'route' => '#bwc/index.php?module='.$module_name.'&action=RunProcessManager&isRunningFromMenu=true',
		'label' =>'Run Process Manager',
		'acl_action'=>'run_process',
		'acl_module'=>$module_name,
		'icon' => 'icon-bar-chart',
	),
	array(
		'route' => '#bwc/index.php?module='.$module_name.'&action=license',
		'label' =>'Validate License Key',
		'acl_action'=>'process_manager',
		'acl_module'=>$module_name,
		'icon' => 'icon-check',
	),
	array(
		'route' => '#bwc/index.php?module='.$module_name.'&action=transferLegacyWF',
		'label' =>'Transfer Legacy Workflow',
		'acl_action'=>'process_manager',
		'acl_module'=>$module_name,
		'icon' => 'icon-check',
	),	
);
