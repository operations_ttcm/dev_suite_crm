<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('data/BeanFactory.php');

class PMCustomAPI extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'record_reports' => array(
                'reqType' => 'GET',
                'path' => array('PM_ProcessManager', '?', 'record_reports', '?'),
                'pathVars' => array('module', 'record', 'method', 'focus_module'),
                'method' => 'recordReports',
                'shortHelp' => 'Retrieve the processes being triggered on a certain event',
                'longHelp' => '',
            ),
            'delete_waiting' => array(
                'reqType' => 'GET',
                'path' => array('PM_ProcessManager', '?', 'delete_waiting'),
                'pathVars' => array('module', 'record'),
                'method' => 'deleteWaiting',
                'shortHelp' => 'Delete Waiting Todo record',
                'longHelp' => '',
            ),
            'delete_completed' => array(
                'reqType' => 'GET',
                'path' => array('PM_ProcessManager', '?', 'delete_completed'),
                'pathVars' => array('module', 'record'),
                'method' => 'deleteCompleted',
                'shortHelp' => 'Delete completed record',
                'longHelp' => '',
            ),
        );
    }

    public function recordReports($api, $args)
    {
        global $db;
        
        $test = true;
        
        $record = BeanFactory::getBean($args['focus_module'], $args['record']);
        
    	$get = "SELECT 
        	pm_processmanager.name,
        	pm_process_completed_process.id as completed_id,
    	    pm_process_completed_process.process_id,
    		pm_process_completed_process.stage_id,
    		pm_process_completed_process.process_complete_date,
    		pm_processmanagerstage.name as 'stage_name'
    		FROM
    		pm_process_completed_process
    		LEFT JOIN pm_processmanagerstage ON pm_processmanagerstage.id = pm_process_completed_process.stage_id 
    		JOIN pm_processmanager ON pm_processmanager.id = pm_process_completed_process.process_id 
    		WHERE
    		 pm_process_completed_process.object_id = '{$record->id}'
    		 AND
    		 pm_process_completed_process.object_type = '{$record->table_name}'
    		";	
    	$res = $db->query($get);
    	$completed = array();
    	while($row = $db->fetchByAssoc($res)){
    		$completed[] = array(
    		    'process_link' => '#bwc/index.php?module=PM_ProcessManager&action=CreateV2&record=' . $row['process_id'],
    		    'name' => $row['name'],
    			'id' => $row['process_id'],
    			'completed_id' => $row['completed_id'],
    			'module' => $record->object_name,
    			'date_completed' => date('F j, Y, g:i a', strtotime($row['process_complete_date'])),
    			'stage_id' => $row['stage_id'],
    			'stage_name' => $row['stage_name']
    		);
    	}
    	
        $get = "SELECT 
    	    pm_processmanager.name,
    	    pm_process_stage_waiting_todo.process_id,
    		pm_process_stage_waiting_todo.id as waiting_id,
    		pm_process_stage_waiting_todo.object_id,
    		pm_process_stage_waiting_todo.object_type,
    		pm_process_stage_waiting_todo.stage_id,
    		pm_process_stage_waiting_todo.start_delay_type,
    		pm_process_stage_waiting_todo.start_time,
    		pm_processmanagerstage.name as 'stage_name'
    		FROM
    		pm_process_stage_waiting_todo
    		LEFT JOIN pm_processmanagerstage ON pm_processmanagerstage.id = pm_process_stage_waiting_todo.stage_id 
            JOIN pm_processmanager ON pm_processmanager.id = pm_process_stage_waiting_todo.process_id 
    		WHERE
    		  pm_process_stage_waiting_todo.object_id = '{$record->id}'
    		 AND
    		 pm_process_stage_waiting_todo.object_type = '{$record->table_name}'";
    	$res = $db->query($get);
    	$waiting = array();
    	while($row = $db->fetchByAssoc($res)){
    		$waiting[] = array(
    		    'process_link' => '#bwc/index.php?module=PM_ProcessManager&action=CreateV2&record=' . $row['process_id'],
    		    'waiting_id' => $row['waiting_id'],
    		    'name' => $row['name'],
    			'id' => $row['process_id'],
    			'module' => $record->object_name,
    			'start_time' => date('F j, Y, g:i a', strtotime($row['start_time'])),
    			'start_delay_type' => $row['start_delay_type'],
    			'stage_id' => $row['stage_id'],
    			'stage_name' => $row['stage_name']
    		);
    	}
    	
    	return array( 'completed' => $completed, 'waiting' => $waiting);
    }
    
    public function deleteWaiting($api, $args){
        global $db;
        if(empty($args['record'])){
            return array('success' => false);
        }
        if($db->query("DELETE FROM pm_process_stage_waiting_todo WHERE id = '{$args['record']}'")){
            return array('success' => true);
        }else{
            return array('success' => false);
        }
    }
    
    public function deleteCompleted($api, $args){
        global $db;
        if(empty($args['record'])){
            return array('success' => false);
        }
        if($db->query("DELETE FROM pm_process_completed_process WHERE id = '{$args['record']}'")){
            return array('success' => true);
        }else{
            return array('success' => false);
        }
    }
}

?>
