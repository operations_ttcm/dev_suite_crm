<?php
if(!defined('sugarEntry'))define('sugarEntry', true);
/*********************************************************************************
 * Copyright: SierraCRM, Inc. 2014
 * Portions created by SierraCRM are Copyright (C) SierraCRM, Inc.
 * The contents of this file are subject to the SierraCRM, Inc. End User License Agreement
 * You may not use this file except in compliance with the License. 
 * You may not rent, lease, lend, or in any way distribute or transfer any rights or this file or Process Manager
 * registrations (purchased licenses) to third parties without SierraCRM, Inc. written approval, and subject to
 * agreement by the recipient of the terms of this EULA.
 * Process Manager for SugarCRM is owned by SierraCRM, Inc. and is protected by international and local copyright laws and
 * treaties. You must not remove or alter any copyright notices on any copies of Process Manager for SugarCRM. 
 * You may not use, copy, or distribute Process Manager for SugarCRM, except as granted by SierraCRM, Inc.
 * without written authorization from SierraCRM, Inc. or its designated agents. Furthermore, this Copyright notice
 * does not grant you any rights in connection with any trademarks or service marks of SierraCRM, Inc. 
 * SierraCRM, Inc. reserves all intellectual property rights, including copyrights, and trademark rights of this software.
 ********************************************************************************/
/*********************************************************************************
 *SierraCRM, Inc
 *14563 Ward Court
 *Grass Valley, CA. 95945
 *www.sierracrm.com
 ********************************************************************************/

include_once('config.php');
require_once('include/entryPoint.php');


global $previousTaskId;
global $runningProcessManager;

class ProcessManagerEngine extends SugarBean {

	var $object_name = "ProcessManageEngine";
	var $module_dir = 'ProcessManager';

	function ProcessManagerEngine() {
		$GLOBALS['log'] = LoggerManager :: getLogger('SugarCRM');
		global $sugar_config;
		//parent::SugarBean();

	}

  public function __construct()
    {
        global $db;
        $this->db = $db;
    }
	var $new_schema = true;
	function processManagerMain($OnDemand = FALSE){
	  $GLOBALS['log']->info("Process Manager - Starting Process Manager");
	  global $current_user, $sugar_config;
	  $sugar_config['default_limit'] = '50000';
	  $query = "Select id, object_id, object_type, object_event from pm_processmanager_entry_table";
    $result = $this->db->query($query,true);
		$queryDeleteEntryTable = "Delete from pm_processmanager_entry_table";
		$this->db->query($queryDeleteEntryTable);
	  while($row_process_entry_stage_table = $this->db->fetchByAssoc($result))
			{						
				$entryTableId = $row_process_entry_stage_table['id'];
				$focusObjectId = $row_process_entry_stage_table['object_id'];
				$focusObjectType = $row_process_entry_stage_table['object_type'];
				$focusObjectEvent = $row_process_entry_stage_table['object_event'];
				$GLOBALS['log']->info("Process Manager - Calling Process Manager Main Controller for object id $focusObjectId, type $focusObjectType, and event $focusObjectEvent");
				$this->startProcessManagerMainControlBlock($focusObjectId,$focusObjectType,$focusObjectEvent);
			}
        //If not OnDemand
        if(!$OnDemand) {
            $query = "Select * from pm_process_stage_waiting_todo";
            $resultStageWaitingToDo = $this->db->query($query, false);
            //Get the current time in the users time zone converted to gmt time because we use gmt time
            $timezone = date('Z') / 3600;
            $timezone = substr($timezone, 1);
            $today = gmdate('Y-m-d H:i:s');
            $GLOBALS['log']->info("Process Manager - Checking for Stages Waiting to do");
            while ($row_process_stage_waiting_todo = $this->db->fetchByAssoc($resultStageWaitingToDo)) {
                $rowId = $row_process_stage_waiting_todo['id'];
                $stage_id = $row_process_stage_waiting_todo['stage_id'];
                $process_id = $row_process_stage_waiting_todo['process_id'];
                $focusObjectId = $row_process_stage_waiting_todo['object_id'];
                $focusObjectType = $row_process_stage_waiting_todo['object_type'];
                //Check if the Focus Object is delete if so - remove from waiting todo.
                $check_deleted_query = "SELECT id FROM {$focusObjectType} WHERE deleted = 1 AND id = '{$focusObjectId}'";
                $check_deleted_res = $this->db->query($check_deleted_query);
                if ($check_deleted_row = $this->db->fetchByAssoc($check_deleted_res)) {
                    //Delete from waiting todo and move to next waiting todo
                    $this->db->query("DELETE FROM pm_process_stage_waiting_todo WHERE id = '{$rowId}'");
                    continue;
                }
                //Get the start time from the row and if we are passed time to do it then do it
                $startTime = $row_process_stage_waiting_todo['start_time'];
                $startTimeToString = strtotime($startTime);
                $todayTimeToString = strtotime($today);

                if ($todayTimeToString > $startTimeToString) {
                    //Run the stage tasks then delete the entry in the stage waiting table
                    //Set the id of the row to the stage id because the function getStageTasks is expecting a row from the stage table
                    $row_process_stage_waiting_todo['id'] = $stage_id;
                    $resultStageTaskIds = $this->getStageTasks($row_process_stage_waiting_todo);
                    $resultStageTaskIdsCounter = $this->getStageTasks($row_process_stage_waiting_todo);
                    if ($resultStageTaskIds != "") {
                        $GLOBALS['log']->info("Process Manager - Stage Waiting To Do ready to do stage tasks for process id $process_id and stage id $stage_id");
                        if ($this->checkFocusProcess()) {
                            $query = "Delete from pm_process_stage_waiting_todo where id = '" . $rowId . "'";
                            $this->db->query($query, false);
                            try {
                                $this->doStageTasks($process_id, $stage_id, $resultStageTaskIds, $resultStageTaskIdsCounter, $focusObjectId, $focusObjectType);
                            } catch (Exception $e) {
                                $errorMessage = $e->getMessage();
                                $GLOBALS['log']->info("Process Manager Error caught doing stage tasks for object type $focusObjectType and object id $focusObjectId and process id $process_id and stage id $stage_id and error message is $errorMessage");
                            }
                        }
                    }
                    //Insert into process complete table
                    $this->insertIntoProcessCompleted($focusObjectId, $process_id, $stage_id, $focusObjectType);
                }

            }
            //Check for escalation
            $db_type = $sugar_config['dbconfig']['db_type'];
            if($db_type == 'mysql'){
	            $this->performTaskEscalation();
	            $this->performCallEscalation();
			}
            //Now see if there are any processes of type 'At This Time'
            require_once('modules/PM_ProcessManager/ProcessManagerEngine2.php');
            $processManagerEngine2 = new ProcessManagerEngine2();
            $processManagerEngine2->processManagerMain2();
        }
}	


function startProcessManagerMainControlBlock($focusObjectId,$focusObjectType,$focusObjectEvent){
	global $current_user;  
	$GLOBALS['log']->info("ProcessManager - Starting the Process Manager Main Control Block for focus object id " .$focusObjectId ." and focus type " .$focusObjectType);
			$isObjectCurrentlyWaitingForTaskArray = array();
			$doesObjectHaveAnyDefaultProcessArray = array();	
			if (($focusObjectType == 'calls' && $focusObjectEvent == "modify") || ($focusObjectType == 'tasks' && $focusObjectEvent == "modify")) {
				$waitingToDoTaskId = $this->checkIfTaskHasObjectWaitingOnTask($focusObjectId,$focusObjectType);
			}		
		$this->checkObjectProcess($focusObjectId,$focusObjectType,$focusObjectEvent,true);
		return;
	
}
//***************************************************************************
//This function checks to see if there are any leads, contacts or opps
//that are associated to the call - and if so then see if there are any
//tasks waiting to do. We pass in the focus id and type which will be a call or task
//***************************************************************************

function checkIfTaskHasObjectWaitingOnTask($focusObjectId,$focusObjectType){
	$GLOBALS['log']->info("ProcessManager - Checking if object has task or call waiting on the completion of the task or call ");
	global $current_user;
	if ($focusObjectType == 'tasks') {
		$query = "Select parent_type, parent_id, contact_id, status from " .$focusObjectType ." where id = '" .$focusObjectId ."'";
	}
	else{
		$query = "Select parent_type, parent_id, status from " .$focusObjectType ." where id = '" .$focusObjectId ."'";
	}
	$result = $this->db->query($query);
	$rowTask = $this->db->fetchByAssoc($result);
	$parent_type = $rowTask['parent_type'];
	$parentID = $rowTask['parent_id'];
	//The Task Table has added contact_id but not calls so check for contacts
	if ($focusObjectType == 'tasks') {
		$contactId = $rowTask['contact_id'];
			if ($contactId != '') {
				if(($parent_type == 'Accounts') || ($parent_type == 'Contacts')){
					$parent_type = 'contacts';
					$parentID = $contactId;
				}
			}
	}
	//If focus object type is calls and parent_id is blank then we are dealing with a call for a Contact so go and get the contact id
	//From the calls_contacts table
	if ($focusObjectType == 'calls') {
		if ($parentID == '') {
			$queryCallsContacts = "select contact_id from calls_contacts where call_id = '$focusObjectId'";
			$resultCallsContacts = $this->db->query($queryCallsContacts);
			$rowCallsContacts = $this->db->fetchByAssoc($resultCallsContacts);
			$parentID = $rowCallsContacts['contact_id'];
			$parent_type = 'contacts';
		}
	}
	$isObjectCurrentlyWaitingForTaskArray = array();
	$parent_type = strtolower($parent_type);
		$isObjectCurrentlyWaitingForTaskArray = $this->checkIfObjectIsCurrentlyWaitingOnTask($parentID,$focusObjectType,$focusObjectId);
		if ($isObjectCurrentlyWaitingForTaskArray['count'] != 0) {
		//Is the task it is waiting on the completion of this call?
		//Get the previous task for this stage for this process - the array being returned is the task info
		//from the waiting table.	
			//At least one task is waiting to do something - is this task waiting on this call to be complete?
			$waitingOnId = $isObjectCurrentlyWaitingForTaskArray['waiting_on_id_1'];
			if ($isObjectCurrentlyWaitingForTaskArray['waiting_on_id_1'] == $focusObjectId) {
				//This next block is for calls
				$processID = $isObjectCurrentlyWaitingForTaskArray['process_id'];
				$stageID = $isObjectCurrentlyWaitingForTaskArray['stage_id'];
				//Patch 12/15/2009 - get TaskID and use in passing to removeTaskFromWaitingToDo
				$taskID = $isObjectCurrentlyWaitingForTaskArray['task_id_1'];
				if ($rowTask['status'] == 'Held' || $rowTask['status'] == 'Completed') {
					$previousWaitingOnCallId = $focusObjectId;
					//Get the pm_process_manager task for the next task to be created
					
					$rowTask = $this->getTask($isObjectCurrentlyWaitingForTaskArray['task_id_1']);
					//Determine what type of task it is	
					$taskType = $rowTask['task_type'];
					$rowTaskID = $rowTask['id'];
					if($taskType == 'Send Email'){
						$this->runEmailTask($isObjectCurrentlyWaitingForTaskArray['task_id_1'],$focusObjectId,$focusObjectType);
					}
                    //Legacy Code
					if($taskType == 'Schedule Call'){
						$queryTaskDefs = "select id from pm_process_task_call_defs where task_id = '$rowTaskID'";
						$resultTaskDefs = $this->db->query($queryTaskDefs);
						$rowTaskDefs = $this->db->fetchByAssoc($resultTaskDefs);
						$rowTask['calls_defs_id'] = $rowTaskDefs['id'];
						$GLOBALS['log']->info("Process Manager - Creating Schedule Call that was waiting on Call to complete for process with id $processID");
						$newTaskCallId = $this->runScheduleCallTaskThatWasWaiting($focusObjectId,$focusObjectType,$processID,$stageID,$rowTask,$parentID,$parent_type);										
					}
                    //Legacy Code
					if($taskType == 'Create Task'){
						$queryTaskDefs = "select id from pm_process_task_task_defs where task_id = '$rowTaskID'";
						$resultTaskDefs = $this->db->query($queryTaskDefs);
						$rowTaskDefs = $this->db->fetchByAssoc($resultTaskDefs);
						$rowTask['task_defs_id'] = $rowTaskDefs['id'];
						$GLOBALS['log']->info("Process Manager - Creating Sugar Task that was waiting on Task to complete for process with id $processID");
						$newTaskCallId = $this->runScheduleTaskTaskThatWasWaiting($focusObjectId,$focusObjectType,$processID,$stageID,$rowTask,$parentID,$parent_type);						
					}
					if($taskType == 'Create New Record'){
						$queryTaskDefs = "select * from pm_processmanagerstagetask where id = '$rowTaskID'";
						$resultTaskDefs = $this->db->query($queryTaskDefs);
						$rowTaskDefs = $this->db->fetchByAssoc($resultTaskDefs);
						$rowTaskDefs['start_delay_type'] = "--None--";
						require_once 'modules/PM_ProcessManager/ProcessManagerEngine1.php';
						$processManagerEngine1 = new ProcessManagerEngine1();
						$newTaskCallId = $processManagerEngine1->createRecord($processID,$stageID,$parent_type,$parentID,$rowTaskDefs);
					}															
					//Patch 12/15/2009 - pass waiting on id to removeTaskFromWaitingToDo
					$this->removeTaskFromWaitingToDo($taskID,$waitingOnId);						
				}
				//Now see if there was acutally more than one task waiting to do. If so then we need to update the 
				//task # 2 in the order with the id of the newly created call or task.
				if (($isObjectCurrentlyWaitingForTaskArray['count'] > 1) && ($newTaskCallId != '')) {
					//Update task_id_2 with the new id
					$taskId = $isObjectCurrentlyWaitingForTaskArray['task_id_2'];															
					$queryUpdateTaskWaitingToDo = "Update pm_process_task_waiting_todo set waiting_on_id = '" .$newTaskCallId ."' where object_id = '" .$parentID ."' and process_id = '$processID' and stage_id = '$stageID' ";
					$this->db->query($queryUpdateTaskWaitingToDo);
				}
				
			}
		
		//This means that there is more than one task waiting to do on completion of the previous task
		//But since the new task or call has not been created we dont know the task or call id - so
		//we need to update the process task waiting to do and place the new id in the field
	
		}	
}
//*************************************************************************
//Remove a task from the waiting to do table
//Patch 12/15/2009 - Update this function to include waiting on id
//************************************************************************
function removeTaskFromWaitingToDo($taskID,$waitingOnId){
	$query = "Delete from pm_process_task_waiting_todo where task_id = '" .$taskID ."' and waiting_on_id = '$waitingOnId' ";
	$result = $this->db->query($query);
}

//**************************************************************************
//This function is a helper function that gets the row from the task table
//passed info is task id
//*************************************************************************

function getTask($taskID){
	$query = "Select * from pm_processmanagerstagetask where id = '" .$taskID ."'";
	$result = $this->db->query($query);
	$rowTask = $this->db->fetchByAssoc($result);
	return $rowTask;
}

//****************************************************************************
//This function is called by Main Control Block to see if the 
//object id object type combination is currently in process - if so then
//we return an array with either 0 in count field or 1 or more process id's
//****************************************************************************
//Patch 12/08/2009 - enable mixing of both calls and task for feature - From Completion of Previous Tasks
//Remove Task_Type from the query

function checkIfObjectIsCurrentlyWaitingOnTask($parentID,$focusObjectType,$focusObjectId){
	$GLOBALS['log']->info("Process Manager - Checking to see if object is currently waiting on a task to complete for parent id $parentID");
	$resultArray = array();
	$query = "Select process_id, stage_id, task_id, waiting_on_id, task_order from pm_process_task_waiting_todo where object_id = '" .$parentID ."' and waiting_on_id = '$focusObjectId' order by task_order ASC ";
	$result = $this->db->query($query,true);	
	//Are there more than one process's currently running against the object?
	$counter = 1;
		while($rowCurrentInProcess = $this->db->fetchByAssoc($result))
			{			
				$process_id = 'process_id';
				$stage_id = 'stage_id';
				$task_id = 'task_id_' .$counter;
				$waiting_on_id = 'waiting_on_id_' .$counter;
				$resultArray[$process_id] = $rowCurrentInProcess['process_id'];
				$resultArray[$stage_id] = $rowCurrentInProcess['stage_id'];
				$resultArray[$task_id] = $rowCurrentInProcess['task_id'];
				$resultArray[$waiting_on_id] = $rowCurrentInProcess['waiting_on_id'];
				$counter = $counter + 1;
				
			}
		if ($counter == 1) {
			$resultArray['count'] = 0;
		}
		else{		
			$resultArray['count'] = $counter;
		}
		return $resultArray;	
}


	function checkObjectProcess($focusObjectId,$focusObjectType,$focusObjectEvent,$isDefault){	
		$GLOBALS['log']->info("Process Manager - Begin Check Object Process for focus type $focusObjectType and focus id $focusObjectId");	
		global $current_user;	
	  require_once('modules/PM_ProcessManager/ProcessManagerEngine1.php');
	  require_once('modules/PM_ProcessManager/ProcessManagerEngine2.php');
		$processManagerEngine1 = new ProcessManagerEngine1();
		$processManagerEngine2 = new ProcessManagerEngine2();
		$checkDefaultProcess = false;
		$resultStageTaskIds = array();
		//If the focus object event is an insert then we look for a create event
		if ($focusObjectEvent == 'insert') {
			$query = "SELECT id, related_object FROM pm_processmanager WHERE process_object = '" .$focusObjectType ."'";
			$query .="  and (start_event = 'Create' OR start_event = 'Create or Modify') and status = 'Active' and deleted = 0";
			$result = $this->db->query($query);
			//If we have a default process then first make sure we have not already done the process
			//If not done then kick it off
			while($row = $this->db->fetchByAssoc($result)){	
				//Patch 12-17-2011 The and-or filter fields array were not correctly in the while loop for each process
				//For each process start off with an empty and or filter fields array
				$process_id = $row['id'];
				//PM Pro V2
				$related_object = $row['related_object'];
				$isDefaultProcessAlreadyDone = $this->checkIfDefaultProcessAlreadyDone($focusObjectId,$process_id,$this);
  			if (!$isDefaultProcessAlreadyDone) {		
					$this->checkFilterFields($process_id,$related_object, $focusObjectId, $focusObjectType);
				}
		}
	}
	//Event is an update so see if there is a process for Modify for the given object
	else{		
		$processManagerEngine1->processManagerMain1($focusObjectId,$focusObjectType,$focusObjectEvent,$isDefault,$this);
	}
}


//**************************************************************************************************
//This process checks to see if the default process for the object/event
//has already been done and if not then we kick it off. We also check
//**************************************************************************************************

function checkIfDefaultProcessAlreadyDone($focusObjectId,$process_id,$thisPM){
	$GLOBALS['log']->info("Process Manager - checking if default process is already done for process id $process_id and object id $focusObjectId");
	$queryProcessAlreadyDone = "Select id from pm_process_completed_process where object_id = '" .$focusObjectId ."' and process_id = '";
	$queryProcessAlreadyDone .= $process_id ."' and process_complete = 1";
	$resultProcessComplete = $thisPM->db->query($queryProcessAlreadyDone);
	$rowProcessComplete = $thisPM->db->fetchByAssoc($resultProcessComplete);
	if ($rowProcessComplete) {
		return true;
	}
	else {
        //Could be that the object is only in the stage waiting to do
        $qstageWait = "select id from pm_process_stage_waiting_todo where object_id = '$focusObjectId' and process_id = '$process_id'";
        $resStageWait = $thisPM->db->query($qstageWait);
        $rowStageWait = $thisPM->db->fetchByAssoc($resStageWait);
        if ($rowStageWait) {
            return true;
        }
        else{
            return false;
        }
	}
}

//*********************************************************************
//This function inserts an entry into the pm_process_completed_process
//table such that we know that we have completed this process and we dont
//need to do it again
//*********************************************************************

function insertIntoProcessCompleted($focusObjectId,$process_id, $stage_id, $focusObjectType)
{
    global $sugar_config;
    $GLOBALS['log']->info("Process Manager - inserting into process completed table for and object id $focusObjectId and process id $process_id");
    //First check to make sure we have not already inserted this record into the table
    $qc = "select id from pm_process_completed_process where object_id = '$focusObjectId' and process_id = '$process_id' and stage_id = '$stage_id' and object_type = '$focusObjectType'";
    $qcr = $this->db->query($qc);
    if ($this->db->getAffectedRowCount($qcr) == 0) {
        $id = create_guid();
        $today = date("Y-m-d H:i:s");
        $db_type = $sugar_config['dbconfig']['db_type'];
  			if($db_type == 'mysql'){
        	$query = "Insert into pm_process_completed_process (`id`,`object_id`,`process_id`,`process_complete`,`process_complete_date`,`stage_id`,`object_type`) VALUES('$id','$focusObjectId','$process_id','1','$today', '$stage_id', '$focusObjectType') ";
        }
  			if($db_type == 'mssql'){
        	$query = "Insert into pm_process_completed_process (id,object_id,object_type,process_id,stage_id,task_id,process_complete, process_complete_date) "
          . " VALUES ('$id', '$focusObjectId','$focusObjectType','$process_id','','','1','$today')";
        }        
        $this->db->query($query);
    }
	
}

//*********************************************************************************
//This function loads a delayed stage into the table pm_process_stage_waiting_todo
//We need to determine if the stage is tied to a process that is a create event or
//a non create event - this will be the key to how we set the start_time field
//**********************************************************************************

function loadDelayedStage($focusObjectId,$focusObjectType,$process_id,$row_stage){
	global $sugar_config;
	$stageWaitingId = create_guid();
	$focusFieldsArray = array();
	$focusFieldsArray['date_entered'] = 'date_entered';
	$focusFieldsArray['date_modified'] = 'date_modified';
	$arrayFieldsFromFocusObject = $this->getFocusObjectFields($focusObjectId,$focusObjectType,$focusFieldsArray);
	$focusObjectCreateDate = $arrayFieldsFromFocusObject['date_entered'];
	$focusObjectModifiedDate = $arrayFieldsFromFocusObject['date_modified'];
	
	//Now find out if the process is a create or non create
	$processCreateModifyType = $this->getProcessCreateModifyType($process_id);	
	if ($processCreateModifyType == "Create") {		
		$calculatedStartTime = $this->getNewDateTimeStartDelayedStage($focusObjectCreateDate,$row_stage['start_delay_days'],$row_stage['start_delay_hours'],$row_stage['start_delay_minutes'],$row_stage['start_delay_months'],$row_stage['start_delay_years']);
	}
	else{
		$calculatedStartTime = $this->getNewDateTimeStartDelayedStage($focusObjectModifiedDate,$row_stage['start_delay_days'],$row_stage['start_delay_hours'],$row_stage['start_delay_minutes'],$row_stage['start_delay_months'],$row_stage['start_delay_years']);
	}
  $stid = $row_stage['id'];
	$db_type = $sugar_config['dbconfig']['db_type'];
  if($db_type == 'mysql'){
		$query = "Insert into pm_process_stage_waiting_todo (`id`, `object_id`, `object_type`, `process_id`, `stage_id`, `start_delay_type`, `start_time` ) VALUES ('$stageWaitingId', '$focusObjectId','$focusObjectType','$process_id', '$stid', 'Create', '$calculatedStartTime')";
	}
  if($db_type == 'mssql'){
		$query = "Insert into pm_process_stage_waiting_todo (id,object_id,object_type,process_id,stage_id,start_delay_type,start_time) "
          . " VALUES ('$stageWaitingId', '$focusObjectId','$focusObjectType','$process_id','$stid','Create','$calculatedStartTime')";
	}	
	$result = $this->db->query($query); 
	
}

//******************************************************************
//This little function will return the process create/modify type
//for the LoadDelaytedStage
//***************************************************************
function getProcessCreateModifyType($process_id){
		$query = "Select start_event from pm_processmanager where id = '" .$process_id ."'";
		$result = $this->db->query($query);
		$rowProcess = $this->db->fetchByAssoc($result);
		$processStartType = $rowProcess['start_event']; 
		return $processStartType;

}

//**********************************************************************
//This function is called to retrieve the stages for the given process's
//**********************************************************************
	function getProcessStages($processID){
		$query = "Select pm_processmanagerstage_idb from pm_processmmanagerstage where pm_processmanager_ida = '" .$processID ."' and deleted = 0";
		$result = $this->db->query($query);
		$num_rows_result = count($result);	
		if ($num_rows_result == 0) {
			//There are no stages so do nothing
			return null;
			}
		else{
			return $result;			
		}
		
	}
//*************************************************************************************
//This function will take in the result list of all the stage id for the process and we
//are going to get all the stages ordered by stage_orders
//*************************************************************************************
function getOrderedStages($result,$resultCount){
    if($resultCount == 0)
        return '';
	$queryStageOrder1 = "Select * from pm_processmanagerstage where id = ";
	$counter = 1;
	while($row_stage_list = $this->db->fetchByAssoc($result))
		{
			$queryStageOrder1 .= "'";
			$stage_id = $row_stage_list["pm_processmanagerstage_idb"];
			if($counter < $resultCount ){
				$queryStageOrder1 .= $stage_id ."'  or id = ";
			}
			else{
				$queryStageOrder1 .= $stage_id ."' order by stage_order ASC";
			}
			$counter = $counter + 1;
		}
	$resultOrderedStages = $this->db->query($queryStageOrder1);
	$num_rows_result_stage_1 = count($resultOrderedStages);
	if ($num_rows_result_stage_1 == 0) {
			//There is no stage 1 - so exit
			return '';
		}
	else{
		return $resultOrderedStages;
	}
}


function getStageTasks($row_stage_1){
		$stageId = $row_stage_1['id'];
		$query = "Select pm_processmanagerstagetask_idb from pm_processmgerstagetask where pm_processmanagerstage_ida = '" .$stageId ."' and deleted = 0";
		$result = $this->db->query($query);
		$num_rows_result = count($result);	
		if ($num_rows_result == 0) {
			//There are no stages so do nothing
			$result = "";
			return $result;
			}
		else{
			return $result;			
		}
}

//**************************************************************************
//This function is passed in a result set of task ids and is the call
//to do these tasks
//**************************************************************************

function doStageTasks($processID, $stageID, $resultStageTaskIds,$resultStageTaskIdsCounter,$focusObjectId,$focusObjectType){
    $num_rows_result = 0;
    while($row = $this->db->fetchByAssoc($resultStageTaskIdsCounter)){
    	$num_rows_result = $num_rows_result + 1;
    }
	global $current_user;
	//If there are more than one task then we need to order the tasks
	$taskTableQuery = "Select * from pm_processmanagerstagetask where id = '";
	$counter = 1;
	if ($num_rows_result == 0){
		return;
	}	
	if ($num_rows_result == 1) {
		$row_task_id = $this->db->fetchByAssoc($resultStageTaskIds);
		$taskTableQuery .= $row_task_id['pm_processmanagerstagetask_idb'];
		$taskTableQuery .= "'";
	}
	else{	
		while($row_task_id = $this->db->fetchByAssoc($resultStageTaskIds))
		{
			//$row_task_id = $this->db->fetchByAssoc($resultStageTaskIds);		
			if($counter < $num_rows_result ){
				$taskTableQuery .= $row_task_id['pm_processmanagerstagetask_idb'] ."' or id ='";
			}
			else{
				$taskTableQuery .= $row_task_id['pm_processmanagerstagetask_idb'] ."' ORDER by task_order ASC";
			}
			$counter = $counter + 1;
		}
		
	}
	$result = $this->db->query($taskTableQuery);
	//Now we have all the tasks in order - so get the first row and do the task - whatever it is
	while($rowTask = $this->db->fetchByAssoc($result))
		{
				$this->runTask($processID,$stageID,$rowTask,$focusObjectId,$focusObjectType);
				unset($rowTask);
		}

}



//*****************************************************************************
//This function is called with a single row from the task table
//and is the function that will actually do the task
//*****************************************************************************

function runTask($processID,$stageID,$rowTask,$focusObjectId,$focusObjectType){
  global $sugar_config;
  $sugar_config['default_limit'] = "5000";
  require_once('modules/PM_ProcessManager/ProcessManagerEngine1.php');
  require_once('modules/PM_ProcessManager/ProcessManagerEngine2.php');
  $GLOBALS['log']->info("Process Manager - Starting the runTask function for object type $focusObjectType and object id $focusObjectId and process id $processID and stage id $stageID");
	require_once('modules/Users/User.php');
	global $current_user;
	$rowUser = $this->getFocusOwner($focusObjectType,$focusObjectId);
	//For version 4.5.1 we get the user preferences from the user_preference table
	$userId = $rowUser['id'];
	$rowUserPreferenceContents = $this->getUserPreferenceRow($userId);
	$current_user->id = $rowUser['id'];
	$current_user->user_name = $rowUser['user_name'];
	$user_name = $rowUser['user_name'];
	$_SESSION[$current_user->user_name . '_PREFERENCES']['global'] = unserialize(base64_decode($rowUserPreferenceContents));				
	$current_user->user_preferences['global'] = unserialize(base64_decode($rowUserPreferenceContents));	
	if ($rowTask['task_type'] == 'Send Email') {
		$taskId = $rowTask['id'];
		$this->runEmailTask($taskId,$focusObjectId,$focusObjectType);
	}
	if ($rowTask['task_type'] == 'Create Task') {
		$this->runCreateTask($processID,$stageID,$rowTask,$focusObjectId,$focusObjectType);
	}
	if ($rowTask['task_type'] == 'Create Project Task') {
	    $this->runCreateProjectTask($processID,$stageID,$rowTask,$focusObjectId,$focusObjectType);
	}
	if ($rowTask['task_type'] == 'Schedule Call') {
	    $this->runScheduleCallTask($processID,$stageID,$rowTask,$focusObjectId,$focusObjectType);
	}
	if ($rowTask['task_type'] == 'Custom Script') {
	    $this->runCustomScript($processID,$stageID,$rowTask,$focusObjectId,$focusObjectType);
	}
	if ($rowTask['task_type'] == 'Schedule Meeting') {
		$this->runScheduleMeetingTask($processID,$stageID,$rowTask,$focusObjectId,$focusObjectType);
	}
    if ($rowTask['task_type'] == 'Create New Record') {
	    $processManagerEngine1 = new ProcessManagerEngine1();
		$processManagerEngine1->createRecord($processID,$stageID,$focusObjectType,$focusObjectId,$rowTask);
	}
	if ($rowTask['task_type'] == 'Modify Object Field') {
	    $processManagerEngine2 = new ProcessManagerEngine2();
	    $processManagerEngine2->updateObjectField($processID,$focusObjectType,$focusObjectId,$rowTask);
	}
	if ($rowTask['task_type'] == 'Convert Lead') {
	    $processManagerEngine2 = new ProcessManagerEngine2();
	    $processManagerEngine2->convertLead($focusObjectType,$focusObjectId,$rowTask);
	}
    if ($rowTask['task_type'] == 'REST Service') {
        $processManagerEngine2 = new ProcessManagerEngine2();
        $processManagerEngine2->runRestService($focusObjectType,$focusObjectId,$rowTask);
    }
    if ($rowTask['task_type'] == 'Trigger Process') {
        $newProcessName = $rowTask['trigger_process_name'];
    	$getNewProcessQuery = "SELECT id, related_object FROM pm_processmanager WHERE deleted = 0 AND name ='$newProcessName'";
    	$getNewProcessRes = $this->db->query($getNewProcessQuery);
        $newProcessRow = $this->db->fetchByAssoc($getNewProcessRes);
        $newProcessID = $newProcessRow['id'];
        $newRelatedObject = $newProcessRow['related_object'];
    	$this->checkFilterFields($newProcessID, $newRelatedObject, $focusObjectId, $focusObjectType);
    	return;
	}	
	if ($rowTask['task_type'] == 'Modify Related Object Field') {
	    $processManagerEngine2 = new ProcessManagerEngine2();
        $processManagerEngine2->updateRelatedObjectFields($focusObjectType,$focusObjectId,$rowTask);
	}
  if ($rowTask['task_type'] == 'Modify Related Field From Related Object') {
      $processManagerEngine2 = new ProcessManagerEngine2();
      $processManagerEngine2->updateRelatedObjectFieldsFromRelatedObject($focusObjectType,$focusObjectId,$rowTask);
  }	
	if ($rowTask['task_type'] == 'Route Object') {
        $processManagerEngine2 = new ProcessManagerEngine2();
        $getRouteObjectDef = "SELECT * FROM pm_process_task_routing_defs WHERE task_id = '{$rowTask['id']}'";
        $resRouteObjectDef = $this->db->query($getRouteObjectDef);
        if($rowRouteObjectDef = $this->db->fetchByAssoc($resRouteObjectDef)){
            $rowRouteObjectDef['process_id'] = $processID;
            $processManagerEngine2->routeObject($rowRouteObjectDef,$focusObjectId,$focusObjectType);
        }
	}
	
    if ($rowTask['task_type'] == 'Send Sugar Notification') {
        $processManagerEngine2 = new ProcessManagerEngine2();
        $processManagerEngine2->sendSugarNotifications($focusObjectType,$focusObjectId,$rowTask);
	}	
	
}
//****************************************************************************
//This function returns the row from the user table for the focus owner
//****************************************************************************

function getFocusOwner($focusObjectType,$focusObjectId){
	if($focusObjectType == 'activities'){
		return;
	}	
	$query = "Select assigned_user_id from " .$focusObjectType ." where id = '" .$focusObjectId ."'";
	$result = $this->db->query($query, true);
	$rowLeads = $this->db->fetchByAssoc($result);
	
	$assigned_user_id = $rowLeads['assigned_user_id'];
	
	$queryUsers = "Select id, user_name from users where id = '" .$assigned_user_id ."'";
	$result = $this->db->query($queryUsers, true);
	$rowUser = $this->db->fetchByAssoc($result);
	
	return $rowUser;
	
}

//****************************************************************************
//This function returns the row from the user table for the focus owner
//****************************************************************************

function getUserPreferenceRow($userId){
	$query = "Select contents from user_preferences where assigned_user_id = '" . $userId ."' and category = 'global'";
	$result = $this->db->query($query, true);
	$rowUserPreference = $this->db->fetchByAssoc($result);
	$contents = $rowUserPreference['contents'];
	return $contents;
	
}

//******************************************************************************
//This function will retrieve the email address and id from the contact table
//for the given opp. 
//*****************************************************************************

function getContactOppEmails($focusObjectId,$focusObjectType,$contact_role){
	$query_opps_contacts = "Select contact_id from opportunities_contacts where opportunity_id  = ";
	$query_opps_contacts .= "'";
	$query_opps_contacts .= $focusObjectId;
	$query_opps_contacts .= "' and deleted = 0";
	if ($contact_role == '') {
		$query_opps_contacts .= " and contact_role IS NULL ";
	}
	
	$result_opps_contacts =& $this->db->query($query_opps_contacts, true);
	$row = $this->db->fetchByAssoc($result_opps_contacts);
	if ($row) {
		$queryContact = "Select id from contacts where id = '" .$row['contact_id'] ."'";
		$resultContacts = $this->db->query($queryContact, true);
		$rowContact = $this->db->fetchByAssoc($resultContacts);
		if ($rowContact) {
			return $rowContact;
		}
	}
	
}

//******************************************************************************
//This function will retrieve the email address and id from the account table
//for the given case
//*****************************************************************************

function getAccountEmailForCases($focusObjectId,$focusObjectType){
	$query_accounts_cases = "Select account_id from cases where id  = ";
	$query_accounts_cases .= "'";
	$query_accounts_cases .= $focusObjectId;
	$query_accounts_cases .= "'";
	$result_accounts_cases = $this->db->query($query_accounts_cases, true);
	$row = $this->db->fetchByAssoc($result_accounts_cases);
	return $row;
	
}

//*************************************************************************
//This function runs a custom script created by the end user
//This script lives in the ProcessManager folder called customScripts
//*************************************************************************

function runCustomScript($processID,$stageID,$rowTask,$focusObjectId,$focusObjectType){
	$GLOBALS['log']->info("Process Manager - Calling the custom script for object type $focusObjectType and object id $focusObjectId and  process id $processID and stage id $stageID");	
	//First get the name of the script
	$scriptName = $rowTask['custom_script'];
	if(!empty($scriptName)){
        if (file_exists("modules/PM_ProcessManager/customScripts/$scriptName")) {
            require_once("modules/PM_ProcessManager/customScripts/$scriptName");
            $scriptName = str_replace(".php","",$scriptName);
            $customScript = new $scriptName($focusObjectId,$focusObjectType);
        }
    }
}

//***************************************************************************
//This is the schedule call task that was waiting
//Patch 12/10/2009 - pass $previousFocusObjectType
//**************************************************************************
function runScheduleCallTaskThatWasWaiting($previousWaitingOnCallId,$previousFocusObjectType,$processID,$stageID,$rowTask,$focusObjectId,$focusObjectType){
		//Go and get the defs record from pm_process_task_call_defs
		global $current_user;
		//Patch 08-31-2011 to fix the issue with the assigned user id for the call to be created
		$rowUser = $this->getFocusOwner($focusObjectType,$focusObjectId);	
		//For version 4.5.1 we get the user preferences from the user_preference table
		$userId = $rowUser['id'];
		$rowUserPreferenceContents = $this->getUserPreferenceRow($userId);
		$current_user->id = $rowUser['id'];
		$current_user->user_name = $rowUser['user_name'];
		$user_name = $rowUser['user_name'];
		$_SESSION[$current_user->user_name . '_PREFERENCES']['global'] = unserialize(base64_decode($rowUserPreferenceContents));				
		$current_user->user_preferences['global'] = unserialize(base64_decode($rowUserPreferenceContents));			
		$taskId = $rowTask['id'];
		$queryTaskCallDefs = "Select * from pm_process_task_call_defs where task_id = '" .$taskId ."'";
		$resultTaskCallDefs = $this->db->query($queryTaskCallDefs);
		$rowTaskCallDefs = $this->db->fetchByAssoc($resultTaskCallDefs);
		if ($rowTaskCallDefs) {
			//Calculate the start time which is entered into the table as date_start and time_start
			//The edit view only allows times to show at 00,15,39,45 minutes after the hours
			//The field start_delay_type will hold the type of delay - from object creation or previous
			//task complete.
				//Get the create date from the function that gets focus fields
				$focusFieldsArray = array();
				$focusFieldsArray['date_modified'] = 'date_modified';
				//Patch 12/10/2009 replace "calls" with $previousFocusObjectType
				$arrayFieldsFromFocusObject = $this->getFocusObjectFields($previousWaitingOnCallId,$previousFocusObjectType,$focusFieldsArray);
				$focusObjectCreateDate = $arrayFieldsFromFocusObject['date_modified'];
				//If the task start_delay_days =  0 then we use the focus object create date for the call 
				//date_start.
	
				$newCallTimeStart = $this->getNewCallTimeStart($focusObjectCreateDate,$rowTaskCallDefs['start_delay_years'],$rowTaskCallDefs['start_delay_months'],$rowTaskCallDefs['start_delay_days'],$rowTaskCallDefs['start_delay_hours'],$rowTaskCallDefs['start_delay_minutes']);

				$newCallTaskId = $this->createNewCallTask($focusObjectId,$focusObjectType,$rowTaskCallDefs,$newCallTimeStart,$focusObjectCreateDate);
			
			//If this task is to be done when the previous task is complete then queue it up
			return $newCallTaskId;
		}
		
	}
	
//***************************************************************************
//This is the schedule task task that was waiting
//**************************************************************************
function runScheduleTaskTaskThatWasWaiting($focusObjectID,$focusObjectTypeCallLead,$processID,$stageID,$rowTask,$focusObjectId,$focusObjectType){
		//Go and get the defs record from pm_process_task_call_defs
		global $current_user;
		//Patch 08-31-2011 to fix the issue with the assigned user id for the call to be created
		$rowUser = $this->getFocusOwner($focusObjectType,$focusObjectId);	
		//For version 4.5.1 we get the user preferences from the user_preference table
		$userId = $rowUser['id'];
		$rowUserPreferenceContents = $this->getUserPreferenceRow($userId);
		$current_user->id = $rowUser['id'];
		$current_user->user_name = $rowUser['user_name'];
		$user_name = $rowUser['user_name'];
		$_SESSION[$current_user->user_name . '_PREFERENCES']['global'] = unserialize(base64_decode($rowUserPreferenceContents));				
		$current_user->user_preferences['global'] = unserialize(base64_decode($rowUserPreferenceContents));					
		$taskId = $rowTask['id'];
		$queryTaskTaskDefs = "Select * from pm_process_task_task_defs where task_id = '" .$taskId ."'";
		$resultTaskTaskDefs = $this->db->query($queryTaskTaskDefs);
		$rowTaskTaskDefs = $this->db->fetchByAssoc($resultTaskTaskDefs);
		if ($rowTaskTaskDefs) {
			//Calculate the start time which is entered into the table as date_start and time_start
			//The edit view only allows times to show at 00,15,39,45 minutes after the hours
			//The field start_delay_type will hold the type of delay - from object creation or previous
			//task complete.
				//Get the create date from the function that gets focus fields
				$focusFieldsArray = array();
				$focusFieldsArray['date_modified'] = 'date_modified';
				//Now was the previous task a task or call? We need to know this to get the date modified
				
				if ($focusObjectTypeCallLead == 'calls') {
					$arrayFieldsFromFocusObject = $this->getFocusObjectFields($focusObjectID,"calls",$focusFieldsArray);
					$focusObjectCreateDate = $arrayFieldsFromFocusObject['date_modified'];
				}
				if ($focusObjectTypeCallLead == 'tasks'){
					$arrayFieldsFromFocusObject = $this->getFocusObjectFields($focusObjectID,"tasks",$focusFieldsArray);
					$focusObjectCreateDate = $arrayFieldsFromFocusObject['date_modified'];
				}
				
				//If the task start_delay_days =  0 then we use the focus object create date for the call 
				//date_start.
	
				$newCallTimeStart = $this->getNewCallTimeStart($focusObjectCreateDate,$rowTaskTaskDefs['due_date_delay_years'],$rowTaskTaskDefs['due_date_delay_months'],$rowTaskTaskDefs['due_date_delay_days'],$rowTaskTaskDefs['due_date_delay_hours'],$rowTaskTaskDefs['due_date_delay_minutes']);
				$newTaskTaskId = $this->createNewTaskTask($focusObjectId,$focusObjectType,$rowTaskTaskDefs,$newCallTimeStart);
							
			//If this task is to be done when the previous task is complete then queue it up
			return $newTaskTaskId;
		}
		
	}	
//***************************************************************************
//This is the create a new task.
//**************************************************************************
function runCreateTask($processID,$stageID,$rowTask,$focusObjectId,$focusObjectType){
		//Go and get the defs record from pm_process_task_task_defs
		global $current_user;
		$taskId = $rowTask['id'];
		$GLOBALS['log']->info("Process Manager - Creating a new Sugar Task for object type $focusObjectType and object id $focusObjectId and task id $taskId and process id $processID and stage id $stageID");	
		$taskOrder = $rowTask['task_order'];
		$queryTaskTaskDefs = "Select * from pm_process_task_task_defs where task_id = '" .$taskId ."'";
		$resultTaskTaskDefs = $this->db->query($queryTaskTaskDefs);
		$rowTaskTaskDefs = $this->db->fetchByAssoc($resultTaskTaskDefs);
		if ($rowTaskTaskDefs) {
			//Calculate the due datetime which is entered into the table as date_start and time_start
			//If there is no delay then there is no due date so just create the task
			//If this task is to be done when the previous task is complete then queue it up
			if ($rowTaskTaskDefs['due_date_delay_type'] == 'From Completion of Previous Task'){	
				$this->insertTaskIntoWaitingTable($processID,$stageID,$focusObjectId,$focusObjectType,$taskId,$taskOrder,'tasks');
			    return;
			}
			//Here means that it is a create or modify delay so go and get either date
			$delay_type = $rowTaskTaskDefs['due_date_delay_type'];
			$focusFieldsArray = array();
			if ($delay_type == "Create") {
				$focusFieldsArray['date_entered'] = 'date_entered';
				$arrayFieldsFromFocusObject = $this->getFocusObjectFields($focusObjectId,$focusObjectType,$focusFieldsArray);
				$focusObjectCreateDate = $arrayFieldsFromFocusObject['date_entered'];
			}
			else{
				$focusFieldsArray['date_modified'] = 'date_modified';
				$arrayFieldsFromFocusObject = $this->getFocusObjectFields($focusObjectId,$focusObjectType,$focusFieldsArray);
				$focusObjectCreateDate = $arrayFieldsFromFocusObject['date_modified'];
			}			
				//If the task start_delay_days =  0 then we use the focus object create date for the call 
				//date_start.
				$newCallTimeStart = $this->getNewCallTimeStart($focusObjectCreateDate,$rowTaskTaskDefs['due_date_delay_years'],$rowTaskTaskDefs['due_date_delay_months'],$rowTaskTaskDefs['due_date_delay_days'],$rowTaskTaskDefs['due_date_delay_hours'],$rowTaskTaskDefs['due_date_delay_minutes']);
				$this->createNewTaskTask($focusObjectId,$focusObjectType,$rowTaskTaskDefs,$newCallTimeStart);			
		}
		
	}
//***************************************************************************
//This is the create a new Project task.
//**************************************************************************
function runCreateProjectTask($processID,$stageID,$rowTask,$focusObjectId,$focusObjectType){
		//Go and get the defs record from pm_process_task_task_defs
		global $current_user;
		$taskId = $rowTask['id'];
		$GLOBALS['log']->info("Process Manager - Creating a new Project Task for object type $focusObjectType and object id $focusObjectId and task id $taskId and process id $processID and stage id $stageID");	
		$queryProjectTaskDefs = "Select * from pm_process_project_task_defs where project_id = '" .$taskId ."'";
		$resultProjectTaskDefs = $this->db->query($queryProjectTaskDefs);
		$rowProjectTaskDefs = $this->db->fetchByAssoc($resultProjectTaskDefs);
		$focusFieldsArray = array();
		$focusFieldsArray['date_modified'] = 'date_modified';
		$arrayFieldsFromFocusObject = $this->getFocusObjectFields($focusObjectId,$focusObjectType,$focusFieldsArray);
		$focusObjectCreateDate = $arrayFieldsFromFocusObject['date_modified'];
		$newCallTimeStart = $this->getNewCallTimeStart($focusObjectCreateDate,0,0,0,0,0);
		if ($rowProjectTaskDefs) {
				$this->createNewProjectTask($focusObjectId,$focusObjectType,$rowProjectTaskDefs,$newCallTimeStart);			
		}
		
}
//***************************************************************************
//This is the schedule call task
//**************************************************************************
function runScheduleCallTask($processID,$stageID,$rowTask,$focusObjectId,$focusObjectType){
		//Go and get the defs record from pm_process_task_call_defs
		global $current_user;
		global $previousTaskId;
		$taskId = $rowTask['id'];
		$GLOBALS['log']->info("Process Manager - Creating a new Scheduled Call for object type $focusObjectType and object id $focusObjectId and task id $taskId and process id $processID and stage id $stageID");	
		$taskOrder = $rowTask['task_order'];
		$queryTaskCallDefs = "Select * from pm_process_task_call_defs where task_id = '" .$taskId ."'";
		$resultTaskCallDefs = $this->db->query($queryTaskCallDefs);
		$rowTaskCallDefs = $this->db->fetchByAssoc($resultTaskCallDefs);
		if ($rowTaskCallDefs) {
			//Calculate the start time which is entered into the table as date_start and time_start
			//The edit view only allows times to show at 00,15,39,45 minutes after the hours
			//The field start_delay_type will hold the type of delay - from object creation or previous
			//task complete.
			if($rowTaskCallDefs['start_delay_type'] == 'From Completion of Previous Task'){	
				$this->insertTaskIntoWaitingTable($processID,$stageID,$focusObjectId,$focusObjectType,$taskId,$taskOrder,'calls');
				return;
			}			
			//Get the delay type
			$focusFieldsArray = array();
			$delay_type = $rowTaskCallDefs['start_delay_type'];
			if ($rowTaskCallDefs['start_delay_type'] == 'Create') {
				$focusFieldsArray['date_entered'] = 'date_entered';
				$arrayFieldsFromFocusObject = $this->getFocusObjectFields($focusObjectId,$focusObjectType,$focusFieldsArray);
				$focusObjectCreateDate = $arrayFieldsFromFocusObject['date_entered'];
			}
			else{
				$focusFieldsArray['date_modified'] = 'date_modified';
				$arrayFieldsFromFocusObject = $this->getFocusObjectFields($focusObjectId,$focusObjectType,$focusFieldsArray);
				$focusObjectCreateDate = $arrayFieldsFromFocusObject['date_modified'];
			}			
				$newCallTimeStart = $this->getNewCallTimeStart($focusObjectCreateDate,$rowTaskCallDefs['start_delay_years'],$rowTaskCallDefs['start_delay_months'],$rowTaskCallDefs['start_delay_days'],$rowTaskCallDefs['start_delay_hours'],$rowTaskCallDefs['start_delay_minutes']);
				$this->createNewCallTask($focusObjectId,$focusObjectType,$rowTaskCallDefs,$newCallTimeStart,$focusObjectCreateDate);			
		}		
	}

//**************************************************************************
//This function inserts a task into the task waiting table/queue.
//This table is the pm_process_task_waiting_todo
//These are the tasks that are waiting to do something based on an event
//happening against an object.
//**************************************************************************

function insertTaskIntoWaitingTable($processID,$stageID,$focusObjectId,$focusObjectType,$taskId,$taskOrder,$taskType){
	global $previousTaskId;
	global $sugar_config;
    $db_type = $sugar_config['dbconfig']['db_type'];
	$newTaskWaitingTodoId = create_guid();
	if($db_type == 'mysql'){
		$newTaskWaitingTodoQuery = "Insert into pm_process_task_waiting_todo (`id`,`object_id`,`object_type`,`task_id`,`process_id`,`stage_id`,`waiting_on_id`,`task_order`) VALUES ('$newTaskWaitingTodoId','$focusObjectId','$focusObjectType','$taskId','$processID','$stageID','$previousTaskId','$taskOrder')";
	}
	if($db_type == 'mssql'){
		$query = "Insert into pm_process_task_waiting_todo (id,object_id,object_type,task_id,process_id,stage_id,waiting_on_id,task_order,task_type) "
           . " VALUES ('$newTaskWaitingTodoId', '$focusObjectId','$focusObjectType','$taskId','$processID','$stageID','$previousTaskId',$taskOrder,'$taskType')";
	}	
	$this->db->query($newTaskWaitingTodoQuery);
}

//***************************************************************************
//This function loads a new task into the task table
//***************************************************************************

function createNewTaskTask($focusObjectId,$focusObjectType,$rowTaskTaskDefs,$dueDate){
	require_once('modules/PM_ProcessManager/ProcessManagerEngine2.php');
	$processManagerEngine2 = new ProcessManagerEngine2();
	global $previousTaskId;
	global $current_user;
	$current_user->name = "SugarCRM Administrator";
	$newTask = new Task();
	$dbTable = $focusObjectType;
	//Mods to fix date start issue
	$timedate=new TimeDate();
	$timezone = date('Z') / 3600;
	$timezone = substr($timezone,1);
	$today = date('Y-m-d H:i:s', time() + $timezone * 60 * 60);
	$newCallUserid = create_guid();
	if ($dueDate != '') {
		//First parse the start time
		$spaceLocation = strpos($dueDate," ");
		$taskDueDate = substr($dueDate,0,$spaceLocation);
		$taskDueTime = substr($dueDate,$spaceLocation);
	}
	else{
		$taskDueDate = "000-00-00";
		$taskDueTime = "00:00:00";
	}
	
	$dateDue = $timedate->to_display_date_time($dueDate);
	if ($focusObjectType == 'leads') {
		$focusObjectType = 'Leads';
	}
	
	if ($focusObjectType == 'opportunities') {
		$focusObjectType = 'Opportunities';
	}
	if ($focusObjectType == 'cases') {
		$focusObjectType = 'Cases';
	}
	if ($focusObjectType == 'project') {
		$focusObjectType = 'Project';
	}	

    //New for PM 6.2 - see if the task subject has variable data
    //See if the task subject has a $ - if so then call the parsetemplate function
    $taskSubject = $rowTaskTaskDefs['task_subject'];
    if (strchr($taskSubject,"$")) {
    		$taskSubject = $processManagerEngine2->parseTemplate($taskSubject,$dbTable,$focusObjectId);
    }
	$newTask->name = $taskSubject;
	$newTask->status = 'Not Started';
	$newTask->date_due_flag = 0;
	$newTask->date_due = $dueDate;
	$newTask->date_start_flag = 1;
	$newTask->priority = $rowTaskTaskDefs['task_priority'];
	//Are we a contact focus object?
	if ($focusObjectType == 'contacts') {
		$newTask->parent_type = 'Accounts';
		$newTask->contact_id = $focusObjectId;
	}
	elseif ($focusObjectType == 'accounts'){
		$newTask->parent_type = 'Accounts';
		$newTask->parent_id = $focusObjectId;
	}
	elseif ($focusObjectType == 'tasks'){
		$fieldsArray = array();
		$fieldsArray['parent_type'] = 'parent_type';
		$fieldsArray['parent_id'] = 'parent_id';
		$fieldsArray['contact_id'] = 'contact_id';
		$fieldsArray['is_pm_created_task'] = 'is_pm_created_task';
		$resultFields = $this->getFocusObjectFields($focusObjectId,$focusObjectType,$fieldsArray);
		$newTask->parent_type = $resultFields['parent_type'];
		$newTask->parent_id = $resultFields['parent_id'];
		$newTask->contact_id = $resultFields['contact_id'];
		$isPMCreatedTask = $resultFields['is_pm_created_task'];
		if ($isPMCreatedTask == 1) {
			return;
		}
	}	
	else{
		$newTask->parent_type = $focusObjectType;
		$newTask->parent_id = $focusObjectId;
	}	

	$newTask->description = $rowTaskTaskDefs['task_description'];
	//Is the assigned_user_task_id set in the def record?
	if($rowTaskTaskDefs['assigned_user_id_task'] != ""){
		$assignedUserTaskId = $rowTaskTaskDefs['assigned_user_id_task'];
		//Now get the actual user id from the users table with the given name
		$assignedUserTaskId = $this->getUserIdByName($assignedUserTaskId);
		$newTask->assigned_user_id = $assignedUserTaskId;
		$newTask->modified_user_id = $assignedUserTaskId;
	}
	else{		
		$newTask->assigned_user_id = $current_user->id;
		$newTask->modified_user_id = $current_user->id;		
	}
	
	//Begin Escalation Mods
	$is_escalatable = $rowTaskTaskDefs['is_escalatable_task'];
	$escalation_delay_minutes = $rowTaskTaskDefs['escalation_delay_minutes_task'];
	if ($escalation_delay_minutes == '') {
		$escalation_delay_minutes = 0;
	}
	$newTask->escalation_delay_minutes_c = $escalation_delay_minutes;
	$newTask->is_escalatable_task_c = $is_escalatable;
	$newTask->is_pm_created_task_c = '1';
	$newTask->initial_date_due_c = $taskDueDate;
	$newTask->initial_time_due_c = $taskDueTime;
	$newTask->save(TRUE);
	$newTaskid = $newTask->id;
	$previousTaskId = $newTaskid;	
	
	return $newTaskid;

}

//***************************************************************************
//This function loads a new task into the task table
//***************************************************************************

function createNewProjectTask($focusObjectId,$focusObjectType,$rowProjectTaskDefs,$dueDate){
	//global $previousTaskId;
	global $current_user;
	$current_user->name = "SugarCRM Administrator";
	$newProjectTask = new ProjectTask();
	
	//Date Function
	$timestamp = date('Y-m-d H:i:s', time());
	$startDelayDays = $rowProjectTaskDefs['project_task_start_date'];
	$endDelayDays = $rowProjectTaskDefs['project_task_end_date'];
	$startDate = $this->getNewDateTimeStartDelayedStage($dueDate,$startDelayDays, 0, 0, 0, 0);
	$endDate = $this->getNewDateTimeStartDelayedStage($dueDate,$endDelayDays, 0, 0, 0, 0);
	$timedate = new TimeDate();
	$dateDue = $timedate->to_display_date_time($dueDate);
	if ($focusObjectType == 'leads') {
		$focusObjectType = 'Leads';
	}
	
	if ($focusObjectType == 'opportunities') {
		$focusObjectType = 'Opportunities';
	}
	if ($focusObjectType == 'cases') {
		$focusObjectType = 'Cases';
	}
	if ($focusObjectType == 'project') {
		$focusObjectType = 'Project';
	}	

	$newProjectTask->name = $rowProjectTaskDefs['project_task_subject'];
	$newProjectTask->status = $rowProjectTaskDefs['project_task_status'];
	$newProjectTask->date_start = $startDate;
	$newProjectTask->date_finish = $endDate;
	$newProjectTask->priority = $rowProjectTaskDefs['project_task_priority'];
	$newProjectTask->project_task_id = $rowProjectTaskDefs['project_task_id'];
	$newProjectTask->project_id = $focusObjectId;	
	$newProjectTask->description = $rowProjectTaskDefs['project_task_description'];
	$idCheck = $this->checkProjectAlreadyCreated($focusObjectId);
	if($idCheck){
		$newProjectTask->date_entered = $timestamp;
	}else{
		$newProjectTask->date_modified = $timestamp;
	}
	if($rowProjectTaskDefs['assigned_user_id_project_task'] != ""){
		$assignedUserTaskId = $rowProjectTaskDefs['assigned_user_id_project_task'];
		//Now get the actual user id from the users table with the given name
		$assignedUserTaskId = $this->getUserIdByName($assignedUserTaskId);
		$newProjectTask->assigned_user_name = $assignedUserTaskId;
	}
	else{		
		$newProjectTask->assigned_user_name = $current_user->id;	
	}
	$newProjectTask->save(TRUE);
	$newProjectTaskid = $newProjectTask->id;
	return $newProjectTaskid;
}

function checkProjectAlreadyCreated($focusObjectId){
	$queryProjectTaskID = "Select * from pm_process_project_task_defs where project_id = '" .$focusObjectId ."'";
	$result = $this->db->query($queryProjectTaskID);
	$row = $this->db->fetchByAssoc($result);
	if($row['id'] != ''){
		$status = true;
	}else{
	  	$status = false;
	}
	return $status;
}

//***************************************************************************
//This function is called by runScheduleCallTask to insert the new call data
//We pass the focus object and focus type, the row of call defs and also
//call time start.
//Iff the call is for a lead then we set parent type and parent id
//If the call is for a contact then we relate with calls_contacts table
//Also we parse the call start time to get the date and time 
//***************************************************************************

function createNewCallTask($focusObjectId,$focusObjectType,$rowTaskCallDefs,$newCallTimeStart,$focusObjectCreateDate){
	global $previousTaskId;
	global $current_user;
	require_once('modules/PM_ProcessManager/ProcessManagerEngine2.php');
	$processManagerEngine2 = new ProcessManagerEngine2();
	//Mods to fix date start issue
	$timedate=new TimeDate();
	$dbTable = $focusObjectType;
	$current_user->name = "SugarCRM Administrator";
	require_once('modules/Calls/Call.php');
	$newCall = new Call();
	$timezone = date('Z') / 3600;
	$timezone = substr($timezone,1);
	$today = date('Y-m-d H:i:s', time() + $timezone * 60 * 60);

	$newCallUserid = create_guid();
	$spaceLocation = strpos($newCallTimeStart," ");
	$callStartDate = substr($newCallTimeStart,0,$spaceLocation);
	$callStartTime = substr($newCallTimeStart,$spaceLocation);	

	//Format the y-m-d of the new call time start to reflect what the user would have used
	$dateStart = $timedate->to_display_date_time($newCallTimeStart);
	if ($focusObjectType == 'leads') {
		$focusObjectType = 'Leads';
	}
	if ($focusObjectType == 'opportunities') {
		$focusObjectType = 'Opportunities';
	}
	if ($focusObjectType == 'cases') {
		$focusObjectType = 'Cases';
	}
	if ($focusObjectType == 'accounts') {
		$focusObjectType = 'Accounts';
	}
	if ($focusObjectType == 'project') {
		$focusObjectType = 'Project';
	}
	//Is the assigned_user_call_id set in the def record?
	if($rowTaskCallDefs['assigned_user_id_call'] != ""){
		$assignedUserCallId = $rowTaskCallDefs['assigned_user_id_call'];
		$assignedUserCallId = $this->getUserIdByName($assignedUserCallId);
	}
	else{		
		$assignedUserCallId	= $current_user->id;
	}			
    $callSubject = $rowTaskCallDefs['call_subject'];
    if (strchr($callSubject,"$")) {
    		$callSubject = $processManagerEngine2->parseTemplate($callSubject,$dbTable,$focusObjectId);
    		$newCall->name = $callSubject;
    }else{			
	//Build the Call object and Save
	$newCall->name = $rowTaskCallDefs['call_subject'];
	}
	//Set the correct parent type
	$ii = 1;
	if ($focusObjectType == 'contacts') {
		$newCall->parent_type = 'Accounts';
	}elseif ($focusObjectType == 'calls'){
		$ii = $ii++;
		$fieldsArray = array();
		$fieldsArray['parent_type'] = 'parent_type';
		$fieldsArray['parent_id'] = 'parent_id';
		$fieldsArray['is_pm_created_call'] = 'is_pm_created_call';
		$resultFields = $this->getFocusObjectFields($focusObjectId,$focusObjectType,$fieldsArray);
		$newCall->parent_type = $resultFields['parent_type'];
		$newCall->parent_id = $resultFields['parent_id'];
		$focusObjectId = $resultFields['parent_id'];
		$focusObjectType = $resultFields['parent_type'];
		$isPMCreatedCall = $resultFields['is_pm_created_call'];
		if ($isPMCreatedCall == 1 OR $ii > 1) {
			return;
		}
	}
	//Patch PM Pro 10-2011 - if the focus object is a task then do the same thing that we do for calls above
	elseif ($focusObjectType == 'tasks'){
			$ii = $ii++;
			$fieldsArray = array();
			$fieldsArray['parent_type'] = 'parent_type';
			$fieldsArray['parent_id'] = 'parent_id';
			$fieldsArray['is_pm_created_task'] = 'is_pm_created_task';
			$resultFields = $this->getFocusObjectFields($focusObjectId,$focusObjectType,$fieldsArray);
			$newCall->parent_type = $resultFields['parent_type'];
			$newCall->parent_id = $resultFields['parent_id'];
			$isPMCreatedTask = $resultFields['is_pm_created_task'];
			if ($isPMCreatedTask == 1 OR $ii > 1) {
				return;
			}
		}	
	else{
	$newCall->parent_type = $focusObjectType;
	}
	$newCall->reminder_time = $rowTaskCallDefs['reminder_time'];
	$newCall->description = $rowTaskCallDefs['call_description'];
	$newCall->duration_hours = 0;
	$newCall->duration_minutes = 15;
	$newCall->direction = "Inbound";
	$newCall->assigned_user_id = $assignedUserCallId;
	$newCall->notify_inworkflow = true;
	$newCall->date_start = $dateStart;
	//Escalation
	$newCall->save(TRUE);
	$newCallId = $newCall->id;
	$previousTaskId = $newCall->id;
	
	//PM Pro Patch 10-2011
	//When triggering a process on Tasks we need to see if the Tasks parent type is contacs and if so then we need to set the calls_contacts
	if ($focusObjectType == 'tasks') {
		$taskParentType = $resultFields['parent_type'];
		$taskParentID = $resultFields['parent_id'];
		if ($taskParentType == 'Contacts'){
			//Now go and insert the call contacts for the contact
			$this->loadCallsContacts($taskParentID,$newCallId,$today);
			$focusObjectType = 'Accounts';
			//Now go and get the account for the contact
			$accountContactId = $this->getAccountContactId($taskParentID);
			$focusObjectId = $accountContactId;				
		}
	}
	
	//If the focus object is a call then we need to insert an entry in calls_contacts
	//We also need to go and get the account related to the contact
	//This used in the call insert: parent_type = Accounts and parent_id is account id
	if ($focusObjectType == 'contacts') {
		$focusObjectType = 'Accounts';
		$this->loadCallsContacts($focusObjectId,$newCallId,$today);
		//Now go and get the account for the contact
		$accountContactId = $this->getAccountContactId($focusObjectId);
		$focusObjectId = $accountContactId;	
	}
	//Begin Escalation Mods
	$is_escalatable = $rowTaskCallDefs['is_escalatable_call'];
	$escalation_delay_minutes = $rowTaskCallDefs['escalation_delay_minutes_call'];
	if ($escalation_delay_minutes == '') {
		$escalation_delay_minutes = 0;
	}
	//Now update the call with the non baseline fields that are new for Process Manager
	//PM Pro Patch 10-2011: added parent_type = focusobjecttype for support for tasks as trigger
	$newCall->parent_id = $focusObjectId;
	$newCall->parent_type = $focusObjectType;
	$newCall->is_escalatable_call_c = $is_escalatable;
	$newCall->escalation_delay_minutes_c = $escalation_delay_minutes;
	$newCall->is_pm_created_call_c = '1';
	$newCall->initial_date_start_c = $callStartDate;
	$newCall->initial_time_start_c = $callStartTime;
	$newCall->assigned_user_id = $assignedUserCallId;
	$newCall->modified_user_id = $assignedUserCallId;
	$newCall->save(FALSE);
	//Now insert into calls_user
	$newCallUserQuery = "Insert into calls_users (`id`,`call_id`,`user_id`,`required`,`accept_status`,`date_modified`,`deleted`) VALUES ($newCallUserid,$newCallId,$assignedUserCallId,'1','accept','$today',0)";
	$this->db->query($newCallUserQuery);
	if ($focusObjectType == 'Leads') {
		$newCallUserQuery = "Insert into calls_leads (`id`,`call_id`,`lead_id`,`required`,`accept_status`,`date_modified`,`deleted`) VALUES ($newCallUserid,$newCallId,$focusObjectId,'1','accept','$today',0)";		
		$this->db->query($newCallLeadQuery);
	}		
	return $newCall->id;
}

//***************************************************************************
//This function gets a custom field from the objects cstm field table
//**************************************************************************
function getFocusObjectCustomFields($focusObjectId,$focusObjectType,$focusFieldsArray){
	//The focusObjectType holds the object like lead, contacts, etc.
	$table = $focusObjectType;
	$table .= '_cstm';	
	$counter = 1;
	$query = "Select ";
		//Get the count of array fields so we know when to not add the ,
		$countOfArrayElements = count($focusFieldsArray);		
		foreach($focusFieldsArray as $field)
			{				
				// Copy the relevant fields
				$fieldName = $field;				
				$query .= $fieldName;
				if ($counter < $countOfArrayElements) {
					$query .= " ,";	
				}	
				$counter ++;
			}
		$query .= " from " .$table ." where id_c = '" .$focusObjectId ."'";
		$resultFieldValues = $this->db->query($query);
		$rowFieldValues = $this->db->fetchByAssoc($resultFieldValues);
		//Now build the array with the values and send back
		foreach($focusFieldsArray as $field)
			{
				// Copy the relevant fields
				$fieldName = $field;
				$fieldValue = $rowFieldValues[$fieldName];
				$focusFieldsArray[$fieldName] = $fieldValue;
			}
		return $focusFieldsArray;	 	
}
//****************************************************************************
//This is a generic function that is passed an array of fields and returns
//the values of the fields.
//***************************************************************************
function getFocusObjectFields($focusObjectId,$focusObjectType,$focusFieldsArray){
	//Here we check to first see if the
	$counter = 1;
	$query = "Select ";
	//Get the count of array fields so we know when to not add the ,
	$countOfArrayElements = count($focusFieldsArray);
	foreach($focusFieldsArray as $field)
	{
		if($field == ''){
			return;
		}
		// Copy the relevant fields
		$fieldName = $field;
		$query .= $fieldName;
		if ($counter < $countOfArrayElements) {
			$query .= " ,";
		}
		$counter ++;
	}
	$query .= " from " .$focusObjectType ." where id = '" .$focusObjectId ."'";
	$resultFieldValues = $this->db->query($query);
	$rowFieldValues = $this->db->fetchByAssoc($resultFieldValues);
	//Now build the array with the values and send back

	foreach($focusFieldsArray as $field)
	{
		// Copy the relevant fields
		$fieldName = $field;
		$fieldValue = $rowFieldValues[$fieldName];
		$focusFieldsArray[$fieldName] = $fieldValue;

	}
	return $focusFieldsArray;
}
	
//****************************************************************************
//											
//***********************--- TIME FUNCTIONS --********************************
//
//****************************************************************************	
	
function getNewCallTimeStart($focusObjectCreateDate,$startDelayYears,$startDelayMonths,$startDelayDays,$startDelayHours,$startDelayMinutes){

	//First thing to do is to see if the delay is in days - if so add that many days to 
	$GLOBALS['log']->info("Process Manager - Calculating the new call time start");	
	//Date Entered for Focus Object in this format - 2005-11-23 18:34:00
	$timedate = new TimeDate();
	$timezone = date('Z') / 3600;	
	//So we know the users timezone - and this value is a value like -8
	//So remove the - and get the offset.
	$timezone = substr($timezone,1);	
	$focusObjectCreateDateNew = date($focusObjectCreateDate, time() - 8 * 60 * 60);	 
	list ($year, $month, $day, $hour, $min, $sec) = split ('[- :]', $focusObjectCreateDate);	
	if ($startDelayYears != 0) {
		$year = $year + $startDelayYears;
	}	
	if ($startDelayMonths != 0) {
		$month = $month + $startDelayMonths;
	}	
	if ($startDelayDays != 0) {
		$day = $day + $startDelayDays;
	}
	if ($startDelayHours != 0) {
		$hour = $hour + $startDelayHours;
	}
	
	if ($startDelayMinutes != 0) {
		$min = $min + $startDelayMinutes;
	}
	//Make seconds = 00
	$sec = '00';
	$callStartDateWhole = date("Y-m-d H:i:s",mktime ($hour, $min, $sec, $month, $day, $year));
	return $callStartDateWhole;	
}

//****************************************************************************
//											
//***********************--- TIME FUNCTIONS --********************************
//
//****************************************************************************	
	

function getNewDateTimeStartDelayedStage($focusObjectCreateDate,$startDelayDays,$startDelayHours,$startDelayMinutes,$startDelayMonths,$startDelayYears){
	//Date Entered for Focus Object in this format - 2005-11-23 18:34:00
	$timedate = new TimeDate();
	$user = new User();
	$timezone = date('Z') / 3600;
	//So we know the users timezone - and this value is a value like -8
	//So remove the - and get the offset.
	$timezone = substr($timezone,1);
	$dateParts = preg_split("/[\s-:]+/", $focusObjectCreateDate);
	$year = $dateParts[0];
	$hour = $dateParts[3];
	$min = $dateParts[4];
	$month = $dateParts[1];
	$day = $dateParts[2];
	if ($startDelayDays != 0) {
		$day = $dateParts[2] + $startDelayDays;
	}
	if ($startDelayHours != 0) {
		$hour = $dateParts[3] + $startDelayHours;
	}
	if ($startDelayMinutes != 0) {
		$min = $dateParts[4] + $startDelayMinutes;
	}
	if ($startDelayMonths != 0) {
		$month = $dateParts[1] + $startDelayMonths;
	}
	if ($startDelayYears != 0) {
		$year = $dateParts[0] + $startDelayYears;
	}
	//Make seconds = 00
	$sec = '00';
	$callStartDateWhole = date("Y-m-d H:i:s",mktime ($hour, $min, $sec, $month, $day, $year));
	return $callStartDateWhole;	
}

//*************************************************************************
//This function will get the contacts related account id and return 
//For when the task is a call

function getAccountContactId($focusObjectId){
	$queryAccountContacts = "Select account_id from accounts_contacts where contact_id = '" .$focusObjectId ."'";
	$result = $this->db->query($queryAccountContacts);
	$row = $this->db->fetchByAssoc($result);
	if ($row) {
		$accountId = $row['account_id'];
	}
	return $accountId;
	
}

function getFieldValue($focusObjectType,$focusObjectId,$emailField){

	$processManagerEngine1 = new ProcessManagerEngine1();
	$newBean = $processManagerEngine1->convertTableToBean($focusObjectType);
	if(empty($newBean)){
		return;
	}
    //$newBean = BeanFactory::retrieveBean($newBean->module_name, $focusObjectId, array('disable_row_level_security' => true));
    $newBean->retrieve($focusObjectId);
	foreach($newBean->field_defs as $key => $def) {
		if (isset($def['type'])) {
			$type = $def['type'];
		}
		if ($key == $emailField){
			if ($type == 'relate'){
				foreach($def as $key2 => $value){
					if ($key2 == 'id_name'){
						$relatedID =  $newBean->{$value};
					}
					if ($key2 == 'module'){
						$relatedModule = strtolower($value);
					}
				}			
				$relatedBean = $processManagerEngine1->convertTableToBean($relatedModule);
				if(empty($relatedBean))
					return;
         //$relatedBean = BeanFactory::retrieveBean($relatedBean->module_name, $relatedID, array('disable_row_level_security' => true));
				$relatedBean->retrieve($relatedID);
				$emailRow = $this->getEmailAddress($relatedBean->id,$relatedBean->table_name);
				$address = $emailRow['email_address'];		
				return $address;
			}
		}
	}			
	$value = $newBean->{$emailField};
	return $value;	
}
//****************************************************************************
//This function is called to run an email task and is passed the task id
//object id and object type
//***************************************************************************

function runEmailTask($taskId,$focusObjectId,$focusObjectType){
	$GLOBALS['log']->info("Process Manager - Running the email task for object type $focusObjectType and object id $focusObjectId and task id $taskId");	
	require_once('modules/Users/User.php');
	$processManagerEngine1 = new ProcessManagerEngine1();
	global $current_user;
	$current_user = new User();
	$dbTable = $focusObjectType;
	$dbTableID = $focusObjectId;
	$rowUser = $this->getFocusOwner($focusObjectType,$focusObjectId);	
	//For version 4.5.1 we get the user preferences from the user_preference table
	$userId = $rowUser['id'];
	$rowUserPreferenceContents = $this->getUserPreferenceRow($userId);
	$current_user->id = $rowUser['id'];
	$current_user->user_name = $rowUser['user_name'];
	$user_name = $rowUser['user_name'];
	$_SESSION[$current_user->user_name . '_PREFERENCES']['global'] = unserialize(base64_decode($rowUserPreferenceContents));				
	$current_user->user_preferences['global'] = unserialize(base64_decode($rowUserPreferenceContents));
	$sendEmailToOppAccount = 0;
	if (isset($GLOBALS['beanList']) && isset($GLOBALS['beanFiles'])) {
				global $beanFiles;
				global $beanList;
			} else {
				require_once('include/modules.php');
			}
	$queryEmailDefsTable = "Select * from pm_process_task_email_defs where task_id = '" .$taskId ."'";
	$result = $this->db->query($queryEmailDefsTable);
	$row = $this->db->fetchByAssoc($result);
	//Initialize the To - CC and BCC Array
	$toccbcc = array('to_address' => array(),
               		 'cc_address'     => array(),
               		 'bcc_address'     => array());
	//Check to see if this is an internal email - is so then skip the rest 
	if ($row['internal_email'] == 1) {
		$to_address = $row['internal_email_to_address'];
		$toccbcc = $this->setToCcBccAddress($to_address,$row,$toccbcc,'The alternate or fixed email address');
	}
	
	if ($row['send_email_to_object_owner'] == 1){
		//Get id of assigned user 
        $rowUser = $this->getFocusOwner($focusObjectType,$focusObjectId);
        $userId = $rowUser['id'];
        $rowEmailAddressOptOut = $this->getEmailAddress($userId,"Users");
        $ownerEmailAddr = $rowEmailAddressOptOut['email_address'];
        $toccbcc = $this->setToCcBccAddress($ownerEmailAddr,$row,$toccbcc,'The current owner of the Object');
	}
    //PM 4.0
    if (!empty($row['send_email_other_owner'])){
        //Get id of assigned user
        switch ($row['send_email_other_owner']){
            case 'user_who_created_record':
                $focusFieldsArray['created_by'] = 'created_by';
                $arrayFieldsFromFocusObject = $this->getFocusObjectFields($focusObjectId,$focusObjectType,$focusFieldsArray);
                $userId = $arrayFieldsFromFocusObject['created_by'];
                break;
            case 'user_who_last_modified_record':
                $focusFieldsArray['modified_user_id'] = 'modified_user_id';
                $arrayFieldsFromFocusObject = $this->getFocusObjectFields($focusObjectId,$focusObjectType,$focusFieldsArray);
                $userId = $arrayFieldsFromFocusObject['modified_user_id'];
                break;
            case 'user_who_was_assigned_record':
                //Get last owner via audit log
                //$userId = getLastOwner($focusObjectId,$focusObjectType);
                $userId  = $this->getAuditTableEntry($focusObjectType,$focusObjectId,'assigned_user_id','before');
                break;
        }
        $rowEmailAddressOptOut = $this->getEmailAddress($userId,"Users");
        $to_address = $rowEmailAddressOptOut['email_address'];
        $emailOptOut = $rowEmailAddressOptOut['opt_out'];
        $focusObjectIdContactOpp = $focusObjectId;
        $toccbcc = $this->setToCcBccAddress($to_address,$row,$toccbcc,'Past Object Owners');
        if ($to_address == "") {
            return;
        }
        if ($emailOptOut != "0") {
            return;
        }
        $to_address = '';
    }
//PM 4.0
    if (!empty($row['send_email_to_role'])){
        //Get all the members of the role and send them the email
        $qroleUsers = "select user_id from acl_roles_users, acl_roles where acl_roles_users.deleted = 0 and acl_roles_users.role_id = acl_roles.id and acl_roles.name = '" .$row['send_email_to_role'] ."' and acl_roles.deleted = 0";
        $resultRoleUsers = $this->db->query($qroleUsers, true);
        while($rowru = $this->db->fetchByAssoc($resultRoleUsers))
        {
            //get the email address for the user
            $rowEmailAddressOptOut = $this->getEmailAddress($rowru['user_id'],"Users");
            $to_address = $rowEmailAddressOptOut['email_address'];
            $toccbcc = $this->setToCcBccAddress($to_address,$row,$toccbcc,'Members of the Role');
            $to_address = '';
        }
    }
    //PM 4.0
    if (!empty($row['send_email_to_team'])){
        if($row['send_email_to_team'] == 'All Members of Assigned Team'){
            $qobjteams = "select team_id from $focusObjectType where id = '$focusObjectId'";
            $resultObjTeams = $this->db->query($qobjteams, true);
            $rowtobjteams = $this->db->fetchByAssoc($resultObjTeams);
            $qt = "select name from teams where id = '" .$rowtobjteams['team_id'] ."' and deleted = 0";
            $resultT = $this->db->query($qt, true);
            $rowteams = $this->db->fetchByAssoc($resultT);
            $row['send_email_to_team'] = $rowteams['name'];
        }
        //Get all the members of the role and send them the email
        $qroleTeams = "select user_id from team_memberships, teams, users where team_memberships.deleted = 0 and team_memberships.team_id = teams.id and team_memberships.user_id = users.id and users.deleted = 0 and teams.name = '" .$row['send_email_to_team'] ."' and teams.deleted = 0";
        $resultTeamUsers = $this->db->query($qroleTeams, true);
        while($rowtu = $this->db->fetchByAssoc($resultTeamUsers))
        {
            //get the email address for the user
            $rowEmailAddressOptOut = $this->getEmailAddress($rowtu['user_id'],"Users");
            $to_address = $rowEmailAddressOptOut['email_address'];
            $toccbcc = $this->setToCcBccAddress($to_address,$row,$toccbcc,'Members of the Team');
            //Clear out the To Address
            $to_address = '';
        }
    }
	//New PM Ent Code for email address on custom field
	if ($row['email_field'] != ''){
		//Email Address if on this field
		$emailField = $row['email_field'];
		if ($emailField != 'email'){
             if($emailField != 'email1') {
                 $to_address = $this->getFieldValue($focusObjectType, $focusObjectId, $emailField);
                 $toccbcc = $this->setToCcBccAddress($to_address, $row, $toccbcc, 'The Process Object Email Address');
             }
		}			

	}
	//Related Module holds the email field
	if ($row['related_module'] != ''){
		require_once('modules/PM_ProcessManager/ProcessManagerEngine2.php');
		$thisPmEngine2 = new ProcessManagerEngine2();
		$relatedModule = $row['related_module'];		
		if ($row['related_module_email_field'] != '' && $row['related_module_email_field'] != 'email' && $row['related_module_email_field'] != 'email1'){
			$relatedField = $row['related_module_email_field'];
                    $relatedBeans = $thisPmEngine2->getRelatedBeans($focusObjectType, $focusObjectId, $relatedModule);
                    //PM 4.0 - if sending to the related Created By - Modified By - or Assigned User Id
                    if (($relatedField == 'reports_to_id') || ($relatedField == 'assigned_user_id') || ($relatedField == 'modified_user_id') || ($relatedField == 'created_by') || ($relatedField == 'assigned_user_name')) {
                        switch ($relatedField) {
                            case 'created_by':
                                $focusFieldsArray['created_by'] = 'created_by';
                                $arrayFieldsFromFocusObject = $this->getFocusObjectFields($focusObjectId, $focusObjectType, $focusFieldsArray);
                                $userId = $arrayFieldsFromFocusObject['created_by'];
                                break;
                            case 'modified_user_id':
                                $focusFieldsArray['modified_user_id'] = 'modified_user_id';
                                $arrayFieldsFromFocusObject = $this->getFocusObjectFields($focusObjectId, $focusObjectType, $focusFieldsArray);
                                $userId = $arrayFieldsFromFocusObject['modified_user_id'];
                                break;
                            case 'assigned_user_id':
                                $focusFieldsArray['assigned_user_id'] = 'assigned_user_id';
                                $arrayFieldsFromFocusObject = $this->getFocusObjectFields($focusObjectId, $focusObjectType, $focusFieldsArray);
                                $userId = $arrayFieldsFromFocusObject['assigned_user_id'];
                                break;
                            case 'assigned_user_name':
                                $focusFieldsArray['assigned_user_id'] = 'assigned_user_id';
                                $arrayFieldsFromFocusObject = $this->getFocusObjectFields($focusObjectId, $focusObjectType, $focusFieldsArray);
                                $userId = $arrayFieldsFromFocusObject['assigned_user_id'];
                                break;
                            case 'reports_to_id':
                                $focusFieldsArray['assigned_user_id'] = 'assigned_user_id';
                                $arrayFieldsFromFocusObject = $this->getFocusObjectFields($focusObjectId, $focusObjectType, $focusFieldsArray);
                                $userId = $arrayFieldsFromFocusObject['assigned_user_id'];
                                //Now get the Reports To Id
                                $user = new User();
                                $user->retrieve($userId);
                                $userId = $user->reports_to_id;
                                break;
                        }
                        $rowEmailAddressOptOut = $this->getEmailAddress($userId, "Users");
                        $to_address = $rowEmailAddressOptOut['email_address'];
                        $toccbcc = $this->setToCcBccAddress($to_address,$row,$toccbcc,'Email Addresses from Related Modules');
                    } else {
                        if ($row['send_email_to_all'] == 1) {
                            foreach ($relatedBeans as $bean) {
                                $to_address = $bean->$relatedField;
                                $toccbcc = $this->setToCcBccAddress($to_address, $row, $toccbcc, 'Email Addresses from Related Modules');
                            }
                        } else {
                            $bean = $relatedBeans[0];
                            $to_address = $bean->$relatedField;
                            $toccbcc = $this->setToCcBccAddress($to_address, $row, $toccbcc, 'Email Addresses from Related Modules');
                        }
                    }
		}
		//Else Person Module
		else{
			//Check to see if we are sending Emails to All Related Object
			$relatedBeans = $thisPmEngine2->getRelatedBeans($focusObjectType, $focusObjectId,$relatedModule);
			if($row['send_email_to_all'] == 1){	
				foreach($relatedBeans as $bean){
					$rowEmailAddressOptOut = $this->getEmailAddress($bean->id,$bean->table_name);
					$to_address = $rowEmailAddressOptOut['email_address'];
                    $toccbcc = $this->setToCcBccAddress($to_address,$row,$toccbcc,'Email Addresses from Related Modules');
				}
			}
			else{
				//Get Related Bean
				$bean = $relatedBeans[0];
				$rowEmailAddressOptOut = $this->getEmailAddress($bean->id,$bean->table_name);
				$to_address = $rowEmailAddressOptOut['email_address'];
                $toccbcc = $this->setToCcBccAddress($to_address,$row,$toccbcc,'Email Addresses from Related Modules');
			}
		}
	}
	//Check to see if we have have any values in the to cc bcc array - if so then send the email
    if(!empty($toccbcc['to_address'])){
        $this->sendEmail($row, $focusObjectId, $focusObjectType, $to_address, $dbTable, $dbTableID, $current_user,$toccbcc );
        return;
    }
		//Legacy Code if the to address is blank
		else{
			//Check for base objects that have emails - contacts, leads, accounts, prospects
				if (($focusObjectType == "contacts") || ($focusObjectType == "leads") || ($focusObjectType == "accounts") || ($focusObjectType == "prospects")) {
					$rowEmailAddressOptOut = $this->getEmailAddress($focusObjectId,$focusObjectType);
					$focusObjectIdContactOpp = $focusObjectId;
					
					if ($rowEmailAddressOptOut['email_address'] == "") {
							return;
					}
					if ($rowEmailAddressOptOut['opt_out'] != "0") {
							return;
					}
		 		}
				//For Opportunities go and see if there is a related contact to the opportunity
				//Must have a releated contact and an email to send an email
				if ($focusObjectType == "opportunities") {	
					$sendEmailToOppAccount = $row['send_email_to_caseopp_account'];
					if ($sendEmailToOppAccount == 1) {
							$rowAccountOpp = $this->getAccountOppEmails($focusObjectId,$focusObjectType);
							$focusObjectIdAccount = $rowAccountOpp['id'];
							$rowEmailAddressOptOut = $this->getEmailAddress($focusObjectIdAccount,"accounts"); 
							$focusObjectType = 'accounts';
							$opportunityID = $focusObjectId; 			
					}
					else{					
							$contact_role = $row['contact_role'];
							//if ($contact_role != "") {
							$rowContactOpp = $this->getContactOppEmails($focusObjectId,$focusObjectType,$contact_role);
							$focusObjectType = 'contacts';
							$focusObjectIdContact = $rowContactOpp['id']; 
							$rowEmailAddressOptOut = $this->getEmailAddress($focusObjectIdContact,"contacts"); 
					}
						$to_address = $rowEmailAddressOptOut['email_address'];
						$emailOptOut = $rowEmailAddressOptOut['opt_out'];
						if ($to_address == "") {
								return;
						}
						if ($emailOptOut != "0") {
								return;
						}
					$focusObjectType = 'opportunities';			
				}
				//For cases - get the email address for the related account			
				if ($focusObjectType == "cases") {			
					$case_id = 	$focusObjectId;				
					$rowAccountCases = $this->getAccountEmailForCases($focusObjectId,$focusObjectType);
					$focusObjectType = 'accounts';
					$focusObjectId = $rowAccountCases['account_id'];
					$rowEmailAddressOptOut = $this->getEmailAddress($focusObjectId,$focusObjectType);
					if ($rowEmailAddressOptOut['email_address'] == "") {
								return;
					}
					if ($rowEmailAddressOptOut['opt_out'] != "0") {
								return;
					}
				$focusObjectId = $case_id;				
				$focusObjectType = 'cases';
				}
				//Checking for process object of type tasks
				if($focusObjectType == "tasks"){
					$getTaskSQL = "SELECT * FROM tasks WHERE id = '$focusObjectId'";
					$taskQuery = $this->db->query($getTaskSQL);
					$row_task_query = $this->db->fetchByAssoc($taskQuery);
					//If the Task is related to the Contact	
					if(isset($row_task_query['contact_id'])){
						$getContactEmailSQL = "SELECT * FROM email_addresses WHERE id = '". $row_task_query['contact_id'] . "', opt_out = '0'";
						$contactEmailQuery = $this->db->query($getContactEmailSQL);
						$row_contact_email_query = $this->db->fetchByAssoc($contactEmailQuery);	
						$to_address = $row_contact_email_query['email_address'];
						$rowEmailAddressOptOut['email_address'] = $to_address;
					}
					
					if($row_task_query['parent_type'] == 'Accounts' || $row_task_query['parent_type'] == 'Leads' || $row_task_query['parent_type'] == 'Contacts' ){
							$taskParentID = $row_task_query['parent_id'];
							//What we do here is determin is this task has been created from Contacts Detail View
							//If so - then the parent id is null and contact_id has a value
							if (($row_task_query['parent_type'] == 'Accounts') && ($row_task_query['contact_id'] != '') ){
								$taskParentID = $row_task_query['contact_id'];
								$rowEmailAddressOptOut = $this->getEmailAddress($taskParentID,'contacts');
								$focusObjectType = 'contacts';
								$focusObjectId = $taskParentID;
								if ($rowEmailAddressOptOut['email_address'] == "") {
									return;
								}
								if ($rowEmailAddressOptOut['opt_out'] != "0") {
									return;
								}
							}
						//Account Only Related to Task
						elseif(($row_task_query['parent_type'] == 'Accounts') && ($row_task_query['parent_id'] != '') && ($row_task_query['contact_id'] == '')){
							$rowEmailAddressOptOut = $this->getEmailAddress($taskParentID,'accounts');
							$focusObjectType = 'accounts';
							if ($rowEmailAddressOptOut['email_address'] == "") {
								return;
							}
							if ($rowEmailAddressOptOut['opt_out'] != "0") {
								return;
							}						
						}
						//Leads
						elseif($row_task_query['parent_type'] == 'Leads'){
							$rowEmailAddressOptOut = $this->getEmailAddress($taskParentID,'leads');
							$focusObjectType = 'leads';
							if ($rowEmailAddressOptOut['email_address'] == "") {
								return;
							}
							if ($rowEmailAddressOptOut['opt_out'] != "0") {
								return;
							}						
						}
						else{
							$rowEmailAddressOptOut = $this->getEmailAddress($taskParentID,'contacts');
							$focusObjectType = 'contacts';
							if ($rowEmailAddressOptOut['email_address'] == "") {
								return;
							}
							if ($rowEmailAddressOptOut['opt_out'] != "0") {
								return;
							}						
						}
				 	}
					 //If To Address is blank then we may have a custom module
					 if($rowEmailAddressOptOut['email_address'] == ''){
					 	//Get the parent type and do a lookup
					 	$rowEmailAddressOptOut = $this->getEmailAddress($row_task_query['parent_type'],$row_task_query['parent_id']);
					 	if ($rowEmailAddressOptOut['email_address'] == "") {
					 		return;
					 	}
					 	if ($rowEmailAddressOptOut['opt_out'] != "0") {
					 		return;
					 	}
					 	$focusObjectType = $row_task_query['parent_type'];
					 	$focusObjectId = $row_task_query['parent_id'];
					 }				 
				}
			//Checking for process object of type calls
			if($focusObjectType == 'calls'){
				$get_contact = "SELECT contact_id FROM calls_contacts WHERE deleted = 0 AND call_id = '$focusObjectId'";
				$get_contact_res = $this->db->query($get_contact);	
				if($get_contact_row = $this->db->fetchByAssoc($get_contact_res)){
					//Calls contact relationship set so use this contact ID to send the email
					$rowEmailAddressOptOut = $this->getEmailAddress($get_contact_row['contact_id'], 'contacts');	
					if ($rowEmailAddressOptOut['email_address'] == "") {
							return;
					}
						if ($rowEmailAddressOptOut['opt_out'] != "0") {
							return;
					}						
				}else{
					//Calls contact not set so use the parent type and parent id
					$get_call = "SELECT * FROM calls WHERE deleted = 0  AND id = '$focusObjectId'";
					$get_call_res = $this->db->query($get_call);
					if($get_call_row = $this->db->fetchByAssoc($get_call_res)){
							$focusObjectType = $get_call_row['parent_type'];
							//Parent Type is Accounts, Leads, Etc
							$thisBean = $this->convertBeanToTable($focusObjectType);
							$focusObjectType = $thisBean->table_name;
							$rowEmailAddressOptOut = $this->getEmailAddress($get_call_row['parent_id'], $focusObjectType);	
							if ($rowEmailAddressOptOut['email_address'] == "") {
								return;
							}
							if ($rowEmailAddressOptOut['opt_out'] != "0") {
								return;
							}
						
					}else{
						return;
					}					
				}
			}
			//FINAL CATCH ALL
			//Check to see if the return array is empty - if so then last chance to find email address
            if(isset($rowEmailAddressOptOut)) {
                if ($rowEmailAddressOptOut['email_address'] == '') {
                    $rowEmailAddressOptOut = $this->getEmailAddress($focusObjectId, $focusObjectType);
                    if ($rowEmailAddressOptOut['email_address'] == "") {
                        return;
                    }
                    if ($rowEmailAddressOptOut['opt_out'] != "0") {
                        return;
                    }
                }
                $to_address = $rowEmailAddressOptOut['email_address'];
            }
		}	
	//If we got this far and no to address then exit
	if ($to_address == "") {
		//return;
	}
	else{
        $toccbcc = $this->setToCcBccAddress($to_address,$row,$toccbcc,'The Process Object Email Address');
		$this->sendEmail($row, $focusObjectId, $focusObjectType, $to_address, $dbTable, $dbTableID, $current_user,$toccbcc );
	}
	
}

function sendEmail($row, $focusObjectId, $focusObjectType, $to_address, $dbTable, $dbTableID, $current_user, $toccbcc ){
    //Initialize Variables
    $bcc_address = '';
    $cc_address = '';
    if(empty($toccbcc['to_address']))
        $toccbcc['to_address'][$to_address] = $to_address;
    //Set the To CC and BCC
    foreach($toccbcc['to_address'] as $to){
        if(empty($to_address)){
            $to_address = $to;
        }else {
            if (strpos($to_address, $to) === false)
                $to_address = $to_address . ";" . $to;
        }
    }
    foreach($toccbcc['cc_address'] as $cc){
        if(empty($cc_address)){
            $cc_address = $cc;
        }else {
            if (strpos($cc_address, $cc) === false)
                $cc_address = $cc_address . ";" . $cc;
        }
    }
    foreach($toccbcc['bcc_address'] as $bcc){
        if(empty($bcc_address)){
            $bcc_address = $bcc;
        }else {
            if (strpos($bcc_address, $bcc) === false)
                $bcc_address = $bcc_address . ";" . $bcc;
        }
    }
    require_once('modules/PM_ProcessManager/ProcessManagerEngine2.php');
    $processManagerEngine2 = new ProcessManagerEngine2();
	global $current_user;
	$userId = $current_user->id;
	$GLOBALS['current_user'] = new User();
	$GLOBALS['current_user']->retrieve($userId);
	$current_user = $GLOBALS['current_user'];		
	if ($row['send_email_to_email_queue'] == 1){
		//Send the email to the email queue
		$processManagerEngine1 = new ProcessManagerEngine1();
		$emailQueueCampaignName = $row['email_queue_campaign_name'];
		$processManagerEngine1->sendEmailToEmailQueue($focusObjectId,$focusObjectType,$emailQueueCampaignName);
		return;
	}
	require_once('modules/Emails/Email.php');
	require_once('modules/EmailTemplates/EmailTemplate.php');
	if (isset($GLOBALS['beanList']) && isset($GLOBALS['beanFiles'])) {
				global $beanFiles;
				global $beanList;
			} else {
				require_once('include/modules.php');
			}
	$userId = $current_user->id;
	$new_email = new Email();
	$emailTemplate = new EmailTemplate();
	$processManagerEngine1 = new ProcessManagerEngine1();
	$newBean = $processManagerEngine1->convertTableToBean($focusObjectType);
  if(empty($newBean)){
				return;
	} 	
	$moduleDir = $newBean->module_dir;
	$emailParentBean = $processManagerEngine1->convertTableToBean($dbTable);
  if(empty($emailParentBean)){
				return;
	}	
	$emailModuleDir = $emailParentBean->module_dir;	
	$email_template_id = $row['email_template_id'];
    if(method_exists(BeanFactory,'retrieveBean')){
    	$emailTemplate = BeanFactory::retrieveBean('EmailTemplates', $email_template_id, array('use_cache' => false, 'disable_row_level_security' => true));
    }else{
     $emailTemplate = BeanFactory::getBean('EmailTemplates', $email_template_id);
    }
		if ($emailTemplate->body_html != "") {
			$email_template_body = $emailTemplate->body_html;
			$email_type = 'HTML';
		}
		else{
            $email_type = '';
			$email_template_body = $emailTemplate->body;
		}
		$email_template_subject = $emailTemplate->subject;
		$object_arr_leads = array();
		$object_arr_leads[$moduleDir] = $focusObjectId;
		$email_template_body = $emailTemplate->parse_template($email_template_body,$object_arr_leads);
		$email_template_subject = $emailTemplate->parse_template($email_template_subject,$object_arr_leads);
		//End Patch for 7/14/2009
		//Last item to parse is the user so go and get the user object
		$object_arr_leads = array();			
		$object_arr_leads['Users'] = $current_user->id;
		$email_template_body = $emailTemplate->parse_template($email_template_body,$object_arr_leads);
		$email_template_subject = $emailTemplate->parse_template($email_template_subject,$object_arr_leads);
		//Now clear the array
		//New support for any variable data - if we still have variable data which is a $ then parse
		if (strchr($email_template_body,"$")) {
    		$email_template_body = $processManagerEngine2->parseTemplate($email_template_body,$focusObjectType,$focusObjectId,$dbTable, $dbTableID);
    	}
        if (strchr($email_template_subject,"$")) {
    		$email_template_subject = $processManagerEngine2->parseSubjectTemplate($email_template_subject,$focusObjectType,$focusObjectId,$dbTable, $dbTableID);
    	}
    	if (strchr($email_template_body,"$")) {
    		$email_template_body = $processManagerEngine2->parseSubjectTemplate($email_template_body,$focusObjectType,$focusObjectId,$dbTable, $dbTableID);
    	}
        //Legacy Workflow Email Template HREF
        if (strchr($email_template_body,"::href_link::")) {
            $email_template_body = $processManagerEngine2->parseTemplateHref($email_template_body,$focusObjectType,$focusObjectId,$dbTable, $dbTableID);
        }
        if(!empty($row['cc_email_to_address'])){
            if(!empty($cc_address))
                $cc_address = $cc_address .";" .$row['cc_email_to_address'];
            else
                $cc_address =  $row['cc_email_to_address'];
        }

			$new_email->cc_addrs_arr = array();
			$new_email->bcc_addrs_arr = array();
			$new_email->to_addrs = $to_address;
            if(isset($cc_address))
			    $new_email->cc_addrs = $cc_address;
            if(isset($bcc_address))
                $new_email->bcc_addrs = $bcc_address;
			$new_email->to_addrs_arr = $new_email->parse_addrs($to_address,'', '', '');
            if(isset($cc_address))
			    $new_email->cc_addrs_arr = $new_email->parse_addrs($cc_address, '', '', '');
			$new_email->name = $email_template_subject;
			$new_email->type = 'out';
			$new_email->parent_type = $emailModuleDir;
			$new_email->parent_id = $dbTableID;
			if ($email_type == "HTML")
			{
				$new_email->description_html = $email_template_body;
				$new_email->isHtml = true;	
			}
			else{
				$new_email->description = $email_template_body;
				$new_email->isHtml = false;	
			}
					
			//PM Pro v2 - see if the email_from_account is set and if so then get this account to send from
			$fromAccountName = $row['email_from_account'];
			if(($fromAccountName != 'Please Specify') || ($fromAccountName != NULL)){
				$fromAccountID = $this->getEmailAccountForSendingByName($fromAccountName);
			}
			else{
				$fromAccountID = $this->getEmailAccountForSending($userId);
			}
			$request = array();
			//Now check to see if there are any attachments
			//From the email_templates id we check table notes on parent_id and if there is a notes then we
			//add the notes id to the request - we already have the email template id and it is $email_template_id
			$attachmentId = $this->getTemplateAttachments($email_template_id);
			if ($attachmentId != '') {
				$_REQUEST['templateAttachments'] = $attachmentId;
				$request['templateAttachments'] = $attachmentId;
			}
			//Custom SierraCRM - get dynamic attachments for Notes Processes Only 	
            if (strchr($email_template_body,"{::attachment::}")) {
                //Make sure the file exists
                $filePath = rtrim(SugarConfig::getInstance()->get('upload_dir', 'upload'), '/\\') . '/' . $focusObjectId;
                if(file_exists($filePath)) {
                    if ($attachmentId != '') {
                        $attachmentId = $attachmentId ."::" .$focusObjectId;
                    }else{
                        $attachmentId = $focusObjectId;
                    }
                    $_REQUEST['templateAttachments'] = $attachmentId;
                    $request['templateAttachments'] = $attachmentId;
                }
                $email_template_body = str_replace("{::attachment::}","",$email_template_body);
            }
			$request['sendSubject'] = $email_template_subject;
			$request['sendDescription'] = $email_template_body;
			$request['sendTo'] = $to_address;
            if(isset($cc_address))
			    $request['sendCc'] = $cc_address;
            if(isset($bcc_address))
                $request['sendBcc'] = $bcc_address;
			//Account information is from inbound_email table
			$request['fromAccount'] = $fromAccountID;
			$request['addressFrom1'] = $fromAccountID;
			//Set parent id to be focus id
			$request['parent_id'] = $dbTableID;
			$request['parent_type'] = $emailModuleDir;
			$request['saveToSugar'] = "1";
			$request['addressTo1'] = $to_address;
			
			//******************************************************
			$_REQUEST['sendSubject'] = $email_template_subject;
			//This is the body of the email
			$_REQUEST['sendDescription'] = $email_template_body;
			$_REQUEST['sendTo'] = $to_address;
            if(isset($cc_address))
			    $_REQUEST['sendCc'] = $cc_address;
            if(isset($bcc_address))
                $_REQUEST['sendBcc'] = $bcc_address;
			$_REQUEST['setEditor'] = "1";
			$_REQUEST['sendCharset'] = "ISO-8859-1";
			$_REQUEST['addressTo2'] = $to_address;
			$_REQUEST['emailUIAction'] = "sendEmail";
			$_REQUEST['addressFrom1'] = $fromAccountID;
			$_REQUEST['fromAccount'] = $fromAccountID;
			$_REQUEST['saveToSugar'] = "1";
			$_REQUEST['addressTo1']  = $to_address;
			$_REQUEST['parent_id'] = $dbTableID;
			$_REQUEST['parent_type'] = $emailModuleDir;
			$_REQUEST['sendBcc'] = '';
			$_REQUEST['composeType'] = '';
				
			$new_email->email2init();
			$new_email->email2Send($request);
				
	unset($new_email);
}

function setToCcBccAddress($to_address,$row,$toccbcc,$type){
    $type = "^" .$type ."^";
  //If the three new PM 4.0 to,cc,bcc fields are empty - set the to_address to the to_address array and return
  if(empty($row['send_email_to']) && empty($row['send_email_cc']) && empty($row['send_email_bcc'])){
  	$toccbcc['to_address'][$to_address] = $to_address;
  	return $toccbcc;
  }
  elseif(strpos($row['send_email_to'],$type) !== FALSE){
			$toccbcc['to_address'][$to_address] = $to_address;
		}
  elseif(strpos($row['send_email_cc'],$type) !== FALSE){
			$toccbcc['cc_address'][$to_address] = $to_address;
		}
  elseif(strpos($row['send_email_bcc'],$type) !== FALSE){
			$toccbcc['bcc_address'][$to_address] = $to_address;
		}
    else{
        $toccbcc['to_address'][$to_address] = $to_address;
	}
	return $toccbcc;		
}

//PM Pro v2
//This function will return the id from the inbound_email table for the current user
function getEmailAccountForSendingByName($fromAccountName){
	$query = "Select id from inbound_email where name = '$fromAccountName' and deleted = 0";
	$resultEmailAccount = $this->db->query($query, true);
	$rowEmailAccount= $this->db->fetchByAssoc($resultEmailAccount);
	$accountId = $rowEmailAccount['id'];
	return $accountId;
}

//This function will return the id from the inbound_email table for the current user
function getEmailAccountForSending($currentUserId){	
	$query = "Select id from inbound_email where created_by = '$currentUserId' and is_personal = 1";
	$resultEmailAccount = $this->db->query($query, true);
    $rowEmailAccount= $this->db->fetchByAssoc($resultEmailAccount);
    $accountId = $rowEmailAccount['id'];
    return $accountId;	
}

//This function will check to see if there is an attachment on the template
function getTemplateAttachments($email_template_id){
		$query = "Select id from notes where parent_id = '$email_template_id' and deleted = 0";
		$resultAttachment = $this->db->query($query, true);
		$attachmentId = '';
		while($row = $this->db->fetchByAssoc($resultAttachment))
			{
				$attachmentId .= '::'. $row['id'];
			}
		return $attachmentId;
}

function getProcessDefs($process_id){
	$query = "Select * from pm_process_defs where process_id = '$process_id'";
	$resultProcessDefs = $this->db->query($query, true);
    $rowProcessDefs = $this->db->fetchByAssoc($resultProcessDefs);
	return $rowProcessDefs;
}

//****************************************************************************
//This function is passed the object type and id and we get the email address
//for sending an email - old function. New function 
//****************************************************************************

function getEmailAddress($focusObjectId,$focusObjectType){
	$GLOBALS['log']->info("Process Manager - getting the email address for focus object id $focusObjectId and focus object type $focusObjectType");	
//Return the row of data with email address and opt out if leads or contacts
//Convert Table to Bean
$processManagerEngine1 = new ProcessManagerEngine1();
$newBean = $processManagerEngine1->convertTableToBean($focusObjectType);
  if(empty($newBean)){
				return;
	}	
$focusObjectType = $newBean->table_name;			
//email_addr_bean_rel - table that holds the pointer to the new email address table
$queryAddrBeanRel = "Select email_address_id from email_addr_bean_rel where bean_id = '$focusObjectId' and primary_address = 1 and bean_module = '$focusObjectType' and deleted = 0";
$resultAddrBeanRel =& $this->db->query($queryAddrBeanRel, true);
$rowAddrBeanRel= $this->db->fetchByAssoc($resultAddrBeanRel);

$emailAddressId = $rowAddrBeanRel['email_address_id'];
//Now query the email_addresses table
$queryemail_addresses = "Select * from email_addresses where id = '$emailAddressId'";
$result_email =& $this->db->query($queryemail_addresses, true);
$row_email= $this->db->fetchByAssoc($result_email);
  //$toAddress = $row_email['email1'];
  return $row_email;
}

//******************************************************************************
//Check for any tasks that are eligible for escalation 
//******************************************************************************

function performTaskEscalation(){

	$query = "select tasks.id as taskId, tasks_cstm.initial_date_due_c as due_date, 
				    tasks_cstm.initial_time_due_c as time_due, 
						tasks.assigned_user_id as assigned_user_id, 
						tasks_cstm.is_escalatable_task_c as is_escalatable, 
						tasks_cstm.escalation_delay_minutes_c as escalation_delay_minutes,
						users.reports_to_id  as reports_to_id 
					 	from  tasks, tasks_cstm, users 
					 	where  users.reports_to_id is not null
					 	and users.id = tasks.assigned_user_id
					 	and tasks.id = tasks_cstm.id_c 
					 	and tasks_cstm.is_pm_created_task_c = 1 and tasks_cstm.is_escalatable_task_c = 1 and tasks_cstm.escalation_delay_minutes_c != '0'
					 	and (UNIX_TIMESTAMP(TIMESTAMP(tasks_cstm.initial_date_due_c , tasks_cstm.initial_time_due_c)) + tasks_cstm.escalation_delay_minutes_c * 60) < UNIX_TIMESTAMP(TIMESTAMP(UTC_DATE(), UTC_TIME()))
					 	and tasks.status != 'Completed'";

	$result = $this->db->query($query,true);
   	while($row_task = $this->db->fetchByAssoc($result))
	{	

		$taskId = 	$row_task['taskId'];
		$reportsToId = $row_task['reports_to_id'];
		if((!is_null($reportsToId)) || ($reportsToId != '')){
			$task_update_query = " update tasks set assigned_user_id = '".$reportsToId."' where id = '".$taskId."'";
			$this->db->query($task_update_query,true);
		}
	}
}

//*********************************************************************
//Check for Calls Eligible for Escalation
//*********************************************************************
function performCallEscalation(){

	$query = "select calls.id as callId, calls_cstm.initial_date_start_c as date_start, 
						calls_cstm.initial_time_start_c as time_start, 
						calls.assigned_user_id as assigned_user_id, 
						calls_cstm.is_escalatable_call_c as is_escalatable, 
						calls_cstm.escalation_delay_minutes_c as escalation_delay_minutes,
						users.reports_to_id  as reports_to_id 
					 	from  calls, calls_cstm, users
					 	where  users.reports_to_id is not null 
					 	and users.id = calls.assigned_user_id
					 	and calls.id = calls_cstm.id_c
					 	and calls_cstm.is_pm_created_call_c = 1 and calls_cstm.is_escalatable_call_c = 1 
					 	and calls_cstm.escalation_delay_minutes_c != '0'
					 	and (UNIX_TIMESTAMP(TIMESTAMP(calls_cstm.initial_date_start_c , calls_cstm.initial_time_start_c)) + calls_cstm.escalation_delay_minutes_c * 60) < UNIX_TIMESTAMP(TIMESTAMP(UTC_DATE(), UTC_TIME()))
					 	and calls.status != 'Held'";

	$result = $this->db->query($query,true);
   	while($row_call = $this->db->fetchByAssoc($result))
	{	

		$taskId = 	$row_call['callId'];
		$reportsToId = $row_call['reports_to_id'];
		if((!is_null($reportsToId)) || ($reportsToId != '')){
			$task_update_query = " update calls set assigned_user_id = '".$reportsToId."' where id = '".$taskId."'";
			$this->db->query($task_update_query,true);
		}
	}
}
//***********************************************************
//Get the id from the user table based on the user_name
//***********************************************************
function getUserIdByName($username){
	$query = "Select id from users where user_name = '$username'";
	$result = $this->db->query($query);
	$row = $this->db->fetchByAssoc($result);
	$userId = $row['id'];
	return $userId;
	
}
//******************************************************************
//This function checks to see if there is an active process for 
//the focus object.
//******************************************************************

function checkFocusProcess(){
    return TRUE;
	require_once ('config.php'); // provides $sugar_config
	global $sugar_config;
	$configOptions = $sugar_config['dbconfig'];
	$dbName = $configOptions['db_name'];
	$dbName = md5($dbName);
	$key = $sugar_config['unique_key'];
	$key = $dbName ."_" .$key;
	$queryProcess = "Select value from config where (category = 'PM_Pro' OR category = 'PM_Ent') AND (name = 'PM_Prod_Key' OR name = 'PM_Dev_Key')";
	$result = $this->db->query($queryProcess);
	$row = $this->db->fetchByAssoc($result);
	$name = $row['value'];
	if ($name == $key) {
		return true;
	}
	else{	
		return false;
	}
	
	
}

//************************************************************
//This function will insert a record into the call_contacts
//table for a new call
//************************************************************

function loadCallsContacts($focusObjectId,$newCallid,$today){
$newCallsContactsId = create_guid();
$newCallsContactsQuery = "Insert into calls_contacts (`id`,`call_id`,`contact_id`,`required`,`accept_status`,`date_modified`,`deleted`) VALUES ('$newCallsContactsId','$newCallid','$focusObjectId','1','none','$today','0')";
$this->db->query($newCallsContactsQuery);
}

//***************************************************************************************************
//This function will get the entry in the filter table for the given process
//***************************************************************************************************

function getProcessFilterTableEntry($process_id){
	$query = "Select * from pm_process_filter_table where process_id = '" .$process_id ."' ORDER BY sequence ASC";
	$result = $this->db->query($query);
	return $result;
}

function getProcessCancelFilterTableEntry($process_id){
	$query = "Select * from pm_process_cancel_filter_table where process_id = '" .$process_id ."' ORDER BY sequence ASC";
	$result = $this->db->query($query);
	return $result;
}

//**********************************************************************************
//Generic Count Function
//*********************************************************************************
function getCount($sql){
	$counter = 0;
	$result = $this->db->query($sql);
	while($row = $this->db->fetchByAssoc($result))
		{
			$counter = $counter + 1;
		}
	return $counter;
}

//***************************************************************************
//Gets the info for Scheduling a Meeting
//***************************************************************************
function runScheduleMeetingTask($processID,$stageID,$rowTask,$focusObjectId,$focusObjectType){
		//Go and get the defs record from pm_process_task_call_defs
		global $current_user;
		global $previousTaskId;
		$taskId = $rowTask['id'];
		$GLOBALS['log']->info("Process Manager - Creating a new Scheduled Meeting for object type $focusObjectType and object id $focusObjectId and task id $taskId and process id $processID and stage id $stageID");	
		$taskOrder = $rowTask['task_order'];
		$queryTaskMeetingDefs = "Select * from pm_process_task_meeting_defs where task_id = '" .$taskId ."'";
		$resultTaskMeetingDefs = $this->db->query($queryTaskMeetingDefs);
		$rowTaskMeetingDefs = $this->db->fetchByAssoc($resultTaskMeetingDefs);
		if ($rowTaskMeetingDefs) {
			if($rowTaskMeetingDefs['start_delay_type'] == 'From Completion of Previous Task'){	
				$this->insertTaskIntoWaitingTable($processID,$stageID,$focusObjectId,$focusObjectType,$taskId,$taskOrder,'meetings');
				return;
			}			
			//Get the delay type
			$focusFieldsArray = array();
			if ($rowTaskMeetingDefs['start_delay_type'] == 'Create') {
				$focusFieldsArray['date_entered'] = 'date_entered';
				$arrayFieldsFromFocusObject = $this->getFocusObjectFields($focusObjectId,$focusObjectType,$focusFieldsArray);
				$focusObjectCreateDate = $arrayFieldsFromFocusObject['date_entered'];
			}
			else{
				$focusFieldsArray['date_modified'] = 'date_modified';
				$arrayFieldsFromFocusObject = $this->getFocusObjectFields($focusObjectId,$focusObjectType,$focusFieldsArray);
				$focusObjectCreateDate = $arrayFieldsFromFocusObject['date_modified'];
			}			
				$newCallTimeStart = $this->getNewCallTimeStart($focusObjectCreateDate,$rowTaskMeetingDefs['start_delay_years'],$rowTaskMeetingDefs['start_delay_months'],$rowTaskMeetingDefs['start_delay_days'],$rowTaskMeetingDefs['start_delay_hours'],$rowTaskMeetingDefs['start_delay_minutes']);
				$this->createNewMeetingTask($focusObjectId,$focusObjectType,$rowTaskMeetingDefs,$newCallTimeStart,$focusObjectCreateDate);			
		}		
	}
	
//*************************************************************************

//***************************************************************************
//This function is called by runScheduleCallTask to insert the new call data
//We pass the focus object and focus type, the row of call defs and also
//call time start.
//Iff the call is for a lead then we set parent type and parent id
//If the call is for a contact then we relate with calls_contacts table
//Also we parse the call start time to get the date and time 
//***************************************************************************

function createNewMeetingTask($focusObjectId,$focusObjectType,$rowTaskMeetingDefs,$newMeetingTimeStart,$focusObjectCreateDate){
	global $previousTaskId;
	global $current_user;
	require_once('modules/Meetings/Meeting.php');
	$newMeeting = new Meeting();
	$timezone = date('Z') / 3600;
	$timezone = substr($timezone,1);
	$today = date('Y-m-d H:i:s', time() + $timezone * 60 * 60);

	$newMeetingUserid = create_guid();
	$spaceLocation = strpos($newMeetingTimeStart," ");
	$meetingStartDate = substr($newMeetingTimeStart,0,$spaceLocation);
	$meetingStartTime = substr($newMeetingTimeStart,$spaceLocation);
	if ($focusObjectType == 'leads') {
		$focusObjectType = 'Leads';
	}
	if ($focusObjectType == 'opportunities') {
		$focusObjectType = 'Opportunities';
	}
	if ($focusObjectType == 'cases') {
		$focusObjectType = 'Cases';
	}
	if ($focusObjectType == 'quotes') {
		$focusObjectType = 'Quotes';
	}	
		//Is the assigned_user_call_id set in the def record?
	if($rowTaskMeetingDefs['assigned_user_id_meeting'] != ""){
		$assignedUserMeetingId = $rowTaskMeetingDefs['assigned_user_id_meeting'];
		$assignedUserMeetingId = $this->getUserIdByName($assignedUserMeetingId);
	}
	else{		
		$assignedUserMeetingId	= $current_user->id;
	}
	//Build the Meeting object and Save
	$newMeeting->name = $rowTaskMeetingDefs['meeting_subject'];
	//Description
	$newMeeting->description = $rowTaskMeetingDefs['meeting_description'];
	//Location
	$newMeeting->location = $rowTaskMeetingDefs['meeting_location'];
	//Duration
	$newMeeting->duration_hours = 0;
	$newMeeting->duration_minutes = 15;
	$newMeeting->date_start = $newMeetingTimeStart;
	$newMeeting->date_end = $meetingStartDate;
	$newMeeting->parent_type = $focusObjectType;
	$newMeeting->status = "Planned";
	$newMeeting->parent_id = $focusObjectId;
	$newMeeting->assigned_user_id = $assignedUserMeetingId;	
	$newMeeting->save(true);
	$newMeetingId = $newMeeting->id;
	$previousTaskId = $newMeeting->id;

	//Due to unforseen circumstances we have to update the new record for meetings for the date start/time
	$queryUpdateMeetings = "update meetings set date_start = '$newMeetingTimeStart', date_end = '$meetingStartDate' where id = '$newMeetingId'";
	$this->db->query($queryUpdateMeetings);
	
	//Now insert into meetings_user
	$newMeetingUserQuery = "Insert into meetings_users (`id`,`meeting_id`,`user_id`,`required`,`accept_status`,`date_modified`,`deleted`) VALUES ('$newMeetingUserid','$newMeetingId', '$assignedUserMeetingId','1','accept','$today',0)";
	$this->db->query($newMeetingUserQuery);	
	return $newMeeting->id;
}

//***********************************************************************************************
//This function is used to determine if we pass the filter test
//***********************************************************************************************

function getFilterTestResult($passFilterTest,$field,$fieldOperator,$value,$focusObjectId,$focusObjectType,$focusFieldsArray,$valueTo, $rowProcessFilterTable){
    $GLOBALS['log']->info("ProcessManager - Getting Filter Test Results for field $field: field operator - $fieldOperator: value $value: " );
	//Determine if this is a custom field - first get the length
	$lengthOfField = strlen($field);
	$start = 	$lengthOfField - 2;
	$checkCustomField = substr($field,$start);
	$table = $focusObjectType;
	//Convert user name to id for created_by, assigned_user_id and other owner fields
	if(($field == 'created_by') || ($field == 'modified_user_id') || ($field == 'assigned_user_id')){
				$value = $this->getUserIdByName($value);
	}
	/*	
	if ($checkCustomField == '_c') {
		//So now we have a custom field								    
			$arrayFieldsFromFocusObject = $this->getFocusObjectCustomFields($focusObjectId,$focusObjectType,$focusFieldsArray);
			$fieldValueFromFocusObect = $arrayFieldsFromFocusObject[$field];
			$table .= '_cstm';
			
	}
	else{
			$arrayFieldsFromFocusObject = $this->getFocusObjectFields($focusObjectId,$focusObjectType,$focusFieldsArray);
			$fieldValueFromFocusObect = $arrayFieldsFromFocusObject[$field];
	}
	*/
	$processManagerEngine1 = new ProcessManagerEngine1();
	$bean = $processManagerEngine1->convertTableToBean($table);
   //$bean = BeanFactory::retrieveBean($bean->module_name, $focusObjectId, array('disable_row_level_security' => true));
	$bean->retrieve($focusObjectId);
	$fieldValueFromFocusObject = $bean->$field;	
	//If field is team_id for SugarPro - get the comma delimted list of teams from teamset
	if($field == 'team_set_id'){
		require_once('modules/Teams/TeamSetManager.php');
		//Get the team id from the object
		$queryTeamSet = "select team_set_id, team_id from $focusObjectType where id = '$focusObjectId'";
		$resultTeamSet = $this->db->query($queryTeamSet,true);
		$rowTeamSet = $this->db->fetchByAssoc($resultTeamSet);
		$teamSet = $rowTeamSet['team_set_id'];
		$teamID = $rowTeamSet['team_id'];
		$fieldValueFromFocusObject =  TeamSetManager::getCommaDelimitedTeams($teamSet, $teamID, false);
	}	
	//New for PM 6.2 - any change
	if ($fieldOperator == 'any change'){
		   require_once('modules/PM_ProcessManager/ProcessManagerEngine2.php');
	      $processManagerEngine2 = new ProcessManagerEngine2();
		  if($processManagerEngine2->checkFieldForAnyChange($field,$focusObjectId,$focusObjectType)){	
		  	return true;	
		  }
		  else{
		  	return false;
		  }
	}
	if ($fieldOperator == 'from to'){
		require_once('modules/PM_ProcessManager/ProcessManagerEngine2.php');
		$processManagerEngine2 = new ProcessManagerEngine2();
		if($processManagerEngine2->checkFieldFromTo($field,$focusObjectId,$focusObjectType,$value,$valueTo)){
			return true;
		}
		else{
			return false;
		}
	}	
	//New for PM 6.2 - see if the filter operator if to check for a date field of today or in x days or in the past x days
	 if ($fieldOperator == 'date field due today' xor $fieldOperator == 'date field due in x days' xor $fieldOperator == 'time field due in x hours' xor $fieldOperator == 'date field due in the past x days' xor $fieldOperator == 'day is today' xor $fieldOperator == 'day and month is today' xor $fieldOperator == 'date is less than' xor $fieldOperator == 'date is greater than') {
        $today = date('Y-m-d H:i:s');
        $dateArray = explode(" ",$today);
        $date = $dateArray[0];
        $time = $dateArray[1];
        $d = explode("-",$date);
        $year = $d[0];
        $month = $d[1];
        $day = $d[2];
		//list ($year, $month, $day, $hour, $min, $sec) = split ('[- :]', $today);
		$dataType = $this->getDataType($table,$field);
		if ($dataType == 'datetime' xor $dataType == 'datetimecombo') {
            $dateArray = explode(" ",date("Y-m-d H:i:s",strtotime($fieldValueFromFocusObject)));
            $date = $dateArray[0];
            $time = $dateArray[1];
            //Now split the date and time
            $d = explode("-",$date);
            $fieldValueYear = $d[0];
            $fieldValueMonth = $d[1];
            $fieldValueDay = $d[2];
            $t = explode(":",$time);
            $hour = $t[0];
            $min = $t[1];
            $sec = $t[2];
			//list ($fieldValueYear, $fieldValueMonth, $fieldValueDay, $hour, $min, $sec) = split ('[- :]', $fieldValueFromFocusObject);
		}
		if ($dataType == 'date') {
            $dateArray = explode("-",date("Y-m-d",strtotime($fieldValueFromFocusObject)));
            $fieldValueYear = $dateArray[0];
            $fieldValueMonth = $dateArray[1];
            $fieldValueDay = $dateArray[2];
			//list ($fieldValueYear, $fieldValueMonth, $fieldValueDay) = split ('[-]', date("Y-m-d",strtotime($fieldValueFromFocusObject));
		}
         if ($fieldOperator == 'day is today') {
             if ($day == $fieldValueDay) {
                 return true;
             }
             else{
                 return false;
             }
         }
         if ($fieldOperator == 'day and month is today') {
             if ($day == $fieldValueDay && $month == $fieldValueMonth) {
                 return true;
             }
             else{
                 return false;
             }
         }
		if ($fieldOperator == 'date field due today') {
            if ($day == $fieldValueDay && $year == $fieldValueYear && $month == $fieldValueMonth) {
				return true;
			}
			else{
				return false;
			}
		}
		if ($fieldOperator ==  'date field due in x days') {
			$valueStrtoTime = strtotime($fieldValueFromFocusObject);
			$todayStrtoTime = strtotime($today);
			$diffStrtoTime = $valueStrtoTime - $todayStrtoTime;
			$deltaTime = date('Y-m-d H:i:s',$diffStrtoTime);
			list ($yeardeltaTime, $monthdeltaTime, $daydeltaTime, $hour, $min, $sec) = split ('[- :]', $deltaTime);
			if ($daydeltaTime == $value) {
				return true;
			}
			else{
				return false;
			}
		}
		if ($fieldOperator ==  'time field due in x hours') {
			$valueStrtoTime = strtotime($fieldValueFromFocusObject);
			$todayStrtoTime = strtotime($today);
			$diffStrtoTime = $valueStrtoTime - $todayStrtoTime;
      $deltaTime = $diffStrtoTime / 60;
      $deltaTime = $deltaTime / 60;
     if($deltaTime > 0) {
         if ($deltaTime < $value) {
            return true;
         } else {
            return false;
         }
      }
      else{
          return false;
      }
		}		
		if ($fieldOperator ==  'date field due in the past x days') {
			$valueStrtoTime = strtotime($fieldValueFromFocusObject);
			$todayStrtoTime = strtotime($today);
			$diffStrtoTime = $todayStrtoTime - $valueStrtoTime;
			$deltaTime = date('Y-m-d H:i:s',$diffStrtoTime);
			list ($yeardeltaTime, $monthdeltaTime, $daydeltaTime, $hour, $min, $sec) = split ('[- :]', $deltaTime);
			if ($daydeltaTime == $value) {
				return true;
			}
			else{
				return false;
			}
		}
         if ($fieldOperator ==  'date is less than') {
             $fieldvalueStrtoTime = strtotime($fieldValueFromFocusObject);
             $valueStrtoTime = strtotime($value);
             if ($fieldvalueStrtoTime < $valueStrtoTime) {
                 return true;
             }
             else{
                 return false;
             }
         }
         if ($fieldOperator ==  'date is greater than') {
             $fieldvalueStrtoTime = strtotime($fieldValueFromFocusObject);
             $valueStrtoTime = strtotime($value);
             if ($fieldvalueStrtoTime > $valueStrtoTime) {
                 return true;
             }
             else{
                 return false;
             }
         }
	 }
    if(isset($rowProcessFilterTable['compare_to_field'])){
        if ($rowProcessFilterTable['compare_to_field'] == 1) {
            $value = $bean->$rowProcessFilterTable['compare_to_field_name'];
        }
    }
	$dataType = $this->getDataType($table,$field);
	if ($dataType == 'datetime') {
		//Return just the y-m-d
		$fieldValueFromFocusObject = strtotime($fieldValueFromFocusObject);
		$value = strtotime($value);
	}
    if ($fieldOperator == '=') {
		if($value != $fieldValueFromFocusObject){
			$passFilterTest = false;		
		}
    }
    if ($fieldOperator == '!=') {
    	    if($value == $fieldValueFromFocusObject){	
				$passFilterTest = false;		
			}
    }
    //Patch 12/4/2009
    if (($fieldOperator == '&lt;') || ($fieldOperator == '<')) {
    			if($value < $fieldValueFromFocusObject){	
				$passFilterTest = false;		
			}
    }

    //Patch 4/23/2015 Less Than or Equal To
    if (($fieldOperator == '&lt;=') || ($fieldOperator == '<=')) {
        if($value < $fieldValueFromFocusObject || ($value == $fieldValueFromFocusObject)){
            $passFilterTest = false;
        }
    }
    //Patch 12/4/2009
    if (($fieldOperator == '&gt;') || ($fieldOperator == '>')) {
    			if($value > $fieldValueFromFocusObject){	
				$passFilterTest = false;		
			}
    }

    if (($fieldOperator == '&gt;=') || ($fieldOperator == '>=')) {
        if(($value > $fieldValueFromFocusObject) || ($value == $fieldValueFromFocusObject)){
            $passFilterTest = false;
        }
    }
    //Patch 03/26/2010
    if ($fieldOperator == 'contains') {
    		if (strstr($fieldValueFromFocusObject, $value) === false) {
    			$passFilterTest = false;
    		}
    }
	//Patch 03/26/2010
    if ($fieldOperator == 'does not contain') {
    		if (strstr($fieldValueFromFocusObject, $value) !== false) {
    			$passFilterTest = false;
    		}
    }
    if ($fieldOperator == 'any value') {
    		if (($fieldValueFromFocusObject == '') || ($fieldValueFromFocusObject === NULL)) {
    			$passFilterTest = false;
    		}
    }
    
    //PM Pro 2.0 - is null
    if ($fieldOperator == 'is null') {
    	if (!is_null($fieldValueFromFocusObject)) {
    		$passFilterTest = false;
    	}
    }
    if ($fieldOperator == 'is empty') {
        if (!empty($fieldValueFromFocusObject)) {
            $passFilterTest = false;
        }
    }
	return $passFilterTest;
}

//Return the data type
function getDataType($table,$field){
	//Convert Table to Bean and get the types for the fields
	$processManagerEngine1 = new ProcessManagerEngine1();
	$bean = $processManagerEngine1->convertTableToBean($table);
	if(empty($bean)){
		return;
	}	
	$exclude = array(
			'id',
			//'date_entered',
			//'date_modified'
	);	
	foreach($bean->field_defs as $def){
		if(!(isset($def['source']) && $def['source'] == 'non-db') && !empty($def['name']) && !in_array($def['name'], $exclude)){
			$fieldName = $def['name'];
			if ($fieldName == $field) {
				$dataType = $def['type'];
			}			
		}
	}
	return $dataType;
}

//******************************************************************************
//This function will retrieve the email address and id from the contact table
//for the given opp. 
//*****************************************************************************

function getAccountOppEmails($focusObjectId,$focusObjectType){
	$GLOBALS['log']->info("ProcessManager - Getting Account Opp Emails for focus object id  " .$focusObjectId);
	$query_opps_accounts = "Select account_id from accounts_opportunities where opportunity_id  = '" .$focusObjectId ."' and deleted = 0";
	$result_opps_accounts =& $this->db->query($query_opps_accounts, true);
	$row = $this->db->fetchByAssoc($result_opps_accounts);
	if ($row) {
		$queryAccount = "Select id from accounts where id = '" .$row['account_id'] ."'";
		$resultAccount = $this->db->query($queryAccount, true);
		$rowAccount = $this->db->fetchByAssoc($resultAccount);
		if ($rowAccount) {
			return $rowAccount;
		}
	}
	
}

function convertBeanToTable($focusObjectType){
	global $moduleList;
	global $beanList;
	$exemptModules = array();
	$exemptModules['Calendar.php'] = "Calendar.php";
	foreach ($moduleList as $key=>$value){
		$module_array = array();
		$module_array[$key]=$value;
		$module_array = convert_module_to_singular($module_array);
		if($key != 0){
			$key1 = $module_array[$key];
			$bean = $key1;
			$key1 .= ".php";
			$beanFile = "modules/$value/$key1";
			require_once "$beanFile";
			if (file_exists("$beanFile")) {
				if (!in_array($key1,$exemptModules)) {
					if ($bean == "Case") {
						$bean = "aCase";
					}
					require_once("$beanFile");
					$newbean = new $bean;
					$objectName = $newbean->module_dir;
					if ($focusObjectType == $objectName) {
						return $newbean;
					}
				}
			}
		}
	}
}

function checkFilterFields($process_id,$related_object, $focusObjectId, $focusObjectType){
	  require_once('modules/PM_ProcessManager/ProcessManagerEngine1.php');
	  require_once('modules/PM_ProcessManager/ProcessManagerEngine2.php');
		$processManagerEngine1 = new ProcessManagerEngine1();
		$processManagerEngine2 = new ProcessManagerEngine2();		
		$andOrFilterFieldsArray = array();
		$groupArray = array();
		$andOrGroupArray = array();				
		$resultProcessFilterTable = $this->getProcessFilterTableEntry($process_id);	
		$passFilterTest = true;
		while($rowProcessFilterTable =  $this->db->fetchByAssoc($resultProcessFilterTable)){
			//PATCH 11-3-2011 - we need to set the pass filter test true for each filter entry and let the code set to false
			$passFilterTest = true;											
			//Get the and/or filter fields entry
			$andOrFilterFields = $rowProcessFilterTable['andorfilterfields'];
			if(empty($andOrFilterFields)) $andOrFilterFields = 'and';
			$groupNumber = $rowProcessFilterTable['group_number'];
			$groupAndOr = $rowProcessFilterTable['group_andor'];
			
			$groupArray[$groupNumber] = $groupAndOr;					
			$focusFieldsArray = array();
			$field = $rowProcessFilterTable['field_name'];
			$value = trim($rowProcessFilterTable['field_value']);
			//Get the Filter Operator: equal, not equal, less than, greater than
			$fieldOperator = $rowProcessFilterTable['field_operator'];
			$valueTo = $rowProcessFilterTable['field_value_to'];
			$focusFieldsArray[$field] = $field;
			if ($field != '' ) {
					$GLOBALS['log']->info("Process Manager - Checking to see if object passes filter test for object type $focusObjectType and object id $focusObjectId");	
					$pos = strpos($value, ",");
					if($pos === false){
						$passFilterTest = $this->getFilterTestResult($passFilterTest,$field,$fieldOperator,$value,$focusObjectId,$focusObjectType,$focusFieldsArray,$valueTo, $rowProcessFilterTable);
					}
					else{
						//get all the values via the list command
						$values = explode(",", $value);
						$commaSeparatedValuesArray = array();
						foreach($values as $key=>$value){
							$passFilterTest = true;
							$passFilterTest = $this->getFilterTestResult($passFilterTest,$field,$fieldOperator,$value,$focusObjectId,$focusObjectType,$focusFieldsArray,$valueTo, $rowProcessFilterTable);
							$commaSeparatedValuesArray[$key] = $passFilterTest;
						}
						//Now see if we passed
						if (in_array("1",$commaSeparatedValuesArray)) {
							$passFilterTest = true;
						}									
					}	
			}
			if($passFilterTest){
				$passFilterTestVal = "1";
			}else{
				$passFilterTestVal = "0";
			}
			$andOrFilterFieldsArray[$groupNumber][$groupAndOr][$andOrFilterFields][] = $passFilterTestVal;							
		}
		foreach($andOrFilterFieldsArray as $group=>$set){
			foreach($set as $groupAndOrValue=>$filterTestGroup){
			    foreach($filterTestGroup as $andOrValue=>$passFilterTestArray){
			    	if(count($passFilterTestArray) == 1){
			    		$passSingleFilterTest = (string)$passFilterTestArray[0];
			    		$andOrGroupArray[$groupAndOrValue][] = $passSingleFilterTest;
			    		break;
			    	}
			        if($andOrValue == 'or'){
				        $bool = "1";
				        $passSingleFilterTest = "0";
				        if(in_array($bool, $passFilterTestArray)) {
				        	$passSingleFilterTest = "1";
				        }
			        }else{
				        $bool = "0";
				        $passSingleFilterTest = "1";
				        foreach($passFilterTestArray as $val){
				        	if($val == $bool){
				        		$passSingleFilterTest = "0";
				        		break;
				        	}
				        }
			        }
			        $andOrGroupArray[$groupAndOrValue][] = $passSingleFilterTest;
				}	
			}	
		}
		if(count($andOrFilterFieldsArray) > 0){
			$passFilterTest = false;
			foreach($andOrGroupArray as $groupAndOrValue => $passGroupTestArray){
				if($groupAndOrValue == 'or'){
			        $bool = "1";
			        if(in_array($bool, $passGroupTestArray)) {
			        	$passFilterTest = true;
			        }
		        }else{
			        $bool = "0";
			        $passFilterTest = true;
			        foreach($passGroupTestArray as $val){
			        	if($val == $bool){
			        		$passFilterTest = false;
			        		break;
			        	}
			        }
		        }
			}
		}
		//PM Pro V2
		if($related_object != ''){
			if($related_object != NULL){
				//For Each Related Object - run the test - go and get all the
				$queryRelatedFilterTable = "select * from pm_process_related_filter_table where process_id = '$process_id' ";
				$resultRelatedFilterTable = $this->db->query($queryRelatedFilterTable);
				while($rowRelatedFilterTable = $this->db->fetchByAssoc($resultRelatedFilterTable)){
					$relatedFilterID = $rowRelatedFilterTable['id'];
					$related_object_filter_table = $rowRelatedFilterTable['related_module'];
					//Legacy Check
					if($related_object_filter_table == ''){
						$related_object_filter_table = $related_object;
					}
					$passFilterTest = $processManagerEngine2->checkRelatedFilterFields($passFilterTest,$relatedFilterID,$process_id,$related_object_filter_table,$focusObjectId,$focusObjectType);
				}
			}
		}
		if ($passFilterTest) {
			$GLOBALS['log']->info("Process Manager - Object passes filter test for object type $focusObjectType and object id $focusObjectId");	
			//Check for Routing Object Process
			$rowProcessDefs = $this->getProcessDefs($process_id);
					$processStagesResult = $this->getProcessStages($process_id);
					$processStagesResultCount = $this->getCount("Select pm_processmanagerstage_idb from pm_processmmanagerstage where pm_processmanager_ida = '" .$process_id ."' and deleted = 0");

					$resultOrderedStages = $this->getOrderedStages($processStagesResult,$processStagesResultCount);
					if ($resultOrderedStages != '') {
						//Here we have a result set of ordered stages for the process
						//We get each row and see if there is a start delay if so then we place the stage info
						//in the pm_process_stage_waiting_todo
						while($row_stage = $this->db->fetchByAssoc($resultOrderedStages)){
							if ($row_stage['start_delay_minutes'] != 0 || $row_stage['start_delay_hours'] != 0 || $row_stage['start_delay_days'] != 0 || $row_stage['start_delay_months'] != 0 || $row_stage['start_delay_years'] != 0) {
								$checkDefaultProcess = true;
								$GLOBALS['log']->info("Process Manager -Loading Delayed Stage for object type $focusObjectType and object id $focusObjectId and process id $process_id");
								$this->loadDelayedStage($focusObjectId,$focusObjectType,$process_id,$row_stage);
							}
							else{
								$stage_id = $row_stage['id'];
								$resultStageTaskIds = $this->getStageTasks($row_stage);
								$resultStageTaskIdsCounter = $this->getStageTasks($row_stage);
									if ($resultStageTaskIds != "") {
										$checkDefaultProcess = true;
										$GLOBALS['log']->info("Process Manager Doing Stage Tasks for object type $focusObjectType and object id $focusObjectId and process id $process_id and stage id $stage_id");	
										try {
											if($this->checkFocusProcess()){
												$this->doStageTasks($process_id,$stage_id,$resultStageTaskIds,$resultStageTaskIdsCounter,$focusObjectId,$focusObjectType);
											}
										} catch (Exception $e) {
										   $errorMessage = $e->getMessage();
									       $GLOBALS['log']->info("Process Manager Error caught doing stage tasks for object type $focusObjectType and object id $focusObjectId and process id $process_id and stage id $stage_id and error message is $errorMessage");
										}
								}
                                $this->insertIntoProcessCompleted($focusObjectId,$process_id, $stage_id,$focusObjectType);
							}
						}
					}
	}	
}

function checkCancelFilterFields($process_id,$related_object, $focusObjectId, $focusObjectType){
	  require_once('modules/PM_ProcessManager/ProcessManagerEngine1.php');
	  require_once('modules/PM_ProcessManager/ProcessManagerEngine2.php');
		$processManagerEngine1 = new ProcessManagerEngine1();
		$processManagerEngine2 = new ProcessManagerEngine2();		
		$andOrFilterFieldsArray = array();
		// = array();
		$andOrGroupArray = array();				
		$resultProcessCancelFilterTable = $this->getProcessCancelFilterTableEntry($process_id);	
		while($rowProcessCancelFilterTable =  $this->db->fetchByAssoc($resultProcessCancelFilterTable)){	
			//PATCH 11-3-2011 - we need to set the pass filter test true for each filter entry and let the code set to false
			$passFilterTest = true;											
			//Get the and/or filter fields entry
			$andOrFilterFields = $rowProcessCancelFilterTable['cancelandorfilterfields'];
			if(empty($andOrFilterFields)) $andOrFilterFields = 'and';
			//$groupNumber = $rowProcessCancelFilterTable['group_number'];
			//$groupAndOr = $rowProcessCancelFilterTable['group_andor'];
			//$groupArray[$groupNumber] = $groupAndOr;
			$focusFieldsArray = array();
			$field = $rowProcessCancelFilterTable['cancel_field_name'];
			$value = trim($rowProcessCancelFilterTable['cancel_field_value']);
			//Get the Filter Operator: equal, not equal, less than, greater than
			$fieldOperator = $rowProcessCancelFilterTable['cancel_field_operator'];
			$valueTo = $rowProcessCancelFilterTable['cancel_field_value_to'];
			$focusFieldsArray[$field] = $field;
			if ($field != '' ) {
					$GLOBALS['log']->info("Process Manager - Checking to see if object passes cancel filter test for object type $focusObjectType and object id $focusObjectId");	
					$pos = strpos($value, ",");
					if($pos === false){
						$passFilterTest = $this->getFilterTestResult($passFilterTest,$field,$fieldOperator,$value,$focusObjectId,$focusObjectType,$focusFieldsArray,$valueTo, $rowProcessCancelFilterTable);
					}
					else{
						//get all the values via the list command
						$values = explode(",", $value);
						$commaSeparatedValuesArray = array();
						foreach($values as $key=>$value){
							$passFilterTest = true;
							$passFilterTest = $this->getFilterTestResult($passFilterTest,$field,$fieldOperator,$value,$focusObjectId,$focusObjectType,$focusFieldsArray,$valueTo, $rowProcessCancelFilterTable);
							$commaSeparatedValuesArray[$key] = $passFilterTest;
						}
						//Now see if we passed
						if (in_array("1",$commaSeparatedValuesArray)) {
							$passFilterTest = true;
						}									
					}	
			}
			if($passFilterTest){
				$passFilterTestVal = "1";
			}else{
				$passFilterTestVal = "0";
			}
			$andOrFilterFieldsArray[$andOrFilterFields][] = $passFilterTestVal;
		}

		if(count($andOrFilterFieldsArray) > 0){
			$passFilterTest = false;
			foreach($andOrFilterFieldsArray as $AndOrValue => $passGroupTestArray){
				if($AndOrValue == 'or'){
			        $bool = "1";
			        if(in_array($bool, $passGroupTestArray)) {
			        	$passFilterTest = true;
                        break;
			        }
		        }else{
			        $bool = "0";
			        $passFilterTest = true;
			        foreach($passGroupTestArray as $val){
			        	if($val == $bool){
			        		$passFilterTest = false;
			        		break;
			        	}
			        }
		        }
			}
		}
		if ($passFilterTest) {
			//Cancel the Process
			$this->cancelProcess($focusObjectId,$focusObjectType,$process_id,'Cancelled');
		}					
}

    function getAuditTableEntry($focusObjectType,$focusObjectId,$field,$before_after){
        //Get the Field
         $focusObjectType = $focusObjectType ."_audit";
        $queryAuditTable = "select * from $focusObjectType where parent_id = '$focusObjectId' and field_name = '$field' order by date_created DESC";
        $resultAuditTable = $this->db->query($queryAuditTable, true);
        $rowAuditTable = $this->db->fetchByAssoc($resultAuditTable);
                if($before_after == 'before')
                return $rowAuditTable['before_value_string'];
                else
                return $rowAuditTable['after_value_string'];
    }

}
?>
