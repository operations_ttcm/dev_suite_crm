<?PHP

//Code to return the Browse button for upload custom script
function upload_custom_script(){
	$fields = "<input type='file' name='customScript'/>";
	return $fields;
}

//New fields for modify object field
function getDetailViewProcessModifyField($focus, $field, $value, $view){
	$focusid = $focus->id;
	$field = substr($field,12);
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_task_modify_field_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$field];
	if ($field != '') {
		$fields = $field; 

	}
	else{
		$fields = '';
	}
	return $fields;
}

function getProcessModifyFieldValue($focus, $field, $value, $view){
	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_task_modify_field_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList["$field"];
	if ($fieldValue != '') {
		$fields = "<textarea name='$field' id='$field'   value='$fieldValue' title='' tabindex='6' rows='3' cols='55' >$fieldValue</textarea> ";
	}
	else{
		$fields = "<textarea name='$field' id='$field'   value='' title='' tabindex='6' rows='3' cols='55' ></textarea> ";
	}
	return $fields;
}

function getProcessModifyField($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed


	$filterListName = "process_object_modify_field";
	$fields = '<name="process_object_modify_field"  size="10" multiple="multiple">';
	if ($view != 'DetailView') {	
		require_once('modules/PM_ProcessManager/ProcessManagerUtils.php');
		$processManagerUtil = new ProcessManagerUtils();
		if ($focus->id != "") {
			//Get the process object for the task
			$queryFocusObject = "select pm.process_object, pm.id as process_id, pmstage.id as stage_id, pmst.pm_processmanager_ida, 
			        pmst.pm_processmanagerstage_idb, pmtask.id as pmtask_id, pmstagetask.pm_processmanagerstage_ida, pmstagetask.pm_processmanagerstagetask_idb
					from pm_processmanager pm, pm_processmanagerstage pmstage, pm_processmmanagerstage pmst, pm_processmanagerstagetask pmtask,
					pm_processmgerstagetask pmstagetask
					where pmstagetask.pm_processmanagerstagetask_idb = '$focus->id' and pmstagetask.pm_processmanagerstage_ida = pmst.pm_processmanagerstage_idb
					and pmst.pm_processmanager_ida = pm.id";
			$resultFocusObject = $processManagerUtil->db->query($queryFocusObject, true);
			while($rowfocusObject = $processManagerUtil->db->fetchByAssoc($resultFocusObject)){
				$table = $rowfocusObject['process_object'];
			}
			//Now see if we have an existing entry for this sequence
			//Patch 12-30-2011 - Updated code to support the Edit View and the existing task that has a modify field		
			$processModifyField = $processManagerUtil->getFieldFromModifyFieldDefs($focus,'process_object_modify_field');
			if ($processModifyField != '') {
			 	$fields = $processManagerUtil->getFieldsFromTable($focus,$table,$processModifyField);
			 	return $fields;			 	
			 	}  
		}
	}
	$fields .= '<option value=Please Specify>Please Specify</option>';
	return $fields;

}

//**********************************************************
//New choice lists that are on the process_defs table

function getProcessDefsEditViewList($focus, $field, $value, $view){
	global $app_list_strings;
	//Determine the app list string
	if ($field == 'process_frequency') {
		$appListStrings = 'process_freq_list';
		$fields = '<name="process_frequency"  size="10" multiple="multiple">';
	}
	if ($field == 'process_start_time_day') {
		$appListStrings = 'process_start_time_daily_list';
		$fields = '<name="process_start_time_day"  size="10" multiple="multiple">';
	}
	if ($field == 'process_start_day_of_week') {
		$appListStrings = 'process_day_of_week_list';
		$fields = '<name="process_start_day_of_week"  size="10" multiple="multiple">';
	}
	if ($field == 'process_day_of_month') {
		$appListStrings = 'start_delay_days';
		$fields = '<name="process_day_of_month"  size="10" multiple="multiple">';
	}	
	if ($field == 'process_month') {
		$appListStrings = 'process_month_list';
		$fields = '<name="process_month"  size="10" multiple="multiple">';
	}		
	if ($field == 'routing_type') {
		$appListStrings = 'routing_type';
		$fields = '<name="routing_type"  size="10" multiple="multiple">';
	}	
	$listElements = $app_list_strings[$appListStrings];
	if ($view != 'DetailView') {	
		require_once('modules/PM_ProcessManager/ProcessManagerUtils.php');
		$processManagerUtil = new ProcessManagerUtils();
		if ($focus->id == "") {
           $fields .= '<option value=Please Specify>Please Specify</option>';
		}
		else{
			$processDefsChoiceListValue = $processManagerUtil->getFieldFromProcessDefsTable($focus,$field);
			if ($processDefsChoiceListValue == '') {
				$fields .= '<option value=Please Specify>Please Specify</option>';
			}
			else{
				$fields .= "<option value=$processDefsChoiceListValue>$processDefsChoiceListValue</option>";
			}
		}
		foreach ($listElements as $key => $value){
			$fields .= "<option value=$key>$value</option>";
		}		
	}
	return $fields;
}

//***********************************************************
//Generic function to get a specific field for the detail view

function getEditViewProcessDefsField($focus, $field, $value, $view){
	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_defs where process_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList["$field"];
	if ($fieldValue != '') {
		if($field == 'process_last_run_date'){
			$fields = "<input name='$field' id='$field'   value='$fieldValue' title='' tabindex='6' ></> ";
		}
		else{
			$fields = "<textarea name='$field' id='$field'   value='$fieldValue' title='' tabindex='6' rows='5' cols='100' >$fieldValue</textarea> ";
		}
	}
	else{
		if ($field == 'process_last_run_date'){
			$fields = "<input name='$field' id='$field'   value='$fieldValue' title='' tabindex='6' ></> ";
		}
		else{
			$fields = "<textarea name='$field' id='$field'   value='' title='' tabindex='6' rows='3' cols='55' ></textarea> ";
		}
	}
	return $fields;
}


function getDetailViewProcessDefsField($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$field = substr($field,12);
	$queryField = "Select $field from pm_process_defs where process_id = '$focusid' ";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$field];
	if ($field != '') {
		$fields = $field; 

	}
	else{
		$fields = 'N/A';
	}
	return $fields;
}
//************************************************************
//PM Pro V2
function getRouteToTeam($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed
	$focusid = $focus->id;
	$queryField = "Select route_to_team from pm_process_defs where process_id = '$focusid' ";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$value = $rowFieldList['route_to_team'];

	$fields = '	<style=\"display:none;\" visiblitity="hidden"  name="route_to_team" size="10" multiple="multiple">';
	if ($value != '') {
		$fields .= '<option value="'.$value .'">'.$value .'</option>';
	}
	else{
		$fields .= "<option value='Please Specify'>Please Specify</option>";
	}
	
	//Make sure the table 
	$query = "SHOW TABLES LIKE 'teams'";
	$results = $focus->db->query($query, true);
	while($row = $focus->db->fetchByAssoc($results))
	{
		$queryFieldList = 'Select name from teams';
		$resultFieldList = $focus->db->query($queryFieldList, true);
		while($rowFieldList = $focus->db->fetchByAssoc($resultFieldList)){
			$fieldName = $rowFieldList['name'];
			$fields .= '<option value="'.$fieldName .'">'.$fieldName .'</option>';
		}
	}

	return $fields;
}

function getRouteToRole($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed
	$focusid = $focus->id;
	$queryField = "Select route_to_role from pm_process_defs where process_id = '$focusid' ";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$value = $rowFieldList['route_to_role'];
	
	$fields = '	<style=\"display:none;\" visiblitity="hidden"  name="route_to_role" size="10" multiple="multiple">';
		if ($value != '') {
			$fields .= '<option value="'.$value .'">'.$value .'</option>';
		}
		else{
			$fields .= "<option value='Please Specify'>Please Specify</option>";
		}	
$queryFieldList = 'Select name from acl_roles where deleted = 0';
$resultFieldList = $focus->db->query($queryFieldList, true);
while($rowFieldList = $focus->db->fetchByAssoc($resultFieldList)){
		$fieldName = $rowFieldList['name'];
		$fields .= '<option value="'.$fieldName .'">'.$fieldName .'</option>';
}
return $fields;
}

function getProcessDefsCheckBox($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$queryField = "Select $field from pm_process_defs where process_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$field];
	if ($field != '') {
		$fields = $field; 

	}
	else{
		$fields = '';
	}
	return $fields;
}

function getProcessObjectModules($focus, $field, $value, $view){
	global $current_user, $app_list_strings;
	global $sugar_config;
	global $db;
	//PATCH FOR READABLE DROPDOWN - 4/18/2012
	require_once('modules/PM_ProcessManager/ProcessManagerEngine.php');
	require_once('modules/PM_ProcessManager/ProcessManagerEngine1.php');
	$pm_engine = new ProcessManagerEngine1();
	//END PATCH
    $dbname=$sugar_config['dbconfig']['db_name'];
	$fields = "<select name='process_object' id='process_object'   onchange='type_change();'>";
	if ($view != 'DetailView') {
			if ($focus->id != "") {
					
					$isSecondPass = strpos($value, "process_object");			
					if ($isSecondPass > 0){
						$focusID = $focus->id;
						$queryProcess = "select process_object from pm_processmanager where id = '$focusID'";
						$resultProcess = $db->query($queryProcess, true);
						$rowProcess = $db->fetchByAssoc($resultProcess);
						
					  	$value = $rowProcess['process_object'];
					  	//PATCH FOR READABLE DROPDOWN - 4/18/2012
						$someBean = $pm_engine->convertTableToBean($value);
						if(isset($someBean->object_name) && !empty($someBean->object_name)){
							$displayName = $someBean->object_name;
						}else{
							$displayName = $value;
						}	
						$fields .="<option value=\"$value\">$displayName</option>";
						//END PATCH
					}
					else{
						//PATCH FOR READABLE DROPDOWN - 4/18/2012
						$someBean = $pm_engine->convertTableToBean($value);
						if(isset($someBean->object_name) && !empty($someBean->object_name)){
							$displayName = $someBean->object_name;
						}else{
							$displayName = $value;
						}
						$fields .="<option value=\"$value\">$displayName</option>";
						//END PATCH
					}
			}
			else{
					$fields .= '<option value=></option>';	
			}
			$columnName = 'Tables_in_' .$dbname;
			$queryShowTables = 'show tables';
			$resultShowTables = $db->query($queryShowTables, true);
			while($rowShowTables = $db->fetchByAssoc($resultShowTables)){
				$tableName = $rowShowTables[$columnName];
				//Don't show the audit tables
				if (stripos($tableName,"audit") == 0) {
					if (checkProcessObjectExemptArray($tableName)) {
						//Remove and _cstm tables
						if (stripos($tableName,"_cstm") == 0) {
							//PATCH FOR READABLE DROPDOWN - 4/18/2012
							$someBean = $pm_engine->convertTableToBean($tableName);
							if(isset($someBean->object_name) && !empty($someBean->object_name)){
								if(isset($displayName) && $displayName != $someBean->object_name || !isset($displayName)){
									$fields .="<option value=\"$tableName\">".$someBean->object_name."</option>";
								}
							}
							//END PATCH
						}
					}
				}	
			}
			$fields .= "</select>";	
			return $fields;		
	}
	else{
		//PATCH FOR READABLE DROPDOWN - 4/18/2012
		$someBean = $pm_engine->convertTableToBean($value);
		if(isset($someBean->object_name) && !empty($someBean->object_name)){
			return $someBean->object_name;
		}else{
			return $value;
		}	
		//END PATCH 	
	}
	
}

//*******************************************************************
//This function will check the process object against the exemption array
//*******************************************************************

function checkProcessObjectExemptArray($tableName){
	$tableExemptArray = array();
	$tableExemptArray[0] = 'accounts_cases';
	$tableExemptArray[1] = 'accounts_contacts';
	$tableExemptArray[2] = 'accounts_opportunities';
	$tableExemptArray[3] = 'acl_actions';
	$tableExemptArray[4] = 'accounts_bugs';
	$tableExemptArray[5] = 'acl_roles';
	$tableExemptArray[6] = 'acl_roles_actions';
	$tableExemptArray[7] = 'acl_roles_users';
	$tableExemptArray[8] = 'address_book';
	$tableExemptArray[9] = 'calls_contacts';
	$tableExemptArray[10] = 'calls_leads';
	$tableExemptArray[11] = 'calls_users';
	$tableExemptArray[12] = 'campaign_log';
	$tableExemptArray[13] = 'campaign_trkrs';
	$tableExemptArray[14] = 'cases_bugs';
	$tableExemptArray[15] = 'config';
	$tableExemptArray[16] = 'contacts_bugs';
	$tableExemptArray[17] = 'contacts_cases';
	$tableExemptArray[18] = 'contacts_users';
	$tableExemptArray[19] = 'currencies';
	$tableExemptArray[20] = 'custom_fields';
	$tableExemptArray[21] = 'documents_accounts';
	$tableExemptArray[22] = 'documents_bugs';
	$tableExemptArray[23] = 'documents_cases';
	$tableExemptArray[24] = 'documents_contacts';
	$tableExemptArray[25] = 'documents_opportunities';
	$tableExemptArray[26] = 'eapm';
	$tableExemptArray[27] = 'email_addr_bean_rel';
	$tableExemptArray[28] = 'email_cache';
	$tableExemptArray[29] = 'email_marketing';
	$tableExemptArray[30] = 'email_marketing_prospect_lists';
	$tableExemptArray[31] = 'emailman';
	$tableExemptArray[32] = 'emails_beans';
	$tableExemptArray[33] = 'emails_email_addr_rel';
	$tableExemptArray[34] = 'emails_text';
	$tableExemptArray[35] = 'fields_meta_data';
	$tableExemptArray[36] = 'folders';
	$tableExemptArray[37] = 'folders_rel';
	$tableExemptArray[38] = 'folders_subsrciptions';
	$tableExemptArray[39] = 'import_maps';
	$tableExemptArray[40] = 'inbound_email_autoreply';
	$tableExemptArray[41] = 'inbound_email_cache_ts';
	$tableExemptArray[42] = 'linked_documents';
	$tableExemptArray[43] = 'folders_subscriptions';
	$tableExemptArray[44] = 'meetings_contacts';
	$tableExemptArray[45] = 'meetings_leads';
	$tableExemptArray[46] = 'meetings_users';
	$tableExemptArray[47] = 'opportunities_contacts';
	$tableExemptArray[48] = 'pm_process_completed_process';
	$tableExemptArray[49] = 'pm_process_defs';
	$tableExemptArray[50] = 'pm_process_filter_table';
	$tableExemptArray[51] = 'pm_process_project_task_defs';
	$tableExemptArray[52] = 'pm_process_stage_waiting_todo';
	$tableExemptArray[53] = 'pm_process_task_call_defs';
	$tableExemptArray[54] = 'pm_process_task_create_object_defs';
	$tableExemptArray[55] = 'pm_process_task_email_defs';
	$tableExemptArray[56] = 'pm_process_task_meeting_defs';
	$tableExemptArray[57] = 'pm_process_task_modify_field_defs';
	$tableExemptArray[58] = 'pm_process_task_task_defs';
	$tableExemptArray[59] = 'pm_process_filter_table';
	$tableExemptArray[60] = 'pm_process_task_waiting_todo';
	$tableExemptArray[61] = 'pm_processmanager';
	$tableExemptArray[62] = 'pm_processmanagerstage';
	$tableExemptArray[63] = 'pm_processmanagerstagetask';
	$tableExemptArray[64] = 'pm_processmanager_entry_table';
	$tableExemptArray[65] = 'pm_process_task_waiting_todo';
	$tableExemptArray[66] = 'pm_round_robin_tracking';
	$tableExemptArray[67] = 'projects_accounts';
	$tableExemptArray[68] = 'pm_processmgerstagetask';
	$tableExemptArray[69] = 'pm_processmmanagerstage';
	$tableExemptArray[70] = 'projects_bugs';
	$tableExemptArray[71] = 'projects_cases';
	$tableExemptArray[72] = 'projects_contacts';
	$tableExemptArray[73] = 'projects_opportunities';
	$tableExemptArray[74] = 'projects_products';
	$tableExemptArray[75] = 'prospect_list_campaigns';
	$tableExemptArray[76] = 'prospect_lists';
	$tableExemptArray[77] = 'prospect_lists_prospects';
	$tableExemptArray[78] = 'relationships';
	$tableExemptArray[79] = 'releases';
	$tableExemptArray[80] = 'roles';
	$tableExemptArray[81] = 'roles_modules';
	$tableExemptArray[82] = 'roles_users';
	$tableExemptArray[83] = 'saved_search';
	$tableExemptArray[84] = 'schedulers';
	$tableExemptArray[85] = 'schedulers_times';
	$tableExemptArray[86] = 'sugarfeed';
	$tableExemptArray[87] = 'upgrade_history';
	$tableExemptArray[88] = 'user_preferences';
	$tableExemptArray[89] = 'users_feeds';
	$tableExemptArray[90] = 'users_last_import';
	$tableExemptArray[91] = 'users_password_link';
	$tableExemptArray[92] = 'users_signatures';
	$tableExemptArray[93] = 'vcals';
	$tableExemptArray[94] = 'versions';
	$tableExemptArray[95] = 'pm_process_related_filter_table';
	$tableExemptArray[96] = 'oauth_consumer';
	$tableExemptArray[97] = 'oauth_nonce';
	$tableExemptArray[98] = 'oauth_tokens';
	$tableExemptArray[99] = 'pm_process_related_filter_table';
	if (in_array($tableName, $tableExemptArray)) {
		return false;
	}
	else{
		return true;
	}
}

//This next function will get the field from the process filter table
//Focus is the Process Manager - so focus id is the id
//field is the field defined in vardefs - detail view
function getDetailViewAndOrField($focus, $field, $value, $view){

}

function getAndOrFilterList($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed
	$fields = '<name="andorfilterfields" id="andorfilterfields"  size="10" multiple="multiple">';
		if ($view != 'DetailView') {	
			require_once('modules/PM_ProcessManager/ProcessManagerUtils.php');
			$processManagerUtil = new ProcessManagerUtils();
			if ($focus->id != "") {

					$andorfileterfield = $processManagerUtil->getFieldFromFilterTable($focus,"andorfilterfields");
					$fields .="<option value=\"$andorfileterfield\">$andorfileterfield</option>";
				
			}
		}	
	$fields .='<option value="and">and</option>
			   <option value="or">or</option>

		 		';
	return $fields;

}

//*************************************************************************************************************
//PM Pro V2

function getRelatedFilterList($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed
	$fields = '<name="filterList"  size="10" multiple="multiple">';
	$sequenceID = substr($field,19);
	$filterListName = "filter_list" ."$sequenceID";
	if ($view != 'DetailView') {
		require_once('modules/PM_ProcessManager/ProcessManagerUtils.php');
		$processManagerUtil = new ProcessManagerUtils();
		if ($focus->id != "") {
			if ($field == 'process_object_cancel_field_operator') {
				$fields .="<option value=\"$value\">$value</option>";
			}
			else{
				//Get the object that the Process is filtering against
				$processObject = $processManagerUtil->getProcessObjectField($focus,"process_object");
				//Now see if we have an existing entry for this sequence
				$processFilterOperator = $processManagerUtil->getRelatdFieldBySequenceID($focus,$sequenceID,'related_field_operator');
				$fields .="<option value=\"$processFilterOperator\">$processFilterOperator</option>";
			}
		}
	}
	$fields .='<option value="=">equal to</option>
	<option value="!=">not equal to</option>
	<option value="<">less than</option>
	<option value=">">greater than</option>
	<option value="contains">contains</option>
	<option value="does not contain">does not contain</option>
	<option value="any change">any change</option>
	<option value="any value">any value</option>
	<option value="date field due today">date field due today</option>
	<option value="date field due in x days">date field due in x days</option>
	<option value="date field due in the past x days">date field due in the past x days</option>
	<option value="is null">is null</option>
	';
	return $fields;

}

function getFilterList($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed
	$fields = '<name="filterList"  size="10" multiple="multiple">';
		$sequenceID = substr($field,11);
		$filterListName = "filter_list" ."$sequenceID";
		if ($view != 'DetailView') {	
			require_once('modules/PM_ProcessManager/ProcessManagerUtils.php');
			$processManagerUtil = new ProcessManagerUtils();
			if ($focus->id != "") {
				if ($field == 'process_object_cancel_field_operator') {
					$fields .="<option value=\"$value\">$value</option>";
				}
				else{
					//Get the object that the Process is filtering against
					$processObject = $processManagerUtil->getProcessObjectField($focus,"process_object");
					//Now see if we have an existing entry for this sequence		
					$processFilterOperator = $processManagerUtil->getFieldBySequenceID($focus,$sequenceID,'field_operator');
					$fields .="<option value=\"$processFilterOperator\">$processFilterOperator</option>";
				}
			}
		}	
	$fields .='<option value="=">equal to</option>
			   <option value="!=">not equal to</option>
				<option value="<">less than</option>
				<option value=">">greater than</option>
				<option value="contains">contains</option>
				<option value="does not contain">does not contain</option>
				<option value="any change">any change</option>
				<option value="any value">any value</option>
				<option value="date field due today">date field due today</option>
				<option value="date field due in x days">date field due in x days</option>
				<option value="date field due in the past x days">date field due in the past x days</option>
				<option value="is null">is null</option>
		 		';
	return $fields;

}

function getObjectFields($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed
if ($field == 'contacts_fields') {
	$table = 'contacts';
}
if ($field == 'accounts_fields') {
	$table = 'accounts';
}
if ($field == 'bugs_fields') {
	$table = 'bugs';
}
if ($field == 'cases_fields') {
	$table = 'cases';
}
if ($field == 'opportunities_fields') {
	$table = 'opportunities';
}
if ($field == 'leads_fields') {
	$table = 'leads';
}
if ($field == 'project_fields') {
	$table = 'project';
}
if ($field == 'tasks_fields') {
	$table = 'tasks';
}
if ($field == 'process_filter_field1') {
	$table = 'contacts';
}

$fields = '	<style=\"display:none;\" visiblitity="hidden"  name="sel1" size="10" multiple="multiple">';
$fields .= '<option value=Please Specify>Please Specify</option>';	
$queryFieldList = 'show fields from ' .$table;
$resultFieldList = $focus->db->query($queryFieldList, true);
while($rowFieldList = $focus->db->fetchByAssoc($resultFieldList)){
		$fieldName = $rowFieldList['Field'];
		$fields .= '<option value="'.$fieldName .'">'.$fieldName .'</option>';
}
//Now go and see if there are any custom fields for the given module
$customTable = $table .'_cstm';
//get the database name 
	global $sugar_config;
$dbname=$sugar_config['dbconfig']['db_name'];
//If we are on windows then we need to set the dbname to lowercase for mysql on windows
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
    $dbname = strtolower($dbname);
} 

$columnName = 'Tables_in_' .$dbname;
$queryShowTables = 'show tables';
$resultShowTables = $focus->db->query($queryShowTables, true);
while($rowShowTables = $focus->db->fetchByAssoc($resultShowTables)){
		$tableName = $rowShowTables[$columnName];
		if ($customTable == $tableName) {
			//we have a custom table so go and get the custom fields and add to the field array
			$queryCustomTable = "show fields from $tableName";
			$resultCustomTable = $focus->db->query($queryCustomTable, true);
				while($rowCustomTable = $focus->db->fetchByAssoc($resultCustomTable)){
					$fieldName = $rowCustomTable['Field'];
    					$fields .= '<option value="'.$fieldName .'">'.$fieldName .'</option>';
    				}
			}
		
		}
		
return $fields;
}

//Function to get the email template names for the Process Stage task

function getEmailTemplates($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed

$fields = '	<style=\"display:none;\" visiblitity="hidden"  name="sel1" size="10" multiple="multiple">';
$queryFieldList = 'Select name from email_templates where deleted = 0 order by name ASC';
$resultFieldList = $focus->db->query($queryFieldList, true);
//First see if there is already an email template defs record
	if ($view != 'DetailView') {	
		require_once('modules/PM_ProcessManager/ProcessManagerUtils.php');
		$processManagerUtil = new ProcessManagerUtils();
		if ($focus->id != "") {
			//Get the object that the Process is filtering against
			$emailTemplateDefsRow = $processManagerUtil->getTaskEmailTemplateDefs($focus);
			//Now see if we have an existing entry for this sequence		
			$processTaskEmailTemplateName = $emailTemplateDefsRow['email_template_name'];
			if ($processTaskEmailTemplateName != '') {
	 				$fields .= '<option value="'.$processTaskEmailTemplateName .'">'.$processTaskEmailTemplateName .'</option>';
			 	}
			else{ 	 
				$fields .= '<option value=Please Specify>Please Specify</option>';	
			}		
		}
		else{
			$fields .= '<option value=Please Specify>Please Specify</option>';	
		}
	while($rowFieldList = $focus->db->fetchByAssoc($resultFieldList)){
		$fieldName = $rowFieldList['name'];
		$fields .= '<option value="'.$fieldName .'">'.$fieldName .'</option>';
	}
  }
return $fields;
}


function getEmailCampaigns($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed

$fields = '	<style=\"display:none;\" visiblitity="hidden"  name="sel1" size="10" multiple="multiple">';
$queryFieldList = 'Select name from campaigns where deleted = 0 order by name ASC';
$resultFieldList = $focus->db->query($queryFieldList, true);
//First see if there is already an email template defs record
	if ($view != 'DetailView') {	
		require_once('modules/PM_ProcessManager/ProcessManagerUtils.php');
		$processManagerUtil = new ProcessManagerUtils();
		if ($focus->id != "") {
			//Get the object that the Process is filtering against
			$emailTemplateDefsRow = $processManagerUtil->getTaskEmailTemplateDefs($focus);
			//Now see if we have an existing entry for this sequence		
			$processTaskemail_queue_campaign_name = $emailTemplateDefsRow['email_queue_campaign_name'];
			if ($processTaskemail_queue_campaign_name != '') {
	 				$fields .= '<option value="'.$processTaskemail_queue_campaign_name .'">'.$processTaskemail_queue_campaign_name .'</option>';
			 	}
			else{ 	 
				$fields .= '<option value=Please Specify>Please Specify</option>';	
			}		
		}
		else{
			$fields .= '<option value=Please Specify>Please Specify</option>';	
		}
	while($rowFieldList = $focus->db->fetchByAssoc($resultFieldList)){
		$fieldName = $rowFieldList['name'];
		$fields .= '<option value="'.$fieldName .'">'.$fieldName .'</option>';
	}
  }
return $fields;
}
//*************************************************
//SugarCRM 5.5 enhancement - support custom modules
//Not Used
//**************************************************

function getProcessObjects($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed

$fields = 'style=\"display:none;\" visiblitity="hidden"  name="sel1" size="10" multiple="multiple" onchange="type_change();"';
$fields .= '<option value= > </option>';
$fields .= '<option value=leads>leads</option>';
$fields .= '<option value=opportunities>opportunities</option>';
$fields .= '<option value=accounts>accounts</option>';
$fields .= '<option value=contacts>contacts</option>';
$fields .= '<option value=cases>cases</option>';
$fields .= '<option value=bugs>bugs</option>';
$fields .= '<option value=project>project</option>';
$fields .= '<option value=tasks>tasks</option>';

//******************************************************************
//Custom Module Support - TBD
//SierraCRM Exemption Array
$sierraCRMProductArray = array();
$sierraCRMProductArray['PM_ProcessManager'] = 'PM_ProcessManager';
$sierraCRMProductArray['PM_ProcessManagerStage'] = 'PM_ProcessManagerStage';
$sierraCRMProductArray['PM_ProcessManagerStageTask'] = 'PM_ProcessManagerStageTask';


if (file_exists('custom/application/Ext/Include/modules.ext.php'))
{
    include('custom/application/Ext/Include/modules.ext.php');
    foreach ($beanFiles as $key=>$value){
 	//Now make sure the custom module has an entry for logic hooks 	
 	//Parse the file looking for custom modules - 

 	if (!in_array($key,$sierraCRMProductArray)) {
 			//Now go and get the table name
 			require_once($value);
 			$customModule = new $key;
 			$customModuleTable = $customModule->table_name;
 			$fields .= "<option value=$customModuleTable>$customModuleTable</option>";
 	}
    }		
}
//End Custom Module Support
//*******************************************************************
return $fields;
}

//Get the contact role for the edit view
/*
function getContactRoles($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed

require_once('modules/PM_ProcessManager/ProcessManagerUtils.php');
require_once('include/utils.php');
global $app_list_strings;
$processManagerUtil = new ProcessManagerUtils();
$emailTemplateDefsRow = $processManagerUtil->getTaskEmailTemplateDefs($focus);
$contact_role = $emailTemplateDefsRow['contact_role'];
$fields =  get_select_options_with_id($app_list_strings['opportunity_relationship_type_dom'], $contact_role);
return $fields;
}
*/
//PM Pro v2 - get Inbound Email names for Send from Acccount

function getSendEmailFromAccount($focus, $field, $value, $view){
	$fields = '	<style=\"display:none;\" visiblitity="hidden"  name="sel1" size="10" multiple="multiple">';
	if ($view != 'DetailView') {
		if ($focus->id != "") {
			//Get the current value 
			$currentValue = getDetailViewEmailDefField($focus, 'email_from_account', $value, $view);
			if($currentValue != ''){
				$fields .= '<option value="'.$currentValue .'">'.$currentValue .'</option>';
				$fields .= '<option value="Please Specify">Please Specify</option>';
			}
			else{
				$fields .= '<option value="Please Specify">Please Specify</option>';
			}
		}
		else{
			$fields .= '<option value="Please Specify">Please Specify</option>';
		}
	}
	
	$queryFieldList = 'Select name from inbound_email';
	$resultFieldList = $focus->db->query($queryFieldList, true);
	while($rowFieldList = $focus->db->fetchByAssoc($resultFieldList)){
		$fieldName = $rowFieldList['name'];
		$fields .= '<option value="'.$fieldName .'">'.$fieldName .'</option>';
	}
	return $fields;
}

function getAssignedUserId($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed
	$fields = '	<style=\"display:none;\" visiblitity="hidden"  name="sel1" size="10" multiple="multiple">';
	//First see if there is already an assigned user for this task or call
	$focusid = $focus->id;
	if ($field == 'assigned_user_id_task') {
		$query = "Select assigned_user_id_task from pm_process_task_task_defs where task_id = '$focusid'";
	}
	elseif($field == 'assigned_user_id_call'){
		$query = "Select assigned_user_id_call from pm_process_task_call_defs where task_id = '$focusid'";
	}
	elseif ($field == 'assigned_user_id_meeting'){
		$query = "Select assigned_user_id_meeting from pm_process_task_meeting_defs where task_id = '$focusid'";
	}
	elseif ($field == 'assigned_user_id_create_object'){
		$query = "Select assigned_user_id_create_object from pm_process_task_create_object_defs where task_id = '$focusid'";
	}elseif ($field == 'assigned_user_id_project_task'){
		$query = "Select assigned_user_id_project_task from pm_process_project_task_defs where project_task_id = '$focusid'";
	}
	elseif ($field == 'route_to_user'){			
		$query =  "Select $field from pm_process_defs where process_id = '$focusid'";
	}
	$resultQueryField = $focus->db->query($query, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$field];
		if ($field != '') {
			$fields .= '<option value="'.$field .'">'.$field .'</option>';
		}
		else{
			$fields .= "<option value='Please Specify'>Please Specify</option>";
		}	
$queryFieldList = 'Select user_name from users';
$resultFieldList = $focus->db->query($queryFieldList, true);
while($rowFieldList = $focus->db->fetchByAssoc($resultFieldList)){
		$fieldName = $rowFieldList['user_name'];
		$fields .= '<option value="'.$fieldName .'">'.$fieldName .'</option>';
}
return $fields;
}

function getProcessFilterField($focus, $field, $value, $view){
	//This function will check to see if this is an edit call or new
	//If Edit then $focus is set - so what we do is get the fields from the object table
	//Fill the array - then set the one that is current.	
	//Ignore if DetailView
	$sequenceID = substr($field,20);
	$filterListName = "process_filter_field" ."$sequenceID";
	$fields = '<name="filterList"  size="10" multiple="multiple">';
	if ($view != 'DetailView') {	
		require_once('modules/PM_ProcessManager/ProcessManagerUtils.php');
		$processManagerUtil = new ProcessManagerUtils();
		if ($focus->id != "") {
			//Get the object that the Process is filtering against
			$processObject = $processManagerUtil->getProcessObjectField($focus,"process_object");
			//Now see if we have an existing entry for this sequence		
			$processFilterField = $processManagerUtil->getFieldBySequenceID($focus,$sequenceID,'field_name');
			if ($processFilterField != '') {
			 	$fields = $processManagerUtil->getFieldsFromTable($focus,$processObject,$processFilterField);
			 	return $fields;			 	
			 }
			 else{
			 	$fields = $processManagerUtil->getFieldsFromTable($focus,$processObject,'');
			 	return $fields;
			 } 
		}
	}
	$fields .= '<option value=Please Specify>Please Specify</option>';
	return $fields;
}

function getProcessCancelField($focus, $field, $value, $view){
	$fields = '<name="filterList"  size="10" multiple="multiple">';
	if ($view != 'DetailView') {	
		require_once('modules/PM_ProcessManager/ProcessManagerUtils.php');
		$processManagerUtil = new ProcessManagerUtils();
		if ($focus->id == "") {
           $fields .= '<option value=Please Specify>Please Specify</option>';
		}
		else{
			$processCancelField = $processManagerUtil->getProcessObjectField($focus,"process_object_cancel_field");
			if ($processCancelField == '') {
				$fields .= '<option value=Please Specify>Please Specify</option>';
			}
			else{
				$fields .= "<option value=$processCancelField>$processCancelField</option>";
			}
		}
	}
	return $fields;
}
//*************************************************************************************************
//Related Filter Field Functions
function getDetailViewRelatedField($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$sequenceID = substr($field,25);
	$queryField = "Select related_field_name from pm_process_related_filter_table  where process_id = '$focusid' and sequence = $sequenceID ";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList['related_field_name'];
	if ($field != '') {
		$fields = $field;

	}
	else{
		$fields = 'N/A';
	}
	return $fields;
}

function getDetailViewRelatedValue($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$sequenceID = substr($field,25);
	$focusid = $focus->id;

	$queryField = "Select related_field_value from pm_process_related_filter_table where process_id = '$focusid' and sequence = $sequenceID ";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList['related_field_value'];
	if ($field != '') {
		$fields = $field;

	}
	else{
		$fields = '';
	}
	return $fields;
}

function getDetailViewRelatedOperator($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$sequenceID = substr($field,28);
	$queryField = "Select related_field_operator from pm_process_related_filter_table where process_id = '$focusid' and sequence = $sequenceID ";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList['related_field_operator'];
	if ($field != '') {
		$fields = $field;

	}
	else{
		$fields = 'N/A';
	}
	return $fields;
}

//********************************************************************************************************
//This next function will get the field from the process filter table
//Focus is the Process Manager - so focus id is the id
//field is the field defined in vardefs - detail view
function getDetailViewField($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$sequenceID = substr($field,17);
	$queryField = "Select field_name from pm_process_filter_table where process_id = '$focusid' and sequence = $sequenceID ";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList['field_name'];
	if ($field != '') {
		$fields = $field; 

	}
	else{
		$fields = 'N/A';
	}
	return $fields;
}

function getDetailViewValue($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
    $sequenceID = substr($field,17);
	$focusid = $focus->id;
	
	$queryField = "Select field_value from pm_process_filter_table where process_id = '$focusid' and sequence = $sequenceID ";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList['field_value'];
	if ($field != '') {
		$fields = $field; 

	}
	else{
		$fields = '';
	}
	return $fields;
}
//********************************************************
//PM Pro v2 - get related edit view info
//********************************************************
function getRelatedEditViewValue($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//Sample field is related_field2_value
	//So get the value and ask for that sequence id
	$sequenceID = substr($field,13,1);
	$focusid = $focus->id;
	$queryField = "Select related_field_value from pm_process_related_filter_table where process_id = '$focusid' and sequence = $sequenceID ";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList['related_field_value'];
	if ($fieldValue != '') {
		$fields = "<input type='text' name='$field' id='$field' size='30' maxlength='255' value='$fieldValue' title='' tabindex='6' > ";
	}
	else{
		$fields = "<input type='text' name='$field' id='$field' size='30' maxlength='255' value='' title='' tabindex='6' > ";
	}
	return $fields;
}

function getEditViewValue($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
    $sequenceID = substr($field,20,1);
	$focusid = $focus->id;	
	$queryField = "Select field_value from pm_process_filter_table where process_id = '$focusid' and sequence = $sequenceID ";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList['field_value'];
	if ($fieldValue != '') {
		$fields = "<input type='text' name='$field' id='$field' size='30' maxlength='255' value='$fieldValue' title='' tabindex='6' > ";
	}
	else{
		$fields = "<input type='text' name='$field' id='$field' size='30' maxlength='255' value='' title='' tabindex='6' > ";
	}
	return $fields;
}
function getDetailViewOperator($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$sequenceID = substr($field,20);
	$queryField = "Select field_operator from pm_process_filter_table where process_id = '$focusid' and sequence = $sequenceID ";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList['field_operator'];
	if ($field != '') {
		$fields = $field; 

	}
	else{
		$fields = 'N/A';
	}
	return $fields;
}

//***********************************************************
//Generic function to get a specific field for the detail view

function getDetailViewObjectField($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	if ($field == 'detail_view_process_object_cancel_field') {
		$field = 'process_object_cancel_field';
	}
	if ($field == 'detail_view_process_object_cancel_field_value') {
		$field = 'process_object_cancel_field_value';
	}
	if ($field == 'detail_view_process_object_cancel_field_operator') {
		$field = 'process_object_cancel_field_operator';
	}		
	$sequenceID = substr($field,20);
	$queryField = "Select $field from pm_processmanager where id = '$focusid' ";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$field];
	if ($field != '') {
		$fields = $field; 

	}
	else{
		$fields = 'N/A';
	}
	return $fields;
}


//***************************************************
//This next function will get Email Defs Values
//for PM Stage Task Detail View
function getDetailViewEmailDefField($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
    $sequenceID = substr($field,17);
	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_task_email_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$field];
	if ($field != '') {
		$fields = $field; 

	}
	else{
		$fields = '';
	}
	return $fields;
}

//********************************************************************************
//This function will get the information for the detail view for the task defs
//getDetailViewTaskDefField
function getDetailViewTaskDefField($focus, $field, $value, $view){
	$focusid = $focus->id;
	$field = substr($field,12);
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_task_task_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$field];
	if ($field != '') {
		$fields = $field; 

	}
	else{
		$fields = '';
	}
	return $fields;
}
//******************************************************************************

//**********************************************************************************
//Function to get the Call Defs Fields - 
function getDetailViewTaskField($focus, $field, $value, $view){
	$focusid = $focus->id;
	if ($field == 'detai_view_task_description') {
		$field = 'task_description';
	}
	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_task_task_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$field];
	if ($field != '') {
		$fields = $field; 

	}
	else{
		$fields = '';
	}
	return $fields;
}

//**********************************************************************************
//Function to get the Call Defs Fields - 
function getDetailViewCallField($focus, $field, $value, $view){
	$focusid = $focus->id;
     $field = substr($field,12);	
	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_task_call_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$field];
	if ($field != '') {
		$fields = $field; 

	}
	else{
		$fields = '';
	}
	return $fields;
}

//**********************************************************************************
//Function to get the Call Defs Fields - 
function getDetailViewCallDefField($focus, $field, $value, $view){
	$focusid = $focus->id;
	$field = substr($field,17);
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_task_call_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$field];
	if ($field != '') {
		$fields = $field; 

	}
	else{
		$fields = '';
	}
	return $fields;
}
//**********************************************************************************
//This function gets the 1 - 0 value from a bool field and returns to set the correct
//value in a check box
//**********************************************************************************
function getTaskDetailViewFieldCheckBox($focus, $field, $value, $view){
	$focusid = $focus->id;
	$queryField = "Select $field from pm_process_task_task_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList[$field];
	return $fieldValue;
}

function getCallDetailViewFieldCheckBox($focus, $field, $value, $view){
	$focusid = $focus->id;
	$queryField = "Select $field from pm_process_task_call_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList[$field];
	return $fieldValue;
}

//Now do the same for the edit views

function getCallEditViewFieldCheckBox($focus, $field, $value, $view){
	$focusid = $focus->id;
	$queryField = "Select is_escalatable_call from pm_process_task_call_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList['is_escalatable_call'];
	$on_or_off = 'CHECKED';
		if ( empty($fieldValue) ||  $fieldValue == '0')
		{
			$on_or_off = '';
		}
return "<input type='checkbox' name='is_escalatable_call_edit' id='is_escalatable_call_edit' $on_or_off >";
}


function getTaskEditViewFieldCheckBox($focus, $field, $value, $view){
	$focusid = $focus->id;
	$queryField = "Select is_escalatable_task from pm_process_task_task_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList['is_escalatable_task'];
	$on_or_off = 'CHECKED';
		if ( empty($fieldValue) ||  $fieldValue == '0')
		{
			$on_or_off = '';
		}
	return "<input type='checkbox' name='is_escalatable_task_edit' id='is_escalatable_task_edit' $on_or_off >";
}
//******************************************************************************
//Function to get the task details in the edit view
function getTaskEditViewField($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$queryField = "Select $field from pm_process_task_task_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList[$field];
		$fields = "<input type=text name='$field' value='$fieldValue' size=50>";
	return $fields;
}

function getTaskEditViewFieldPriority($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed

	$fields = '	<style=\"display:none;\" visiblitity="hidden"  name="sel1" size="10" multiple="multiple">';
	$focusid = $focus->id;	
	$queryField = "Select task_priority from pm_process_task_task_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$field];
		if ($field != '') {
		$fields .= '<option value="'.$field .'">'.$field .'</option>';
	}
	//PM Pro V2 - update to use app list strings - task_priority_dom
	global $app_list_strings;
	$listElements = $app_list_strings['task_priority_dom'];
	foreach ($listElements as $key => $value){
		$fields .= "<option value=$key>$value</option>";
	}
return $fields;
}

//PROJECT TASKS--added by Peter DeMartini
function getProjectTaskEditViewField($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$queryField = "Select $field from pm_process_project_task_defs where project_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList[$field];
	if($field == 'project_task_start_date' || $field == 'project_task_end_date' || $field == 'project_task_id'){
		$fields = "<input type=text name='$field' value='$fieldValue' size=5>";
	}else{
		$fields = "<input type=text name='$field' value='$fieldValue' size=50>";
	}
	return $fields;
}

function getProjectTaskEditViewFieldPriority($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed

	$fields = '	<style=\"display:none;\" visiblitity="hidden"  name="sel1" size="10" multiple="multiple">';
	$focusid = $focus->id;	
	$queryField = "SELECT project_task_priority FROM pm_process_project_task_defs WHERE project_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$field];
		if ($field != '') {
		$fields .= '<option value="'.$field .'">'.$field .'</option>';
	}
	$fields .= '<option value="High">High</option>';
	$fields .= '<option value="Medium">Medium</option>';
	$fields .= '<option value="Low">Low</option>';
return $fields;
}

function getProjectTaskEditViewFieldStatus($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed

	$fields = '	<style=\"display:none;\" visiblitity="hidden"  name="sel1" size="10" multiple="multiple">';
	$focusid = $focus->id;	
	$queryField = "SELECT project_task_status FROM pm_process_project_task_defs WHERE project_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$field];
		if ($field != '') {
		$fields .= '<option value="'.$field .'">'.$field .'</option>';
	}
	$fields .= '<option label="Not Started" value="Not Started">Not Started</option>';
	$fields .= '<option label="In Progress" value="In Progress">In Progress</option>';
	$fields .= '<option label="Completed" value="Completed">Completed</option>';
	$fields .= '<option label="Pending Input" value="Pending Input">Pending Input</option>';
	$fields .= '<option label="Deferred" value="Deferred">Deferred</option>';
return $fields;
}

function getProjectTaskEditViewFieldDescription($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_project_task_defs where project_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList[$field];
    $fields = "<textarea  name=$field value='$fieldValue' rows='6' cols='50'>$fieldValue</textarea>";
	return $fields;
}

function getDetailViewProjectTaskField($focus, $field, $value, $view){
	$focusid = $focus->id;
	$milestone = false;
	if ($field == 'detai_view_project_task_description') {
		$field = 'project_task_description';
	}elseif($field == 'detail_view_project_task_subject'){
		$field = 'project_task_subject';
	}elseif($field == 'detail_view_project_task_id'){
		$field = 'project_task_id';
	}elseif($field == 'detail_view_project_task_status'){
		$field = 'project_task_status';
	}elseif($field == 'detail_view_project_task_priority'){
		$field = 'project_task_priority';
	}elseif($field == 'detail_view_project_task_start_date'){
		$field = 'project_task_start_date';
	}elseif($field == 'detail_view_project_task_end_date'){
		$field = 'project_task_end_date';
	}elseif($field == 'detail_view_assigned_user_id_project_task'){
		$field = 'assigned_user_id_project_task';
	}elseif($field == 'detail_view_project_task_milestone'){
		$field = 'project_task_milestone';
		$milestone = true;
	}elseif($field == 'detail_view_project_task_task_number'){
		$field = 'project_task_task_number';
	}elseif($field == 'detail_view_project_task_order'){
		$field = 'project_task_order';
	}

	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_project_task_defs where project_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$field];
	if($milestone == true){
		if($field == 0){
			$field = 'OFF';
		}else{
			$field = 'ON';
		}	
	}
	if ($field != '') {
		$fields = $field; 

	}
	else{
		$fields = '';
	}
	return $fields;
}

function getProjectTaskEditViewFieldCheckBox($focus, $field, $value, $view){
	$focusid = $focus->id;
	$queryField = "SELECT $field FROM pm_process_project_task_defs WHERE project_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList['project_task_milestone'];
	$on_or_off = 'CHECKED';
		if ( empty($fieldValue) ||  $fieldValue == '0')
		{
			$on_or_off = '';
		}
	return "<input type='checkbox' name='project_task_milestone' id='project_task_milestone' $on_or_off >";
}

//END PROJECT TASKS



//*****************************************************************************

//******************************************************************************
//Function to get the task details in the edit view
function getCallEditViewField($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_task_call_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList[$field];
    $fields = "<input type=text name=$field value='$fieldValue' size=50>";
	return $fields;
}
//******************************************************************************
//Function to get the task details in the edit view
function getCallEditViewFieldDescription($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_task_call_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList[$field];
    $fields = "<textarea  name=$field value='$fieldValue' rows='6' cols='50'>$fieldValue</textarea>";
	return $fields;
}
//******************************************************************************
//Function to get the task details in the edit view
function getTaskEditViewFieldDescription($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_task_task_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList[$field];
    $fields = "<textarea  name=$field value='$fieldValue' rows='6' cols='50'>$fieldValue</textarea>";
	return $fields;
}
//*****************************************************************************
//Passed field is going to be task_due_date_delay_minutes, task_due_date_delay_hours - etc
//Get the string position and set the for loop accordingly
function getTaskEditViewDelayFields($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed
	$fields = '	<style=\"display:none;\" visiblitity="hidden"  name="sel1" size="10" multiple="multiple">';	
	$focusid = $focus->id;
	$delay = substr($field,20);
	$fieldQuery = substr($field,5);
	//Are we a task or a call?
	if (substr_count($field,'call') > 0) {
		//Replace due_date with start
		$fieldQuery = str_replace("due_date","start",$fieldQuery);
		$queryField = "Select $fieldQuery from pm_process_task_call_defs where task_id = '$focusid'";
	}
	else{
		$queryField = "Select $fieldQuery from pm_process_task_task_defs where task_id = '$focusid'";
	}
	//Now setup the case
	switch ($delay) {
	case 'minutes':
    	$delay = 60;
    	break;
	case 'hours':
    	$delay = 24;
    	break;
	case 'days':
    	$delay = 31;
    	break;
    case 'months':
    	$delay = 12;
    	break;
	case 'years':
    	$delay = 10;
    	break;    	
	}
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$fieldQuery];
		if ($field != '') {
		$fields .= '<option value="'.$field .'">'.$field .'</option>';
	}
	//Now build the rest of the option list with the remaing values
	//For loop used
	for($i = 0; $i < $delay; $i++){
		$fields .= "<option value='$i'>$i</option>";
	}
return $fields;
}

//*****************************************************************************
//FOLLOWING FUNCTIONS SUPPORT MEETINGS
//*****************************************************************************
function getMeetingEditViewField($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_task_meeting_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList[$field];
    $fields = "<input type=text name=$field value='$fieldValue'>";
	return $fields;
}
//***********************************************************************************************
//Passed field is going to be meeting_due_date_delay_minutes, meeting_due_date_delay_hours - etc
//Get the string position and set the for loop accordingly
//************************************************************************************************
function getTaskEditViewDelayFieldsMeetings($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed
	$fields = '	<style=\"display:none;\" visiblitity="hidden"  name="sel1" size="10" multiple="multiple">';	
	$focusid = $focus->id;
	$delay = substr($field,20);
	$fieldQuery = substr($field,8);
	$queryField = "Select $fieldQuery from pm_process_task_meeting_defs where task_id = '$focusid'";
	//Now setup the case
	switch ($delay) {
	case 'minutes':
    	$delay = 60;
    	break;
	case 'hours':
    	$delay = 24;
    	break;
	case 'days':
    	$delay = 31;
    	break;
    case 'months':
    	$delay = 12;
    	break;
	case 'years':
    	$delay = 10;
    	break;    	
	}
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$fieldQuery];
		if ($field != '') {
		$fields .= '<option value="'.$field .'">'.$field .'</option>';
	}
	//Now build the rest of the option list with the remaing values
	//For loop used
	for($i = 0; $i < $delay; $i++){
		$fields .= "<option value='$i'>$i</option>";
	}
return $fields;
}
//******************************************************************************
//Function to get the task details in the edit view
function getMeetingEditViewFieldDescription($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_task_meeting_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList[$field];
    $fields = "<textarea  name=$field value='$fieldValue' rows='6' cols='50'>$fieldValue</textarea>";
	return $fields;
}

function getReminderTimeMeeting($focus, $field, $value, $view) {

	global $current_user, $app_list_strings;
	$reminder_t = $current_user->getPreference('reminder_time');
	if (!empty($focus->reminder_time)) {
		$reminder_t = $focus->reminder_time;
	}		

	if($view == 'EditView' || $view == 'MassUpdate') {
		global $app_list_strings;
        $html = '<select name="reminder_time_meeting">';
        $html .= get_select_options_with_id($app_list_strings['reminder_time_options'], $reminder_t);
        $html .= '</select>';
        return $html;
    }
 
    if($reminder_t == -1) {
       return "";	
    }
       
    return translate('reminder_time_options', '', $reminder_t);    
}

function getDetailViewMeetingDefField($focus, $field, $value, $view){
	$focusid = $focus->id;
	$field = substr($field,20);
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_task_meeting_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$field];
	if ($field != '') {
		$fields = $field; 

	}
	else{
		$fields = '';
	}
	return $fields;
}

//*****************************************************************************
//END OF FUNCTIONS THAT SUPPORT MEETINGS
//*****************************************************************************

//************************************************************************
//NEW FUNCTIONS FOR INTERNAL EMAIL
//***********************************************************************

//******************************************************************************
//Function to get the task details in the edit view
function getEmailDefsEditViewField($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$queryField = "Select $field from pm_process_task_email_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList[$field];
    $fields = "<textarea  name=$field value='$fieldValue' rows='6' cols='50'>$fieldValue</textarea>";
	return $fields;
}

function getEmailDefsDetailViewField($focus, $field, $value, $view){
	$focusid = $focus->id;
	$field = substr($field,12);
	$queryField = "Select $field from pm_process_task_email_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList[$field];
	$field = $rowFieldList[$field];
	if ($field != '') {
		$fields = $field; 

	}
	else{
		$fields = '';
	}
	return $fields;
}

function getEmailDefsEditViewFieldCheckBox($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$queryField = "Select $field from pm_process_task_email_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$field];
	if ($field != '') {
		$fields = $field; 

	}
	else{
		$fields = '';
	}
	return $fields;
}
function getEmailDefsDetailViewFieldCheckBox($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	if ($field == 'detail_view_internal_email') {
		$field = 'internal_email';
	}
	if ($field == 'detail_view_send_email_to_caseopp_account') {
		$field = 'send_email_to_caseopp_account';
	}	
	if ($field == 'detail_view_send_email_to_object_owner') {
		$field = 'send_email_to_object_owner';
	}
	if ($field == 'detail_view_send_email_to_email_queue') {
		$field = 'send_email_to_email_queue';
	}	
	if ($field == 'detail_view_send_email_to_contact') {
		$field = 'send_email_to_contact';
	}			
	$queryField = "Select $field from pm_process_task_email_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$field];
	if ($field != '') {
		$fields = $field; 

	}
	else{
		$fields = '';
	}
	return $fields;
}
//********************************************************************
//END INTERNAL EMAIL FUNCTIONS
//*******************************************************************

//******************************************************************************
//BEGIN NEW FUNCTIONS TO SUPPORT CREATE OBJECTS
//******************************************************************************
function getCreateObjectEditViewField($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_task_create_object_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList[$field];
    $fields = "<input type=text name=$field value='$fieldValue'>";
	return $fields;
}

function getCreateObjectEditViewFieldCheckBox($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_task_create_object_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList["$field"];
	if ($fieldValue == '' || $fieldValue == 0) {
		$CHECKED = '';
	}
	else{
		$CHECKED = 'CHECKED';
	}
    $fields = "<input name='$field' type='checkbox' class='checkbox' value='1' $CHECKED>";
	return $fields;
}

function getCreateObjectDetailViewFieldCheckBox($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$field = substr($field,26);
	$queryField = "Select $field from pm_process_task_create_object_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList["$field"];
	if ($fieldValue == '' || $fieldValue == 0) {
		$CHECKED = '';
	}
	else{
		$CHECKED = 'CHECKED';
	}
    $fields = "<input name='$field' type='checkbox' DISABLED class='checkbox' value='1' $CHECKED>";
	return $fields;
}



function getCreateObjectEditViewFieldDescription($focus, $field, $value, $view){
	$focusid = $focus->id;
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_task_create_object_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$fieldValue = $rowFieldList[$field];
    $fields = "<textarea  name=$field value='$fieldValue' rows='6' cols='50'>$fieldValue</textarea>";
	return $fields;
}

function getDetailViewCreateObjectDefField($focus, $field, $value, $view){
	$focusid = $focus->id;
	$field = substr($field,26);
	//Each $field will have 1,2,3,4,5 as the last element
	//So get the value and ask for that sequence id
	$focusid = $focus->id;	
	$queryField = "Select $field from pm_process_task_create_object_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList[$field];
	if ($field != '') {
		$fields = $field; 

	}
	else{
		$fields = '';
	}
	return $fields;
}

function getCreateObjectRecordType($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed
	global $app_list_strings;
	global $current_user;
	global $sugar_config;
	global $db;	
	$dbname=$sugar_config['dbconfig']['db_name'];
	$focusid = $focus->id;	
	$queryField = "Select create_object_type from pm_process_task_create_object_defs where task_id = '$focusid'";
	$resultQueryField = $focus->db->query($queryField, true);
	$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
	$field = $rowFieldList['create_object_type'];	
	$fields = '<select name="create_object_type"  size="10" multiple="multiple">';

		if ($view != 'DetailView') {	
			require_once('modules/PM_ProcessManager/ProcessManagerUtils.php');
			$processManagerUtil = new ProcessManagerUtils();
			if ($focus->id != "") {
					$fields .="<option value=\"$field\">$field</option>";
			}
			else{
					$fields .= '<option value=Please Specify>Please Specify</option>';	
			}			
			$columnName = 'Tables_in_' .$dbname;
			$queryShowTables = 'show tables';
			$resultShowTables = $db->query($queryShowTables, true);
			while($rowShowTables = $db->fetchByAssoc($resultShowTables)){
				$tableName = $rowShowTables[$columnName];
				$fields .="<option value=\"$tableName\">$tableName</option>";	
			}
			$fields .= "</select>";	
			return $fields;	
		}
	else{
		return $fields;
	}	
}

//*****************************************************************************
//END NEW FUNCTIONS TO SUPPORT CREATE OBJECTS
//*****************************************************************************

//*****************************************************************************
//PM Pro V2 - Related Object Filter Fields

function getRelateOnFilterList($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed
	$fields = "<select name='filterOnRelated' id='filterOnRelated'  onchange='toggle_filter_related();' >";
	if ($view != 'DetailView') {
		require_once('modules/PM_ProcessManager/ProcessManagerUtils.php');
		$processManagerUtil = new ProcessManagerUtils();
		if ($focus->id != "") {

			$andorfileterfield = $processManagerUtil->getFieldFromRelatedFilterTable($focus,"andorfilterfields");
			$fields .="<option value=\"$andorfileterfield\">$andorfileterfield</option>";

		}
		else{
			$fields .="<option value=\"\"></option>";
			
		}
	}
	$fields .='<option value="YES">YES</option>
	<option value="NO">NO</option>

	';
	return $fields;

}

//Get the value of the and or filter related 
function getRelatedAndOrFilterList($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed
	$fields = '<name="related_andorfilterfield" id="related_andorfilterfield"  size="10" multiple="multiple">';
	if ($view != 'DetailView') {
		require_once('modules/PM_ProcessManager/ProcessManagerUtils.php');
		$processManagerUtil = new ProcessManagerUtils();
		if ($focus->id != "") {

			$andorfileterfield = $processManagerUtil->getFieldFromRelatedFilterTable($focus,"andorfilterfields");
			$fields .="<option value=\"$andorfileterfield\">$andorfileterfield</option>";

		}
	}
	$fields .='<option value="and">and</option>
	<option value="or">or</option>

	';
	return $fields;

}

function getModifyRelatedObjectModules($focus, $field, $value, $view){
	global $current_user, $app_list_strings;
	global $sugar_config;
	global $db;
	
	$dbname=$sugar_config['dbconfig']['db_name'];
	$fields = "<select name='process_object_modify_related_object' id='process_object_modify_related_object'  onchange='related_type_change();'>";
	if ($view != 'DetailView') {
		if ($focus->id != "") {
			$focusid = $focus->id;
			$queryField = "Select $field from pm_process_task_modify_field_defs where task_id = '$focusid'";
			$resultQueryField = $focus->db->query($queryField, true);
			$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
			$fieldValue = $rowFieldList["$field"];			
			if ($fieldValue == ''){
				$fields .= '<option value=Please Specify>Please Specify</option>';
			}
			else{
				$fields .="<option value=\"$fieldValue\">$fieldValue</option>";
			}
			//Get the process object for the task
			$queryFocusObject = "select pm.process_object, pm.id as process_id, pmstage.id as stage_id, pmst.pm_processmanager_ida,
			pmst.pm_processmanagerstage_idb, pmtask.id as pmtask_id, pmstagetask.pm_processmanagerstage_ida, pmstagetask.pm_processmanagerstagetask_idb
			from pm_processmanager pm, pm_processmanagerstage pmstage, pm_processmmanagerstage pmst, pm_processmanagerstagetask pmtask,
			pm_processmgerstagetask pmstagetask
			where pmstagetask.pm_processmanagerstagetask_idb = '$focus->id' and pmstagetask.pm_processmanagerstage_ida = pmst.pm_processmanagerstage_idb
			and pmst.pm_processmanager_ida = pm.id";
			$resultFocusObject = $db->query($queryFocusObject, true);
			while($rowfocusObject = $db->fetchByAssoc($resultFocusObject)){
				$table = $rowfocusObject['process_object'];
			}			
			//Now fill in the related modules
			//$focusID = $focus->id;
			//$queryProcess = "select process_object_modify_related_object from pm_process_task_modify_field_defs where task_id = '$focusID'";
			//$resultProcess = $db->query($queryProcess, true);
			//$rowProcess = $db->fetchByAssoc($resultProcess);
			//$processObject = $rowProcess['process_object_modify_related_object'];
			$bean = convertTableToBean($table);
			$bean->load_relationships();
			$relationshipFields = $bean->relationship_fields;
			foreach($relationshipFields as $key1=>$value1)
			{
				$fields .= "<option value=\"$value1\">$value1</option>";
			}
		}
		else{
				
			$fields .= '<option value=Please Specify>Please Specify</option>';
		}

		return $fields;
	}
	else{
		return $value;
	}

}

function getProcessModifyRelatedField($focus, $field, $value, $view) { //This is the function that the field will run when it is displayed


	$filterListName = "process_object_modify_related_field";
	$fields = '<name="process_object_modify_related_field"  size="10" multiple="multiple">';
	if ($view != 'DetailView') {
		require_once('modules/PM_ProcessManager/ProcessManagerUtils.php');
		$processManagerUtil = new ProcessManagerUtils();
		if ($focus->id != "") {
			//Get the process object for the task
			$focusid = $focus->id;
			$queryField = "Select process_object_modify_related_object from pm_process_task_modify_field_defs where task_id = '$focusid'";
			$resultQueryField = $focus->db->query($queryField, true);
			$rowFieldList = $focus->db->fetchByAssoc($resultQueryField);
			$fieldValue = $rowFieldList['process_object_modify_related_object'];	
			//Now see if we have an existing entry for this sequence
			//Patch 12-30-2011 - Updated code to support the Edit View and the existing task that has a modify field
			$processModifyField = $processManagerUtil->getFieldFromModifyFieldDefs($focus,'process_object_modify_related_field');
					if ($processModifyField != '') {
						$fields = $processManagerUtil->getFieldsFromTable($focus,$fieldValue,$processModifyField);
						return $fields;
			 	}
					}
		}
		$fields .= '<option value=Please Specify>Please Specify</option>';
		return $fields;

}

function getRelatedObjectModules($focus, $field, $value, $view){
	global $current_user, $app_list_strings;
	global $sugar_config;
	global $db;

	$dbname=$sugar_config['dbconfig']['db_name'];
	$fields = "<select name='related_object' id='related_object'  onchange='related_type_change();'>";
	if ($view != 'DetailView') {
		if ($focus->id != "") {
			if ($value == ''){
				$fields .= '<option value=Please Specify>Please Specify</option>';
			}
			else{
				$fields .="<option value=\"$value\">$value</option>";
			}
			//Now fill in the related modules
			$focusID = $focus->id;
			$queryProcess = "select process_object from pm_processmanager where id = '$focusID'";
			$resultProcess = $db->query($queryProcess, true);
			$rowProcess = $db->fetchByAssoc($resultProcess);
			$processObject = $rowProcess['process_object'];
			$bean = convertTableToBean($processObject);
			$bean->load_relationships();
			$relationshipFields = $bean->relationship_fields;
			foreach($relationshipFields as $key1=>$value1)
			{
				$fields .= "<option value=\"$value1\">$value1</option>";
			}			
		}
		else{
			
			$fields .= '<option value=Please Specify>Please Specify</option>';
		}

		return $fields;
	}
	else{
		return $value;
	}

}

//***********************************************************
function convertTableToBean($focusObjectType){
	global $moduleList;
	global $beanList;
	$exemptModules = array();
	$exemptModules['Calendar.php'] = "Calendar.php";
	foreach ($moduleList as $key=>$value){
		$module_array = array();
		$module_array[$key]=$value;
		$module_array = convert_module_to_singular($module_array);
		$key1 = $module_array[$key];
		$bean = $key1;
		$key1 .= ".php";
		$beanFile = "modules/$value/$key1";
		if (file_exists("$beanFile")) {
			if (!in_array($key1,$exemptModules)) {
				if ($bean == "Case") {
					$bean = "aCase";
				}
				require_once("$beanFile");
				$newbean = new $bean;
				$table = $newbean->table_name;
				if ($focusObjectType == $table) {
					return $newbean;
				}
			}
		}
	}
}

//PM Pro V2
function getRelatedFilterField($focus, $field, $value, $view){
	//Sample Field = related_filter_field1
	$sequenceID = substr($field,20);
	$filterListName = "related_filter_field" ."$sequenceID";
	$fields = '<name="relatedfilterList"  size="10" multiple="multiple">';
	if ($view != 'DetailView') {
		require_once('modules/PM_ProcessManager/ProcessManagerUtils.php');
		$processManagerUtil = new ProcessManagerUtils();
		if ($focus->id != "") {
			//Get the object that the Process is filtering against
			$relatedObject = $processManagerUtil->getProcessObjectField($focus,"related_object");
			//Now see if we have an existing entry for this sequence
			$relatedFilterField = $processManagerUtil->getRelatdFieldBySequenceID($focus,$sequenceID,'related_field_name');
			if(($relatedObject != '') && ($relatedObject != NULL) && ($relatedObject != 'Please Specify')){
				if ($relatedFilterField != '') {
					$fields = $processManagerUtil->getFieldsFromTable($focus,$relatedObject,$relatedFilterField);
					return $fields;
				}
				else{
					$fields = $processManagerUtil->getFieldsFromTable($focus,$relatedObject,'');
					return $fields;
				}
			}
		}
	}
	$fields .= '<option value=Please Specify>Please Specify</option>';
	return $fields;
}

//
?>