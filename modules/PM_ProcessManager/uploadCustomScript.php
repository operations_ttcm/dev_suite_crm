<html>
	<head><title>Upload Custom Script</title></head>
	<body style="margin: 0px;">
		<div style="background-color: white; width: 100%; height: 100%;">
		<?php 
		if(isset($_POST['submit'])){
			if(move_uploaded_file($_FILES['custom_script']['tmp_name'], getcwd() . '/modules/PM_ProcessManager/customScripts/' . $_FILES['custom_script']['name'])){
				echo "Uploaded.";
				die();
			} else {
				echo "Upload Failed.";
			}
		}
		?>

		<form enctype="multipart/form-data" method="POST" action="index.php?module=PM_ProcessManager&action=uploadCustomScript&to_pdf=1">
			<input type="hidden" name="MAX_FILE_SIZE" value="512000" />
			<input name="custom_script" type="file" />
			<input type="submit" name="submit" value="Upload Custom Script" />
		</form>
		</div>
	</body>
</html>
