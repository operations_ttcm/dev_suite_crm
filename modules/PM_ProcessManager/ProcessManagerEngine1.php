<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * Copyright: SierraCRM, Inc. 2013
 * Portions created by SierraCRM are Copyright (C) SierraCRM, Inc.
 * The contents of this file are subject to the SierraCRM, Inc. End User License Agreement
 * You may not use this file except in compliance with the License. 
 * You may not rent, lease, lend, or in any way distribute or transfer any rights or this file or Process Manager
 * registrations (purchased licenses) to third parties without SierraCRM, Inc. written approval, and subject to
 * agreement by the recipient of the terms of this EULA.
 * Process Manager for SugarCRM is owned by SierraCRM, Inc. and is protected by international and local copyright laws and
 * treaties. You must not remove or alter any copyright notices on any copies of Process Manager for SugarCRM. 
 * You may not use, copy, or distribute Process Manager for SugarCRM, except as granted by SierraCRM, Inc.
 * without written authorization from SierraCRM, Inc. or its designated agents. Furthermore, this Copyright notice
 * does not grant you any rights in connection with any trademarks or service marks of SierraCRM, Inc. 
 * SierraCRM, Inc. reserves all intellectual property rights, including copyrights, and trademark rights of this software.
 ********************************************************************************/
/*********************************************************************************
 *SierraCRM, Inc
 *14563 Ward Court
 *Grass Valley, CA. 95945
 *www.sierracrm.com
 ********************************************************************************/
require_once('data/SugarBean.php');
require_once('include/TimeDate.php');
global $previousTaskId;
global $runningProcessManager;
class ProcessManagerEngine1 extends ProcessManagerEngine {


	var $object_name = "ProcessManageEngine1";
	var $module_dir = 'ProcessManager';

	function ProcessManagerEngine1() {
		global $sugar_config;
		//parent::SugarBean();
	}
  public function __construct()
    {
        global $db;
        $this->db = $db;
    }
	var $new_schema = true;


	//**************************************************************************************************
	//This is the main control block for the process manager engine for all process that are of type
	//modify and not create.
	//**************************************************************************************************
	function processManagerMain1($focusObjectId,$focusObjectType,$focusObjectEvent,$isDefault,$thisprocessManagerMain1){
		//First thing we are going to do is see if the focus object has a process setup for
		//a non create event and is a default process
		$doesObjectHaveNonCreateProcess = $this->checkObjectProcessNonCreate($focusObjectId,$focusObjectType,$focusObjectEvent,true);
		return;
	}
//**************************************************************************************************
//Here we are querying the pm_process_mgr_table to see if the object has a non create process
//**************************************************************************************************
function checkObjectProcessNonCreate($focusObjectId,$focusObjectType,$focusObjectEvent,$isDefault){
	//First see if the object that we are checking on has a process
	//with a valid cancel event. Is so run the cancel event by removing the queued stages
	$processManagerEngine2 = new ProcessManagerEngine2();
	$queryCancel = "Select * from pm_processmanager where start_event != 'Recurring Process' and process_object = '" .$focusObjectType ."' and deleted = 0 and status = 'Active'";
	$resultCancel = $this->db->query($queryCancel);
	//If we end up with a non create process then we must also have a filter table entry
		$counter = 1;
		while($row = $this->db->fetchByAssoc($resultCancel)){
			if($this->getFocusObjectDeletedFlag($focusObjectId,$focusObjectType)){
				//First cancel all stages and tasks waiting
				$this->cancelProcess($focusObjectId,$focusObjectType,$row['id'],'Cancelled');
			}
			$this->checkCancelFilterFields($row['id'],'', $focusObjectId, $focusObjectType);
		}
	
	//*********************************************************************************
	//Done with the check for any cancel events now go and get all the modify processes 
	//*********************************************************************************
	$query = "Select * from pm_processmanager where  start_event != 'Recurring Process' AND (start_event = 'Modify' OR start_event = 'Create or Modify') and process_object = '" .$focusObjectType ."'";
	$query .=" and status = 'Active' and deleted = 0";
	$result = $this->db->query($query);
	//If we end up with a non create process then we must also have a filter table entry
		$counter = 1;
		while($row = $this->db->fetchByAssoc($result)){
			$process_id = $row['id'];
			$process_event = $row['start_event'];
			$processObject = $row['process_object'];
		  //PM Pro v2
			$related_object = $row['related_object'];
			$rowProcessDefs = $this->getProcessDefs($process_id);
			$checkForIgnoreAlreadyDone = $rowProcessDefs['ignore_process_prev_run_check'];
			if ($checkForIgnoreAlreadyDone == 1) {
				$isDefaultProcessAlreadyDone = false;
			}
			else{
				$isDefaultProcessAlreadyDone = $this->checkIfDefaultProcessAlreadyDone($focusObjectId,$process_id,$this);		
			}
			if (!$isDefaultProcessAlreadyDone) {				
				$this->checkFilterFields($process_id,$related_object, $focusObjectId, $focusObjectType);
			}
		}
}	
	
//***********************************************************************************************
//This function will return the deleted flag for the given focus object
//***********************************************************************************************

function getFocusObjectDeletedFlag($focusObjectId,$focusObjectType){
	$query = "Select deleted from $focusObjectType where id = '" .$focusObjectId ."'";
	$result = $this->db->query($query);
	$row = $this->db->fetchByAssoc($result);
	$isDeleted = $row['deleted'];
	if ($isDeleted == 1) {
		return true;
	}
	else{
		return false;
	}
}
function cancelProcess($focusObjectId,$focusObjectType,$process_id,$stage_id){
	$this->purgePendingStagesAndTasks($focusObjectId,$process_id);
		//Tell Process Manager that we are done with the Process
	$this->insertIntoProcessCompleted($focusObjectId,$process_id, $stage_id,$focusObjectType);
}
function purgePendingStagesAndTasks($focusObjectId,$process_id){
		//First get the stages
		$queryDeleteStages = "Delete from pm_process_stage_waiting_todo where object_id = '" .$focusObjectId ."' and process_id = '$process_id'";
		$this->db->query($queryDeleteStages, true);
		
		//Now get the tasks
		$queryDeleteTasks = "Delete from pm_process_task_waiting_todo where object_id = '" .$focusObjectId ."'";
		$this->db->query($queryDeleteTasks, true);
	
}
//************************************************************************************
//Functions to support the new feature for creating objects/records
//************************************************************************************

function createRecord($processID,$stageID,$focusObjectType,$focusObjectId,$rowTask){
		//Go and get the defs record from pm_process_task_object_defs
		global $current_user;
		global $previousTaskId;
		global $newBean;
		$taskId = $rowTask['id'];
		$GLOBALS['log']->info("Process Manager - Creating a new Sugar Record for object type $focusObjectType and object id $focusObjectId and task id $taskId ");	
		
		if ($rowTask['start_delay_type'] == 'From Completion of Previous Task'){
			$this->insertTaskIntoWaitingTable($processID,$stageID,$focusObjectId,$focusObjectType,$rowTask['id'],$rowTask['task_order'],$focusObjectType);
			return;
		}
		//Get the defs file	
		$queryTaskCreateRecordDefs = "Select * from pm_process_task_create_object_defs where task_id = '" .$taskId ."'";
		$resultTaskCreateRecordDefs = $this->db->query($queryTaskCreateRecordDefs);
		$rowTaskCreateRecordDefs = $this->db->fetchByAssoc($resultTaskCreateRecordDefs);
		//Go and get the template to be used
		$createObjectType = $rowTaskCreateRecordDefs['create_object_type'];
		$createObjectId  = $rowTaskCreateRecordDefs['create_object_id'];
		$inheritParent = $rowTaskCreateRecordDefs['inherit_parent_data'];
		$inheritParentRelationships = $rowTaskCreateRecordDefs['inherit_parent_relationships'];
        $relate_object_to_parent = 1;
		//$relate_object_to_parent = $rowTaskCreateRecordDefs['relate_object_to_parent'];
		$create_record_method = isset($rowTaskCreateRecordDefs['create_record_method']) ? $rowTaskCreateRecordDefs['create_record_method'] : '';
		//$newBean = $this->createNewBean($createObjectType,$newBean);
		$newBean = $this->convertTableToBean($createObjectType);
  		if(empty($newBean)){
				return;
			}
		//Go and get the parent record
		$queryParentRecord = "Select * from $focusObjectType where id = '$focusObjectId'";
		$resultParentRecord = $this->db->query($queryParentRecord);
		$rowParentRecord = $this->db->fetchByAssoc($resultParentRecord);
		if ($create_record_method == 'By Template') {
			//Use the template
			$newBean = $this->getCreateRecordTemplate($newBean,$createObjectType,$createObjectId,$rowTaskCreateRecordDefs);
			//If Not Found - exit
			if($newBean === FALSE)
				return;
		}
		else if($create_record_method == 'By Field'){
			$newBean = $this->createRecordByField($newBean, $focusObjectType, $focusObjectId, $rowTaskCreateRecordDefs);
		}	
		else{
			//Go and get the parent record
			$queryParentRecord = "Select * from $focusObjectType where id = '$focusObjectId'";
			$resultParentRecord = $this->db->query($queryParentRecord);
			$rowParentRecord = $this->db->fetchByAssoc($resultParentRecord);
			//Now go and set any values from the parent
			$newBean = $this->createRecordSetParentValues($newBean,$createObjectType,$rowParentRecord,$rowTaskCreateRecordDefs);			
		}
		//If the assigned to is empty set to same value as the focus object
		if(empty($newBean->assigned_user_id)){
			$newBean->assigned_user_id = $rowParentRecord['assigned_user_id'];
		}
		//Now save the new bean
		$newBean->save(false);
        //If Method is By Parent and Object is Quote - Grab the Line Items and create new ones and add to the Quote
        if ($create_record_method == 'By Parent' && $createObjectType == 'quotes') {
            $newBean = $this->addQuoteLineItems($newBean,$focusObjectId);
            //Now update the Quote
            $qupdateq = "update quotes set subtotal = '" .$rowParentRecord['subtotal'] ."', subtotal_usdollar = '" .$rowParentRecord['subtotal_usdollar'] ."' ,
                         new_sub = '" .$rowParentRecord['new_sub'] ."', new_sub_usdollar = '" .$rowParentRecord['new_sub_usdollar'] ."' ,
                         total = '" .$rowParentRecord['total'] ."' , total_usdollar = '" .$rowParentRecord['total_usdollar'] ."' where id = '" .$newBean->id ."'";
            $this->db->query($qupdateq);
            //Relate the new quote to the Ship to and Bill To Accounts
            $qacctq = "select * from quotes_accounts where quote_id = '$focusObjectId' and deleted = 0";
            $resacctq = $this->db->query($qacctq);
            while($rowacctq = $this->db->fetchByAssoc($resacctq)){
                $relarray = array('quote_id' => $newBean->id, 'account_id' => $rowacctq['id'], 'account_role' => $rowacctq['account_role']);
                $newBean->set_relationship('quotes_accounts', $relarray);
            }
        }
		if(($create_record_method == 'By Template') && (($newBean->table_name == 'project') || ($newBean->table_name == 'aos_quotes'))){
				$queryRecordTemplate = "Select id from $createObjectType where name = '$createObjectId' and id != '" .$newBean->id ."' and deleted = 0";
	      $resultRecordTemplate = $this->db->query($queryRecordTemplate);
	      $rowRecordTemplate = $this->db->fetchByAssoc($resultRecordTemplate);
	      //Get all the project tasks
	      if($newBean->table_name == 'project'){
		      $qpjids = "select * from project_task where project_id = '" .$rowRecordTemplate['id'] ."' and deleted = 0";
		      $resultpjt = $this->db->query($qpjids);
		      while($rowpjs = $this->db->fetchByAssoc($resultpjt)){
		      	$newProjectTask = new ProjectTask();
		      	foreach($rowpjs as $field => $value){
			      	if($field == 'id'){
			      			continue;
			      	}
			      	else{
			      		$newProjectTask->$field = $value;
			      	}
		      	}
		        $newProjectTask->project_id = $newBean->id;
			      $newProjectTask->save();	      
					}
				}
				if($newBean->table_name == 'aos_quotes'){
					$qapq = "select * from aos_products_quotes where parent_id = '" .$rowRecordTemplate['id'] ."' and deleted = 0";
		      $resultpjt = $this->db->query($qapq);
		      while($rowpjs = $this->db->fetchByAssoc($resultpjt)){
		      	$newAOS_Products_Quotes = new AOS_Products_Quotes();
		      	foreach($rowpjs as $field => $value){
			      	if($field == 'id'){
			      			continue;
			      	}
			      	else{
			      		$newAOS_Products_Quotes->$field = $value;
			      	}
		      	}
		        $newAOS_Products_Quotes->parent_id = $newBean->id;
			      $newAOS_Products_Quotes->save();	      
					}					
				}
		}			
		
		if ($inheritParentRelationships == 1) {
			//TODO
		}
		//Relate New Bean to Parent
		if ($relate_object_to_parent == 1) {
			//Use the template
			$newBean = $this->relateNewBeanToParent($newBean,$focusObjectType,$focusObjectId);
		}
        //PM 4.1.3: Support for calls_users, calls_leads, calls_contacts
        if($focusObjectType == 'calls'){
            if($newBean->parent_type == 'Leads' || $newBean->parent_type == 'Accounts' || $newBean->parent_type == 'Contacts' || $newBean->parent_type == 'Users'){
                //Set the link table
                if($newBean->parent_type == 'Leads'){
                    $relarray = array('call_id' => $newBean->id, 'lead_id' => $newBean->parent_id);
                    $newBean->set_relationship('calls_leads', $relarray);
                }
                if($newBean->parent_type == 'Contacts'){
                    $relarray = array('call_id' => $newBean->id, 'contact_id' => $newBean->parent_id);
                    $newBean->set_relationship('calls_contacts', $relarray);
                }
                if($newBean->parent_type == 'Accounts'){
                    $relarray = array('call_id' => $newBean->id, 'contact_id' => $newBean->parent_id);
                    $newBean->set_relationship('calls_contacts', $relarray);
                }
                if($newBean->parent_type == 'Users'){
                    $relarray = array('call_id' => $newBean->id, 'user_id' => $newBean->parent_id);
                    $newBean->set_relationship('calls_users', $relarray);
                }
            }
        }
		$previousTaskId = $newBean->id;
		return $previousTaskId;
}

function createRecordByField($newBean, $focusObjectType, $focusObjectId, $rowTaskCreateRecordDefs){
	$focusBean = $this->convertTableToBean($focusObjectType);
  		if(empty($focusBean)){
				return;
			}
    //$focusBean = BeanFactory::retrieveBean($focusBean->module_name, $focusObjectId, array('disable_row_level_security' => true));
    $focusBean->retrieve($focusObjectId);
	$getdefs = "SELECT * FROM pm_process_task_create_by_field_defs WHERE task_id = '{$rowTaskCreateRecordDefs['task_id']}'";
	$getdefsres = $this->db->query($getdefs);
	$found = false;
    $teamSet = FALSE;
	while($getdefsrow = $this->db->fetchByAssoc($getdefsres)){
		$found = true;
		$field = $getdefsrow['create_by_field_field'];
		$method = $getdefsrow['create_by_field_method'];
		//if(property_exists($newBean, $field)){
			if($method == 'by_date'){
				$value = $getdefsrow['create_by_field_by_date'];
				//Determine if the field is a date or datetime field
				$fieldType = $this->getFieldType($newBean, $field);
				if(($fieldType == 'datetime') || ($fieldType == 'datetimecombo')){
				    $newBean->{$field} = gmdate('Y-m-d H:i:s', strtotime($value));
				}
				elseif($fieldType == 'date'){
					$newBean->{$field} = gmdate('Y-m-d', strtotime($value));
				}	
					
			}else if($method == 'by_value'){
				$value = $getdefsrow['create_by_field_by_value'];
				//Check for Variable Data
				if (strchr($value,"$")) {
					require_once('modules/PM_ProcessManager/ProcessManagerEngine2.php');
					$processManagerEngine2 = new ProcessManagerEngine2();
					$value = $processManagerEngine2->parseSubjectTemplate($value,$focusObjectType,$focusObjectId,$focusObjectType, $focusObjectId);
				}
				if(($field == 'created_by') || ($field == 'modified_user_id') || ($field == 'assigned_user_id')){
					$queryUserTable = "select id from users where user_name = '$value' and deleted = 0";
					$resultUserTable = $this->db->query($queryUserTable,true);
					$rowUserTable = $this->db->fetchByAssoc($resultUserTable);				
					$value = $rowUserTable['id'];
				}
                if(($field == 'team_id')){
                    $queryTeamTable = "select id from teams where name = '$value' and deleted = 0";
                    $resultTeamTable = $this->db->query($queryTeamTable,true);
                    $rowTeamTable = $this->db->fetchByAssoc($resultTeamTable);
                    $value = $rowTeamTable['id'];
                    $teamSet = TRUE;
                    $team_id = $value;
                }
				$newBean->{$field} = $value;	
			}else if($method == 'from_field'){
				$value = $getdefsrow['create_by_field_by_field'];
				$newBean->{$field} = $focusBean->{$value};	
			}	
		//}
		//New Code for Calls and Tasks Esclation
		if(($newBean->object_name == 'Call') || ($newBean->object_name == 'Task')){
			if ($newBean->object_name == 'Call'){
				$dueDate = $newBean->date_start;
				$spaceLocation = strpos($dueDate," ");
				$taskDueDate = substr($dueDate,0,$spaceLocation);
				$taskDueTime = substr($dueDate,$spaceLocation);
				$newBean->initial_date_start_c = $taskDueDate;
				$newBean->initial_time_start_c = $taskDueTime;
			}
			if ($newBean->object_name == 'Task'){
				$dueDate = $newBean->date_due;
				$spaceLocation = strpos($dueDate," ");
				$taskDueDate = substr($dueDate,0,$spaceLocation);
				$taskDueTime = substr($dueDate,$spaceLocation);
				$newBean->initial_date_due_c = $taskDueDate;
				$newBean->initial_time_due_c = $taskDueTime;
			}			
		}
		//Support for SugarFeeds
		if($newBean->object_name == 'SugarFeed'){
			$newBean->related_module = $focusBean->module_dir;
			$newBean->related_id = $focusBean->id;
			$newBean->assigned_user_id = $focusBean->assigned_user_id;
			//Finalize the actual Description
			//Get the user
			$queryUsers = "Select first_name, last_name  from users where id = '" .$focusBean->modified_user_id ."'";
	        $result = $this->db->query($queryUsers, true);
	        $rowUser = $this->db->fetchByAssoc($result);
			$username = $rowUser['first_name'] ." " .$rowUser['last_name'];
			$value = $newBean->name;
			//Check to see if there is any variable data
			if (strchr($value,"$")) {
				require_once('modules/PM_ProcessManager/ProcessManagerEngine2.php');
				$processManagerEngine2 = new ProcessManagerEngine2();
				$value = $processManagerEngine2->parseTemplate($value,$focusObjectType,$focusObjectId,$focusBean-table_name, $focusObjectId);
			}			
			$newBean->name = "<b>" .$focusBean->object_name ."</b> has been modified by <b>" .$username ."</b> " .$value ."[" .$focusBean->module_dir .":" .$focusBean->id .":" .$focusBean->name ."]";
		}
	}
    if($teamSet){
        require_once ('modules/Teams/TeamSet.php');
        $teamSetBean = new TeamSet();
        $teams = $teamSetBean->getTeams($newBean->team_set_id);
        $team_id = array_keys($teams);
        $newBean->load_relationship('teams');
        $newBean->teams->add($team_id);
    }
	return $newBean;

}

function getFieldType($bean, $field){	
	foreach($bean->field_defs as $def){
		if($def['name'] == $field){
			$found = $def;
			break;
		}
	}
	return $found['type'];
}

function sendEmailToEmailQueue($focusObjectId,$focusObjectType,$emailQueueCampaignName){
	//Get the campaign id, marketing info and prospect list
	require_once 'modules/EmailMan/EmailMan.php';
	$thisEmailMan = new EmailMan();
	$today = gmdate('Y-m-d H:i:s');
	$beanName = $this->convertTableToBean($focusObjectType);
  if(empty($beanName)){
				return;
	}		
	$queryCampaign = "Select id from campaigns where name = '$emailQueueCampaignName'";
	$resultCampaign = $this->db->query($queryCampaign);
	$rowCampaign = $this->db->fetchByAssoc($resultCampaign);	
	$campaignID = $rowCampaign['id'];
	//Now go and get the the marketing and prospect list info
	//Table is email_marketing
	$queryEmailMktQ = "select id from email_marketing where campaign_id = '$campaignID'";
	$resultEmailMktQ = $this->db->query($queryEmailMktQ);
	$rowEmailMktQ = $this->db->fetchByAssoc($resultEmailMktQ);
	$EmailMktQID = $rowEmailMktQ['id'];	
	
	//Get the prospect list
	$pListQ = "select prospect_list_id from prospect_list_campaigns, prospect_lists
               where prospect_list_campaigns.campaign_id = '$campaignID'
               and prospect_list_campaigns.prospect_list_id = prospect_lists.id
               and prospect_lists.deleted = 0 and prospect_list_campaigns.deleted = 0";
	$resultpListQ = $this->db->query($pListQ);
	$rowpListQ = $this->db->fetchByAssoc($resultpListQ);
	$pListQID = $rowpListQ['prospect_list_id'];
	//Make sure the record is not already in the EmailMan
	$queryEmailMan = "select id from emailman where related_id = '$focusObjectId' and related_type = '$beanName->module_name' ";
	$resultEmailMan = $this->db->query($queryEmailMan);
	$rowEmailMan = $this->db->fetchByAssoc($resultEmailMan);
	$EmailManID = $rowEmailMan['id'];
			
	//Insert the record into emailman
	if(($EmailMktQID != '') && ($pListQID != '')){
		if($EmailManID == ''){
			$thisEmailMan->campaign_id = $campaignID;
			$thisEmailMan->marketing_id = $EmailMktQID;
			$thisEmailMan->list_id = $pListQID;
			$thisEmailMan->send_date_time = $today;
			$thisEmailMan->in_queue = 0;
			$thisEmailMan->related_type = $beanName->module_name;
			$thisEmailMan->related_id = $focusObjectId;
			$thisEmailMan->send_attempts = 0;
			$thisEmailMan->deleted = 0;
			$thisEmailMan->save();
            //Now add the object to the Prospect List
            $iquery ="INSERT INTO prospect_lists_prospects (`id`,`prospect_list_id`, `related_id`, `related_type`,`date_modified`) ";
            $iquery .= "VALUES ("."'".create_guid()."',"."'".$pListQID."',"."'".$focusObjectId."',"."'".$beanName->module_name."',"."'".TimeDate::getInstance()->nowDb()."')";
            $this->db->query($iquery); //save the record.
		}	
	}

}

//*****************************************************************************
//Get Contact Email - New for PM Pro 2.0
//*****************************************************************************

function getContactEmail($focusObjectType,$focusObjectId){
	//We can be sent any object and we just need to get the related contact
	//IE - cases, accounts, 
	//First Convert the table to a Bean then go and get the linked Contacts
	$beanName = $this->convertTableToBean($focusObjectType);
  if(empty($beanName)){
				return;
	}	
	$beanName->retrieve($focusObjectId);
	$linkedFields = $beanName->get_linked_fields();
	foreach($linkedFields as $array){
		$moduleName = $array['name'];
		if(($moduleName == 'Contacts') || ($moduleName == 'contacts')){
			//Get the relationship name
			$relationship = $array['relationship'];
			$name = $array['name'];
			break;
		}
	}
	if($relationship != ''){
		//Get all the related contacts and run them through the runEmailTask
		$sortArray = array();
		$thisSugarBean = new SugarBean();
		$beanName->load_relationship($name);
		$contacts = $beanName->get_linked_beans($name, 'Contact','','',1,0,'');
		foreach ($contacts as $contactArray){
			$contactID = $contactArray->id;
			return $contactID;
		}
		
	}
}


//*********************************************************************************
//This function will go and get the record that is used as the template
//********************************************************************************

function getCreateRecordTemplate($newBean,$createObjectType,$createObjectId,$rowTaskCreateRecordDefs){
	//Get the template record
	$queryRecordTemplate = "Select * from $createObjectType where name = '$createObjectId'";
	$resultRecordTemplate = $this->db->query($queryRecordTemplate);
	$rowRecordTemplate = $this->db->fetchByAssoc($resultRecordTemplate);
	//If empty return FALSE
	if(empty($rowRecordTemplate)){
		return FALSE;
	}
	else{	
		$newBean = $this->setNewRecordValues($newBean,$createObjectType,$rowRecordTemplate);
		return $newBean;
	}	
}

//******************************************************************************
//If we are creating an opportunity or quote then we set the 
//*****************************************************************************
function createRecordSetParentValues($newBean,$createObjectType,$rowParentRecord,$rowTaskCreateRecordDefs){
	global $newBean;
	$newBean = $this->setNewRecordValues($newBean,$createObjectType,$rowParentRecord);
	//For some beans we need to set the data fields here and not just transfer the data over
	switch ($createObjectType) {
	    case "leads":
	        break;
	    case "contacts":
	        break;
	    case "quotes":
	    	//We add one to the system_id
	    	//$systemID = $newBean->system_id;
	    	//$systemID++;
	    	//Get the total number of quotes there are for this quote and increment
	    	$systemID = $this->getTotalQuotes($rowParentRecord);
	    	$newBean->system_id = $systemID;  
	        break;
	   	case "opportunities":
	        break;
	   	case "cases":
	   		//Get the next case 
	   		$nextCaseId = $this->getTableCount("cases");
	   		$newBean->case_number = $nextCaseId;  
	        break;
	   	case "bugs":
	        break; 	         	            
	}
	return $newBean;
}

    function addQuoteLineItems($newBean,$focusObjectId){
        //Get all the Product Bundles
        require_once('modules/ProductBundles/ProductBundle.php');
        require_once('modules/Products/Product.php');
        $queryQPB = "select bundle_id,bundle_index from product_bundle_quote where quote_id = '$focusObjectId' and bundle_index != 0 and deleted = 0 ORDER BY bundle_index DESC";
        $resultQPB = $this->db->query($queryQPB);
        while($rowQPB = $this->db->fetchByAssoc($resultQPB)){
            $bundleID = $rowQPB['bundle_id'];
            $PB = new ProductBundle();
            $newPB = new ProductBundle();
            $PB->retrieve($bundleID);
            //Create the new Bundle
            $exclude = array(
                'id',
                'date_entered',
                'date_modified'
            );
            foreach($PB->field_defs as $def){
                if(!(isset($def['source']) && $def['source'] == 'non-db')
                    && !empty($def['name'])
                    && !in_array($def['name'], $exclude)){
                    $field = $def['name'];
                    $newPB->{$field} = $PB->{$field};
                }
            }
            $newPB->save();
            $relarray = array('bundle_id' => $newPB->id, 'quote_id' => $newBean->id);
            $newBean->set_relationship('product_bundle_quote', $relarray);
            //Get the Products
            $products = $PB->getProducts();
            foreach($products as $product){
                 $newProd = new Product();
                    $exclude = array(
                        'id',
                        'date_entered',
                        'date_modified'
                    );
                    foreach($product->field_defs as $def){
                        if(!(isset($def['source']) && $def['source'] == 'non-db')
                            && !empty($def['name'])
                            && !in_array($def['name'], $exclude)){
                            $field = $def['name'];
                            $newProd->{$field} = $product->{$field};
                        }
                    }
                $newProd->save();
                $relarray = array('bundle_id' => $newPB->id, 'product_id' => $newProd->id);
                $newBean->set_relationship('product_bundle_product', $relarray);
            }
        }
        return $newBean;
    }
//*********************************************************************************
//This function will take the record from the parent or template
//and also a result set from show fields for MYSQL and create all the
//fields values in the new bean
//***********************************************************************************
function setNewRecordValues($bean,$createObjectType,$parentTemplateRecord){
    //$bean = BeanFactory::retrieveBean($bean->module_name, $parentTemplateRecord['id'], array('disable_row_level_security' => true));
    $bean->retrieve($parentTemplateRecord['id']);
	$exclude = array(
		'id',
		'date_entered',
		'date_modified'
	);
	$newbean = new $bean->object_name;
	foreach($bean->field_defs as $def){
		if( !in_array($def['name'], $exclude)){
			$field = $def['name'];
			$newbean->{$field} = $bean->{$field};
		}
	}
	return $newbean;
}

//*************************************************************
//This function holds thoses fields that we do not
//want to set - but instead let the save bean create the values
//*************************************************************
function dontIncludeInFieldList(){
		$array_fields_dont_include = array();
		$array_fields_dont_include[] = "id";
		$array_fields_dont_include[] = "date_entered";
		$array_fields_dont_include[] = "date_modified";
		return $array_fields_dont_include;
}

function getDueDate($startDelayDays,$startDelayHours,$startDelayMinutes,$startDelayMonths,$startDelayYears){
	$timezone = date('Z') / 3600;
	$timezone = substr($timezone,1);
	$today = date('Y-m-d H:i:s', time() + $timezone * 60 * 60);
	$dateParts = preg_split("/[\s-:]+/", $today);
	$year = $dateParts[0];
	$hour = $dateParts[3];
	$min = $dateParts[4];
	$month = $dateParts[1];
	$day = $dateParts[2];
	if ($startDelayDays != 0) {
		$day = $dateParts[2] + $startDelayDays;
	}
	if ($startDelayHours != 0) {
		$hour = $dateParts[3] + $startDelayHours;
	}
	if ($startDelayMinutes != 0) {
		$min = $dateParts[4] + $startDelayMinutes;
	}
	if ($startDelayMonths != 0) {
		$month = $dateParts[1] + $startDelayMonths;
	}
	if ($startDelayYears != 0) {
		$year = $dateParts[0] + $startDelayYears;
	}
	//Make seconds = 00
	$sec = '00';
	$StartDateWhole = date("Y-m-d H:i:s",mktime ($hour, $min, $sec, $month, $day, $year));
	return $StartDateWhole;
}


//*********************************************************************************
//Relate the newly create bean to the parent that triggered the process
//*******************************************************************************

function relateNewBeanToParent($newBean,$focusObjectType,$focusObjectId){
	
	//Get the name of the link between the new bean and the parent
	//We call this function from SugarBean - handle_request_relate($new_rel_id, $new_rel_link)
	$focusBean = $this->convertTableToBean($focusObjectType);
  if(empty($focusBean)){
				return $newBean;
	}
   // $focusBean = BeanFactory::retrieveBean($focusBean->module_name, $focusObjectId, array('disable_row_level_security' => true));
	$focusBean->retrieve($focusObjectId);
	$focusBean->load_relationships();
	$linkedFields = $focusBean->get_linked_fields();
	$finalOject = $newBean->table_name;
	foreach ($linkedFields as $link){
		$linkName = $link['name'];
		$lhsTable = $focusBean->$linkName->relationship->def['lhs_table'];
		$rhsTable = $focusBean->$linkName->relationship->def['rhs_table'];
		//Get the name of the RHS table
    if (($rhsTable == $finalOject) || ($lhsTable == $finalOject) && ($linkName != 'task_parent') ){
			//We have the link name and it is rhs
			break;
		}
	}
	$new_rel_id = $newBean->id;
	//Does not work with Sugar 6.4.4 
	//Need to use
	if((empty($lhsTable)) && (empty($rhsTable))){
		return $newBean;
	}
	$focusBean->$linkName->add($new_rel_id);
	//$focusBean->handle_request_relate($new_rel_id, $linkName);
	return $newBean;
	
}

function getRelationshipArray($createObjectType){
	$arrayRelationships = array();
	switch ($createObjectType) {
	    case "leads":
	        break;
	    case "contacts":
	        break;
	    case "quotes":
	    	$arrayRelationships['quotes_contacts'] = 'contact_id';
	    	$arrayRelationships['quotes_opportunities'] = 'opportunity_id';
	        break;
	   	case "opportunities":
	   		$arrayRelationships['opportunities_contacts'] = 'contact_id';
	   		$arrayRelationships['quotes_opportunities'] = 'quote_id';		
	        break;
	   	case "cases":
	        break;
	   	case "bugs":
	        break; 	         	            
	}
	return $arrayRelationships;
}

//****************************************************************************
//This function will get the related id from the relationship table for the given
//relationship for the new bean.
//****************************************************************************

function getRelatedID($tableName,$parentRecordColumn,$parentRecordId,$relatedColumnID){
	$queryRelatedID = "Select $relatedColumnID from $tableName where $parentRecordColumn = '$parentRecordId'";
	$resultRelatedRecord = $this->db->query($queryRelatedID);
	$rowRelatedRecord = $this->db->fetchByAssoc($resultRelatedRecord);
	$relatedID = $rowRelatedRecord[$relatedColumnID];
	return $relatedID;
}


//This functio will get the product bundles
function checkQuoteBundles($parentRecordId){
	$queryBundles = "Select * from product_bundle_quote where quote_id  = '$parentRecordId'";
	$resultQuoteBundles = $this->db->query($queryBundles);
	return $resultQuoteBundles;
}

function getProductBundleProduct($bundleId){
	$queryProductBundleProduct = "Select * from product_bundle_product where bundle_id = '$bundleId'";
	$resultProductBundleProduct = $this->db->query($queryProductBundleProduct);
	return $resultProductBundleProduct;	
}

//Function to get the product bundle when passed the product bundle quote
function getProductBundle($rowQuoteBundles){
	$productBundleId = $rowQuoteBundles['bundle_id'];
	$query = "Select * from product_bundles where id = '$productBundleId'";
	$result = $this->db->query($query);
	$row = $this->db->fetchByAssoc($result);
	return $row;
}

function getTableCount($table){
	$query = "select count() from $table ";
	$result = $this->db->query($query);
	$row = $this->db->fetchByAssoc($result);
	$count = $row['count(*)'];
	return $count;
}

//This function will find out how many quotes there are for a given name and return the count/
//Used to determint the system id

function getTotalQuotes($rowParentRecord){
	$quoteName = $rowParentRecord['name'];
	$query = "select id from quotes where name = '$quoteName'";
	$result = $this->db->query($query);
	$count = 1;
	while($row = $this->db->fetchByAssoc($result)){
		$count++;
	}
	return $count;
}

//**********************************************************************
//This function will return the results of the products for the parent quote
//**********************************************************************

function getProduct($parentRecordId){
	$queryProducts = "Select * from products where id  = '$parentRecordId'";
	$resultQuoteProducts = $this->db->query($queryProducts);
	$rowProduct = $this->db->fetchByAssoc($resultQuoteProducts);
	return $rowProduct;
}

function convertTableToBean($focusObjectType){
	global $moduleList;
	global $beanList;
	$exemptModules = array();
	$exemptModules['Calendar.php'] = "Calendar.php";
	if(($focusObjectType == 'Users') || ($focusObjectType == 'users') || ($focusObjectType == 'user')){
		require_once("modules/Users/User.php");
		$newbean = new User();
		return $newbean;
	}
	if($focusObjectType == 'aCase' || $focusObjectType == 'Cases' || $focusObjectType == 'Case'){
		require_once("modules/Cases/Case.php");
		$newbean = new aCase();
		return $newbean;
	}
	if($focusObjectType == 'activities'){
			require_once("modules/ActivityStream/Activities/Activity.php");
			$newbean = new Activity();
			return $newbean;
	}	
	if($focusObjectType == 'sugarfeed'){
		require_once("modules/SugarFeed/SugarFeed.php");
		$newbean = new SugarFeed();
		return $newbean;
	}
	if($focusObjectType == 'project_task'){
		require_once("modules/ProjectTask/ProjectTask.php");
		$newbean = new ProjectTask();
		return $newbean;
	}
    if($focusObjectType == 'revenuelineitem' || $focusObjectType == 'RevenueLineItem'){
        require_once("modules/RevenueLineItems/RevenueLineItem.php");
        $newbean = new RevenueLineItem();
        return $newbean;
    }
    if($focusObjectType == 'product' || $focusObjectType == 'Products' || $focusObjectType == 'Product'){
        require_once("modules/Products/Product.php");
        $newbean = new Product();
        return $newbean;
    }
	foreach ($moduleList as $key=>$value){
		$module_array = array();
		$module_array[$key]=$value;
		$module_array = convert_module_to_singular($module_array);
		if($key != 0){
			$key1 = $module_array[$key];
			$bean = $key1;
			$key1 .= ".php";
			$beanFile = "modules/$value/$key1";
				if (!in_array($key1,$exemptModules)) {
					if ($bean == "Case") {
						$bean = "aCase";
					}
					if(file_exists($beanFile)){
						require_once("$beanFile");
						$newbean = new $bean;
						$table = $newbean->table_name;
						if ($focusObjectType == $table) {
							return $newbean;
						}
					}
			}	  
		}
	}
}



function convert_module_to_singular($module_array){
	global $beanList;

	foreach($module_array as $key => $value){
		if(!empty($beanList[$value])) $module_array[$key] = $beanList[$value];

		if($value=="Cases") {
			$module_array[$key] = "Case";
		}
		if($key=="projecttask"){
			$module_array['ProjectTask'] = "Project Task";
			unset($module_array[$key]);
		}
	}

	return $module_array;
}

}
?>
