<?php
/*********************************************************************************

 ********************************************************************************/
$dictionary['PM_ProcessManager'] = array(
	'table'=>'pm_processmanager',
	'audited'=>true,
	'fields'=>array (
  'status' => 
  array (
    'required' => false,
    'name' => 'status',
    'vname' => 'LBL_STATUS',
    'type' => 'enum',
    'massupdate' => 1,
    'default' => 'Active',
    'comments' => '',
    'help' => '',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => 0,
    'audited' => 0,
    'reportable' => 0,
    'len' => 100,
    'options' => 'process_status_dom',
    'studio' => 'visible',
  ),
  'assigned_user_link' => 
  array (
    'name' => 'assigned_user_link',
    'type' => 'link',
    'relationship' => 'pm_processmanager_assigned_user',
    'vname' => 'LBL_ASSIGNED_TO_USER',
    'link_type' => 'one',
    'module' => 'Users',
    'bean_name' => 'User',
    'source' => 'non-db',
    'duplicate_merge' => 'enabled',
    'rname' => 'user_name',
    'id_name' => 'assigned_user_id',
    'table' => 'users',
  ),
  'process_object' => 
  array (
    'required' => false,
    'name' => 'process_object',
    'vname' => 'LBL_PROCESS_OBJECT',
    'type' => 'char',
    'massupdate' => 0,
    //'default' => 'Lead',
    'comments' => '',
    'help' => '',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => 0,
    'audited' => 0,
    'reportable' => 0,
    'len' => 100,
 
  ),
  'start_event' => 
  array (
    'required' => false,
    'name' => 'start_event',
    'vname' => 'LBL_START_EVENT',
    'type' => 'enum',
    'massupdate' => 0,
    'default' => 'Create',
    'comments' => '',
    'help' => '',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => 0,
    'audited' => 0,
    'reportable' => 0,
    'len' => 100,
    'options' => 'process_start_event',
    'studio' => 'visible',
  ),
  'cancel_on_event' => 
  array (
    'required' => false,
    'name' => 'cancel_on_event',
    'vname' => 'LBL_CANCEL_ON_EVENT',
    'type' => 'enum',
    'massupdate' => 0,
    'default' => '--None--',
    'comments' => '',
    'help' => '',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => 0,
    'audited' => 0,
    'reportable' => 0,
    'len' => 100,
    'options' => 'process_cancel_event',
    'studio' => 'visible',
  ),
  'process_object_cancel_field' => 
  array (
    'required' => false,
    'name' => 'process_object_cancel_field',
    'vname' => 'LBL_PROCESS_OBJECT_CANCEL_FIELD',
    'type' => 'enum',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => 0,
    'audited' => 0,
    'reportable' => 0,
    'len' => 100,
    'studio' => 'visible',

  ),
  'process_object_cancel_field_value' => 
  array (
    'required' => false,
    'name' => 'process_object_cancel_field_value',
    'vname' => 'LBL_PROCESS_OBJECT_CANCEL_FIELD_VALUE',
    'type' => 'varchar',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => 0,
    'audited' => 0,
    'reportable' => 0,
    'len' => '25',
  ),
  
    'process_object_cancel_field_operator' => 
  array (
    'required' => false,
    'name' => 'process_object_cancel_field_operator',
    'vname' => 'LBL_PROCESS_OBJECT_CANCEL_FIELD_VALUE',
    'type' => 'enum',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => 0,
    'audited' => 0,
    'reportable' => 0,
    'len' => '25',
    
  ),
  



 //PATCH 05-04-2010 - Support for Teams in Pro and Enterprise
   'team_id' => 
  array (
    'required' => false,
    'name' => 'team_id',
    'vname' => 'LBL_TEAM_ID',
    'type' => 'varchar',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => 0,
    'audited' => 0,
    'reportable' => 0,
    'len' => '36',
    'default' => 'null',
  ), 
    'team_set_id' => 
  array (
    'required' => false,
    'name' => 'team_set_id',
    'vname' => 'LBL_TEAM_SET_ID',
    'type' => 'varchar',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => 0,
    'audited' => 0,
    'reportable' => 0,
    'len' => '36',
    'default' => 'null',
  ), 			
			'related_object' =>
			array (
				    'required' => false,
				    'name' => 'related_object',
				    'vname' => 'LBL_START_EVENT',
				    'type' => 'char',
				    'massupdate' => 0,
				    'default' => 'Create',
				    'comments' => '',
				    'help' => '',
				    'duplicate_merge' => 'disabled',
				    'duplicate_merge_dom_value' => 0,
				    'audited' => 0,
				    'reportable' => 0,
				    'len' => 100,
				
			),	

			'filter_on_related' =>
			array (
					'required' => false,
					'name' => 'filter_on_related',
					'vname' => 'LBL_START_EVENT',
					'type' => 'enum',
					'massupdate' => 0,
					'default' => '',
					'comments' => '',
					'help' => '',
					'duplicate_merge' => 'disabled',
					'duplicate_merge_dom_value' => 0,
					'audited' => 0,
					'reportable' => 0,
					'len' => 100,
					'options' => 'filter_on_related',
					'studio' => 'visible',
			),

			

//*******************************************************************
),
	'relationships'=>array (
  'pm_processmanager_assigned_user' => 
  array (
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'PM_ProcessManager',
    'rhs_table' => 'pm_processmanager',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
  ),
),
	'optimistic_lock'=>true,
	'unified_search' => false,
    'full_text_search' => false,
);
if (!class_exists('VardefManager')){
    require_once 'include/SugarObjects/VardefManager.php';
}
VardefManager::createVardef('PM_ProcessManager','PM_ProcessManager', array('basic','team_security','assignable'));