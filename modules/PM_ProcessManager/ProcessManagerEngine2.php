<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * Copyright: SierraCRM, Inc. 2105
 * Portions created by SierraCRM are Copyright (C) SierraCRM, Inc.
 * The contents of this file are subject to the SierraCRM, Inc. End User License Agreement
 * You may not use this file except in compliance with the License. 
 * You may not rent, lease, lend, or in any way distribute or transfer any rights or this file or Process Manager
 * registrations (purchased licenses) to third parties without SierraCRM, Inc. written approval, and subject to
 * agreement by the recipient of the terms of this EULA.
 * Process Manager for SugarCRM is owned by SierraCRM, Inc. and is protected by international and local copyright laws and
 * treaties. You must not remove or alter any copyright notices on any copies of Process Manager for SugarCRM. 
 * You may not use, copy, or distribute Process Manager for SugarCRM, except as granted by SierraCRM, Inc.
 * without written authorization from SierraCRM, Inc. or its designated agents. Furthermore, this Copyright notice
 * does not grant you any rights in connection with any trademarks or service marks of SierraCRM, Inc. 
 * SierraCRM, Inc. reserves all intellectual property rights, including copyrights, and trademark rights of this software.
 ********************************************************************************/
/*********************************************************************************
 *SierraCRM, Inc
 *388 Mill Street
 *Grass Valley, CA. 95945
 *www.sierracrm.com
 ********************************************************************************/

require_once('data/SugarBean.php');
require_once('include/TimeDate.php');
class ProcessManagerEngine2 extends ProcessManagerEngine {


	var $object_name = "ProcessManageEngine2";
	var $module_dir = 'ProcessManager';

	function ProcessManagerEngine2() {
		$GLOBALS['log'] = LoggerManager :: getLogger('SugarCRM');
		global $sugar_config;
		//parent::SugarBean();
	}
  public function __construct()
    {
        global $db;
        $this->db = $db;
    }
	var $new_schema = true;


	//**************************************************************************************************
	//Main Function to run all process or type - 'At This Time'
	//**************************************************************************************************
	function processManagerMain2(){
		    $GLOBALS['log']->info("ProcessManager - Checking for Process of type 'Recurring Process'");
			$queryAtThisTimeProcesses = "select pm.id, pm.name, pm.status, pm.process_object, pm.related_object, pm.start_event, pm.deleted, pmdefs.process_id,
						pmdefs.process_query_value, pmdefs.process_frequency, pmdefs.process_start_time_day, pmdefs.process_start_day_of_week, 
						pmdefs.process_day_of_month, pmdefs.process_month, pmdefs.process_last_run_date, pmdefs.route_object
						from pm_processmanager pm, pm_process_defs pmdefs
						where pm.start_event = 'Recurring Process' and pm.status = 'Active' and pm.deleted = 0 and pm.id = pmdefs.process_id";
			$resultAtThisTime = $this->db->query($queryAtThisTimeProcesses);
			while($rowProcess = $this->db->fetchByAssoc($resultAtThisTime)){	
				if ($this->processReadyToRun($rowProcess)) {
					//Set the last run date for the process to update when the process was last run
					$process_id = $rowProcess['id'];
					$timezone = date('Z') / 3600;
					 $timezone = substr($timezone,1);
					 $today = date('Y-m-d H:i:s');
					 $GLOBALS['log']->info("ProcessManager - Setting last run date to $today for process of type 'Recurring Process' and name " .$rowProcess['name']);
					 $queryUpdate = "update pm_process_defs set process_last_run_date = '$today' where process_id = '$process_id'";
					 $this->db->query($queryUpdate, true);
					 $GLOBALS['log']->info("ProcessManager - Processing Process of type 'Recurring Process' and name " .$rowProcess['name']);
					 //Now run the query
					 $focusObjectType = $rowProcess['process_object'];
					 $queryValue = str_replace('\"', '"', str_replace("\'", "'", html_entity_decode($rowProcess['process_query_value'], ENT_QUOTES)));
					 $queryValue = $this->stripAsciiCharacters($queryValue);
					 $resultQueryValue = $this->db->query($queryValue);
					 $routeProcess = $rowProcess['route_object'];
						if ($routeProcess == 1) {
							$processManagerEngine2 = new ProcessManagerEngine2();
							$rowProcessDefs = $this->getProcessDefs($process_id);
							$routeProcess = true;
						}					 
					 while($rowQueryValue = $this->db->fetchByAssoc($resultQueryValue)){
                         $focusObjectId = $rowQueryValue['id'];
                         $focusObjectType = $rowProcess['process_object'];
                         $relatedObject = $rowProcess['related_object'];
                         $this->checkFilterFields($process_id, $relatedObject, $focusObjectId , $focusObjectType);
					}
			
			}
			//return;
	}
	return;
}
	
	
//*********************************************************************
//This function determines if the process is ready to run based on the
//attributes of the at this time values
//*********************************************************************	
function processReadyToRun($rowProcess){
	//Get the variables that help determine if this campaign is ready to run
	//The server is in EST and the ui will tell the user to choose EST
	$GLOBALS['log']->info("Process Manager - Checking to see if the Process is ready to run");	
	$currentDate = date("Y-m-d H:i:s");
    $strToTimeCDate = strtotime($currentDate);
    $date = explode(" ", $currentDate);
    $d = $date[0];
    $t = $date[1];
    $dd = explode("-",$d);
    $tt = explode(":",$t);
    $year = $dd[0];
    $month = $dd[1];
    $day = $dd[2];
    $hour = $tt[0];
    $min = $tt[1];
    $sec = $tt[2];
	//list ($year, $month, $day, $hour, $min, $sec) = split ('[- :]', $currentDate);
	$frequency = $rowProcess['process_frequency'];
	$last_run_date = $rowProcess['process_last_run_date'];
    $strToTimeLastRunDate = strtotime($last_run_date);
	$GLOBALS['log']->info("Process Manager - Checking to see if the Process is ready to run. Last run date is $last_run_date");
	if (($last_run_date == null) || ($last_run_date == "")) {
		$last_run_date = "2000-01-01 00:00:00";
	}
	//list ($yearLastRunDate, $monthLastRunDate, $dayLastRunDate, $hourLastRunDate, $min, $sec) = split ('[- :]', $last_run_date);
    $date = explode(" ", $last_run_date);
    $d = $date[0];
    $t = $date[1];
    $dd = explode("-",$d);
    $tt = explode(":",$t);
    $yearLastRunDate = $dd[0];
    $monthLastRunDate = $dd[1];
    $dayLastRunDate = $dd[2];
    $hourLastRunDate = $tt[0];
    $minLastRunDate = $tt[1];
    $sec = $tt[2];

    $status = $rowProcess['status'];
	//Right off the bat - if the status is anything other than Active then dont run
	if ($status != 'Active') {
		return false;
	}
    if ($frequency == 'Every Time PM Runs') {
        return true;
    }
    if ($frequency == 'Every 15 Minutes') {
        if (($strToTimeCDate - $strToTimeLastRunDate) > 900) {
            return true;
        }
        else{
            return false;
        }
    }
    if ($frequency == 'Every 30 Minutes') {
        if (($strToTimeCDate - $strToTimeLastRunDate) > 1800) {
            return true;
        }
        else{
            return false;
        }
    }
	if ($frequency == 'Hourly') {
		if ($hour != $hourLastRunDate) {
				return true;			
		}
		else{
			return false;
		}
	}	
	if ($frequency == 'Daily') {
		//Have we reached the hour to run the campaign?
		//The current hour is in the $hour var
		$dailyHour = $rowProcess['process_start_time_day'];
		if ($hour >= $dailyHour) {
			//Now see if we have already run the campaign for today
			if ($day != $dayLastRunDate) {
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}

	//Now see if we are weekly - is it the right day of the week and is it passed the hour to run
	if ($frequency == "Weekly") {
		$weeklyDay = $rowProcess['process_start_day_of_week'];
		$weeklyHour = $rowProcess['process_start_time_day'];

		$todaysDay = $this->curdate();

		//Check to see if it is the right day of the week
		if ($weeklyDay == $todaysDay) {
			if ($weeklyHour <= $hour) {
				//Now see if we have already run the campaign for today
				if ($day != $dayLastRunDate) {
					return true;
				}
				else{
					return false;
				}
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}
	//Check for monthly
	if ($frequency == "Monthly") {
		$monthlyDay = $rowProcess['process_day_of_month'];
		$monthlyHour = $rowProcess['process_start_time_day'];
		if ($monthlyDay == $day) {
			if ($monthlyHour <= $hour) {
				if ($month != $monthLastRunDate) {
					return true;
				}
				else{
					return false;
				}

			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}
	//Check for Yearly
	if ($frequency == "Yearly") {
		$frequencyYearlyMonth = $rowProcess['process_month'];
		$monthlyDay = $rowProcess['process_day_of_month'];
		$monthlyHour = $rowProcess['process_start_time_day'];
		$frequencyMonth = $this->convertMonth($frequencyYearlyMonth);
		if ($frequencyMonth == $month) {
			if ($monthlyDay == $day) {
				if ($hour >= $monthlyHour) {
					if ($year != $yearLastRunDate) {
						return true;
					}
				}
				else{
					return false;
				}
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}

	}

}

function convertday($n){

	switch ($n){
		case "Mon": return "Monday";break;
		case "Tue": return "Tuesday";break;
		case "Wed": return "Wednesday";break;
		case "Thu": return "Thursday";break;
		case "Fri": return "Friday";break;
		case "Sat": return "Saturday";break;
		case "Sun": return "Sunday";break;
	};

}

function curdate(){

	$ret = $this->convertday(date("D"));

	return $ret;
}

function convertMonth($frequencyYearlyMonth){

	if ($frequencyYearlyMonth == 'January') {
		$month = '01';
	}
	if ($frequencyYearlyMonth == 'February') {
		$month = '02';
	}
	if ($frequencyYearlyMonth == 'March') {
		$month = '03';
	}	
	if ($frequencyYearlyMonth == 'April') {
		$month = '04';
	}
	if ($frequencyYearlyMonth == 'May') {
		$month = '05';
	}
	if ($frequencyYearlyMonth == 'June') {
		$month = '06';
	}
	if ($frequencyYearlyMonth == 'July') {
		$month = '07';
	}
	if ($frequencyYearlyMonth == 'August') {
		$month = '08';
	}
	if ($frequencyYearlyMonth == 'September') {
		$month = '09';
	}
	if ($frequencyYearlyMonth == 'October') {
		$month = '10';
	}
	if ($frequencyYearlyMonth == 'November') {
		$month = '11';
	}
	if ($frequencyYearlyMonth == 'December') {
		$month = '12';
	}		
	return $month;
}

    function isModule($module){
        global $moduleList;
        $exemptModules = array();
        $exemptModules['Calendar.php'] = "Calendar.php";
        if($module == 'Users'){
            return true;
        }
        if($module == 'aCase'){
            return true;
        }
        if($module == 'sugarfeed'){
            return true;
        }
        foreach ($moduleList as $key=>$value){
            $module_array = array();
            $module_array[$key]=$value;
            $module_array = convert_module_to_singular($module_array);
            if(!empty($key)){
                $key1 = $module_array[$key];
                $bean = $key1;
                $key1 .= ".php";
                $beanFile = "modules/$value/$key1";
                if (!in_array($key1,$exemptModules) && file_exists($beanFile)) {
                    if ($bean == "Case") {
                        $bean = "aCase";
                    }
                    require_once("$beanFile");
                    $newbean = new $bean;
                    $table = $newbean->table_name;
                    if ($module == $table) {
                        return true;
                    }
                }
            }
        }
    }

    function isRelationship($rel){
        try{
            $this->load_relationship($rel);
            return isset($this->$rel);
        }catch(Exception $e){
            return false;
        }
    }

    function getFromRel($rel){
        try{
            $this->load_relationship($rel);
            return isset($this->$rel[0]) ? $this->$rel[0] : false;
        }catch(Exception $e){
            return false;
        }
    }

//Custom SierraCRM
    //Custom SierraCRM
    function parseSubjectTemplate($string,$focusObjectType,$focusObjectId,$dbTable = "", $dbTableID = ""){
    	global $sugar_config;
    	require_once('modules/PM_ProcessManager/ProcessManagerEngine1.php');
    	$processManagerEngine1 = new ProcessManagerEngine1();
    	//For each row of the email template - see if there is a $ in the row
    	//If so - then get the position of the $ and from there find the next space
    	//Get this value see if there is an _
    	//If so we have either a focus object or a related object
    	//If the value of $xxxxxxx is $focusObjectType then get the bean else get the related bean
    	$parseProcessObject = FALSE;
    	$configs = array();
    	foreach(preg_split("/((\r?\n)|(\r\n?))/", $string) as $line){
    		// Check for Sugar Config
    		preg_match_all('/\$sugar_config_([\w\:]+)/i', $line, $matches);
    
    		// Check if it found any matches
    		if(!empty($matches) && !empty($matches[1])){
    			$exclude = array(
    					'dbconfig'
    			);
    			foreach($matches[1] as $match){
    				if(!empty($match)){
    					// Explode for grabbing multi-level values (key:key2:key3)
    					$ex = explode(':', $match);
    					if(!empty($ex)){
    						// Assign Sugar Config to $val
    						$val = $sugar_config;
    						foreach($ex as $key){
    							// If it is a forbidden array (like db) break out of loop
    							if(in_array($key, $exclude)){
    								break;
    							}
    							// Does the key exist
    							if(isset($val[$key])){
    								// Use that key as the value for checking for possible nested values
    								$val = $val[$key];
    								// if it isn't array, break out of the loop
    								if(!is_array($val)) break;
    							}
    						}
    						// Check if isn't a array - if it isn't replace value in line
    						if(!is_array($val)){
    							// Replace val in line
    							$origLine = $line;
    							$string = str_ireplace('$sugar_config_' . $match, $val, $string);
    							//Now replace the original lineinthe string with the new line
    
    						}
    					}
    				}
    			}
    		}
    
    		// Check for config table
    		preg_match_all('/\$config_(\w+)/i', $line, $matches);
    
    		// Check if it found any matches
    		if(!empty($matches) && !empty($matches[1])){
    			foreach($matches[1] as $match){
    				if(!empty($match)){
    					// If we haven't already searched for config items
    					if(empty($configs)){
    						$res = $this->db->query("SELECT * FROM config");
    						$configs = array();
    						while($configs[] = $this->db->fetchByAssoc($res));
    					}
    					foreach($configs as $conf){
    						// Replace val in configs
    						$origLine = $line;
    						$string = str_ireplace('$config_' . $conf['category'] . '_' . $conf['name'], $conf['value'], $string);
    						 
    					}
    				}
    			}
    		}
        //Break out each line separated by the space
    		$piecesofline = explode(" ", $line);
    		foreach($piecesofline as $key=>$value){
    			$posofvar = strpos($value, "$");
    			$posofvar++;
    			if ($posofvar === false) {
    				continue;
    			} 
    				else{
              //Move through the $value starting at one past the $ and move all the down until you 
              //find a character that is not alphanumeric
              //Get the length of $value
              $totallen = strlen($value);
              for($x = $posofvar; $x < $totallen; $x++){
              	$charToCheck = substr($value,$x,1);
              	if(!ctype_alnum($charToCheck) && strcmp($charToCheck, "_") !== 0 ){
              		break;
              	}
              }
                //We have the length of the bean and field                 
                $finalOject = substr($value,$posofvar, $x - $posofvar);
    						//Using explode we will check for all possible combos
    						$piecesOfBean = explode("_", $finalOject);
    						$counter = 0;
    						foreach($piecesOfBean as $key1=>$value1){
    							if($counter == 0){
    								$beanToCheck = $value1;
    							}
    							else{
    								$beanToCheck = $beanToCheck ."_" .$value1;
    							}
    							$checkBean = $processManagerEngine1->convertTableToBean($beanToCheck);
    							if(!empty($checkBean)){
    								$finalOject = $beanToCheck;
    								break;
    							}
    							$counter++;
    						}
    					if(($checkBean->module_dir == 'Cases') && ($finalOject == 'aCase')){
    						$finalOject = "cases";
    					}	
    					if($finalOject == $focusObjectType){
    						//Replace the focus object - call the function
    						$string = $this->parseTemplateWithBean($string,$focusObjectType,$focusObjectId);
    					}
    					else{
    						//Convert the original focus object to bean
    						//The final object is not the focus object but a related one
    						//We have our link name - now go and get the related object
    						$relatedBean = $processManagerEngine1->convertTableToBean($finalOject);
						    if(empty($relatedBean)){
								  return $string;
							  }      						
    						$relatedObjectName = $relatedBean->object_name;
    						$focusBean = $processManagerEngine1->convertTableToBean($focusObjectType);
						    if(empty($focusBean)){
								  return $string;
							  }
                //$focusBean = BeanFactory::retrieveBean($focusBean->module_name, $focusObjectId, array('disable_row_level_security' => true));
    						$focusBean->retrieve($focusObjectId);
    						foreach($focusBean->field_defs as $key => $def) {
    							if (isset($def['type'])) {
    								$type=$def['type'];
    								if($type == 'link'){
    									$linkName = $def['name'];
    									//Get the module if it is set
    									if(isset($def['module'])){
    										$module = $def['module'];
    									}
    									elseif($finalOject == $linkName){
    										$module = $relatedBean->module_name;
    									}
    									 
    									if(!is_null($module)){
    										$bean = BeanFactory::getBean($module);
    										$relatedtableName = $bean->table_name;
    										if($linkName != 'teams'){
    											if(($relatedtableName == $dbTable) || ($relatedtableName == $finalOject)){
    												$relatedBeans = $focusBean->get_linked_beans($linkName,$relatedObjectName);
    												foreach($relatedBeans as $relatedBean){
    													$relatedID = $relatedBean->id;
    													$relatedtableName = $relatedBean->table_name;
    													if(($relatedtableName == $finalOject) && ($relatedID != $dbTableID) && ($relatedtableName != $dbTable)) {
    														$string = $this->parseTemplateWithBean($string,$finalOject,$relatedID);
    														break 2;
    													}
    													if(($relatedID == $dbTableID) && ($relatedtableName == $dbTable) && (!$parseProcessObject)) {
    														$string = $this->parseTemplateWithBean($string,$finalOject,$relatedID);
    														$parseProcessObject = TRUE;
    														break 2;
    													}
    													elseif(($relatedID != $dbTableID) && ($relatedtableName == $dbTable)){
    														next;
    													}
    												}
    											}
    										}
    									}
    								}
    							}
    						}
    					}
    			}
    		}
    	}
    	return $string;
    }
function parseTemplateHref($string,$focusObjectType,$focusObjectId,$dbTable = "", $dbTableID = ""){
    require_once('modules/PM_ProcessManager/ProcessManagerEngine1.php');
    $processManagerEngine1 = new ProcessManagerEngine1();
    global $bwcModules;
    global $sugar_config;
    $link = $sugar_config['site_url'];
    $focusBean = $processManagerEngine1->convertTableToBean($focusObjectType);
    //$focusBean = BeanFactory::retrieveBean($focusBean->module_name, $focusObjectId, array('disable_row_level_security' => true));
    $focusBean->retrieve($focusObjectId);
    if (in_array($focusBean->module_name, $bwcModules)) {
        $link .= "/#bwc/index.php?module={$focusBean->module_dir}&action=DetailView&record={$focusObjectId}";
    } else {
        $link .= '/#' .$focusBean->module_dir .= "/$focusObjectId";
    }
    //Now replace the ::href_link:: with the link
    if (!empty($focusBean->name)) {
        $label = $focusBean->name;
    } else {
        $label = translate('LBL_EMAIL_LINK_RECORD', $focusBean->module_dir);
    }
    $link =  '<a href="' . $link . '">' . $label . '</a>';
    //Now do the replace find the position of {::href_link
    $pos1 = stripos($string, "{::href_link");
    $pos2 = stripos($string, "}",$pos1);
    $pos2++;
    $len = $pos2 - $pos1;
    $orig = substr($string,$pos1,$len);
    return str_ireplace($orig,$link,$string);
}

//Custom SierraCRM
function parseTemplate($string,$focusObjectType,$focusObjectId,$dbTable = "", $dbTableID = ""){
    global $sugar_config;
	require_once('modules/PM_ProcessManager/ProcessManagerEngine1.php');
	$processManagerEngine1 = new ProcessManagerEngine1();
	//For each row of the email template - see if there is a $ in the row
	//If so - then get the position of the $ and from there find the next space
	//Get this value see if there is an _
	//If so we have either a focus object or a related object
	//If the value of $xxxxxxx is $focusObjectType then get the bean else get the related bean
	$configs = array();
    // Check for Sugar Config
    preg_match_all('/\\W\\$sugar_config_([\\w\\:]+)\W/i', $string, $matches);

    // Check if it found any matches
    if(!empty($matches) && !empty($matches[1])){
        $exclude = array(
            'dbconfig'
        );
        foreach($matches[1] as $match){
            if(!empty($match)){
                // Explode for grabbing multi-level values (key:key2:key3)
                $ex = explode(':', $match);
                if(!empty($ex)){
                    // Assign Sugar Config to $val
                    $val = $sugar_config;
                    foreach($ex as $key){
                        // If it is a forbidden array (like db) break out of loop
                        if(in_array($key, $exclude)){
                            break;
                        }
                        // Does the key exist
                        if(isset($val[$key])){
                            // Use that key as the value for checking for possible nested values
                            $val = $val[$key];
                            // if it isn't array, break out of the loop
                            if(!is_array($val)) break;
                        }
                    }
                    // Check if isn't a array - if it isn't replace value in line
                    if(!is_array($val)){
                        // Replace val in line
                        $string = str_ireplace('$sugar_config_' . $match, $val, $string);
                        //Now replace the original lineinthe string with the new line

                    }
                }
            }
        }
    }

    // Check for config table
    preg_match_all('/\\W\\$config_(\\w+)\\W/i', $string, $matches);

    // Check if it found any matches
    if(!empty($matches) && !empty($matches[1])){
        foreach($matches[1] as $match){
            if(!empty($match)){
                // If we haven't already searched for config items
                if(empty($configs)){
                    $res = $this->db->query("SELECT * FROM config");
                    $configs = array();
                    while($configs[] = $this->db->fetchByAssoc($res));
                }
                foreach($configs as $conf){
                    // Replace val in configs
                    $string = str_ireplace('$config_' . $conf['category'] . '_' . $conf['name'], $conf['value'], $string);

                }
            }
        }
    }

    preg_match_all("/\\W\\$(\\w+)\\W/i", $string, $matches);
    $x = array('teams');
    if(!empty($matches) && !empty($matches[1])){
        foreach($matches[1] as $match){
            if(!empty($match)){

                $parseProcessObject = false;
                $words = explode('_', $match);

                $focusBean = $processManagerEngine1->convertTableToBean($focusObjectType);
						    if(empty($focusBean)){
								  return;
							  }
                //$focusBean = BeanFactory::retrieveBean($focusBean->module_name, $focusObjectId, array('disable_row_level_security' => true));
                $focusBean->retrieve($focusObjectId);
                $word = '';
                $field = '';
                $bean = false;
                $found = false;
                foreach($words as $i => $w){
                    $word .= $w;

                    if($this->isModule($word)){
                        if(isset($words[$i + 1])){
                            $field = implode('_', array_slice($words, $i + 1, count($words)));
                        }
                        $found = true;
                        $bean = $processManagerEngine1->convertTableToBean($word);
                        if(empty($bean)){
							return;
						}
                        if($word == $focusObjectType){
                            //$bean = BeanFactory::retrieveBean($bean->module_name, $focusObjectId, array('disable_row_level_security' => true));
                            $bean->retrieve($focusObjectId);
                        }else{
                            $relatedObjectName = $bean->object_name;

                            foreach($focusBean->field_defs as $def) {
                                if (isset($def['type']) && $def['type'] == 'link') {

                                    $linkName = $def['name'];
                                    //Get the module if it is set
                                    if(isset($def['module'])){
                                        $module = $def['module'];
                                    }else if($word == $linkName){
                                        $module = $bean->module_name;
                                    }

                                    if(empty($module)) continue;

                                    $bean = BeanFactory::getBean($module);
                                    $relatedTableName = $bean->table_name;
                                    if(!in_array($word, $x) && ($relatedTableName == $dbTable) || ($relatedTableName == $word)){
                                        $relatedBeans = $focusBean->get_linked_beans($linkName,$relatedObjectName);
                                        foreach($relatedBeans as $relatedBean){
                                            $relatedID = $relatedBean->id;
                                            $relatedTableName = $relatedBean->table_name;
                                            if($relatedTableName == $word && $relatedID != $dbTableID && $relatedTableName != $dbTable) {
                                                $bean = $relatedBean;
                                                break 2;
                                            }else if($relatedID == $dbTableID && $relatedTableName == $dbTable && !$parseProcessObject) {
                                                $bean = $relatedBean;
                                                break 2;
                                            }else if($relatedID != $dbTableID && $relatedTableName == $dbTable){
                                                continue;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    }else if($this->isRelationship($word)){
                        $found = true;
                        $bean = $this->getFromRel($word);
                        $word = $bean->table_name;
                        break;
                    }
                }
				//Is this an audit field?
				//Check for Audit Table
				$pos = strpos($field, "_audit_");
				if($pos !== false){
                    $auditValue = $this->parseAuditTable($bean,$field, $pos);
                    if(!empty($auditValue)){
                        $bean->{$field} = $auditValue;
                    }
					}
					if ($found && !empty($bean->id)) {
						//Check if currency field
						if ($bean->field_name_map[$field]['type'] == 'currency') {
							require_once('modules/Currencies/Currency.php');
							$value = currency_format_number($bean->{$field});
							$bean->{$field} = $value;
						}
                        if($bean->field_name_map[$field]['type'] == 'enum'){
                            $translated = translate($bean->field_defs[$field]['options'],$bean->module_name,$bean->{$field});
                            $bean->{$field} = $translated;
                        }
						//Support for the Created By, Assigned User Id and Last Modified Field
                        if($field == 'created_by' || $field == 'assigned_user_id' || $field == 'modified_user_id'){
                            $uid = $bean->{$field};
                            $tuser = new User();
                            $tuser->retrieve($uid);
                            $value = $tuser->user_name;
                            $string = str_ireplace('$' . $word . '_' . $field, $value, $string);
                        }else{
                            $string = str_ireplace('$' . $word . '_' . $field, $bean->{$field}, $string);
                        }

					}

            }
        }
    }

	return $string;
}
	function parseAuditTable($bean,$field, $pos){
		//Get the Field
		$fld = substr($field,0,$pos);
		$focusObjectType = $bean->table_name ."_audit";
		$queryAuditTable = "select * from $focusObjectType where parent_id = '$bean->id' and field_name = '$fld' order by date_created DESC";
		$resultAuditTable = $this->db->query($queryAuditTable, true);
		$rowAuditTable = $this->db->fetchByAssoc($resultAuditTable);
		//If Before Value String blank - then return NULL
		//Are we before or after
		$ba = substr($field,$pos + 7);
		//Text or String
		if($bean->field_name_map[$fld]['type'] == 'text'){
			if($ba == 'before'){
				return $rowAuditTable['before_value_text'];
			}
			else{
				return $rowAuditTable['after_value_text'];
			}
		}
		else{
			if($ba == 'before'){
				return $rowAuditTable['before_value_string'];
			}
			else{
				return $rowAuditTable['after_value_string'];
			}
		}
	}

function parseTemplateWithBean($string,$focusObjectType,$focusObjectId){
	$processManagerEngine1 = new ProcessManagerEngine1();
	if($focusObjectType == 'meetings'){
		require_once('modules/Meetings/Meeting.php');
		$newBean = new Meeting();
	}
	else{
		$newBean = $processManagerEngine1->convertTableToBean($focusObjectType);
	}
  if(empty($newBean)){
		return;
	}
    //$newBean = BeanFactory::retrieveBean($newBean->module_name, $focusObjectId, array('disable_row_level_security' => true));
    $result = $newBean->retrieve($focusObjectId);
	$focusObjectType1 = $focusObjectType;

	foreach($newBean->field_defs as $field_def) {
		if(($field_def['type'] == 'relate' && empty($field_def['custom_type'])) || $field_def['type'] == 'assigned_user_name' || $field_def['type'] == 'link') {
			continue;
		}

		if($field_def['type'] == 'enum') {
			$translated = translate($field_def['options'], 'Accounts' ,$contact->$field_def['name']);

			if(isset($translated) && ! is_array($translated)) {
				$repl_arr["\\$focusObjectType_".$field_def['name']] = $translated;

			} else { // unset enum field, make sure we have a match string to replace with ""
				$repl_arr["\\$focusObjectType_".$field_def['name']] = '';

			}
		} else {
			if (isset($newBean->$field_def['name'])) {
				$repl_arr["\\$focusObjectType_".$field_def['name']] = $newBean->$field_def['name'];

			} // if
		}
		$repl_arr["\\$focusObjectType_".$field_def['name']] = $newBean->$field_def['name'];
	}

	foreach ($repl_arr as $name=>$value) {
		if ($name != 'id') {
			$name = substr($name,1);
			$name = "\\$$focusObjectType1" ."_" ."$name";
			$name = substr($name,1);
			if($value != '') {
				$pos = strpos($string,$name);
				if($pos !== false) {
					$string = str_replace("$name", $value, $string);
				}

			} else {
				$string = str_replace("$name", ' ', $string);
			}
		}
	}
	return $string;
}

	function stripAsciiCharacters($query_string){

		$query_string = str_replace("&quot",'"',$query_string);
	    $query_string = str_replace(";","",$query_string);
		$query_string = str_replace("&#039","'",$query_string);
		$query_string = str_replace(";","",$query_string);
		$query_string = str_replace("&gt",">",$query_string);
		$query_string = str_replace("&lt","<",$query_string);
		return $query_string;

	}
	
	function sendSugarNotifications($focusObjectType,$focusObjectId,$rowTask){
		$thisNotification = new Notifications();
		$get_defs = "SELECT * FROM pm_process_task_notification_defs WHERE task_id = '{$rowTask['id']}'";
		$get_defs_res = $this->db->query($get_defs);
		if($def = $this->db->fetchByAssoc($get_defs_res)){
			$user = $def['assigned_user_id_notification'];
			if(!empty($user)){
				$queryUserTable = "select id from users where user_name = '$user' and deleted = 0";
				$resultUserTable = $this->db->query($queryUserTable,true);
				$rowUserTable = $this->db->fetchByAssoc($resultUserTable);
				$value = $rowUserTable['id'];
			}else{
				$processManagerEngine = new ProcessManagerEngine();
				$rowUser = $processManagerEngine->getFocusOwner($focusObjectType,$focusObjectId);
				$value = $rowUser['id'];
			}
			$processManagerEngine1 = new ProcessManagerEngine1();
			$focusBean = $processManagerEngine1->convertTableToBean($focusObjectType);
  		if(empty($focusBean)){
				return;
			}			
			$module = $focusBean->module_dir;
			$thisNotification->name = $def['notification_name'];
			$thisNotification->description = $def['notification_description'];
			$thisNotification->assigned_user_id = $value;
			$thisNotification->severity = $def['severity_notification'];
			$thisNotification->parent_type = $module;
			$thisNotification->parent_id = $focusObjectId;
			$thisNotification->save();
		}
	}

    function runRestService($focusObjectType,$focusObjectId,$rowTask){


        $get_defs = "SELECT * FROM pm_process_task_rest_defs WHERE task_id = '{$rowTask['id']}'";

        $get_defs_res = $this->db->query($get_defs);

        if($def = $this->db->fetchByAssoc($get_defs_res)){
            $url = $def['rest_url'];
            $method = !empty($def['rest_method']) ? $def['rest_method'] : 'GET';

            // Has Required Fields
            if(empty($url) && !empty($method)){
                return;
            }

            $headers = array();

            $data_array = array();

            if(function_exists('json_decode')){
                if(!empty($def['rest_json'])){
                	$def['rest_json'] = str_replace('\"', '"', str_replace("\'", "'", html_entity_decode($def['rest_json'], ENT_QUOTES)));
                	
                	$def['rest_json'] = $this->parseTemplate($def['rest_json'], $focusObjectType, $focusObjectId);
                	
                    $data_array = json_decode($def['rest_json'], true);
                }

                if(!empty($def['rest_headers'])){
                	$def['rest_headers'] = str_replace('\"', '"', str_replace("\'", "'", html_entity_decode($def['rest_headers'], ENT_QUOTES)));
                	
                	$def['rest_headers'] = $this->parseTemplate($def['rest_headers'], $focusObjectType, $focusObjectId); 
                	
                    $headers = json_decode($def['rest_headers'], true);
                }
            }

            $data_string = '';

            if($def['rest_http_query'] == '1' || $method == 'GET'){
                $data_string = http_build_query($data_array);
            }else if(function_exists('json_encode')){
                $data_string = json_encode($data_array);
            }

            if($method == 'GET'){
                if(preg_match("/\\w+\\?\\w+/u", $url)){
                    $url .= "&";
                }else{
                    $url .= "?";
                }
                $url .= $data_string;
            }

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            
            if($method != 'GET' && $method != 'DELETE'){
            	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            }
            
            if($method == 'POST') {
                curl_setopt($ch, CURLOPT_POST, true);
            }
            if($method == 'PUT' || $method == 'POST'){
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            }
            curl_setopt($ch, CURLOPT_HEADER, true);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            if(($method != 'GET' && $method != 'DELETE' && !empty($data_string)) || !empty($headers)){
                curl_setopt($ch, CURLOPT_HTTPHEADER, array_merge(array(
                    'Content-Type: application/json'
                ), $headers));
            }

            $output = curl_exec($ch);
            
            if($output == false){
            	$error = curl_error($ch);
            }

            $custom_script = $def['rest_custom_script'];

            $path = "modules/PM_ProcessManager/customScripts/$custom_script";

            if(!empty($custom_script) && file_exists($path)){
                require_once($path);
                $fn = str_replace(".php", "", $custom_script);

                $customScript = new $fn($focusObjectId, $focusObjectType, $output);

            }
        }




    }
	
function convertLead($focusObjectType,$focusObjectId,$rowTask){
	if(strtolower($focusObjectType) != 'leads') return;
	$get_defs = "SELECT * FROM pm_process_task_convert_lead_defs WHERE task_id = '{$rowTask['id']}'";
	$get_defs_res = $this->db->query($get_defs);
	if($def = $this->db->fetchByAssoc($get_defs_res)){
		require_once('modules/Leads/Lead.php');	
		$lead = new Lead();
    //$lead = BeanFactory::retrieveBean($lead->module_name, $focusObjectId, array('disable_row_level_security' => true));
		$lead->retrieve($focusObjectId);
		if($lead->converted == '1'){
			return;
		}else{
			$lead->converted = '1';
			$lead->status = 'Converted';
		}
		//First Create the Contact
		if($def['create_contact'] == '1' && empty($lead->contact_id)){
			require_once('modules/Contacts/Contact.php');
			$con = new Contact();
			$transfer_fields = array(
				'first_name',
				'last_name',
				'description',
				'title',
				'department',
				'do_not_call',
				'salutation',
				'phone_home',
				'phone_mobile',
				'phone_work',
				'phone_other',
				'phone_fax',
				'primary_address_street',
				'primary_address_city',
				'primary_address_state',
				'primary_address_postalcode',
				'primary_address_country',
				'alt_address_street',
				'alt_address_city',
				'alt_address_state',
				'alt_address_postalcode',
				'alt_address_country',
				'assistant',
				'assistant_phone',
				'lead_source',
				'birthdate',
				'email1',
			);
			foreach($transfer_fields as $field){
				if(property_exists($con, $field) && property_exists($lead, $field)){
					$con->{$field} = $lead->{$field};
				}
			}
			$con->save();
			$lead->contact_id = $con->id;
		}		
		if($def['create_account'] == '1' && empty($lead->account_id)){
			require_once('modules/Accounts/Account.php');
			$acc = new Account();
			$acc->name = (!empty($lead->account_name)) ? $lead->account_name : $lead->last_name . ', ' . $lead->first_name;
			$acc->description = $lead->account_description;
			$acc->phone_office = $lead->phone_work;
			$acc->phone_fax = $lead->phone_fax;
			$acc->billing_address_street = $lead->primary_address_street;
			$acc->billing_address_city = $lead->primary_address_city;
			$acc->billing_address_state = $lead->primary_address_state;
			$acc->billing_address_postalcode = $lead->primary_address_postalcode;
			$acc->billing_address_country = $lead->primary_address_country;
			$acc->shipping_address_street = $lead->alt_address_street;
			$acc->shipping_address_city = $lead->alt_address_city;
			$acc->shipping_address_state = $lead->alt_address_state;
			$acc->shipping_address_postalcode = $lead->alt_address_postalcode;
			$acc->shipping_address_country = $lead->alt_address_country;
			$acc->website = $lead->website;
			$acc->assigned_user_id = $lead->assigned_user_id;
			$acc->email1 = $lead->email1;
			if(isset($acc->team_id)){
				$acc->team_id = $lead->team_id;	
				$acc->team_set_id = $lead->team_set_id;	
			}
			$acc->save();
			$lead->account_id = $acc->id;
			//Now Set the relationships
			//Contact to Account
			$contactAccount = array('contact_id' => $con->id, 'account_id' => $acc->id);
    	$lead->set_relationship('accounts_contacts', $contactAccount);				
		}

		if($def['create_opportunity'] == '1' && empty($lead->opportunity_id)){
			require_once('modules/Opportunities/Opportunity.php');
			$opp = new Opportunity();
			$opp->name = (!empty($def['create_opportunity_name'])) ? $def['create_opportunity_name'] : $lead->opportunity_name;
			$opp->sales_stage = (!empty($def['create_opportunity_sales_stage'])) ? $def['create_opportunity_sales_stage'] : '';
			$opp->amount = $lead->opportunity_amount;
			$opp->amount_usdollar = $lead->opportunity_amount;
			$opp->date_closed = (!empty($def['create_opportunity_close_date'])) ? date('Y-m-d', strtotime($def['create_opportunity_close_date'])) : date('Y-m-d');
			$opp->save();
			$lead->opportunity_id = $opp->id;
			//Now Set the relationships
			//Contact to Account
			$contactOpp = array('contact_id' => $con->id, 'opportunity_id' => $opp->id, 'contact_role' => 'Primary Decision Maker');
    	$lead->set_relationship('opportunities_contacts', $contactOpp);
			//Contact to Account
			$accountOpp = array('account_id' => $acc->id, 'opportunity_id' => $opp->id);
    	$lead->set_relationship('accounts_opportunities', $accountOpp);    				
		}

			
		//Contact to Opportunity
		//Account to Opportunity
		
		$lead->save();
	}
}

function routeObject($rowProcessDefs, $focusObjectId, $focusObjectType) {
if(!function_exists('get_dbtype')){
    function get_dbtype(){
        global $sugar_config;
        return $sugar_config['dbconfig']['db_type'];
    }
}

if(!function_exists('show_tablequery')){
    function show_tablequery($table){
        switch(get_dbtype()){
        case 'ibm_db2':
            $query = "SELECT * FROM sysibm.systables
                        WHERE name LIKE '".strtoupper($table)."' 
                        AND type = 'T'";
            break;
        case 'mssql':
        $query = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE '{$table}'";
        break;    
        case 'mysql':
        default:
            $query = "SHOW TABLES LIKE '{$table}'";
            break;
        }
        return $query;
    }
}	
		global $beanFiles;
		require_once('modules/PM_ProcessManager/ProcessManagerEngine1.php');
		$processManagerEngine1 = new ProcessManagerEngine1();
		$tableName = $focusObjectType;
	    $update_or_insert = 'modify';
		$routingType = $rowProcessDefs['routing_type'];
		$routeToUser = $rowProcessDefs['route_to_user'];
		$routeToRole = $rowProcessDefs['route_to_role'];
		$routeToSecurityGroups = $rowProcessDefs['route_to_securitygroups'];
		$checkLoginStatus = $rowProcessDefs['check_login_status'];
		$findLoggedInUser = $rowProcessDefs['find_logged_in_user'];
		$process_id = $rowProcessDefs['process_id'];
		$routeToTeam = $rowProcessDefs['route_to_team'];
		$newBean = $processManagerEngine1->convertTableToBean($focusObjectType);
  		if(empty($newBean)){
				return;
			}
    //$newBean = BeanFactory::retrieveBean($newBean->module_name, $focusObjectId, array('disable_row_level_security' => true));
		$newBean->retrieve($focusObjectId);
        $GLOBALS['log']->info("ProcessManager - Routing Object with id $focusObjectId and routing type $routingType");
		if ($routingType == "User") {
			//Go ahead and route the object - first check to see if the check login status flag is set
			//if it is then make sure the user is logged in - otherwise send to the manager
			$userId = $this->getUserIdByName($routeToUser);
			$isLoggedIn = true;
			if ($checkLoginStatus == 1) {
				$isLoggedIn = $this->isUserLoggedIn($userId);
			}
			if ($isLoggedIn) {
				//Instantiate the bean
				$newBean->assigned_user_id = $userId;
				$newBean->save(true);
			}
		}
    elseif($routingType == "Reports To"){
        //Get how this person reports to - getFocusOwner
        $rowUser = $this->getFocusOwner($focusObjectType,$focusObjectId);
        if(!empty($rowUser['reports_to_id'])){
            $newBean->assigned_user_id = $rowUser['reports_to_id'];
            $newBean->save(true);
        }
    }
		elseif($routingType == "Assign To Security Group"){
			//Set the object to the Security Group
			$processManagerEngine1 = new ProcessManagerEngine1();
			$focusBean = $processManagerEngine1->convertTableToBean($focusObjectType);
			if(empty($focusBean)){
				return;
			}
			require_once('modules/SecurityGroups/SecurityGroup.php');
			$thisSecGrp = new SecurityGroup();
			$deleteFromSecGrpRec = "delete from securitygroups_records where record_id = '$focusObjectId'";
			$rsltSecGrpRec = $focusBean->db->query($deleteFromSecGrpRec);
			//Get the Security Group ID
			$querySecGrp = "select id from securitygroups where name = '$routeToSecurityGroups' and deleted = 0";
			$rsltSecGrp = $focusBean->db->query($querySecGrp);
			$rowSecGrp = $focusBean->db->fetchByAssoc($rsltSecGrp);
			$secGrpId = $rowSecGrp['id'];
			$thisSecGrp->addGroupToRecord($focusBean->module_dir, $focusObjectId, $secGrpId);		
		}
    else{
			//Go ahead and route the object to the Role
			$arrayRoundRobinTracking = $this->getRoundRobinUser($routeToRole,$process_id,$process_id,$tableName,$update_or_insert);
			$userId = $arrayRoundRobinTracking[0];
			$roleId = $arrayRoundRobinTracking[1];
			 $GLOBALS['log']->info("ProcessManager - Routing Object with id $focusObjectId and routing type $routingType to the user with id $userId and role id of $roleId");
			$isLoggedIn = true;
			if ($checkLoginStatus == 1) {
				$isLoggedIn = $this->isUserLoggedIn($userId);
				//If the User is Not Logged In, check to see if the rule want us to find the next logged in user
				if (($isLoggedIn == false) && ($findLoggedInUser == 1)) {
					$arrayRoundRobinTracking = $this->getRoundRobinUserLoggedIn($routeToRole,$focusObjectId,$process_id,$tableName,$update_or_insert);
					//Now see if anybody in the role is logged in
					$userId = $arrayRoundRobinTracking[0];
					if ($userId != '') {
						$isLoggedIn = true;
					}
				}
			}
			if ($isLoggedIn) {
				$GLOBALS['log']->info("ProcessManager - Routing Object with id $focusObjectId and routing type $routingType to the user with id $userId and role id of $roleId and this user is logged in");
				$newBean->assigned_user_id = $userId;
				$newBean->save(true);
				$this->updateUserInRoundRobinTracking($userId,$roleId,$process_id,$process_id,$tableName,'update');
			}
			//Now add the object to the routed object table
			$this->setRoutedObject($rowProcessDefs['process_id'],$focusObjectId,$tableName,$update_or_insert);
		}
        //Assign the Object to the Team
        if(!empty($routeToTeam)){
            //Get the Team by Name
             $teamid = array();
            $queryTeam = "select id from teams where name = '$routeToTeam' and deleted = 0";
            $rsltTeam = $newBean->db->query($queryTeam);
            $rowTeam = $newBean->db->fetchByAssoc($rsltTeam);
            $team_id = $rowTeam['id'];
            require_once ('modules/Teams/TeamSet.php');
            $teamSetBean = new TeamSet();
            $teams = $teamSetBean->getTeams($newBean->team_set_id);
            //$team_id = array_keys($teams);
            $newBean->load_relationship('teams');
            $newBean->teams->add($team_id);
            $newBean->save(false);
        }
}	

//****************************************************************************
//This function will update the rm_round_robin_tracking table and set
//the date time for the user to current time.
//****************************************************************************
function updateUserInRoundRobinTracking($userID,$roleID,$routingID,$recordID,$tableName,$update_or_insert){
	$dateTimeNow = gmdate('Y-m-d H:i:s');
	$GLOBALS['log']->info("ProcessManager - Updateing Round Robin Tracking - setting user with id $userID and process id $recordID at this time $dateTimeNow");
	$query = "update pm_round_robin_tracking set date_last_object_routed = '$dateTimeNow' ";
	$query .= " where user_id = '$userID'and role_id = '" .$roleID."' and process_id = '" .$routingID ."'";
	$this->db->query($query);
}

//This function will insert a record into the routed object table
//the $id is the id of the routing record and the $recordID is the id
//of the bean.
function setRoutedObject($id,$recordID,$tableName,$update_or_insert){
	$entryTableId = create_guid();
	global $sugar_config;
	$db_type = $sugar_config['dbconfig']['db_type'];
	if($db_type == 'mysql'){
		$query = "INSERT into pm_round_robin_tracking (`id`,`process_id`,`object_id`,`object_type`,`object_event`) VALUES ('$entryTableId','$id','$recordID','$tableName','$update_or_insert')";
	}
	if($db_type == 'mssql'){
		$query = "INSERT into pm_round_robin_tracking (id,process_id,object_id,object_type,object_event) "
							. " VALUES ('$entryTableId', '$id','$recordID','$tableName','$update_or_insert')";	
	}	
	$result = $this->db->query($query);
}


function isUserLoggedIn($userID){
	//Get the session save path
	require_once ('config.php'); // provides $sugar_config
	global $sugar_config;
	$session_dir = $sugar_config['session_dir'];
	$version = $sugar_config['sugar_version'];
	$pos = strpos($version,'7.');
	
	if ($session_dir == '') {
		$session_dir = ini_get("session.save_path");
	}
	//Now go get the newest session id from the tracker table
	if($pos !== false)
		$queryTrackerTable = "select session_id from tracker_sessions where user_id = '$userID' and DATE_FORMAT(date_start, '%m-%d') = RIGHT(CURDATE(),5) order by date_start DESC";
	else
		$queryTrackerTable = "select session_id from tracker where user_id = '$userID' and DATE_FORMAT(date_modified, '%m-%d') = RIGHT(CURDATE(),5) order by date_modified DESC";
	$resultTrackerTable = $this->db->query($queryTrackerTable, true);
	$rowTrackerTable = $this->db->fetchByAssoc($resultTrackerTable);
	$session_id = $rowTrackerTable['session_id'];
	$session = $session_dir ."/sess_$session_id";
	if (file_exists($session)) {
		return true;
	}
	else{
		return false;
	}

}

//*****************************************************************
//This is the function that will figure out the next available
//user in a round robin routing
//*****************************************************************

function getRoundRobinUser($routeToRole,$routingId,$recordID,$tableName,$update_or_insert){
	//Get the Role Id
	$arrayRoundRobinTracking = array();
	$queryGetRoleId = "Select id from acl_roles where name = '$routeToRole' and deleted = 0";
	$resultGetRoleId = $this->db->query($queryGetRoleId, true);
	$rowGetRoleId = $this->db->fetchByAssoc($resultGetRoleId);
	$roleId = 	$rowGetRoleId['id'];
	//First get all the users for the given role
	$queryGetUsersByRole = "Select user_id from acl_roles_users where role_id = '$roleId' and deleted = 0";
	$resultGetUsersByRole = $this->db->query($queryGetUsersByRole, true);
	//Now go and get all the users in the rm_round_robin_tracking table
	//We will end up with an array of user ids
	$queryRoundRobinTracking = "Select user_id from pm_round_robin_tracking where role_id = '$roleId' and process_id = '$routingId' order by date_last_object_routed ASC";
	$resultRoundRobinTracking = $this->db->query($queryRoundRobinTracking, true);
	$reload = FALSE;
	//If the user has been removed from the role - then remove from round robing tracking
	while($rowRoundRobinT= $this->db->fetchByAssoc($resultRoundRobinTracking)){
		$user_id = $rowRoundRobinT['user_id'];
		$query = "select id from acl_roles_users where user_id = '$user_id' and role_id = '$roleId' and deleted = 0";
		$result = $this->db->query($query, true);
		$num_row = $this->db->getRowCount($result);
		if($num_row == 0){
			//Delete the user from pm_round_robin_tracking
			$deleteQuery = "delete from pm_round_robin_tracking where user_id = '$user_id' and role_id = '$roleId'";
			$this->db->query($deleteQuery, true);
			$reload = TRUE;
		}
	}
	$queryRoundRobinTracking = "Select user_id from pm_round_robin_tracking where role_id = '$roleId' and process_id = '$routingId' order by date_last_object_routed ASC";
	$resultRoundRobinTracking = $this->db->query($queryRoundRobinTracking, true);		
	
	//Fill the Array
	$counter = 0;
	while($rowRoundRobinTracking = $this->db->fetchByAssoc($resultRoundRobinTracking)){
		$userID = $rowRoundRobinTracking['user_id'];
		$arrayRoundRobinTracking[$counter] = $userID;
		$counter++;
	}
	//This first while loop makes sure that all the current users in the role are represented in the 
	//round robin tracking table
	$counter = 0;
	while($rowRoleUsers = $this->db->fetchByAssoc($resultGetUsersByRole)){
		//Get the User Id from the result set
		$userIdFromAclRole = $rowRoleUsers['user_id'];
		$arrayRoleUsers[$counter] = $userIdFromAclRole;
		$counter++;
		//See if the User Id is in the array from the rm tracking table - if not then enter the data and quit
		if (!in_array($userIdFromAclRole,$arrayRoundRobinTracking)) {	
				//If the user is not logged in then don't add to the tracking table and go to the next user
				$this->setUserInRoundRobinTracking($userIdFromAclRole,$roleId,$routingId,$recordID,$tableName,$update_or_insert);
				$arrayRoundRobinTracking[0] = $userIdFromAclRole;
				$arrayRoundRobinTracking[1] = $roleId;
				return $arrayRoundRobinTracking;
		}
	}
	//Now make sure that the reverse is not happening
	foreach ($arrayRoundRobinTracking as $key=>$value){
		if (!in_array($value,$arrayRoleUsers)) {
			//If this value is equal to the first entry in the $arrayRoundRobinTracking - then remove
			//unset($arrayRoundRobinTracking[$key]);
			$nextUserInArray = $arrayRoundRobinTracking[$key];
			$arrayRoundRobinTracking[0] = $nextUserInArray;
		}
	}
	//If we have made it to here then all the users are represented in the round robin tracking table
	//So go and get the first record and return the user id - this will be the correct person to get the lead
	$arrayRoundRobinTracking[1] = $roleId;
	//Update the tracking table that we are about to give this user a new object
	//$this->updateUserInRoundRobinTracking($userIdFromAclRole,$roleId,$routingId,$recordID,$tableName,$update_or_insert);
	return $arrayRoundRobinTracking;
}

//*****************************************************************************
//This function will add an entry into the round robin entry table for the user
//******************************************************************************

function setUserInRoundRobinTracking($userID,$roleID,$routingID,$recordID,$tableName,$update_or_insert){
	$entryTableId = create_guid();
	$dateTimeNow = gmdate('Y-m-d H:i:s');
	global $sugar_config;
	$db_type = $sugar_config['dbconfig']['db_type'];
	if($db_type == 'mysql'){
		$query = "Insert into pm_round_robin_tracking (`id`,`user_id`,`role_id`,`process_id`,`object_id`,`object_event`,`object_type`,`date_last_object_routed`) VALUES('$entryTableId','$userID','$roleID','$routingID','$recordID','$update_or_insert','$tableName','$dateTimeNow')";
	}
	if($db_type == 'mssql'){
		$query = "Insert into pm_round_robin_tracking (id,user_id,role_id,process_id,object_id,object_event,object_type,date_last_object_routed) VALUES('$entryTableId','$userID','$roleID','$routingID','$recordID','$update_or_insert','$tableName','$dateTimeNow')";
	}
	$this->db->query($query);
}

//***************************************************************************
//This function is very similiar to the getRoundRobinUser
//What we are doing is finding the first logged in user who is next in line
//to get the object 
//***************************************************************************

function getRoundRobinUserLoggedIn($routeToRole,$recordID,$routingId,$tableName,$update_or_insert){
	$arrayRoundRobinTracking = array();
	$arrayRoundRobinTracking[0] = '';
	$queryGetRoleId = "Select id from acl_roles where name = '$routeToRole'";
	$resultGetRoleId = $this->db->query($queryGetRoleId, true);
	$rowGetRoleId = $this->db->fetchByAssoc($resultGetRoleId);
	$roleId = 	$rowGetRoleId['id'];
	//First get all the users for the given role	
	$queryGetUsersByRole = "Select user_id from acl_roles_users where role_id = '$roleId' and deleted = 0";
	$resultGetUsersByRole = $this->db->query($queryGetUsersByRole, true);
	//Now go and get all the users in the rm_round_robin_tracking table
	//We will end up with an array of user ids
	$queryRoundRobinTracking = "Select user_id from pm_round_robin_tracking where role_id = '$roleId' and process_id = '$routingId' order by date_last_object_routed ASC";
	$resultRoundRobinTracking = $this->db->query($queryRoundRobinTracking, true);
	//Fill the Array
	$counter = 0;
	while($rowRoundRobinTracking = $this->db->fetchByAssoc($resultRoundRobinTracking)){
		$userID = $rowRoundRobinTracking['user_id'];
		//For each user id go find out if the user is logged in 
		$isLoggedIn = $this->isUserLoggedIn($userID);
		if ($isLoggedIn) {
			$arrayRoundRobinTracking[0]= $userID;
			$arrayRoundRobinTracking[1]= $roleId;
			return $arrayRoundRobinTracking;
		}
	}

}

function sendNotifications($check_notify,$bean){
		
			require_once("modules/Administration/Administration.php");
			$admin = new Administration();
			$admin->retrieveSettings();
			$sendNotifications = false;

			if ($admin->settings['notify_on'])
			{
				$GLOBALS['log']->info("Notifications: user assignment has changed, checking if user receives notifications");
				$sendNotifications = true;
			}
			elseif(isset($_REQUEST['send_invites']) && $_REQUEST['send_invites'] == 1)
			{
				// cn: bug 5795 Send Invites failing for Contacts
				$sendNotifications = true;
			}
			else
			{
				$GLOBALS['log']->info("Notifications: not sending e-mail, notify_on is set to OFF");
			}


			if($sendNotifications == true)
			{
				$notify_list = $bean->get_notification_recipients();
				foreach ($notify_list as $notify_user)
				{
					$bean->send_assignment_notifications($notify_user, $admin);
				}
			}
		
	}

//Function checks to see if the field has recently changed
function checkFieldForAnyChange($field,$focusObjectId,$focusObjectType){	
    $GLOBALS['log']->info("Process Manager - checking to see if the field has changed for field named $field and table $focusObjectType and ID $focusObjectId");	
	$focusObjectType .="_audit";
	$queryAuditTable = "select date_created from $focusObjectType where parent_id = '$focusObjectId' and field_name = '$field' order by date_created DESC";
	$resultAuditTable = $this->db->query($queryAuditTable, true);
	$rowAuditTable = $this->db->fetchByAssoc($resultAuditTable);
	$dateCreated = $rowAuditTable['date_created'];
	$today = gmdate('Y-m-d H:i:s'); 
	$valueStrtoTime = strtotime($dateCreated);
	$todayStrtoTime = strtotime($today);
	$deltaTime = $todayStrtoTime - $valueStrtoTime;	
	if ($deltaTime < 600) {
		return true;
	}
	else{
		return false;
	}
	
}

//Function checks to see if the field has recently changed
function checkFieldFromTo($field,$focusObjectId,$focusObjectType,$value,$valueTo){
	$GLOBALS['log']->info("Process Manager - checking to see if the field has changed for field named $field and table $focusObjectType and ID $focusObjectId");
	$focusObjectType .="_audit";
	$queryAuditTable = "select * from $focusObjectType where parent_id = '$focusObjectId' and field_name = '$field' order by date_created DESC";
	$resultAuditTable = $this->db->query($queryAuditTable, true);
	$rowAuditTable = $this->db->fetchByAssoc($resultAuditTable);
	$dateCreated = $rowAuditTable['date_created'];
	$today = gmdate('Y-m-d H:i:s');
	$valueStrtoTime = strtotime($dateCreated);
	$todayStrtoTime = strtotime($today);
	$deltaTime = $todayStrtoTime - $valueStrtoTime;
	$before_value_string = $rowAuditTable['before_value_string'];
	$after_value_string = $rowAuditTable['after_value_string'];
	if ($deltaTime < 600) {
		if(($value == $before_value_string) && ($valueTo == $after_value_string)){
			return true;
		}
		else {
			return false;
		}
	}
	else{
		return false;
	}

}


function updateObjectField($processID,$focusObjectType, $focusObjectId, $rowTask){
	$processManagerEngine1 = new ProcessManagerEngine1();
	$focusBean = $processManagerEngine1->convertTableToBean($focusObjectType);
	if(empty($focusBean)){
				return;
	}
    //$focusBean = BeanFactory::retrieveBean($focusBean->module_name, $focusObjectId, array('disable_row_level_security' => true));
    $focusBean->retrieve($focusObjectId);
	if(empty($focusBean->id)) return false; // If Empty
	$getdefs = "SELECT * FROM pm_process_task_modify_field_defs WHERE task_id = '{$rowTask['id']}'";
	$getdefsres = $this->db->query($getdefs);
	$found = false;
    $teamSet = FALSE;
	while($getdefsrow = $this->db->fetchByAssoc($getdefsres)){
		$found = true;
		$field = $getdefsrow['process_object_modify_field'];
		$method = $getdefsrow['modify_method'];
		if(property_exists($focusBean, $field)){
			if($method == 'by_date'){
				$value = $getdefsrow['process_object_modify_field_by_date'];
				//Set the field if datetime vs date
				foreach($focusBean->field_defs as $def){
					if($def['name'] == $field){
						$type = $def['type'];
						break;
					}
				}
				if($type == 'date'){
					$focusBean->{$field} = gmdate('Y-m-d', strtotime($value));
				}
				else{
					$focusBean->{$field} = gmdate('Y-m-d H:i:s', strtotime($value));
				}	
			}else if($method == 'by_value' || $method == 'append_by_value'){
				if(($field == 'created_by') || ($field == 'modified_user_id') || ($field == 'assigned_user_id')) {
           $value = $getdefsrow['process_object_modify_field_value'];
            if($value != 'Created By' && $value != 'Last Modified By') {
                $queryUserTable = "select id from users where user_name = '$value' and deleted = 0 ";
                $resultUserTable = $this->db->query($queryUserTable, true);
                $rowUserTable = $this->db->fetchByAssoc($resultUserTable);
                $value = $rowUserTable['id'];
            }
            if ($value == 'Created By') {
                $value = $focusBean->created_by;
            } elseif ($value == 'Last Modified By') {
                $value = $focusBean->modified_user_id;
            }
				}elseif($field == 'team_id'){
              $value = $getdefsrow['process_object_modify_field_value'];
              $queryTeamTable = "select id from teams where name = '$value' and deleted = 0";
              $resultTeamTable = $this->db->query($queryTeamTable,true);
              $rowTeamTable = $this->db->fetchByAssoc($resultTeamTable);
              $value = $rowTeamTable['id'];
              $teamSet = TRUE;
              $team_id = $value;
          }
				else{
					$value = $getdefsrow['process_object_modify_field_value'];
				}
				if($method == 'append_by_value'){
					$focusBean->{$field} .= " ";
				    $focusBean->{$field}  .= $value;	
				}else{
    				$focusBean->{$field} = $value;	
				}
				
			}else if(($method == 'from_field' || $method == 'append_by_field') && property_exists($focusBean, $field)){
				$from_field = $getdefsrow['process_object_modify_field_by_field'];
				if($method == 'append_by_field'){
					$focusBean->{$field} .= " ";
				    $focusBean->{$field}  .= $focusBean->{$from_field};
				}else{
    				$focusBean->{$field} = $focusBean->{$from_field};
				}
					
            }else if(($method == 'from_related' || $method == 'append_from_related') && property_exists($focusBean, $field)){
                $from_field = $getdefsrow['modify_field_by_related_field'];
                $relatedObject = $getdefsrow['modify_field_by_related_module'];
                $relateField = FALSE;
                //Relate Field or Related Module?
                foreach($focusBean->field_defs as $key => $def) {
                	if(($def['type'] == 'relate') && ($def['name'] == $relatedObject)){
                		$relateField = TRUE;
                		foreach($def as $key2 => $value){
                			if ($key2 == 'id_name'){
                				$relatedID =  $focusBean->{$value};
                			}
                			if ($key2 == 'module'){
                				$relatedModule = strtolower($value);
                			}
                		}                		
                	}
                }
                if($relateField){
                	$relatedBean = $processManagerEngine1->convertTableToBean($relatedModule);
                	if(empty($relatedBean)){
                		return;
                	}
                  //$relatedBean = BeanFactory::retrieveBean($relatedBean->module_name, $focusObjectId, array('disable_row_level_security' => true));
                	$relatedBean->retrieve($relatedID);
                	if($method == 'append_from_related'){
                		$focusBean->{$field} = $focusBean->{$field} ." " .$relatedBean->{$from_field};
                	}else{
                		$focusBean->{$field} = $relatedBean->{$from_field};
                	}
                }
                else{
	                //Not Relate Field
                foreach($focusBean->field_defs as $key => $def) {
                    if(($def['type'] == 'link') && ($def['name'] == $relatedObject)){
                        foreach($def as $key2 => $value){
                            if ($key2 == 'module'){
                                $relatedObject = strtolower($value);
                            }
                        }
                    }
                }
                $focusBean->load_relationships();
                $linkedFields = $focusBean->get_linked_fields();
                $relatedObject = strtolower($relatedObject);
                $relatedbean = $processManagerEngine1->convertTableToBean($relatedObject);
                if(empty($relatedbean)){
									return;
								}
                foreach ($linkedFields as $link){
                    //On Custom modules the name is left object and right object
                    $linkName = $link['name'];
					if((!empty($linkName)) && ($linkName != 'teams')){
	                    $relationship = $focusBean->get_linked_beans($linkName,$relatedbean->object_name);
	                    $tableName = $relationship[0]->table_name;
	                    //Get the name of the RHS table
	                    if ($tableName == $relatedObject){
	                        //We have the link name and it is rhs
	                        break;
	                    }
                  	}
                }	
                $relatedObjectName = $relatedbean->object_name;
                $relatedBeans = $focusBean->get_linked_beans($linkName,$relatedObjectName,NULL,0,100,0,'');
                if(isset($relatedBeans[0])){
                    if($method == 'append_from_related'){
                    	  $fieldvalue = $focusBean->{$field};
                    	  $fieldvalue = $fieldvalue ." " .$relatedBeans[0]->{$from_field};
                        $focusBean->{$field} = $fieldvalue;
                    }else{
                        $focusBean->{$field} = $relatedBeans[0]->{$from_field};
                    }
                }
            		}
            }	
		}
	}

	if($found){ 
		//Set Logic Hook depth to 11 if Ignore Check for Previously Run Process is Checked
		$qp = "select ignore_process_prev_run_check from pm_process_defs where process_id = '$processID'";
		$resp = $this->db->query($qp);
		$rowp = $this->db->fetchByAssoc($resp);
		if($rowp['ignore_process_prev_run_check'] == 1){
			$focusBean->logicHookDepth['after_save'] = 20;
		}
		$focusBean->save();
        if($teamSet){
            require_once ('modules/Teams/TeamSet.php');
            $teamSetBean = new TeamSet();
            $teams = $teamSetBean->getTeams($focusBean->team_set_id);
            $team_id = array_keys($teams);
            $focusBean->load_relationship('teams');
            $focusBean->teams->add($team_id);
        }
	}
	//DONE 
	return true;
}

    function convertRelatedNameToTable($focusBean, $relatedObject){
        $relateField = FALSE;
        //Relate Field or Related Module?
        foreach($focusBean->field_defs as $key => $def) {
            if(($def['type'] == 'relate') && ($def['name'] == $relatedObject)){
                $relateField = TRUE;
                foreach($def as $key2 => $value){
                    if ($key2 == 'id_name'){
                        $relatedID =  $focusBean->{$value};
                    }
                    if ($key2 == 'module'){
                        $relatedModule = strtolower($value);
                    }
                }
            }
        }
        if(!$relateField){
            foreach($focusBean->field_defs as $key => $def) {
                if(($def['type'] == 'link') && ($def['name'] == $relatedObject)){
                    foreach($def as $key2 => $value){
                        if ($key2 == 'module'){
                            $relatedModule = strtolower($value);
                        }
                    }
                }
            }
        }
        return $relatedModule;
    }

    function updateRelatedObjectFieldsFromRelatedObject($focusObjectType, $focusObjectId, $rowTask){
        //Convert table to bean
        $processManagerEngine1 = new ProcessManagerEngine1();
        $focusBean = $processManagerEngine1->convertTableToBean($focusObjectType);
        if(empty($focusBean)){
            return;
        }
        //$focusBean = BeanFactory::retrieveBean($focusBean->module_name, $focusObjectId, array('disable_row_level_security' => true));
        $focusBean->retrieve($focusObjectId);
        if(empty($focusBean->id)) return false; // If Empty
        $getdefs = "SELECT * FROM pm_process_task_modify_field_defs WHERE task_id = '{$rowTask['id']}'";
        $getdefsres = $this->db->query($getdefs);
        $focusBean->load_relationships();
        $linkedFields = $focusBean->get_linked_fields();
        while($getdefsrow = $this->db->fetchByAssoc($getdefsres)){
            $field = $getdefsrow['process_object_modify_related_field'];
            $method = $getdefsrow['related_modify_method'];
            $relatedObject = $getdefsrow['process_object_modify_related_object'];
            $fromObject = $getdefsrow['process_object_modify_related_object_from_object'];
            $fromField = $getdefsrow['process_object_modify_related_field_from_object'];
            $relatedObject = $this->convertRelatedNameToTable($focusBean,$relatedObject);
            $relatedbean = $processManagerEngine1->convertTableToBean($relatedObject);
            $fromObject = $this->convertRelatedNameToTable($focusBean,$fromObject);
            $frombean = $processManagerEngine1->convertTableToBean($fromObject);
            if(empty($relatedbean)){
                return;
            }
            if(empty($frombean)){
                return;
            }
            foreach ($linkedFields as $link){
                //On Custom modules the name is left object and right object
                $linkName = $link['name'];
                if((!empty($linkName)) && ($linkName != 'teams')) {
                    $relationship = $focusBean->get_linked_beans($linkName, $relatedbean->object_name);
                    if (array_key_exists(0, $relationship)) {
                        $tableName = $relationship[0]->table_name;
                        //Get the name of the RHS table
                        if ($tableName == $relatedObject) {
                            //We have the link name and it is rhs
                            break;
                        }
                    }
                }
            }
            foreach ($linkedFields as $link){
                //On Custom modules the name is left object and right object
                $fromlinkName = $link['name'];
                if((!empty($fromlinkName)) && ($fromlinkName != 'teams')){
                    $relationship = $focusBean->get_linked_beans($fromlinkName,$frombean->object_name);
                    if (array_key_exists(0, $relationship)) {
                        $fromtableName = $relationship[0]->table_name;
                        //Get the name of the RHS table
                        if ($fromtableName == $fromObject) {
                            //We have the link name and it is rhs
                            break;
                        }
                    }
                }
            }
            //Get all the related beans
            $relatedObjectName = $relatedbean->object_name;
            $fromObjectName = $frombean->object_name;
            $relatedBeans = $focusBean->get_linked_beans($linkName,$relatedObjectName);
            $fromBeans = $focusBean->get_linked_beans($fromlinkName,$fromObjectName);
            $fromBeanObject = $fromBeans[0];
            foreach($relatedBeans as $bean){
                if(property_exists($bean, $field)){
                    if($method == 'from_field'){
                        $bean->{$field} = $fromBeanObject->{$fromField};
                        }
                    if(($method ==  'append_by_field')){
                            $bean->{$field} .= " ";
                            $bean->{$field}  .= $fromBeanObject->{$fromField};
                    }
                }
                $bean->save();
            }
        }

        //DONE
        return true;
    }

function updateRelatedObjectFields($focusObjectType, $focusObjectId, $rowTask){
	//Convert table to bean
	$processManagerEngine1 = new ProcessManagerEngine1();
	$focusBean = $processManagerEngine1->convertTableToBean($focusObjectType);
    if(empty($focusBean)){
		return;
	}
  //$focusBean = BeanFactory::retrieveBean($focusBean->module_name, $focusObjectId, array('disable_row_level_security' => true));
	$focusBean->retrieve($focusObjectId);
	if(empty($focusBean->id)) return false; // If Empty
	$getdefs = "SELECT * FROM pm_process_task_modify_field_defs WHERE task_id = '{$rowTask['id']}'";
	$getdefsres = $this->db->query($getdefs);
	$focusBean->load_relationships();
	$linkedFields = $focusBean->get_linked_fields();
	while($getdefsrow = $this->db->fetchByAssoc($getdefsres)){
		$field = $getdefsrow['process_object_modify_related_field'];
		$method = $getdefsrow['related_modify_method'];
		$relatedObject = strtolower($getdefsrow['process_object_modify_related_object']);
        //If there is a dash in the related object - this means there are more than one relationships from process object to related object
        $pos = strpos($relatedObject, '-');
        if ($pos === false) {
            $relatedbean = $processManagerEngine1->convertTableToBean($relatedObject);
        } else {
            $linkName = substr($relatedObject,$pos +1);
            $relatedObject = substr($relatedObject,0,$pos);
            $relatedbean = $processManagerEngine1->convertTableToBean($relatedObject);
        }
    if(empty($relatedbean)){
        $table = $focusBean->field_defs[$relatedObject];
        if(isset($table['module'])){
            $linkedModule = $table['module'];
        }else{
            $linkedModule = $table['link'];
        }
        $linkedModule= strtolower($linkedModule);
        $relatedbean = $processManagerEngine1->convertTableToBean($linkedModule);
	  }		
		if(empty($linkName)) {
            foreach ($linkedFields as $link) {
                //On Custom modules the name is left object and right object
                $linkName = $link['name'];
                if ((!empty($linkName)) && ($linkName != 'teams')) {
                    $relationship = $focusBean->get_linked_beans($linkName, $relatedbean->object_name,NULL,0,100,0,'');
                    if (array_key_exists(0, $relationship)) {
                        $tableName = $relationship[0]->table_name;
                        //Get the name of the RHS table
                        if ($tableName == $relatedObject) {
                            //We have the link name and it is rhs
                            break;
                        }
                    }
                }
            }
        }
		//Get all the related beans
		$relatedObjectName = $relatedbean->object_name;
		$relatedBeans = $focusBean->get_linked_beans($linkName,$relatedObjectName,NULL,0,100,0,'');
		foreach($relatedBeans as $bean){
			if(property_exists($bean, $field)){
				if($method == 'by_date'){
					$value = $getdefsrow['process_object_modify_related_field_by_date'];
					//Set the field if datetime vs date
					foreach($bean->field_defs as $def){
						if($def['name'] == $field){
							$type = $def['type'];
							break;
						}
					}
					if($type == 'date'){
						$bean->{$field} = date('Y-m-d', strtotime($value));
					}
					else{
					$bean->{$field} = date('Y-m-d H:i:s', strtotime($value));	
					}	
				}
				if($method == 'by_value' || $method == 'append_by_value'){
					//Bug fix 6-4-2014 - if field to update is assigned user , modified user or created by - get the actual id
					if(($field == 'created_by') || ($field == 'modified_user_id') || ($field == 'assigned_user_id')){
					$value = $getdefsrow['process_object_modify_related_value'];
						$queryUserTable = "select id from users where user_name = '$value' and deleted = 0 ";
						$resultUserTable = $this->db->query($queryUserTable,true);
						$rowUserTable = $this->db->fetchByAssoc($resultUserTable);
						$value = $rowUserTable['id'];
					}
					else{					
						$value = $getdefsrow['process_object_modify_related_value'];
					}
					if($method == 'append_by_value'){
                        $bean->{$field} .= " ";
                        $bean->{$field}  .= $value;
                    }else{
                        $bean->{$field} = $value;
                    }
				}
				if($method == 'from_field'){
					$value = $getdefsrow['process_object_modify_related_field_by_field'];
					if(property_exists($focusBean, $value)){
						$bean->{$field} = $focusBean->{$value};
					}	
				}
				if(($method == 'from_field') || ($method == 'append_by_field')){
					$from_field = $getdefsrow['process_object_modify_related_field_by_field'];
					if($method == 'append_by_field'){
						$bean->{$field} .= " ";
					    $bean->{$field}  .= $focusBean->{$from_field};
					}else{
	    				$bean->{$field} = $focusBean->{$from_field};
					}
				}					
			}
			$bean->save();
		}	
	}

	//DONE 
	return true;
}

function getRelatedBeans($focusObjectType, $focusObjectId,$relatedModule){
	//Convert table to bean
	$processManagerEngine1 = new ProcessManagerEngine1();
	$focusBean = $processManagerEngine1->convertTableToBean($focusObjectType);
    if(empty($focusBean)){
		  return;
	  }
    //$focusBean = BeanFactory::retrieveBean($focusBean->module_name, $focusObjectId, array('disable_row_level_security' => true));
	$focusBean->retrieve($focusObjectId);
	if(empty($focusBean->id)) return false; // If Empty
	$focusBean->load_relationships();
	$linkedFields = $focusBean->get_linked_fields();
	$relatedbean = $processManagerEngine1->convertTableToBean(strtolower($relatedModule));
    if(empty($relatedbean)){
        $table = $focusBean->field_defs[$relatedModule];
        if(isset($table['module'])){
            $linkedModule = $table['module'];
        }else{
            $linkedModule = $table['link'];
        }
        $linkedModule= strtolower($linkedModule);
        $relatedbean = $processManagerEngine1->convertTableToBean($linkedModule);
	  }
	$relatedObjectName = $relatedbean->object_name;
    $linkedFields = $focusBean->get_linked_fields();
    foreach ($linkedFields as $link) {
        if($link['module'] == $relatedModule){
            $relatedModule = $link['name'];
            break;
        }
    }
	$relatedBeans = $focusBean->get_linked_beans($relatedModule,$relatedObjectName,NULL,0,100,0,'');
	return $relatedBeans;
}

function getRelatedObjectEmailField($focusObjectType, $focusObjectId,$relatedModule,$relatedField){
	//Convert table to bean
	$processManagerEngine1 = new ProcessManagerEngine1();
	$focusBean = $processManagerEngine1->convertTableToBean($focusObjectType);
    if(empty($focusBean)){
		  return;
	  }
    $focusBean = BeanFactory::retrieveBean($focusBean->module_name, $focusObjectId, array('disable_row_level_security' => true));
	//$focusBean->retrieve($focusObjectId);
	if(empty($focusBean->id)) return false; // If Empty
	$focusBean->load_relationships();
	$linkedFields = $focusBean->get_linked_fields();
	$relatedbean = $processManagerEngine1->convertTableToBean($relatedModule);
    if(empty($relatedbean)){
		  return;
	  }	
	foreach ($linkedFields as $link){
		$linkName = $link['name'];
		//$relationship = $focusBean->load_relationships($linkName);
		//Get the name of the RHS table
		if((!empty($linkName)) && ($linkName != 'teams')) {
			if ($linkName == $relatedModule) {
				//We have the link name and it is rhs
				break;
			}
		}
	}
	$relatedObjectName = $relatedbean->object_name;
	$relatedBeans = $focusBean->get_linked_beans($linkName,$relatedObjectName);
	foreach($relatedBeans as $bean){
		$to_address = $bean->$relatedField;
	}
	return $to_address;		
}



//New for PM Pro V2
//Check for related filter fields
function checkRelatedFilterFields($passFilterTest,$relatedFilterID,$process_id,$related_object,$focusObjectId,$focusObjectType){
	//Get the related module
	//Instantiate the bean first
	//First check to make sure we are still true - no need to check the related object if we failed the main object
	if($passFilterTest){
	  $processManagerEngine1 = new ProcessManagerEngine1();
	  $newBean = $processManagerEngine1->convertTableToBean($focusObjectType);
    if(empty($newBean)){
		  return;
    }
    $newBean = BeanFactory::retrieveBean($newBean->module_name, $focusObjectId, array('disable_row_level_security' => true));
    //$newBean->retrieve($focusObjectId);
    $related_object = strtolower($related_object);
    //If there is a dash in the related object - this means there are more than one relationships from process object to related object
    $pos = strpos($related_object, '-');
    if ($pos === false) {
        $relatedBean = $processManagerEngine1->convertTableToBean($related_object);
    } else {
        $linkName = substr($related_object,$pos +1);
        $related_object = substr($related_object,0,$pos);
        $relatedBean = $processManagerEngine1->convertTableToBean($related_object);
    }
    //$relatedBean =   $this->getRelatedBeans($focusObjectType, $focusObjectId,$related_object);
    if(empty($relatedBean)){
        $table = $newBean->field_defs[$related_object];
        if(isset($table['module'])){
            $linkedModule = $table['module'];
        }else{
            $linkedModule = $table['link'];
        }
        $linkedModule= strtolower($linkedModule);
        $relatedBean = $processManagerEngine1->convertTableToBean($linkedModule);
    }
    $linkedFields = $newBean->get_linked_fields();
        if(empty($linkName)) {
            foreach ($linkedFields as $link) {
                //On Custom modules the name is left object and right object
                $linkName = $link['name'];
                if ((!empty($linkName)) && ($linkName != 'teams')) {
                    $relationship = $newBean->get_linked_beans($linkName, $relatedBean->object_name);
                    if (array_key_exists(0, $relationship)) {
                        $tableName = $relationship[0]->table_name;
                        //Get the name of the RHS table
                        if ($tableName == $related_object) {
                            //We have the link name and it is rhs
                            break;
                        }
                    }
                }
            }
        }
	  $newBean->load_relationships();
      $relatedBeans = $newBean->get_linked_beans($linkName,$relatedBean->module_name);
      if($related_object != $relatedBean->table_name)
         $related_object = $relatedBean->table_name;

		//Get all the related beans
	  	$resultRelatedProcessFilterTable = $this->getProcessRelatedFilterTableEntry($relatedFilterID);
		$counter = 0;
		while($rowRelatedProcessFilterTable =  $this->db->fetchByAssoc($resultRelatedProcessFilterTable)){
			//PATCH 11-3-2011 - we need to set the pass filter test true for each filter entry and let the code set to false
			$passFilterTest = true;
			$rowProcessFilterTableID = $rowRelatedProcessFilterTable['id'];
			//If we have a process filter table entry then see if the field value pair is equal
			//to the focus object field value pair and if so then run the process - else exit
			//We use the function getFocusObjectFields
			if($rowProcessFilterTableID != ''){
					//Get the and/or filter fields entry
					$andOrFilterFields = $rowRelatedProcessFilterTable['andorfilterfields'];
					$focusFieldsArray = array();
					$field = $rowRelatedProcessFilterTable['related_field_name'];
					$value = $rowRelatedProcessFilterTable['related_field_value'];
					//Get the Filter Operator: equal, not equal, less than, greater than
					$fieldOperator = $rowRelatedProcessFilterTable['related_field_operator'];
					$focusFieldsArray[$field] = $field;
					//If we are checking for a custom field then call the function to get the
					//custom fields for the object - else call the original getFocusObjectFields
					//Dont do anything is both the field name and value are blank
					if ($field != '' ) {
						$GLOBALS['log']->info("Process Manager - Checking to see if object passes filter test for object type $focusObjectType and object id $focusObjectId");
						//For each related object - could be a OTM
						if(empty($relatedBeans)){
							$passFilterTest = FALSE;
						}
						else{
							foreach ($relatedBeans as $bean){
								$relatedFocusObjectId = $bean->id;
								$passFilterTest = $this->getFilterTestResult($passFilterTest,$field,$fieldOperator,$value,$relatedFocusObjectId,$related_object,$focusFieldsArray, '');
							}
						}
					}
			
				}
				//If this is an or'ing then we are going to hold the value of the fileter test in the array
				if ($andOrFilterFields == 'or') {
					$andOrFilterFieldsArray[$counter] = $passFilterTest;
				}
				//Patch 12-9-2011 - if the first filter field is false and the 2nd is false then we have a bug that said it is true for and
				else{
					$andFilterFieldsArray[$counter] = $passFilterTest;
				}
				$counter = $counter + 1;
			}
			//So we have a default process - now we are going to check to see if
			//we are finally ready to enter the steps to check for stages and tasks
			//If there were any filters and all filter conditions passed then we are true
			//otherwise we would be false
			
			//Here we are going to see if the filter fields were or'd and if there are any trues then we pass
			if ($andOrFilterFields == 'or') {
				if (in_array("1",$andOrFilterFieldsArray)) {
					$passFilterTest = true;
				}
			}
			//Patch 12-9-2011 - if the first filter field is false and the 2nd is false then we have a bug that said it is true for and
			else{
				if (in_array("0",$andFilterFieldsArray)) {
					$passFilterTest = false;
				}
			}
		
		}
	return $passFilterTest;
}

//***************************************************************************************************
//This function will get the entry in the filter table for the given process
//***************************************************************************************************

function getProcessRelatedFilterTableEntry($process_id){
	$query = "Select * from  pm_process_related_filter_table where id = '" .$process_id ."' ORDER BY sequence ASC";
	$result = $this->db->query($query);
	return $result;
}

}
?>
