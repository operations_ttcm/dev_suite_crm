<?php
require_once('include/MVC/View/views/view.detail.php');

class PM_ProcessManagerViewDetail extends ViewDetail {

	function PM_ProcessManagerViewDetail() {
		parent::ViewDetail();
	}
	
	function preDisplay() {
		if(!isset($_REQUEST['legacy'])){
			if(isset($_REQUEST['record']))
				$record = $_REQUEST['record'];
			else
				$record = false;

			$queryParams = array(
				'module' => 'PM_ProcessManager',
				'action' => 'CreateV2',
				'record' => $record,
			);
			if($record === false) unset($queryParams['record']);
			SugarApplication::redirect('index.php?' . http_build_query($queryParams));
			//Don't go here since we are making this mandatory
			echo "<script type='text/javascript'>
				var msg = 'To go to the legacy Process Manager interface press Cancel. Pressing OK will bring you to the new and improved PM Enterprise interface.';
				if(!confirm(msg)){
					//GO TO LEGACY
					window.location.href='index.php?module=PM_ProcessManager&action=DetailView$record&legacy=1';
				}else{
					//Go to New Interface
					window.location.href='index.php?module=PM_ProcessManager&action=CreateV2$record&legacy=0';
				}	
			</script>";
		}
		parent::preDisplay();
	}
	function display() {
		parent::display();
	}

}
