<?php

/*******
 * PM Process Reports
 * *****/
global $db, $process_id, $current_user;
require_once('include/TimeDate.php');
$td = new TimeDate();

if(isset($_REQUEST['process_id']) && !empty($_REQUEST['process_id'])){
	$process_id = $_REQUEST['process_id'];
}else{
	die('Invalid Request.');
}
if(isset($_REQUEST['method']) && !empty($_REQUEST['method'])){
	if(function_exists($_REQUEST['method']))
		$_REQUEST['method']();
	else
		die('Invalid AJAX Request.');
}

$data['completed'] = get_completed_data();

$data['waiting'] = get_waiting_data();
extract($data);

function delete_waiting(){
	global $db;
	$id = !empty($_REQUEST['id']) ? $_REQUEST['id'] : die('0');
	$res = $db->query("DELETE FROM pm_process_stage_waiting_todo WHERE id = '$id'");
	if($res) die('1');
	else die('0');
}

function get_completed_data(){
	global $db, $process_id;
	//Get the process object for the process
	$pmobject = "select process_object from pm_processmanager where id = '$process_id'";
	$respmobject = $db->query($pmobject);
	$rowpmobject = $db->fetchByAssoc($respmobject);
	$process_object = $rowpmobject['process_object'];
	$bean = convert_table_to_bean($process_object);//
	$get = "SELECT 
		pm_process_completed_process.object_id,
		pm_process_completed_process.object_type,
		pm_process_completed_process.stage_id,
		pm_process_completed_process.process_complete_date,
		pm_processmanagerstage.name as 'stage_name'
		FROM
		pm_process_completed_process
		LEFT JOIN pm_processmanagerstage ON pm_processmanagerstage.id = pm_process_completed_process.stage_id 
		WHERE
		pm_process_completed_process.process_id = '$process_id'
		";	
	$res = $db->query($get);
	$data = array();
	while($row = $db->fetchByAssoc($res)){
		$bean->retrieve($row['object_id']); 
		$data[] = array(
			'id' => $row['object_id'],
			'name' => $bean->name,
			'module' => $bean->module_dir,
			'object_name' => $bean->object_name,
			'table_name' => $bean->table_name,
			'date_entered' => $row['process_complete_date'],
			'date_completed' => $row['process_complete_date'],
			'stage_id' => $row['stage_id'],
			'stage_name' => $row['stage_name']
		);
	}
	return $data;
}

function get_waiting_data($stage_id = false){
	global $db, $process_id;
	//Get the process object for the process
	$pmobject = "select process_object from pm_processmanager where id = '$process_id'";
	$respmobject = $db->query($pmobject);
	$rowpmobject = $db->fetchByAssoc($respmobject);
	$process_object = $rowpmobject['process_object'];
	$bean = convert_table_to_bean($process_object);//

	$get = "SELECT 
		pm_process_stage_waiting_todo.id as waiting_id,
		pm_process_stage_waiting_todo.object_id,
		pm_process_stage_waiting_todo.object_type,
		pm_process_stage_waiting_todo.stage_id,
		pm_process_stage_waiting_todo.start_delay_type,
		pm_process_stage_waiting_todo.start_time,
		pm_processmanagerstage.name as 'stage_name'
		FROM
		pm_process_stage_waiting_todo
		LEFT JOIN pm_processmanagerstage ON pm_processmanagerstage.id = pm_process_stage_waiting_todo.stage_id 
		WHERE
		pm_process_stage_waiting_todo.process_id = '$process_id'";
	if($stage_id !== false) {
		$get .= " AND pm_process_stage_waiting_todo.stage_id = '$stage_id' ";
	}
	$res = $db->query($get);
	$data = array();
	while($row = $db->fetchByAssoc($res)){
		$bean->retrieve($row['object_id']);
		$data[] = array(
			'waiting_id' => $row['waiting_id'],
			'id' => $row['object_id'],
			'name' => $bean->name,
			'module' => $bean->module_dir,
			'object_name' => $bean->object_name,
			'table_name' => $bean->table_name,
			'start_time' => $row['start_time'], 
			'start_delay_type' => $row['start_delay_type'], 
			'stage_id' => $row['stage_id'],
			'stage_name' => $row['stage_name']
		);
	}
	return $data;
}

function get_waiting_data_json(){
	$stage_id = !empty($_REQUEST['stage_id']) ? $_REQUEST['stage_id'] : false;
	$data = get_waiting_data($stage_id);
	include('modules/PM_ProcessManager/ProcessReportsWaiting.php');
	die();
}


function convert_table_to_bean($module){
	require_once('modules/PM_ProcessManager/ProcessManagerEngine.php');
	require_once('modules/PM_ProcessManager/ProcessManagerEngine1.php');
	$pm1 = new ProcessManagerEngine1();
	return $pm1->convertTableToBean($module);
}

?>
<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.3.0/pure-min.css">
<style>
	.pure-button-xsmall {
	    font-size: 85%;
	}
	.process_table {
		border: 1px solid black;
	}
	.process_table td {
		padding: 5px;
	}
</style>
<h1>Completed Processes</h1>
<input type="hidden" value="<?php echo $process_id; ?>" id="process_id">
<table class="pure-table">
	<thead>
		<tr>
			<th>Record Name</th>
			<th>Module</th>
			<th>Date Entered</th>
			<th>Date Completed</th>
			<th>Completed Stage</th>
		</tr>
	</thead>
	<tbody>
		<?php $c = 0; foreach($completed as $row): ?>
			<tr <?php if($c % 2 == 0) echo "bgcolor = \"#00FF00;\""; else echo "bgcolor = \"#5e9a5e;\""; ?> >
				<td><a href="index.php?module=<?php echo $row['module']; ?>&action=DetailView&record=<?php echo $row['id']; ?>" title="Go to Record" ><?php echo $row['name']; ?></a></td>
				<td><?php echo $row['module']; ?></td>
				<td><?php echo date('Y-m-d H:i:s', strtotime($row['date_entered'])); ?></td>
				<td><?php echo date('Y-m-d H:i:s', strtotime($row['date_completed'])); ?></td>
				<td class="filter_by_stage" data-stage="<?php echo $row['stage_id']; ?>"><?php echo $row['stage_name']; ?></td>
			</tr>
		<?php $c++; endforeach; ?>
	</tbody>
</table>
<?php include('modules/PM_ProcessManager/ProcessReportsWaiting.php'); ?>
<script type="text/javascript">
$(document).ready(function(){
	$('#filter_by_stage').click(function(){
		var data = {};
		data.stage_id = $(this).attr('data-stage');
		data.process_id = $('#process_id').val();
		data.method = 'get_waiting_data_json';
		//Make AJAX Request to get the stages wating to do
		$.post('index.php?module=PM_ProcessManager&action=ProcessReports&to_pdf=1', data)
			.success(function(res){
				$('#waiting').html(res);
				init_stuff();
			})
				.error(function(err){
					console.log(err);
					alert('An Error Occurred.');
				});
	});
	init_stuff();
});
function init_stuff(){
	$('.delete').unbind('click');
	$('.delete').click(function(){
		var obj = $(this);
		var id = obj.attr('data-waiting-id');
		if(id && id.length > 0){
			var data = {};
			data.id = id;
			data.process_id = $('#process_id').val();
			data.method = 'delete_waiting';
			$.post('index.php?module=PM_ProcessManager&action=ProcessReports&to_pdf=1', data)
				.success(function(res){
					if(res == '0'){
						alert('Cannot Delete Waiting TODO - it may not exist.');
						return false;
					}
					obj.parents('tr.row').remove();
					init_stuff();
				})
					.error(function(err){
						console.log(err);
						alert('An Error Occurred.');
					});
			return false;
		}else{
			alert('Error: Try again later.');
			return false;
		}
	});
}
</script>
