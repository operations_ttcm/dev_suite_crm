<?php


class OutfittersLicense
{
    /**
     * For validation via server-side. Whomever calls this should handle what to do in case of a failure.
     *
     * Check return for "!== true" as the error will be returned in the case that it didn't validate.
     *
     * returns true or else the error string
     */
    public static function isValid($thisModule=null)
    {
        if(empty($thisModule)) {
            global $currentModule;
            $thisModule = $currentModule;
            
            //if still empty...then get out of here...in an odd spot in SugarCRM
            if(empty($thisModule)) {
                return true;
            }
        }

        //load license validation config
        require('modules/'.$thisModule.'/license/config.php');
        
        //check last validation
        require_once('modules/Administration/Administration.php');
        $administration = new Administration();
        $administration->retrieveSettings();
        $last_validation = $administration->settings['SugarOutfitters_'.$outfitters_config['shortname']];       
        $trimmed_last = trim($last_validation); //to be safe...
        //make sure serialized string is not empty
        if (!empty($trimmed_last)){
            $last_validation = base64_decode($last_validation);
            $last_validation = unserialize($last_validation);
            
            //if enough time hasn't passed then just return the last result
            //even if the last result failed
            $frequency = $outfitters_config['validation_frequency'];
            $elapsed = (7 * 24 * 60 * 60); //default to weekly
            if($frequency == 'hourly') {
                $elapsed = (60 * 60);
            } else if($frequency == 'daily') {
                $elapsed = (24 * 60 * 60);
            }
            
            if(($last_validation['last_ran'] + $elapsed) >= time()) {
                if($last_validation['last_result']['success'] === false) {
                    return $last_validation['last_result']['result'];
                } else {
                    return true; 
                }
            }
        }
        //otherwise continue with validation
        
        $validated = OutfittersLicense::doValidate($thisModule);
        
        $store = array(
            'last_ran' => time(),
            'last_result' => $validated,
        );
 
        $serialized = base64_encode(serialize($store));
        $administration->saveSetting('SugarOutfitters', $outfitters_config['shortname'], $serialized);
    
        if($validated['success'] === false) {
            return $validated['result'];
        } else {
            return true; 
        }
    }
    
    /**
     * For validation via client-side (used by License Configuration form)
     *
     * Does NOT obey the validation_frequency setting. Validates every time.
     * This function is meant to be used only on the License Configuration screen for a specific add-on
     */
    public static function validate()
    {
        $json = getJSONobj();
        if(empty($_REQUEST['key'])) {
            header('HTTP/1.1 400 Bad Request');
            $response = "Key is required.";
            echo $json->encode($response);
        }
        
        global $sugar_config, $currentModule;

        //load license validation config
        require('modules/'.$currentModule.'/license/config.php');

        $validated = OutfittersLicense::doValidate($currentModule,$_REQUEST['key']);

        $store = array(
            'last_ran' => time(),
            'last_result' => $validated,
        );

        require_once('modules/Administration/Administration.php');
        $administration = new Administration();
        $serialized = base64_encode(serialize($store));
        $administration->saveSetting('SugarOutfitters', $outfitters_config['shortname'], $serialized);
        
        if($validated['success'] === false) {
            header('HTTP/1.1 400 Bad Request');
        } else {
            //use config_override.php...config.php has a higher chance of having rights restricted on servers
            global $currentModule;

            //load license validation config
            require('modules/'.$currentModule.'/license/config.php');
            
            require('modules/Configurator/Configurator.php');
            $cfg = new Configurator();
            $cfg->config['outfitters_licenses'][$outfitters_config['shortname']] = $_REQUEST['key'];
            $cfg->handleOverride();     
        }

        echo $json->encode($validated['result']);
    }
    
    /**
     * Internal method that makes the actual API request
     *
     * returns array(
     *      success => true/false
     *      result    => result set returned by the server
     */
    public static function doValidate($thisModule,$key=null)
    {
        global $sugar_config;

        //load license validation config
        require('modules/'.$thisModule.'/license/config.php');
        
        //if no key is provided then look for an existing key
        if(empty($key)) {
            $key = $sugar_config['outfitters_licenses'][$outfitters_config['shortname']];
            if(empty($key)) {
                return array(
                        'success' => false,
                        'result' => 'Key could not be found locally. Please go to the license configuration tool and enter your key.'
                    );
            }
        }

        $post_fields = 'public_key='.$outfitters_config['public_key'].'&key='.$key;

        if(isset($outfitters_config['validate_users']) && $outfitters_config['validate_users'] == true) {
            $active_users = get_user_array(FALSE,'Active','',false,'',' AND portal_only=0 AND is_group=0');
            $post_fields .= '&user_count='.count($active_users);
        }
   
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $outfitters_config['api_url'].'/key/validate?'.$post_fields); 
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        $json = getJSONobj();
        $result = $json->decode($response);        

        //if it is not a 200 response assume a 400. Good enough for this purpose.
        if($info['http_code'] != 200) {
            $GLOBALS['log']->fatal('Unable to validate: '.print_r($result,true));
            return array(
                    'success' => false,
                    'result' => $result
                );
        } else {
            return array(
                    'success' => true,
                    'result' => $result
                );
        }    
    }
    
    /**
     * Only meant to be ran from the scope of the main module. Uses $currentModule.
     */
    public static function change()
    {
        if(empty($_REQUEST['key'])) {
            header('HTTP/1.1 400 Bad Request');
            $response = "Key is required.";
            $json = getJSONobj();
            echo $json->encode($response);
        }
        if(empty($_REQUEST['user_count'])) {
            header('HTTP/1.1 400 Bad Request');
            $response = "User count is required.";
            $json = getJSONobj();
            echo $json->encode($response);
        }

        global $currentModule;

        //load license validation config
        require('modules/'.$currentModule.'/license/config.php');

        $post_fields = 'public_key='.$outfitters_config['public_key'].'&key='.$_REQUEST['key'].'&user_count='.$_REQUEST['user_count'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $outfitters_config['api_url'].'/key/change');
        curl_setopt($ch, CURLOPT_POST, 1); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields); 
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        //if it is not a 200 response assume a 400. Good enough for this purpose.
        if($info['http_code'] != 200) {
            header('HTTP/1.1 400 Bad Request');
            $GLOBALS['log']->fatal('Unable to update the user count: '.print_r($result,true));
        } else {
            require_once('modules/Administration/Administration.php');
            global $sugar_config, $sugar_version;
            $sugar_config['outfitters_licenses'][$outfitters_config['shortname']] = $_REQUEST['key'];
            rebuildConfigFile($sugar_config, $sugar_version);
        }

        echo $response;
    }
}
