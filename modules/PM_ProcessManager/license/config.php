<?php

$outfitters_config = array(
    'shortname' => 'process-manager-enterprise-20', //The short name of the Add-on. e.g. For the url https://www.sugaroutfitters.com/addons/sugaroutfitters the shortname would be sugaroutfitters
    'public_key' => '51d12a8580081f1fd9474f988a1756d8', //The public key associated with the group
    'api_url' => 'https://www.sugaroutfitters.com/api/v1',
    'validate_users' => false,
    'validation_frequency' => 'weekly', //default: weekly options: hourly, daily, weekly
    'continue_url' => 'index.php?module=PM_ProcessManager&action=ListView', 
);

$sierracrm_config = array(
     'api_url' => 'http://sierracrm.com/ValidateLicenseKey.php',
    'validate_users' => false,
    'validation_frequency' => 'weekly', //default: weekly options: hourly, daily, weekly
    'validate_sugaroutfitters' => false,
);

