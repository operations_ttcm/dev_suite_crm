<?php
//This is the Development Instnace
if(!defined('sugarEntry'))define('sugarEntry', true);
//chdir(realpath(dirname(__FILE__)));
require_once('modules/PM_ProcessManager/ProcessManagerEngine.php');

global $today;
global $current_user;
global $db;
$today = date('Y-M-D h:i:s');
$pmengine = new ProcessManagerEngine();

$qwf = "select * from workflow where deleted = 0 and type = 'Normal' and status = 1";
$reswf = $pmengine->db->query($qwf);
while($rowwf = $pmengine->db->fetchByAssoc($reswf)) {
    //Create the Process Stub
    $pid = createProcess($rowwf,'');
    //Now go and see if there are any conditions
    if($rowwf['type'] == 'Normal'){
        checkConditions($rowwf,$pid);
        //Now add a single Stage
        $stageID = createStage('0',$pid, $rowwf);
        //Now lets do the Alerts
        checkAlerts($rowwf,$pid,$stageID);
        checkActions($rowwf,$pid, $stageID);
    }
}

$qwft = "select * from workflow where deleted = 0 and type = 'Time' and status = 1";
$reswft = $pmengine->db->query($qwft);
while($rowwft = $pmengine->db->fetchByAssoc($reswft)) {
			checkElapsedTimeConditions($rowwft);
}

//Return to PM
header("Location: index.php?action=index&module=PM_ProcessManager");

function createProcess($rowwf,$name)
{
    global $today, $current_user, $process_module;
    $modules = $GLOBALS['moduleList'];
    foreach ($modules as $module) {
        $basMod = $rowwf['base_module'];
        if ($module == $rowwf['base_module']) {
            $bean = BeanFactory::getBean($module);
            $table = $bean->table_name;
            $process_module = $module;
            break;
        }
    }
    $newPM = new PM_ProcessManager();
    if(!empty($name)){
    	$newPM->name = $name;
    }else{
    	$newPM->name = $rowwf['name'];
  	}
  	$newPM->assigned_user_id = $current_user->id;
    $newPM->modified_user_id = $current_user->id;
    $newPM->created_by = $current_user;
    $newPM->related_object = '';
    if ($rowwf['status'] == 1)
        $newPM->status = 'Active';
    else
        $newPM->status = 'Inactive';
    $newPM->status = 'Inactive';
    $newPM->process_object = $table;
    if ($rowwf['record_type'] == 'New')
        $newPM->start_event = 'Create';
    elseif($rowwf['record_type'] == 'Update')
        $newPM->start_event = 'Modify';
    else
        $newPM->start_event = 'Create or Modify';
    $newPM->save();
    //Create the dummy pm_process_defs record
    $table = 'pm_process_defs';
    $values = array();
    $values['id'] = $newPM->id;
    $values['process_id'] = $newPM->id;
    $values['ignore_process_prev_run_check'] = '0';
    $values['process_last_run_date'] = '0000-00-00 00:00:00';
    insertSql($table,$values);    
    return $newPM->id;
}

function checkActions($rowwf,$pid, $stageID){
    global $db;
    $resActionShells = getActionShells($rowwf['id']);
    while($rowactShell = $db->fetchByAssoc($resActionShells)) {
        //Get the Actual Actions for this shell
        switch ($rowactShell['action_type']){
            case 'new':
                createNewRecordTask($rowactShell,$pid,$stageID);
                break;
            case 'update':
                createUpdateRecordTask($rowactShell,$pid,$stageID);
                break;
            case 'update_rel':
                createUpdateRelatedRecordTask($rowactShell,$pid,$stageID);
                break;
        }
    }
}

function createUpdateRelatedRecordTask($rowactShell,$pid,$stageID){
    //Get all the Actions for this Action Shell
    global $db;
    $name = "Update Related Record";
    $taskID = createProcessTask($name,'Modify Related Object Field',$stageID);
    $resActions = getActions($rowactShell['id']);
    $rowaction = $db->fetchByAssoc($resActions);
    if($rowaction['field'] == 'assigned_user_id' || $rowaction['field'] == 'team_id'){
        if($rowaction['field'] == 'team_id'){
            $rowaction['value'] = getTeamName($rowaction['value']);
        }else{
            $rowaction['value'] = getAssignedUserName($rowaction['value']);
        }
    }
    $table = 'pm_process_task_modify_field_defs';
    $values = array();
    $values['id'] = create_guid();
    $values['task_id'] = $taskID;
    $values['process_object_modify_related_object'] = $rowactShell['rel_module'];
    $values['process_object_modify_related_field'] = $rowaction['field'];
    $values['related_modify_method'] = 'by_value';
    $values['process_object_modify_related_value'] = $rowaction['value'];
    $values['process_object_modify_related_field_from_object'] = '';
    insertSql($table,$values);
}

function createUpdateRecordTask($rowactShell,$pid,$stageID){
    //Get all the Actions for this Action Shell
    global $db;
    $name = "Update Record";
    $taskID = createProcessTask($name,'Modify Object Field',$stageID);
    $resActions = getActions($rowactShell['id']);
    
    while($rowaction = $db->fetchByAssoc($resActions)){
	    $table = 'pm_process_task_modify_field_defs';
	    $values = array();
	    $values['id'] = create_guid();
	    $values['task_id'] = $taskID;
	    $values['process_object_modify_field'] = $rowaction['field'];
	    $values['modify_method'] = 'by_value';
	    //Convert assigned user and team id fields fields
	    if($rowaction['field'] == 'assigned_user_id' || $rowaction['field'] == 'team_id'){
	    	if($rowaction['field'] == 'team_id'){
	    		$rowaction['value'] = getTeamName($rowaction['value']);
	    	}else{
	    		$rowaction['value'] = getAssignedUserName($rowaction['value']);
	    	}
	    }
	    $values['process_object_modify_field_value'] = $rowaction['value'];
	    insertSql($table,$values);
  	}
}

function createNewRecordTask($rowactShell,$pid,$stageID){
    //Get all the Actions for this Action Shell
    $name = "New " .$rowactShell['action_module'] ." Record";
    $taskID = createProcessTask($name,'Create New Record',$stageID);
    //Now create the pm_process_task_create_object_defs record
    global $db;
    $table = 'pm_process_task_create_object_defs';
    $values = array();
    $values['id'] = create_guid();
    $values['create_object_type'] = $rowactShell['action_module'];
    $values['task_id'] = $taskID;
    $values['create_record_method'] = 'By Field';
    insertSql($table,$values);

    $resActions = getActions($rowactShell['id']);
    while($rowaction = $db->fetchByAssoc($resActions)) {
        if($rowaction['field'] == 'assigned_user_id' || $rowaction['field'] == 'team_id'){
            if($rowaction['field'] == 'team_id'){
                $rowaction['value'] = getTeamName($rowaction['value']);
            }else{
                $rowaction['value'] = getAssignedUserName($rowaction['value']);
            }
        }
        createProcessObjectDefs($rowaction, $stageID, $taskID );
    }

}
//Now create each record in the pm_process_task_create_by_field_defs
function createProcessObjectDefs($rowaction, $stageID, $taskID ){
    global $db;
    $table = 'pm_process_task_create_by_field_defs';
    $values = array();
    $values['id'] = create_guid();
    $values['task_id'] = $taskID;
    $values['create_by_field_field'] = $rowaction['field'];
    //Convert datetime value for PM
    if($rowaction['adv_type'] == 'datetime'){
    	$value = convertDateTime($rowaction['value']);
    	$values['create_by_field_method'] = 'by_date';
    	$values['create_by_field_by_date'] = $value ;
    }else{
    	$values['create_by_field_method'] = 'by_value';
    	$values['create_by_field_by_value'] = $rowaction['value'];
    }
    
    insertSql($table,$values);
}

function checkAlerts($rowwf,$pid, $stageID){
    global $db;
    global $emailArray, $mulitEmail, $createProcessTask, $lastEmail;
    $mulitEmail = FALSE;
    $createProcessTask = TRUE;
    $counter = 1;
    $resAS = getAlertShells($rowwf['id']);
    while($rowas = $db->fetchByAssoc($resAS)) {
        //Get the Actual Alerts for this shell
        //Process Email Alert Shells
        if($rowas['alert_type'] == 'Email'){
            $resAlerts = getAlerts($rowas['id']);
            //If there is just one alert then call process alert for one
            if($resAlerts->num_rows < 2){
            	$rowalert = $db->fetchByAssoc($resAlerts);
            	processAlert($rowalert, $rowas, $stageID);
            }
            else{
            	$alertsCount = $resAlerts->num_rows;
	            while($rowalert = $db->fetchByAssoc($resAlerts)) {
	            	  $mulitEmail = TRUE;
	            	  if($alertsCount == $counter){
	            	  	$lastEmail = TRUE;
	            	  }
	            	  else{
	            	  	$lastEmail = FALSE;
	            	  	$counter++;
	            	  } 
	                processAlert($rowalert, $rowas, $stageID);
	                $createProcessTask = FALSE;
	            }
          	}
        }
        //Handle Invite Alert Type for non System Default
        if($rowas['alert_type'] == 'Invite' && $rowas['source_type'] == 'Normal Message'){
            $resAlerts = getAlerts($rowas['id']);
            while($rowalert = $db->fetchByAssoc($resAlerts)) {
                processAlert($rowalert, $rowas, $stageID);
            }
        }        
        //TODO Process The Other Type of Alerts - Invites
    }
}

function processAlert($rowalert,$rowas, $stageID){
    //Create the Process Task and Relate to the Stage
    global $emailArray, $mulitEmail, $createProcessTask, $lastEmail;
    if($createProcessTask){
    	$taskID = createProcessTask($rowas['name'],'Send Email',$stageID);
    }
    switch ($rowalert['user_type']){
        case 'specific_user':
            createEmailProcessTask($rowas,FALSE,$rowalert['field_value'],'','','',$taskID,'',$rowalert['address_type'],$rowalert);
            break;
        case 'login_user':
            createEmailProcessTask($rowas,TRUE,$rowalert['field_value'],'','','',$taskID,'',$rowalert['address_type'],$rowalert);
            break;
        case 'specific_team':
            $teamName = getTeamName($rowalert['field_value']);
            createEmailProcessTask($rowas,FALSE,$rowalert['field_value'],$teamName,'','',$taskID,'',$rowalert['address_type'],$rowalert);
            break;            
        case 'current_user':
            currentUserAlert($rowalert,$rowas, $stageID,$taskID,$taskID);
            break;
        case 'rel_user':
            relatedUserAlert($rowalert,$rowas, $stageID,$taskID);
            break;
        case 'rel_user_custom':
            relatedUserCustomAlert($rowalert,$rowas, $stageID,$taskID);
            break;
        case 'trig_user_custom':
            targetModuleCustomEmailFieldAlert($rowalert,$rowas, $stageID,$taskID);
            break;
    }
}

function targetModuleCustomEmailFieldAlert($rowalert,$rowas, $stageID,$taskID){
	  global $emailArray, $mulitEmail, $lastEmail;
    createEmailProcessTask($rowas,FALSE,'','','','',$taskID,'',$rowalert['address_type'],$rowalert);
}

function relatedUserCustomAlert($rowalert,$rowas, $stageID,$taskID){
	  global $emailArray, $mulitEmail, $lastEmail;
    createEmailProcessTask($rowas,FALSE,'','','','',$taskID,'',$rowalert['address_type'],$rowalert);
}

function relatedUserAlert($rowalert,$rowas, $stageID,$taskID){
	  global $emailArray, $mulitEmail, $lastEmail;
    createEmailProcessTask($rowas,FALSE,'','','','',$taskID,'',$rowalert['address_type'],$rowalert);
}

/*
Sending an Alert to "A User Associated with the Target Module , you have four options
1 - user who created the record - Supported by PM
2 - user who last modified the record - Supported by PM
3 - user who is assigned the record - Supported by PM
4 - user who was assigned the record
*/
function currentUserAlert($rowalert,$rowas, $stageID,$taskID){
	  global $emailArray, $mulitEmail, $lastEmail;
    if($rowalert['array_type'] == 'future' && $rowalert['field_value'] == 'created_by'){
        //Send Email To Who Created the Object
        createEmailProcessTask($rowas,FALSE,$rowalert['field_value'],'','','',$taskID,'user_who_created_record',$rowalert['address_type'],$rowalert);
    }
    if($rowalert['array_type'] == 'past' && $rowalert['field_value'] == 'modified_user_id'){
        //Send Email To Who Last Modified the Object
        createEmailProcessTask($rowas,FALSE,$rowalert['field_value'],'','','',$taskID,'user_who_last_modified_record',$rowalert['address_type'],$rowalert);
    }
    if($rowalert['array_type'] == 'future' && $rowalert['field_value'] == 'assigned_user_id'){
        //Send Email To Who the Object is Assigned to
        createEmailProcessTask($rowas,TRUE,$rowalert['field_value'],'','','',$taskID,'',$rowalert['address_type'],$rowalert);
    }
}

/*
Function createProcessTask
Inputs:
Name of Task
Task Type
Stage
Returns: Task ID
*/
function createProcessTask($name,$taskType,$StageId){
    $newPMStageTask = new PM_ProcessManagerStageTask();
    $newPMStageTask->name = $name;
    $newPMStageTask->task_type = $taskType;
    $newPMStageTask->save();
    $pmstagetaskarray= array('pm_processmanagerstage_ida' => $StageId, 'pm_processmanagerstagetask_idb' => $newPMStageTask->id);
    $newPMStageTask->set_relationship('pm_processmgerstagetask', $pmstagetaskarray);
    return $newPMStageTask->id;
}

/*
Function createEmailProcessTask
Inputs:
Alert Shell Record
Send Email to Object Owner
Send Email to Specific User
Send Email to all members of Team
Send Email to all members of Role
Internal Email To Address - fixed email address
Send Email to Logged In User
Related Task ID
Other Owner
Address Type
Row Alert
*/
function createEmailProcessTask($alertShellRecord,$obectOwner = FALSE,$specificUserId,$team,$role,$internalEmailToAddress,$taskID,$otherOwner, $addressType,$rowalert){
    global $db;
    global $emailArray, $mulitEmail, $lastEmail;
    //Check if Custom Template
    if($alertShellRecord['source_type'] == 'Custom Template'){
        //convert the Email Template
        $body = convertEmailTemplate($alertShellRecord);
        $subject = convertEmailTemplateSubject($alertShellRecord);
        //Now set the email template id
        $name = $alertShellRecord['name'] ." - PM";
        //Only create if we have not yet
        $qemt = "select id from email_templates where name = '$name' and deleted = 0";
        $resemt = $db->query($qemt);
        $rowemt = $db->fetchByAssoc($resemt);
        $emtid = $rowemt['id'];
        if(empty($emtid)) {
            $emailTemplateId = createEmailTemplate($name, $alertShellRecord['alert_text'], $body, $subject);
        }else{
            $emailTemplateId = $emtid;
        }
    }else {
        $name = $alertShellRecord['name'];
        $qemt = "select id from email_templates where name = '$name' and deleted = 0";
        $resemt = $db->query($qemt);
        $rowemt = $db->fetchByAssoc($resemt);
        $emtid = $rowemt['id'];
        if(empty($emtid)) {
            $emailTemplateId = createEmailTemplate($alertShellRecord['name'], $alertShellRecord['alert_text'], '', "WORKFLOW ALERT");
        }else{
            $emailTemplateId = $emtid;
        }
    }
    $table = 'pm_process_task_email_defs';
    if(!$mulitEmail){
    	$values = array();
    }elseif($mulitEmail && empty($emailArray)){
    	$values = array();
    }else{
    	$values = $emailArray;
    }
    $values['id'] = create_guid();
    $values['email_template_name'] = $alertShellRecord['name'] ." - PM";
    $values['email_template_id'] = $emailTemplateId;
    if(!array_key_exists("task_id", $values))
    	$values['task_id'] = $taskID;
    $values['contact_role'] = '';
    if(!empty($internalEmailToAddress)){
        $values['internal_email'] = 1;
        $values['internal_email_to_address'] = $internalEmailToAddress;
        //TO - CC - BCC
        $values = checkToCCBcc($values,$rowalert,'^The alternate or fixed email address^');
    }
    if(!empty($specificUserId) && $rowalert['user_type'] == 'specific_user'){
        //Get the email address of the specific user
        $rowEmailAddress = getEmailAddress($specificUserId,"Users");
        $ownerEmailAddr = $rowEmailAddress['email_address'];
        $values['internal_email'] = 1;
        $values['internal_email_to_address'] = $ownerEmailAddr;
        $values = checkToCCBcc($values,$rowalert,'^The alternate or fixed email address^');
    }
    $values['send_email_to_caseopp_account'] = 0;
    if($obectOwner){
        $values['send_email_to_object_owner'] = 1;
        $values = checkToCCBcc($values,$rowalert,'^The current owner of the Object^');
    }
    $values['email_from_account'] = '';
    $values['send_email_to_contact'] = 0;
    $values['email_queue_campaign_name'] = '';
    $values['send_email_to_email_queue'] = 0;
    if($rowalert['user_type'] == 'trig_user_custom')
        $values['email_field'] = $rowalert['rel_email_value'];
    if(!empty($rowalert['rel_module1'])){
        $values['related_module'] = $rowalert['rel_module1'];
        if(!empty($rowalert['rel_email_value'])){
            $values['related_module_email_field'] = $rowalert['rel_email_value'];
        }else{
            $values['related_module_email_field'] = $rowalert['field_value'];
        }
        $values = checkToCCBcc($values,$rowalert,'^Email Addresses from Related Modules^');
    }
    $values['send_email_to_all'] = 0;
    if($addressType == 'cc'){
        $values['cc_email_to_address'] = $ownerEmailAddr;
    }
    if(!empty($otherOwner)){
        $values['send_email_other_owner'] = $otherOwner;
        $values = checkToCCBcc($values,$rowalert,'^Past Object Owners^');
    }
    if(!empty($team)){
    	$values['send_email_to_team'] = $team;
    	$values = checkToCCBcc($values,$rowalert,'^Members of the Team^');
    }
    if(!$mulitEmail){
    	//If the To, CC and BCC values are empty - default to the process object email address
    	if(!array_key_exists("send_email_to", $values))
    		$values['send_email_to'] = "^The Process Object Email Address^";
    	insertSql($table,$values);
    }
    else{
    	//Is this the last one?
    	if($lastEmail){
    		if(!array_key_exists("send_email_to", $values))
    				$values['send_email_to'] = "process_object_email_address";
    		insertSql($table,$values);
    	}else{
    		$emailArray = $values;
    	}
    }
}

function checkToCCBcc($values,$rowalert,$pmtype){
	  //Determine address type
    if($rowalert['address_type'] != ''){
    	if($rowalert['address_type'] == 'to'){
    		if(empty($values['send_email_to'])){
    			$values['send_email_to'] = $pmtype;
    		}
    		else{
    			$values['send_email_to'] = $values['send_email_to'] ."," .$pmtype;
    		}
    	}
    	if($rowalert['address_type'] == 'cc'){
    		if(empty($values['send_email_cc'])){
    			$values['send_email_cc'] = $pmtype;
    		}
    		else{
    			$values['send_email_cc'] = $values['send_email_cc'] ."," .$pmtype;
    		}
    	}
    	if($rowalert['address_type'] == 'bcc'){
    		if(empty($values['send_email_bcc'])){
    			$values['send_email_bcc'] = $pmtype;
    		}
    		else{
    			$values['send_email_bcc'] = $values['send_email_bcc'] ."," .$pmtype;
    		}
    	}
    	return $values;    	    	
    }
    else{
    	return $values;
    }
}

function convertEmailTemplateSubject($alertShellRecord){
    $emTemplate = new EmailTemplate();
    $emTemplate->retrieve($alertShellRecord['custom_template_id']);
    $subject = $emTemplate->subject;
    $posPast = strpos($subject, '{::past::');
    $pos = strpos($subject, '{::future::');
    if ($pos === false && $posPast === false) {
        return $subject;
    } else {
        //Get the Future Fields
        $countFuture = substr_count($subject, '::future::');
        $i = 0;
        while ($i < $countFuture) {
            $pos = strpos($subject, '{::future::');
            $pos++;
            $posend = strpos($subject, '}', $pos + 1);
            $length = $posend - $pos;
            $section = substr($subject, $pos, $length);
            $sectionConverted = convertSection($section,FALSE);
            //Now replace the current row section with the updated
            $section = "{" .$section ."}";
            $subject = str_replace($section,$sectionConverted,$subject);

            $i++;
        }
        $countPast = substr_count($subject, '::past::');
        $i = 0;
        while ($i < $countPast) {
            $pos = strpos($subject, '{::past::');
            $pos++;
            $posend = strpos($subject, '}', $pos + 1);
            $length = $posend - $pos;
            $section = substr($subject, $pos, $length);
            $sectionConverted = convertSection($section,TRUE);
            //Now replace the current row section with the updated
            $section = "{" .$section ."}";
            $subject = str_replace($section,$sectionConverted,$subject);
            $i++;
        }
    }
    return $subject;
}

function convertEmailTemplate($alertShellRecord){
    $emTemplate = new EmailTemplate();
    $emTemplate->retrieve($alertShellRecord['custom_template_id']);
    $body = $emTemplate->body_html;
    $bodyExplode = explode("\n", $body);
    foreach($bodyExplode as &$row) {
        $posPast = strpos($row, '{::past::');
        $pos = strpos($row, '{::future::');
        if ($pos === false && $posPast === false) {
           continue;
        } else {
            //Get the Future Fields
            $countFuture = substr_count($row, '::future::');
            $i = 0;
            while ($i < $countFuture) {
                $pos = strpos($row, '{::future::');
                $pos++;
                $posend = strpos($row, '}', $pos + 1);
                $length = $posend - $pos;
                $section = substr($row, $pos, $length);
                $sectionConverted = convertSection($section,FALSE);
                //Now replace the current row section with the updated
                $section = "{" .$section ."}";
                $body = str_replace($section,$sectionConverted,$body);
                $row = str_replace($section,$sectionConverted,$row);
                $i++;
            }
            $countPast = substr_count($row, '::past::');
            $i = 0;
            while ($i < $countPast) {
                $pos = strpos($row, '{::past::');
                $pos++;
                $posend = strpos($row, '}', $pos + 1);
                $length = $posend - $pos;
                $section = substr($row, $pos, $length);
                $sectionConverted = convertSection($section,TRUE);
                //Now replace the current row section with the updated
                $section = "{" .$section ."}";
                $body = str_replace($section,$sectionConverted,$body);
                $row = str_replace($section,$sectionConverted,$row);
                $i++;
            }
        }

    }
    return $body;
}

function convertSection($section,$past){
    require_once('modules/PM_ProcessManager/ProcessManagerEngine1.php');
    $pmengine1 = new ProcessManagerEngine1();
    global $process_module;
    $foundName = FALSE;
    $mod_fields = explode("::", $section);
    foreach($mod_fields as &$modfield) {
        if($modfield == 'future' || $modfield == ''){
            continue;
        }else{
            if($modfield == $process_module){
               //Convert the Bean to Table
                $bean = BeanFactory::getBean($modfield);
                $table = $bean->table_name;
                $section = "$" .$table;
            }else{
                    //Either a related module or a field name
                    //already set as table for process object
                    foreach($bean->field_defs as $key => $def) {
                        if(($def['name'] == $modfield) ) {
                            if ($def['type'] != 'link') {
                                if($past){
                                    $section = $section . "_" . $modfield . "_audit_before";
                                }else {
                                    $section = $section . "_" . $modfield;
                                }
                                $foundName = TRUE;
                            }
                        }
                    }
                    if(!$foundName) {
                        //Related Field
                        $section = "$" . $modfield;
                        $bean = $pmengine1->convertTableToBean($modfield);
                        $foundName = TRUE;
                    }
                }
            }
        }
    return $section;
}

function createEmailTemplate($name,$text,$body, $subject){
    $emTemplate = new EmailTemplate();
    $emTemplate->name = $name;
    $emTemplate->description = "Created from Conversion of Legacy Workflow to Process Manager";
    $emTemplate->subject = $subject;
    if(!empty($body)){
        $emTemplate->body_html = $body;
    }else {
        $emTemplate->body = $text;
    }
    $emTemplate->save();
    return $emTemplate->id;
}


/*
Used for Processes with delayed stages
*/
function checkElapsedTimeConditions($rowwf){
    global $db;
    $baseModule = strtolower($rowwf['base_module']);
    $qwfts = "select * from workflow_triggershells where parent_id = '" .$rowwf['id'] ."' and deleted = 0";
    $reswfts = $db->query($qwfts);
    $cancelEvent = FALSE;
    $name = $rowwf['name'] ." - " ."Elapsed Time Process";
    $pid = createProcess($rowwf,$name);    
    //Get the first Cancel Field - PM only supports one Cancel
    while($rowwfts = $db->fetchByAssoc($reswfts)) {
        if($rowwfts['type'] == 'compare_any_time'){
                handleCompareAnyTimeCondition($rowwfts, $pid);
                $cancelEvent = TRUE;
                //Create the Stage
                $stageID = createStage(trim($rowwfts['parameters']),$pid, $rowwf);
        }
        elseif($rowwfts['type'] == 'compare_specific'){
            $expressions = getExpressions($rowwfts);
            $rowexp = $db->fetchByAssoc($expressions);
            //If the Expression lhs_field is empty, then this is a Cancel Event, else a date filter
            if($rowexp['lhs_field'] == ''){
            	$stageID = createStage(trim($rowexp['ext1']),$pid, $rowwf);
            	//Now add the Cancel Event - the operator is either = or !=
           		 handleCompareAnyTimeConditionExp($rowexp,$pid);
          	}
            //Now add the Condition
            $stageID = createStage('0',$pid, $rowwf);
            addTimeFilterField($rowwfts,$pid,$baseModule);
            //$cancelEvent = TRUE;
            //addCondition($rowwfts,$pid);
        }
        else{
            addCondition($rowwfts,$pid);
        }

    }
    checkAlerts($rowwf,$pid,$stageID);
    checkActions($rowwf,$pid, $stageID);
}

function convertDateTime($value){
switch ($value){
        case '14440':
            $convertedDateTime = '+4 hours';
            break;
        case '28800':
        $convertedDateTime = '+8 hours';
            break;
        case '43200':
        $convertedDateTime = '+12 hours';
             break;
        case '86400':
        $convertedDateTime = '+1 days';
            break;
        case '172800':
        $convertedDateTime = '+2 days';
            break;
        case '259200':
        $convertedDateTime = '+3 days';
            break;
        case '345600':
        $convertedDateTime = '+4 days';
            break;
        case '432000':
        $convertedDateTime = '+5 days';
            break;
        case '604800':
        $convertedDateTime = '+6 days';
            break;
        case '1209600':
        $convertedDateTime = '+14 days';
            break;
        case '1814400':
        $convertedDateTime = '+21 days';
            break;
        case '2592000':
        $convertedDateTime = '+1 months';
            break;
        case '5184000':
        $convertedDateTime = '+2 months';
            break;
        case '7776000':
        $convertedDateTime = '+3 months';
            break;
        case '10368000':
        $convertedDateTime = '+4 months';
            break;
        case '1296000':
        $convertedDateTime = '+5 months';
            break;
        case '15552000':
        $convertedDateTime = '+6 months';
            break;
          }
        if(empty($convertedDateTime)){
            if($value < 3600){
                $minutes = $value / 60;
                $convertedDateTime = "+$minutes minutes";
            }elseif($value < 86400){
                //Calculate Hours
                $hours = $value / 60;
                $hours = $hours / 60;
                $convertedDateTime = "+$hours hours";
            }
            else{
                //Calculate Daye
                $days = $value / 24;
                $days = $days / 60;
                $days = $days / 60;
                $convertedDateTime = "+$days days";
            }
        }
	return $convertedDateTime;
}

function convertSecondsToHours($delay){
 //If delay is less than 86400, return 1
 $hours = $delay / 3600;
 return $hours;

}

function convertSecondsToDays($delay){
 //If delay is less than 86400, return 1
 if($delay < 86400){
 	return 0;
 }
 elseif($delay = 86400){
 	return 1;
 }else{
 $days = $delay / 86400;
 return $days;
 }
}

/*
 * Delay is integer that needs to be converted to minutes - hours - days - months - years
 */
function createStage($delay,$pid, $rowwf){
    $newPMStage = new PM_ProcessManagerStage();
    $newPMStage->start_delay_minutes = 0;
    $newPMStage->start_delay_hours = 0;
    $newPMStage->start_delay_days = 0;
    $newPMStage->start_delay_months = 0;
    $newPMStage->start_delay_years = 0;
    switch ($delay){
        case '14440':
            $newPMStage->start_delay_hours = 4;
            break;
        case '28800':
            $newPMStage->start_delay_hours = 8;
            break;
        case '43200':
            $newPMStage->start_delay_hours = 12;
            break;
        case '86400':
            $newPMStage->start_delay_days = 1;
            break;
        case '172800':
            $newPMStage->start_delay_days = 2;
            break;
        case '259200':
            $newPMStage->start_delay_days = 3;
            break;
        case '345600':
            $newPMStage->start_delay_days = 4;
            break;
        case '432000':
            $newPMStage->start_delay_days = 5;
            break;
        case '604800':
            $newPMStage->start_delay_days = 7;
            break;
        case '1209600':
            $newPMStage->start_delay_days = 14;
            break;
        case '1814400':
            $newPMStage->start_delay_days = 21;
            break;
        case '2592000':
            $newPMStage->start_delay_months = 1;
            break;
        case '5184000':
            $newPMStage->start_delay_months = 2;
            break;
        case '7776000':
            $newPMStage->start_delay_months = 3;
            break;
        case '10368000':
            $newPMStage->start_delay_months = 4;
            break;
        case '1296000':
            $newPMStage->start_delay_months = 5;
            break;
        case '15552000':
            $newPMStage->start_delay_months = 6;
            break;
        default:
            if($delay < 3600){
                $minutes = $delay / 60;
                round($minutes,PHP_ROUND_HALF_DOWN);
                $newPMStage->start_delay_minutes = $minutes;
            }elseif($delay < 86400){
                //Calculate Hours
                $hours = $delay / 60;
                $hours = $hours / 60;
                round($hours,PHP_ROUND_HALF_DOWN);
                $newPMStage->start_delay_hours = $hours;
            }elseif($delay < 2678400){
                $days = $delay / 24;
                $days = $days / 3600;
                round($days,PHP_ROUND_HALF_DOWN);
                $newPMStage->start_delay_days = $days;
            }elseif($delay < 31536000){
                $months = $delay / 86400;
                $months = $months / 30;
                round($months,PHP_ROUND_HALF_DOWN);
                $newPMStage->start_delay_months = $months;
            }
            else{
                //Years
                $years = $delay / 31536000;
                round($years,PHP_ROUND_HALF_DOWN);
                $newPMStage->start_delay_years = $years;
            }
    }
    $newPMStage->name = $rowwf['name'];
    $newPMStage->save();
    //Now Relate the Stage to the Process
    $pmpmstagearray= array('pm_processmanager_ida' => $pid, 'pm_processmanagerstage_idb' => $newPMStage->id);
    $newPMStage->set_relationship('pm_processmmanagerstage', $pmpmstagearray);
    return $newPMStage->id;
}

function checkConditions($rowwf,$pid){
    global $db;
    $qwfts = "select * from workflow_triggershells where parent_id = '" .$rowwf['id'] ."' and deleted = 0";
    $reswfts = $db->query($qwfts);
    while($rowwfts = $db->fetchByAssoc($reswfts)) {
        //Trigger Record Change is no Conditions
        if($rowwfts['type'] != 'trigger_record_change'){
            addCondition($rowwfts,$pid);
        }
    }
}

function addCondition($rowwfts,$pid){
    switch ($rowwfts['type']){
        case 'filter_field':
            addFilterField($rowwfts,$pid);
            break;
        case 'compare_specific':
            addFromToFilterField($rowwfts,$pid);
            break;
        case 'compare_change':
            addAnyChangeFilterField($rowwfts,$pid);
            break;
        case 'filter_rel_field':
            addRelatedFilterField($rowwfts,$pid);
            break;
    }

}

//Similar to the other handleCompareAnyTimeCondition function - this function is getting an expressions record
function handleCompareAnyTimeConditionExp($rowwfts,$pid){
    //Create the Cancel Event
    //Determine the Operator
    if ($rowwfts['operator'] == 'Equals'){
    	$op = "!=";
    }else{
    	$op = "=";
    }
    if($rowwfts['rhs_value'] == 'bool_true' && $rowwfts['exp_type'] == 'bool'){
        $rowwfts['rhs_value'] = 1;
    }
    if($rowwfts['rhs_value'] == 'bool_false' && $rowwfts['exp_type'] == 'bool'){
        $rowwfts['rhs_value'] = 0;
    }
    $newPM = BeanFactory::getBean('PM_ProcessManager', $pid);
    $newPM->cancel_on_event = 'Modify';
    $newPM->process_object_cancel_field = $rowwfts['lhs_field'];
    $newPM->process_object_cancel_field_operator = $op;
    $newPM->process_object_cancel_field_value = $rowwfts['rhs_value'];
    $newPM->save();

    //Insert into the PM Cancel Filter Table
    $table = 'pm_process_cancel_filter_table';
    $values = array(
        'id' => create_guid(),
        'process_id' => $pid,
        'cancel_field_name' => $rowwfts['lhs_field'],
        'cancel_field_value' => $rowwfts['rhs_value'],
        'cancel_field_operator' => $op,
        'sequence' => 0,
        'cancelandorfilterfields' => 'and',
        'group_number' => 1,
        'group_andor' => 'and',
        'cancel_field_value_to' => '',
        'cancel_compare_to_field' => 0,
        'cancel_compare_to_field_name' => '',
    );
    insertSql($table,$values);
}

//This handles the Conditions where a field does not change for a specific amount of time
function handleCompareAnyTimeCondition($rowwfts,$pid){
    //Create the Cancel Event
    $newPM = BeanFactory::getBean('PM_ProcessManager', $pid);
    $newPM->cancel_on_event = 'Modify';
    $newPM->process_object_cancel_field = $rowwfts['field'];
    $newPM->process_object_cancel_field_operator = 'any change';
    $newPM->save();

    //Insert into the PM Cancel Filter Table
    $table = 'pm_process_cancel_filter_table';
    $values = array(
        'id' => create_guid(),
        'process_id' => $pid,
        'cancel_field_name' => $rowwfts['field'],
        'cancel_field_value' => '',
        'cancel_field_operator' => 'any change',
        'sequence' => 0,
        'cancelandorfilterfields' => 'and',
        'group_number' => 1,
        'group_andor' => 'and',
        'cancel_field_value_to' => '',
        'cancel_compare_to_field' => 0,
        'cancel_compare_to_field_name' => '',
    );
    insertSql($table,$values);
}

function addRelatedFilterField($rowwfts,$pid){
    global $db;
    $table = 'pm_process_related_filter_table';
    $expressions = getExpressions($rowwfts);
    while($rowexp = $db->fetchByAssoc($expressions)){
        $operator = convertOperator($rowexp['operator']);
        $values = array(
            'id' => create_guid(),
            'process_id' => $pid,
            'status' => '',
            'related_module' => strtolower($rowexp['lhs_module']),
            'related_field_name' => $rowexp['lhs_field'],
            'related_field_operator' => $operator,
            'sequence' => 1,
        );
        insertSql($table,$values);
    }
}

function convertOperator($operator){
    if($operator == 'Equals')
        return "=";
    else
        return "!=";
}

function addAnyChangeFilterField($rowwfts,$pid){
    //Get the expression
    global $db;
    $table = 'pm_process_filter_table';
    $values = array(
        'id' => create_guid(),
        'process_id' => $pid,
        'status' => '',
        'field_name' => $rowwfts['field'],
        'field_value' => '',
        'field_operator' => 'any change',
        'sequence' => 0,
        'andorfilterfields' => 'and',
        'group_number' => 1,
        'group_andor' => 'and',
        'field_value_to' => '',
        'compare_to_field' => 0,
        'compare_to_field_name' => '',
    );
    insertSql($table,$values);
}

function addFromToFilterField($rowwfts,$pid){
    global $db;
    $table = 'pm_process_filter_table';
    $expressions = getExpressions($rowwfts);
    //If there are two rows in expressions then filter operator is From_To else only = or !=
    $values = array();
    $values['process_id'] = $pid;
    if($db->getRowCount($expressions) == 1){
        $rowexp = $db->fetchByAssoc($expressions);
        if($rowexp['rhs_value'] == 'bool_true' && $rowexp['exp_type'] == 'bool'){
            $rowexp['rhs_value'] = 1;
        }
        if($rowexp['rhs_value'] == 'bool_false' && $rowexp['exp_type'] == 'bool'){
            $rowexp['rhs_value'] = 0;
        }
        //It is a future trigger only
        $values['field_value'] = $rowexp['rhs_value'];
        $operator = convertOperator($rowexp['operator']);
        $values['field_operator'] = $operator;
        $values['field_name'] = $rowexp['lhs_field'];
    }
    else{
        while($rowexp = $db->fetchByAssoc($expressions)){
            if($rowexp['parent_type'] == 'past_trigger'){
                $values['field_value'] = $rowexp['rhs_value'];
            }else{
                $values['field_value_to'] = $rowexp['rhs_value'];
            }
            $values['field_name'] = $rowexp['lhs_field'];
        }
        $values['field_operator'] = 'from to';
    }
    $values['id'] = create_guid();
    //$values['status'] = $rowexp['lhs_field'];
    //$values['sequence'] = $rowexp['lhs_field'];
    $values['andorfilterfields'] = 'and';
    $values['group_number'] = 1;
    $values['group_andor'] = 'and';
    $values['compare_to_field'] = 0;
    $values['compare_to_field_name'] = '';
    insertSql($table,$values);

    //Now update the Process and set to Modify only
    $newPM = BeanFactory::getBean('PM_ProcessManager', $pid);
    $newPM->start_event = 'Modify';
    $newPM->save();
}

function addTimeFilterField($rowwfts,$pid,$baseModule){
	  //Get the expression
    global $db;
    $recurring = FALSE;
    $table = 'pm_process_filter_table';
    $expressions = getExpressions($rowwfts);
    while($rowexp = $db->fetchByAssoc($expressions)){
    	  $days = convertSecondsToDays($rowexp['ext1']);
        if($rowexp['operator'] == 'Less Than'){
        	  $recurring = TRUE;
        	  //Days or Hours?
        	  if($days == 0){
        	  	$days = convertSecondsToHours($rowexp['ext1']);
        	  	$op = "time field due in x hours";
        	  	//Hourly Recurring Process
        	  	$qup = "update pm_process_defs set process_query_value = 'select id from " .$baseModule ." where deleted = 0' , process_frequency = 'Hourly', process_start_time_day = '8', process_last_run_date = '0000-00-00 00:00:00', process_start_day_of_week = NULL, process_day_of_month = NULL, process_month = NULL  where process_id = '$pid' ";
        	  }else{
            	$op = "date field due in x days";
            	//Daily Recurring Process
            	$qup = "update pm_process_defs set process_query_value = 'select id from " .$baseModule ." where deleted = 0' , process_frequency = 'Daily', process_start_time_day = '8', process_last_run_date = '0000-00-00 00:00:00', process_start_day_of_week = NULL, process_day_of_month = NULL, process_month = NULL where process_id = '$pid' ";
            }
          }
        elseif($rowexp['operator'] == 'More Than'){
        	  $recurring = TRUE;
        	  $op = "date field due in the past x days";
            //Daily Recurring Process
            $qup = "update pm_process_defs set process_query_value = 'select id from " .$baseModule ." where deleted = 0' , process_frequency = 'Daily', process_start_time_day = '8', process_last_run_date = '0000-00-00 00:00:00', process_start_day_of_week = NULL, process_day_of_month = NULL, process_month = NULL where process_id = '$pid' ";
        }
        else{
            $op = "!=";
          }
				//Now Convert the time
        $values = array(
        'id' => create_guid(),
        'process_id' => $pid,
        'status' => '',
        'field_name' => $rowexp['lhs_field'],
        'field_value' => $days,
        'field_operator' => $op,
        'sequence' => 0,
        'andorfilterfields' => 'and',
        'group_number' => 1,
        'group_andor' => 'and',
        'field_value_to' => '',
        'compare_to_field' => 0,
        'compare_to_field_name' => '',
        );
        insertSql($table,$values);
        if($recurring){
        	$db->query($qup);
        	$qp = "update pm_processmanager set start_event = 'Recurring Process' where id = '$pid'";
        	$db->query($qp);
        }
    }

}

function addFilterField($rowwfts,$pid){
    //Get the expression
    global $db;
    $table = 'pm_process_filter_table';
    $expressions = getExpressions($rowwfts);
    while($rowexp = $db->fetchByAssoc($expressions)){
        if($rowexp['operator'] == 'Equals')
            $op = "=";
        else
            $op = "!=";
        if($rowexp['rhs_value'] == 'bool_true' && $rowexp['exp_type'] == 'bool'){
            $rowexp['rhs_value'] = 1;
        }
        if($rowexp['rhs_value'] == 'bool_false' && $rowexp['exp_type'] == 'bool'){
            $rowexp['rhs_value'] = 0;
        }
        $values = array(
        'id' => create_guid(),
        'process_id' => $pid,
        'status' => '',
        'field_name' => $rowexp['lhs_field'],
        'field_value' => $rowexp['rhs_value'],
        'field_operator' => $op,
        'sequence' => 0,
        'andorfilterfields' => 'and',
        'group_number' => 1,
        'group_andor' => 'and',
        'field_value_to' => '',
        'compare_to_field' => 0,
        'compare_to_field_name' => '',
        );
        insertSql($table,$values);
    }
}

function insertSql($table,$row){
    global $db;
    $sql = "INSERT INTO $table (`" . implode('`,`', array_keys($row)) . "`) VALUES (" . implode_sql(',', array_values($row)) . ")";
    $res = $db->query($sql);
}

function getExpressions($rowwfts){
    global $db;
    $qe = "select * from expressions where parent_id = '" .$rowwfts['id'] ."' and deleted = 0";
    $resexpr = $db->query($qe);
    return $resexpr;

}

function getActionShells($pid){
    global $db;
    $qas = "select * from workflow_actionshells where parent_id = '" .$pid ."' and deleted = 0";
    $resas = $db->query($qas);
    return $resas;
}

function getAlertShells($pid){
    global $db;
    $qas = "select * from workflow_alertshells where parent_id = '" .$pid ."' and deleted = 0";
    $resas = $db->query($qas);
    return $resas;
}

function getActions($asid){
    global $db;
    $qas = "select * from workflow_actions where parent_id = '" .$asid ."' and deleted = 0";
    $resactions = $db->query($qas);
    return $resactions;
}

function getAlerts($asid){
    global $db;
    $qas = "select * from workflow_alerts where parent_id = '" .$asid ."' and deleted = 0";
    $resalerts = $db->query($qas);
    return $resalerts;
}

function getAssignedUserName($assignedID){
    global $db;
    $qt = "select user_name from users where id =  '$assignedID' and deleted = 0";
    $rest = $db->query($qt);
    $rowt= $db->fetchByAssoc($rest);
    return $rowt['user_name'];		
}

function getTeamName($teamID){
    global $db;
    $qt = "select name from teams where id =  '$teamID' and deleted = 0";
    $rest = $db->query($qt);
    $rowt= $db->fetchByAssoc($rest);
    return $rowt['name'];	
}
function implode_sql($glue, $arr){
    $str = "";
    foreach($arr as $key => $val){
        if(is_numeric($val) || is_int($val) || is_float($val)){
            $str .= $val . $glue;
        }else if(is_null($val)){
            $str .= 'NULL' . $glue;
        }else{
            $str .= '"' . $val . '"' . $glue;
        }
    }
    $str = substr($str, 0, -strlen($glue));
    return $str;
}

function getEmailAddress($focusObjectId,$focusObjectType){
    global $db;
    $newBean = convertTableToBean($focusObjectType);
    if(empty($newBean)){
        return;
    }
    $focusObjectType = $newBean->table_name;
//email_addr_bean_rel - table that holds the pointer to the new email address table
    $queryAddrBeanRel = "Select email_address_id from email_addr_bean_rel where bean_id = '$focusObjectId' and primary_address = 1 and bean_module = '$focusObjectType' and deleted = 0";
    $resultAddrBeanRel = $db->query($queryAddrBeanRel, true);
    $rowAddrBeanRel= $db->fetchByAssoc($resultAddrBeanRel);

    $emailAddressId = $rowAddrBeanRel['email_address_id'];
//Now query the email_addresses table
    $queryemail_addresses = "Select * from email_addresses where id = '$emailAddressId'";
    $result_email =& $db->query($queryemail_addresses, true);
    $row_email= $db->fetchByAssoc($result_email);
    //$toAddress = $row_email['email1'];
    return $row_email;
}

function convertTableToBean($focusObjectType){
    global $moduleList;
    global $beanList;
    $exemptModules = array();
    $exemptModules['Calendar.php'] = "Calendar.php";
    if(($focusObjectType == 'Users') || ($focusObjectType == 'users')){
        require_once("modules/Users/User.php");
        $newbean = new User();
        return $newbean;
    }
    if($focusObjectType == 'aCase'){
        require_once("modules/Cases/Case.php");
        $newbean = new aCase();
        return $newbean;
    }
    if($focusObjectType == 'activities'){
        require_once("modules/ActivityStream/Activities/Activity.php");
        $newbean = new Activity();
        return $newbean;
    }
    if($focusObjectType == 'sugarfeed'){
        require_once("modules/SugarFeed/SugarFeed.php");
        $newbean = new SugarFeed();
        return $newbean;
    }
    if($focusObjectType == 'project_task'){
        require_once("modules/ProjectTask/ProjectTask.php");
        $newbean = new ProjectTask();
        return $newbean;
    }
    foreach ($moduleList as $key=>$value){
        $module_array = array();
        $module_array[$key]=$value;
        $module_array = convert_module_to_singular($module_array);
        if($key != 0){
            $key1 = $module_array[$key];
            $bean = $key1;
            $key1 .= ".php";
            $beanFile = "modules/$value/$key1";
            if (!in_array($key1,$exemptModules)) {
                if ($bean == "Case") {
                    $bean = "aCase";
                }
                require_once("$beanFile");
                $newbean = new $bean;
                $table = $newbean->table_name;
                if ($focusObjectType == $table) {
                    return $newbean;
                }
            }
        }
    }
}