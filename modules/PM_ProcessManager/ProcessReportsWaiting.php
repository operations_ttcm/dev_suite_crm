<div id="waiting" class="pure-table">
	<h1>Stages Waiting Todo</h1>
	<table>
		<thead>
			<tr>
				<th>Actions</th>
				<th>Record Name</th>
				<th>Module</th>
				<th>Date To Run</th>
				<th>Stage (Click to Show Stages Waiting To-Do)</th>
				<th>Stage Start Delay Type</th>
			</tr>
			
		</thead>
		<tbody>
			<?php $c = 0; foreach($waiting as $row):
                //convert the start_time held in GMT DB to the users local time zone
                $startTime = $row['start_time'];
                $startTime = $td->to_display_date_time($startTime, true, true, $current_user);
			?>
				<tr class="row" class="row" <?php if($c % 2 == 0) echo "bgcolor = \"#00FF00;\""; else echo "bgcolor = \"#5e9a5e;\""; ?> >
					<td><span class="delete pure-button pure-button-xsmall" data-waiting-id="<?php echo $row['waiting_id']; ?>">Delete</span></td>
					<td><a href="index.php?module=<?php echo $row['module']; ?>&action=DetailView&record=<?php echo $row['id']; ?>" title="Go to Record" ><?php echo $row['name']; ?></a></td>
					<td><?php echo $row['module']; ?></td>
					<td><?php echo $startTime; ?></td>
					<td class="filter_by_stage" data-stage="<?php echo $row['stage_id']; ?>"><?php echo $row['stage_name']; ?></td>
					<td><?php echo $row['start_delay_type']; ?></td>
				</tr>
			<?php  $c++; endforeach; ?>
		</tbody>
	</table>
</div>
