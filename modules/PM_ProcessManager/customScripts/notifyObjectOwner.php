<?php 
if(!defined('sugarEntry'))define('sugarEntry', true);
/*********************************************************************************
 * Copyright: SierraCRM, Inc. 2007
 * Portions created by SierraCRM are Copyright (C) SierraCRM, Inc.
 * The contents of this file are subject to the SierraCRM, Inc. End User License Agreement
 * You may not use this file except in compliance with the License. 
 * You may not rent, lease, lend, or in any way distribute or transfer any rights or this file or Process Manager
 * registrations (purchased licenses) to third parties without SierraCRM, Inc. written approval, and subject to
 * agreement by the recipient of the terms of this EULA.
 * Process Manager for SugarCRM is owned by SierraCRM, Inc. and is protected by international and local copyright laws and
 * treaties. You must not remove or alter any copyright notices on any copies of Process Manager for SugarCRM. 
 * You may not use, copy, or distribute Process Manager for SugarCRM, except as granted by SierraCRM, Inc.
 * without written authorization from SierraCRM, Inc. or its designated agents. Furthermore, this Copyright notice
 * does not grant you any rights in connection with any trademarks or service marks of SierraCRM, Inc. 
 * SierraCRM, Inc. reserves all intellectual property rights, including copyrights, and trademark rights of this software.
 ********************************************************************************/
/*********************************************************************************
 *SierraCRM, Inc
 *14563 Ward Court
 *Grass Valley, CA. 95945
 *www.sierracrm.com
 ********************************************************************************/

//******************************************************************************
//Function customScript - template to assist in a custom script

file_put_contents("test.txt", "ran\n", FILE_APPEND);
	class notifyObjectOwner extends SugarBean {

	function notifyObjectOwner($focusObjectId,$focusObjectType)

	{
		//The focus object id will be the id column from the process object
		$GLOBALS['log']->info("Process Manager - Process Manager Custom Script and the focus object id is $focusObjectId");
		//The focus object type will be the database name process object
		$GLOBALS['log']->info("Process Manager - Process Manager Custom Script and the focus object type is is $focusObjectType");
	//You can now put any php code in this script to do anything you want.....
	
file_put_contents("test.txt", $focusObjectType, FILE_APPEND);
file_put_contents("test.txt", $focusObjectId, FILE_APPEND);
	}

}
?>
