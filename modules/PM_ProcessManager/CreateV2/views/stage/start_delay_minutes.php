<?php if($field == 'stage_order'): ?>
<select name="stage_order" class="stage_dropdown">
    <?php endif; ?>
<?php if($field == 'start_delay_minutes'): ?>
<select name="start_delay_minutes" class="stage_dropdown">
<?php endif; ?>
<?php if($field == 'start_delay_hours'): ?>
<select name="start_delay_hours" class="stage_dropdown">
<?php endif; ?>
<?php if($field == 'start_delay_days'): ?>
<select name="start_delay_days" class="stage_dropdown">
<?php endif; ?>
    <?php if($field == 'start_delay_months'): ?>
    <select name="start_delay_months" class="stage_dropdown">
        <?php endif; ?>
        <?php if($field == 'start_delay_years'): ?>
        <select name="start_delay_years" class="stage_dropdown">
            <?php endif; ?>
<?php foreach($options as $key => $val): ?> 
<?php $selected = ''; if($value != "" && $value == $key) $selected = "selected='selected'"; ?>
	<option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $val; ?></option>
<?php endforeach; ?>
</select>
