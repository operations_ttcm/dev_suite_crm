    <link rel="stylesheet" type="text/css" href="cache/themes/Sugar5/css/yui.css?v=RhgIjp-9sCV0qBlSzH5E4g" /><link rel="stylesheet" type="text/css" href="include/javascript/jquery/themes/base/jquery.ui.all.css" /><link rel="stylesheet" type="text/css" href="cache/themes/Sugar5/css/deprecated.css?v=RhgIjp-9sCV0qBlSzH5E4g" /><link rel="stylesheet" type="text/css" href="cache/themes/Sugar5/css/style.css?v=RhgIjp-9sCV0qBlSzH5E4g" />
    <table border="0" cellpadding="0" cellspacing="0" class="dateTime">
        <tr valign="middle">
            <td nowrap><input autocomplete="off" type="text" id="date_start_date" value="" size="11" maxlength="10" title='' tabindex="0" onblur="combo_date_start.update();" onchange="combo_date_start.update(); "> <img src="themes/Sugar5/images/jscalendar.gif?v=waTHa_N_fEWmWtL_bJrJ2w" style="position:relative; top:6px" border="0" id="date_start_trigger" alt="">&nbsp;</td>

            <td nowrap>
                <div id="date_start_time_section"></div><script type="text/javascript">
	            function set_date_start_values(form) {
	                if(form.date_start_flag.checked)  {
	                	form.date_start_flag.value=1;
	                	form.date_start.value="";
	                	form.date_start.readOnly=true;
	                } else  {
	                	form.date_start_flag.value=0;
	                	form.date_start.readOnly=false;
	                }
                }
                </script>
            </td>
        </tr>

        <tr valign="middle">
            <td nowrap><span class="dateFormat">mm/dd/yyyy</span></td>

            <td nowrap><span class="dateFormat">11:00pm</span></td>
        </tr>
    </table>

    <form>
	<input type="hidden" class="DateTimeCombo" id="date_start" class="custom_field" name="date_start" value="<?php if($value != ""): echo $value; endif; ?>"> <script type="text/javascript" src="include/SugarFields/Fields/Datetimecombo/Datetimecombo.js?v=waTHa_N_fEWmWtL_bJrJ2w">
</script> <script type="text/javascript">
var combo_date_start = new Datetimecombo("", "date_start", "11:00pm", "0", '1', false, true);
        //Render the remaining widget fields
        text = combo_date_start.html('');
        document.getElementById('date_start_time_section').innerHTML = text;

        //Call eval on the update function to handle updates to calendar picker object
        eval(combo_date_start.jsscript(''));

        //bug 47718: this causes too many addToValidates to be called, resulting in the error messages being displayed multiple times
        //    removing it here to mirror the Datetime SugarField, where the validation is not added at this level
        //addToValidate('EditView',"date_start_date",'date',false,"date_start");
        addToValidateBinaryDependency('EditView',"date_start_hours", 'alpha', false, "Missing required field: Hours" ,"date_start_date");
        addToValidateBinaryDependency('EditView', "date_start_minutes", 'alpha', false, "Missing required field: Minutes" ,"date_start_date");
        addToValidateBinaryDependency('EditView', "date_start_meridiem", 'alpha', false, "Missing required field: Meridiem","date_start_date");

        YAHOO.util.Event.onDOMReady(function()
        {

        Calendar.setup ({
        onClose : update_date_start,
        inputField : "date_start_date",
        ifFormat : "%m/%d/%Y %I:%M%P",
        daFormat : "%m/%d/%Y %I:%M%P",
        button : "date_start_trigger",
        singleClick : true,
        step : 1,
        weekNumbers: false,
        startWeekday: 0,
        comboObject: combo_date_start
        });

        //Call update for first time to round hours and minute values
        combo_date_start.update(false);

        }); 
        </script>
    </form>
    
