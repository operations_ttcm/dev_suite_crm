<?php if($multi): ?>
<select name="filter_field" class="custom_field multi_select" multiple="true">
<?php else: ?>
<select name="filter_field" class="custom_field">
<?php endif; ?>
<?php foreach($options as $key => $val): ?> 
<?php $selected = ''; if($value != "" && $value == $key) $selected = "selected='selected'"; ?>
	<option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $val; ?></option>
<?php endforeach; ?>
</select>
