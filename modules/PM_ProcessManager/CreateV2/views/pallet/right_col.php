<section id="master-pallet" class="right_col">
	<h1>Add to Process</h1>
	<ul>
	<?php foreach($theflow as $master): ?>
		<?php if($master['is_multiple'] == '1'): ?>
		<li class="master-draggable" data-val="<?php echo $master['name']; ?>" <?php if(!empty($master['child'])): ?>data-child="<?php echo $master['child']; ?>" <?php endif; ?>><img src="<?php echo $master['icon'] ?>" alt="Process Icon" class="process_icon inline" /><?php echo $master['lbl']; ?></li>
		<?php endif; ?>
	<?php endforeach; ?>
	</ul>
	<span class="help">To add a Stage or Task to the Process, drag from this panel to the main process window and drop on the green section that appears.</span>
</section>
<section id="groups-pallet" class="right_col">
	<h1>Filter Field Groups</h1>
	<ul>
	<?php foreach($groups as $i => $group): ?>
	    <?php get_view('views/_parts/group_list_item.php', array('group' => $group, 'first' => ($i == 0))) ?>
    <?php endforeach; ?>
	</ul>
	<input type="button" value="Add New Group" class="bttn center" id="add_group" />
	<span class="help">Click Add New Group to add a new group to the process conditions. Then choose the Group for each condition in your process.</span>
</section>

<section id="import-pallet" class="right_col">
	<h1>Import Process</h1>
	<form action="importProcess.php" method="post" id="fileinfo" name="fileinfo" enctype="multipart/form-data" onsubmit="return submitForm();">
        <input type="file" name="processFile" required />
        <input type="submit" value="Import" />
     </form>
     <span class="help">Import Processes via this panel</span>
</section>
