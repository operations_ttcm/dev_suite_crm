			<footer class="center">
				Copyright &copy; <?php echo date('Y'); ?>. SierraCRM Inc.
			</footer>
		</div>
		<div class="modal"><!-- Place at bottom of page --></div>
		    <script src="<?php echo base_url(); ?>js/jquery-1.9.1.min.js" type="text/javascript"></script>
		    <script src="<?php echo base_url(); ?>js/jquery-ui.min.js" type="text/javascript"></script>
		    <script src="<?php echo base_url(); ?>js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
		    <script src="<?php echo base_url(); ?>js/jquery-ui-sliderAccess.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>js/utils.js?v=2.0" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>js/events.js?v=2.0" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>js/view.js?v=2.4.2" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>js/init.js?v=2.0" type="text/javascript"></script>
        </body>
</html>
