<li class="group" data-number="<?php echo $group['number']; ?>">
    <div style="background-color: <?php echo $group['color']; ?>;" data-color="<?php echo $group['color']; ?>" class="group-color inline"></div>
    <div class="inline">
	   Group <?php echo $group['number']; ?>
	   <?php if(!$first): ?>
	    <select class="group_andor" >
		    <option value="and" <?php if($group['andor'] == 'and'): ?>selected="selected"<?php endif; ?>>And</option>
		    <option value="or" <?php if($group['andor'] == 'or'): ?>selected="selected"<?php endif; ?>>Or</option>
	    </select>
	    <?php endif; ?>
    </div>
</li>
