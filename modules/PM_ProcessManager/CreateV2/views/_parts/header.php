<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    	<title><?php echo $title; ?></title>
    	<link rel='stylesheet' href='<?php echo base_url(); ?>css/styles.css?v=2.2'/>
    	<link rel='stylesheet' href='<?php echo base_url(); ?>css/jquery-ui.css'/>
    	<link rel='stylesheet' href='<?php echo base_url(); ?>css/jquery-ui-timepicker-addon.css'/>
  	</head>
  	<body>  		
 		<div id="container">
