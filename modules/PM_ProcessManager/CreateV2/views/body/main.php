<section id="main">
	<input type="hidden" name="process_id" id="process_id" value="<?php if(isset($_GET['record'])): echo $_GET['record']; endif; ?>" />
	<div class="action-btns" style="margin-top: -12px; margin-left: 0px;">
    <input type="button" name="export" value="Export" class="export_everything bttn"/>
    <input type="button" name="duplicate" value="Duplicate" class="duplicate_everything bttn"/>
	<input type="button" name="reports" value="Reports" class="go_to_process_reports bttn"/>
	<input type="button" name="cancel" value="Cancel" class="cancel_everything bttn"/>
    <input type="button" name="delete" value="Delete" class="delete_everything bttn"/>
	<input type="button" name="save" value="Save" class="save_everything bttn"/>
	</div>
    <br>
<?php
$process_id = isset($_GET['record']) ? $_GET['record'] : create_guid();
$parent_id = mt_rand(100000000,999999999);
$id = mt_rand(10000,9999999999);
$master_id = mt_rand(10000000,9999999999);
$stage_id = create_guid();
foreach($theflow as $flow):
	$master_sugar_id = create_guid();
?>
	<div
		class="master_flow"
		data-closed="0" data-id="<?php echo $master_id; ?>"
		data-name="<?php echo $flow['name']?>"
		<?php if($flow['name'] == 'main_process'): ?>
		data-parent-id="false"
		data-sugar-id="<?php echo $process_id; ?>"
		<?php $master_sugar_id = $process_id; ?>
		<?php elseif($flow['name'] == 'stages'): ?>
		data-parent-id="<?php echo $process_id; ?>"
		data-sugar-id="<?php echo $stage_id; ?>"
		<?php $master_sugar_id = $stage_id;  ?>
		<?php elseif($flow['name'] == 'tasks'): ?>
		data-parent-id="<?php echo $stage_id; ?>"
		data-sugar-id="<?php echo $master_sugar_id; ?>"
		<?php else: ?>
		data-parent-id="<?php echo $process_id; ?>"
		data-sugar-id="<?php echo $master_sugar_id; ?>"
		<?php endif; ?>
	>
		<div class="master-header">
			<img src="<?php echo $flow['icon'] ?>" alt="Process Icon" class="process_icon inline" />
			<h1 class="inline" data-name="parent"><?php echo $flow['lbl']; ?></h1>
			<?php if(($flow['name'] == 'tasks') || ($flow['name'] == 'stages')): ?>
			<div class="expand master-item inline" data-id="<?php echo $master_id; ?>">+</div>
			<div class="collapse master-item inline" style="display: none;" data-id="<?php echo $master_id; ?>">-</div>
			<?php endif; ?>
			<div class="edit_master inline" data-id="<?php echo $master_id; ?>"></div>
		</div>
		<?php
		foreach($flow['parents'] as $fl):
		?>
            <?php if($fl['type'] == 'actions'): ?>

                <div class="actions">
                    <?php foreach($fl['actions'] as $action): ?>
                        <input type="button" onclick="javascript:PMLogic.<?php echo $action['action']; ?>(this, '<?php echo implode("','", $action['parameters']); ?>');" value="<?php echo $action['lbl']; ?>" class="action bttn_actions" />
                    <?php endforeach; ?>
                </div>

            <?php else: ?>
<?php if($fl['name'] == 'filter_fields'): ?>
	<div class="filter-fields">
	<h1 class="inline" data-name="parent"><?php echo $fl['lbl']; ?></h1>
	<div class="expandff master-item inline" style="display: none;" data-id="<?php echo $parent_id; ?>">+</div>
  <div class="collapseff master-item inline"  data-id="<?php echo $parent_id; ?>">-</div>
	</div>
	<div class="filterfields">
<?php endif; ?>

<?php if($fl['name'] == 'related_filter_fields'): ?>
	<div class="related-filter-fields">
	<h1 class="inline" data-name="parent"><?php echo $fl['lbl']; ?></h1>
	<div class="expandrff master-item inline" style="display: none;" data-id="<?php echo $parent_id; ?>">+</div>
  <div class="collapserff master-item inline"  data-id="<?php echo $parent_id; ?>">-</div>
	</div>
	<div class="relatedfilterfields">
<?php endif; ?>

<?php if($fl['name'] == 'cancel_filter_fields'): ?>
	<div class="cancel-filter-fields">
	<h1 class="inline" data-name="parent"><?php echo $fl['lbl']; ?></h1>
	<div class="expandcff master-item inline" style="display: none;" data-id="<?php echo $parent_id; ?>">+</div>
  <div class="collapsecff master-item inline"  data-id="<?php echo $parent_id; ?>">-</div>
	</div>
	<div class="cancelfilterfields">
<?php endif; ?>
			<div class="parent_flow"
				data-id="<?php echo $parent_id; ?>"
				data-name="<?php echo $fl['name']; ?>"
				data-header="<?php if($fl['static']): echo '1'; else: echo '0'; endif; ?>"
				data-closed="0"
				data-parent-id="<?php echo $master_sugar_id ?>"
				<?php if(isset($fl['is_multiple']) && $fl['is_multiple']): ?>
				data-sugar-id="<?php echo create_guid(); ?>"
				data-is-multiple="true"
				<?php else: ?>
				data-sugar-id="false"
				data-is-multiple="false"
				<?php endif; ?>
				<?php if(isset($fl['visible']) && !$fl['visible']): ?>
					style="display: none;"
				<?php endif; ?>
			>
				<?php if(!$fl['static']): ?>
					<h2><?php echo $fl['lbl']; ?></h2>
					<?php $x = 0; foreach($fl['steps'] as $step): ?>
					<?php
						$sub_name_ids[$step['name']] = $id;
					?>
					<div
						class="sub_flow droppable<?php if(isset($step['class'])): ?> <?php echo $step['class']; ?><?php endif; ?>"
						data-id="<?php echo $id; ?>"
						data-name="<?php echo $step['name']; ?>"
						data-failed-show-if="0"
						data-pallet="<?php echo (string)$step['pallet']; ?>"
						data-pallet-cb="<?php echo $step['callbacks']['pallet']; ?>"
						data-save="<?php echo $step['callbacks']['save']; ?>"
						data-load="<?php echo $step['callbacks']['load']; ?>"
						data-change="<?php echo $step['callbacks']['change']; ?>"
						data-clear-until="<?php if(isset($step['clear_until']) && $step['clear_until']): ?>1<?php else: ?>0<?php endif; ?>"

						<?php if(isset($step['visible']) && !$step['visible']): ?>
							style="display: none;"
						<?php endif; ?>
					>
						<label><?php echo $step['lbl']; ?></label>
						<span class="value"></span>
						<?php if(isset($step['show_next']) && $step['show_next']): ?>
							<span class="donebtn" style="margin-top: -25px;"><?php if(isset($value)): echo $value; else: ?>Next<?php endif; ?></span>
						<?php endif; ?>
						<span class="help" style="display: none;"><?php echo $step['hints']['lbl']; ?></span>
						<?php if(!empty($step['show_if'])): ?>
							<?php
							//	$show_if_id = show_if_sub_id($step['show_if']['name'], $sub_name_ids);
							//	$step['show_if'] = array_merge($step['show_if'], array('id' => $show_if_id));
							?>
							<span style="display: none;" class="sub_show_if"><?php echo json_encode($step['show_if']); ?></span>
						<?php endif; ?>
					</div>

					<?php if($x < (count($fl['steps'])-2)): ?>
						<div class="connector" <?php if($x > 0){echo "style='display: none;'"; } ?> ></div>
					<?php endif; ?>
					<?php $id++; $x++; endforeach; ?>

				<?php else: ?>
					<h2><?php echo $fl['lbl']; ?></h2>
					<?php echo $fl['customCode']; ?>
				<?php endif; ?>
				<?php if(is_array($fl['multiple'])): ?>
					<span class="next_options" data-id="<?php echo mt_rand(10000000,9999999999); ?>" style="display: none;">
						<?php foreach($fl['multiple'] as $button): ?>
							<input type="button" name="<?php echo $button['name']; ?>" onclick="javascript:PMLogic.<?php echo $button['function']; ?>(this, <?php echo $parent_id; ?>);" value="<?php echo $button['lbl']; ?>" class="next_option bttn" />
						<?php endforeach; ?>
					</span>
				<?php endif; ?>

				<?php if(is_array($fl['switch'])): ?>
					<div class="switch">
						<?php foreach($fl['switch'] as $button): ?>
						    <?php if(empty($button['customCode'])): ?>
							<div
								data-name="<?php echo $button['name']; ?>"
								onclick="javscript:PMLogic.<?php echo $button['function']; ?>(this, <?php echo $parent_id; ?>);"
								class="switch_option"
								data-selected="false"
								data-group-name="<?php echo $button['group_name']?>"
								>
									<?php echo $button['lbl']; ?>
							</div>
							<?php else: ?>
				            <div class="switch_option">
				                <?php echo $button['customCode']; ?>
				            </div>
							<?php endif; ?>
						<?php endforeach; ?>
					</div>
				<?php if($fl['name'] == 'filter_fields'): ?>
					</div>			
				<?php endif; ?>	
				<?php if($fl['name'] == 'related_filter_fields'): ?>
					</div>			
				<?php endif; ?>	
				<?php if($fl['name'] == 'cancel_filter_fields'): ?>
					</div>			
				<?php endif; ?>										
				<?php endif; ?>
				<?php if(isset($fl['empty_options']) && is_array($fl['empty_options'])): ?>
					<div class="empty_options" style="display: none;">
						<?php foreach($fl['empty_options'] as $option): ?>
							<input type="button" name="<?php echo $option['name']; ?>" onclick="javascript:PMLogic.<?php echo $option['function']; ?>(this);" value="<?php echo $option['lbl']; ?>" class="next_option bttn" />
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
				<?php if(!empty($fl['show_if'])): ?>
						<?php
						//	$show_if_id = show_if_sub_id($fl['show_if']['name'], $sub_name_ids);
						//	$fl['show_if'] = array_merge($fl['show_if'], array('id' => $show_if_id));
						?>
				<span style="display: none;" class="show_if"><?php echo json_encode($fl['show_if']); ?></span>
				<?php endif; ?>
			</div>
            <?php endif; ?>
		<?php $parent_id++; endforeach; ?>
		<?php if($flow['name'] != 'main_process'): ?>
		<input type="button" name="delete_master" value="Delete <?php echo $flow['lbl']?>" data-name="<?php echo $flow['name']?>" class="delete_master bttn"/>
		<?php endif; ?>
		<?php if($flow['is_multiple'] == '1' && !empty($flow['parent'])): ?>
			<div class="master-droppable" data-name="<?php echo $flow['name']?>" ></div>
		<?php endif; ?>
		</div>
	<?php $master_id++; endforeach; ?>
	<?php foreach($theflow as $flow): ?>
		<?php if($flow['is_multiple'] == '1' && empty($flow['parent'])): ?>
			<div class="master-droppable" data-name="<?php echo $flow['name']?>" ></div>
		<?php endif; ?>
	<?php endforeach; ?>
	<div class="action-btns" style="top: 12px; position: relative;"><input type="button" name="cancel" value="Cancel" class="cancel_everything bttn"/><input type="button" name="save" value="Save" class="save_everything bttn"/></div>
</section>
