<?php
/*********************************************************************************
 * Copyright: SierraCRM, Inc. 2014
 * Portions created by SierraCRM are Copyright (C) SierraCRM, Inc.
 * The contents of this file are subject to the SierraCRM, Inc. End User License Agreement
 * You may not use this file except in compliance with the License.
 * You may not rent, lease, lend, or in any way distribute or transfer any rights or this file or Process Manager
 * registrations (purchased licenses) to third parties without SierraCRM, Inc. written approval, and subject to
 * agreement by the recipient of the terms of this EULA.
 * Process Manager for SugarCRM is owned by SierraCRM, Inc. and is protected by international and local copyright laws and
 * treaties. You must not remove or alter any copyright notices on any copies of Process Manager for SugarCRM.
 * You may not use, copy, or distribute Process Manager for SugarCRM, except as granted by SierraCRM, Inc.
 * without written authorization from SierraCRM, Inc. or its designated agents. Furthermore, this Copyright notice
 * does not grant you any rights in connection with any trademarks or service marks of SierraCRM, Inc.
 * SierraCRM, Inc. reserves all intellectual property rights, including copyrights, and trademark rights of this software.
 ********************************************************************************/
/*********************************************************************************
 *SierraCRM, Inc
 *14563 Ward Court
 *Grass Valley, CA. 95945
 *www.sierracrm.com
 ********************************************************************************/

//The Main Flow
$theflow = array(
    array(
        'name' => 'main_process',
        'lbl' => 'LBL_PROCESS',
        'icon' => base_url() . 'imgs/process_manager_color_48.png',
        'parents' => array(
            array(
                'lbl' => '', //Sugar Module Label
                'name' => 'overview', //Unique Name - All Lower Case
                'static' => true, // Determines whether or not to use the customCode
                'visible' => true, // Default visible
                'multiple' => false,
                'switch' => false,
                'customCode' => '
					<div class="col1">
						<input type="text" id="name" name="name" value="{focus.name}" placeholder="{LBL_CREATEV2_PROCESS_NAME}"/>
						<select name="active" id="active">
							<option value="Active">Active</option>
							<option value="Inactive">Inactive</option>
						</select>
						<input type="checkbox" value="1" name="ignore_check" id="ignore_check" />
						<label id="ignore_check_label"><b>Ignore Check for Previously Run Process</b></label>
					</div>
					<div class="col2">
						<textarea id="description" name="description" placeholder="{LBL_CREATEV2_PROCESS_DESC}">{focus.desc}</textarea>
					</div>
				'
            ),
            array(
                'lbl' => 'LBL_CREATEV2_PROCESS', //Sugar Module Label
                'name' => 'process', //Unique Name - All Lower Case
                'static' => false,
                'visible' => true,
                'multiple' => false,
                'switch' => false,
                'steps' => array( //Sub Flows for the Parent Flow
                    array(
                        'lbl' => 'LBL_CREATEV2_OBJECT', //Sugar Module Label
                        'name' => 'module', //Unique Name - All Lower Case
                        'visible' => true,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => NULL, // JS Callbacks on Load @TODO
                            'change' => NULL, // JS Callbacks on Change @TODO
                            'pallet' => 'get_objects', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name', //PHP Function to Save Options
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_OBJECT_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATEV2_START_EVENT',
                        'name' => 'start_event',
                        'visible' => false,
                        'pallet' => '1',
                        'callbacks' => array(
                            'load' => NULL,
                            'change' => NULL,
                            'pallet' => 'get_start_events',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_START_EVENT_HELP'
                        )
                    ),
                ),
            ),
            array(
                'lbl' => 'LBL_GENERIC_HELP', //Sugar Module Label
                'name' => '', //Unique Name - All Lower Case
                'static' => false,
                'visible' => false,
                'switch' => false,
                'pallet' => '0',
                'show_if' => array(
                    'operator' => 'equal_to', // Operator To Compare
                    'name' => 'start_event', //Sub Flow Name
                    'value' => 'Create' // Sub Flow Value
                ),

            ),
            array(
                'lbl' => 'LBL_CREATEV2_RECURRING_PROCESS', //Sugar Module Label
                'name' => 'recurring_process', //Unique Name - All Lower Case
                'static' => false,
                'visible' => false,
                'switch' => false,
                'show_if' => array(
                    'operator' => 'equal_to', // Operator To Compare
                    'name' => 'start_event', //Sub Flow Name
                    'value' => 'Recurring Process' // Sub Flow Value
                ),
                'multiple' => false,
                'steps' => array( //Sub Flows for the Parent Flow
                    array(
                        'lbl' => 'LBL_PROCESS_QUERY', //Sugar Module Label
                        'name' => 'process_query', //Unique Name - All Lower Case
                        'visible' => true,
                        'pallet' => '0',
                        'class' => 'fluid',
                        'callbacks' => array(
                            'load' => 'get_process_query', //PHP Function to get Pallet Options
                            'change' => 'get_process_query',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_PROCESS_QUERY_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_PROCESS_FREQUENCY',
                        'name' => 'process_frequency',
                        'visible' => false,
                        //'class' => 'large',
                        'pallet' => '1',
                        'callbacks' => array(
                            'pallet' => 'get_process_frequency',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_PROCESS_FREQUENCY_HELP'
                        )
                    ),

                    array(
                        'lbl' => 'LBL_PROCESS_START_DAY_OF_WEEK',
                        'name' => 'day_to_run',
                        'visible' => false,
                        'pallet' => '1',
                        'show_if' => array(
                            'operator' => 'equal_to', // Operator To Compare
                            'name' => 'process_frequency', //Sub Flow Name
                            'value' => 'Weekly' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'pallet' => 'get_day_to_run',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_PROCESS_START_DAY_OF_WEEK_HELP'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_PROCESS_DAY_OF_MONTH',
                        'name' => 'process_day_of_month',
                        'visible' => false,
                        'pallet' => '1',
                        'show_if' => array(
                            'operator' => 'equal_to', // Operator To Compare
                            'name' => 'process_frequency', //Sub Flow Name
                            'value' => 'Monthly' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'pallet' => 'get_process_day_of_month',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_PROCESS_DAY_OF_MONTH_HELP'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_PROCESS_MONTH',
                        'name' => 'process_month',
                        'visible' => false,
                        'pallet' => '1',
                        'show_if' => array(
                            'operator' => 'equal_to', // Operator To Compare
                            'name' => 'process_frequency', //Sub Flow Name
                            'value' => 'Yearly' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'pallet' => 'get_process_month',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_PROCESS_MONTH_HELP'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_DAILY_START_TIME',
                        'name' => 'daily_start_time',
                        'visible' => false,
                        'pallet' => '1',
							'show_if' => array(
									'operator' => 'equal_to', // Operator To Compare
									'name' => 'process_frequency', //Sub Flow Name
									'value' => 'Daily||Weekly||Monthly||Yearly' // Sub Flow Value
							),
                        'callbacks' => array(
                            'pallet' => 'get_daily_start_time',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_DAILY_START_TIME_HELP'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATEV2_LAST_RUN_DATE',
                        'name' => 'last_run_date',
                        //'class' => 'large',
                        'visible' => false,
                        'pallet' => '0',
                        'show_if' => array(
                            'operator' => 'equal_to', // Operator To Compare
                            'name' => 'start_event', //Sub Flow Name
                            'value' => 'Recurring Process' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => 'get_datetime_field',
                            'change' => 'get_datetime_field',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_START_EVENT_HELP'
                        )
                    ),
                ),
            ),
            array(
                'lbl' => '',
                'name' => 'process_actions',
                'type' => 'actions',
                'actions' => array(
                    array(
                        'action' => 'go_to_parent',
                        'parameters' => array(
                            'filter_fields'
                        ),
                        'lbl' => 'LBL_CREATEV2_ADD_PROCESS_FILTERS'
                    ),
                    array(
                        'action' => 'go_to_parent',
                        'parameters' => array(
                            'related_filter_fields'
                        ),
                        'lbl' => 'LBL_CREATEV2_ADD_RELATED_PROCESS_FILTERS'
                    ),
                    array(
                        'action' => 'go_to_parent',
                        'parameters' => array(
                            'cancel_filter_fields'
                        ),
                        'lbl' => 'LBL_CREATEV2_ADD_CANCEL_EVENT'
                    ),
                    array(
                        'action' => 'go_to_master',
                        'parameters' => array(
                            'stages'
                        ),
                        'lbl' => 'LBL_GOTOSTAGE'
                    )
                )
            ),
            array(
                'lbl' => 'LBL_CREATEV2_PROCESS_FILTERS', //Sugar Module Label
                /*
                'show_if' => array(
                    'operator' => 'not_equal_to', // Operator To Compare
                    'name' => 'start_event', //Sub Flow Name
                    'value' => 'Recurring Process' // Sub Flow Value
                ),
                */
                'name' => 'filter_fields', //Unique Name - All Lower Case
                'static' => false,
                'visible' => false,
                'is_child' => true,
                'is_multiple' => true,
                'multiple_id' => 1,
                'multiple' => array( // Buttons Array

                ),

                'switch' => array( // Buttons Array
                    array(
                        'customCode' => '<select class="filter-field-group" id="0322" data-sugar-id=""><option value="">[New Group]</option></select>',
                    ),
                    array(
                        'lbl' => 'LBL_CREATEV2_AND', // Sugar Module Label
                        'name' => 'and',
                        'group_name' => 'process_filter_andors',
                        'function' => 'select_switch' // JS Function
                    ),
                    array(
                        'lbl' => 'LBL_CREATEV2_OR',
                        'name' => 'or',
                        'group_name' => 'process_filter_andors',
                        'function' => 'select_switch'
                    ),
                    array(
                        'lbl' => 'LBL_REMOVE',
                        'name' => 'remove_previous_parent',
                        'group_name' => 'remove_previous_parent',
                        'function' => 'remove_previous_parent'
                    ),
                ),
                'steps' => array( //Sub Flows for the Parent Flow
                    array(
                        'lbl' => 'LBL_CREATEV2_FILTER_FIELD', //Sugar Module Label
                        'name' => 'filter_field', //Unique Name - All Lower Case
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_filter_fields', // JS Callbacks on Load @TODO
                            'change' => 'get_filter_fields', // JS Callbacks on Change @TODO
                            'pallet' => 'get_filter_fields', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_FILTER_FIELD_HELP'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATEV2_OPERATOR',
                        'name' => 'filter_operator',
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' =>'get_filter_operators',
                            'change' => 'get_filter_operators',
                            'pallet' => 'get_filter_operators',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_OPERATOR_HELP'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATEV2_COMPARE_TO_FIELD',
                        'name' => 'compare_to_field',
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_checkbox_next',
                            'change' => 'get_checkbox_next',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_COMPARE_TO_FIELD_HELP'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATEV2_FILTER_FIELD', //Sugar Module Label
                        'name' => 'compare_to_field_name', //Unique Name - All Lower Case
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'show_if' => array(
                            'operator' => 'equal_to', // Operator To Compare
                            'name' => 'compare_to_field', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => 'get_filter_fields', // JS Callbacks on Load @TODO
                            'change' => 'get_filter_fields', // JS Callbacks on Change @TODO
                            'pallet' => 'get_filter_fields', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_FILTER_FIELD_HELP'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATEV2_FIELD_VALUES',
                        'name' => 'filter_field_values',
                        'pallet' => '0',
                        'visible' => false,
                        'class' => 'fluid',
                        'show_if' => array(
                            'operator' => 'equal_to', // Operator To Compare
                            'name' => 'filter_operator', //Sub Flow Name
                            'check_compare_to' => TRUE, //Sub Flow Name
                            'value' => '&gt;||&gt=;||&lt;||&lt=;||=||!=||<||<=||>||>=||contains||does not contain||date field due in x days||time field due in x hours||date field due in the past x days||from to||date is less than||date is greater than' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => 'get_filter_field_data',
                            'change' => 'get_filter_field_data',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_FIELD_VALUES_HELP'
                        )
                    ),
                		array(
                				'lbl' => 'LBL_CREATEV2_FIELD_VALUES',
                				'name' => 'filter_field_values_from_to',
                				'pallet' => '0',
                				'visible' => false,
                				'class' => 'large',
                				'show_if' => array(
                						'operator' => 'equal_to', // Operator To Compare
                						'name' => 'filter_operator', //Sub Flow Name
                						'value' => 'from to' // Sub Flow Value
                				),
                				'callbacks' => array(
                						'load' => 'get_filter_field_data',
                						'change' => 'get_filter_field_data',
                						'pallet' => NULL,
                						'save' => NULL,
                				),
                				'hints' => array(
                						'lbl' => 'LBL_CREATEV2_FIELD_VALUES_HELP'
                				)
                		),
                ),
            ),
            array(
                'lbl' => 'LBL_CREATEV2_RELATED_FILTERS', //Sugar Module Label
                /*
                'show_if' => array(
                    'operator' => 'not_equal_to', // Operator To Compare
                    'name' => 'start_event', //Sub Flow Name
                    'value' => 'Recurring Process' // Sub Flow Value
                ),
                */
                'name' => 'related_filter_fields', //Unique Name - All Lower Case
                'static' => false,
                'visible' => false,
                'is_child' => true,
                'is_multiple' => true,
                'multiple_id' => 554,
                /*
                'multiple' => array( // Buttons Array
                    array(
                        'lbl' => 'LBL_ADD_CANCEL_EVENT',
                        'name' => 'continue',
                        'function' => 'go_to_next'
                    ),
                ),
                */
                'switch' => array( // Buttons Array
                    array(
                        'lbl' => 'LBL_CREATEV2_AND', // Sugar Module Label
                        'name' => 'and',
                        'group_name' => 'related_process_filter_andors',
                        'function' => 'select_switch' // JS Function
                    ),
                    array(
                        'lbl' => 'LBL_CREATEV2_OR',
                        'name' => 'or',
                        'group_name' => 'related_process_filter_andors',
                        'function' => 'select_switch'
                    ),
                    array(
                        'lbl' => 'LBL_REMOVE',
                        'name' => 'remove_previous_parent',
                        'group_name' => 'remove_previous_parent',
                        'function' => 'remove_previous_parent'
                    ),
                ),
                'steps' => array( //Sub Flows for the Parent Flow
                    array(
                        'lbl' => 'LBL_CREATEV2_RELATED_MODULES', //Sugar Module Label
                        'name' => 'related_module', //Unique Name - All Lower Case
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_related_modules', // JS Callbacks on Load @TODO
                            'change' => 'get_related_modules', // JS Callbacks on Change @TODO
                            'pallet' => 'get_related_modules', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_RELATED_MODULE_HELP'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATEV2_FILTER_FIELD', //Sugar Module Label
                        'name' => 'related_filter_field', //Unique Name - All Lower Case
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_related_fields', // JS Callbacks on Load @TODO
                            'change' => 'get_related_fields', // JS Callbacks on Change @TODO
                            'pallet' => 'get_related_fields', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_FILTER_FIELD_HELP'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATEV2_OPERATOR',
                        'name' => 'related_filter_operator',
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_filter_operators',
                            'change' => 'get_filter_operators',
                            'pallet' => 'get_filter_operators',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_OPERATOR_HELP'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATEV2_FIELD_VALUES',
                        'name' => 'related_filter_field_values',
                        'pallet' => '0',
                        'visible' => false,
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'equal_to', // Operator To Compare
                            'name' => 'related_filter_operator', //Sub Flow Name
                            'check_compare_to' => FALSE, //Sub Flow Name
                            'value' => '&gt;||&gt=;||&lt;||&lt=;||=||!=||<||<=||>||>=||contains||does not contain||date field due in x days||time field due in x hours||date field due in the past x days' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => 'get_related_filter_field_data',
                            'change' => 'get_related_filter_field_data',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_FIELD_VALUES_HELP'
                        )
                    ),
                ),
            ),
            array(
                'lbl' => 'LBL_PROCESS_CANCEL_EVENT', //Sugar Module Label
                'name' => 'cancel_filter_fields', //Unique Name - All Lower Case
                'static' => false,
                'visible' => false,
                'is_child' => true,
                'is_multiple' => true,
                'multiple_id' => 654,

                'switch' => array( // Buttons Array
                    array(
                        'lbl' => 'LBL_CREATEV2_AND', // Sugar Module Label
                        'name' => 'and',
                        'group_name' => 'cancel_process_filter_andors',
                        'function' => 'select_switch' // JS Function
                    ),
                    array(
                        'lbl' => 'LBL_CREATEV2_OR',
                        'name' => 'or',
                        'group_name' => 'cancel_process_filter_andors',
                        'function' => 'select_switch'
                    ),
                    array(
                        'lbl' => 'LBL_REMOVE',
                        'name' => 'remove_previous_parent',
                        'group_name' => 'remove_previous_parent',
                        'function' => 'remove_previous_parent'
                    ),
                ),
                'steps' => array( //Sub Flows for the Parent Flow

                    array(
                        'lbl' => 'LBL_CREATEV2_FILTER_FIELD', //Sugar Module Label
                        'name' => 'cancel_filter_field', //Unique Name - All Lower Case
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_filter_fields', // JS Callbacks on Load @TODO
                            'change' => 'get_filter_fields', // JS Callbacks on Change @TODO
                            'pallet' => 'get_filter_fields', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_FILTER_FIELD_HELP'
                        )
                    ),

                    array(
                        'lbl' => 'LBL_CREATEV2_OPERATOR',
                        'name' => 'cancel_filter_operator',
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' =>'get_filter_operators',
                            'change' => 'get_filter_operators',
                            'pallet' => 'get_filter_operators',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_OPERATOR_HELP'
                        )
                    ),

                    array(
                        'lbl' => 'LBL_CREATEV2_COMPARE_TO_FIELD',
                        'name' => 'cancel_compare_to_field',
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_checkbox_next',
                            'change' => 'get_checkbox_next',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_COMPARE_TO_FIELD_HELP'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATEV2_FILTER_FIELD', //Sugar Module Label
                        'name' => 'cancel_compare_to_field_name', //Unique Name - All Lower Case
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'show_if' => array(
                            'operator' => 'equal_to', // Operator To Compare
                            'name' => 'cancel_compare_to_field', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => 'get_filter_fields', // JS Callbacks on Load @TODO
                            'change' => 'get_filter_fields', // JS Callbacks on Change @TODO
                            'pallet' => 'get_filter_fields', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_FILTER_FIELD_HELP'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATEV2_FIELD_VALUES',
                        'name' => 'cancel_filter_field_values',
                        'pallet' => '0',
                        'visible' => false,
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'equal_to', // Operator To Compare
                            'name' => 'cancel_filter_operator', //Sub Flow Name
                            'check_compare_to' => TRUE, //Sub Flow Name
                            'value' => '&gt;||&gt=;||&lt;||&lt=;||=||!=||<||<=||>||>=||contains||does not contain||date field due in x days||time field due in x hours||date field due in the past x days||from to' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => 'get_cancel_filter_field_data',
                            'change' => 'get_cancel_filter_field_data',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_FIELD_VALUES_HELP'
                        )
                    ),
                		array(
                				'lbl' => 'LBL_CREATEV2_FIELD_VALUES',
                				'name' => 'cancel_filter_field_values_from_to',
                				'pallet' => '0',
                				'visible' => false,
                				'class' => 'large',
                				'show_if' => array(
                						'operator' => 'equal_to', // Operator To Compare
                						'name' => 'cancel_filter_operator', //Sub Flow Name
                						'value' => 'from to' // Sub Flow Value
                				),
                				'callbacks' => array(
                						'load' => 'get_cancel_filter_field_data',
                						'change' => 'get_cancel_filter_field_data',
                						'pallet' => NULL,
                						'save' => NULL,
                				),
                				'hints' => array(
                						'lbl' => 'LBL_CREATEV2_FIELD_VALUES_HELP'
                				)
                		),
                ),

            ),
        ),
    ),
    array(
        'name' => 'stages',
        'lbl' => 'LBL_STAGE',
        'is_multiple' => '1', // This will make this master flow show in the right hand column pallet.
        'child' => 'tasks',
        'icon' => base_url() . 'imgs/stage_color_48.png',
        'parents' => array(
            array(
                'lbl' => 'LBL_STAGE', //Sugar Module Label
                'name' => 'stage', //Unique Name - All Lower Case
                'static' => false,
                'visible' => false,
                'multiple_id' => 99,
                'switch' => false,
                'steps' => array( //Sub Flows for the Parent Flow
                    array(
                        'lbl' => 'LBL_STAGE_NAME', //Sugar Module Label
                        'name' => 'stage_name', //Unique Name - All Lower Case
                        'visible' => false,
                        'pallet' => '0',
                        'class' => 'large',
                        //When Pallet = 0 we just use two load and change for the callback - there are javascript function
                        //Therfore the load function lives in view.js
                        'callbacks' => array(
                            'load' => 'get_stage_name', //PHP Function to get Pallet Options
                            'change' => 'get_stage_name', // THis change will call this function
                            //'pallet' => 'get_stage_name', //PHP Function to get Pallet Options
                            //'save' => 'get_stage_name', //PHP Function to Save Options
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_STAGE_NAME_HINTS'
                        )
                    ),

                    array(
                        'lbl' => 'LBL_STAGE_ORDER', //Sugar Module Label
                        'name' => 'stage_order', //Unique Name - All Lower Case
                        'visible' => false,
                        'pallet' => '0',
                        'show_next' => true,
                        //'class' => 'large',
                        //When Pallet = 0 we just use two load and change for the callback - there are javascript function
                        //Therfore the load function lives in view.js
                        'callbacks' => array(
                            'load' => 'get_stage_order', //PHP Function to get Pallet Options
                            'change' => 'get_stage_order', // THis change will call this function
                            //'pallet' => 'get_stage_name', //PHP Function to get Pallet Options
                            //'save' => 'get_stage_name', //PHP Function to Save Options
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_STAGE_ORDER_HINTS'
                        )
                    ),

                    //First Drop Down List Stages
                    array(
                        'lbl' => 'LBL_START_DELAY_MINUTES', //Sugar Module Label
                        'name' => 'start_delay_minutes', //Unique Name - All Lower Case
                        'pallet' => '1',
                        'visible' => false,
                        //'class' => 'large',
                        'show_next' => true,
                        'callbacks' => array(
                            'load' => 'get_start_delay_minutes', // JS Callbacks on Load @TODO
                            'change' => 'get_start_delay_minutes', // JS Callbacks on Change @TODO
                            'pallet' => NULL, //PHP Function to get Pallet Options
                            'save' => NULL, //PHP Function to Save Options
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_STAGE_DELAY_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_START_DELAY_HOURS', //Sugar Module Label
                        'name' => 'start_delay_hours', //Unique Name - All Lower Case
                        'pallet' => '1',
                        'visible' => false,
                        //'class' => 'large',
                        'show_next' => true,
                        'callbacks' => array(
                            'load' => 'get_start_delay_hours', // JS Callbacks on Load @TODO
                            'change' => 'get_start_delay_hours', // JS Callbacks on Change @TODO
                            'pallet' => NULL, //PHP Function to get Pallet Options
                            'save' => NULL, //PHP Function to Save Options
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_STAGE_DELAY_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_START_DELAY_DAYS', //Sugar Module Label
                        'name' => 'start_delay_days', //Unique Name - All Lower Case
                        'pallet' => '1',
                        'visible' => false,
                        //'class' => 'large',
                        'show_next' => true,
                        'callbacks' => array(
                            'load' => 'get_start_delay_days', // JS Callbacks on Load @TODO
                            'change' => 'get_start_delay_days', // JS Callbacks on Change @TODO
                            'pallet' => NULL, //PHP Function to get Pallet Options
                            'save' => NULL, //PHP Function to Save Options
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_STAGE_DELAY_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_START_DELAY_MONTHS', //Sugar Module Label
                        'name' => 'start_delay_months', //Unique Name - All Lower Case
                        'pallet' => '1',
                        'visible' => false,
                        //'class' => 'large',
                        'show_next' => true,
                        'callbacks' => array(
                            'load' => 'get_start_delay_months', // JS Callbacks on Load @TODO
                            'change' => 'get_start_delay_months', // JS Callbacks on Change @TODO
                            'pallet' => NULL, //PHP Function to get Pallet Options
                            'save' => NULL, //PHP Function to Save Options
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_STAGE_DELAY_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_START_DELAY_YEARS', //Sugar Module Label
                        'name' => 'start_delay_years', //Unique Name - All Lower Case
                        'pallet' => '1',
                        'visible' => false,
                        //'class' => 'large',
                        'show_next' => true,
                        'callbacks' => array(
                            'load' => 'get_start_delay_years', // JS Callbacks on Load @TODO
                            'change' => 'get_start_delay_years', // JS Callbacks on Change @TODO
                            'pallet' => NULL, //PHP Function to get Pallet Options
                            'save' => NULL, //PHP Function to Save Options
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_STAGE_DELAY_HINTS'
                        )
                    ),
                ),
            ),
        ),
    ),

    array(
        'name' => 'tasks',
        'lbl' => 'LBL_PROCESS_TASKS',
        'parent' => 'stage',
        'icon' => base_url() . 'imgs/action_stage_task_color_48.png',
        'is_multiple' => '1', // This will make this master flow show in the right hand column pallet.
        'parents' => array(
            array(
                'lbl' => 'LBL_PROCESS_TASK', //Sugar Module Label
                'name' => 'task', //Unique Name - All Lower Case
                'static' => false,
                'visible' => false,
                'multiple' => false,
                'switch' => false,
                'steps' => array( //Sub Flows for the Parent Flow
                    array(
                        'lbl' => 'LBL_TASK_NAME', //Sugar Module Label
                        'name' => 'task_name', //Unique Name - All Lower Case
                        'visible' => false,
                        'pallet' => '0',
                        'class' => 'large',
                        //When Pallet = 0 we just use two load and change for the callback - there are javascript function
                        //Therfore the load function lives in view.js
                        'callbacks' => array(
                            'load' => 'get_task_name', //PHP Function to get Pallet Options
                            'change' => 'get_task_name', // THis change will call this function
                            //'pallet' => 'get_stage_name', //PHP Function to get Pallet Options
                            //'save' => 'get_stage_name', //PHP Function to Save Options
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_TASK_NAME_HINTS'
                        )
                    ),
                    //First Drop Down Task Type
                    array(
                        'lbl' => 'LBL_TASK_TYPE', //Sugar Module Label
                        'name' => 'task_type', //Unique Name - All Lower Case
                        'visible' => false,
                        //This just says grab the pallet
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => NULL, // JS Callbacks on Load @TODO
                            'change' => NULL, // JS Callbacks on Change @TODO
                            //This next function calls an ajax function to get the function and method in
                            //ajax_start.php - this get_text_field is referenced in this file as a function
                            //So we create a function if not already available for an array that represents
                            'pallet' => 'get_task_type', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name', //PHP Function to Save Options
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_TASK_TYPE_HINTS'
                        )
                    ),

                ),
            ),
            //Now here is each of the Tasks
            array(
                'lbl' => 'LBL_SEND_EMAIL', //Sugar Module Label
                'name' => 'send_email', //Unique Name - All Lower Case
                'static' => false,
                'visible' => false,
                'multiple' => false,
                'switch' => false,
                'clear_until' => true,
                'show_if' => array(
                    'operator' => 'equal_to', // Operator To Compare
                    'name' => 'task_type', //Sub Flow Name
                    'value' => 'Send Email' // Sub Flow Value
                ),
                'steps' => array( //Sub Flows for the Parent Flow
                    array(
                        'lbl' => 'LBL_SEND_EMAIL_TO_EMAIL_QUEUE',
                        'name' => 'send_email_to_email_queue',
                        'visible' => false,
                        'pallet' => '0',
                        'class' => 'large',
                        'callbacks' => array(
                            'load' => 'get_checkbox_next',
                            'change' => 'get_checkbox_next',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_TASK_SEND_EMAIL_TO_EMAIL_QUEUE_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_EMAIL_QUEUE_CAMPAIGN_NAME',
                        'name' => 'email_queue_campaign_name',
                        'visible' => false,
                        'pallet' => '1',
                        'class' => 'large',
                        'show_next' => true,
                        'show_if' => array(
                            'operator' => 'equal_to', // Operator To Compare
                            'name' => 'send_email_to_email_queue', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => NULL,
                            'change' => NULL,
                            'pallet' => 'get_email_queue_campiagn_name',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_TASK_SEND_EMAIL_TO_CAMPAIGN_HINTS'
                        )
                    ),

                    array(
                        'lbl' => 'LBL_CHOOSE_EMAIL_TEMPLATE', //Sugar Module Label
                        'name' => 'email_template', //Unique Name - All Lower Case
                        'visible' => false,
                        //This just says grab the pallet
                        'pallet' => '1',
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'not_equal_to', // Operator To Compare
                            'name' => 'send_email_to_email_queue', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => NULL, // JS Callbacks on Load @TODO
                            'change' => NULL, // JS Callbacks on Change @TODO
                            //This next function calls an ajax function to get the function and method in
                            //ajax_start.php - this get_text_field is referenced in this file as a function
                            //So we create a function if not already available for an array that represents
                            'pallet' => 'getEmailTemplates', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name', //PHP Function to Save Options
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_TASK_EMAIL_TEMPLATES_HINTS'
                        )
                    ),

                    array(
                        'lbl' => 'LBL_ALTERNATE_EMAIL',
                        'name' => 'alternate_email',
                        'visible' => false,
                        'pallet' => '0',
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'not_equal_to', // Operator To Compare
                            'name' => 'send_email_to_email_queue', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => 'get_checkbox_next',
                            'change' => 'get_checkbox_next',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_TASK_ALTERNATE_EMAIL_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_ALTERNATE_EMAIL_ADDRESS',
                        'name' => 'alternate_email_address',
                        'visible' => false,
                        'pallet' => '0',
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'not_equal_to', // Operator To Compare
                            'name' => 'send_email_to_email_queue', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'show_if' => array(
                            'operator' => 'equal_to', // Operator To Compare
                            'name' => 'alternate_email', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => 'get_text_field',
                            'change' => 'get_text_field',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_ALTERNATE_EMAIL_ADDRESS_HINTS'
                        )
                    ),

                    array(
                        'lbl' => 'LBL_CC_EMAIL_ADDRESS',
                        'name' => 'cc_email_to_address',
                        'visible' => false,
                        'pallet' => '0',
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'not_equal_to', // Operator To Compare
                            'name' => 'send_email_to_email_queue', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => 'get_text_field',
                            'change' => 'get_text_field',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_CC_EMAIL_ADDRESS_HINTS'
                        )
                    ),

                    array(
                        'lbl' => 'LBL_SEND_EMAIL_TO_OBJECT_OWNER',
                        'name' => 'send_email_to_object_owner',
                        'visible' => false,
                        'pallet' => '0',
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'not_equal_to', // Operator To Compare
                            'name' => 'send_email_to_email_queue', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => 'get_checkbox_next',
                            'change' => 'get_checkbox_next',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_TASK_SEND_EMAIL_OBjECT_OWNER_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_SEND_EMAIL_OTHER_OWNER', //Sugar Module Label
                        'name' => 'send_email_other_owner', //Unique Name - All Lower Case
                        'visible' => false,
                        'pallet' => '1',
                        'show_next' => true,
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'not_equal_to', // Operator To Compare
                            'name' => 'send_email_to_email_queue', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => NULL, // JS Callbacks on Load @TODO
                            'change' => NULL, // JS Callbacks on Change @TODO
                            'pallet' => 'get_email_other_owners', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name', //PHP Function to Save Options
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_TASK_TYPE_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_SEND_EMAIL_TO_ROLE', //Sugar Module Label
                        'name' => 'send_email_to_role', //Unique Name - All Lower Case
                        'visible' => false,
                        'pallet' => '1',
                        'show_next' => true,
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'not_equal_to', // Operator To Compare
                            'name' => 'send_email_to_email_queue', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => NULL, // JS Callbacks on Load @TODO
                            'change' => NULL,// JS Callbacks on Change @TODO
                            'pallet' => 'get_all_sugar_roles', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name', //PHP Function to Save Options
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_TASK_TYPE_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_SEND_EMAIL_TO_TEAM', //Sugar Module Label
                        'name' => 'send_email_to_team', //Unique Name - All Lower Case
                        'visible' => false,
                        'pallet' => '1',
                        'show_next' => true,
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'not_equal_to', // Operator To Compare
                            'name' => 'send_email_to_email_queue', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => NULL, // JS Callbacks on Load @TODO
                            'change' => NULL, // JS Callbacks on Change @TODO
                            'pallet' => 'get_all_sugar_teams', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name', //PHP Function to Save Options
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_TASK_TYPE_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_SEND_EMAIL_FROM_THIS_ACCOUNT',
                        'name' => 'email_from_account',
                        'visible' => false,
                        'pallet' => '1',
                        'show_next' => true,
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'not_equal_to', // Operator To Compare
                            'name' => 'send_email_to_email_queue', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => NULL,
                            'change' => NULL,
                            'pallet' => 'get_email_from_accounts',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_TASK_SEND_EMAIL_FROM_ACCOUNT_HINTS'
                        )
                    ),


                    array(
                        'lbl' => 'LBL_EMAIL_FIELDS',
                        'name' => 'email_field',
                        'visible' => false,
                        'pallet' => '1',
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'not_equal_to', // Operator To Compare
                            'name' => 'send_email_to_email_queue', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'show_next' => true,
                        'callbacks' => array(
                            'load' => 'get_email_fields', // JS Callbacks on Load @TODO
                            'change' => 'get_email_fields', // JS Callbacks on Change @TODO
                            'pallet' => 'get_email_fields', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_TASK_SEND_EMAIL_EMAIL_FIELD'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_RELATED_MODULE',
                        'name' => 'email_related_module',
                        'visible' => false,
                        'pallet' => '1',
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'not_equal_to', // Operator To Compare
                            'name' => 'send_email_to_email_queue', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'show_next' => true,
                        'callbacks' => array(
                            'load' => 'get_related_modules', // JS Callbacks on Load @TODO
                            'change' => 'get_related_modules', // JS Callbacks on Change @TODO
                            'pallet' => 'get_related_modules', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_TASK_SEND_EMAIL_RELATED_MODULE'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_RELATED_MODULE_EMAIL_FIELD',
                        'name' => 'related_module_email_field',
                        'visible' => false,
                        'pallet' => '1',
                        'class' => 'large',
                        'show_next' => true,
                        'show_if' => array(
                            'operator' => 'not_equal_to', // Operator To Compare
                            'name' => 'send_email_to_email_queue', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'show_if' => array(
                            'operator' => 'not_null', // Operator To Compare
                            'name' => 'email_related_module', //Sub Flow Name
                            'check_compare_to' => FALSE, //Sub Flow Name
                            'value' => 'not_null' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => 'get_related_email_fields', // JS Callbacks on Load @TODO
                            'change' => 'get_related_email_fields', // JS Callbacks on Change @TODO
                            'pallet' => 'get_related_email_fields', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_TASK_SEND_EMAIL_RELATED_MODULE_FIELD'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_SEND_EMAIL_TO_ALL',
                        'name' => 'send_email_to_all',
                        'visible' => false,
                        'pallet' => '0',
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'not_equal_to', // Operator To Compare
                            'name' => 'send_email_to_email_queue', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => 'get_checkbox_next',
                            'change' => 'get_checkbox_next',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_TASK_SEND_EMAIL_TO_ALL'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CHOOSE_TO',
                        'name' => 'send_email_to',
                        'visible' => false,
                        'pallet' => '0',
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'not_equal_to', // Operator To Compare
                            'name' => 'send_email_to_email_queue', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => 'get_to_cc_bcc_address',
                            'change' => 'get_to_cc_bcc_address',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_TASK_SEND_EMAIL_TO_ALL'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CHOOSE_CC',
                        'name' => 'send_email_cc',
                        'visible' => false,
                        'pallet' => '0',
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'not_equal_to', // Operator To Compare
                            'name' => 'send_email_to_email_queue', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => 'get_to_cc_bcc_address',
                            'change' => 'get_to_cc_bcc_address',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_TASK_SEND_EMAIL_TO_ALL'
                        )
                    ), 
                    array(
                        'lbl' => 'LBL_CHOOSE_BCC',
                        'name' => 'send_email_bcc',
                        'visible' => false,
                        'pallet' => '0',
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'not_equal_to', // Operator To Compare
                            'name' => 'send_email_to_email_queue', //Sub Flow Name
                            'value' => '1' // Sub Flow Value
                        ),
                        'callbacks' => array(
                            'load' => 'get_to_cc_bcc_address',
                            'change' => 'get_to_cc_bcc_address',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_TASK_SEND_EMAIL_TO_ALL'
                        )
                    ),                                                             
                ),
            ),
            array(
                'lbl' => 'LBL_TRIGGER_PROCESS', //Sugar Module Label
                'name' => 'trigger_process', //Unique Name - All Lower Case
                'static' => false,
                'visible' => false,
                'multiple' => false,
                'switch' => false,
                'show_if' => array(
                    'operator' => 'equal_to', // Operator To Compare
                    'name' => 'task_type', //Sub Flow Name
                    'value' => 'Trigger Process' // Sub Flow Value
                ),
                'steps' => array( //Sub Flows for the Parent Flow
                    array(
                        'lbl' => 'LBL_PROCESS', //Sugar Module Label
                        'name' => 'trigger_process_name', //Unique Name - All Lower Case
                        'visible' => false,
                        //This just says grab the pallet
                        'pallet' => '1',
                        'class' => 'large',
                        'callbacks' => array(
                            'load' => NULL, // JS Callbacks on Load @TODO
                            'change' => NULL, // JS Callbacks on Change @TODO
                            'pallet' => 'get_processes', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name', //PHP Function to Save Options
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_TASK_EMAIL_TEMPLATES_HINTS'
                        )
                    ),
                ),
            ),
        		array(
        				'lbl' => 'LBL_SEND_SUGAR_NOTIFICATION', //Sugar Module Label
        				'name' => 'send_sugar_notification', //Unique Name - All Lower Case
        				'static' => false,
        				'visible' => false,
        				'multiple' => false,
        				'switch' => false,
        				'show_if' => array(
        						'operator' => 'equal_to', // Operator To Compare
        						'name' => 'task_type', //Sub Flow Name
        						'value' => 'Send Sugar Notification' // Sub Flow Value
        				),
			            'steps' => array( //Sub Flows for the Parent Flow
			                    array(
			                        'lbl' => 'LBL_NOTIFICATION_NAME', //Sugar Module Label
			                        'name' => 'notification_name', //Unique Name - All Lower Case
			                        'visible' => false,
			                        'pallet' => '0',
			                        'class' => 'large',
			                        'callbacks' => array(
			                            'change' => 'get_text_field',
			                            'load' => 'get_text_field',
			                        ),
			                        'hints' => array(
			                            'lbl' => 'LBL_NOTIFICATION_NAME_HINTS'
			                        )
			                    ),
			            		array(
			            				'lbl' => 'LBL_NOTIFICATION_DESCRIPTION', //Sugar Module Label
			            				'name' => 'notification_description', //Unique Name - All Lower Case
			            				'visible' => false,
			            				'pallet' => '0',
			            				'class' => 'large',
			            				'callbacks' => array(
			            						'change' => 'get_text_field',
			            						'load' => 'get_text_field',
			            				),
			            				'hints' => array(
			            						'lbl' => 'LBL_NOTIFICATION_DESCRIPTION_HINTS'
			            				)
			            		),
			            		array(
			            				'lbl' => 'LBL_ASSIGN_TO',
			            				'name' => 'assigned_user_id_notification',
			            				'visible' => false,
			            				'show_next' => true,
			            				'pallet' => '1',
			            				'callbacks' => array(
			            						'load' => NULL,
			            						'change' => NULL,
			            						'pallet' => 'get_all_sugar_users',
			            						'save' => 'save_by_drag_name',
			            				),
			            				'hints' => array(
			            						'lbl' => 'LBL_CREATEV2_ASSIGN_TO'
			            				)
			            		),
			            		//notifications_severity_list
			            		array(
			            				'lbl' => 'LBL_NOTIFICATION_SEVERITY',
			            				'name' => 'severity_notification',
			            				'visible' => false,
			            				'class' => 'large',
			            				'pallet' => '1',
			            				'callbacks' => array(
			            						'load' => NULL,
			            						'change' => NULL,
			            						'pallet' => 'get_notifications_severity',
			            						'save' => 'save_by_drag_name',
			            				),
			            				'hints' => array(
			            						'lbl' => 'LBL_NOTIFICATION_SEVERITY_HINTS'
			            				)
			            		),
			              ),
        		),
            array(
                'lbl' => 'LBL_CUSTOM_SCRIPT', //Sugar Module Label
                'name' => 'custom_script', //Unique Name - All Lower Case
                'static' => false,
                'visible' => false,
                'multiple' => false,
                'switch' => false,
                'show_if' => array(
                    'operator' => 'equal_to', // Operator To Compare
                    'name' => 'task_type', //Sub Flow Name
                    'value' => 'Custom Script' // Sub Flow Value
                ),
                'steps' => array(
                    array(
                        'lbl' => 'LBL_CUSTOM_SCRIPT',
                        'name' => 'upload_custom_script',
                        'visible' => false,
                        'pallet' => '0',
                        'class' => 'fluid',
                        'show_next' => true,
                        'callbacks' => array(
                            'load' => 'get_upload_custom_script',
                            'change' => 'get_upload_custom_script',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_UPLOAD_CUSTOM_SCRIPT_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CUSTOM_SCRIPT',
                        'name' => 'custom_script',
                        'visible' => false,
                        'pallet' => '1',
                        'callbacks' => array(
                            'pallet' => 'get_custom_script',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_CUSTOM_SCRIPT_HINTS'
                        )
                    ),

                ),
            ),
            array(
                'lbl' => 'LBL_MODIFY_OBJECT_FIELD',
                'show_if' => array(
                    'operator' => 'equal_to',
                    'name' => 'task_type',
                    'value' => 'Modify Object Field'
                ),
                'name' => 'modify_object_field',
                'static' => false,
                'visible' => false,
                'is_multiple' => true,
                'multiple_id' => 102,
                'multiple' => array( // Buttons Array
                    array(
                        'lbl' => 'LBL_CREATEV2_ADD_ANOTHER', // Sugar Module Label
                        'name' => 'add_another',
                        'function' => 'add_previous_parent_flow' // JS Function
                    ),
                ),
                'steps' => array(
                    array(
                        'lbl' => 'LBL_MODIFY_OBJECT_FIELD', //Sugar Module Label
                        'name' => 'modify_object_field', //Unique Name - All Lower Case
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_filter_fields',
                            'change' => 'get_filter_fields',
                            'pallet' => 'get_filter_fields', //php function to get pallet options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_MODIFY_OBJECT_FIELD_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_MODIFY_FIELD_METHOD',
                        'name' => 'modify_field_method',
                        'visible' => false,
                        'pallet' => '1',
                       // 'class' => 'large',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_modify_object_field_method',
                            'change' => 'get_modify_object_field_method',
                            'pallet' => 'get_modify_object_field_method',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_MODIFY_OBJECT_FIELD_METHOD_HINTS'
                        )
                    ),

                    array(
                        'lbl' => 'LBL_MODIFY_FIELD_BY_FIELD',
                        'name' => 'modify_field_by_field',
                        'show_if' => array(
                            'operator' => 'equal_to', // operator to compare
                            'name' => 'modify_field_method', //sub flow name
                            'value' => 'from_field||append_by_field' // sub flow value
                        ),
                        'visible' => false,
                        'pallet' => '1',
                        'class' => 'large',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_modify_fields_by_field',
                            'change' => 'get_modify_fields_by_field',
                            'pallet' => 'get_modify_fields_by_field', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_MODIFY_OBJECT_FIELD_BY_FIELD_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_MODIFY_FIELD_BY_RELATED_MODULE',
                        'name' => 'modify_field_by_related_module',
                        'show_if' => array(
                            'operator' => 'equal_to', // operator to compare
                            'name' => 'modify_field_method', //sub flow name
                            'value' => 'from_related||append_from_related' // sub flow value
                        ),
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_related_modules',
                            'change' => 'get_related_modules',
                            'pallet' => 'get_related_modules', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_RELATED_MODULE_MODIFY_HELP'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_MODIFY_FIELD_BY_RELATED_FIELD',
                        'name' => 'modify_field_by_related_field',
                        'show_if' => array(
                            'operator' => 'equal_to', // operator to compare
                            'name' => 'modify_field_method', //sub flow name
                            'value' => 'from_related||append_from_related' // sub flow value
                        ),
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_related_fields',
                            'change' => 'get_related_fields',
                            'pallet' => 'get_related_fields', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_RELATED_FIELD_MODIFY_HELP'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_MODIFY_FIELD_BY_VALUE',
                        'name' => 'modify_field_by_value',
                        'pallet' => '0',
                        'class' => 'large',
                        'show_next' => false,
                        'show_if' => array(
                            'operator' => 'equal_to', // Operator To Compare
                            'name' => 'modify_field_method', //Sub Flow Name
                            'value' => 'by_value||append_by_value' // Sub Flow Value
                        ),
                        'visible' => false,
                        'callbacks' => array(
                            'load' => 'modify_field_by_value',
                            'change' => 'modify_field_by_value',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_MODIFY_OBJECT_FIELD_BY_VALUE_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_MODIFY_FEILD_BY_DATE',
                        'name' => 'modify_field_by_date',
                        'pallet' => '0',
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'equal_to', // Operator To Compare
                            'name' => 'modify_field_method', //Sub Flow Name
                            'value' => 'by_date' // Sub Flow Value
                        ),
                        'visible' => false,
                        'callbacks' => array(
                            'load' => 'modify_field_by_date',
                            'change' => 'modify_field_by_date',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_MODIFY_OBJECT_FIELD_BY_DATE_HINTS'
                        )
                    ),

                ),
            ),
            array(
                'lbl' => 'LBL_MODIFY_RELATED_OBJECT_FIELD_FROM_RELATED', //Sugar Module Label
                'show_if' => array(
                    'operator' => 'equal_to', // Operator To Compare
                    'name' => 'task_type', //Sub Flow Name
                    'value' => 'Modify Related Field From Related Object' // Sub Flow Value
                ),

                'name' => 'modify_related_object_field_from_related_object', //Unique Name - All Lower Case
                'static' => false,
                'visible' => false,
                'is_multiple' => true,
                'multiple_id' => 104,
                'multiple' => array( // Buttons Array
                    array(
                        'lbl' => 'LBL_CREATEV2_ADD_ANOTHER', // Sugar Module Label
                        'name' => 'add_another',
                        'function' => 'add_previous_parent_flow' // JS Function
                    ),
                ),
                'steps' => array( //Sub Flows for the Parent Flow
                    array(
                        'lbl' => 'LBL_MODIFY_RELATED_OBJECT_MODULE', //Sugar Module Label
                        'name' => 'modify_related_object_module', //Unique Name - All Lower Case
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_related_modules', // JS Callbacks on Load @TODO
                            'change' => 'get_related_modules', // JS Callbacks on Change @TODO
                            'pallet' => 'get_related_modules', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_RELATED_MODIFY_OBJECT_MODULE_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_MODIFY_RELATED_FIELD', //Sugar Module Label
                        'name' => 'modify_related_field', //Unique Name - All Lower Case
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_modify_related_fields', // JS Callbacks on Load @TODO
                            'change' => 'get_modify_related_fields', // JS Callbacks on Change @TODO
                            'pallet' => 'get_modify_related_fields', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_RELATED_MODIFY_OBJECT_FIELD_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_MODIFY_FIELD_METHOD',
                        'name' => 'modify_related_field_method',
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_modify_related_field_method_from_object',
                            'change' => 'get_modify_related_field_method_from_object',
                            'pallet' => 'get_modify_related_field_method_from_object',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_MODIFY_OBJECT_FIELD_METHOD_HINTS'
                        )
                    ),

                    array(
                        'lbl' => 'LBL_MODIFY_RELATED_OBJECT_MODULE_FROM_OBJECT', //Sugar Module Label
                        'name' => 'modify_related_object_module_from_object', //Unique Name - All Lower Case
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_related_modules', // JS Callbacks on Load @TODO
                            'change' => 'get_related_modules', // JS Callbacks on Change @TODO
                            'pallet' => 'get_related_modules', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_RELATED_MODIFY_OBJECT_MODULE_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_MODIFY_RELATED_FIELD_FROM_FIELD', //Sugar Module Label
                        'name' => 'modify_related_field_from_object', //Unique Name - All Lower Case
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_modify_related_fields_from_object', // JS Callbacks on Load @TODO
                            'change' => 'get_modify_related_fields_from_object', // JS Callbacks on Change @TODO
                            'pallet' => 'get_modify_related_fields_from_object', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_RELATED_MODIFY_OBJECT_FIELD_HINTS'
                        )
                    ),

                ),
            ),
            array(
                'lbl' => 'LBL_MODIFY_RELATED_OBJECT_FIELD', //Sugar Module Label
                'show_if' => array(
                    'operator' => 'equal_to', // Operator To Compare
                    'name' => 'task_type', //Sub Flow Name
                    'value' => 'Modify Related Object Field' // Sub Flow Value
                ),
                'name' => 'modify_related_object_field', //Unique Name - All Lower Case
                'static' => false,
                'visible' => false,
                'is_multiple' => true,
                'multiple_id' => 103,
                'multiple' => array( // Buttons Array
                    array(
                        'lbl' => 'LBL_CREATEV2_ADD_ANOTHER', // Sugar Module Label
                        'name' => 'add_another',
                        'function' => 'add_previous_parent_flow' // JS Function
                    ),
                ),
                'steps' => array( //Sub Flows for the Parent Flow
                    array(
                        'lbl' => 'LBL_MODIFY_RELATED_OBJECT_MODULE', //Sugar Module Label
                        'name' => 'modify_related_object_module', //Unique Name - All Lower Case
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_related_modules', // JS Callbacks on Load @TODO
                            'change' => 'get_related_modules', // JS Callbacks on Change @TODO
                            'pallet' => 'get_related_modules', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_RELATED_MODIFY_OBJECT_MODULE_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_MODIFY_RELATED_FIELD', //Sugar Module Label
                        'name' => 'modify_related_field', //Unique Name - All Lower Case
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_modify_related_fields', // JS Callbacks on Load @TODO
                            'change' => 'get_modify_related_fields', // JS Callbacks on Change @TODO
                            'pallet' => 'get_modify_related_fields', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_RELATED_MODIFY_OBJECT_FIELD_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_MODIFY_FIELD_METHOD',
                        'name' => 'modify_related_field_method',
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_modify_related_field_method',
                            'change' => 'get_modify_related_field_method',
                            'pallet' => 'get_modify_related_field_method',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_MODIFY_OBJECT_FIELD_METHOD_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_MODIFY_FIELD_BY_FIELD',
                        'name' => 'modify_related_field_by_field',
                        'show_if' => array(
                            'operator' => 'equal_to', // Operator To Compare
                            'name' => 'modify_related_field_method', //Sub Flow Name
                            'value' => 'from_field||append_by_field' // Sub Flow Value
                        ),
                        'visible' => false,
                        'pallet' => '1',
                        'class' => 'large',
                        'callbacks' => array(
                            'load' => 'get_modify_related_fields_by_field',
                            'change' => 'get_modify_related_fields_by_field',
                            'pallet' => 'get_modify_related_fields_by_field', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_MODIFY_OBJECT_FIELD_BY_FIELD_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_MODIFY_FIELD_BY_VALUE',
                        'name' => 'modify_related_field_by_value',
                        'pallet' => '0',
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'equal_to', // Operator To Compare
                            'name' => 'modify_related_field_method', //Sub Flow Name
                            'value' => 'by_value||append_by_value' // Sub Flow Value
                        ),
                        'visible' => false,
                        'callbacks' => array(
                            'load' => 'modify_related_field_by_value',
                            'change' => 'modify_related_field_by_value',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_MODIFY_OBJECT_FIELD_BY_VALUE_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_MODIFY_FEILD_BY_DATE',
                        'name' => 'modify_related_field_by_date',
                        'pallet' => '0',
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'equal_to', // Operator To Compare
                            'name' => 'modify_related_field_method', //Sub Flow Name
                            'value' => 'by_date' // Sub Flow Value
                        ),
                        'visible' => false,
                        'callbacks' => array(
                            'load' => 'modify_field_by_date',
                            'change' => 'modify_field_by_date',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_MODIFY_OBJECT_FIELD_BY_DATE_HINTS'
                        )
                    ),
                ),
            ),
            array(
                'lbl' => 'LBL_ROUTE_OBJECT',
                'show_if' => array(
                    'operator' => 'equal_to',
                    'name' => 'task_type',
                    'value' => 'Route Object'
                ),
                'name' => 'route_object',
                'static' => false,
                'visible' => false,
                'is_multiple' => false,
                'multiple' => false,
                'steps' => array(
                    array(
                        'lbl' => 'LBL_ROUTING_TYPE',
                        'name' => 'routing_type',
                        'visible' => false,
                        'pallet' => '1',
                        'callbacks' => array(
                            'load' => NULL,
                            'change' => NULL,
                            'pallet' => 'get_routing_type',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_ROUTING_TYPE_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_ROUTE_TO_USER',
                        'show_if' => array(
                            'operator' => 'equal_to',
                            'name' => 'routing_type',
                            'value' => 'User'
                        ),
                        'name' => 'route_to_user',
                        'visible' => false,
                        'pallet' => '1',
                        'callbacks' => array(
                            'load' => NULL,
                            'change' => NULL,
                            'pallet' => 'get_all_sugar_users',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_ROUTE_TO_USER_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_ROUTE_TO_ROLE',
                        'show_if' => array(
                            'operator' => 'equal_to',
                            'name' => 'routing_type',
                            'value' => 'Round Robin to Role'
                        ),
                        'name' => 'route_to_role',
                        'visible' => false,
                        'pallet' => '1',
                        'callbacks' => array(
                            'load' => NULL,
                            'change' => NULL,
                            'pallet' => 'get_all_sugar_roles',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_ROUTE_TO_ROLE_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_ROUTE_TO_TEAM',
                        'show_if' => array(
                            'operator' => 'equal_to',
                            'name' => 'routing_type',
                            'value' => 'Assign Object to Team'
                        ),
                        'name' => 'route_to_team',
                        'visible' => false,
                        'pallet' => '1',
                        'show_next' => true,
                        'callbacks' => array(
                            'load' => NULL,
                            'change' => NULL,
                            'pallet' => 'get_all_sugar_teams',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_ROUTE_TO_TEAM_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_ROUTE_TO_SECURITY_GROUP',
                        'show_if' => array(
                            'operator' => 'equal_to',
                            'name' => 'routing_type',
                            'value' => 'Assign To Security Group'
                        ),
                        'name' => 'route_to_securitygroups',
                        'visible' => false,
                        'pallet' => '1',
                        'callbacks' => array(
                            'load' => NULL,
                            'change' => NULL,
                            'pallet' => 'get_all_security_groups',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_ROUTE_TO_ROLE_HINTS'
                        )
                    ),

                    array(
                        'lbl' => 'LBL_CHECK_LOGIN_STATUS',
                        'show_if' => array(
                            'operator' => 'equal_to',
                            'name' => 'routing_type',
                            'value' => 'Round Robin to Role||User'
                        ),
                        'name' => 'check_login_status',
                        'visible' => false,
                        'pallet' => '0',
                        'callbacks' => array(
                            'load' => 'get_checkbox_next',
                            'change' => 'get_checkbox_next',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array('lbl' => 'LBL_CREATEV2_CHECK_LOGIN_STATUS_HINTS')
                    ),
                    array(
                        'lbl' => 'LBL_FIND_LOGGED_IN_USER',
                        'show_if' => array(
                            'operator' => 'equal_to',
                            'name' => 'routing_type',
                            'value' => 'Round Robin to Role'
                        ),
                        'name' => 'find_logged_in_user',
                        'visible' => false,
                        'pallet' => '0',
                        'class' => 'large',
                        'callbacks' => array(
                            'load' => 'get_checkbox_next',
                            'change' => 'get_checkbox_next',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array('lbl' => 'LBL_CREATEV2_FIND_LOGGED_IN_USER_HINTS')
                    ),
                ),
            ),
            array(
                'lbl' => 'LBL_CONVERT_LEAD',
                'show_if' => array(
                    'operator' => 'equal_to',
                    'name' => 'task_type',
                    'value' => 'Convert Lead'
                ),
                'name' => 'convert_lead',
                'static' => false,
                'visible' => false,
                'is_multiple' => false,
                'multiple' => false,
                'steps' => array(
                    array(
                        'lbl' => 'LBL_CREATE_ACCOUNT',
                        'name' => 'create_account',
                        'visible' => false,
                        'pallet' => '0',
                        'callbacks' => array(
                            'load' => 'get_checkbox_next',
                            'change' => 'get_checkbox_next',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_CREATE_ACCOUNT_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATE_CONTACT',
                        'name' => 'create_contact',
                        'visible' => false,
                        'pallet' => '0',
                        'callbacks' => array(
                            'load' => 'get_checkbox_next',
                            'change' => 'get_checkbox_next',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_CREATE_CONTACT_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATE_OPPORTUNITIY',
                        'name' => 'create_opportunity',
                        'visible' => false,
                        'pallet' => '0',
                        'callbacks' => array(
                            'load' => 'get_checkbox_next',
                            'change' => 'get_checkbox_next',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_CREATE_OPPORTUNITY_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATE_OPPORTUNITY_NAME',
                        'show_if' => array(
                            'operator' => 'equal_to',
                            'name' => 'create_opportunity',
                            'value' => '1'
                        ),
                        'name' => 'create_opportunity_name',
                        'visible' => false,
                        'pallet' => '0',
                        'class' => 'large',
                        'callbacks' => array(
                            'load' => 'get_text_field',
                            'change' => 'get_text_field',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_OPPORTUNITY_NAME_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATE_OPPORTUNITY_SALES_STAGE',
                        'show_if' => array(
                            'operator' => 'equal_to',
                            'name' => 'create_opportunity',
                            'value' => '1'
                        ),
                        'name' => 'create_opportunity_sales_stage',
                        'visible' => false,
                        'class' => 'large',
                        'pallet' => '1',
                        'callbacks' => array(
                            'load' => NULL,
                            'change' => NULL,
                            'pallet' => 'get_opp_sales_stage',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_OPPORTUNITY_SALES_STAGE_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATE_OPPORTUNITY_CLOSE_DATE',
                        'show_if' => array(
                            'operator' => 'equal_to',
                            'name' => 'create_opportunity',
                            'value' => '1'
                        ),
                        'name' => 'create_opportunity_close_date',
                        'visible' => false,
                        'class' => 'large',
                        'pallet' => '0',
                        'callbacks' => array(
                            'load' => 'get_text_field',
                            'change' => 'get_text_field',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_OPPORTUNITY_CLOSE_DATE_HINTS'
                        )
                    ),
                ),
            ),
            array(
                'lbl' => 'LBL_CREATE_NEW_RECORD',
                'show_if' => array(
                    'operator' => 'equal_to',
                    'name' => 'task_type',
                    'value' => 'Create New Record'
                ),
                'name' => 'create_new_record',
                'static' => false,
                'visible' => false,
                'multiple' => false,
                'steps' => array(
                    array(
                        'lbl' => 'LBL_CREATE_RECORD_MODULE',
                        'name' => 'create_record_module',
                        'visible' => false,
                        'pallet' => '1',
                        'callbacks' => array(
                            'load' => NULL, // JS Callbacks on Load @TODO
                            'change' => NULL, // JS Callbacks on Change @TODO
                            'pallet' => 'get_objects', //PHP Function to get Pallet Options
                            'save' => 'save_by_drag_name', //PHP Function to Save Options
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_CREATE_RECORD_MODULE_HINTS'
                        )
                    ),
                		array(
                				'lbl' => 'LBL_CREATEV2_START_DELAY_TYPE', //Sugar Module Label
                				'name' => 'start_delay_type', //Unique Name - All Lower Case
                				'visible' => false,
                				//'show_next' => true,
                				'class' => 'large',
                				//This just says grab the pallet
                				'pallet' => '1',
               					'show_if' => array(
                            		'operator' => 'equal_to',
                            		'name' => 'create_record_module',
                            		'value' => 'tasks||calls'
                        		),
                				'callbacks' => array(
                						'load' => NULL, // JS Callbacks on Load @TODO
                						'change' => NULL, // JS Callbacks on Change @TODO
                						//This next function calls an ajax function to get the function and method in
                						//ajax_start.php - this get_text_field is referenced in this file as a function
                						//So we create a function if not already available for an array that represents
                						'pallet' => 'get_start_delay_type', //PHP Function to get Pallet Options
                						'save' => 'save_by_drag_name', //PHP Function to Save Options
                				),
                				'hints' => array(
                						'lbl' => 'LBL_CREATEV2_TASK_START_DELAY_TYPE_HINTS'
                				)
                		),
                		array(
                				'lbl' => 'LBL_TASK_ORDER', //Sugar Module Label
                				'name' => 'task_order', //Unique Name - All Lower Case
                				'visible' => false,
                				//This just says grab the pallet
                				'pallet' => '0',
                				'class' => 'large',
               					'show_if' => array(
                            		'operator' => 'equal_to',
                            		'name' => 'create_record_module',
                            		'value' => 'tasks||calls'
                        		),
                				'callbacks' => array(
                						'load' => 'get_task_order', // JS Callbacks on Load @TODO
                						'change' => 'get_task_order', // JS Callbacks on Change @TODO
                						//This next function calls an ajax function to get the function and method in
                						//ajax_start.php - this get_text_field is referenced in this file as a function
                						//So we create a function if not already available for an array that represents
                						//'pallet' => 'get_start_delay_type', //PHP Function to get Pallet Options
                						//'save' => 'save_by_drag_name', //PHP Function to Save Options
                				),
                				'hints' => array(
                						'lbl' => 'LBL_CREATEV2_TASK_ORDER_HINTS'
                				)
                		),
                    array(
                        'lbl' => 'LBL_CREATE_RECORD_METHOD',
                        'name' => 'create_record_method',
                        'visible' => false,
                        'pallet' => '1',
                        'callbacks' => array(
                            'load' => 'get_create_record_method',
                            'change' => 'get_create_record_method',
                            'pallet' => 'get_create_record_method',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_CREATE_RECORD_METHOD_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATE_NEW_RECORD_BY_NAME',
                        'name' => 'create_new_record_by_name',
                        'visible' => false,
                        'pallet' => '0',
                        'show_if' => array(
                            'operator' => 'equal_to',
                            'name' => 'create_record_method',
                            'value' => 'By Template'
                        ),
                        'class' => 'large',
                        'callbacks' => array(
                            'load' => 'get_text_field',
                            'change' => 'get_text_field',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_CREATE_RECORD_BY_NAME_HINTS'
                        )
                    ),
                ),
            ),
            array(
                'lbl' => 'LBL_CREATE_NEW_RECORD_BY_FIELD',
                'show_if' => array(
                    'operator' => 'equal_to',
                    'name' => 'create_record_method',
                    'value' => 'By Field'
                ),
                'name' => 'create_by_field',
                'static' => false,
                'visible' => false,
                'is_multiple' => true,
                'multiple_id' => 105,
                'multiple' => array( // Buttons Array
                    array(
                        'lbl' => 'LBL_CREATEV2_ADD_ANOTHER', // Sugar Module Label
                        'name' => 'add_another',
                        'function' => 'add_previous_parent_flow' // JS Function
                    ),
                ),
                'steps' => array(
                    array(
                        'lbl' => 'LBL_CREATE_NEW_FEILD',
                        'name' => 'create_new_field',
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_create_new_field',
                            'change' => 'get_create_new_field',
                            'pallet' => 'get_create_new_field',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_CREATE_NEW_FEILD_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATE_NEW_FIELD_METHOD',
                        'name' => 'create_new_field_method',
                        'visible' => false,
                        'pallet' => '1',
                        'clear_until' => true,
                        'callbacks' => array(
                            'load' => 'get_create_new_field_method', //get_modify_related_field_method
                            'change' => 'get_create_new_field_method',
                            'pallet' => 'get_create_new_field_method',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_MODIFY_OBJECT_FIELD_METHOD_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATE_NEW_FIELD_BY_FIELD',
                        'name' => 'create_new_field_by_field',
                        'show_if' => array(
                            'operator' => 'equal_to',
                            'name' => 'create_new_field_method',
                            'value' => 'from_field||append_by_field'
                        ),
                        'visible' => false,
                        'pallet' => '1',
                        //'class' => 'large',
                        'callbacks' => array(
                            'load' => 'get_create_new_field_by_field',
                            'change' => 'get_create_new_field_by_field',
                            'pallet' => 'get_create_new_field_by_field',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_MODIFY_OBJECT_FIELD_BY_FIELD_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATE_RECORD_BY_VALUE',
                        'name' => 'create_record_by_value',
                        'pallet' => '0',
                        //'class' => 'large',
                        'show_if' => array(
                            'operator' => 'equal_to',
                            'name' => 'create_new_field_method',
                            'value' => 'by_value||append_by_value'
                        ),
                        'visible' => false,
                        'callbacks' => array(
                            'load' => 'create_new_field_by_value',
                            'change' => 'create_new_field_by_value',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_MODIFY_OBJECT_FIELD_BY_VALUE_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CREATE_RECORD_BY_DATE',
                        'name' => 'create_record_by_date',
                        'pallet' => '0',
                        'class' => 'large',
                        'show_if' => array(
                            'operator' => 'equal_to',
                            'name' => 'create_new_field_method',
                            'value' => 'by_date',
                        ),
                        'visible' => false,
                        'callbacks' => array(
                            'load' => 'create_record_by_date',
                            'change' => 'create_record_by_date',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_MODIFY_OBJECT_FIELD_BY_DATE_HINTS'
                        )
                    ),
                ),
            ),
            array(
                'lbl' => 'LBL_REST_SERVICE',
                'show_if' => array(
                    'operator' => 'equal_to',
                    'name' => 'task_type',
                    'value' => 'REST Service'
                ),
                'name' => 'rest_service',
                'static' => false,
                'visible' => false,
                'multiple' => false,
                'steps' => array(
                    array(
                        'lbl' => 'LBL_REST_URL',
                        'name' => 'rest_url',
                        'pallet' => '0',
                        'class' => 'large',
                        'visible' => false,
                        'callbacks' => array(
                            'load' => 'get_text_field',
                            'change' => 'get_text_field',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_REST_URL_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_REST_METHOD',
                        'name' => 'rest_method',
                        'pallet' => '1',
                        'class' => '',
                        'visible' => false,
                        'callbacks' => array(
                            'load' => null,
                            'change' => null,
                            'pallet' => 'get_rest_methods',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_REST_METHOD_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_REST_HTTP_QUERY',
                        'name' => 'rest_http_query',
                        'pallet' => '0',
                        'class' => '',
                        'visible' => false,
                        'callbacks' => array(
                            'load' => 'get_checkbox_next',
                            'change' => 'get_checkbox_next',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_REST_HTTP_QUERY_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_REST_JSON',
                        'name' => 'rest_json',
                        'pallet' => '0',
                        'class' => 'large',
                        'visible' => false,
                        'callbacks' => array(
                            'load' => 'get_textarea',
                            'change' => 'get_textarea',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_REST_JSON_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_REST_HEADERS_JSON',
                        'name' => 'rest_headers',
                        'pallet' => '0',
                        'class' => 'large',
                        'visible' => false,
                        'callbacks' => array(
                            'load' => 'get_textarea',
                            'change' => 'get_textarea',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_REST_HEADERS_JSON_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CUSTOM_SCRIPT',
                        'name' => 'rest_upload_custom_script',
                        'visible' => false,
                        'pallet' => '0',
                        'class' => 'fluid',
                        'show_next' => true,
                        'callbacks' => array(
                            'load' => 'get_upload_custom_script',
                            'change' => 'get_upload_custom_script',
                            'pallet' => NULL,
                            'save' => NULL,
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_UPLOAD_CUSTOM_SCRIPT_HINTS'
                        )
                    ),
                    array(
                        'lbl' => 'LBL_CUSTOM_SCRIPT',
                        'name' => 'rest_custom_script',
                        'visible' => false,
                        'pallet' => '1',
                        'callbacks' => array(
                            'pallet' => 'get_custom_script',
                            'save' => 'save_by_drag_name',
                        ),
                        'hints' => array(
                            'lbl' => 'LBL_CREATEV2_CUSTOM_SCRIPT_HINTS'
                        )
                    )
                )
            )
        ),
    ),
);

?>
