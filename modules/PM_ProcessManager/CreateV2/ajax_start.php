<?php
/*********************************************************************************
 * Copyright: SierraCRM, Inc. 2014
 * Portions created by SierraCRM are Copyright (C) SierraCRM, Inc.
 * The contents of this file are subject to the SierraCRM, Inc. End User License Agreement
 * You may not use this file except in compliance with the License. 
 * You may not rent, lease, lend, or in any way distribute or transfer any rights or this file or Process Manager
 * registrations (purchased licenses) to third parties without SierraCRM, Inc. written approval, and subject to
 * agreement by the recipient of the terms of this EULA.
 * Process Manager for SugarCRM is owned by SierraCRM, Inc. and is protected by international and local copyright laws and
 * treaties. You must not remove or alter any copyright notices on any copies of Process Manager for SugarCRM. 
 * You may not use, copy, or distribute Process Manager for SugarCRM, except as granted by SierraCRM, Inc.
 * without written authorization from SierraCRM, Inc. or its designated agents. Furthermore, this Copyright notice
 * does not grant you any rights in connection with any trademarks or service marks of SierraCRM, Inc. 
 * SierraCRM, Inc. reserves all intellectual property rights, including copyrights, and trademark rights of this software.
 ********************************************************************************/
/*********************************************************************************
 *SierraCRM, Inc
 *14563 Ward Court
 *Grass Valley, CA. 95945
 *www.sierracrm.com
 ********************************************************************************/

require_once('modules/PM_ProcessManager/CreateV2/functions.php');
//require_once('modules/PM_ProcessManager/CreateV2/functions.php');

$flow = get_flow();

$data = array();

$data['flow'] = $flow;

$method = $_POST['method'];


switch($method){
case 'get_guid':
    get_guid();
    die();
case 'get_value_from_post':
    get_value_from_post();
    die();
case 'get_objects':
    get_objects();
    die();
case 'get_next_group':
    get_next_group();
    die();
case 'get_start_delay_minutes':
    get_start_delay_minutes();
    die();
case 'get_start_delay_hours':
    get_start_delay_hours();
    die();
case 'get_start_delay_days':
    get_start_delay_days();
    die();
case 'get_start_delay_months':
    get_start_delay_months();
    die();
case 'get_start_delay_years':
    get_start_delay_years();
    die();
case 'get_all_sugar_users':
    get_all_sugar_users();
    die();
case 'get_task_priority':
    get_task_priority();
    die();
case 'get_project_task_priority':
    get_project_task_priority();
    die();
case 'get_project_task_status':
    get_project_task_status();
    die();
case 'get_task_name':
    get_task_name();
    die();
case 'get_task_order':
    get_task_order();
    die();
case 'get_task_type':
    get_task_type();
    die();
case 'get_start_events':
    get_start_events();
    die();
case 'get_modify_object_field_method':
    get_modify_object_field_method();
    die();
case 'get_modify_field_method':
    get_modify_field_method();
    die();
case 'get_yes_no':
    get_yes_no();
    die();
case 'get_contact_roles':
    get_contact_roles();
    die();
case 'save_by_drag_name':
    save_by_drag_name();
    die();
case 'getEmailTemplates':
    getEmailTemplates();
    die();
case 'get_filter_fields':
    get_filter_fields();
    die();
case 'get_filter_fields_from_field':
    get_filter_fields_from_field();
    die();
case 'get_related_modules':
    get_related_modules();
    die();
case 'get_email_fields':
    get_email_fields();
    die();
case 'get_related_fields':
    get_related_fields();
    die();
case 'get_related_fields_by_field':
    get_related_fields_by_field();
    die();
case 'get_start_delay_type':
    get_start_delay_type();
    die();
case 'get_filter_operators':
    get_filter_operators();
    die();
case 'get_filter_field_data':
    get_filter_field_data();
    die();
case 'get_users_array':
    get_users_array();
    die();
case 'get_teams_array':
    get_teams_array();
    die();
case 'get_textarea':
    get_textarea();
    die();
case 'get_text_field':
    get_text_field();
    die();
case 'get_datetime_field':
    get_datetime_field();
    die();
case 'get_date_field':
    get_date_field();
    die();
case 'get_checkbox_next':
    get_checkbox_next();
    die();
case 'get_stage_name':
    get_stage_name();
    die();
case 'get_process_query':
    get_process_query();
    die();
case 'get_process_frequency':
    get_process_frequency();
    die();
case 'get_daily_start_time':
    get_daily_start_time();
    die();
case 'get_day_to_run':
    get_day_to_run();
    die();
case 'get_process_month':
    get_process_month();
    die();
case 'get_process_day_of_month':
    get_process_day_of_month();
    die();
case 'get_check_delete_modify_cancel':
    get_check_delete_modify_cancel();
    die();
case 'get_process_object_cancel_field_operator':
    get_process_object_cancel_field_operator();
    die();
case 'get_email_queue_campiagn_name':
    get_email_queue_campiagn_name();
    die();
case 'get_email_from_accounts':
    get_email_from_accounts();
    die();
case 'get_custom_script':
    get_custom_script();
    die();
case 'get_upload_custom_script':
    get_upload_custom_script();
    die();
case 'get_mapping':
    get_mapping();
    die();
case 'get_mapping_with_values':
    get_mapping_with_values();
    die();
case 'get_rendered_flow':
    get_rendered_flow();
    die();
case 'save_everything':
    save_everything();
    die();
case 'duplicate_everything':
    duplicate_everything();
    die();
case 'delete_everything':
     delete_everything();
     die();
case 'get_routing_type':
    get_routing_type();
    die();
case 'get_all_sugar_roles':
    get_all_sugar_roles();
    die();
case 'get_all_security_groups':
    get_all_security_groups();
    die();    
case 'get_all_sugar_teams':
    get_all_sugar_teams();
    die();
case 'get_opp_sales_stage':
    get_opp_sales_stage();
    die();
case 'get_create_record_method':
    get_create_record_method();
    die();    
case 'delete_stage':
    delete_stage();
    die();    
case 'delete_task':
    delete_task();
    die();
case 'get_processes':
    get_processes();
    die();
case 'get_rest_methods':
    get_rest_methods();
    die();
case 'get_notifications_severity':
   get_notifications_severity();
   die();
case 'get_modify_field_method_from_object':
    get_modify_field_method_from_object();
    die();
case 'get_email_other_owners':
    get_email_other_owners();
    die();
case 'get_to_cc_bcc_address':
      get_to_cc_bcc_address();
      die();
case 'get_stage_order':
    get_stage_order();
    die();
}

function get_guid(){
	echo create_guid();
	die();
}

function get_value_from_post(){
	if(isset($_POST['value']) && strlen($_POST['value']) > 0){
		$value = $_POST['value'];
	}else{
		$value = "";
	}
	return $value;
} 

function getSugarVersion(){
	global $sugar_config;
	$version = $sugar_config['sugar_version'];
	$pos = strpos($version, '7.');
	if ($pos === false) {
		return FALSE;
	}
	else{
		return TRUE;
	}	
}

function get_objects(){
	global $data, $mod_strings, $app_list_strings;
	global $sugar_config;
	$version = $sugar_config['sugar_version'];
	$pos = strpos($version, '7.');
	$isSugarPro = FALSE;
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_CREATEV2_OBJECT'];
	
	$modules = $GLOBALS['moduleList'];

	foreach($modules as $module){
		$bean = BeanFactory::getBean($module);
        $module = $app_list_strings['moduleList'][$module];
		if(!is_object($bean)) continue;
		$data['items'][$bean->table_name] = $module;
		if($bean->table_name == 'quotes'){
			$isSugarPro = TRUE;
		}
	}
	$data['items']['project_task'] = 'Project Task';
    if($pos !== FALSE){
		$data['items']['activities'] = 'Activity Stream';
	}
	else{
		$data['items']['sugarfeed'] = 'Activity Stream';
	}
	asort($data['items']);
	get_view('views/pallet/ajax.php', $data);
}

function get_next_group(){
    $last_group = isset($_POST['last_group']) ? (int)$_POST['last_group'] : 1;
    $group = array(
            'number' => ++$last_group,
            'andor' => 'and',
            'color' => convert_group_num_to_color($last_group)
        );
    get_view('views/_parts/group_list_item.php', array('group' => $group, 'first' => false));
}

//Modified Start Delay Minutes

function get_stage_order()
{
    global $data, $mod_strings, $app_list_strings;
//See the Language file for the following
    $data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_START_DELAY_MINUTES'];
    $data['items'] = array();
    $data['field'] = 'stage_order';
    $data['options'] = $app_list_strings['start_delay_hours'];
    get_view('views/stage/start_delay_minutes.php', $data);

}

function get_start_delay_minutes(){
	global $data, $mod_strings, $app_list_strings;
	//See the Language file for the following
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_START_DELAY_MINUTES'];
	$data['items'] = array();
    $data['field'] = 'start_delay_minutes';
    $data['options'] = $app_list_strings['start_delay_minutes'];
	get_view('views/stage/start_delay_minutes.php', $data);
}

//This is the ajax function for Stages for the Start Delay Hours
function get_start_delay_hours(){
	global $data, $mod_strings, $app_list_strings;
	//See the Language file for the following
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_START_DELAY_HOURS'];
    $data['items'] = array();
    $data['field'] = 'start_delay_hours';
    $data['options'] = $app_list_strings['start_delay_hours'];
    get_view('views/stage/start_delay_minutes.php', $data);
}

//This is the ajax function for Stages for the Start Delay Days
function get_start_delay_days(){
	global $data, $mod_strings, $app_list_strings;
	//See the Language file for the following
    $data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_START_DELAY_HOURS'];
    $data['items'] = array();
    $data['field'] = 'start_delay_days';
    $data['options'] = $app_list_strings['start_delay_days'];
    get_view('views/stage/start_delay_minutes.php', $data);
}

//This is the ajax function for Stages for the Start Delay Days
function get_start_delay_months(){
	global $data, $mod_strings, $app_list_strings;
	//See the Language file for the following
    $data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_START_DELAY_HOURS'];
    $data['items'] = array();
    $data['field'] = 'start_delay_months';
    $data['options'] = $app_list_strings['start_delay_months'];
    get_view('views/stage/start_delay_minutes.php', $data);
}

function get_start_delay_years(){
    global $data, $mod_strings, $app_list_strings;
    //See the Language file for the following
    $data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_START_DELAY_HOURS'];
    $data['items'] = array();
    $data['field'] = 'start_delay_years';
    $data['options'] = $app_list_strings['start_delay_years'];
    get_view('views/stage/start_delay_minutes.php', $data);
}

function get_all_sugar_users(){
	global $data, $mod_strings, $app_list_strings, $db;
	//See the Language file for the following
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_ASSIGN_TO'];

	$sql = "SELECT id, user_name FROM users WHERE deleted = 0 and status = 'Active' ORDER BY user_name ASC";
	$res = $db->query($sql);
	while($row = $db->fetchByAssoc($res)){
		$data['items'][$row['user_name']] = $row['user_name'];	
	}
    //Now add User who Created the Object, User who Last Modified the Object
    $data['items']['Created By'] = 'Created By';
    $data['items']['Last Modified By'] = 'Last Modified By';
	get_view('views/pallet/ajax.php', $data);
}

function get_task_priority(){
	global $data, $mod_strings, $app_list_strings;
	//See the Language file for the following
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_TASK_PRIORITY'];
	$data['items'] = $app_list_strings['task_priority'];
	//Nothing to do below
	get_view('views/pallet/ajax.php', $data);
}

function get_project_task_priority(){
	global $data, $mod_strings, $app_list_strings;
	//See the Language file for the following
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_PROJECT_TASK_PRIORITY'];
	$data['items'] = $app_list_strings['project_task_priority_options'];
	//Nothing to do below
	get_view('views/pallet/ajax.php', $data);
}

function get_project_task_status(){
	global $data, $mod_strings, $app_list_strings;
	//See the Language file for the following
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_PROJECT_TASK_STATUS'];
	$data['items'] = $app_list_strings['project_task_status_options'];
	//Nothing to do below
	get_view('views/pallet/ajax.php', $data);
}

//*************************************************
//Process Stage Tasks Functions

function get_task_name(){
	global $data, $mod_strings, $app_list_strings;
	$data['value'] = get_value_from_post();
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_STAGE_NAME'];
	get_view('views/filter_fields/varchar.php', $data);
	unset($data['value']);
	get_view('views/filter_fields/done.php', $data);
}

function get_task_order(){
	global $data, $mod_strings, $app_list_strings;
	$data['value'] = get_value_from_post();
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_TASK_ORDER'];
	$data['default'] = '1';
	get_view('views/filter_fields/varchar.php', $data);
	unset($data['value']);
	get_view('views/filter_fields/done.php', $data);
}

function get_task_type(){
	global $data, $mod_strings, $app_list_strings;
	$modules = $GLOBALS['moduleList'];
	$isSugarPro = FALSE;
	//See the Language file for the following
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_TASK_TYPE'];
	$data['items'] = array();
	foreach ($app_list_strings['task_type'] as $key=>$value){
		if(!empty($value)){
			$data['items'][$key] = $value;
		}
	}
	if(in_array('Quotes', $modules))
		$data['items']['Send Sugar Notification'] = 'Send Sugar Notification';
	//Nothing to do below
	get_view('views/pallet/ajax.php', $data);
}

function get_email_other_owners(){
    global $data, $mod_strings, $app_list_strings;
    $modules = $GLOBALS['moduleList'];
    $isSugarPro = FALSE;
    //See the Language file for the following
    $data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_EMAIL_OTHER_OWNERS'];
    $data['items'] = array();
    foreach ($app_list_strings['send_email_other_owner'] as $key=>$value){
        if(!empty($value)){
            $data['items'][$key] = $value;
        }
    }
    //Nothing to do below
    get_view('views/pallet/ajax.php', $data);
}

function get_start_events(){
	global $data, $mod_strings, $app_list_strings;
	
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_CREATEV2_START_EVENT'];
	
	$data['items'] = $app_list_strings['process_start_event']; 
	
	get_view('views/pallet/ajax.php', $data);
}


function get_rest_methods(){
    global $data, $mod_strings;

    $data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_CREATEV2_START_EVENT'];

    $data['items'] = array(
        'GET' => 'GET',
        'POST' => 'POST',
        'PUT' => 'PUT',
        'DELETE' => 'DELETE'
    );

    get_view('views/pallet/ajax.php', $data);
}


function get_modify_object_field_method(){
	global $data, $mod_strings;
	
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_MODIFY_FIELD_METHOD'];

	$module = (!empty($_POST['module_name'])) ? $_POST['module_name'] : die('Error, No Module.');
	$field = (!empty($_POST['field'])) ? $_POST['field'] : die('Error, No Field.');

	$data['items'] = array(
		'by_value' => 'By Value',
		'from_field' => 'Copy From Field',
		'from_related' => 'From Related Module',
	);
	$bean = convert_table_to_bean($module);
	$found = array();
	
	foreach($bean->field_defs as $def){
		if($def['name'] == $field){
			$found = $def;
			break;
		}
	}
	//echo $found['type'];
	switch($found['type']){
		case 'int':
		case 'float':
		case 'iframe':
			break;
		case 'enum':
			break;	
		case 'multienum':
			break;		
		case 'bool':
			break;	
		case 'date':
		case 'datetime':
		case 'datetimecombo':
			$data['items']['by_date'] = 'Calculate Date';
			break;	
        case 'varchar':
		case 'text':
		default:
		    $data['items']['append_by_value'] = 'Append By Value';
		    $data['items']['append_by_field'] = 'Append By Field';
		    $data['items']['append_from_related'] = 'Append From Related Module';
			break;	
	}

	get_view('views/pallet/ajax.php', $data);
}

function get_modify_field_method_from_object(){
    global $data, $mod_strings;

    $data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_MODIFY_FIELD_METHOD'];

    $module = (!empty($_POST['module_name'])) ? $_POST['module_name'] : die('Error, No Module.');
    $field = (!empty($_POST['field'])) ? $_POST['field'] : die('Error, No Field.');
    $data['items'] = array(
        'from_field' => 'Copy From Field',
        'append_by_field' => 'Append By Field',
    );

    get_view('views/pallet/ajax.php', $data);
}

function get_modify_field_method(){
	global $data, $mod_strings;
	
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_MODIFY_FIELD_METHOD'];

	$module = (!empty($_POST['module_name'])) ? $_POST['module_name'] : die('Error, No Module.');
	$field = (!empty($_POST['field'])) ? $_POST['field'] : die('Error, No Field.');
	$data['items'] = array(
		'by_value' => 'By Value',
		'from_field' => 'Copy From Field',
	);
	$bean = convert_table_to_bean(strtolower($module));
	$found = array();
	
	foreach($bean->field_defs as $def){
		if($def['name'] == $field){
			$found = $def;
			break;
		}
	}
	//echo $found['type'];
	switch($found['type']){
		case 'int':
		case 'float':
		case 'iframe':
			break;
		case 'enum':
			break;	
		case 'multienum':
			break;		
		case 'bool':
			break;	
		case 'date':
            $data['items']['by_date'] = 'Calculate Date';
            break;
		case 'datetime':
            $data['items']['by_date'] = 'Calculate Date';
            break;
		case 'datetimecombo':
			$data['items']['by_date'] = 'Calculate Date';
			break;	
        case 'varchar':
		case 'text':
		default:
		    $data['items']['append_by_value'] = 'Append By Value';
		    $data['items']['append_by_field'] = 'Append By Field';
			break;	
	}

	get_view('views/pallet/ajax.php', $data);
}

function get_yes_no(){
	global $data, $mod_strings;
	
	$data['title'] = '';
	
	$data['items'] = array(
		'1' => 'Yes',
		'0' => 'No',
	);
	
	get_view('views/pallet/ajax.php', $data);
}

function get_contact_roles(){
	global $data, $mod_strings, $app_list_strings;
	
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_CHOOSE_CONTACT_ROLES'];
	
	$data['items'] = $app_list_strings['contact_roles'];
	
	get_view('views/pallet/ajax.php', $data);
}

function save_by_drag_name(){
	echo html_entity_decode($_POST['drag_name']);
}

function getEmailTemplates(){
	global $db;
	$queryFieldList = 'Select name from email_templates where deleted = 0 order by name ASC';
	$resultFieldList = $db->query($queryFieldList, true);
	while($rowFieldList = $db->fetchByAssoc($resultFieldList)){
		$fieldName = str_replace("'","\'",$rowFieldList['name']);
		$data['items'][$fieldName] = $fieldName;
		//$fields .= '<option value="'.$fieldName .'">'.$fieldName .'</option>';
	}
	get_view('views/pallet/ajax.php', $data);			
}

function get_filter_fields(){
	global $data, $current_language, $mod_strings;
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_CREATEV2_FIELD'];
	$module = (!empty($_POST['module_name'])) ? $_POST['module_name'] : die('Error. No Module.');
	if($module == 'sugarfeed'){
		$data['items']['name'] = 'name';
	}
	else{
		$data['value'] = get_value_from_post();
		$bean = convert_table_to_bean($module);
		$mod_strings = return_module_language($current_language, $bean->module_dir);
		$data['items'] = array();
		
		foreach($bean->field_defs as $def){
            //Check for Email Fields
            if($def['name'] == 'email1')
                $data['items'][$def['name']] = $mod_strings[$def['vname']];
            if($def['type'] == 'link' && !empty($mod_strings[$def['vname']]))
                $data['items'][$def['name']] = $mod_strings[$def['vname']];
			if(!(isset($def['source']) && $def['source'] == 'non-db') && !empty($def['name'])){
				$req = "";
				if(isset($def['required']) && $def['required']) $req = "<span style='color: red;'>*</span>";
				if(isset($def['vname']) && isset($mod_strings[$def['vname']])){
					$data['items'][$def['name']] = $mod_strings[$def['vname']] . $req;
				}else{
					$data['items'][$def['name']] = $def['name'] . $req;
				}
                //Support for Assigned User Id
                if($def['name'] == 'assigned_user_id'){
                    $data['items'][$def['name']] = $mod_strings[$def['vname']];
                }
			}
		}
	}
	
	get_view('views/pallet/ajax.php', $data);
}


function get_filter_fields_from_field(){
	global $data, $current_language, $mod_strings;
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_CREATEV2_FIELD'];
	$module = (!empty($_POST['module_name'])) ? $_POST['module_name'] : die('Error. No Module.');
	$from_module = (!empty($_POST['from_module'])) ? $_POST['from_module'] : $module;
	$field = (!empty($_POST['field'])) ? $_POST['field'] : die('Error. No Field.');
	$data['value'] = get_value_from_post();
	$bean = convert_table_to_bean($module);
	$from_bean = convert_table_to_bean($from_module);
	$mod_strings = return_module_language($current_language, $bean->module_dir);
	$data['items'] = array();

	$current_type = false;
	$current_dbtype = false;
	foreach($from_bean->field_defs as $def){
		//echo $def['name'] . ' :: ' . $field;
		if($def['name'] == $field){
			$current_type = $def['type'];
			$current_dbtype = $def['dbType'];
		}
	}
	foreach($bean->field_defs as $def){
		$type = $def['type'];
		$name = $def['name'];
		//Only Show Fields with the same type - name fields are type name so also allow varchar
		if($current_type == false){
			continue;
		}
		$add = false;
		//Allow ID
		if(($current_type == 'id') && ($def['type'] == 'id')){
			$add = true;
		}
        //Allow ID to Varchar
        if(($current_type == 'varchar') && ($def['type'] == 'id')){
            $add = true;
        }
        //Allow Varchar to ID
        if(($current_type == 'id') && ($def['type'] == 'varchar')){
            $add = true;
        }
		//Allow Names
        if(($current_type == 'name') && (($def['type'] == 'name') || ($def['dbType'] == 'varchar'))){
			$add = true;
		}
		if(($current_type == 'name') && (($def['type'] == 'varchar') ||($def['type'] == 'char')) ){
			$add = true;
		}
		if(($current_type == 'varchar') && (($def['type'] == 'name') ||($def['type'] == 'char')) ){
			$add = true;
		}				
		//Allow Varchar and Text 		
		if(($current_type == 'text') && (($def['type'] == 'varchar') ||($def['type'] == 'char')) ){
			$add = true;
		}
		
		if(($current_type == 'datetimecombo' || $current_type == 'datetime') && ($def['type'] == 'datetimecombo' || $def['type'] == 'datetime') ){
			$add = true;
		}
        //Allow Assigned User ID
        if(($current_type == 'assigned_user_name') && ($name == 'assigned_user_id')){
            $add = true;
        }
        //Allow Relate Fields
        if(($current_type == 'relate') && (($type == 'relate') || ($def['type'] == 'varchar'))){
            $add = true;
        }
        //Allow Email Fields
        if(($name == 'email1') && ($type == 'relate')){
            $add = true;
        }
        //All Matching Fields
        if($current_type == $def['type']){
            $add = true;
        }
		if($add){
			$req = "";
			if(isset($def['required']) && $def['required']) $req = "<span style='color: red;'>*</span>";
			if(isset($def['vname']) && isset($mod_strings[$def['vname']])){
				$data['items'][$def['name']] = $mod_strings[$def['vname']] . $req;
			}else{
				$data['items'][$def['name']] = $def['name'] . $req;
			}
		}
		if($add) continue;
		if($current_dbtype != $def['dbType'] || $current_type != $def['type']) continue;
		
		if(!(isset($def['source']) && $def['source'] == 'non-db') && !empty($def['name'])){
			$req = "";
			if(isset($def['required']) && $def['required']) $req = "<span style='color: red;'>*</span>";
			if(isset($def['vname']) && isset($mod_strings[$def['vname']])){
				$data['items'][$def['name']] = $mod_strings[$def['vname']] . $req;
			}else{
				$data['items'][$def['name']] = $def['name'] . $req;
			}
		}
	}
    get_view('views/pallet/ajax.php', $data);
}




function get_related_modules(){
	global $data, $current_language, $mod_strings, $app_list_strings;

	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_CREATEV2_RELATED_MODULES'];
	$module = (!empty($_POST['module_name'])) ? $_POST['module_name'] : die('Error. No Module.');
	$current = convert_table_to_bean($module);
	$mod_strings = return_module_language($current_language, $current->module_dir);

	$current->load_relationships();
	$linked_fields = $current->get_linked_fields();

	$data['items'] = array();
	foreach($linked_fields as $field){
		$link_table = $field['name'];
		$current->load_relationship($link_table);
		if(!isset($current->$link_table)){
			continue;
		}
		$related_module = $current->$link_table->getRelatedModuleName();
		if(array_search($related_module, $GLOBALS['moduleList']) !== false){
			$bean = BeanFactory::getBean($related_module);
            if(in_array($related_module, $data['items'])){
                //$link_table = $related_module ."-" .$link_table;
                $related_module = $related_module ."-" .$field['name'];
            }
            if(in_array($related_module,$app_list_strings['moduleList']))
                $related_module =$app_list_strings['moduleList'][$related_module];
            if(!empty($related_module)) {
                $data['items'][$related_module] = $related_module;
            }
		}

	}
    if(getSugarVersion() && $module == 'quotes'){
        $data['items']['RevenueLineItem'] = 'Revenue Line Items';
        $data['items']['Product'] = 'Quoted Line Items';
    }
	//Include Project Tasks
    $data['items']['project_task'] = 'Project Tasks';
    $data['items']['User'] = 'Users';
	$data['items'] = array_unique($data['items']);

	asort($data['items']);

	foreach($current->field_defs as $def){
		//Only Show Fields with the same type
		//if($current_type !== false && $def['type'] != $current_type) continue;
		if( !empty($def['name']) && ($def['type'] =! 'relate') ){
			$req = "";
			if(isset($def['required']) && $def['required']) $req = "<span style='color: red;'>*</span>";
			if(isset($def['vname']) && isset($mod_strings[$def['vname']])){
				$data['items'][$def['name']] = $mod_strings[$def['vname']] . $req;
			}else{
				$data['items'][$def['name']] = $def['name'] . $req;
			}
		}
	}


	get_view('views/pallet/ajax.php', $data);
}

function get_email_fields(){
	global $data, $current_language, $mod_strings;

	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_EMAIL_FIELDS'];
	$module = (!empty($_POST['module_name'])) ? $_POST['module_name'] : die('Error. No Module.');
	$bean = convert_table_to_bean($module);
	if ($bean == NULL){
	  $pm_module =  $_POST['parent_module'];
	  $pm_bean = convert_table_to_bean($pm_module);
		$table = $pm_bean->field_defs[$module];
		if(isset($table['module'])){
			$linkedModule = $table['module'];
		}else{
			$linkedModule = $table['link'];
		}
		$linkedModule= strtolower($linkedModule);
		$bean = convert_table_to_bean($linkedModule);
	}	
	$mod_strings = return_module_language($current_language, $bean->module_dir);
	$data['items'] = array();

	foreach($bean->field_defs as $def){
		if((!(isset($def['source']) && $def['source'] == 'non-db') || ($def['type'] == 'relate') ) && !empty($def['name'])){
			$req = "";
			if(($def['type'] == 'varchar') || ($def['type'] == 'email'  || ($def['type'] == 'relate') )){
				if(isset($def['required']) && $def['required']) $req = "<span style='color: red;'>*</span>";
				if(isset($def['vname']) && isset($mod_strings[$def['vname']])){
					$data['items'][$def['name']] = $mod_strings[$def['vname']] . $req;
				}else{
					$data['items'][$def['name']] = $def['name'] . $req;
				}
		  	}
		}
		elseif(($def['name'] == 'email') || ($def['name'] == 'email1')){
					$data['items'][$def['name']] = $def['name'] . $req;
		}

	 }

	get_view('views/pallet/ajax.php', $data);
}

function get_related_fields(){
	global $data, $current_language, $mod_strings;

	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_CREATEV2_RELATED_FIELD'];
	$module = (!empty($_POST['module_name'])) ? $_POST['module_name'] : die('Error. No Module.');
    //Check to see if there is a - in the module name - if so strip it out and strotolower
    $pos = strpos($module, '-');
    if ($pos === false) {
        $module = strtolower($module);
    } else {
        $module = strtolower(substr($module,0,$pos));
    }
	$bean = convert_table_to_bean($module);
    //Support for Revenue Line Items
    if($module == 'revenuelineitem'){
        $bean = new RevenueLineItem();
    }
    if($module == 'product'){
        $bean = new Product();
    }
    if($module == 'user'){
        $bean = new User();
    }
	if ($bean == NULL){
	$pm_module =  $_POST['value'];
	$pm_bean = convert_table_to_bean($pm_module);
	$table = $pm_bean->field_defs[$module];
	if(isset($table['module'])){
		$linkedModule = $table['module'];
	}else{
		$linkedModule = $table['link'];
	}
	$linkedModule= strtolower($linkedModule);
	$bean = convert_table_to_bean($linkedModule);
	}
	$mod_strings = return_module_language($current_language, $bean->module_dir);
	$data['items'] = array();

	foreach($bean->field_defs as $def){
		if(!(isset($def['source']) && $def['source'] == 'non-db') && !empty($def['name'])){
			$req = "";
			if(isset($def['required']) && $def['required']) $req = "<span style='color: red;'>*</span>";
			if(isset($def['vname']) && isset($mod_strings[$def['vname']])){
				$data['items'][$def['name']] = $mod_strings[$def['vname']] . $req;
			}else{
				$data['items'][$def['name']] = $def['name'] . $req;
			}
		}
	}
	
	get_view('views/pallet/ajax.php', $data);
}

function get_related_fields_by_field(){
	global $data, $current_language, $mod_strings;
	
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_CREATEV2_RELATED_MODULES'];
	$module = (!empty($_POST['module_name'])) ? $_POST['module_name'] : die('Error. No Module.');
	$field = (!empty($_POST['field'])) ? $_POST['field'] : die('Error. No Field.');
	$bean = convert_table_to_bean($module);
	$mod_strings = return_module_language($current_language, $bean->module_dir);
	$data['items'] = array();

	$current_type = false;
	foreach($bean->field_defs as $def){
		//echo $def['name'] . ' :: ' . $field;
		if($def['name'] == $field){
			//echo $def['name'];
			$current_type = $def['type'];
            break;
		}
	}

    //Allow any field to copy over to text
	foreach($bean->field_defs as $def){
		//Only Show Fields with the same type

		if($current_type !== false && $def['type'] != $current_type && $current_type != 'text' && $current_type != 'name') continue;
		if(!(isset($def['source']) && $def['source'] == 'non-db') && !empty($def['name'])){
			$req = "";
			if(isset($def['required']) && $def['required']) $req = "<span style='color: red;'>*</span>";
			if(isset($def['vname']) && isset($mod_strings[$def['vname']])){
				$data['items'][$def['name']] = $mod_strings[$def['vname']] . $req;
			}else{
				$data['items'][$def['name']] = $def['name'] . $req;
			}
		}
	}
	
	get_view('views/pallet/ajax.php', $data);
}


function get_start_delay_type(){
	global $data, $mod_strings;
	
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_CREATEV2_START_DELAY_TYPE'];
	
	$data['items'] = array(
		'--None--' => '--None--',
		'From Completion of Previous Task' => 'From Completion of Previous Task'		
	);
	
	get_view('views/pallet/ajax.php', $data);

}

function get_filter_operators(){
	global $data, $mod_strings;
	$module = (!empty($_POST['module_name'])) ? $_POST['module_name'] : die('Error. No Module.');
	if(!empty($_POST['field'])){
		$field = $_POST['field'];
	}
	elseif(!empty($_POST['related_field'])){
		$field = $_POST['related_field'];
	}else{
        $field = $_POST['cancel_field'];
    }
	if(!empty($_POST['related_module'])){
		$module = strtolower($_POST['related_module']);
	}
	
	//$field = (!empty($_POST['field'])) ? $_POST['field'] : die('Error, No Field.');
	$bean = convert_table_to_bean($module);
	$found = array();
    if(!empty($bean)) {
        foreach ($bean->field_defs as $def) {
            if ($def['name'] == $field) {
                $found = $def;
                break;
            }
        }
    }
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_CREATEV2_OPERATOR'];
    $type = $found['type'];
	switch($found['type']){
		case 'int':
			$data['items']['='] = 'Equal To';
			$data['items']['!='] = 'Not Equal To';
			$data['items']['<'] = 'Less Than';
            $data['items']['<='] = 'Less Than or Equal To';
			$data['items']['>'] = 'Greater Than';
            $data['items']['>='] = 'Greater Than or Equal To';
			$data['items']['any change'] = 'Any Change';
			$data['items']['any value'] = 'Any Value';
			$data['items']['is null'] = 'Is Null';
			$data['items']['from to'] = 'Field Changes From To';			
			break;
        case 'decimal':
            $data['items']['='] = 'Equal To';
            $data['items']['!='] = 'Not Equal To';
            $data['items']['<'] = 'Less Than';
            $data['items']['<='] = 'Less Than or Equal To';
            $data['items']['>'] = 'Greater Than';
            $data['items']['>='] = 'Greater Than or Equal To';
            $data['items']['any change'] = 'Any Change';
            $data['items']['any value'] = 'Any Value';
            $data['items']['is null'] = 'Is Null';
            $data['items']['from to'] = 'Field Changes From To';
            break;
		case 'bool':
			$data['items']['='] = 'Equal To';
			$data['items']['any change'] = 'Any Change';
			break;
		case 'float':
			$data['items']['='] = 'Equal To';
			$data['items']['!='] = 'Not Equal To';
			$data['items']['<'] = 'Less Than';
            $data['items']['<='] = 'Less Than or Equal To';
			$data['items']['>'] = 'Greater Than';
            $data['items']['>='] = 'Greater Than or Equal To';
			break;
		case 'currency':
			$data['items']['='] = 'Equal To';
			$data['items']['!='] = 'Not Equal To';
			$data['items']['<'] = 'Less Than';
            $data['items']['<='] = 'Less Than or Equal To';
			$data['items']['>'] = 'Greater Than';
            $data['items']['>='] = 'Greater Than or Equal To';
			$data['items']['any change'] = 'Any Change';
			$data['items']['any value'] = 'Any Value';
			$data['items']['is null'] = 'Is Null';
			$data['items']['from to'] = 'Field Changes From To';
			break;			
		case 'iframe':
			break;
		case 'enum':
			$data['items']['='] = 'Equal To';
			$data['items']['!='] = 'Not Equal To';
			$data['items']['any change'] = 'Any Change';
			$data['items']['from to'] = 'Field Changes From To';
			$data['items']['contains'] = 'Contains';
            $data['items']['does not contain'] = 'Does Not Contain';
            $data['items']['is empty'] = 'Is Empty';
            $data['items']['any value'] = 'Any Value';
			break;
		case 'multienum':
			$data['items']['='] = 'Equal To';
			$data['items']['!='] = 'Not Equal To';
			$data['items']['any change'] = 'Any Change';
			$data['items']['from to'] = 'Field Changes From To';
			$data['items']['contains'] = 'Contains';
            $data['items']['does not contain'] = 'Does Not Contain';
            $data['items']['is empty'] = 'Is Empty';
            $data['items']['any value'] = 'Any Value';
			break;
		case 'date':
			$data['items']['='] = 'Equal To';
			$data['items']['date field due today'] = 'Date Field Due Today';
			$data['items']['date field due in x days'] = 'Date Field Due in X Days';
			$data['items']['date field due in the past x days'] = 'Date Field Due in the Past x Days';
            $data['items']['day is today'] = 'Day of Month is Today';
            $data['items']['day and month is today'] = 'Day and Month is Today';
			$data['items']['any change'] = 'Any Change';
            $data['items']['any value'] = 'Any Value';
            $data['items']['is null'] = 'Is Null';
            $data['items']['is empty'] = 'Is Empty';
            $data['items']['date is greater than'] = 'Date is Greater Than';
            $data['items']['date is less than'] = 'Date is Less Than';
		case 'datetime':
			$data['items']['='] = 'Equal To';
			$data['items']['date field due today'] = 'Date Field Due Today';
			$data['items']['date field due in x days'] = 'Date Field Due in X Days';
			$data['items']['date field due in the past x days'] = 'Date Field Due in the Past x Days';
			$data['items']['any change'] = 'Any Change';
            $data['items']['any value'] = 'Any Value';
            $data['items']['is null'] = 'Is Null';
            $data['items']['is empty'] = 'Is Empty';
            $data['items']['date is greater than'] = 'Date is Greater Than';
            $data['items']['date is less than'] = 'Date is Less Than';
		case 'datetimecombo':
			$data['items']['='] = 'Equal To';
			$data['items']['date field due today'] = 'Date Field Due Today';
			$data['items']['time field due in x hours'] = 'Time Field Due in the Next x Hours';
			$data['items']['date field due in x days'] = 'Date Field Due in X Days';
			$data['items']['date field due in the past x days'] = 'Date Field Due in the Past x Days';
			$data['items']['any change'] = 'Any Change';
            $data['items']['any value'] = 'Any Value';
            $data['items']['is null'] = 'Is Null';
            $data['items']['is empty'] = 'Is Empty';
            $data['items']['date is greater than'] = 'Date is Greater Than';
            $data['items']['date is less than'] = 'Date is Less Than';
			break;
		case 'varchar':
			$data['items']['='] = 'Equal To';
			$data['items']['!='] = 'Not Equal To';
			$data['items']['<'] = 'Less Than';
            $data['items']['<='] = 'Less Than or Equal To';
			$data['items']['>'] = 'Greater Than';
            $data['items']['>='] = 'Greater Than or Equal To';
			$data['items']['any change'] = 'Any Change';
			$data['items']['any value'] = 'Any Value';
			$data['items']['is null'] = 'Is Null';
			$data['items']['from to'] = 'Field Changes From To';
			$data['items']['contains'] = 'Contains';
            $data['items']['does not contain'] = 'Does Not Contain';
            $data['items']['is empty'] = 'Is Empty';
			break;
		case 'name':
			$data['items']['='] = 'Equal To';
			$data['items']['='] = 'Equal To';
			$data['items']['!='] = 'Not Equal To';
			$data['items']['any change'] = 'Any Change';
			$data['items']['any value'] = 'Any Value';
			$data['items']['is null'] = 'Is Null';
			$data['items']['from to'] = 'Field Changes From To';
			$data['items']['contains'] = 'Contains';
            $data['items']['does not contain'] = 'Does Not Contain';
            $data['items']['is empty'] = 'Is Empty';
			break;
		case 'text':
			$data['items']['='] = 'Equal To';
			$data['items']['!='] = 'Not Equal To';
			$data['items']['any change'] = 'Any Change';
			$data['items']['any value'] = 'Any Value';
			$data['items']['is null'] = 'Is Null';
			$data['items']['from to'] = 'Field Changes From To';
			$data['items']['contains'] = 'Contains';
            $data['items']['does not contain'] = 'Does Not Contain';
            $data['items']['is empty'] = 'Is Empty';
			break;
		default:
			$data['items']['='] = 'Equal To';
			$data['items']['!='] = 'Not Equal To';
			$data['items']['any change'] = 'Any Change';
			$data['items']['from to'] = 'Field Changes From To';
			$data['items']['contains'] = 'Contains';
            $data['items']['does not contain'] = 'Does Not Contain';
			break;
	}	

	get_view('views/pallet/ajax.php', $data);
}

function get_to_cc_bcc_address(){
	    global $data, $mod_strings, $app_list_strings;
	    $data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_CREATEV2_FIELD_VALUES'];
	    $data['value'] = get_value_from_post();	    
      $arrOpt = array();
			$data['multi'] = true;
			$data['options'] = $app_list_strings['to_cc_bcc_email_list'];
            foreach($data['options'] as $option){
                $opt = "^" .$option ."^";
                $arrOpt[$opt] = $opt;
            }
      $data['options'] = $arrOpt;
      get_view('views/filter_fields/dropdown.php', $data);
      unset($data['value']);
      get_view('views/filter_fields/done.php', $data);				
}

function get_filter_field_data(){
	global $data, $mod_strings, $app_list_strings;
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_CREATEV2_FIELD_VALUES'];
	$data['value'] = get_value_from_post();
	$module = (!empty($_POST['module_name'])) ? $_POST['module_name'] : die('Error, No Module.');
	$field = (!empty($_POST['field'])) ? $_POST['field'] : die('Error, No Field.');
	$operator = (!empty($_POST['operator'])) ? $_POST['operator'] : false;
    if(($operator == 'contains') || ($operator == 'date field due today') || ($operator == 'date field due in x days') || ($operator == 'time field due in x hours') || ($operator == 'date field due in the past x days')){
        // IF Any Change then always show checkbox
        get_view('views/filter_fields/varchar.php', $data);
        unset($data['value']);
        get_view('views/filter_fields/done.php', $data);
        return true;
    }
	
	$bean = convert_table_to_bean($module);
	$found = array();
	
	foreach($bean->field_defs as $def){
		if($def['name'] == $field){
			$found = $def;
			break;
		}
	}
	if(in_array($field, array('assigned_user_id', 'created_by', 'modified_user_id'))){
		$found['type'] = 'user_id';
	}
	if(in_array($field, array('team_id', 'team_set_id'))){
		$found['type'] = 'team_id';
	}
	switch($found['type']){
		case 'varchar':
		case 'int':
		case 'float':
		case 'iframe':
			get_view('views/filter_fields/varchar.php', $data);
			break;
		case 'assigned_user_name':
		case 'user_id':
			$data['multi'] = false;
			$data['options'] = get_users_array();
			get_view('views/filter_fields/dropdown.php', $data);
			break;
		case 'team_id':
			$data['multi'] = false;
			$data['options'] = get_teams_array();
			get_view('views/filter_fields/dropdown.php', $data);
			break;
		case 'enum':
			$data['multi'] = false;
			$data['options'] = $app_list_strings[$found['options']];
			get_view('views/filter_fields/dropdown.php', $data);
			break;	
		case 'multienum':
            $arrOpt = array();
			$data['multi'] = true;
			$data['options'] = $app_list_strings[$found['options']];
            //Add the ^ ^ around each option

            foreach($data['options'] as $key => $option){
                $key = "^" .$key ."^";
                $arrOpt[$key] = $option;
            }
            $data['options'] = $arrOpt;

			get_view('views/filter_fields/dropdown.php', $data);
			break;		
		case 'text':
			get_view('views/filter_fields/textarea.php', $data);
			break;	
		case 'bool':
			get_view('views/filter_fields/checkbox.php', $data);
			break;	
		case 'date':
			get_view('views/filter_fields/date.php', $data);
			break;	
		case 'datetime':
		case 'datetimecombo':
			get_view('views/filter_fields/datetime.php', $data);
			break;	
		default:
			get_view('views/filter_fields/varchar.php', $data);
			break;	
	}
    unset($data['value']);
    get_view('views/filter_fields/done.php', $data);
}

function get_users_array()
{
    global $db;

    $sql = "SELECT id, user_name FROM users WHERE deleted = 0 ORDER BY user_name ASC";
    $res = $db->query($sql);
    $data = array();
    while ($row = $db->fetchByAssoc($res)) {
        if($row['user_name'] != 'SNIPUser')
            $data[$row['user_name']] = $row['user_name'];
    }
    $data['Created By'] = 'Created By';
    $data['Last Modified By'] = 'Last Modified By';
	return $data;
}

function get_teams_array(){
	global $db;

	$sql = "SELECT id, name FROM teams WHERE deleted = 0";
	$res = $db->query($sql);
	$data = array();
	while($row = $db->fetchByAssoc($res))
		$data[$row['name']] = $row['name'];
	return $data;
}


function get_textarea(){
	$data['value'] = get_value_from_post();
	get_view('views/filter_fields/textarea.php', $data);
	unset($data['value']);
	get_view('views/filter_fields/done.php', $data);
}

function get_text_field(){
	$data['value'] = get_value_from_post();
	get_view('views/filter_fields/varchar.php', $data);
	unset($data['value']);
	get_view('views/filter_fields/done.php', $data);
}

function get_datetime_field(){
	$data['value'] = get_value_from_post();
	get_view('views/filter_fields/datetime.php', $data);
	unset($data['value']);
	get_view('views/filter_fields/done.php', $data);
}

function get_date_field(){
	$data['value'] = get_value_from_post();
	get_view('views/filter_fields/date.php', $data);
	unset($data['value']);
	get_view('views/filter_fields/done.php', $data);
}

function get_checkbox_next(){
	$data['value'] = get_value_from_post();
	get_view('views/filter_fields/checkbox.php', $data);
	unset($data['value']);
	$data['value'] = 'Next';
	get_view('views/filter_fields/done.php', $data);
}

function get_stage_name(){
	global $data, $mod_strings, $app_list_strings;
	$data['value'] = get_value_from_post();
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_STAGE_NAME'];
	//get_view('views/filter_fields/stageName.php', $data);
    get_view('views/filter_fields/varchar.php', $data);
    $data['value'] = 'Set Stage Order';
	get_view('views/filter_fields/done.php', $data);
}

function get_process_query(){
	global $data, $mod_strings, $app_list_strings;
	$data['value'] = get_value_from_post();
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_PROCESS_QUERY'];
	get_view('views/filter_fields/textarea.php', $data);
	unset($data['value']);
	get_view('views/filter_fields/done.php', $data);
}

function get_process_frequency(){
	global $data, $mod_strings;
	
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_PROCESS_FREQUENCY'];
	
	$data['items'] = array(
        'Every Time PM Runs' => 'Every Time PM Runs',
        'Every 15 Minutes' => 'Every 15 Minutes',
        'Every 30 Minutes' => 'Every 30 Minutes',
		'Hourly' => 'Hourly',
		'Daily' => 'Daily',
		'Weekly' => 'Weekly',
		'Monthly' => 'Monthly',
		'Yearly' => 'Yearly'	
	);
	
	get_view('views/pallet/ajax.php', $data);
}

function get_daily_start_time(){
	global $data, $mod_strings;
	
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_DAILY_START_TIME'];
	
	$data['items'] = array(
		'0' => '12 AM',
		'1' => '1 AM',
		'2' => '2 AM',
		'3' => '3 AM',
		'4' => '4 AM',
		'5' => '5 AM',
		'6' => '6 AM',
		'7' => '7 AM',
		'8' => '8 AM',
		'9' => '9 AM',
		'10' => '10 AM',
		'11' => '11 AM',
		'12' => '12 PM',
		'13' => '1 PM',
		'14' => '2 PM',
		'15' => '3 PM',
		'16' => '4 PM',
		'17' => '5 PM',
		'18' => '6 PM',
		'19' => '7 PM',
		'20' => '8 PM',
		'21' => '9 PM',
		'22' => '10 PM',
		'23' => '11 PM',	
	);
	
	get_view('views/pallet/ajax.php', $data);
}

function get_day_to_run(){
	global $data, $mod_strings;
	
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_PROCESS_START_DAY_OF_WEEK'];
	
	$data['items'] = array(
		'Monday' => 'Monday',
		'Tuesday' => 'Tuesday',
		'Wednesday' => 'Wednesday',
		'Thursday' => 'Thursday',
		'Friday' => 'Friday',
		'Saturday' => 'Saturday',
		'Sunday' => 'Sunday'	
	);
	
	get_view('views/pallet/ajax.php', $data);
}

function get_process_month(){
	global $data, $mod_strings;
	
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_PROCESS_START_DAY_OF_WEEK'];
	
	$data['items'] = array(
		'january' => 'January',
		'february' => 'february',
		'march' => 'March',
		'april' => 'April',
		'may' => 'May',
		'june' => 'June',
		'july' => 'July',
		'august' => 'August',
		'september' => 'September',
		'october' => 'October',
		'november' => 'November',
		'december' => 'December'	
	);
	
	get_view('views/pallet/ajax.php', $data);
}

function get_process_day_of_month(){
	global $data, $mod_strings;
	
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_PROCESS_START_DAY_OF_WEEK'];
	
	$data['items'] = array(
		'0',
		'1',
		'2',
		'3',
		'4',
		'5',
		'6',
		'7',
		'8',
		'9',
		'10',
		'11',
		'12',
		'13',
		'14',
		'15',
		'16',
		'17',
		'18',
		'19',
		'20',
		'21',
		'22',
		'23',
		'24',
		'25',
		'26',
		'27',
		'28',
		'29',
		'30',
		'31'	
	);
	
	get_view('views/pallet/ajax_vals.php', $data);
}

function get_check_delete_modify_cancel(){
	global $data, $mod_strings;
	
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_CHECK_DELETE_MODIFY_CANCEL'];
	
	$data['items'] = array(
		'1' => 'Yes',
		'0' => 'No'
	);
	
	get_view('views/pallet/ajax.php', $data);
}

function get_process_object_cancel_field_operator(){
	global $data, $mod_strings;
	
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_PROCESS_OBJECT_CANCEL_FIELD_OPERATOR'];
	
	$data['items'] = array(
		'=' => 'Equal To',
		'!=' => 'Not Equal To',
		'<' => 'Less Than',
		'>' => 'Greater Than',
		'contains' => 'Contains',
		'does not contain' => 'Does Not Contain',
		'any change' => 'Any Change',		
		'any value' => 'Any Value',		
		'is null' => 'Is NULL'		
	);
	
	get_view('views/pallet/ajax.php', $data);
}

function get_email_queue_campiagn_name(){
	global $db, $data, $mod_strings;
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_EMAIL_QUEUE_CAMPAIGN_NAME'];
	$sql = 'SELECT name FROM campaigns WHERE deleted = 0 ORDER BY name ASC';
	$res = $db->query($sql);
	$data['items'][] = 'Please Specifiy';
	while($row = $db->fetchByAssoc($res)){
		$data['items'][] = $row['name'];	
	}
	get_view('views/pallet/ajax_vals.php', $data);
}

function get_email_from_accounts(){
	global $db, $data, $mod_strings;
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_SEND_EMAIL_FROM_THIS_ACCOUNT'];
	$sql = "SELECT name FROM inbound_email where deleted = 0";
	$res = $db->query($sql);
	$data['items'][] = 'Please Specifiy';
	while($row = $db->fetchByAssoc($res)){
		$data['items'][] = $row['name'];	
	}
	get_view('views/pallet/ajax_vals.php', $data);
}

function get_custom_script(){
	global $db, $data, $mod_strings;
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_CUSTOM_SCRIPT'];
	$dir = "modules/PM_ProcessManager/customScripts/";
	foreach (glob($dir . '*.php') as $filename){
		$data['items'][] = str_replace($dir, '', $filename);	
	}

	get_view('views/pallet/ajax_vals.php', $data);
}

function get_upload_custom_script(){
	global $db, $data, $mod_strings;
	$data['break'] = false;
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_CUSTOM_SCRIPT'];
	$data['value'] = 'Done with Upload';
	get_view('views/filter_fields/done.php', $data);
	$data['value'] = 'Select From Existing';
	get_view('views/filter_fields/done.php', $data);
	get_view('views/filter_fields/custom_script.php', $data);
}

function get_mapping(){
	require_once('modules/PM_ProcessManager/CreateV2/mapping.php');
	echo json_encode($map);
	die();
}

function get_mapping_with_values(){
	$id = (!empty($_POST['process_id'])) ? $_POST['process_id'] : die('Error, No Process.');
	$res = get_everything($id);
	echo json_encode($res);
	die();
}

function get_rendered_flow(){
	$theflow = get_flow();
	$data = array();
	$data['theflow'] = $theflow;
	get_view('views/body/main.php', $data);

}

function save_everything(){
	$data = (!empty($_POST['data'])) ? $_POST['data'] : die(json_encode(array('success' => false)));
	handle_save_everything($data);
	//echo json_encode(array('success' => true, 'test' => $data));
	die();
}

function duplicate_everything(){
	$data = (!empty($_POST['data'])) ? $_POST['data'] : die(json_encode(array('success' => false)));
	handle_duplicate_everything($data);
	//echo json_encode(array('success' => true, 'test' => $data));
	die();
}

function delete_everything(){
    $data = (!empty($_POST['data'])) ? $_POST['data'] : die(json_encode(array('success' => false)));
    handle_duplicate_everything($data);
    //echo json_encode(array('success' => true, 'test' => $data));
    die();
}

function get_routing_type(){
	global $data, $mod_strings, $app_list_strings, $sugar_config;
	//See the Language file for the following
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_ROUTING_TYPE'];
	//TODO - global app_list_strings
	$data['items'] = array();
	foreach ($app_list_strings['routing_type'] as $key=>$value){
		if(!empty($value)){
			$data['items'][$key] = $value;
		}
	}
	//See if Security Groups has been installed
	if(array_key_exists('securitysuite_version', $sugar_config)){
		$data['items']['Assign To Security Group'] = 'Assign To Security Group';
	}

	get_view('views/pallet/ajax.php', $data);
}

function get_all_security_groups(){
	global $data, $mod_strings, $app_list_strings, $db;
	//See the Language file for the following
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_ROLES'];

	$sql = "SELECT id, name FROM securitygroups WHERE deleted = 0";
	$res = $db->query($sql);
	while($row = $db->fetchByAssoc($res)){
		$data['items'][$row['name']] = $row['name'];
	}
	get_view('views/pallet/ajax.php', $data);
}

function get_all_sugar_roles(){
	global $data, $mod_strings, $app_list_strings, $db;
  $field = $_POST['field'];
	//See the Language file for the following
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_ROLES'];

	$sql = "SELECT id, name FROM acl_roles WHERE deleted = 0";
	$res = $db->query($sql);
	while($row = $db->fetchByAssoc($res)){
		$data['items'][$row['name']] = $row['name'];
	}
	get_view('views/pallet/ajax.php', $data);
}

function get_all_sugar_teams(){
    global $db;
    if(!function_exists('get_dbtype')){
        function get_dbtype(){
            global $sugar_config;
            return $sugar_config['dbconfig']['db_type'];
        }
    }

    if(!function_exists('show_tablequery')){
        function show_tablequery($table){
            switch(get_dbtype()){
                case 'ibm_db2':
                    $query = "SELECT * FROM sysibm.systables
                        WHERE name LIKE '".strtoupper($table)."'
                        AND type = 'T'";
                    break;
                case 'mssql':
                    $query = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE '{$table}'";
                    break;
                case 'mysql':
                default:
                    $query = "SHOW TABLES LIKE '{$table}'";
                    break;
            }
            return $query;
        }
    }

	global $data, $mod_strings, $app_list_strings, $db;
	//See the Language file for the following
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_TEAMS'];
    $query = show_tablequery('teams');
	$results = $db->query($query, true);
	$data['items'] = array();
    $data['items']['All Members of Assigned Team'] = 'All Members of Assigned Team';
	while($row = $db->fetchByAssoc($results)){
		$sql = "SELECT id, name FROM teams WHERE deleted = 0";
		$res = $db->query($sql);
		while($row = $db->fetchByAssoc($res)){
			$data['items'][$row['name']] = $row['name'];
		}
	}

	if(count($data['items']) == 0){
		$data['items']['1'] = 'Default';
	}
	
	get_view('views/pallet/ajax.php', $data);
}

function get_notifications_severity(){
	global $data, $mod_strings, $app_list_strings, $db;
	//See the Language file for the following
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_NOTIFICATION_CHOOSE_SEVERITY'];

	$data['items'] = array();

	$data['items'] = $app_list_strings['notifications_severity_list'];

	get_view('views/pallet/ajax.php', $data);
}

function get_opp_sales_stage(){
	global $data, $mod_strings, $app_list_strings, $db;
	//See the Language file for the following
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_CREATE_OPPORTUNITY_SALES_STAGE'];

	$data['items'] = array();
	
	$data['items'] = $app_list_strings['sales_stage_dom'];

	get_view('views/pallet/ajax.php', $data);
}

function get_create_record_method(){
	global $data, $mod_strings, $app_list_strings, $db;
	//See the Language file for the following
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_CREATE_NEW_FIELD_METHOD'];

	$data['items'] = array();
	
	$data['items']['By Template'] = 'By Template';
	$data['items']['By Parent'] = 'By Parent';
	$data['items']['By Field'] = 'By Field';
		
	get_view('views/pallet/ajax.php', $data);
}


function delete_stage(){
	global $db;
	$sugar_id = (!empty($_POST['sugar_id'])) ? $_POST['sugar_id'] : die('Error. No Sugar ID.');
	$parent_id = (!empty($_POST['parent_id'])) ? $_POST['parent_id'] : false;

	$db->query("DELETE FROM pm_processmanagerstage WHERE id = '$sugar_id'");
	$db->query("DELETE FROM pm_processmmanagerstage WHERE pm_processmanagerstage_idb = '$sugar_id'");
}

function delete_task(){
	global $db;
	$sugar_id = (!empty($_POST['sugar_id'])) ? $_POST['sugar_id'] : die('Error. No Sugar ID.');
	$parent_id = (!empty($_POST['parent_id'])) ? $_POST['parent_id'] : false;

	$db->query("DELETE FROM pm_processmanagerstagetask WHERE id = '$sugar_id'");
	$db->query("DELETE FROM pm_processmgerstagetask WHERE pm_processmanagerstagetask_idb = '$sugar_id'");
	$db->query("DELETE FROM pm_process_task_convert_lead_defs WHERE task_id = '$sugar_id'");
	$db->query("DELETE FROM pm_process_task_create_by_field_defs WHERE task_id = '$sugar_id'");
	$db->query("DELETE FROM pm_process_task_create_object_defs WHERE task_id = '$sugar_id'");
	$db->query("DELETE FROM pm_process_task_email_defs WHERE task_id = '$sugar_id'");
	$db->query("DELETE FROM pm_process_task_modify_field_defs WHERE task_id = '$sugar_id'");
	$db->query("DELETE FROM pm_process_task_routing_defs WHERE task_id = '$sugar_id'");
	$db->query("DELETE FROM pm_process_task_notification_defs WHERE task_id = '$sugar_id'");
}

function get_processes(){
    global $data, $mod_strings, $db;
    $current_id = isset($_POST['process_id']) ? $_POST['process_id'] : '';
	//See the Language file for the following
	$data['title'] = $mod_strings['LBL_CREATEV2_CHOOSE'] . ' ' . $mod_strings['LBL_PROCESS'];

	$data['items'] = array();
	
	$query = "SELECT id, name FROM pm_processmanager WHERE deleted = 0 AND status = 'Active' AND id != '{$current_id}'";
	$res = $db->query($query);
	while($row = $db->fetchByAssoc($res)){
    	$data['items'][] = $row['name'];
	}   
		
	get_view('views/pallet/ajax_vals.php', $data);
}
?>
