<?php
/*********************************************************************************
 * Copyright: SierraCRM, Inc. 2014
 * Portions created by SierraCRM are Copyright (C) SierraCRM, Inc.
 * The contents of this file are subject to the SierraCRM, Inc. End User License Agreement
 * You may not use this file except in compliance with the License.
 * You may not rent, lease, lend, or in any way distribute or transfer any rights or this file or Process Manager
 * registrations (purchased licenses) to third parties without SierraCRM, Inc. written approval, and subject to
 * agreement by the recipient of the terms of this EULA.
 * Process Manager for SugarCRM is owned by SierraCRM, Inc. and is protected by international and local copyright laws and
 * treaties. You must not remove or alter any copyright notices on any copies of Process Manager for SugarCRM.
 * You may not use, copy, or distribute Process Manager for SugarCRM, except as granted by SierraCRM, Inc.
 * without written authorization from SierraCRM, Inc. or its designated agents. Furthermore, this Copyright notice
 * does not grant you any rights in connection with any trademarks or service marks of SierraCRM, Inc.
 * SierraCRM, Inc. reserves all intellectual property rights, including copyrights, and trademark rights of this software.
 ********************************************************************************/

/*********************************************************************************
 *SierraCRM, Inc
 *14563 Ward Court
 *Grass Valley, CA. 95945
 *www.sierracrm.com
 ********************************************************************************/
class HandleSave
{
    var $db;
    var $map;
    var $data;
    var $parentids;
    var $ext_tables = array(
        'pm_process_task_create_object_defs',
        'pm_process_task_email_defs',
        'pm_process_task_routing_defs',
        'pm_process_task_convert_lead_defs',
        'pm_process_project_task_defs',
        'pm_process_task_rest_defs',
        'pm_process_task_notification_defs',
    );

    public function __construct()
    {
        global $db;
        $this->db = $db;
    }

    //Step 1: load data
    public function load($data)
    {

        if (isset($data['map'])) {
            $data['map'] = json_decode(str_replace('&quot;', '"', $data['map']), true);
            $this->map = $data['map'];
            return true;
        } else {
            return false;
        }
    }

    //Step 2: Initiate everything
    public function init()
    {
        //Step 2a: Parse Mapping
        $this->parse_mapping();
        //Step 2b: Build SQL Statements and Execute
        $this->generate_sql();
        //Check the global Logic Hook
        $hook = array();
        $hook['module'] = '';
        $hook['hook'] = 'after_save';
        $hook['order'] = '69';
        $hook['description'] = 'Process Manager Hook';
        $hook['file'] = 'modules/PM_ProcessManager/insertIntoPmEntryTable.php';
        $hook['class'] = 'insertIntoPmEntryTable';
        $hook['function'] = 'setPmEntryTable';
        check_logic_hook_file($hook['module'], $hook['hook'], array($hook['order'], $hook['description'], $hook['file'], $hook['class'], $hook['function']));

    }

		private function get_dbtype(){
	        global $sugar_config;
	        return $sugar_config['dbconfig']['db_type'];
	    }
	    
    /*****
     * Convert Mapping File into something better for SQL
     *
     * Mapping to
     *    $data = array(
     *        'table_name' => array(
     *            //FOR EACH ROW
     *            array(
     *                'id' => '', //if not empty then update else insert
     *                'col1' => 'value',
     *                'col2' => 'value'
     *            )
     *        ),
     *
     *    );
     ******/
    private function parse_mapping()
    {
        global $current_user;
        $data = array();
        $relids = array();

        if (!empty($_POST['duplicate'])) {
            $newIds = array();

            foreach ($this->map as $row) {
                foreach ($row['value'] as $val) {
                    if (!empty($val['parent_id']) && !isset($newIds[$val['parent_id']])) {
                        $newIds[$val['parent_id']] = create_guid();
                    }
                    if (!empty($val['sugar_id']) && !isset($newIds[$val['sugar_id']])) {
                        $newIds[$val['sugar_id']] = create_guid();
                    }
                }
            }

            foreach ($this->map as &$row) {
                foreach ($row['value'] as &$val) {
                    if (!empty($val['parent_id']) && isset($newIds[$val['parent_id']])) {
                        $val['parent_id'] = $newIds[$val['parent_id']];
                    }
                    if (!empty($val['sugar_id']) && isset($newIds[$val['sugar_id']])) {
                        $val['sugar_id'] = $newIds[$val['sugar_id']];
                    }
                }
            }
        }

        foreach ($this->map as $row) {
            $db = explode('.', $row['db']); // $db[0] == table name, $db[1] == col_name
            //	echo $row['db'] . ' == ' . var_dump($row['value']) . " \n";
            if (!isset($row['value'])) $row['value'] = array();
            foreach ($row['value'] as $val) {
                if (!isset($row['rel_table'])) {
                    $row['rel_table'] = false;
                }
                echo $row['db'] . ' == ' . $val['value'] . " \n";
                if (!isset($row['default'])) $row['default'] = '';
                if (!isset($row['rel'])) $row['rel'] = '';
                $this->update_sql_data_row($data, $db[0], $db[1], $val, $row['default'], $row['rel_type'], $row['rel'], $row['rel_table']);
                if (!empty($val['parent_id']) && $row['rel'] !== false && $row['rel_type'] == 'rel' && $row['rel_table'] !== false) {
                    if (!$this->does_rel_exist($row['rel'], $val, $row['rel_table'])) {
                        if (!isset($relids[$val['sugar_id']])) $relids[$val['sugar_id']] = create_guid();
                        $this->update_sql_data_row($data, $row['rel_table'], 'deleted', array('parent_id' => 'uhoh', 'sugar_id' => $relids[$val['sugar_id']], 'value' => 0), '');
                        $this->update_sql_data_row($data, $row['rel_table'], 'date_modified', array('parent_id' => 'uhoh', 'sugar_id' => $relids[$val['sugar_id']], 'value' => date('Y-m-d H:i:s', time())), '');
                        foreach ($row['rel'] as $type => $col) {
                            if ($type == 'parent') {
                                $id = $val['parent_id'];
                            } else if ($type == 'sugar') {
                                $id = $val['sugar_id'];
                            }
                            $rel_val = array(
                                'parent_id' => 'uhoh',
                                'sugar_id' => $relids[$val['sugar_id']],
                                'value' => $id
                            );
                            $this->update_sql_data_row($data, $row['rel_table'], $col, $rel_val, '');
                        }
                    }
                }

            }
        }
        //UPDATE BACKWARDS COMPATIBILITY
        //Set related filter object to the pm table
        $table = 'pm_process_related_filter_table';
        $process_id = $data['pm_processmanager'][0]['id'];
        if(array_key_exists($table,$data)) {
            foreach ($data[$table] as $i => $row) {
                if (empty($row['related_field_name'])) {
                    unset($data[$table][$i]);
                    continue;
                }
                $val = array(
                    'sugar_id' => $process_id,
                    'parent_id' => $process_id,
                    'value' => $row['related_module']
                );
                $this->update_sql_data_row($data, 'pm_processmanager', 'related_object', $val, '');
                $val = array(
                    'sugar_id' => $process_id,
                    'parent_id' => $process_id,
                    'value' => 'Yes'
                );
                $this->update_sql_data_row($data, 'pm_processmanager', 'filter_on_related', $val, '');
                $val = array(
                    'sugar_id' => $row['id'],
                    'parent_id' => $process_id,
                    'value' => ($i + 1)
                );
                $this->update_sql_data_row($data, $table, 'sequence', $val, '');
            }
        }
        $table = 'pm_process_filter_table';
        if(array_key_exists($table,$data)) {
            foreach ($data['pm_process_filter_table'] as $i => $row) {
                if (empty($row['field_name'])) {
                    unset($data['pm_process_filter_table'][$i]);
                    continue;
                }
                $val = array(
                    'sugar_id' => $row['id'],
                    'parent_id' => $process_id,
                    'value' => ($i + 1)
                );
                $this->update_sql_data_row($data, 'pm_process_filter_table', 'sequence', $val, '');
            }
        }
        //SET EMAIL TEMPLATE ID
        $table = 'pm_process_task_email_defs';
        if(array_key_exists($table,$data)) {
            foreach ($data['pm_process_task_email_defs'] as $i => $row) {
                $val = array(
                    'sugar_id' => $row['id'],
                    'parent_id' => $process_id,
                    'value' => $this->get_email_template_id($row['email_template_name'])
                );
                $this->update_sql_data_row($data, 'pm_process_task_email_defs', 'email_template_id', $val, '');
            }
        }
        $val = array(
            'sugar_id' => $process_id,
            'parent_id' => $process_id,
            'value' => 'Modify'
        );
        $this->update_sql_data_row($data, 'pm_processmanager', 'cancel_on_event', $val, '');
        //Set Assigned User ID
        $val = array(
            'sugar_id' => $process_id,
            'parent_id' => $process_id,
            'value' => $current_user->id
        );
        $this->update_sql_data_row($data, 'pm_processmanager', 'assigned_user_id', $val, '');
        foreach ($data['pm_processmanagerstage'] as $i => $row) {
            $val = array(
                'sugar_id' => $row['id'],
                'parent_id' => $process_id,
                'value' => $current_user->id
            );
            $this->update_sql_data_row($data, 'pm_processmanagerstage', 'assigned_user_id', $val, '');
        }
        foreach ($data['pm_processmanagerstagetask'] as $i => $row) {
            $val = array(
                'sugar_id' => $row['id'],
                'parent_id' => $row['_parent_db'],
                'value' => $current_user->id
            );
            $this->update_sql_data_row($data, 'pm_processmanagerstagetask', 'assigned_user_id', $val, '');
        }
        $table = 'pm_process_defs';
        if (!isset($data[$table])) {
            $sql = "SELECT id FROM $table WHERE process_id = '$process_id'";
            $res = $this->db->query($sql);
            $row = $this->db->fetchByAssoc($res);
            if (!$row) {
                $process_def_id = create_guid();
                $val = array(
                    'sugar_id' => $process_def_id,
                    'parent_id' => $process_id,
                    'value' => $process_def_id
                );
                $this->update_sql_data_row($data, $table, 'id', $val, '');
                $val = array(
                    'sugar_id' => $process_def_id,
                    'parent_id' => $process_id,
                    'value' => $process_id
                );
                $this->update_sql_data_row($data, $table, 'process_id', $val, '');
            } else {
                $process_def_id = $row['id'];
            }
        } else {
            $process_def_id = $data[$table][0]['id'];

            if (empty($data[$table][0]['process_id'])) {
                $val = array(
                    'sugar_id' => $process_def_id,
                    'parent_id' => $process_id,
                    'value' => $process_id
                );
                $this->update_sql_data_row($data, $table, 'process_id', $val, '');
            }
        }
        $process_def = $data[$table][0];
        //SET DEFAULT VALUES
        $values = array(
            'ignore_process_prev_run_check' => 0,
            'process_query_value' => '',
            'process_frequency' => '',
            'process_start_time_day' => '',
            'process_start_day_of_week' => '',
            'process_day_of_month' => '',
            'process_month' => '',
            'route_object' => 0,
            'routing_type' => '',
            'route_to_user' => '',
            'check_login_status' => 0,
            'find_logged_in_user' => 0,
            'route_to_role' => '',
            'route_to_team' => '',
        );
        global $sugar_config;
	      $db_type = $sugar_config['dbconfig']['db_type'];
        foreach ($values as $field => $default) {
            $this->convert_to_default_format($process_def[$field], $default);
            if ($field == 'process_last_run_date'){ 
	            if($db_type == 'mysql'){
	            	$process_def[$field] = '0000-00-00 00:00:00'; 
	            }
	            if($db_type == 'mssql'){
	            	$process_def[$field] = '2016-03-22 03:22:19.059';
	            }
          	}
            $val = array(
                'sugar_id' => $process_def_id,
                'parent_id' => $process_id,
                'value' => (isset($process_def[$field]) || $process_def[$field] == '') ? $process_def[$field] : $default
            );
            $this->update_sql_data_row($data, $table, $field, $val, '');
        }
        $val = array(
            'sugar_id' => $process_def_id,
            'parent_id' => $process_id,
            'value' => $this->escape_sql($process_def['process_query_value'])
        );
        $this->update_sql_data_row($data, $table, 'process_query_value', $val, '');
        $table = 'pm_processmanager';
        $values = array(
            'name' => '',
            'description' => '',
            'deleted' => 0,
            'status' => 'Active',
            'process_object' => NULL,
            'start_event' => 'Create',
            'related_object' => 'Create',
            'filter_on_related' => NULL,
            //'check_delete_modify_cancel' => 0,
        );
        foreach ($data[$table] as $i => $row) {
            foreach ($values as $field => $default) {
                $this->convert_to_default_format($row[$field], $default);
                $val = array(
                    'sugar_id' => $row['id'],
                    'parent_id' => false,
                    'value' => (isset($row[$field]) || $row[$field] == '') ? $row[$field] : $default
                );
                $this->update_sql_data_row($data, $table, $field, $val, '');
            }
        }
        $table = 'pm_processmanagerstagetask';
        $values = array(
            'name' => '',
            //'date_entered' => date('Y-m-d H:i:s', time()),
            'description' => '',
            'deleted' => 0,
            'task_order' => 0,
            'stage_id' => '',
            'email_template_defs_id' => '',
            'calls_defs_id' => '',
            'project_task_defs_id' => '',
            'task_defs_id' => '',
            'task_type' => '',
            'start_delay_type' => '',
            'custom_script' => '',
            'meetings_defs_id' => '',
            'trigger_process_name' => '',
        );
        foreach ($data[$table] as $i => $row) {
            if (empty($row['name'])) unset($data[$table][$i]);
        }
        foreach ($data[$table] as $i => $row) {
            foreach ($values as $field => $default) {
                $this->convert_to_default_format($row[$field], $default);
                $val = array(
                    'sugar_id' => $row['id'],
                    'parent_id' => false,
                    'value' => (isset($row[$field]) || $row[$field] == '') ? $row[$field] : $default
                );
                $this->update_sql_data_row($data, $table, $field, $val, '');
            }
        }
        $table = 'pm_processmanagerstage';
        $values = array(
            'name' => '',
            //'date_entered' => date('Y-m-d H:i:s', time()),
            'description' => '',
            'deleted' => 0,
            'stage_order' => 1,
            'start_delay_minutes' => 0,
            'start_delay_hours' => 0,
            'start_delay_days' => 0,
            'start_delay_months' => 0,
            'start_delay_years' => 0,
            'process_id' => '',
        );
        foreach ($data[$table] as $i => $row) {
            foreach ($values as $field => $default) {
                $this->convert_to_default_format($row[$field], $default);
                $val = array(
                    'sugar_id' => $row['id'],
                    'parent_id' => false,
                    'value' => (isset($row[$field]) || $row[$field] == '') ? $row[$field] : $default
                );
                $this->update_sql_data_row($data, $table, $field, $val, '');
            }
        }
        $table = 'pm_process_task_email_defs';
        $values = array(
            'email_template_name' => '',
            'email_template_id' => '',
            'contact_role' => '',
            'internal_email' => 0,
            'internal_email_to_address' => '',
            'send_email_to_caseopp_account' => 0,
            'send_email_to_object_owner' => 0,
            'email_from_account' => '',
            'send_email_to_contact' => 0,
            'email_queue_campaign_name' => '',
            'send_email_to_email_queue' => 0,
            'email_field' => '',
            'related_module' => '',
            'related_module_email_field' => '',
            'send_email_to_all' => 0,
            'cc_email_to_address' => '',
            'send_email_other_owner' => '',
            'send_email_to_role' => '',
            'send_email_to_team' => '',
            'send_email_to' => '',
            'send_email_cc' => '',
            'send_email_bcc' => '',
        );
        if(array_key_exists($table,$data)) {
            foreach ($data[$table] as $i => $row) {
                foreach ($values as $field => $default) {
                    $this->convert_to_default_format($row[$field], $default);
                    $val = array(
                        'sugar_id' => $row['task_id'],
                        'parent_id' => $row['task_id'],
                        'value' => (isset($row[$field]) || $row[$field] == '') ? $row[$field] : $default
                    );
                    //Handle single quotes
                    $val = str_replace("&#039;","\'",$val);
                    $this->update_sql_data_row($data, $table, $field, $val, '');
                }
            }
        }
        $table = 'pm_process_project_task_defs';
        $values = array(
            'project_task_id' => '',
            'project_task_subject' => '',
            'project_task_status' => '',
            'project_task_priority' => '',
            'project_task_start_date' => 0,
            'project_task_end_date' => 0,
            'assigned_user_id_project_task' => '',
            'project_task_milestone' => 0,
            'project_task_task_number' => 0,
            'project_task_order' => 0,
            'project_task_description' => 0,
        );
        if(array_key_exists($table,$data)) {
            foreach ($data[$table] as $i => $row) {
                foreach ($values as $field => $default) {
                    $this->convert_to_default_format($row[$field], $default);
                    $val = array(
                        'sugar_id' => $row['id'],
                        'parent_id' => $row['project_id'],
                        'value' => (isset($row[$field]) || $row[$field] == '') ? $row[$field] : $default
                    );
                    $this->update_sql_data_row($data, $table, $field, $val, '');
                }
            }
        }
        $table = 'pm_process_task_rest_defs';
        $values = array(
            'rest_url' => '',
            'rest_method' => '',
            'rest_http_query' => '',
            'rest_custom_script' => ''
        );
        if(array_key_exists($table,$data)) {
            foreach ($data[$table] as $i => $row) {
                foreach ($values as $field => $default) {
                    $this->convert_to_default_format($row[$field], $default);
                    $val = array(
                        'sugar_id' => $row['id'],
                        'parent_id' => $row['project_id'],
                        'value' => (isset($row[$field]) || $row[$field] == '') ? $row[$field] : $default
                    );
                    $this->update_sql_data_row($data, $table, $field, $val, '');
                }
                $val = array(
                    'sugar_id' => $row['id'],
                    'parent_id' => $row['project_id'],
                    'value' => $this->escape_sql($row['rest_json'])
                );
                $this->update_sql_data_row($data, $table, 'rest_json', $val, '');

                $val = array(
                    'sugar_id' => $row['id'],
                    'parent_id' => $row['project_id'],
                    'value' => $this->escape_sql($row['rest_headers'])
                );
                $this->update_sql_data_row($data, $table, 'rest_headers', $val, '');
            }
        }

        $table = 'pm_process_task_create_object_defs';
        $values = array(
            'create_object_type' => '',
            'create_object_id' => '',
            'create_object_description' => '',
            'assigned_user_id_create_object' => '',
            'inherit_parent_data' => 0,
            'inherit_parent_relationships' => 0,
            'relate_object_to_parent' => 1,
            'create_record_method' => '',
        );
        if(array_key_exists($table,$data)) {
            foreach ($data[$table] as $i => $row) {
                foreach ($values as $field => $default) {
                    $this->convert_to_default_format($row[$field], $default);
                    $val = array(
                        'sugar_id' => $row['id'],
                        'parent_id' => $row['task_id'],
                        'value' => (isset($row[$field]) || $row[$field] == '') ? $row[$field] : $default
                    );
                    $this->update_sql_data_row($data, $table, $field, $val, '');
                }
            }
        }
        $table = 'pm_process_task_create_by_field_defs';
        $values = array(
            'create_by_field_field' => '',
            'create_by_field_method' => '',
            'create_by_field_by_field' => '',
            'create_by_field_by_value' => '',
            'create_by_field_by_date' => '',
        );
        if(array_key_exists($table,$data)) {
            foreach ($data[$table] as $i => $row) {
                foreach ($values as $field => $default) {
                    $this->convert_to_default_format($row[$field], $default);
                    $val = array(
                        'sugar_id' => $row['id'],
                        'parent_id' => $row['task_id'],
                        'value' => (isset($row[$field]) || $row[$field] == '') ? $row[$field] : $default
                    );
                    $this->update_sql_data_row($data, $table, $field, $val, '');
                }
            }
        }

        $table = 'pm_process_task_notification_defs';
        $values = array(
            'notification_name' => '',
            'notification_description' => '',
            'assigned_user_id_notification' => '',
            'severity_notification' => '',
        );
        if(array_key_exists($table,$data)) {
            foreach ($data[$table] as $i => $row) {
                foreach ($values as $field => $default) {
                    $this->convert_to_default_format($row[$field], $default);
                    $val = array(
                        'sugar_id' => $row['id'],
                        'parent_id' => $row['task_id'],
                        'value' => (isset($row[$field]) || $row[$field] == '') ? $row[$field] : $default
                    );
                    $this->update_sql_data_row($data, $table, $field, $val, '');
                }
            }
        }
        $table = 'pm_process_task_convert_lead_defs';
        $values = array(
            'create_account' => 0,
            'create_contact' => 0,
            'create_opportunity' => 0,
            'create_opportunity_name' => '',
            'create_opportunity_sales_stage' => '',
            'create_opportunity_close_date' => '',
        );
        if(array_key_exists($table,$data)) {
            foreach ($data[$table] as $i => $row) {
                foreach ($values as $field => $default) {
                    $this->convert_to_default_format($row[$field], $default);
                    $val = array(
                        'sugar_id' => $row['task_id'],
                        'parent_id' => $row['task_id'],
                        'value' => (isset($row[$field]) || $row[$field] == '') ? $row[$field] : $default
                    );
                    $this->update_sql_data_row($data, $table, $field, $val, '');
                }
            }
        }
        $table = 'pm_process_task_routing_defs';
        $values = array(
            'routing_type' => '',
            'route_to_user' => '',
            'check_login_status' => 0,
            'find_logged_in_user' => 0,
            'route_to_role' => '',
            'route_to_team' => '',
            'route_to_securitygroups' => '',
        );
        if(array_key_exists($table,$data)) {
            foreach ($data[$table] as $i => $row) {
                foreach ($values as $field => $default) {
                    $this->convert_to_default_format($row[$field], $default);
                    $val = array(
                        'sugar_id' => $row['task_id'],
                        'parent_id' => $row['task_id'],
                        'value' => (isset($row[$field]) || $row[$field] == '') ? $row[$field] : $default
                    );
                    $this->update_sql_data_row($data, $table, $field, $val, '');
                }
            }
        }
        $table = 'pm_process_filter_table';
        $values = array(
            'process_id' => $process_id,
            'status' => '',
            'field_name' => '',
            'field_value' => '',
            'field_operator' => '',
            'sequence' => 0,
            'andorfilterfields' => 'and',
            'group_number' => 1,
            'group_andor' => 'and',
            'field_value_to' => '',
            'compare_to_field' => 0,
            'compare_to_field_name' => '',
        );
        if(array_key_exists($table,$data)) {
            foreach ($data[$table] as $i => $row) {
                foreach ($values as $field => $default) {
                    $this->convert_to_default_format($row[$field], $default);
                    $val = array(
                        'sugar_id' => $row['id'],
                        'parent_id' => $process_id,
                        'value' => (isset($row[$field]) || $row[$field] == '') ? $row[$field] : $default
                    );
                    $this->update_sql_data_row($data, $table, $field, $val, '');
                }
            }
        }
        $table = 'pm_process_related_filter_table';
        $values = array(
            'process_id' => $process_id,
            'status' => '',
            'related_module' => '',
            'related_field_name' => '',
            'related_field_value' => '',
            'related_field_operator' => '',
            'sequence' => 0,
            'relatedandorfilterfields' => 'and',
        );
        if(array_key_exists($table,$data)) {
            foreach ($data[$table] as $i => $row) {
                foreach ($values as $field => $default) {
                    $this->convert_to_default_format($row[$field], $default);
                    $val = array(
                        'sugar_id' => $row['id'],
                        'parent_id' => $row['process_id'],
                        'value' => (isset($row[$field]) || $row[$field] == '') ? $row[$field] : $default
                    );
                    $this->update_sql_data_row($data, $table, $field, $val, '');
                }
            }
        }
        $table = 'pm_process_cancel_filter_table';
        $values = array(
            'process_id' => $process_id,
            'cancel_field_name' => '',
            'cancel_field_value' => '',
            'cancel_field_operator' => '',
            'sequence' => 0,
            'cancelandorfilterfields' => 'and',
            'group_number' => 0,
            'cancel_field_value_to' => '',
            'cancel_compare_to_field' => '',
            'cancel_compare_to_field_name' => '',
        );
        if(array_key_exists($table,$data)) {
            foreach ($data[$table] as $i => $row) {
                foreach ($values as $field => $default) {
                    $this->convert_to_default_format($row[$field], $default);
                    $val = array(
                        'sugar_id' => $row['id'],
                        'parent_id' => $row['process_id'],
                        'value' => (isset($row[$field]) || $row[$field] == '') ? $row[$field] : $default
                    );
                    $this->update_sql_data_row($data, $table, $field, $val, '');
                }
            }
        }
        $table = 'pm_process_task_modify_field_defs';
        $values = array(
            'process_object_modify_field' => '',
            'process_object_modify_field_value' => '',
            'process_object_modify_field_by_field' => '',
            'process_object_modify_field_by_date' => '',
            'process_object_modify_related_object' => '',
            'process_object_modify_related_field' => '',
            'process_object_modify_related_value' => '',
            'process_object_modify_related_field_by_field' => '',
            'process_object_modify_related_field_by_date' => '',
            'related_modify_method' => '',
            'modify_method' => '',
            'process_object_modify_related_object_from_object' => '',
            'process_object_modify_related_field_from_object' => '',
        );
        if(array_key_exists($table,$data)) {
            foreach ($data[$table] as $i => $row) {
                foreach ($values as $field => $default) {
                    $this->convert_to_default_format($row[$field], $default);
                    $val = array(
                        'sugar_id' => $row['id'],
                        'parent_id' => $row['task_id'],
                        'value' => (isset($row[$field]) || $row[$field] == '') ? $row[$field] : $default
                    );
                    $this->update_sql_data_row($data, $table, $field, $val, '');
                }
            }
        }
        //get rid of empty tables
        foreach ($data[$table] as $i => $row) {
            if (empty($row['process_object_modify_field']) && empty($row['process_object_modify_related_field'])) {
                unset($data[$table][$i]);
            }
        }
        $this->data = $data;
    }

    private function escape_sql($str)
    {
        $str = str_replace('&amp;', '&', $str);
        return str_replace('"', '\"', str_replace("'", "\'", html_entity_decode($str, ENT_QUOTES)));
    }

    private function convert_to_default_format(&$val, $default)
    {
        if (is_int($default)) {
            $val = (int)$val;
        } else if (is_float($default)) {
            $val = (float)$val;
        } else if (is_double($default)) {
            $val = (double)$val;
        } else if (is_string($default)) {
            $val = (string)$val;
        } else if (is_null($default) && $val == 'NULL') {
            $val = NULL;
        }
    }

    private function get_email_template_id($name)
    {

        $name = str_replace("&#039;","'",$name);
        $sql = "SELECT id FROM email_templates WHERE deleted = 0 AND name LIKE '" . addslashes($name) . "'";
        $res = $this->db->query($sql);
        if ($row = $this->db->fetchByAssoc($res)) {
            return $row['id'];
        } else {
            return '';
        }
    }

    private function does_rel_exist($rel, $val, $table)
    {
        $sql = "SELECT id FROM $table WHERE " . $rel['parent'] . " = '" . $val['parent_id'] . "' AND " . $rel['sugar'] . " = '" . $val['sugar_id'] . "' AND deleted = 0";
        $res = $this->db->query($sql);
        if ($row = $this->db->fetchByAssoc($res)) {
            return true;
        } else {
            return false;
        }
    }

    //TODO default
    private function update_sql_data_row(&$data, $table, $col, $val, $default, $rel_type = false, $rel = false, $rel_table = false)
    {
        $found = false;
        //if($table == 'pm_processmanager' || $table == 'pm_processmanagerstage' || $table == 'pm_processmanagerstagetask'){
        //	$val['sugar_id'] = $val['parent_id'];
        //	}
        if (isset($data[$table])) {
            foreach ($data[$table] as $i => $row) {
                //if($row['_parent_db'] == $val['parent_id']){
                if ($row['id'] == $val['sugar_id']) {
                    $found = $i;
                    break;
                }
            }
        }
        //Fix for double encoded HTML Strings
        $val['value'] = html_entity_decode(html_entity_decode($val['value']));
        if ($found !== false) {
            $data[$table][$found][$col] = $val['value'];
        } else {
            $new = array();
            if (!empty($val['sugar_id']) && $val['sugar_id'] != 'false') {
                $new['id'] = $val['sugar_id'];
            }
            if ($rel !== false && $rel_type == 'col') {
                if ($val['parent_id'] == 'false' || $val['parent_id'] == false) {
                    /*
                    if(isset($this->parentids[$rel])){
                        $parent_id = $this->parentids[$rel];
                    }else{
                        $parent_id = create_guid();
                        $this->parentids[$rel] = $parent_id;
                    }
                     */
                    //SHOULD NEVER GET HERE
                    $parent_id = '$generate';
                    $new[$rel] = $parent_id;
                } else {
                    if (!in_array($table, $this->ext_tables)) {
                        $new[$rel] = $val['parent_id'];
                    } else {
                        $new[$rel] = $val['sugar_id']; // Use to be sugar_id
                    }
                    $new['_parent_col'] = $rel;
                }
            }
            if ($val['parent_id'] !== false)
                $new['_parent_db'] = $val['parent_id'];
            $new[$col] = $val['value'];
            $data[$table][] = $new;
        }
    }

    private function generate_sql()
    {
        global $current_user;
        $exceptions = array(
            'pm_process_filter_table',
            'pm_process_related_filter_table',
            'pm_process_cancel_filter_table',
            'pm_process_task_modify_field_defs',
            'pm_process_task_create_by_field_defs',
        );
        $main_tables = array(
            'pm_processmanager',
            'pm_processmanagerstage',
            'pm_processmanagerstagetask'
        );
        //Delete the Process if requested
        if (!empty($_POST['delete_process'])) {
            $this->delete_everything();
            return;
        }
        $process_id = $this->data['pm_processmanager'][0]['id'];
        if (empty($_POST['export_process'])) {
            //Remove Filter Fields, Related Filter Fields, Cancel Filter Fields Create by Fields Modify Object and Modify Related Object rows.
            $this->db->query("DELETE FROM pm_process_filter_table WHERE process_id = '$process_id'");
            $this->db->query("DELETE FROM pm_process_related_filter_table WHERE process_id = '$process_id'");
            $this->db->query("DELETE FROM pm_process_cancel_filter_table WHERE process_id = '$process_id'");
            if (isset($this->data['pm_processmanagerstagetask'])) {
                foreach ($this->data['pm_processmanagerstagetask'] as $rows) {
                    foreach ($rows as $x => $row) {
                        if($x == 'id') {
                            $this->db->query("DELETE FROM pm_process_task_modify_field_defs WHERE task_id = '$row'");
                            $this->db->query("DELETE FROM pm_process_task_create_by_field_defs WHERE task_id = '$row'");
                        }
                    }
                }
            }
        }
        //End Remove
        foreach ($this->data as $table => $rows) {
            foreach ($rows as $x => $row) {
                $filtered = $row;
                if ($row['id'] == 'false') {
                    $row['id'] = false;
                }
                $parent_col = isset($row['_parent_col']) ? $row['_parent_col'] : false;
                unset($row['_parent_db']);
                unset($row['_parent_col']);
                unset($filtered['id']);
                $create = true;
                if ($row['id'] !== false) {
                    $checksql = "SELECT $table.id FROM $table WHERE $table.id = '" . $row['id'] . "'";
                    $checkres = $this->db->query($checksql);
                    if ($checkrow = $this->db->fetchByAssoc($checkres)) {
                        $create = false;
                    }
                }
                //Check for existing extension table then get the id.
                if ($parent_col !== false && isset($row[$parent_col]) && !in_array($table, $exceptions)) {
                    $values = array_values($filtered);
                    $search_i = array_search($row[$parent_col], $values);
                    if ($search_i !== false) {
                        //Then an extension table
                        //Find Existing record update row['id'] with found id
                        $keys = array_keys($filtered);
                        $field = $keys[$search_i];
                        $val = $values[$search_i];
                        $sql = "SELECT $table.id FROM $table WHERE $table.$field = '$val'";
                        echo $sql . "\n";
                        $res = $this->db->query($sql);
                        if ($therow = $this->db->fetchByAssoc($res)) {
                            $row['id'] = $therow['id'];
                            $create = false;
                        } else {
                            //	unset($row['id']);
                        }
                    }
                }
                //BUILD SQL STATEMENTS
                if (!empty($_POST['export_process'])) {
                    $create = TRUE;
                }
                if (!$create) {
                    //UPDATE
                    if (in_array($table, $main_tables)) {
                        $row['date_modified'] = date('Y-m-d H:i:s');
                        $row['modified_user_id'] = $current_user->id;
                    }
                    $sweet = $row;
                    unset($sweet['id']);
                    $sql = "UPDATE $table SET " . implode_key_value(',', $sweet) . " WHERE $table.id = '" . $row['id'] . "'";
                } else {
                    if (empty($row['id'])) $row['id'] = create_guid();
                    //$this->data[$table][$x]['id'] = $row['id'];
                    //$this->data[$table][$x]['_create'] = true;
                    //INSERT
                    if (in_array($table, $main_tables)) {
                        $row['date_entered'] = date('Y-m-d H:i:s');
                        $row['date_modified'] = date('Y-m-d H:i:s');
                        $row['created_by'] = $current_user->id;
                        $row['modified_user_id'] = $current_user->id;
                        $row['assigned_user_id'] = $current_user->id;
                        $row['team_id'] = '1';
                        $row['team_set_id'] = '1';
                    }
                    //Check Mysql / MSSQL
                    global $sugar_config;
	                  $db_type = $sugar_config['dbconfig']['db_type'];
                    if($db_type == 'mysql'){                    
                    	$sql = "INSERT INTO $table (`" . implode('`,`', array_keys($row)) . "`) VALUES (" . implode_sql(',', array_values($row)) . ")";
                    }elseif($db_type == 'mssql'){
                    	$sql = "INSERT INTO $table (".implode(', ', array_keys($row)).") VALUES (".implode_sql(", ", array_values($row)).")";
                    	//Double up the single quotes
                    	$sql = str_replace("'","''",$sql);
                    	$sql = str_replace('"',"'",$sql);
                    }
                    else{
                    	$sql = "INSERT INTO $table (`" . implode('`,`', array_keys($row)) . "`) VALUES (" . implode_sql(',', array_values($row)) . ")";
                    }

                }
                if (!empty($_POST['export_process'])) {
                    $esql .= $sql ."; \n";
                }
                else {
                    $res = $this->db->query($sql);
                    echo $sql;
                    echo "\n";
                    if ($res) {
                        echo "Successful \n";
                    } else {
                        echo "Failed \n";
                    }
                }
            }
        }
        if (!empty($_POST['export_process'])) {
            //Go get all the link tables
            $esql = $this->getLinkTables($esql,$process_id);
            $fp = fopen("modules/PM_ProcessManager/export.sql","w");
            fwrite($fp,$esql);
            fclose($fp);
        }
    }

    private function getLinkTables($esql,$process_id){

        $res = $this->db->query("select * from pm_processmmanagerstage where pm_processmanager_ida = '$process_id' and deleted = 0");
        while($row = $this->db->fetchByAssoc($res)){
            $id = $row['id'];
            $date_modified = $row['date_modified'];
            $pm_processmanager_ida = $row['pm_processmanager_ida'];
            $pm_processmanagerstage_idb = $row['pm_processmanagerstage_idb'];
            $sql = "INSERT INTO pm_processmmanagerstage (`id`,`deleted`,`date_modified`,`pm_processmanager_ida`,`pm_processmanagerstage_idb`) VALUES (\"$id\",0,\"$date_modified\",\"$pm_processmanager_ida\",\"$pm_processmanagerstage_idb\") ";
            $esql .= $sql ."; \n";
            //Now go and get all the tasks related to this stage
            $rest = $this->db->query("select * from pm_processmgerstagetask where pm_processmanagerstage_ida = '$pm_processmanagerstage_idb' and deleted = 0");
            while($rowt = $this->db->fetchByAssoc($rest)){
                $id = $rowt['id'];
                $date_modified = $rowt['date_modified'];
                $pm_processmanagerstage_ida = $rowt['pm_processmanagerstage_ida'];
                $pm_processmanagerstagetask_idb = $rowt['pm_processmanagerstagetask_idb'];
                $sql = "INSERT INTO pm_processmgerstagetask (`id`,`deleted`,`date_modified`,`pm_processmanagerstage_ida`,`pm_processmanagerstagetask_idb`) VALUES (\"$id\",0,\"$date_modified\",\"$pm_processmanagerstage_ida\",\"$pm_processmanagerstagetask_idb\") ";
                $esql .= $sql ."; \n";
            }
        }
        return $esql;
    }

    private function delete_everything(){
        $process_id = $this->data['pm_processmanager'][0]['id'];
        //Get the Process Stages
        $qs = "select pm_processmanagerstage_idb from pm_processmmanagerstage where pm_processmanager_ida = '$process_id' and deleted = 0";
        $ress = $this->db->query($qs);
        while($rows = $this->db->fetchByAssoc($ress)){
            $stid = $rows['pm_processmanagerstage_idb'];
            $qt = "select pm_processmanagerstagetask_idb from pm_processmgerstagetask where pm_processmanagerstage_ida = '$stid' and deleted = 0";
            $rest = $this->db->query($qt);
            while($rowt = $this->db->fetchByAssoc($rest)){
                $tid = $rowt['pm_processmanagerstagetask_idb'];
                $qd = "delete from pm_process_task_routing_defs where task_id = '$tid'";
                $this->db->query($qd);
                $qd = "delete from pm_process_task_rest_defs where task_id = '$tid'";
                $this->db->query($qd);
                $qd = "delete from pm_process_task_notification_defs where task_id = '$tid'";
                $this->db->query($qd);
                $qd = "delete from pm_process_task_modify_field_defs where task_id = '$tid'";
                $this->db->query($qd);
                $qd = "delete from pm_process_task_email_defs where task_id = '$tid'";
                $this->db->query($qd);
                $qd = "delete from pm_process_task_create_object_defs where task_id = '$tid'";
                $this->db->query($qd);
                $qd = "delete from pm_process_task_create_by_field_defs where task_id = '$tid'";
                $this->db->query($qd);
                $qd = "delete from pm_process_task_convert_lead_defs where task_id = '$tid'";
                $this->db->query($qd);
                $qd = "delete from pm_processmanagerstagetask where id = '$tid'";
                $this->db->query($qd);
                $qd = "delete from pm_processmgerstagetask where pm_processmanagerstagetask_idb = '$tid'";
                $this->db->query($qd);
            }
            //Now delete the stage
            $qd = "delete from pm_processmanagerstage where id = '$stid'";
            $this->db->query($qd);
            $qd = "delete from pm_processmmanagerstage where pm_processmanagerstage_idb = '$stid' and pm_processmanager_ida = '$process_id' ";
            $this->db->query($qd);
            $qd = "delete from pm_process_stage_waiting_todo where stage_id = '$stid' and process_id = '$process_id'";
            $this->db->query($qd);
        }
        //Now get rid of the Process Tables and defs
        $qd = "delete from pm_processmanager where id = '$process_id' ";
        $this->db->query($qd);
        $qd = "delete from pm_process_defs where process_id = '$process_id' ";
        $this->db->query($qd);
        //pm_process_filter_table
        $qd = "delete from pm_process_filter_table where process_id = '$process_id' ";
        $this->db->query($qd);
        //Cancel Filter Table
         $qd = "delete from pm_process_cancel_filter_table where process_id = '$process_id' ";
        $this->db->query($qd);
    }
}

?>
