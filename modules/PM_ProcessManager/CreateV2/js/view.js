var PMLogic = (function () {
    var DEFAULT_HELP_TEXT = 'To edit a process, double click on any box in the process. You will then be able to change the value for that portion of the Process';
    var activeConnections = 0; // This will allow you to set loading concurrently on multiple function and it wont stop until its done

    var ajax_url = 'index.php?module=PM_ProcessManager&action=CreateV2AJAX&to_pdf=1';

    function init_stuff() {
        resize_iframe();
        $(".draggable").draggable({
            revert: function (dropped) {
                if ($(dropped).hasClass('current')) {
                    $(this).css('border', 'none');
                    $(this).empty();
                    return false;
                } else {
                    return true;
                }
            },
            drag: function (event, ui) {
                $(this).clone();
            },
            cursor: 'move',
            helper: 'clone',
            appendTo: 'body'
        });
        $(".droppable").droppable({
            tolerance: 'pointer',
            drop: function (event, ui) {
                save_item(
                    this,
                    ui.draggable
                );
                return true;
            },
            accept: function () {
                return $(this).hasClass('current');
            }
        });
        $(".master-draggable").draggable({
            revert: true,
            start: function (event, ui) {
                $('.master-droppable[data-name="' + $(this).attr('data-val') + '"]').addClass('highlighted');
            },
            stop: function (event, ui) {
                $('.master-droppable[data-name="' + $(this).attr('data-val') + '"]').removeClass('highlighted');
            },
            cursor: 'move',
            helper: 'clone',
            appendTo: 'body'
        });
        $(".master-droppable").droppable({
            tolerance: 'pointer',
            drop: function (event, ui) {
                add_master_flow(
                    this,
                    ui.draggable
                );
                return true;
            },
            accept: function (draggable) {
                return $(this).is('.master-droppable[data-name="' + $(draggable).attr('data-val') + '"]');
            }
        });
        $('.sub_flow').unbind('dblclick');
        $('.sub_flow').dblclick(function () {
            var is_custom = $(this).find('.custom_field:not([type="checkbox"])');
            if (is_custom.length > 0)
                return;
            var msg = "Do you want to change this value? \n Warning this will change all values after this?";
            if (confirm(msg)) {
                clear_until_subflow(this);
            }
        });
        $('.donebtn').unbind('click');
        $('.donebtn').click(function () {
            var sub_flow = $(this).parents('.sub_flow');
            if (sub_flow && sub_flow.length > 0) {
                $('.sub_flow').each(function (i, item) {
                    $(item).removeClass('current');
                });
                sub_flow.addClass('current');
            }
            get_next_sub_flow_imp({
            }, function (next) {
                if (next)
                    get_pallet();
            });
        });
        $('.save_everything').unbind('click');
        $('.save_everything').click(function () {
            loading(true);
            save_everything();
        });
        $('.duplicate_everything').unbind('click');
        $('.duplicate_everything').click(function () {
            loading(true);
            save_everything(true,false,false);
        });
        $('.export_everything').unbind('click');
        $('.export_everything').click(function () {
            loading(true);
            save_everything(false,true,false);
        });
        $('.delete_everything').unbind('click');
        $('.delete_everything').click(function () {
            loading(true);
            save_everything(false,false,true);
        });
        $('.go_to_process_reports').unbind('click');
        $('.go_to_process_reports').click(function () {
            var pid = get_process_id();
            if (!pid || pid.length == 0) {
                alert('You must save the Process first.');
                return false;
            }
            parent.window.location.href = 'index.php?module=PM_ProcessManager&action=ProcessReports&process_id=' + pid;
        });

        $('.cancel_everything').unbind('click');
        $('.cancel_everything').click(function () {
            window.history.back()
        });

        $('.edit_master').unbind('click');
        $('.edit_master').click(function () {
            edit_master($(this).attr('data-id'));
        });

        $('.delete_master').unbind('click');
        $('.delete_master').click(function () {
            delete_master(this);
        });

        $('#add_group').unbind('click');
        $('#add_group').click(function () {
            create_new_group();
        });

        $('.filter-field-group').unbind('change');
        $('.filter-field-group').change(function () {
            var item = $(this);
            var val = item.find('option:selected').val();
            if (!(val && val.length > 0)) {
                //It is blank, create group
                create_new_group();
            } else {
                regroup_filter_fields();
            }
        });

        $('.expand').unbind('click');
        $('.expand').click(function () {
            m = $(this).parents('.master_flow');
            remove_read_only(m);
            m.find('.collapse').show();
            $(this).hide();
        });

        $('.collapse').unbind('click');
        $('.collapse').click(function () {
            m = $(this).parents('.master_flow');
            set_read_only(m);
            m.find('.expand').show();
            $(this).hide();

        });

        $('.expandff').unbind('click');
        $('.expandff').click(function () {
        	   m = $(this).parents('.master_flow');
             remove_read_only( $('.filterfields'));
              m.find('.collapseff').show();
              m.find('.expandff').hide();
        });

        $('.collapseff').unbind('click');
        $('.collapseff').click(function () {
        	    m = $(this).parents('.master_flow');
        	    set_read_only($('.filterfields'));
							//$('.filterfields').slideToggle('slow');
							m.find('.expandff').show();
							m.find('.collapseff').hide();
        });

        $('.expandrff').unbind('click');
        $('.expandrff').click(function () {
        	   m = $(this).parents('.master_flow');
        	   remove_read_only( $('.relatedfilterfields'));
              m.find('.collapserff').show();
              m.find('.expandrff').hide();
        });

        $('.collapserff').unbind('click');
        $('.collapserff').click(function () {
        	    m = $(this).parents('.master_flow');
        	    set_read_only($('.relatedfilterfields'));
							m.find('.expandrff').show();
							m.find('.collapserff').hide();
        });

        $('.expandcff').unbind('click');
        $('.expandcff').click(function () {
        	   m = $(this).parents('.master_flow');
        	   remove_read_only( $('.cancelfilterfields'));
             //$('.cancelfilterfields').slideToggle('slow');
              m.find('.collapsecff').show();
              m.find('.expandcff').hide();
        });

        $('.collapsecff').unbind('click');
        $('.collapsecff').click(function () {
        	    m = $(this).parents('.master_flow');
        	    set_read_only($('.cancelfilterfields'));
							//$('.cancelfilterfields').slideToggle('slow');
							m.find('.expandcff').show();
							m.find('.collapsecff').hide();
        });

        $('.custom_field.date').datepicker();
        $('.custom_field.datetime').datetimepicker();

        //PM 3.0 - Hide Delete Stage and Delete Task if no stages or tasks - or brand new Create Process

        $.each([ //each is a jquery function with two arrays
            {
                master: 'stages',
                name: 'stage'
            },
            {
                master: 'tasks',
                name: 'task'
            },
            {
                master: 'main_process',
                name: 'process_cancel_event'
            },

        ], function (x, item) { //x is the iterator, first paramater - item represents the master  and name
            $('.master_flow[data-name="' + item.master + '"]').each(function (i, master_flow) {
                master_flow = $(master_flow);

                var sub_flow = master_flow.find('.sub_flow[data-name="' + item.name + '_name"]');

                if (sub_flow.size()) { //if 1 or greate does a subflow exist for this name

                    var value = get_value_from_sub_flow(sub_flow, 'custom'); //get the actual value for this subflow

                    if (value && value.length) { //if there is an actual data in the stage or task name then show else hide
                        return master_flow.find('.delete_master').show();
                    }else{
                        value = get_value_from_sub_flow(sub_flow); //get the actual value for this subflow
                        if(item.name == 'stage'){
                            if(value != "Set Stage Order") {
                                $(this).find("[data-name='parent']").text('Stage: ' + value);
                            }
                        }
                        if(item.name == 'task'){
                            $(this).find("[data-name='parent']").text('Task: ' + value );
                        }
 ;                       if (value && value.length) { //if there is an actual data in the stage or task name then show else hide
                            return master_flow.find('.delete_master').show();
                        }
                    }
                }
                master_flow.find('.delete_master').hide();

            });
        });

        //Hide Cancel
        $.each([
                {
                    master: 'main_process',
                    name: 'cancel_process_object_field'
                },
            ], function (x,item){
                $('.master_flow[data-name="' + item.master + '"]').each(function (i, master_flow){
                    master_flow = $(master_flow);
                    var sub_flow = master_flow.find('.sub_flow[data-name="' + item.name + '"]');
                    var value = get_value_from_sub_flow(sub_flow, 'basic');
                    if(value && value.length){
                        return;
                    }else {
                        if (cancel == 'false') {
                            $('div[data-name="process_cancel_event"]').hide();
                            return;
                        }
                    }
                });
            }
        );

        loading(false);
    }

    function create_new_group() {
        $.post(ajax_url,
            {
                method: 'get_next_group',
                last_group: $('#groups-pallet .group:last').attr('data-number')
            }
        )
            .success(function (res) {
                $('#groups-pallet ul').append(res);
                populate_group_dropdowns();
                regroup_filter_fields();
            })
            .error(function (err) {
                console.log(err);
                alert('An Error Occurred.');
            });
    }

    function clear_until_subflow(sub_flow) {
        $('.sub_flow').each(function (i, item) {
            $(item).removeClass('current');
        });
        $(sub_flow).addClass('current');
        $(sub_flow).fadeIn();
        if ($(sub_flow).attr('data-clear-until') == '0') {
            get_pallet();
            return true;
        }
        var is_multiple = false;
        var this_parent = $(sub_flow).parents('.parent_flow');
        if (this_parent.find('.next_options').length > 0 || this_parent.attr('data-name') != 'process') {
            is_multiple = true;
        }
        if (this_parent.attr('data-is-mulitple') == 'true') {
            is_multiple = true;
        }

        var is_after = false;
        $(sub_flow).parents('.master_flow').find('.parent_flow:visible > .sub_flow')
            .filter(function (i) {
                if (is_multiple) {
                    if ($(this).parents('.parent_flow').is(this_parent)) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return true;
                }
            })
            .each(function (i, item) {
                if (is_after) {
                    $(item).removeClass('current');
                    $(item).fadeOut();
                    $(item).next('.connector').hide();
                    $(item).find('span.value').empty();
                }
                if ($(item).is(sub_flow))
                    is_after = true;
            }).promise().done(function () {
                get_pallet();
            });
    }

    function is_last_sub_in_parent(sub_flow) {
        return $(sub_flow).parents('.parent_flow').find('.sub_flow:visible').length == 1;
    }

    function remove_parent_flow(sub_flow) {
        $(sub_flow).parents('.parent_flow').hide();
    }

    function resize_iframe() {
        return; //DISABLED
        var height = $('html').height();
        $('iframe', window.parent.document).height(height + 'px');
    }

    function edit_master(id) {
        var master = $('.master_flow[data-id="' + id + '"]');
        remove_read_only(master);
        var first = master.find('.sub_flow:first')
        first.addClass('current')
        get_next_sub_flow_imp({
            force_next: false,
            force_next_master: false,
            edit: true,
            master: master
        }, function (next) {
            if (next)
                get_pallet();
        });
    }

//Called when dragging from Add To Process
    function add_master_flow(dropped, dragged) {
        var drop_name = $(dropped).attr('data-name');
        var drag_name = $(dragged).attr('data-val');
        var child_name = $(dragged).attr('data-child');
        $.post(ajax_url,
            {
                method: 'get_rendered_flow'
            }
        )
            .success(function (res) {
                if (($(dropped).parents('.master_flow').length > 0)) {
                    var is_child = true;
                } else {
                    var is_child = false;
                }
                var res_master = $(res).find('.master_flow[data-name="' + drag_name + '"]');
                var data = {};
                if (is_child) {
                    var mast = $(dropped).parents('.master_flow');
                    var stage;
                    if (mast.attr('data-name') == 'stages') {
                        stage = mast;
                    } else {
                        stage = mast.prevAll('.master_flow[data-name="stages"]').first();
                    }
                    data.parent_id = stage.attr('data-sugar-id');
                    data.sugar_id = $(res_master).attr('data-sugar-id');
                } else {
                    data.parent_id = get_process_id();
                    data.sugar_id = $(res_master).attr('data-sugar-id');
                }
                var master = duplicate_flow(
                    $(res).find('.master_flow[data-name="' + drag_name + '"]'),
                    data,
                    true
                );
                if (is_child) {
                    //Add to bottom of master flow
                    $(dropped).parents('.master_flow').after(master);
                } else {
                    //Add to bottom of everything
                    if (child_name) master = $(master).append('<div class="master-droppable" data-name="' + child_name + '" ></div>').wrap('<span/>').parent().html();
                    $(dropped).before(master);
                }
                var new_master = $('.master_flow[data-id="' + $(master).attr('data-id') + '"]');
                new_master.fadeIn();
                var first = new_master.find('.sub_flow:first')
                first.addClass('current')
                get_next_sub_flow_imp({
                    edit: true,
                    master: new_master
                }, function (next) {
                    if (next)
                        get_pallet();
                });
            })
            .error(function (err) {
                console.log(err);
                alert('An Error Occurred.');
            });
    }

    function get_rendered_flow(cb) {
        $.ajax({
            type: 'POST',
            url: ajax_url,
            data: {
                method: 'get_rendered_flow'
            },
            success: function (res) {
                cb(res);
            },
            error: function (err) {
                console.log(err);
                alert('An Error Occurred');
            },
            dataType: 'html',
            async: false
        });
    }

    function create_guid(cb) {
        $.ajax({
            type: 'POST',
            url: ajax_url,
            data: {
                method: 'get_guid'
            },
            success: function (res) {
                cb(res);
            },
            error: function (err) {
                console.log(err);
                alert('An Error Occurred');
            },
            dataType: 'html',
            async: false
        });
    }

    function regroup_filter_fields() {
        var ff = $('.parent_flow[data-name="filter_fields"]');
        var html = $('<div id="temp-html"></div>');
        ff.sortElements(function (a, b) {
            var num = parseInt($(a).find('.filter-field-group option:selected').val());
            var num2 = parseInt($(b).find('.filter-field-group option:selected').val());
            if (num == num2) {
                return 0;
            }
            if (num > num2) {
                return 1;
            } else {
                return -1;
            }
        });
        ff.each(function (i, item) {
            f = $(item);
            f.find('.filter-field-color').remove();
            var num = $(f).find('.filter-field-group option:selected').val();
            var color = $('#groups-pallet .group[data-number="' + num + '"] .group-color').attr('data-color');
            f.prepend("<div class='filter-field-color' style='background-color: " + color + ";'></div>");
        });
        init_stuff();
    }

    function delete_master(obj) {
        n = $(obj).attr('data-name');
        m = $(obj).parents('.master_flow');
        sugar_id = m.attr('data-sugar-id');
        if (n == 'stages') {
            if (!confirm("Deleting this stage will permanently remove it and all related tasks. Are you sure you want to continue?")) return;
            tasks = $('.master_flow[data-parent-id="' + sugar_id + '"][data-name="tasks"]');
        } else {
            if (!confirm("Deleting this task remove it permanently. Are you sure you want to continue?")) return;
            tasks = m;
        }
        if (tasks.length > 0) {
            $(tasks).each(function (i, task) {
                task = $(task);
                $.post(ajax_url,
                    {
                        method: 'delete_task',
                        sugar_id: task.attr('data-sugar-id'),
                        parent_id: task.attr('data-parent-id')
                    }
                )
                    .success(function (response) {
                        task.slideUp("normal", function () {
                            $(this).remove();
                        });
                    })
                    .error(function (err) {
                        console.log(err);
                        alert('An Error Occurred.');
                    });
            });
        }
        if (n == 'stages') {
            $.post(ajax_url,
                {
                    method: 'delete_stage',
                    sugar_id: m.attr('data-sugar-id'),
                    parent_id: m.attr('data-parent-id')
                }
            )
                .success(function (response) {
                    m.slideUp("normal", function () {
                        $(this).remove();
                    });
                })
                .error(function (err) {
                    console.log(err);
                    alert('An Error Occurred.');
                });
        }
    }

    function save_item(dropped, dragged) {
        loading(true);
        var drop_name = $(dropped).attr('data-name');
        var save_function = $(dropped).attr('data-save');
        var drag_name = $(dragged).attr('data-val');
        if (!drop_name || !drag_name || !save_function) {
            console.log('Invalid Save Item Request');
            return;
        }
        $.post(ajax_url,
            {
                method: save_function,
                drop_name: drop_name,
                drag_name: drag_name
            }
        )
            .success(function (response) {
                $(dropped).find('span.value').text(response);
                init_stuff();
                get_next_sub_flow_imp({

                }, function (next) {
                    if (next)
                        get_pallet();
                });
                //get_next_sub_flow(function(next){
                //	if(next !== false)
                //		get_pallet();
                //});
            })
            .error(function (err) {
                console.log(err);
                alert('An Error Occurred.');
            });
    }

    function get_sub_flow_help(sub_flow) {
      if(!sub_flow || !sub_flow.length || !sub_flow.hasClass('current')) {
        return DEFAULT_HELP_TEXT
      }
      return sub_flow.find('.help').text()
    }


    function get_pallet() {
        get_current_sub_flow(function (sub_flow) {
            if(!sub_flow || !sub_flow.length) {
              $('#pallet').html('<span class="help"></span>');
              $('#pallet').children('.help').text(get_sub_flow_help(sub_flow));
              return
            }
            if ($(sub_flow).attr('data-change').length > 0) {
                //Call Change Function
                var strFun = $(sub_flow).attr('data-change');
                var strParam = $(sub_flow).attr('data-id');
                var fn = PMEvents[strFun];
                fn(strParam);

                loading(false);
                return;
            }
            if ($(sub_flow).attr('data-pallet') == '0') {
                $('#pallet').html('<span class="help"></span>');
                $('#pallet').children('.help').text(get_sub_flow_help(sub_flow));
            } else {
                var method = $(sub_flow).attr('data-pallet-cb');
                $.post('index.php?module=PM_ProcessManager&action=CreateV2AJAX&to_pdf=1', { method: method, process_id: get_process_id() })
                    .success(function (html) {
                        $('#pallet').html(html);
                        $('#pallet').children('.help').text(get_sub_flow_help(sub_flow));
                        init_stuff();
                    })
                    .error(function (err) {
                        console.log(err);
                        alert('An Error Occurred.');
                    });
            }
        });
    }

    function update_help(sub_flow) {
        var pallet = $('#pallet');
        pallet.find('.help').remove();
        pallet.append('<span class="help"></span>');
        pallet.children('.help').text(get_sub_flow_help(sub_flow));
    }


    function get_current_sub_flow(cb) {
        var sub_flow = $('.sub_flow.current:visible:last');
        if (!sub_flow || sub_flow.length == 0) {
          if (is_new_process()) {
            sub_flow = $('.sub_flow:visible:last');
          }
        }
        if (sub_flow && sub_flow.length) {
          $('.sub_flow').each(function (i, item) {
              $(item).removeClass('current');
          });
          sub_flow.addClass('current');
        }

        cb(sub_flow);
    }


    function get_next_sub_flow_imp(data, cb) {
        var current = $('.sub_flow.current');
        $('.sub_flow').each(function (i, item) {
            $(item).removeClass('current');
        });
        if (!current || current.length == 0) {
            current = $('.sub_flow:visible:last');
        }
        current.addClass('current');
        var current_master = current.parents('.master_flow');
        var current_parent = current.parents('.parent_flow');

        var doneWithParent = false;

        var nextSubs = current.nextAll('.sub_flow');

        function isValid(nextSub){
            var text = nextSub.find('.value').text();
            var valid = validate_sub_show_if(nextSub);

            if(valid && text && text.length){
                return true;
            }
            return false;
        }

        if(nextSubs.length){
            nextSubs.each(function(i, item){
                if(isValid($(item))){
                    doneWithParent = true;
                }
            });
        }else{
            doneWithParent = true;
        }

        var next;
        if(doneWithParent){
            current_parent.nextAll().each(function(i, item){
                if(next) return;

                var el = $(item);
                if(el.hasClass('actions') || ( el.is(':hidden') && el.hasClass('parent_flow') && validate_show_if(el))){
                    next = el;
                }
            });
        }

        if(next && next.hasClass('actions')){
            current.addClass('current');
            return;
        }

        //Remove Empty Parent Flow
        var last = $(current_parent).find('.sub_flow:visible').last();
        var first = $(current_parent).find('.sub_flow:visible').first();
        if (last && first && last.is(first)) {
            var val = get_sub_flow_val(first);
            if (val.length == 0) {
                current_parent.attr('data-closed', '1');
                current_parent.fadeOut();
                current_parent.next('.next_options').hide();
                current_parent.after(current_parent.find('.empty_options').show().clone());
            }
        }
        //Now we have the current sub_flow
        //Check if we are passing in the master
        // -- If we aren't the get the current master flow
        if (typeof data.force_next == 'undefined') {
            data.force_next = false;
        }
        //Force next master probably isn't needed if you pass in the master
        if (typeof data.force_next_master == 'undefined') {
            data.force_next_master = false;
        }
        if (typeof data.master == 'undefined') {
            data.master = current_master;
        }
        if (typeof data.edit == 'undefined') {
            data.edit = false;
        }
        var found = false;
        var sweet = false;
        var is_after = false;
        //If force next master get next master flow
        if (data.force_next_master) {
            data.master = data.master.next('.master_flow');
            is_after = true;
        }
        //If edit then return the first last visible sub flow
        if (data.edit) {
            var sub = $(data.master).find('.sub_flow:visible:last');
            if (!sub || sub.length == 0)
                sub = $(data.master).find('.sub_flow:hidden:first');
            if (sub) {
                current.removeClass('current');
                parent_flow = sub.parents('.parent_flow');
                parent_flow.fadeIn();
                sub.addClass('current');
                sub.fadeIn('fast');
                sub.next('.connector').show();
                found = sub;
                sweet = true;
            }
        }
        //For the current master obj. Filter out bad sub flows
        while (!sweet) {

            $(data.master).find('.sub_flow').each(function (i, item) {
                if (found) return;
                var sub = $(item);
                var val = get_sub_flow_val(sub);

                if (!is_after) {
                    //Check if it is after
                    //If not, return
                    if (sub.is(current)) {
                        is_after = true;
                    }
                    return;
                }

                //We are now after the current obj
                //Now validate sub flow
                //Check Parent Object to see if validated
                //Check Sub to see if valid
                var parent_flow = sub.parents('.parent_flow');

                if (data.force_next) {
                    //If parent isn't after then return
                    if (parent_flow.is(current_parent)) return
                }
                if (validate_show_if(parent_flow) && validate_sub_show_if(sub)) {
                    parent_flow.fadeIn();
                    sub.addClass('current');
                    sub.fadeIn('fast');
                    sub.next('.connector').show();
                    found = sub;
                    return false;
                }
            });

            if (!found) {
                sweet = false;
                new_master = data.master.next('.master_flow');
                if (!new_master || new_master.length == 0) {
                    if (data.master.attr('data-name') == 'stages') {
                        alert("Please drag a task from the right column pallet before you can continue.");
                    }
                    sweet = true;
                }
                data.master = new_master;
            } else {
                sweet = true;
            }
        }
        redo_options();
        if (found) {
            cb(true);
        } else {
            cb(false);
        }
    }

    function go_to_specific_flow(data, cb) {
        var master, parent, sub_flow;

        if(data.master && data.master.length){

            master = data.master;
            parent = master.find('.parent_flow:first');
            sub_flow = parent.find('.sub_flow:first');

        }else if(data.parent && data.parent.length){

            parent = data.parent;
            master = parent.parents('.master_flow');
            sub_flow = parent.find('.sub_flow:first');

        }else if(data.sub_flow && data.sub_flow.length){

            sub_flow = data.sub_flow;
            master = data.sub_flow.parents('.master_flow');
            parent = data.sub_flow.parents('.parent_flow');

        }

        if(validate_show_if(parent) &&
            validate_sub_show_if(sub_flow)){

            master.show();
            parent.show();
            $('.sub_flow.current').removeClass('current');
            sub_flow.fadeIn('fast');
            sub_flow.addClass('current');
            sub_flow.next('.connector').show();
            get_pallet(cb || function(){});

        }
    }

    function get_next_sub_flow(cb, force_next, force_next_master, master_flow) {
        var last_parent_flow = $('.parent_flow:visible').last();
        if (force_next_master == undefined) {
            force_next_master = false;
        }
        if (force_next == undefined) {
            force_next = false;
        }
        if (master_flow == undefined) {
            master_flow = last_parent_flow.parents('.master_flow');
            go_to_master = false;
        } else {
            //Patch for header hidding
            $('.parent_flow[data-closed="0"][data-header="1"]:hidden').show();
            go_to_master = true;
        }
        if (force_next_master) {
            hide_master_flow(master_flow);
        }
        if (go_to_master) {
            master_flow.attr('data-closed', '0');
        }
        if (last_parent_flow && (last_parent_flow.find('.sub_flow[data-failed-show-if="0"]:hidden').length == 0 || force_next || go_to_master)) {
            var parent_flows = $('.parent_flow[data-closed="0"][data-header="0"]').filter(function () {
                if (!go_to_master)
                    return $(this).parents('.master_flow').attr('data-closed') == '0' && $(this).is(':hidden');
                else
                    return master_flow.attr('data-id') == $(this).parents('.master_flow').attr('data-id');
            });
            var next_parent_flow = parent_flows.filter('.parent_flow').first();
            for (var i = 0; i < parent_flows.length; i++) {
                if (i < parent_flows.length) {
                    if (!validate_show_if(next_parent_flow)) {
                        next_parent_flow = $(parent_flows[i]);
                    }
                }
            }
            next_parent_flow.fadeIn('fast');
        }

        if (next_parent_flow) {
            var next_master_flow = next_parent_flow.parents('.master_flow');

            if (next_master_flow.attr('data-id') !== master_flow.attr('data-id')) {
                hide_master_flow(master_flow);
            }

        }

        //Check if last parent flow is blank then hide parent flow
        if (last_parent_flow && last_parent_flow.find('.sub_flow:visible').length == 1 && !go_to_master) {
            var first_subflow_value = last_parent_flow.find('.sub_flow:visible').first().find('span.value').text();
            if (!(first_subflow_value && first_subflow_value.length > 0)) {
                last_parent_flow.attr('data-closed', '1');
                last_parent_flow.fadeOut();
                last_parent_flow.next('.next_options').hide();
                last_parent_flow.after(last_parent_flow.find('.empty_options').show().clone());
            }
        }

        if (force_next) {
            var current_obj = next_parent_flow.find('.sub_flow:hidden').first();
        } else {
            var current_obj = last_parent_flow.find('.sub_flow:visible').last();
        }

        if (next_parent_flow && next_parent_flow.length > 0) {
            //var new_objs = next_parent_flow.find('.sub_flow:hidden');
            var new_objs = getElementsAfter(next_parent_flow, '.sub_flow', current_obj);
            if (new_objs.length == 0) {
                new_objs = next_parent_flow.find('.sub_flow:hidden');
            }
            //var new_objs = next_parent_flow.find('.sub_flow:hidden').filter();
        } else {
            var new_objs = getElementsAfter(last_parent_flow, '.sub_flow', current_obj);
            //var new_objs = current_obj.nextAll('.sub_flow').filter(function(){
            //	return $(this).parents('.parent_flow').is(last_parent_flow);
            //});
        }
        redo_options();
        var new_obj = new_objs.filter('.sub_flow').first();
        for (var i = 0; i < new_objs.length; i++) {
            if (!validate_sub_show_if(new_obj)) {
                new_obj = $(new_objs[i]);
                new_obj.attr('data-failed-show-if', '1');
            } else {
                new_obj.attr('data-failed-show-if', '0');
            }
            if (i == (new_objs.length - 1)) { // If the end of the loop
                if (!validate_sub_show_if(new_obj)) {
                    new_obj.attr('data-failed-show-if', '1');
                    cb(false);
                    return;
                } else {
                    new_obj.attr('data-failed-show-if', '0');
                }
            }
        }

        if (current_obj.not(new_obj)) {
            $('.sub_flow').each(function (i, item) {
                $(item).removeClass('current');
            });
            //current_obj.addClass('current');
            //Only go on to next obj if last one is full and new obj is defined
            var cur_obj_val = current_obj.find('span.value').html();
            if (new_obj && new_obj.length > 0 && (cur_obj_val && cur_obj_val.length > 0)) {
                new_obj.addClass('current');
                new_obj.fadeIn('fast');
                new_obj.next('.connector').show();
            } else {
                current_obj.addClass('current');
                current_obj.fadeIn('fast');
                current_obj.next('.connector').show();
            }
        } else {
            new_obj = false;
        }
        cb(new_obj);
    }

    function does_parent_flow_have_values(obj) {
        var test = false;
        $(obj).find('.sub_flow').each(function (i, item) {
            var sub = $(item);
            var val = get_sub_flow_val_by_id(sub.attr('data-id'));
            if (val.length > 0) test = true;
        });
        return test;
    }

    function hide_master_flow(master_flow) {
        master_flow.attr('data-closed', '1');
        master_flow.find('.edit_master').show();
        master_flow.each(function (i, item) {
            item = $(item);
            set_read_only(item);
        });
    }

    function remove_read_only(master_flow) {
        loading(true);
        master_flow.attr('data-closed', '0');
        master_flow.find('.sub_flow[data-readonly="1"]').each(function (i, item) {
            $(item).attr('data-readonly', '0');
            $(item).parents('.parent_flow:hidden').slideDown('fast');
            $(item).show();
        });
        if (master_flow.attr('data-name') == 'main_process') {
            master_flow.prev('.readonly').remove();
        }
        master_flow.find('.readonly').remove();
        init_stuff();
    }

    function set_read_only(master_flow) {
        loading(true);
        master_flow.attr('data-closed', '1');
        master_flow.find('.readonly').remove();
        master_flow.find('.sub_flow:visible').each(function (i, item) {
            $(item).attr('data-readonly', '1');
        });
        var parents = master_flow.find('.parent_flow');
        var first = parents.first();
        var str = "";
        if (master_flow.attr('data-name') == 'main_process') {
            master_flow.prev('.readonly').remove();
            str = str + ' Name >> ' + $('#name').val().toString() + "; ";
            str = str + ' Description >> ' + $('#description').val().toString() + "; ";
            str = str + ' Active >> ' + $('#active').find('option:selected').val().toString() + "; ";
            str = str + ' ' + $('#save_everything').show().wrap('<p/>').parent().html();
            first = parents.next('.parent_flow').first();
            master_flow.prepend("<div class='readonly'>" + str + "</div>");
        }
        parents.first().each(function (i, par) {
            str = "";
            $(par).find('.sub_flow:visible').each(function (i, item) {
                item = $(item);
                var custom_field = item.find('.custom_field');
                type = (custom_field && custom_field.length > 0) ? 'custom' : 'text';
                value = get_value_from_sub_flow(item, type);
                if (value.length) {
                    title = item.find('label').text();
                    str = str + " <strong>" + title + "</strong> = '" + value + "'; ";
                }
            });
            if (str.length) master_flow.find('.master-header').after("<div class='readonly'>" + str + "</div>");
        });
        parents.slideUp('fast');
        init_stuff();
    }

    function getElementsAfter(parent, selector, start) {
        var is_after = false;
        return $('#main').find(selector).filter(function () {
            if (is_after) {
                return $(this).parents('.parent_flow').is(parent);
            } else {
                if ($(this).is(start)) {
                    is_after = true;
                }
                return false;
            }
        });
    }

    function save_continue(obj) {

    }

    function parseJSON(str) {
        str = str.toString().trim();
        if (str === "") {
            str = '""';
        }
        try {
            var p = JSON.parse(str);
        } catch (e) {
            return "";
        }
        return p;
    };

    function validate_show_if(parent_flow) {
        var show_if = parent_flow.find('.show_if').text();
        if (show_if) {
            show_if = parseJSON(show_if);
            if (show_if.length == 0) return true;
            var focus_sub_flow = parent_flow.find('.sub_flow[data-name="' + show_if.name + '"]');
            if (focus_sub_flow.length == 0) {
                focus_sub_flow = parent_flow.parents('.master_flow').find('.sub_flow[data-name="' + show_if.name + '"]');
            }
            var val = get_sub_flow_val(focus_sub_flow);
            var checks = show_if.value.split('||');
            for (var x = 0; x < checks.length; x++) {
                if (show_if.operator == 'equal_to') {
                    if (val == checks[x]) return true;
                } else if (show_if.operator == 'not_equal_to') {
                    if (val != checks[x]) return true;
                }
            }
            return false;
        }
        return true;
    }

    function validate_sub_show_if(sub_flow) {
        var show_if = sub_flow.children('.sub_show_if');
        if (show_if && show_if.text()) {
            //	show_if = JSON.parse(show_if.text());
            show_if = parseJSON(show_if.text());
            var focus_sub_flow = sub_flow.parents('.parent_flow').find('.sub_flow[data-name="' + show_if.name + '"]');
            var focus_sub_flow_compare_to_field = sub_flow.parents('.parent_flow').find('.sub_flow[data-name="compare_to_field"]');
            if (focus_sub_flow.length == 0) {
                focus_sub_flow = sub_flow.parents('.master_flow').find('.sub_flow[data-name="' + show_if.name + '"]');
            }
            var val = get_sub_flow_val(focus_sub_flow);
            var compare_to_val = get_sub_flow_val(focus_sub_flow_compare_to_field);
            if(compare_to_val == 1 && show_if.check_compare_to) return false;
            var checks = show_if.value.split('||');
            for (var x = 0; x < checks.length; x++) {
                if (show_if.operator == 'equal_to') {
                    if (val == checks[x]) return true;
                } else if (show_if.operator == 'not_equal_to') {
                    if (val != checks[x]) return true;
                } else if (show_if.operator == 'not_null') {
                    if (val != '') return true;
                }
            }
            return false;
        }
        return true;
    }

    function add_previous_parent_flow(obj, id) {
        //var prev_parent_flow = ;
        var prev_parent_flow = $(obj).parents('.next_options').prev('.parent_flow');
        if (prev_parent_flow.length == 0) {
            prev_parent_flow = $(obj).parents('.parent_flow').last();
        }
        if (prev_parent_flow.length == 0) {
            prev_parent_flow = $('.parent_flow[data-id="' + id + '"]').last();
        }
        var new_parent_flow = duplicate_flow(prev_parent_flow, false, true, true);
        $(prev_parent_flow).after(
            new_parent_flow
        );
        
        redo_options();
        var found_parent = $('.parent_flow[data-id="' + $(new_parent_flow).attr('data-id') + '"]').last();
        clear_last_parent_flow(found_parent);
        $('.sub_flow:visible').each(function (i, item) {
            $(item).removeClass('current');
        });
        found_parent.find('.sub_flow:visible:first').each(function (i, item) {
            $(item).addClass('current');
        });
        get_pallet();
    }

    function remove_previous_parent(obj, id) {
        var prev_parent_flow = $(obj).parents('.parent_flow').last();
        if (prev_parent_flow.length == 0) {
            prev_parent_flow = $('.parent_flow[data-id="' + id + '"]').last();
        }
        var is_last = false;
        if (prev_parent_flow.parents('.master_flow').find('.parent_flow[data-name="' + prev_parent_flow.attr('data-name') + '"]').length == 1) {
            is_last = true;
        }

        redo_options();
        if (is_last)
            clear_last_parent_flow(prev_parent_flow);
        else
            prev_parent_flow.remove();
        get_pallet();

    }

    //This function re-renders the elements outside of the parent flow elements. Just cleans things up while moving between objects
    function redo_options() {
    	
        $('section.next_options').remove();
        get_multiple_ids(function (ids) {
            for (i = 0; i < ids.length; i++) {
                buttons = $('span.next_options[data-id="' + ids[i] + '"]').last();
                buttons_html = buttons.html();
                var parent_flow = buttons.parents('.parent_flow');
                if (parent_flow.is(':visible')) {
                    parent_flow.after('<section class="next_options">' + buttons_html + '</section>');
                }
            }
        });
        $('.master_flow').each(function (i, item) {
            item = $(item);
            var closed = item.attr('data-closed');
            var dataName = item.attr('data-name');
            if(dataName == 'tasks'){
            	//See if this is a task of type create new record
            	var createNewRecord = item.find('.parent_flow[data-name="create_new_record"]');//modify_object_field
            	var modifyObjectField = item.find('.parent_flow[data-name="modify_object_field"]');
            	var modifyRelatedObjectField = item.find('.parent_flow[data-name="modify_related_object_field"]');
            	if(createNewRecord.children().length > 0){
            		var isCreateRecord = true;
            	}
            	if(modifyObjectField.children().length > 0){
            		var modifyObjectField = true;
            	}
            	if(modifyRelatedObjectField.children().length > 0){
            		var modifyRelatedObjectField = true;
            	}             	
            }
            if (closed == '1') {
            	  if(!isCreateRecord || !modifyObjectField || !modifyRelatedObjectField){
            	  	item.find('section.next_options').remove();
            	  }
                item.find('div.empty_options').hide();
                item.find('.readonly').show();
            } else {
                item.find('div.empty_options').show();
                item.find('.readonly').hide();
            }
        });

    }

    function get_multiple_ids(cb) {
        var ids = [];
        $('span.next_options').each(function (i, item) {
            ids.push($(item).attr('data-id'));
        }).promise().done(function () {
            cb(ids.getUnique());
        });
    }

    function clear_last_parent_flow(parent_flow) {
        $(parent_flow).find('.sub_flow').each(function (i, item) {
            if (i > 0) {
                $(item).hide();
                $(item).removeClass('current');
            } else {
                $(item).addClass('current');
            }
            $(item).attr('data-id',
                (
                parseInt($(item).attr('data-id')) + 500
                )
            );
            $(item).find('span.value').empty();
        });
        $(parent_flow).find('.next_options').hide();
    }

    function go_to_master(obj, master_name){
        if(master_name){
            var masters = $('.master_flow[data-name="' + master_name + '"]');
            if(masters && masters.length){
                var master = masters.first();
                go_to_specific_flow({
                    master : master
                });
            }
        }
    }

    function go_to_parent(obj, parent_name){

        if(parent_name == 'process_cancel_event')
            cancel = 'true';
        if(parent_name){
            var parents = $('.parent_flow[data-name="' + parent_name + '"]');
            if(parents && parents.length){
                var parent = parents.first();
                go_to_specific_flow({
                    parent : parent
                });
            }
        }
    }

    function go_to_next_master(obj, id) {
        get_next_sub_flow_imp({
            force_next: true,
            force_next_master: true,
            master: $(obj).parents('.master_flow')
        }, function (next) {
            if (next)
                get_pallet();
        });
    }

    function go_to_next(obj, id) {

        get_next_sub_flow_imp({force_next: true}, function (next) {
            if (next)
                get_pallet();
        });
    }

    function select_switch(obj, id) {
        var add = true;
        $(obj).parents('.switch').find('.switch_option').each(function (i, item) {
            $(item).attr('data-selected', false);
        });
        $(obj).attr('data-selected', true);
        if (add) add_previous_parent_flow(obj, id);
    }

    function get_sub_flow_val(sub_flow) {
        custom = get_custom_value(sub_flow);
        if (custom && custom.length > 0) {
            return custom;
        } else {
            var val = sub_flow.children('span.value');
            var c = val.find('.custom_field');
            if (c && c.length > 0)
                return ''; // Custom field is there but blank
            else
                return val.text(); // Custom field not present
        }
    }

    function get_custom_value_stage(sub) {
        var custom_field = sub.find('.stage_dropdown');
        
        if (custom_field.is('input[type="checkbox"]')) {
            if (custom_field.is(':checked'))
                return '1';
            else
                return '0';
        } else if (custom_field.is('input[type="text"]')) {
            return custom_field.val();
        } else if (custom_field.is('textarea')) {
            return custom_field.val();
        } else if (custom_field.is('select')) {
            if(custom_field.attr('multiple')){
                return custom_field.find('option:selected').map(function(){ return this.value }).get().join(",");
            }else {
                return custom_field.find('option:selected').val();
            }
        } else {
            return custom_field.val();
        }
    }
        
        function get_sub_flow_val_stage(sub_flow) {
        
        custom = get_custom_value_stage(sub_flow);
        if (custom && custom.length > 0) {
            return custom;
        } else {
            var val = sub_flow.children('span.value');
            var c = val.find('.custom_field');
            if (c && c.length > 0)
                return ''; // Custom field is there but blank
            else
                return val.text(); // Custom field not present
        }
    }


    function get_sub_flow_val_by_id(id) {
        var sub_flow = $('.sub_flow[data-id="' + id + '"]');
        return get_sub_flow_val(sub_flow);
    }

    function get_sub_flow_val_by_name(name) {
        return get_sub_flow_val($('.sub_flow[data-name="' + name + '"]'));
    }

    function get_mapping(cb, retrieve_values) {
        if (retrieve_values === undefined) retrieve_values = false;
        var pid = $('#process_id').val();
        if (retrieve_values && pid && pid.length > 0)
            method = 'get_mapping_with_values';
        else
            method = 'get_mapping';
        $.post(ajax_url,
            {
                method: method,
                values: retrieve_values,
                process_id: pid
            }
        )
            .success(function (map) {
                cb($.parseJSON(map));
            })
            .error(function (err) {
                console.log(err);
                alert('An Error Occurred.');
            });
    }

    function save_everything(duplicate,export_process,delete_process) {
    	  var process_name = $('#name').val();
        if (!process_name || process_name.length == 0) {
            loading(false);
            alert('Please fill out the "Process Name" before saving.');
            return;
        }
        var start_event = get_sub_flow_val_by_name('start_event');
        if (start_event == 'Recurring Process') {
            var query = get_sub_flow_val_by_name('process_query');

            var starts_select = new RegExp("^SELECT", "i");
            if (starts_select.test(query)) {
                // It passed, do nothing
            } else {
                alert("Process Query must start with SELECT.");
                loading(false);
                return;
            }
            var destruct_matches = query.match(/(UPDATE|DELETE|TRUNCATE|EMPTY|DROP|ALTER|INSERT)[\s]*/gi);
            var not_destruct_matches = query.match(/["'].*(UPDATE|DELETE|TRUNCATE|EMPTY|DROP|ALTER|INSERT).*["']/gi);
            if ((destruct_matches && not_destruct_matches) && destruct_matches.length != not_destruct_matches.length) {
                alert("Process Query must not contain any database altering commands.");
                loading(false);
                return;
            }
        }
        if(delete_process){
            delete_process = true;
        }
        if(duplicate){
            duplicate = true;
            if(process_name.match(/Duplicate\s*\d*\s*$/)){
                var number = process_name.replace(/.*(\d+)\s*$/g, "$1");
                number = parseInt(number);
                if(number && !isNaN(number)){
                    process_name = process_name.replace(/(\d+)\s*$/g, "");
                    process_name += ' ' + (number + 1).toString();
                }else{
                    process_name += ' 2';
                }

            }else{
                process_name += ' Duplicate';
            }

        }else{
            duplicate = false;
        }
        get_mapping(function (map) {

            //Grab Header values
            map = set_map_item(map, 'name', process_name.toString());
            map = set_map_item(map, 'description', $('#description').val().toString());
            map = set_map_item(map, 'active', $('#active').find('option:selected').val().toString());
            var ignore_check = $('#ignore_check').is(':checked');
            if (ignore_check)
                map = set_map_item(map, 'ignore_check', '1');
            else
                map = set_map_item(map, 'ignore_check', '0');
            //Grab All other values
            $('.sub_flow:visible, .sub_flow[data-readonly="1"]').each(function (i, sub_flow) {
                sub_flow = $(sub_flow);
                var name = sub_flow.attr('data-name');
                map = set_map_item(map, name, sub_flow);
            });
            $('.switch').each(function (i, item) {
                var sw = $(item);
                var option = sw.find('.switch_option[data-selected="true"]');
                if (option && option.length > 0) {
                    var val = option.attr('data-name');
                    var parent_flow = sw.parents('.parent_flow');
                    map = set_map_item(map, option.attr('data-group-name'), {
                        value: val.toString(),
                        sugar_id: parent_flow.attr('data-sugar-id'),
                        parent_id: parent_flow.attr('data-parent-id'),
                        type: 'parent'
                    });
                }
            });
            $('.parent_flow[data-name="filter_fields"]').each(function (i, item) {

                var sugar_id = $(item).attr('data-sugar-id');
                var parent_id = $(item).attr('data-parent-id');
                var group = $(item).find('.switch .filter-field-group option:selected').val();
                var group_andor = $('#groups-pallet li.group[data-number="' + group + '"] .group_andor option:selected').val();
                if (group.length == 0) return;
                map = set_map_item(map, 'group_number', {
                    value: group.toString(),
                    sugar_id: sugar_id,
                    parent_id: parent_id,
                    type: 'parent'
                });
                if (((group == 1 || group == 0) && !group_andor )) group_andor = 'and';
                if(group_andor === undefined) group_andor = 'and';
                map = set_map_item(map, 'group_andor', {
                    value: group_andor.toString(),
                    sugar_id: sugar_id,
                    parent_id: parent_id,
                    type: 'parent'
                });
            });
            //Send map file back to Sugar
            $.post(ajax_url,
                {
                    method: 'save_everything',
                    contentType: "application/json",
                    duplicate : duplicate ? '1' : '0',
                    export_process : export_process ? '1' : '0',
                    delete_process : delete_process ? '1' : '0',
                    data: JSON.stringify(map),
                    dataType: "json"
                }
            )

                .success(function (data) {
                    loading(false);
                    if(duplicate){
                        alert('Duplicated!');
                        parent.window.location = 'index.php?module=PM_ProcessManager&action=index';
                    }
                    else if(delete_process){
                        alert('Deleted!');
                        parent.window.location = 'index.php?module=PM_ProcessManager&action=index';
                    }
                    else if(export_process){

                        parent.window.location = 'index.php?module=PM_ProcessManager&action=downloadExport';
                        //parent.window.location = 'index.php?module=PM_ProcessManager&action=index';
                    }
                    else{
                        alert('Saved!');

                        if ($('#process_id').val().length == 0 && confirm('Reload?')) {
                            var process_id = $('.master_flow[data-name="main_process"]').attr('data-sugar-id');
                            parent.window.location.href = "index.php?module=PM_ProcessManager&action=CreateV2&record=" + process_id;
                        }
                    }


                })
                .error(function (err) {
                    console.log(err);
                    loading(false);
                    alert('An Error Occurred.');
                });
        })
    }


    function getMapItem(v1, object, map) {
        var result = $.grep(map, function (e) {
            return e.v1 == v1 && e.object == object;
        }); // Search to see if Sugar ID is stored
        if (result.length == 0) return false;
        return result[0];
    }

//Better each function
    function xEach(obj, cb) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                var value = obj[key];
                cb(key, value);
            }
        }

    }

		   function setSubValue(v2, value, flow) {
		   	
	        sub = $(flow).find('.sub_flow[data-name="' + v2 + '"]');
	        if (sub.length === 0) return false;
	        sub.find('span.value').html(value);
	        //Handle Multi Select
	        if(value){
		        if(value.indexOf('^') > -1){
		            var sc = $(sub).children();
		            var s = $(sub).find('span.value');
		            var m = $(s).children("select");
		            var selectedOptions = value.split(",");
		            for(var i in selectedOptions) {
		                var optionVal = selectedOptions[i];
		                m.find("option[value="+optionVal+"]").prop("selected", "selected");
		            }
		            return true; 
		        }
		      }
	        return true;  
			 }



    function populate_group_dropdowns() {
        var groups = $('#groups-pallet .group');
        var dds = $('.filter-field-group');
        var groups_data = [];
        groups.each(function (i, item) {
            var group = new Object();
            g = $(item);
            group.num = g.attr('data-number');
            group.color = g.find('.group-color').attr('data-color');
            group.new_option = $("<option></option>").val(group.num).text("Group " + group.num);
            groups_data.push(group);
        });
        dds.each(function (i, d) {
            var added_group = 1;
            $.each(groups_data, function (x, group) {
                var c = $(d).find('option[value="' + group.num + '"]');
                if (!(c && c.length > 0)) {
                    added_group = group.num;
                    $(d).append(group.new_option);
                }
            });
            var val = $(d).find('option:selected').val();

            if (val == '') {
                $(d).find('option').removeAttr('selected');
                $(d).find('option[value="' + added_group + '"]').attr('selected', 'selected');
            }
            if (!$(d).find('option:selected')) {
                $(d).find('option:first').attr('selected', 'selected');
            }
        });
    }

    function retrieve_everything_imp(cb) {
        get_mapping(function (map) {

            /*
             var maps = [];
             xEach(map, function(key, val){
             if(key.match(/^map/)){
             maps = $.extend(maps, map[key]);
             delete map[key];
             }
             });
             map.map = maps;
             console.log(map);
             */
            populate_group_dropdowns();
            if (!map.map) {
                cb(false);
                return;
            }

            $(map.everything).each(function (i, process) {
                //In the process
                process_id = process.id;
                process_flow = $('.master_flow[data-name="main_process"]');
                //Set IDS
                $('#name').val(process.name);
                $('#description').val(process.description);
                $('#ignore_check').prop('checked', (process.ignore_process_prev_run_check == '1'));
                $('#active').find('option').each(function (e, option) {
                    var opt = $(option);
                    if (opt.val() == process.status) {
                        opt.attr('selected', 'selected');
                    } else {
                        opt.removeAttr('selected');
                    }
                });
                process_flow.attr('data-sugar-id', process_id);
                process_flow.attr('data-parent-id', process_id);
                process_flow.find('.parent_flow').each(function (i, parent_flow) {
                    parent_flow = $(parent_flow);
                    if (parent_flow.attr('data-is-multiple') === 'false') {
                        parent_flow.attr('data-sugar-id', 'false');
                    }
                    parent_flow.attr('data-parent-id', process_id);
                });
                xEach(process, function (process_field, process_value) {
                    if ($.isArray(process_value)) {
                        //Is Child Loop Through
                        if (process_field === 'stages') {
                            //This is the stage
                            var existing = true;
                            var tasks_existing = true;
                            var tasks_flow = $('.master_flow[data-name="tasks"]');
                            $(process_value).each(function (x, stage) {

                                var stage_flow = $('.master_flow[data-name="' + process_field + '"]');
                                if (!existing) {
                                    //Duplicate Flow
                                    stage_flow = stage_flow.last();
                                    var html = stage_flow.clone();
                                    html = duplicate_flow(html, {sugar_id: stage.id, parent_id: process_id});
                                    //Find Last master flow and set it afterward to perserve correct order and sub tasks
                                    $('.master_flow:last').after(html);
                                } else {
                                    stage_flow.attr('data-parent-id', process_id);
                                    stage_flow.attr('data-sugar-id', stage.id);
                                }
                                stage_flow = $('.master_flow[data-name="' + process_field + '"][data-sugar-id="' + stage.id + '"]');
                                stage_flow.find('.parent_flow').each(function (i, parent_flow) {
                                    var parent_flow = $(parent_flow);
                                    if (parent_flow.attr('data-is-multiple') === 'false') {
                                        parent_flow.attr('data-sugar-id', 'false');
                                    }
                                    parent_flow.attr('data-parent-id', stage.id);
                                });
                                stage_flow.find('.master-droppable[data-name="tasks"]').remove();
                                //var org_tasks_flow = $('.master_flow[data-name="tasks"]');
                                xEach(stage, function (stage_field, stage_value) {

                                    if ($.isArray(stage_value)) {
                                        var taskcount = stage_value.length;
                                        // IS Tasks
                                        // Set existing task ids if the stage id matches tasks parent id
                                        // Else duplicate flow
                                        if (stage_value.length === 0 && stage_flow.find('.master-droppable[data-name="tasks"]').length === 0) {
                                            stage_flow.append("<div class='master-droppable' data-name='tasks'></div>");
                                        }
                                        $(stage_value).each(function (x, task) {
                                            if (!tasks_existing) {
                                                //Duplicate Flow
                                                tasks_flow = tasks_flow.last();
                                                var html = tasks_flow.clone();
                                                html = duplicate_flow(html, {sugar_id: task.id, parent_id: stage.id});
                                                //Find Parent Set HTML after
                                                stage_flow.after(html);
                                            } else {
                                                tasks_flow.attr('data-parent-id', stage.id);
                                                tasks_flow.attr('data-sugar-id', task.id);
                                            }
                                            tasks_flow = $('.master_flow[data-name="tasks"][data-sugar-id="' + task.id + '"]');
                                            tasks_flow.find('.parent_flow').each(function (i, parent_flow) {
                                                parent_flow = $(parent_flow);
                                                if (parent_flow.attr('data-is-multiple') === 'false') {
                                                    parent_flow.attr('data-sugar-id', 'false');
                                                }
                                                parent_flow.attr('data-parent-id', task.id);
                                                parent_flow.attr('data-set-id', '0');
                                            });
                                            tasks_existing = false;
                                            xEach(task, function (task_field, task_value) {
                                                if ($.isArray(task_value)) {
                                                    subtask_object = task_field;
                                                    // IS Modify Object or Modify Related Object
                                                    $(task_value).each(function (x, subtask) {
                                                        var parent_name;
                                                        if (subtask.process_object_modify_field && subtask.process_object_modify_field.length > 0) {
                                                            parent_name = 'modify_object_field';
                                                        } else if (subtask.process_object_modify_related_field && subtask.process_object_modify_related_field.length > 0 && subtask.process_object_modify_related_field_from_object.length == 0) {
                                                            parent_name = 'modify_related_object_field';
                                                        }
                                                        else if (subtask.process_object_modify_related_field_from_object && subtask.process_object_modify_related_field_from_object.length > 0) {
                                                        		parent_name = 'modify_related_object_field_from_related_object';
                                                        }
                                                        else {
                                                            parent_name = subtask_object;
                                                        }
                                                        subtask_existing = true;
                                                        flow = tasks_flow.find('.parent_flow[data-name="' + parent_name + '"][data-set-id="1"]');
                                                        if (flow.length > 0) {
                                                            subtask_existing = false;
                                                        }
                                                        if (!subtask_existing) {
                                                            //Duplicate Flow
                                                            flow = flow.last();
                                                            var html = flow.clone();
                                                            html = duplicate_flow(html, {sugar_id: subtask.id, parent_id: task.id});
                                                            //Find Parent Set HTML after
                                                            flow.after(html);
                                                        } else {
                                                            var flows = tasks_flow.find('.parent_flow[data-name="' + parent_name + '"]');
                                                            if(flows.length > 1){
                                                                flows.slice(1).remove();
                                                            }
                                                            flow = flows.first();
                                                            // remove repeated flows from previous copy
                                                            flow.attr('data-sugar-id', subtask.id);
                                                            flow.attr('data-parent-id', task.id);
                                                            flow.attr('data-set-id', '1');
                                                        }
                                                        xEach(subtask, function (subtask_field, subtask_value) {
                                                            var xmap = getMapItem(subtask_field, subtask_object, map.map);
                                                            if (xmap !== false) setSubValue(xmap.v2, subtask_value, $('.parent_flow[data-sugar-id="' + subtask.id + '"]'));
                                                        });
                                                    });

                                                } else {
                                                    var xmap = getMapItem(task_field, 'task', map.map);
                                                    if (xmap !== false) setSubValue(xmap.v2, task_value, tasks_flow);
                                                }
                                            });
                                            //If the task has create_by_field - then show the Add Another
                                            if(flow.data("name") == 'create_by_field' || flow.data("name") == 'modify_object_field' || flow.data("name") == 'modify_related_object_field' ){
                                            	flow.find('.next_options').show();
                                            }    
                                        });
                                        
                                    } else {
                                        var xmap = getMapItem(stage_field, 'stage', map.map);
                                        if (xmap !== false) setSubValue(xmap.v2, stage_value, stage_flow);
                                    }
                                });
                                existing = false;
                            });
                        } else {
                            //This will set the filter fields, related filter fields and cancel filter fields
                            object = process_field;
                            var parent_name;
                            switch (object) {
                                case 'filters':
                                    parent_name = 'filter_fields';
                                    break;
                                case 'related_filters':
                                    parent_name = 'related_filter_fields';
                                    break;
                                case 'cancel_filters':
                                    parent_name = 'cancel_filter_fields';
                                    break;                                    
                            }
                            flow = $('.parent_flow[data-name="' + parent_name + '"]');
                            filterFieldsCount = 0;
                            $(process_value).each(function (x, filter) {                     	  
                                if (!existing) {
                                    //Duplicate Flow
                                    flow = flow.last();
                                    if(filterFieldsCount == 0){
                                    	flow.find('#0322').attr('data-sugar-id', filter.id);
                                    	filterFieldsCount = 1;
                                    }else{
                                      var html = flow.clone();
                                    	html = duplicate_flow(html, {sugar_id: filter.id, parent_id: process_id, filter_name: parent_name});
                                    	//Find Parent Set HTML after
                                    	flow.after(html);
																			flow = $('.parent_flow[data-name="' + parent_name + '"]');
																			flow = flow.last();
                                    }  
                                } else {
                                    flow.attr('data-sugar-id', filter.id);
                                }
                                
                                xEach(filter, function (filter_field, filter_value) {
                                    if(filter_value == ''){
                                       filter_value = ' ';
                                     } 
                                       var xmap = getMapItem(filter_field, object, map.map);
                                    if (xmap !== false) {
                                        var exist = setSubValue(xmap.v2, filter_value, flow);
                                        if (!exist) {
                                            if (xmap.v2 == 'group_number') {                             	
                                                flow.find('.filter-field-group[data-sugar-id="' + filter.id + '"] option:selected').removeAttr('selected');
                                                //flow.find('.filter-field-group[data-sugar-id="' + filter.id + '"] option[value="' + filter_value + '"]').attr('selected', 'selected');
                                                flow.find('.filter-field-group[data-sugar-id="' + filter.id + '"] option[value="' + filter_value + '"]').prop('selected', true);

                                            } else if (xmap.v2 == 'group_andor') {
                                                $('#groups-pallet .group[data-number="' + filter['group_number'] + '"] .group_andor option:selected').removeAttr('selected');
                                                $('#groups-pallet .group[data-number="' + filter['group_number'] + '"] .group_andor option[value="' + filter_value + '"]').attr('selected', 'selected');
                                            } else {
                                                flow.find('.switch_option').attr('data-selected', 'false');
                                                flow.find('.switch_option[data-name="' + filter_value + '"]').attr('data-selected', 'true');
                                            }
                                        }
                                    }
                                });
                                existing = false;
                                
                            });
                        }
                    } else {
                        //Get Map Item
                        var xmap = getMapItem(process_field, 'process', map.map);
                        if (xmap !== false) setSubValue(xmap.v2, process_value, process_flow);
                    }
                });
            });
            
            //STUFF TO DO AFTER RENDERING
            if ($('#process_id').val().length > 0) {
                //NOW SHOW AND HIDE PARENT AND SUB FLOW BASED ON SHOW IF
                $('.parent_flow[data-header="0"]').each(function (i, _parent) {
                    _parent = $(_parent);
                    var show_parent = false;
                    _parent.find('.sub_flow').each(function (x, sub_flow) {
                        sub_flow = $(sub_flow);
                        var value = get_value_from_sub_flow(sub_flow);
                        if (validate_sub_show_if(sub_flow) && value && value.length > 0 && value != 'Please') {
                            show_parent = true;
                            sub_flow.next('.connector').show();
                            sub_flow.fadeIn();
                            //New Code to show yes/no in checkboxes
                            renderCheckBox(sub_flow);
                        } else {
                            sub_flow.next('.connector').hide();
                            sub_flow.hide();
                        }

                    });
                    if (show_parent && validate_show_if(_parent)) {
                        _parent.parents('.master_flow').fadeIn();
                        _parent.fadeIn();
                    } else {
                        _parent.hide();
                    }
                });
            
                //Move Parent ID to child elements
                $('.master_flow[data-name="stages"]:first').each(function (i, master) {
                    var stage_id = $(master).attr('data-sugar-id');
                    var first_task = $(master).next('.master_flow[data-name="tasks"]');
                    first_task.attr('data-parent-id', stage_id);

                });

                var last_process = $('.master_flow:last');
                if (last_process.attr('data-name') == 'stages') {
                    last_process.append('<div class="master-droppable" data-name="tasks" ></div>');
                }
                $('.master_flow[data-name="stages"]:visible').each(function (i, item) {

                    var set_readonly = true;
                    var result = $(item).find('.parent_flow:first');
                    if (result[0] && $(result[0]).is(':hidden')) set_readonly = false;
                    if (set_readonly) set_read_only($(item));

                });
                $('.master_flow[data-name="tasks"]:visible').each(function (i, item) {

                    var set_readonly = true;
                    var result = $(item).find('.parent_flow:first');
                    if (result[0] && $(result[0]).is(':hidden')) set_readonly = false;
                    if (set_readonly) set_read_only($(item));

                });

                $('.parent_flow[data-name="related_filter_fields"]:visible').each(function (i, item) {

                    var set_readonly = true;
                    var result = $(item).find('.parent_flow:first');
                    if (result[0] && $(result[0]).is(':hidden')) set_readonly = false;
                    if (set_readonly) set_read_only($('.relatedfilterfields'));
                    //if (set_readonly) $('.relatedfilterfields').slideToggle('slow');
                    $('.expandrff').show();
                    $('.collapserff').hide();
                });


                $('.parent_flow[data-name="filter_fields"]:visible').each(function (i, item) {

                    var set_readonly = true;
                    var result = $(item).find('.parent_flow:first');
                    if (result[0] && $(result[0]).is(':hidden')) set_readonly = false;
                    if (set_readonly) set_read_only($('.filterfields'));
                    //if (set_readonly) $('.filterfields').slideToggle('slow');
                    $('.expandff').show();
                    $('.collapseff').hide();

                });
                $('.parent_flow[data-name="cancel_filter_fields"]:visible').each(function (i, item) {

                    var set_readonly = true;
                    var result = $(item).find('.parent_flow:first');
                    if (result[0] && $(result[0]).is(':hidden')) set_readonly = false;
                    if (set_readonly) set_read_only($('.cancelfilterfields'));
                    //if (set_readonly) $('.cancelfilterfields').slideToggle('slow');
                    $('.expandcff').show();
                    $('.collapsecff').hide();

                });
                //Regroup Filter Fields
                regroup_filter_fields();
            }
            redo_options();
            cb(true);
        }, true);
    }

    //New function to set yes/no in check boxes
    function renderCheckBox(subflow) {
        var change = subflow.attr('data-change');
        if (change == 'get_checkbox_next') {
            PMEvents.get_checkbox_next(subflow.attr('data-id'));

        }
        return;
    }

    function duplicate_flow(flow, data, return_html, clone) {
        if (return_html === undefined) return_html = true;
        if (clone === undefined) clone = false;
        //	flow.css('display', 'inline-block');
        if (clone) flow = $($(flow).clone());
        if (data !== false) {
        	  if(data.filter_name == 'filter_fields'){
        	  	groupid = "0322" + data.sugar_id;
        	  	flow.find('#0322').attr('data-sugar-id', data.sugar_id);
        	  }
            flow.attr('data-sugar-id', data.sugar_id);
            flow.attr('data-parent-id', data.parent_id);
        } else {
            create_guid(function (id) {
                flow.attr('data-sugar-id', id);
                data.sugar_id = id;
            });
        }
        var d = new Date();
        var n = d.getTime();
        var master_id = (randomXToY(1000, 100000000) + n);
        flow.attr('data-id', master_id);
        flow.find('.edit_master').attr('data-id', master_id);

        flow.find('.master_flow, .parent_flow, .sub_flow').each(function (i, item) {
            item = $(item);
            //	item.css('display', 'inline-block');
            var d = new Date();
            var n = d.getTime();
            if (item.is('.parent_flow')) {
                if (item.attr('data-is-multiple') == 'false') {
                    item.attr('data-sugar-id', 'false');
                } else {
                    create_guid(function (new_id) {
                        item.attr('data-sugar-id', new_id);
                    });
                }
                item.attr('data-parent-id', data.sugar_id);
            }
            item.attr('data-id', (randomXToY(1000, 100000000) + n));

        });

        flow.find('.connector:not(:first)').hide();

        if (return_html)
            return flow.wrap('<p/>').parent().html();
        else
            return flow;
    }

    function randomXToY(minVal, maxVal) {
        var randVal = minVal + (Math.random() * (maxVal - minVal));
        return Math.round(randVal);
    }

    function is_new_process() {
      return !$('#process_id').val()
    }

    function get_process_id() {
        var val = $('#process_id').val();
        if (val.length > 0) {
            return val;
        } else {
            return $('.master_flow[data-name="main_process"]').attr('data-sugar-id');
        }
    }

//SubFlow Param could be string as value
    function set_map_item(map, v2_name, sub_flow) {
        for (x in map) {
            var item = map[x];
            if (item.v2 == v2_name) {
                if (item.value instanceof Array) {
                    if (typeof sub_flow === "string") {
                        map[x].value.push({
                            value: sub_flow,
                            sugar_id: get_process_id(),
                            parent_id: get_process_id(),
                            type: 'master'
                        });
                    } else if (sub_flow instanceof jQuery && sub_flow.is('.sub_flow')) {
                        map[x].value = get_values_from_sub_flow(sub_flow, item.type);
                    } else {
                        map[x].value.push(sub_flow);
                    }
                } else {

                }
                return map;
            }
        }
        return map;
    }

    function get_value_from_sub_flow(sub, type) {
        switch (type) {
            case 'custom':
                return get_custom_value(sub);
                break;
            case 'basic':
                return sub.children('span.value').text();
                break;
            case 'switch':
                return sub.children('.switch_option[data-selected="true"]').attr('data-name');
                break;
            default:
                return sub.children('span.value').text();
                break;
        }
    }

    function get_values_from_sub_flow(sub, type) {

        var values = [];
        $('.sub_flow[data-name="' + sub.attr('data-name') + '"]').each(function (i, sub_flow) {
            var obj = {};
            obj.value = get_sub_flow_val($(sub_flow));
            var val = $(sub_flow).children('span.value');
            var c = val.find('.stage_dropdown');
            if (c && c.length > 0){
               obj.value = get_sub_flow_val_stage($(sub_flow));
            }
            //Stage Name
            var sn = val.find('.stage_name');
            if (sn && sn.length > 0){
                obj.value = get_sub_flow_val_stage($(sub_flow));
            }
            if (obj.value.length == 0) return;
            var parent_flow = $(sub_flow).parent('.parent_flow').first();
            if (!parent_flow.attr('data-sugar-id') || parent_flow.attr('data-sugar-id') == 'false') {
                parent_flow = parent_flow.parent('.master_flow');
            }
            if (parent_flow.attr('data-sugar-id'))
                obj.sugar_id = parent_flow.attr('data-sugar-id');
            else
                obj.sugar_id = false;

            if (parent_flow.attr('data-parent-id'))
                obj.parent_id = parent_flow.attr('data-parent-id');
            else
                obj.parent_id = false;
            if (parent_flow.is('.master_flow')) {
                obj.type = 'master';
            } else if (parent_flow.is('.parent_flow')) {
                obj.type = 'parent';
            } else {
                obj.type = 'sub';
            }

            values.push(obj);
        });
        return values;
    }

    function get_custom_value(sub) {
        var custom_field = sub.find('.custom_field');

        if (custom_field.is('input[type="checkbox"]')) {
            if (custom_field.is(':checked'))
                return '1';
            else
                return '0';
        } else if (custom_field.is('input[type="text"]')) {
            return custom_field.val();
        } else if (custom_field.is('textarea')) {
            return custom_field.val();
        } else if (custom_field.is('select')) {
            if(custom_field.attr('multiple')){
                return custom_field.find('option:selected').map(function(){ return this.value }).get().join(",");
            }else {
                return custom_field.find('option:selected').val();
            }
        } else {
            return custom_field.val();
        }
    }

 function get_stage_name_value(sub){
     var custom_field = sub.find('.stage_name');
     return custom_field.val();
 }

    function set_custom_value(sub, value) {
        var custom_field = sub.find('.custom_field');
        if (custom_field.is('input[type="checkbox"]')) {
            if (value == '1')
                custom_field.prop('checked', true);
            else
                custom_field.prop('checked', false);
        } else if (custom_field.is('input[type="text"]')) {
            custom_field.val(value);
        } else if (custom_field.is('textarea')) {
            custom_field.val(value);
        } else if (custom_field.is('select')) {
            custom_field.find('option[value="' + value + '"]').attr('selected', 'selected');
        } else {
            custom_field.val(value);
        }
    }
    /*
    function get_custom_value(sub) {
        var custom_field = sub.find('.custom_field');
        if (custom_field.is('input[type="checkbox"]')) {
            if (custom_field.is(':checked'))
                return '1';
            else
                return '0';
        } else if (custom_field.is('input[type="text"]')) {
            return custom_field.val();
        } else if (custom_field.is('textarea')) {
            return custom_field.val();
        } else if (custom_field.is('select')) {
            return custom_field.find('option:selected').val();
        } else {
            return custom_field.val();
        }
    }
    */

    function loading(a) {
        if (activeConnections < 0) activeConnections = 0;
        if (a) {
            activeConnections++;
            $('body').addClass('loading');
            setTimeout(function () {
                if (activeConnections > 0) {
                    $('body').removeClass('loading');
                }
            }, 10000);
        } else {
            if (!--activeConnections) {
                $('body').removeClass('loading');
            }
        }
    }

    function makeAJAXRequest(sub_flow, data, pallet, fn){
        $.post(
            ajax_url,
            data
        )
            .success(function(res){
                if(pallet){
                    $('#pallet').html(res);
                }else{
                    $('#pallet').html('');
                    $(sub_flow).children('span.value').html(res);
                }

                update_help(sub_flow);
                init_stuff();
                fn(res);
            })
            .error(function(err){
                console.log(err);
                alert('An Error Occurred.');
            });
    }

    var publicAPI = {
        init : function(){
            loading(true);
             retrieve_everything_imp(function(res){
             init_stuff();
             get_pallet();
            });
            PMEvents.init();
        },
        ajax : makeAJAXRequest,
        loading : loading,
        get_sub_flow_val_by_name : get_sub_flow_val_by_name,
        get_sub_flow_val_by_id : get_sub_flow_val_by_id,
        go_to_next : go_to_next,
        go_to_next_master : go_to_next_master,
        go_to_master : go_to_master,
        go_to_parent : go_to_parent,
        add_previous_parent_flow : add_previous_parent_flow,
        select_switch : select_switch,
        remove_parent_flow : remove_parent_flow,
        remove_previous_parent : remove_previous_parent
    };
    var cancel = 'false';
    return publicAPI;
})($, PMEvents);
