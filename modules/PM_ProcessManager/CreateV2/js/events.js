var PMEvents = (function(){
    var PMLogic;
    return {
        init : function(){
            PMLogic = window.PMLogic;
        },

        modify_field_by_value : function(id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="modify_object_field"]').find('span.value').text();
            var post_data = {
                method : 'get_filter_field_data',
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                field : field,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },

        get_all_sugar_roles : function(id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var role = sub_flow['0']['outerHTML'];
            if(role.indexOf("send_email_to_role") > -1){
                field = "send_email_to_role";
            }
            if(role.indexOf("route_to_role") > -1){
                field = "route_to_role";
            }            
            var post_data = {
                method : 'get_all_sugar_roles',
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                field : field,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },

        get_to_cc_bcc_address : function(id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="send_email_to"]').find('span.value').text();
            var post_data = {
                method : 'get_to_cc_bcc_address',
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                field : field,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },

        create_new_field_by_value : function(id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="create_new_field"]').find('span.value').text();
            var module = $(sub_flow).parents('.master_flow').find('.sub_flow[data-name="create_record_module"]').find('span.value').text();
            var post_data = {
                method : 'get_filter_field_data',
                module_name : module,
                field : field,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };

            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },

        modify_related_field_by_value : function(id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="modify_related_field"]').find('span.value').text();
            var module = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="modify_related_object_module"]').find('span.value').text();
            var post_data = {
                method : 'get_filter_field_data',
                module_name : module,
                field : field,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },

        get_process_object_cancel_field_value : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="cancel_process_object_field"]').find('span.value').text();
            var post_data = {
                method : 'get_filter_field_data',
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                field : field,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };

            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },

        get_filter_field_data : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="filter_field"]').find('span.value').text();
            var post_data = {
                method : method,
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                field : field,
                value : PMLogic.get_sub_flow_val_by_id(id),
                operator : $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="filter_operator"]').find('span.value').text()
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },
        
       get_cancel_filter_field_data : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="cancel_filter_field"]').find('span.value').text();
            var module_name = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="module"]').find('span.value').text();
            var post_data = {
                 method : 'get_filter_field_data',
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                field : field,
                value : PMLogic.get_sub_flow_val_by_id(id),
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },
        
        get_stage_order : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="stage_order"]').find('span.value').text();
            var post_data = {
                method : method,
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                field : field,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },
        get_start_delay_minutes : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="start_delay_minutes"]').find('span.value').text();
            var post_data = {
                method : method,
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                field : field,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },
        get_start_delay_hours : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="start_delay_hours"]').find('span.value').text();
            var post_data = {
                method : method,
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                field : field,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },
        get_start_delay_days : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="start_delay_days"]').find('span.value').text();
            var post_data = {
                method : method,
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                field : field,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },
        get_start_delay_months : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="start_delay_months"]').find('span.value').text();
            var post_data = {
                method : method,
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                field : field,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },
        get_start_delay_years : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="start_delay_years"]').find('span.value').text();
            var post_data = {
                method : method,
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                field : field,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },
        get_related_filter_field_data : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="related_filter_field"]').find('span.value').text();
            var module_name = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="related_module"]').find('span.value').text();
            var post_data = {
                method : 'get_filter_field_data',
                module_name : module_name,
                field : field,
                value : PMLogic.get_sub_flow_val_by_id(id),
                operator : $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="related_filter_operator"]').find('span.value').text()
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },
        
        get_modify_object_field_method : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="modify_object_field"]').find('span.value').text();
            var post_data = {
                method : method,
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                value : PMLogic.get_sub_flow_val_by_id(id),
                field : field
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },

        get_modify_field_method : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="modify_object_field"]').find('span.value').text();
            var post_data = {
                method : method,
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                value : PMLogic.get_sub_flow_val_by_id(id),
                field : field
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },

        get_modify_field_method_from_object : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="modify_object_field"]').find('span.value').text();
            var post_data = {
                method : method,
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                value : PMLogic.get_sub_flow_val_by_id(id),
                field : field
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },

        get_create_record_method : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="create_record_module"]').find('span.value').text();
            var post_data = {
                method : method,
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                value : PMLogic.get_sub_flow_val_by_id(id),
                field : field
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },


        get_create_new_field_method : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var field = $(sub_flow).parents('.parent_flow').find('.sub_flow[data-name="create_new_field"]').find('span.value').text();
            var module = $(sub_flow).parents('.master_flow').find('.sub_flow[data-name="create_record_module"]').find('span.value').text();
            var post_data = {
                method : 'get_modify_field_method',
                module_name : module,
                value : PMLogic.get_sub_flow_val_by_id(id),
                field : field
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },

        get_modify_related_field_method: function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="modify_related_field"]').find('span.value').text();
            var module = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="modify_related_object_module"]').find('span.value').text();
            var post_data = {
                method : 'get_modify_field_method',
                module_name : module,
                //module_name : PMLogic.get_sub_flow_val_by_name('module'),
                value : PMLogic.get_sub_flow_val_by_id(id),
                field : field
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },
        get_modify_related_field_method_from_object: function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="modify_related_field"]').find('span.value').text();
            var module = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="modify_related_object_module"]').find('span.value').text();
            var post_data = {
                method : 'get_modify_field_method_from_object',
                module_name : module,
                //module_name : PMLogic.get_sub_flow_val_by_name('module'),
                value : PMLogic.get_sub_flow_val_by_id(id),
                field : field
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },
        get_filter_fields : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var post_data = {
                method : method,
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },
        get_filter_operators : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="filter_field"]').find('span.value').text();
            var related_field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="related_filter_field"]').find('span.value').text();
            var cancel_field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="cancel_filter_field"]').find('span.value').text();
            var related_module = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="related_module"]').find('span.value').text();
            var post_data = {
                method : method,
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                field : field,
                related_field : related_field,
                cancel_field : cancel_field,
                related_module : related_module,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },
        get_generic : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var post_data = {
                method : method,
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },

        get_related_modules : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var post_data ={
                method : method,
                module_name : PMLogic.get_sub_flow_val_by_name('module'),
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },

        get_related_fields : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var module_name = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="related_module"], .sub_flow[data-name="modify_field_by_related_module"]').find('span.value').text();
            var module = PMLogic.get_sub_flow_val_by_name('module');
        
            var post_data ={
                method : method,
                module_name : module_name,
                value : module
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },
        

        get_email_fields : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var module_name = PMLogic.get_sub_flow_val_by_name('module');
            var post_data ={
                method : method,
                module_name : module_name,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },

        get_related_email_fields : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = "get_related_fields";
            var module_name = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="email_related_module"]').find('span.value').text();
            var module = PMLogic.get_sub_flow_val_by_name('module');
            var post_data = {
                method : method,
                module_name : module_name,
                parent_module : module,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },

        get_modify_related_fields : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var module = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="modify_related_object_module"]').find('span.value').text();
            var post_data ={
                method : 'get_related_fields',
                module_name : module,
                value : PMLogic.get_sub_flow_val_by_name('module')
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },
        get_modify_related_fields_from_object : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var module = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="modify_related_object_module_from_object"]').find('span.value').text();
            var post_data ={
                method : 'get_related_fields',
                module_name : module,
                value : PMLogic.get_sub_flow_val_by_name('module')
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },
        get_create_new_field : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var module = $(sub_flow).parents('.master_flow').find('.sub_flow[data-name="create_record_module"]').find('span.value').text();
            var post_data ={
                method : 'get_filter_fields',
                module_name : module,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },

        get_related_fields_for_modify : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var module = $(sub_flow).parents('.master_flow').find('.sub_flow[data-name="modify_field_by_related_module"]').find('span.value').text();
            var post_data ={
                method : 'get_filter_fields',
                module_name : module,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },

        get_create_new_field_by_field : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var module = PMLogic.get_sub_flow_val_by_name('module');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="create_new_field"]').find('span.value').text();
            var from_module = $(sub_flow).parents('.master_flow').find('.sub_flow[data-name="create_record_module"]').find('span.value').text();
            var post_data ={
                method : 'get_filter_fields_from_field',
                module_name : module,
                field : field,
                from_module : from_module,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },

        get_modify_fields_by_field : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var module = PMLogic.get_sub_flow_val_by_name('module');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="modify_object_field"]').find('span.value').text();
            var post_data ={
                method : 'get_filter_fields_from_field',
                module_name : module,
                field : field,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },


        get_modify_related_fields_by_field : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var module = PMLogic.get_sub_flow_val_by_name('module');
            var field = $(sub_flow).parent('.parent_flow').find('.sub_flow[data-name="modify_related_field"]').find('span.value').text();
            var post_data ={
                method : 'get_related_fields_by_field',
                module_name : module,
                field : field,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, true, function(res){
            });
        },

        get_stage_name: function (id){

            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var post_data ={
                method : method,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },

        create_record_by_date :  function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var post_data ={
                method : 'get_text_field',
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },

        modify_field_by_date : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var post_data ={
                method : 'get_text_field',
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });

        },

        get_datetime_field : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');

            var post_data ={
                method : method,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },

        get_text_field : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var post_data ={
                method : method,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },

        get_custom_script : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var post_data ={
                method : method,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },

        get_upload_custom_script : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var post_data ={
                method : method,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },


        get_checkbox_next : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var post_data ={
                method : method,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },

        get_textarea: function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var post_data ={
                method : method,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },

        get_task_order : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var post_data ={
                method : method,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },

        get_task_name : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var post_data ={
                method : method,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        },

        get_process_query : function (id){
            PMLogic.loading(true);
            var sub_flow = $('.sub_flow[data-id="'+id+'"]');
            var method = $(sub_flow).attr('data-change');
            var post_data ={
                method : method,
                value : PMLogic.get_sub_flow_val_by_id(id)
            };
            if(post_data['value'] == ''){
            	var pobj = PMLogic.get_sub_flow_val_by_name('module');
            	post_data['value'] = 'select id from ' + pobj + ' where deleted = 0';
            }
            PMLogic.ajax(sub_flow, post_data, false, function(res){
            });
        }
    };

})($);