<?php
/*********************************************************************************
 * Copyright: SierraCRM, Inc. 2014
 * Portions created by SierraCRM are Copyright (C) SierraCRM, Inc.
 * The contents of this file are subject to the SierraCRM, Inc. End User License Agreement
 * You may not use this file except in compliance with the License. 
 * You may not rent, lease, lend, or in any way distribute or transfer any rights or this file or Process Manager
 * registrations (purchased licenses) to third parties without SierraCRM, Inc. written approval, and subject to
 * agreement by the recipient of the terms of this EULA.
 * Process Manager for SugarCRM is owned by SierraCRM, Inc. and is protected by international and local copyright laws and
 * treaties. You must not remove or alter any copyright notices on any copies of Process Manager for SugarCRM. 
 * You may not use, copy, or distribute Process Manager for SugarCRM, except as granted by SierraCRM, Inc.
 * without written authorization from SierraCRM, Inc. or its designated agents. Furthermore, this Copyright notice
 * does not grant you any rights in connection with any trademarks or service marks of SierraCRM, Inc. 
 * SierraCRM, Inc. reserves all intellectual property rights, including copyrights, and trademark rights of this software.
 ********************************************************************************/
/*********************************************************************************
 *SierraCRM, Inc
 *14563 Ward Court
 *Grass Valley, CA. 95945
 *www.sierracrm.com
 ********************************************************************************/

function convert_table_to_bean($module){
	require_once('modules/PM_ProcessManager/ProcessManagerEngine.php');
	require_once('modules/PM_ProcessManager/ProcessManagerEngine1.php');
	$pm1 = new ProcessManagerEngine1();
	return $pm1->convertTableToBean($module);
}

function get_view($view, $data){
	global $mod_strings;
	extract($data);
	@include('modules/PM_ProcessManager/CreateV2/' . $view);
}

function base_url(){
    $baseUrl = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    //Strip off the index.php
    $pos = strpos($baseUrl,"index.php");
    $baseUrl = substr($baseUrl,0,$pos);
    $baseUrl = $baseUrl . '/modules/PM_ProcessManager/CreateV2/';
    return $baseUrl;
    //return rtrim($sugar_config['site_url'], '/') . '/modules/PM_ProcessManager/CreateV2/';
}

function get_flow(){
	include('modules/PM_ProcessManager/CreateV2/flow.php');
//	var_dump($theflow);
	$theflow = process_flow($theflow);
	return $theflow;
}

function show_if_sub_id($name, $data){
	if(isset($data[$name])){
		return $data[$name];
	}else{
		return false;
	}
}

function get_groups($id = false){
    global $db;
    $data = array();
    if(empty($id)){
        $data[] = array(
            'number' => 1,
            'andor' => 'amd',
            'color' => convert_group_num_to_color(1)
        );
        return $data;
        
    }
    global $sugar_config;
	  $db_type = $sugar_config['dbconfig']['db_type'];
	  if($db_type == 'mysql'){
    	$query = "SELECT group_number, group_andor FROM pm_process_filter_table WHERE process_id = '{$id}' GROUP BY group_number ORDER BY group_number ASC";
  	}
	  if($db_type == 'mssql'){
    	$query = "SELECT group_number, group_andor FROM pm_process_filter_table WHERE process_id = '{$id}' GROUP BY group_number, group_andor ORDER BY group_number";
  	}  	
    $result = $db->query($query);
    while($row = $db->fetchByAssoc($result)){
        $data[] = array(
            'number' => $row['group_number'],
            'andor' => $row['group_andor'],
            'color' => convert_group_num_to_color($row['group_number'])
        );
    }
    return $data;
}

function convert_group_num_to_color($num){
    switch((int)$num){
    case 1:
        return 'Cyan';
    case 2:
        return 'Gold';
    case 3:
        return 'Maroon';
    case 4:
        return 'MidnightBlue';
    case 5:
        return 'PaleGreen';
    case 6:
        return 'Tomato';
    case 7:
        return 'PaleVioletRed';
    case 8:
        return 'OrangeRed';
    case 9:
        return 'MediumPurple';
    case 10:
        return 'LightSeaGreen';
    case 11:
        return 'LightSlateGray';
    case 12:
        return 'GhostWhite';
    case 13:
        return 'Red';
    case 14:
        return 'Brown';
    case 15:
        return 'Beige';
    default: 
        return 'Aquamarine';
    }
    
}

function process_flow($arr){
	global $mod_strings;
	$converted = array();
//	var_dump($arr);
	foreach($arr as $key => $val){
		if(!is_array($val) && $key == 'lbl' && isset($mod_strings[$val])){
			$converted[$key] = $mod_strings[$val];
		}else if(!is_array($val) && $key == 'customCode'){
			$converted[$key] = parse_vars($val);
		}else if(is_array($val)){	
			$converted[$key] = process_flow($val);
		}else{
			$converted[$key] = $val;
		}
	}
	return $converted;
}

function parse_vars($val){
	global $mod_strings;
	preg_match_all('/{([^}]*)}/', $val, $matches);
	foreach($matches[1] as $match){
		$replace = "";
		if(isset($mod_strings[$match])){
			$replace = $mod_strings[$match];
		}
		$val = str_replace('{' . $match .'}', $replace, $val);
	}
	return $val;
}

function get_everything($process_id){
	if(empty($process_id)) return false;

	$everything = array();

	$process = get_process_by_sql($process_id);

	if($process !== false){
		$everything = $process;
		$filters = get_filters_by_sql($process_id);
		$everything['filters'] = $filters;
		$related_filters = get_related_filters_by_sql($process_id);
		$cancel_filters = get_cancel_filters_by_sql($process_id);
		//Backward compatibility
		foreach($related_filters as &$rf){
			if(empty($rf['related_module'])){
				$rf['related_module'] = $process['related_object'];
			}
		}
		$everything['related_filters'] = $related_filters;
    $everything['cancel_filters'] = $cancel_filters;
		$stages = get_stages_by_sql($process_id);
		if(!empty($stages)){
			foreach($stages as &$stage){
				$tasks = get_tasks_by_sql($stage['id']);
				foreach($tasks as &$task){
					$task['modify_object'] = get_modify_objects($task['id']);
					$task['create_by_field'] = get_create_by_field($task['id']);
				}
				$stage['tasks'] = $tasks;
			}
		}
		$everything['stages'] = $stages;
		//NOW Convert values through the mapping file
		//@TODO
		
		$map = process_everything($everything);
		//echo "<pre>";
		//var_dump($map);
		//echo "</pre>";
		//die();
		/*
		$maps = array_chunk($map, 20, true);
		
		
		foreach($maps as $i => $map){
			$return['map'.$i] = $map;	
		}
		*/
		$return = array('everything' => $everything, 'map' => $map);
		return $return;
	}else{
		return $process_id;
	}
}


function process_everything($data){
	include('modules/PM_ProcessManager/CreateV2/mapping.php');
	foreach($map as &$row){
		//Foreach Row
		//Based on Type Get Section of everything array
		$child = false;
		$subchild = false;
		$type = 'sub';
		$parent_id = false;
		switch($row['object']){
			case 'process':
				$section = $data;
				$parent_id = $data['id'];
				$type = 'master';
				break;
			case 'filters':
				$section = $data['filters'];
				$parent_id = $data['id'];
				$type = 'parent';
				break;
			case 'related_filters':
				$section = $data['related_filters'];
				$parent_id = $data['id'];
				$type = 'parent';
				break;
			case 'cancel_filters':
				$section = $data['cancel_filters'];
				$parent_id = $data['id'];
				$type = 'parent';
				break;				
			case 'modify_object':
				$section = $data['stages'];
				$child = 'tasks';
				$subchild = 'modify_object';
				$type = 'parent';
				break;
			case 'create_by_field':
				$section = $data['stages'];
				$child = 'tasks';
				$subchild = 'create_by_field';
				$type = 'parent';
				break;
			case 'stage':
				$section = $data['stages'];
				$parent_id = $data['id'];
				$type = 'master';
				break;
			case 'task':
				$section = $data['stages'];
				$type = 'master';
				$child = 'tasks';
				break;
			default:
				$section = array();
				break;
		}

		//Find matching data in section
		//Set Values
		if(is_array($section)){
			$row = set_map_row_value($row, $section, $type, $parent_id);
			if(empty($row['value'])){
				foreach($section as $subrow){
					if(is_array($subrow)){
						if($child !== false){
							if(isset($subrow[$child])){
								foreach($subrow[$child] as $subsubrow){
									if($subchild !== false && isset($subsubrow[$subchild])){
										foreach($subsubrow[$subchild] as $sub2row){
											$row = set_map_row_value($row, $sub2row, $type, $sub2row['task_id']);
										}
									}else{
										$row = set_map_row_value($row, $subsubrow, $type, $subrow['id']);
									}
								}
							}
						}else{
							$row = set_map_row_value($row, $subrow, $type, $parent_id);
						}
					}
				}
			}
		}
	}
	return $map;
}

function set_map_row_value($map_row, $data_row, $type, $parent_id = false){
	$db_col = explode('.', $map_row['db']);
	$table = $db_col[0];
	$col = $db_col[1];
	if(isset($data_row[$map_row['v1']])){
		if(is_array($map_row['value'])){
			$map_row['value'][] = array(
				'parent_id' => $parent_id,
				'sugar_id' => (isset($data_row['id'])) ? $data_row['id'] : false,
				'type' => $type,
				'value' => $data_row[$map_row['v1']]
			);
		}else{
			$map_row['value'] = $data_row[$map_row['v1']];
		}
	}
	return $map_row;
}

function get_process_by_sql($id){
	global $db;

	$sql = "
			SELECT
				pm.*,
				defs.ignore_process_prev_run_check,
				defs.process_query_value,
				defs.process_frequency,
				defs.process_start_time_day,
				defs.process_start_day_of_week,
				defs.process_day_of_month,
				defs.process_last_run_date,
				defs.route_object,
				defs.routing_type,
				defs.route_to_user,
				defs.check_login_status,
				defs.find_logged_in_user,
				defs.route_to_role,
				defs.route_to_team
			FROM
				pm_processmanager as pm
				LEFT JOIN pm_process_defs defs ON defs.process_id = pm.id
			WHERE
				pm.deleted = 0
				AND
				pm.id = '$id'
			";
	$data = array();
	$res = $db->query($sql);
	if($row = $db->fetchByAssoc($res)){
		return $row;
	}else{
		return false;
	}
}

function get_create_by_field($id){
	global $db;

	$sql = "
			SELECT
				by_field.id,
				by_field.task_id,
				by_field.create_by_field_field,
				by_field.create_by_field_method,
				by_field.create_by_field_by_field,
				by_field.create_by_field_by_value,
				by_field.create_by_field_by_date
			FROM
				pm_process_task_create_by_field_defs as by_field
			WHERE
				by_field.task_id = '$id'
			";
	$data = array();
	$res = $db->query($sql);
	while($row = $db->fetchByAssoc($res)){
		$data[] = $row;
	}
	return $data;
}

function get_modify_objects($id){
	global $db;

	$sql = "
			SELECT
				modify.id,
				modify.task_id,
				modify.process_object_modify_field,
				modify.modify_method,
				modify.related_modify_method,
				modify.process_object_modify_field_value,
				modify.process_object_modify_field_by_field,
				modify.process_object_modify_field_by_date,
				modify.process_object_modify_related_object,
				modify.process_object_modify_related_field,
				modify.process_object_modify_related_value,
				modify.process_object_modify_related_field_by_field,
				modify.process_object_modify_related_field_by_date,
				modify.modify_field_by_related_module,
				modify.modify_field_by_related_field,
				modify.process_object_modify_related_object_from_object,
				modify.process_object_modify_related_field_from_object
			FROM
				pm_process_task_modify_field_defs as modify
			WHERE
				modify.task_id = '$id'
			";
	$data = array();
	$res = $db->query($sql);
	while($row = $db->fetchByAssoc($res)){
		$data[] = $row;
	}
	return $data;
}

function get_filters_by_sql($id){
	global $db;

	$sql = "
			SELECT
				a.*
			FROM
				pm_process_filter_table as a
			WHERE
				a.process_id = '$id' ORDER by group_number ASC
			";
	$data = array();
	$res = $db->query($sql);
	while($row = $db->fetchByAssoc($res)){
		$data[] = $row;
	}
	return $data;
}

function get_related_filters_by_sql($id){
	global $db;

	$sql = "
			SELECT
				a.*
			FROM
				pm_process_related_filter_table as a
			WHERE
				a.process_id = '$id'
			";
	$data = array();
	$res = $db->query($sql);
	while($row = $db->fetchByAssoc($res)){
		$data[] = $row;
	}
	return $data;
}

function get_cancel_filters_by_sql($id){
	global $db;

	$sql = "
			SELECT
				a.*
			FROM
				pm_process_cancel_filter_table as a
			WHERE
				a.process_id = '$id'
			";
	$data = array();
	$res = $db->query($sql);
	while($row = $db->fetchByAssoc($res)){
		$data[] = $row;
	}
	return $data;	
}

function get_stages_by_sql($id){
	global $db;

	$sql = "
			SELECT
				stage.*
			FROM
				pm_processmanagerstage as stage,
				pm_processmmanagerstage as rel
			WHERE
				stage.deleted = 0
				AND
				rel.deleted = 0
				AND
				rel.pm_processmanager_ida = '$id'
				AND
				stage.id = rel.pm_processmanagerstage_idb
			ORDER BY stage_order ASC
			";
	$data = array();
	$res = $db->query($sql);
	while($row = $db->fetchByAssoc($res)){
		$data[] = $row;
	}

	usort($data, 'sort_stages');
	//Bring First to Last
//	$first = $data[0];
	//array_shift($data);
	//array_reverse($data);
	//$data[] = $first;
	

	return $data;
}

function sort_stages($a, $b){
	$a_total = 0; // IN MINUTES
	$a_total += set_delay_val($a['start_delay_minutes']);
	$a_total += set_delay_val($a['start_delay_hours']) * 60;
	$a_total += set_delay_val($a['start_delay_days']) * 60 * 24;
	$a_total += set_delay_val($a['start_delay_months']) * 60 * 24 * (365 / 12);
	$a_total += set_delay_val($a['start_delay_years']) * 60 * 24 * 365;

	$b_total = 0; // IN MINUTES
	$b_total += set_delay_val($b['start_delay_minutes']);
	$b_total += set_delay_val($b['start_delay_hours']) * 60;
	$b_total += set_delay_val($b['start_delay_days']) * 60 * 24;
	$b_total += set_delay_val($b['start_delay_months']) * 60 * 24 * (365 / 12);
	$b_total += set_delay_val($b['start_delay_years']) * 60 * 24 * 365;

	if ($a_total == $b_total) {
		return 0;
	}
	return ($a_total < $b_total) ? -1 : 1;
}

function set_delay_val($val){
	if($val != '' && !is_null($val)){
		return (int)$val;
	}else{
		return 0;
	}
}

function get_tasks_by_sql($id){
	global $db;

	$sql = "
			SELECT
				task.*,
				create_obj.create_object_type,
				create_obj.create_object_id,
				create_obj.create_object_description,
				create_obj.assigned_user_id_create_object,
				create_obj.inherit_parent_data,
				create_obj.inherit_parent_relationships,
				create_obj.relate_object_to_parent,
				create_obj.create_record_method,
				email.email_template_name,
				email.email_template_id,
				email.contact_role,
				email.internal_email,
				email.internal_email_to_address,
				email.send_email_to_caseopp_account,
				email.send_email_to_object_owner,
				email.email_from_account,
				email.send_email_to_contact,
				email.email_queue_campaign_name,
				email.send_email_to_email_queue,
				email.email_field,
				email.related_module as email_related_module,
				email.related_module_email_field,
				email.send_email_to_all,
				email.cc_email_to_address,
				email.send_email_other_owner,
				email.send_email_to_role,
				email.send_email_to_team,
				email.send_email_to,
				email.send_email_cc,
				email.send_email_bcc,
				routing_def.routing_type,
				routing_def.route_to_user,
				routing_def.check_login_status,
				routing_def.find_logged_in_user,
				routing_def.route_to_role,
				routing_def.route_to_team,
				routing_def.route_to_securitygroups,
				convert_lead.create_account,
				convert_lead.create_contact,
				convert_lead.create_opportunity,
				convert_lead.create_opportunity_name,
				convert_lead.create_opportunity_sales_stage,
				convert_lead.create_opportunity_close_date,
				rest.rest_url,
				rest.rest_method,
				rest.rest_http_query,
				rest.rest_json,
				rest.rest_headers,
				rest.rest_custom_script,
				notification.notification_name,
				notification.notification_description,
				notification.assigned_user_id_notification,
				notification.severity_notification
			FROM
			";
			global $sugar_config;
	    $db_type = $sugar_config['dbconfig']['db_type'];
	    if($db_type == 'mysql'){
				$sql .= " pm_processmanagerstagetask as `task`
			 	JOIN pm_processmgerstagetask `rel`
				LEFT JOIN pm_process_task_create_object_defs `create_obj` ON task.id = create_obj.task_id
				LEFT JOIN pm_process_task_email_defs `email` ON task.id = email.task_id
				LEFT JOIN pm_process_task_routing_defs `routing_def` ON task.id = routing_def.task_id
				LEFT JOIN pm_process_task_convert_lead_defs `convert_lead` ON task.id = convert_lead.task_id
				LEFT JOIN pm_process_task_rest_defs `rest` ON task.id = rest.task_id
				LEFT JOIN pm_process_task_notification_defs `notification` ON task.id = notification.task_id ";
			}
	    if($db_type == 'mssql'){
				$sql .= " pm_processmanagerstagetask as task
			 	JOIN pm_processmgerstagetask rel on 1 = 1
				LEFT JOIN pm_process_task_create_object_defs create_obj ON task.id = create_obj.task_id
				LEFT JOIN pm_process_task_email_defs email ON task.id = email.task_id
				LEFT JOIN pm_process_task_routing_defs routing_def ON task.id = routing_def.task_id
				LEFT JOIN pm_process_task_convert_lead_defs convert_lead ON task.id = convert_lead.task_id
				LEFT JOIN pm_process_task_rest_defs rest ON task.id = rest.task_id
				LEFT JOIN pm_process_task_notification_defs notification ON task.id = notification.task_id ";
			}	
			$sql .= "			
			WHERE
				task.id = rel.pm_processmanagerstagetask_idb
				AND
				rel.pm_processmanagerstage_ida = '$id'
				AND
				task.deleted = 0
				AND
				rel.deleted = 0
		  ";
	$data = array();
	$res = $db->query($sql);
	while($row = $db->fetchByAssoc($res)){
		$data[] = $row;
	}
	return $data;
}

function handle_save_everything($map){
	require_once('modules/PM_ProcessManager/CreateV2/HandleSave.php');
	$hs = new HandleSave();
	$data = array();
	$data['map'] = $map;
	$hs->load($data);
	$hs->init();
}

function handle_duplicate_everything($map){
	require_once('modules/PM_ProcessManager/CreateV2/HandleDuplicate.php');
	$hs = new HandleDuplicate();
	$data = array();
	$data['map'] = $map;
	$hs->load($data);
	$hs->init();
}

function implode_key_value($glue, $arr){
	//Check Mysql / MSSQL
  global $sugar_config;
	$db_type = $sugar_config['dbconfig']['db_type'];
	if($db_type == 'mysql'){
		$str = "";
		foreach($arr as $key => $val){
			if(is_numeric($val) || is_int($val) || is_float($val)){
				$str .= '`' . $key . '` = ' . $val . $glue;
			}else if(is_null($val)){
				$str .= '`' . $key . '` = ' . 'NULL' . $glue;
			}else{
				$str .= '`' . $key . '` = "' . $val . '"' . $glue;
			}
		}
		$str = substr($str, 0, -strlen($glue));
	}
	if($db_type == 'mssql'){
		$str = "";
		foreach($arr as $key => $val){
			if(is_numeric($val) || is_int($val) || is_float($val)){
				$str .= '' . $key . ' = ' . $val . $glue;
			}else if(is_null($val)){
				$str .= '' . $key . ' = ' . 'NULL' . $glue;
			}else{
				$str .= '' . $key . " = '" . $val . "'" . $glue;
			}
		}
		$str = substr($str, 0, -strlen($glue));
	}	
	return $str;
}
function implode_sql($glue, $arr){
	$str = "";
	foreach($arr as $key => $val){
		if(is_numeric($val) || is_int($val) || is_float($val)){
			$str .= $val . $glue;
		}else if(is_null($val)){
			$str .= 'NULL' . $glue;
		}else{
			$str .= '"' . $val . '"' . $glue;
		}
	}
	$str = substr($str, 0, -strlen($glue));
	return $str;
}


?>
