<?php






class SugarChimpActivity_sugar extends Basic 
{
    var $new_schema = true;
    var $module_dir = 'SugarChimpActivity';
    var $object_name = 'SugarChimpActivity';
    var $table_name = 'sugarchimpactivity';
    
    var $importable = false;
    var $disable_row_level_security = true;

    var $id;
    var $date_entered;
    var $date_modified;
    var $modified_user_id;
    var $modified_by_name;
    var $created_by;
    var $created_by_name;
    var $deleted;
    var $created_by_link;
    var $modified_user_link;
    var $assigned_user_id;
    var $assigned_user_name;
    var $assigned_user_link;
    
    var $name; // email address
    var $description; // raw data from MC
    var $activity; // activity type
    var $activity_type; // enum activity type
    var $timestamp; // when activity occurred
    var $url; // if a click, the url of the click
    var $ip; // ip address of user
    var $mailchimp_list_id; // related mailchimp list id
    var $mailchimp_campaign_id; // related mailchimp campaign id
    var $include_empty; // will be true if it was found on first run

    function bean_implements($interface)
    {
        switch($interface)
        {
            case 'ACL': return true;
        }
        return false;
    }
}