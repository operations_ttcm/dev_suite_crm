<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');





$module_name = 'SugarChimpActivity';
$listViewDefs[$module_name] = array(
    "NAME" => array(
        'width' => '10',
        'label' => 'LBL_LIST_NAME',
        'link' => true,
        'default' => true,
    ),
    "ACTIVITY" => array(
        'width' => '5',
        'label' => 'LBL_LIST_ACTIVITY',
        'default' => true,
    ),
    'MAILCHIMP_CAMPAIGN_ID' => array(
        'width' => '10',
        'label' => 'LBL_LIST_MAILCHIMP_CAMPAIGN_ID',
        'default' => true,
    ),
    'MAILCHIMP_LIST_ID' => array(
        'width' => '10',
        'label' => 'LBL_LIST_MAILCHIMP_LIST_ID',
        'default' => true,
    ),
    "TIMESTAMP" => array(
        'width' => '10',
        'label' => 'LBL_LIST_TIMESTAMP',
        'default' => true,
    ),
    "URL" => array(
        'width' => '10',
        'label' => 'LBL_LIST_URL',
        'default' => true,
    ),
    "IP" => array(
        'width' => '10',
        'label' => 'LBL_LIST_IP',
        'default' => true,
    ),
    "DESCRIPTION" => array(
        'width' => '10',
        'label' => 'LBL_LIST_DESCRIPTION',
        'default' => false,
    ),
    "DATE_ENTERED" => array(
        'width' => '10',
        'label' => 'LBL_LIST_DATE_ENTERED',
        'default' => false,
    ),
    "DATE_MODIFIED" => array(
        'width' => '10',
        'label' => 'LBL_LIST_DATE_MODIFIED',
        'default' => false,
    ),
    "MODIFIED_USER_ID" => array(
        'width' => '10',
        'label' => 'LBL_LIST_MODIFIED_USER_ID',
        'default' => false,
    ),
    "ASSIGNED_USER_ID" => array(
        'width' => '10',
        'label' => 'LBL_LIST_ASSIGNED_USER_ID',
        'default' => false,
    ),
    "CREATED_BY" => array(
        'width' => '10',
        'label' => 'LBL_LIST_CREATED_BY',
        'default' => false,
    ),
    'INCLUDE_EMPTY' => array(
        'width' => '10',
        'label' => 'LBL_LIST_INCLUDE_EMPTY',
        'default' => false,
    ),
);
