<?php





$module_name = 'SugarChimpActivity';
$viewdefs[$module_name]['QuickCreate'] = array(
    'templateMeta' => array(
        'maxColumns' => '2', 
        'widths' => array(
            array(
                'label' => '10',
                'field' => '30'
            ), 
            array(
                'label' => '10',
                'field' => '30'
            )
        ),                                                                                                                                    
    ),
    'panels' => array(
        'default' => array(
            array(
                'name',
                'activity_type',
                'mailchimp_campaign_id',
                'mailchimp_list_id',
                'assigned_user_name',
                'timestamp',
            ),
        ),
    ),
);