<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






$module_name = 'SugarChimpActivity';
$searchFields[$module_name] = array(
    "name" => array(
        'query_type'=>'default'
    ),
    "modified_user_id" => array(
        'query_type'=>'default'
    ),
    "assigned_user_id" => array(
        'query_type'=>'default'
    ),
    "description" => array(
        'query_type'=>'default'
    ),
    "activity" => array(
        'query_type'=>'default'
    ),
    "url" => array(
        'query_type'=>'default'
    ),
    "ip" => array(
        'query_type'=>'default'
    ),
    'mailchimp_list_id' => array(
        'query_type'=>'default'
    ),
    'mailchimp_campaign_id' => array(
        'query_type'=>'default'
    ),
    'include_empty' => array(
        'query_type'=>'default'
    ),
    'range_date_entered' => array(
        'query_type' => 'default', 
        'enable_range_search' => true, 
        'is_date_field' => true
    ),
    'start_range_date_entered' => array(
        'query_type' => 'default', 
        'enable_range_search' => true, 
        'is_date_field' => true
    ),
    'end_range_date_entered' => array(
        'query_type' => 'default', 
        'enable_range_search' => true, 
        'is_date_field' => true
    ),
    'range_date_modified' => array(
        'query_type' => 'default', 
        'enable_range_search' => true, 
        'is_date_field' => true
    ),
    'start_range_date_modified' => array(
        'query_type' => 'default', 
        'enable_range_search' => true, 
        'is_date_field' => true
    ),
    'end_range_date_modified' => array(
        'query_type' => 'default', 
        'enable_range_search' => true, 
        'is_date_field' => true
    ), 
);