<?php






$module_name = 'SugarChimpActivity';
$searchdefs[$module_name] = array(
    'templateMeta' => array(
        'maxColumns' => '3',
        'maxColumnsBasic' => '4', 
        'widths' => array(
            'label' => '10',
            'field' => '30'
        ),
    ),
    'layout' => array(
        'basic_search' => array(
            'activity',
            'mailchimp_campaign_id',
            'mailchimp_list_id',
        ),
        'advanced_search' => array(
            'activity',
            'mailchimp_campaign_id',
            'mailchimp_list_id',
            'name',
            'timestamp',
            'date_created',
            'date_modified',
        ),
    ),
);
