<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');





global $app_strings;

$dashletMeta['SugarChimpActivityDashlet'] = array(
    'module' => 'SugarChimpActivity',
    'title' => translate('LBL_HOMEPAGE_TITLE', 'SugarChimpActivity'), 
    'description' => 'A customizable view into SugarChimpActivity',
    'icon' => 'icon_SugarChimpActivity_32.gif',
    'category' => 'Module Views'
);