<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');





require_once('include/Dashlets/DashletGeneric.php');
require_once('modules/SugarChimpActivity/SugarChimpActivity.php');

class SugarChimpActivityDashlet extends DashletGeneric 
{ 
    function SugarChimpActivityDashlet($id, $def = null) 
    {
        global $current_user, $app_strings;
        require('modules/SugarChimpActivity/metadata/dashletviewdefs.php');

        parent::DashletGeneric($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'SugarChimpActivity');

        $this->searchFields = $dashletData['SugarChimpActivityDashlet']['searchFields'];
        $this->columns = $dashletData['SugarChimpActivityDashlet']['columns'];

        $this->seedBean = new SugarChimpActivity();        
    }
}