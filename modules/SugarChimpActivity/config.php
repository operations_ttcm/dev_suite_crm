<?php






$config = array(
    'team_security' => true,
    'assignable' => true,
    'acl' => true,
    'has_tab' => true,
    'studio' => true,
    'audit' => true,
    'activity_enabled' => 0,
    'templates' => 
    array(
        'basic' => 1,
    ),
    'label' => 'MailChimp Activities',
    'label_singular' => 'MailChimp Activity',
    'importable' => true,
);