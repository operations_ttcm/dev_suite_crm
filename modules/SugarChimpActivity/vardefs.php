<?php






$dictionary['SugarChimpActivity'] = array(
    'table' => 'sugarchimpactivity',
    'audited' => true,
    'duplicate_merge' => true,
    'optimistic_locking' => true,
    'unified_search'=>true,
    'comment' => 'MailChimp Campaign Activity Records',
    'fields' => array(
        "activity" => array(
            'name' => 'activity',
            'vname' => 'LBL_ACTIVITY',
            'type' => 'enum',
            'options' => 'sugarchimpactivity_type_list',
            'len' => 50,
            'duplicate_on_record_copy' => 'always',
            'comment' => 'The type of activity registered',
            'reportable' => true,
        ),
        "timestamp" => array(
            'name' => 'timestamp',
            'vname' => 'LBL_TIMESTAMP',
            'type' => 'datetimecombo',
            'comment' => 'Time MailChimp Activity Occurred',
            'enable_range_search' => true,
            'options' => 'date_range_search_dom',
        ),
        "url" => array(
            'name' => 'url',
            'vname' => 'LBL_URL',
            'type' => 'varchar',
            'len' => 2048,
            'comment' => 'The URL of a click activity',
        ),
        "ip" => array(
            'name' => 'ip',
            'vname' => 'LBL_IP',
            'type' => 'varchar',
            'len' => 32,
            'comment' => 'The IP address of the person who made the activity',
        ),
        //Deprecated as of 7.8.3
        // 'mailchimp_list_using_function' => array(
        //     'name' => 'mailchimp_list_id',
        //     'vname' => 'LBL_MAILCHIMP_LIST_NAME',
        //     'comment' => 'The id of the MailChimp list the activity occurred on',
            
        //     // straight id from mailchimp
        //     // 'type' => 'varchar',
        //     // 'len' => 32,
            
        //     // data from mailchimp
        //     'type' => 'enum',
        //     'function' => 'getSugarChimpMCLists',
        //     'function_bean' => 'SugarChimp',
        //     'len' => 255,
        // ),
        //alternate method if maintaining dropdown and pull from there
        'mailchimp_list_id' => array(
            'name' => 'mailchimp_list_id',
            'vname' => 'LBL_MAILCHIMP_LIST_NAME',
            'comment' => 'The id of the MailChimp list the activity occurred on',
            'type' => 'enum',
            'options' => 'mailchimp_lists_list',
            'len' => 255,
            'duplicate_on_record_copy' => 'always',
            'audited' => false,
            'reportable' => true,
        ),

        // Deprecated as of 7.8.3
        // 'mailchimp_campaign_using_function' => array(
        //     'name' => 'mailchimp_campaign_id',
        //     'vname' => 'LBL_MAILCHIMP_CAMPAIGN_NAME',
        //     'comment' => 'The id of the MailChimp Campaign the acitivity occurred on',
            
        //     // data from sugarchimpmccampaign
        //     'type' => 'enum',
        //     'function' => 'getSugarChimpMCCampaigns',
        //     'function_bean' => 'SugarChimp',
        //     'len' => 255,
        // ),

        // Use and maintain dropdown options 
        'mailchimp_campaign_id' => array(
            'name' => 'mailchimp_campaign_id',
            'vname' => 'LBL_MAILCHIMP_CAMPAIGN_NAME',
            'comment' => 'The id of the MailChimp Campaign the activity occurred on',
            'type' => 'enum',
            'options' => 'mailchimp_campaigns_list',
            'len' => 255,
            'duplicate_on_record_copy' => 'always',
            'audited' => false,
            'reportable' => true,
        ),
        'include_empty' => array(
            'name' => 'include_empty',
            'vname' => 'LBL_INCLUDE_EMPTY',
            'type' => 'bool',
            'len'  => '1',
            'default'   => '0',
            'required'  => false
        ),
    ),
    'indices' => array(
        array('name' =>'sugarchimpactivity_activity', 'type' =>'index', 'fields'=>array('activity')),
        array('name' =>'sugarchimpactivity_mailchimp_list_id', 'type' =>'index', 'fields'=>array('mailchimp_list_id')),
        array('name' =>'sugarchimpactivity_mailchimp_campaign_id', 'type' =>'index', 'fields'=>array('mailchimp_campaign_id')),
    ),
    'relationships' => array(),
);

if (!class_exists('VardefManager'))
{
    require_once('include/SugarObjects/VardefManager.php');
}

VardefManager::createVardef('SugarChimpActivity','SugarChimpActivity', array('basic','assignable'));