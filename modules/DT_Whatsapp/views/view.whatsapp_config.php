<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}


require_once("modules/Administration/Administration.php");

class Viewwhatsapp_config extends SugarView {

    public function preDisplay() {
        global $current_user;
        global $sugar_config;
        if (!is_admin($current_user)) {
            sugar_die("Unauthorized access to administration.");
        }
       // require_once('modules/DT_Whatsapp/license/WhatsAppOutfittersLicense.php');
       // $validate = WhatsAppOutfittersLicense::isValid('DT_Whatsapp');
       // if ($validate != 1) {
       //     header('Location: index.php?module=DT_Whatsapp&action=license');
       // }
    }

    public function display() {

        require_once('include/Sugar_Smarty.php');
        global $sugar_config, $db;

        if (!is_admin($GLOBALS['current_user'])) {
            sugar_die('You do not have permission.');
        }

        $whatsapp_admin = new Administration();
        $whatsapp_ss = new Sugar_Smarty();
        

        if ((isset($_POST['save_whatsapp'])) && (!empty($_POST['save_whatsapp']))) {
            // Saving Whatsapp configuration
            $whatsapp_admin->saveSetting('whatsapp_config', 'apiwha_key', $_REQUEST['apiwha_key']);
            // $whatsapp_admin->saveSetting('whatsapp_config', 'whatsapp_auth_token', $_REQUEST['whatsapp_auth_token']);
            // $whatsapp_admin->saveSetting('whatsapp_config', 'whatsapp_from_number', $_REQUEST['whatsapp_from_number']);
            // $whatsapp_admin->saveSetting('whatsapp_config', 'whatsapp_to', $_REQUEST['whatsapp_to']);
            SugarApplication::redirect('index.php?module=DT_Whatsapp&action=whatsapp_config');
            
        }else
        {
            $settings_whatsapp = $whatsapp_admin->retrieveSettings('whatsapp_config');
            $whatsapp_ss->assign('apiwha_key', $settings_whatsapp->settings['whatsapp_config_apiwha_key']);
            // $whatsapp_ss->assign('WHATSAPP_AUTH_TOKEN', $settings_whatsapp->settings['whatsapp_config_whatsapp_auth_token']);
            // $whatsapp_ss->assign('WHATSAPP_FROM_NUMMBER', $settings_whatsapp->settings['whatsapp_config_whatsapp_from_number']);
            // $whatsapp_ss->assign('WHATSAPP_TO', $settings_whatsapp->settings['whatsapp_config_whatsapp_to']);
            
        }

        $whatsapp_ss->assign('MOD', $GLOBALS['mod_strings']);
        $whatsapp_ss->assign('APP', $GLOBALS['app_strings']);
        echo $whatsapp_ss->fetch('modules/DT_Whatsapp/tpls/whatsapp_config.tpl');
    }

}
