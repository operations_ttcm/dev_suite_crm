<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

require_once("modules/Administration/Administration.php");

class dt_whatsapp_utils {

    function getmobilenumbers($modulefrom, $records) {

        $sending_rec_id = explode(",", $records);

        $listwithnumber = array();
        $mobile_list = "";
        for ($iwhatsappc = 0; $iwhatsappc < count($sending_rec_id); $iwhatsappc++) {
            $phone_mobile = $this->getnumberfromid($modulefrom, $sending_rec_id[$iwhatsappc]);
            $listwithnumber[$phone_mobile] = $sending_rec_id[$iwhatsappc];
            if((count($sending_rec_id) -1 ) == $iwhatsappc)
            {
            $mobile_list.=$phone_mobile;    
            }else
            {
            $mobile_list.=$phone_mobile . ",";
            }
        }
        $listwithnumber['whatsapp_rec_list'] = $mobile_list;
        return $listwithnumber;
    }


    public function getnumberfromid($module, $rec_id) {

        $Bean = BeanFactory::newBean($module);
        $SearchSMS = $Bean->retrieve_by_string_fields(array('id' => $rec_id));
        $foundmobilenumb = "";
        if ($module == "Accounts") {
            $acc_val = array("phone_office", "phone_alternate", "phone_fax");
            foreach ($acc_val as $fieldssearch_id) {
                $fieldssearch = trim($fieldssearch_id);
                $foundmob = $SearchSMS->$fieldssearch;
                if (!empty($foundmob)) {
                    $foundmobilenumb = $foundmob;
                    break;
                }
            }
        }

        if ($module == "Contacts") {
            $getarr_val = array("phone_mobile", "phone_other", "phone_work", "phone_fax", "phone_home");
            foreach ($getarr_val as $fieldssearch_id) {
                $fieldssearch = trim($fieldssearch_id);
                $foundmob = $SearchSMS->$fieldssearch;
                if (!empty($foundmob)) {
                    $foundmobilenumb = $foundmob;
                    break;
                }
            }
        }

        if ($module == "Leads") {
            $getarr_val = array("phone_mobile", "phone_other", "phone_work", "phone_fax", "phone_home");
            foreach ($getarr_val as $fieldssearch_id) {
                $fieldssearch = trim($fieldssearch_id);
                $foundmob = $SearchSMS->$fieldssearch;
                if (!empty($foundmob)) {
                    $foundmobilenumb = $foundmob;
                    break;
                }
            }
        }

        return $foundmobilenumb;
    }

    function send_whatsapp_message($mobile_numbers, $template_name, $message, $sl_mod, $sl_mod_id) {
        
        $whatsapp_admin = new Administration();
        $settings_whatsapp = $whatsapp_admin->retrieveSettings('whatsapp_config');

        $mobile_numbers_array = explode(",", $mobile_numbers);
        for ($ce = 0; $ce < count($mobile_numbers_array); $ce++) {

            $message = htmlspecialchars_decode($message, ENT_QUOTES);
            $post = array(
                "apikey" => trim($settings_whatsapp->settings['whatsapp_config_apiwha_key']),
                "number" => trim($mobile_numbers_array[$ce]),
                "text" => $message
            );
            $ch = curl_init();
            $tl_url = "https://panel.apiwha.com/send_message.php";
            curl_setopt($ch, CURLOPT_URL, $tl_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            $result = curl_exec($ch);
            curl_close($ch);

            $result_whatsapp = json_decode($result, true);
           //$GLOBALS['log']->fatal(print_r($post,true).print_r($result_whatsapp,true));
            if ($result_whatsapp['success'] == "true") {
                $call_status = "Sent";
            } else {
                $call_status = "Failure";
            }
            //Create the whatsapp record
            return $this->create_whatsapp($template_name, $message, $call_status, $sl_mod, $sl_mod_id);
        }
    }


    function create_whatsapp($template_name, $message, $call_status, $sl_mod, $sl_mod_id) {
        global $timedate;
        global $current_user;
        $create_bean = BeanFactory::newBean("Calls");
        $create_bean->date_start = $timedate->getInstance()->nowDb();
        $create_bean->date_end = $timedate->getInstance()->nowDb();

        if (!empty($template_name) && trim($template_name) != "No Whatsapp Template Found") {
            $create_bean->name = "Outbound_Whatsapp-" . $template_name;
        } else {
            $create_bean->name = "Outbound_Whatsapp";
        }
        $create_bean->description = $message;
        $create_bean->status = $call_status;
        $create_bean->direction = "Outbound";

        $create_bean->set_created_by = false;
        $create_bean->update_date_entered = false;
        $create_bean->update_date_modified = false;
        $create_bean->update_modified_by = false;

        $create_bean->assigned_user_id = $current_user->id;
        $create_bean->modified_user_id = $current_user->id;
        $create_bean->created_by = $current_user->id;

        $create_bean->parent_type = $sl_mod;
        $create_bean->parent_id = $sl_mod_id;
        $create_id = $create_bean->save();

        $RelCall = BeanFactory::getBean('Calls', $create_id);
        if ($sl_mod == "Contacts") {
            $RelCall->load_relationship('contacts');
            $RelCall->contacts->add($sl_mod_id);
        }
        if ($sl_mod == "Leads") {
            $RelCall->load_relationship('leads');
            $RelCall->leads->add($sl_mod_id);
        }

        //Creating Relationships
        return $create_id;
    }

    

}
