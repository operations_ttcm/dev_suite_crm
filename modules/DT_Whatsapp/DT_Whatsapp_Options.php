<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}


require_once('modules/DT_Whatsapp/license/WhatsAppOutfittersLicense.php');
require_once('modules/DT_Whatsapp/DT_Whatsapp_Utils.php');
require_once("modules/Administration/Administration.php");

$dt_whatsapp_utils = new dt_whatsapp_utils();
$whatsapp_admin = new Administration();
$settings_whatsapp = $whatsapp_admin->retrieveSettings('whatsapp_config');


if (isset($_REQUEST['action']) && trim($_REQUEST['action'] === "get_whatsappbulk_body")) {

    $modulefrom = trim($_REQUEST['modulefrom']);
    $recid = trim($_REQUEST['recid']);
    $gotres = $dt_whatsapp_utils->getmobilenumbers($modulefrom, $recid);
    $mobilenumbers = $gotres['whatsapp_rec_list'];

    $recid_res = serialize($gotres);

    $sql_et = "SELECT et.id,et.name FROM email_templates as et,email_templates_cstm as etc where et.id=etc.id_c and type='whatsapp' and etc.sms_module_c='$modulefrom' and et.deleted=0";
    $result_et = $GLOBALS['db']->query($sql_et);
    $total_recs = $result_et->num_rows;

    if (!empty($modulefrom) && $total_recs >= 1) {
        $optionlist = "<option value='NS'>--Not Selected--</option>";

        while ($row_et = $GLOBALS['db']->fetchByAssoc($result_et)) {
            //Use $row['id'] to grab the id fields value
            $id = $row_et['id'];
            $etname = $row_et['name'];
            $optionlist.="<option value='$id'>$etname</option>";
        }
    } else {
        $optionlist = "<option>No SMS Template Found</option>";
    }

     // $validate = WhatsAppOutfittersLicense::isValid('DT_Whatsapp');
     //   if ($validate == 1) {

    echo "<div class='panel-body panel-collapse collapse in' id='detailpanel_-1'>"
    . "<div class='tab-content'><div class='row edit-view-row'>"
    . "<font color='red' style='text-align: center;'><span id='errr_msg' style='display:none'><b>Empty message cant be send.</b></span></font>"
    . "<div id='loading_whatsapp' style='display:none'><div style='display: flex;justify-content: center;'><image src='modules/DT_Whatsapp/images/whatsapp_loading.gif'/></div></div>"
    . "<div class='col-xs-12 col-sm-6 edit-view-row-item'>"
    . "<div class='col-xs-12 col-sm-4 label'>"
    . "Send to Numbers:</div><br>"
    . "<div class='col-xs-12 col-sm-8 edit-view-field ' type='varchar'>"
    . "<textarea id='whatsapp_bulkmobile_numbers' rows='2' cols='90'>" . $mobilenumbers . "</textarea>"
    . "</div>"
    . "</div>"
    . "<div class='col-xs-12 col-sm-6 edit-view-row-item'>"
    . "<div class='col-xs-12 col-sm-4 label'>Select Template:</div><br>"
    . "<div class='col-xs-12 col-sm-8 edit-view-field ' type='varchar'>"
    . "<select style='width:90%' name='whatsapp_bulktemplate_id' id='whatsapp_bulktemplate_id'>$optionlist</select>"
    . "</div></div>"
    . "<div class='col-xs-12 col-sm-6 edit-view-row-item'>"
    . "<div class='col-xs-12 col-sm-4 label'>"
    . "Message Description:</div><br>"
    . "<div class='col-xs-12 col-sm-8 edit-view-field ' type='varchar'>"
    . "<textarea id='whatsapp_bulk_description' rows='3' cols='90'></textarea>"
    . "</div>"
    . "<input type='hidden' name='sl_mod' id='whatsapp_bulk_sl_mod' value='$modulefrom'/>"
    . "<input type='hidden' name='sl_mod_id' id='whatsapp_bulk_sl_mod_id' value='$recid_res'/>"
    . "</div>"
    . "</div></div></div>";
    // }else
    // {
    //     echo "DT_Whatsapp addon is not active.";
    // }
}

if (isset($_REQUEST['action']) && trim($_REQUEST['action'] === "get_testwhatsapp_body")) {
    
        $result_s = "";
        $post = array(
            "From" => "whatsapp:".trim($settings_whatsapp->settings['whatsapp_config_whatsapp_from_number']),
            "To" => "whatsapp:+" . trim($settings_whatsapp->settings['whatsapp_config_whatsapp_to']),
            "Body" => 'Testing Whatsapp'
        );
        $ch = curl_init();
        $tl_url = "https://api.twilio.com/2010-04-01/Accounts/" . trim($settings_whatsapp->settings['whatsapp_config_whatsapp_sid']) . "/Messages.json";
        curl_setopt($ch, CURLOPT_URL, $tl_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_USERPWD, trim($settings_whatsapp->settings['whatsapp_config_whatsapp_sid']) . ":" . $settings_whatsapp->settings['whatsapp_config_whatsapp_auth_token']);
        $result = curl_exec($ch);
        curl_close($ch);

        $result_whats = json_decode($result, true);
        $GLOBALS['log']->fatal(print_r($result_whats,true));
        if (!empty($result_whats['sid'])) {
            $result_s = "<font color='green'>Message Successfully Sent</font>";
        } else {
            $result_s = print_r($result_whats, true);
        }

        if (empty($result_s)) {
            $result_s = "Please enter details of Whatsapp Twilio Details";
        }

        echo "<div class='panel-body panel-collapse collapse in' id='detailpanel_-1'>"
        . "<div class='tab-content'><div class='row edit-view-row'>"
        . "<div class='col-xs-12 col-sm-6 edit-view-row-item' style='float: left;overflow-y: auto;height: 100%;'>"
        . "<div class='col-xs-12 col-sm-4 label'>"
        . "Testing Whatsapp Results:- <br> <pre>"
        . $result_s
        . "</pre></div>"
        . "</div>"
        . "</div></div></div>";
}

if (isset($_REQUEST['action']) && trim($_REQUEST['action'] === "get_whatsapp_body")) {

    $mobile = trim($_REQUEST['mobile']);
    $modulefrom = trim($_REQUEST['modulefrom']);
    $recid = trim($_REQUEST['recid']);


    $sql_et = "SELECT et.id,et.name FROM email_templates as et,email_templates_cstm as etc where et.id=etc.id_c and type='whatsapp' and etc.sms_module_c='$modulefrom' and et.deleted=0";
    $result_et = $GLOBALS['db']->query($sql_et);
    $total_recs = $result_et->num_rows;

    if (!empty($modulefrom) && $total_recs >= 1) {
         $optionlist = "<option value='NS'>--Not Selected--</option>";

         while ($row_et = $GLOBALS['db']->fetchByAssoc($result_et)) {
             //Use $row['id'] to grab the id fields value
             $id = $row_et['id'];
             $etname = $row_et['name'];
             $optionlist.="<option value='$id'>$etname</option>";
         }
     } else {
        $optionlist = "<option>No Whatsapp Template Found</option>";
    }

    
       // $validate = WhatsAppOutfittersLicense::isValid('DT_Whatsapp');
       // if ($validate == 1) {

    echo "<div class='panel-body panel-collapse collapse in' id='detailpanel_-1'>"
    . "<div class='tab-content'><div class='row edit-view-row'>"
    . "<font color='red' style='text-align: center;'><span id='errr_msg' style='display:none'><b>Empty message cant be send.</b></span></font>"
    . "<div id='loading_whatsapp' style='display:none'><div style='display: flex;justify-content: center;'><image src='modules/DT_Whatsapp/images/whatsapp_loading.gif'/></div></div>"
    . "<div class='col-xs-12 col-sm-6 edit-view-row-item'>"
    . "<div class='col-xs-12 col-sm-4 label'>"
    . "Send to Numbers:</div><br>"
    . "<div class='col-xs-12 col-sm-8 edit-view-field ' type='varchar'>"
    . "<textarea id='whatsapp_mobile_numbers' rows='1' cols='90'>" . $mobile . "</textarea>"
    . "</div>"
    . "</div>"
    . "<div class='col-xs-12 col-sm-6 edit-view-row-item'>"
    . "<div class='col-xs-12 col-sm-4 label'>Select Template:</div><br>"
    . "<div class='col-xs-12 col-sm-8 edit-view-field ' type='varchar'>"
    . "<select style='width:90%' name='whatsapp_template_id' id='whatsapp_template_id'>$optionlist</select>"
    . "</div></div>"
    . "<div class='col-xs-12 col-sm-6 edit-view-row-item'>"
    . "<div class='col-xs-12 col-sm-4 label'>"
    . "Message Description:</div><br>"
    . "<div class='col-xs-12 col-sm-8 edit-view-field ' type='varchar'>"
    . "<textarea id='whatsapp_description' rows='3' cols='90'></textarea>"
    . "</div>"
    . "<input type='hidden' name='whatsapp_sl_mod' id='whatsapp_sl_mod' value='$modulefrom'/>"
    . "<input type='hidden' name='whatsapp_sl_mod_id' id='whatsapp_sl_mod_id' value='$recid'/>"
    . "</div>"
    . "</div></div></div>";
    // }else
    // {
    //     echo "DT_Whatsapp addon is not active.";
    // }
}


if (isset($_REQUEST['action']) && trim($_REQUEST['action'] === "whatsapp_fetch")) {

    $et_id = trim($_REQUEST['et_id']);
    $sl_mod = trim($_REQUEST['sl_mod']);
    $sl_mod_id = trim($_REQUEST['sl_mod_id']);

    $template = new EmailTemplate();
    $template->retrieve_by_string_fields(array('id' => $et_id, 'type' => 'whatsapp'));

    $module_et = BeanFactory::getBean($sl_mod, $sl_mod_id);

    //Parse Body HTML
    echo $template->body = $template->parse_template_bean($template->body, $module_et->module_dir, $module_et);
}

if (isset($_REQUEST['action']) && trim($_REQUEST['action'] === "bulkwhatsapp_fetch")) {

    $et_id = trim($_REQUEST['et_id']);

    $template = new EmailTemplate();
    $template->retrieve_by_string_fields(array('id' => $et_id, 'type' => 'whatsapp'));
    echo $template->body;
}


if (isset($_REQUEST['action']) && trim($_REQUEST['action'] === "send_whatsapp")) {

    global $sugar_config;
    $mobile_numbers = $_REQUEST['mobile_numbers'];
    $template_name = trim($_REQUEST['template_name']);
    $message = $_REQUEST['body'];
    $sl_mod = trim($_REQUEST['sl_mod']);
    $sl_mod_id = trim($_REQUEST['sl_mod_id']);

    echo $dt_whatsapp_utils->send_whatsapp_message($mobile_numbers, $template_name, $message, $sl_mod, $sl_mod_id);

    // print_r(explode(",",$mobile_numbers));
}


if (isset($_REQUEST['action']) && trim($_REQUEST['action'] === "bulksend_whatsapp")) {
    global $sugar_config;
    $template_id = $_REQUEST['template_id'];
    $template_name = $_REQUEST['template_name'];
    $mobile_numbers = $_REQUEST['mobile_numbers'];
    $sl_mod = trim($_REQUEST['sl_mod']);

    $numbers_array2 = htmlspecialchars_decode(trim($_REQUEST['sl_mod_id']), ENT_QUOTES);
    $numbers_array = unserialize($numbers_array2);
    $body = trim($_REQUEST['body']);

    $mobile_numbers_arr = explode(",", $mobile_numbers);

    for ($bm = 0; $bm < count($mobile_numbers_arr); $bm++) {
        $mobile_numberss = $mobile_numbers_arr[$bm];
        $mob_record = $numbers_array[$mobile_numbers_arr[$bm]];

        if ($template_id == "No SMS Template Found") {
            $template_name = "Template Not Selected";
            $message = $body;
        } else {
            $template = new EmailTemplate();
            $template->retrieve_by_string_fields(array('id' => $template_id, 'type' => 'whatsapp'));

            $module_et = BeanFactory::getBean($sl_mod, $mob_record);

            //Parse Body HTML
            $message = $template->body = $template->parse_template_bean($template->body, $module_et->module_dir, $module_et);
        }

        $dt_whatsapp_utils->send_whatsapp_message($mobile_numberss, $template_name, $message, $sl_mod, $mob_record);
    }
}


