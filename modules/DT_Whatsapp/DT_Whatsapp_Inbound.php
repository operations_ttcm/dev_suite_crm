<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

// require_once('modules/DT_Whatsapp/license/WhatsAppOutfittersLicense.php');
// $validate = WhatsAppOutfittersLicense::isValid('DT_Whatsapp');
// if($validate == 1) {

$data = str_replace("&quot;", '"', $_REQUEST['data']);
$data = json_decode($data,true);

$GLOBALS['log']->fatal(print_r($data,true));
global $timedate;
if (!empty($data['from']) && !empty($data['text'])) {

    $from = trim($data['from']);
    $from = substr($from, -10);

    $ParentID ="";
    $UserID = "";
    $ParentType = "";

    $Account_Query = "SELECT a.id FROM accounts a LEFT JOIN accounts_cstm ac ON a.id = ac.id_c WHERE a.phone_fax LIKE '%" . $from . "%' OR a.phone_office LIKE '%" . $from . "%' OR a.phone_alternate LIKE '%" . $from . "%' OR ac.phone_mobile_c LIKE '%" . $from . "%' limit 1";

    $Result_Account = $GLOBALS['db']->query($Account_Query);
    $Row_Res = $GLOBALS['db']->fetchByAssoc($Result_Account);
    $ParentID = $Row_Res['id'];


    if(!empty($ParentID))
    {
        $ParentID = $Row_Res['id'];
        $UserID = $Row_Res['assigned_user_id'];
        $ParentType = "Accounts";
    }else
    {

         $Leads_Query = "SELECT * FROM leads WHERE phone_mobile LIKE '%" . $from . "%' OR phone_other LIKE '%" . $from . "%' OR phone_work LIKE '%" . $from . "%' OR phone_fax LIKE '%" . $from . "%' OR phone_home LIKE '%" . $from . "%' Limit 1";

        $Result_Leads = $GLOBALS['db']->query($Leads_Query);
        $Row_Res = $GLOBALS['db']->fetchByAssoc($Result_Leads);
        $ParentID = $Row_Res['id'];
        $UserID = $Row_Res['assigned_user_id'];
        $ParentType = "Leads";

    }

    $New_Call = BeanFactory::newBean("Calls");

    $direction = "Inbound";
    $subject = "Inbound_Whatsapp From " . $from;

    $New_Call->status = "Held";
    $New_Call->name = $subject;
    $New_Call->date_start = $timedate->getInstance()->nowDb();
    $New_Call->direction = $direction;
    $New_Call->description = $data['text'];

    if (!empty($ParentID)) {
        $New_Call->parent_id = $ParentID;
        $New_Call->parent_type = $ParentType;
    }
    if (!empty($UserID)) {
        $New_Call->assigned_user_id = $UserID;
        $New_Call->modified_user_id = $UserID;
    } else {
        $New_Call->assigned_user_id = "1";
        $New_Call->modified_user_id = "1";
    }

    $New_Call->update_date_modified = false;
    $New_Call->update_modified_by = false;
    $New_Call->set_created_by = false;
    $New_Call->update_date_entered = false;

    $Save_Call = $New_Call->save();
    
    if(!empty($Save_Call)){
    $alert = BeanFactory::newBean('Alerts');
    $alert->name = $subject;
    $alert->description = $data['text'];
    $alert->url_redirect = 'index.php?module=Calls&action=DetailView&record='.$Save_Call;
    $alert->target_module = 'Calls';
    if (!empty($UserID)) {
        $alert->assigned_user_id = $UserID;
        $alert->modified_user_id = $UserID;
    } else {
        $alert->assigned_user_id = "1";
        $alert->modified_user_id = "1";
    }
    $alert->type = 'info';
    $alert->is_read = 0;
    $alert->save();
    }
    //Create Relationship
    if (!empty($ParentID)) {
        $Rel_Call = BeanFactory::getBean('Calls', $Save_Call);
        if ($ParentType == "Contacts") {
            $Rel_Call->load_relationship('contacts');
            $Rel_Call->contacts->add($ParentID);
        }
    }
} else {
    $GLOBALS['log']->fatal('Inbound Whatsapp Failed.');
}
//}
