<?php
$module_name = 'PM_ProcessManagerStageTask';
$viewdefs = array (
$module_name =>
array (
  'EditView' => 
  array (
    'templateMeta' => array('form' => array('enctype'=> 'multipart/form-data',
                                            ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
'javascript' => '<script type="text/javascript" src="include/javascript/popup_parent_helper.js?s={$SUGAR_VERSION}&c={$JS_CUSTOM_VERSION}"></script>
<script type="text/javascript">
var loadFilterFieldsCounter;
loadFilterFieldsCounter = 0;    		
document.getElementById("detailpanel_11").style.display="none";
document.getElementById("detailpanel_12").style.display="none";
document.getElementById("detailpanel_13").style.display="none";
strTaskType = document.forms[\'EditView\'].task_type.value;	
togglePanels(strTaskType);     		
var YUC = YAHOO.util.Connect;
function type_change() {ldelim}      
	var return_id = document.forms[\'EditView\'].return_id.value;
	var gURL = "getFilterFieldsForModifyObject.php?stageid=" + return_id;
	YUC.successEvent.subscribe(handleEvents.success, handleEvents);
    var callback = {ldelim}
        success: function(o) {ldelim}
            document.getElementById("DIV_INFO").innerHTML = o.responseText;       
        {rdelim}
    {rdelim} 
    var connectionObject = YUC.asyncRequest ("GET", gURL, callback);
    //Now toggle the Panels
    strTaskType = document.forms[\'EditView\'].task_type.value;	
	togglePanels(strTaskType); 

 		    		
    		
 {rdelim}

function related_type_change() {ldelim}
    
	var objectType = document.forms[\'EditView\'].process_object_modify_related_object.options[document.forms[\'EditView\'].process_object_modify_related_object.selectedIndex].text;
	
    document.forms[\'EditView\'].selected_related_object.value = objectType;		
	var gURL = "getRelatedFilterFields.php?table=" + objectType;
	YUC.successEvent.subscribe(handleEventsRelatedFilterFields.success, handleEvents);
    var callback = {ldelim}
        success: function(o) {ldelim}
            document.getElementById("RELATED_FILTER_FIELDS_INFO").innerHTML = o.responseText;       
        {rdelim}
    {rdelim} 
    		
    var connectionObject = YUC.asyncRequest ("GET", gURL, callback);
    
    		
 {rdelim}     		

	
var handleEvents = {ldelim}
	start: function(eventType, args){ldelim}

    {rdelim},
 
	complete: function(eventType, args) {ldelim}
	{rdelim},
 
	success: function(eventType, args) {ldelim}
	load_filter_fields();
	{rdelim},
 
	failure: function(eventType, args) {ldelim}

	{rdelim},
 

	upload: function(eventType, args){ldelim}

	{rdelim},
 
	abort: function(eventType, args) {ldelim}

	{rdelim}
{rdelim};

var handleEventsRelatedFilterFields = {ldelim}
	start: function(eventType, args){ldelim}

    {rdelim},
 
	complete: function(eventType, args) {ldelim}
	{rdelim},
 
	success: function(eventType, args) {ldelim}
	load_related_filter_fields();
	{rdelim},
 
	failure: function(eventType, args) {ldelim}

	{rdelim},
 

	upload: function(eventType, args){ldelim}

	{rdelim},
 
	abort: function(eventType, args) {ldelim}

	{rdelim}
{rdelim};     		
    		
var handleEventsRelatedModules = {ldelim}
	start: function(eventType, args){ldelim}

    {rdelim},
 
	complete: function(eventType, args) {ldelim}
    		
	{rdelim},
 
	success: function(eventType, args) {ldelim}
		load_related_modules();	
	{rdelim},
 
	failure: function(eventType, args) {ldelim}

	{rdelim},
 

	upload: function(eventType, args){ldelim}

	{rdelim},
 
	abort: function(eventType, args) {ldelim}

	{rdelim}
{rdelim};

function load_related_modules() {ldelim}
    		
	var finalobjectType = "related_fields";
	var elementid;
	elementid = finalobjectType;
	var lstToCopyFromArray = document.getElementsByName(elementid);
	var lstToCopyToArray = document.getElementsByName("process_object_modify_related_object"); 
	var lstToCopyTo = lstToCopyToArray[0];
    var lstToCopyFrom = lstToCopyFromArray[0];		
    //Get the current value
     var currentFilterFilter1 = document.forms[\'EditView\'].process_object_modify_related_object.options[document.forms[\'EditView\'].process_object_modify_related_object.selectedIndex].text;		
    		for (x = lstToCopyTo.length; x >= 0; x--)
			{ldelim}
				lstToCopyTo[x] = null;
	 		{rdelim} 
   for (i=0;i<lstToCopyFrom.options.length;i++)
		{ldelim}
		 var listText = lstToCopyFrom.options[i].text; 
		 var listValue = lstToCopyFrom.options[i].value; 
		 lstToCopyTo.options.add(new Option(listText,listValue));
 	{rdelim} 	 
{rdelim}     		

//Load the Related Filter Fields    		
function load_related_filter_fields() {ldelim}

var finalobjectType = "related_filter_fields";
var elementid;
elementid = finalobjectType;
var lstToCopyFromArray = document.getElementsByName(elementid); 
var lstToCopyFrom = lstToCopyFromArray[0];
//Now setup all 5 filter lists vars
var lstToCopyToArray = document.getElementsByName("process_object_modify_related_field"); 
var lstToCopyTo = lstToCopyToArray[0];
   		
var myTextField = document.getElementById(finalobjectType);
//Get the current values if this is an edit
var currentFilterFilter1 = document.forms[\'EditView\'].process_object_modify_related_field.options[document.forms[\'EditView\'].process_object_modify_related_field.selectedIndex].text;

    		
//First clear out the select list for all the lists
//Filter List 1 - only fill the list if it is currently Please Specify
//First clear out the select list for all the lists
	 	for (x = lstToCopyTo.length; x >= 0; x--)
			{ldelim}
				lstToCopyTo[x] = null;
	 		{rdelim}	    		
			    		

for (i=0;i<lstToCopyFrom.options.length;i++)
		{ldelim}
		var listValue = lstToCopyFrom.options[i].text; 
		 lstToCopyTo.options.add(new Option(listValue,listValue));
 	{rdelim}


//Now set the value in the Related Object
  var selindex = document.forms[\'EditView\'].relatedObject.value;
  document.forms[\'EditView\'].related_object.selectedIndex = selindex; 		    		
{rdelim};    		
    		
function togglePanels(strTaskType) {ldelim}
 	if(strTaskType == \'\'){ldelim}
		document.getElementById("detailpanel_2").style.display="none";
		document.getElementById("detailpanel_3").style.display="none";
		document.getElementById("detailpanel_4").style.display="none";
		document.getElementById("detailpanel_5").style.display="none";
		document.getElementById("detailpanel_6").style.display="none";
		document.getElementById("detailpanel_7").style.display="none";
		document.getElementById("detailpanel_8").style.display="none";
		document.getElementById("detailpanel_9").style.display="none";
    document.getElementById("detailpanel_10").style.display="none";
	 {rdelim}     		
	if(strTaskType == \'Custom Script\'){ldelim}
		document.getElementById("detailpanel_2").style.display="inline";
		document.getElementById("detailpanel_3").style.display="none";
		document.getElementById("detailpanel_4").style.display="none";
		document.getElementById("detailpanel_5").style.display="none";
		document.getElementById("detailpanel_6").style.display="none";
		document.getElementById("detailpanel_7").style.display="none";
		document.getElementById("detailpanel_8").style.display="none";
		document.getElementById("detailpanel_9").style.display="none";
		document.getElementById("detailpanel_10").style.display="none";
	 {rdelim} 
	if(strTaskType == \'Send Email\'){ldelim}
		document.getElementById("detailpanel_2").style.display="none";
		document.getElementById("detailpanel_3").style.display="inline";
		document.getElementById("detailpanel_4").style.display="none";
		document.getElementById("detailpanel_5").style.display="none";
		document.getElementById("detailpanel_6").style.display="none";
		document.getElementById("detailpanel_7").style.display="none";
		document.getElementById("detailpanel_8").style.display="none";
		document.getElementById("detailpanel_9").style.display="none";
		document.getElementById("detailpanel_10").style.display="none";
	 {rdelim} 
	if(strTaskType == \'Create Task\'){ldelim}
		document.getElementById("detailpanel_2").style.display="none";
		document.getElementById("detailpanel_3").style.display="none";
		document.getElementById("detailpanel_4").style.display="inline";
		document.getElementById("detailpanel_5").style.display="none";
		document.getElementById("detailpanel_6").style.display="none";
		document.getElementById("detailpanel_7").style.display="none";
		document.getElementById("detailpanel_8").style.display="none";
		document.getElementById("detailpanel_9").style.display="none";
		document.getElementById("detailpanel_10").style.display="none";
	 {rdelim} 
	if(strTaskType == \'Schedule Meeting\'){ldelim}
		document.getElementById("detailpanel_2").style.display="none";
		document.getElementById("detailpanel_3").style.display="none";
				document.getElementById("detailpanel_4").style.display="none";
		document.getElementById("detailpanel_5").style.display="none";
		document.getElementById("detailpanel_6").style.display="none";
		document.getElementById("detailpanel_7").style.display="inline";
		document.getElementById("detailpanel_8").style.display="none";
		document.getElementById("detailpanel_9").style.display="none";
		document.getElementById("detailpanel_10").style.display="none";
	 {rdelim} 
 	if(strTaskType == \'Schedule Call\'){ldelim}
		document.getElementById("detailpanel_2").style.display="none";
		document.getElementById("detailpanel_3").style.display="none";
		document.getElementById("detailpanel_4").style.display="none";
		document.getElementById("detailpanel_5").style.display="none";
		document.getElementById("detailpanel_6").style.display="inline";
		document.getElementById("detailpanel_7").style.display="none";
		document.getElementById("detailpanel_8").style.display="none";
		document.getElementById("detailpanel_9").style.display="none";
		document.getElementById("detailpanel_10").style.display="none";
	 {rdelim}
	if(strTaskType == \'Create New Record\'){ldelim}
		document.getElementById("detailpanel_2").style.display="none";
		document.getElementById("detailpanel_3").style.display="none";
		document.getElementById("detailpanel_4").style.display="none";
		document.getElementById("detailpanel_5").style.display="none";
		document.getElementById("detailpanel_6").style.display="none";
		document.getElementById("detailpanel_7").style.display="none";
		document.getElementById("detailpanel_8").style.display="inline";
		document.getElementById("detailpanel_9").style.display="none";
		document.getElementById("detailpanel_10").style.display="none";
	 {rdelim} 
	if(strTaskType == \'Create Project Task\'){ldelim}
		document.getElementById("detailpanel_2").style.display="none";
		document.getElementById("detailpanel_3").style.display="none";
		document.getElementById("detailpanel_4").style.display="none";
		document.getElementById("detailpanel_5").style.display="inline";
		document.getElementById("detailpanel_6").style.display="none";
		document.getElementById("detailpanel_7").style.display="none";
		document.getElementById("detailpanel_8").style.display="none";
		document.getElementById("detailpanel_9").style.display="none";
		document.getElementById("detailpanel_10").style.display="none";
	 {rdelim} 
 	if(strTaskType == \'Modify Object Field\'){ldelim}
		document.getElementById("detailpanel_2").style.display="none";
		document.getElementById("detailpanel_3").style.display="none";
		document.getElementById("detailpanel_4").style.display="none";
		document.getElementById("detailpanel_5").style.display="none";
		document.getElementById("detailpanel_6").style.display="none";
		document.getElementById("detailpanel_7").style.display="none";
		document.getElementById("detailpanel_8").style.display="none";
		document.getElementById("detailpanel_9").style.display="inline";
		document.getElementById("detailpanel_10").style.display="none";
	 {rdelim} 
 	if(strTaskType == \'Modify Related Object Field\'){ldelim}
		document.getElementById("detailpanel_2").style.display="none";
		document.getElementById("detailpanel_3").style.display="none";
		document.getElementById("detailpanel_4").style.display="none";
		document.getElementById("detailpanel_5").style.display="none";
		document.getElementById("detailpanel_6").style.display="none";
		document.getElementById("detailpanel_7").style.display="none";
		document.getElementById("detailpanel_8").style.display="none";
		document.getElementById("detailpanel_9").style.display="none";
		document.getElementById("detailpanel_10").style.display="inline";
	 {rdelim}    		    		    		    		    		   		 	
{rdelim}   		

    		
function load_filter_fields() {ldelim}

var finalobjectType = "object_fields";
var elementid;
elementid = finalobjectType;
var lstToCopyFromArray = document.getElementsByName(elementid); 
var lstToCopyFrom = lstToCopyFromArray[0];
//Now setup all 5 filter lists vars
var lstToCopyToArray = document.getElementsByName("process_object_modify_field"); 
var lstToCopyTo = lstToCopyToArray[0];

var currentFilterFilter1 = document.forms[\'EditView\'].process_object_modify_field.options[document.forms[\'EditView\'].process_object_modify_field.selectedIndex].text;

	for (x = lstToCopyTo.length; x >= 0; x--)
	{ldelim}
		lstToCopyTo[x] = null;
	{rdelim}	
	lstToCopyTo.options.add(new Option(\'Please Specify\',\'Please Specify\')); 
	for (i=0;i<lstToCopyFrom.options.length;i++)
		{ldelim}
		var listValue = lstToCopyFrom.options[i].text; 
		 lstToCopyTo.options.add(new Option(listValue,listValue));
 	{rdelim}
if(loadFilterFieldsCounter == 0){ldelim}
	objectType = document.forms[\'EditView\'].parent_object.value;
	var gURL = "getRelatedModules.php?table=" + objectType;
	YUC.successEvent.subscribe(handleEventsRelatedModules.success, handleEventsRelatedModules);
    var callback = {ldelim}
        success: function(o) {ldelim}
            document.getElementById("RELATED_INFO").innerHTML = o.responseText;       
        {rdelim}
    {rdelim}
    		loadFilterFieldsCounter = 1; 
     var connectionObject = YUC.asyncRequest ("GET", gURL, callback);     		
  {rdelim}   		
{rdelim}; 

    		

</script>',      
    ),
    'panels' => 
    array (
      'DEFAULT' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_NAME',
          ),
          1 => 
          array (
            'name' => 'task_type',
            'label' => 'LBL_TASK_TYPE',
           'displayParams' => 
            array (
              'required' => true,
              'javascript' => 'onchange="type_change();"',
            ),
          ),
        ),
        1 => 
        array (
         0 => 
          array (
            'name' => 'start_delay_type',
            'label' => 'LBL_START_DELAY_TYPE',
          ),
          1 => 
          array (
            'name' => 'task_order',
            'label' => 'LBL_TASK_ORDER',
          ),

        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'label' => 'LBL_DESCRIPTION',
          ),
        ),
      	3 =>
      	array (
      	0 =>
      	array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO_NAME',
      		),
      	),
      ),
     'CUSTOM SCRIPT' => 
      array (
                 0 =>
    				array (
    						0 =>
    						array (
    								'name' => '',
    								'label' => '',
    						),
    						1 =>
    						array (
    								'name' => '',
    								'label' => '',
    						),
    				),
        1 => 
        array (

          1 => 
          array (
            'name' => 'custom_script',
            'label' => 'LBL_CUSTOM_SCRIPT',
          ),
          2 =>
        	array (
        		'name' => 'upload_custom_script',
        		'label' => 'LBL_UPLOAD_CUSTOM_SCRIPT',
        		),

        ),
      ),
     'TASK EMAIL DETAILS' => 
      array (
                 0 =>
    				array (
    						0 =>
    						array (
    								'name' => '',
    								'label' => '',
    						),
    						1 =>
    						array (
    								'name' => '',
    								'label' => '',
    						),
    				),
        1 => 
        array (
          0 => 
          array (
            'name' => 'email_templates',
            'label' => 'LBL_CHOOSE_EMAIL_TEMPLATE',
          ),
          1 => 
          array (
            'name' => 'contact_roles',
            'label' => 'LBL_CHOOSE_CONTACT_ROLE',
          ),           
        ),
        //New Fields for Internal Email
        2 => 
        array (
          0 => 
      		array(
      		'name' => 'internal_email',
            'customCode' => '{if $fields.internal_email.value == "1"}' .
            	 	        '{assign var="INTERNAL_EMAIL_CHECK" value="checked"}' .
            	 	        '{else}' .
            	 	        '{assign var="INTERNAL_EMAIL_CHECK" value=""}' .
            	 	        '{/if}' .
            	 	        '<input name="internal_email"  type="checkbox" class="checkbox" value="1" {$INTERNAL_EMAIL_CHECK}>',
                    'label' => 'LBL_INTERNAL_EMAIL',
          ),
          1 => 
      		array(
      		'name' => 'send_email_to_caseopp_account',
            'customCode' => '{if $fields.send_email_to_caseopp_account.value == "1"}' .
            	 	        '{assign var="SEND_EMAIL_TO_CASE_OPP_ACCOUNT" value="checked"}' .
            	 	        '{else}' .
            	 	        '{assign var="SEND_EMAIL_TO_CASE_OPP_ACCOUNT" value=""}' .
            	 	        '{/if}' .
            	 	        '<input name="send_email_to_caseopp_account" id="send_email_to_caseopp_account"  type="checkbox" class="checkbox" value="1" {$SEND_EMAIL_TO_CASE_OPP_ACCOUNT}>',
                    'label' => 'LBL_SEND_EMAIL_TO_CASEOPP_ACCOUNT',
          ),  
        ),
        3 => 
        array (
          0 => 
      		array(
            'name' => 'internal_email_to_address',
            'label' => 'LBL_INTERNAL_EMAIL_ADDRESS',
           ),
          1 => 
      		array(
      		'name' => 'send_email_to_object_owner',
            'customCode' => '{if $fields.send_email_to_object_owner.value == "1"}' .
            	 	        '{assign var="SEND_EMAIL_TO_OBJECT_OWNER" value="checked"}' .
            	 	        '{else}' .
            	 	        '{assign var="SEND_EMAIL_TO_OBJECT_OWNER" value=""}' .
            	 	        '{/if}' .
            	 	        '<input name="send_email_to_object_owner" id="send_email_to_object_owner"  type="checkbox" class="checkbox" value="1" {$SEND_EMAIL_TO_OBJECT_OWNER}>',
                    'label' => 'LBL_SEND_EMAIL_TO_OBJECT_OWNER',
          ), 
        ), 
      		4 =>
      		array (
      				0 =>
      				array(
      						'name' => 'email_from_account',
      						'label' => 'LBL_EMAIL_FROM_ACCOUNT',
      				),
          1 => 
      		array(
      		'name' => 'send_email_to_contact',
            'customCode' => '{if $fields.send_email_to_contact.value == "1"}' .
            	 	        '{assign var="SEND_EMAIL_TO_CONTACT" value="checked"}' .
            	 	        '{else}' .
            	 	        '{assign var="SEND_EMAIL_TO_CONTACT" value=""}' .
            	 	        '{/if}' .
            	 	        '<input name="send_email_to_contact" id="send_email_to_contact"  type="checkbox" class="checkbox" value="1" {$SEND_EMAIL_TO_CONTACT}>',
                    'label' => 'LBL_SEND_EMAIL_TO_CONTACT',
          ), 
      		),
      		5 =>
      		array (
		          0 => 
		      		array(
		      		'name' => 'send_email_to_email_queue',
		            'customCode' => '{if $fields.send_email_to_email_queue.value == "1"}' .
		            	 	        '{assign var="SEND_EMAIL_TO_EMAIL_QUEUE" value="checked"}' .
		            	 	        '{else}' .
		            	 	        '{assign var="SEND_EMAIL_TO_EMAIL_QUEUE" value=""}' .
		            	 	        '{/if}' .
		            	 	        '<input name="send_email_to_email_queue" id="send_email_to_email_queue"  type="checkbox" class="checkbox" value="1" {$SEND_EMAIL_TO_EMAIL_QUEUE}>',
		                    'label' => 'LBL_SEND_EMAIL_TO_EMAIL_QUEUE',
		          ), 
          1 => 
          array (
            'name' => 'email_queue_campaign_name',
            'label' => 'LBL_EMAIL_QUEUE_CAMPAIGN_NAME',
          ),
      		),     		
      		
        //End New Fields for Internal Email         
      ), 
      'TASK DETAILS' => 
      array (
         0 =>
    				array (
    						0 =>
    						array (
    								'name' => '',
    								'label' => '',
    						),
    						1 =>
    						array (
    								'name' => '',
    								'label' => '',
    						),
    				),
        1 => 
        array (
          0 => 
          array (
            'name' => 'task_subject',
            'label' => 'LBL_TASK_SUBJECT',
          ),
          1 => 
          array (
            'name' => 'task_priority',
            'label' => 'LBL_TASK_PRIORITY',
          ), 
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'task_due_date_delay_minutes',
            'label' => 'LBL_DUE_DATE_DELAY_MINUTES',
          ),
          1 => 
          array (
            'name' => 'task_due_date_delay_hours',
            'label' => 'LBL_DUE_DATE_DELAY_HOURS',
          ), 
        ), 
        3 => 
        array (
          0 => 
          array (
            'name' => 'task_due_date_delay_days',
            'label' => 'LBL_DUE_DATE_DELAY_DAYS',
          ),
          1 => 
          array (
            'name' => 'task_due_date_delay_months',
            'label' => 'LBL_DUE_DATE_DELAY_MONTHS',
          ), 
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'task_due_date_delay_years',
            'label' => 'LBL_DUE_DATE_DELAY_YEARS',
          ),
          1 => 
          array (
            'name' => 'assigned_user_id_task',
            'label' => 'LBL_ASSIGNED_TO',
          ), 
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'is_escalatable_task_edit',
            'label' => 'LBL_IS_ESCALATABLE_TASK',
          ),
          1 => 
          array (
            'name' => 'escalation_delay_minutes_task',
            'label' => 'LBL_ESCALATION_DELAY_MINUTES_TASK',
          ), 
        ),       
        6 => 
        array (
          0 => 
          array (
            'name' => 'task_description',
            'label' => 'LBL_TASK_DESCRIPTION',
          ),   

        ),                                
      ),
      //PROJECT TASKS
      'PROJECT TASK DETAILS' => 
      array (
         0 =>
    				array (
    						0 =>
    						array (
    								'name' => '',
    								'label' => '',
    						),
    						1 =>
    						array (
    								'name' => '',
    								'label' => '',
    						),
    				),
        1 => 
        array (
          0 => 
          array (
            'name' => 'project_task_subject',
            'label' => 'LBL_PROJECT_TASK_SUBJECT',
          ),
          1 => 
          array (
          	'name' => 'project_task_id',
          	'label' => 'LBL_PROJECT_TASK_ID',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'project_task_status',
            'label' => 'LBL_PROJECT_TASK_STATUS',
          ),
          1 => 
          array (
            'name' => 'project_task_priority',
            'label' => 'LBL_PROJECT_TASK_PRIORITY',
          ), 
        ), 
        3 => 
        array (
          0 => 
          array (
            'name' => 'project_task_start_date',
            'label' => 'LBL_PROJECT_TASK_START_DATE_DELAY',
          ),
          1 => 
          array (
            'name' => 'project_task_end_date',
            'label' => 'LBL_PROJECT_TASK_END_DATE_DELAY',
          ), 
        ),
        4 => 
        array (
          0 => 
     	  array (
            'name' => 'assigned_user_id_project_task',
            'label' => 'LBL_ASSIGNED_TO',
          ),
          1 => 
     	  array (
            'name' => 'project_task_milestone',
            'label' => 'LBL_PROJECT_TASK_MILESTONE',
          ),  
        ),
        5 => 
        array (
          0 => 
     	  array (
            'name' => 'project_task_task_number',
            'label' => 'LBL_PROJECT_TASK_TASK_NUMBER',
          ),
          1 => 
     	  array (
            'name' => 'project_task_order',
            'label' => 'LBL_PROJECT_TASK_ORDER',
          ),  
        ),   
        6 => 
        array (
          0 => 
          array (
            'name' => 'project_task_description',
            'label' => 'LBL_PROJECT_TASK_DESCRIPTION',
          ),   

        ),                                
      ),
      //END PROJECT TASKS
      'CALL DETAILS' => 
      array (
                 0 =>
    				array (
    						0 =>
    						array (
    								'name' => '',
    								'label' => '',
    						),
    						1 =>
    						array (
    								'name' => '',
    								'label' => '',
    						),
    				),
        
        1 => 
        array (
          0 => 
          array (
            'name' => 'call_subject',
            'label' => 'LBL_CALL_SUBJECT',
          ),
          1 => 
      		array(
      		'name' => 'reminder_time',
            'customCode' => '{if $fields.reminder_checked.value == "1"}' .
            	 	        '{assign var="REMINDER_TIME_DISPLAY" value="inline"}' .
            	 	        '{assign var="REMINDER_CHECKED" value="checked"}' .
            	 	        '{else}' .
            	 	        '{assign var="REMINDER_TIME_DISPLAY" value="none"}' .
            	 	        '{assign var="REMINDER_CHECKED" value=""}' .
            	 	        '{/if}' .
            	 	        '<input name="reminder_checked" type="hidden" value="0"><input name="reminder_checked" onclick=\'toggleDisplay("should_remind_list");\' type="checkbox" class="checkbox" value="1" {$REMINDER_CHECKED}><div id="should_remind_list" style="display:{$REMINDER_TIME_DISPLAY}">{$fields.reminder_time.value}</div>',
                    'label' => 'LBL_REMINDER',
          ),
       ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'call_due_date_delay_minutes',
            'label' => 'LBL_START_DATE_DELAY_MINUTES',
          ),
          1 => 
          array (
            'name' => 'call_due_date_delay_hours',
            'label' => 'LBL_START_DATE_DELAY_HOURS',
          ), 
        ), 
        3 => 
        array (
          0 => 
          array (
            'name' => 'call_due_date_delay_days',
            'label' => 'LBL_START_DATE_DELAY_DAYS',
          ),
          1 => 
          array (
            'name' => 'call_due_date_delay_months',
            'label' => 'LBL_START_DATE_DELAY_MONTHS',
          ), 
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'call_due_date_delay_years',
            'label' => 'LBL_START_DATE_DELAY_YEARS',
          ),
          1 => 
          array (
            'name' => 'assigned_user_id_call',
            'label' => 'LBL_ASSIGNED_TO',
          ), 
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'is_escalatable_call_edit',
            'label' => 'LBL_IS_ESCALATABLE_CALL',
          ),
          1 => 
          array (
            'name' => 'escalation_delay_minutes_call',
            'label' => 'LBL_ESCALATION_DELAY_MINUTES_CALL',
          ), 
        ),        
        6 => 
        array (
          0 => 
          array (
            'name' => 'call_description',
            'label' => 'LBL_CALL_DESCRIPTION',
          ),   

        ),                                
      ),
//NEW PANEL TO SUPPORT MEETINGS      
      'MEETING DETAILS' => 
      array (
                 0 =>
    				array (
    						0 =>
    						array (
    								'name' => '',
    								'label' => '',
    						),
    						1 =>
    						array (
    								'name' => '',
    								'label' => '',
    						),
    				),
        1 => 
        array (
          0 => 
          array (
            'name' => 'meeting_subject',
            'label' => 'LBL_MEETING_SUBJECT',
          ),
          1 => 
      		array(
      		'name' => 'meeting_reminder_time',
            'customCode' => '{if $fields.meeting_reminder_checked.value == "1"}' .
            	 	        '{assign var="MEETING_REMINDER_TIME_DISPLAY" value="inline"}' .
            	 	        '{assign var="MEETING_REMINDER_CHECKED" value="checked"}' .
            	 	        '{else}' .
            	 	        '{assign var="MEETING_REMINDER_TIME_DISPLAY" value="none"}' .
            	 	        '{assign var="MEETING_REMINDER_CHECKED" value=""}' .
            	 	        '{/if}' .
            	 	        '<input name="meeting_reminder_checked" type="hidden" value="0"><input name="meeting_reminder_checked" onclick=\'toggleDisplay("should_remind_list_meeting");\' type="checkbox" class="checkbox" value="1" {$MEETING_REMINDER_CHECKED}><div id="should_remind_list_meeting" style="display:{$MEETING_REMINDER_TIME_DISPLAY}">{$fields.meeting_reminder_time.value}</div>',
                    'label' => 'LBL_REMINDER',
          ),
       ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'meeting_start_delay_minutes',
            'label' => 'LBL_START_DATE_DELAY_MINUTES',
          ),
          1 => 
          array (
            'name' => 'meeting_start_delay_hours',
            'label' => 'LBL_START_DATE_DELAY_HOURS',
          ), 
        ), 
        3 => 
        array (
          0 => 
          array (
            'name' => 'meeting_start_delay_days',
            'label' => 'LBL_START_DATE_DELAY_DAYS',
          ),
          1 => 
          array (
            'name' => 'meeting_start_delay_months',
            'label' => 'LBL_START_DATE_DELAY_MONTHS',
          ), 
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'meeting_start_delay_years',
            'label' => 'LBL_START_DATE_DELAY_YEARS',
          ),
          1 => 
          array (
            'name' => 'assigned_user_id_meeting',
            'label' => 'LBL_ASSIGNED_TO',
          ), 
        ),
         5 => 
        array (
          0 => 
          array (
            'name' => 'meeting_location',
            'label' => 'LBL_MEETING_LOCATION',
          ),
          1 => 
          array (
            'name' => '',
            'label' => '',
          ), 
        ),     
        6 => 
        array (
          0 => 
          array (
            'name' => 'meeting_description',
            'label' => 'LBL_MEETING_DESCRIPTION',
          ),   

        ),                                
      ), 
//END NEW PANEL TO SUPPORT MEETINGS   

     'CREATE OBJECT DETAILS' => 
      array (
                 0 =>
    				array (
    						0 =>
    						array (
    								'name' => '',
    								'label' => '',
    						),
    						1 =>
    						array (
    								'name' => '',
    								'label' => '',
    						),
    				),
        1 => 
        array (
          0 => 
          array (
            'name' => 'create_object_type',
            'label' => 'LBL_CREATE_OBJECT_TYPE',
          ),
          1 => 
          array (
            'name' => 'create_object_id',
            'label' => 'LBL_CREATE_OBJECT_ID',
          ), 
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'create_object_delay_minutes',
            'label' => 'LBL_CREATE_OBJECT_DELAY_MINUTES',
          ),
          1 => 
          array (
            'name' => 'create_object_delay_hours',
            'label' => 'LBL_CREATE_OBJECT_DELAY_HOURS',
          ), 
        ), 
        3 => 
        array (
          0 => 
          array (
            'name' => 'create_object_delay_days',
            'label' => 'LBL_CREATE_OBJECT_DELAY_DAYS',
          ),
          1 => 
          array (
            'name' => 'create_object_delay_months',
            'label' => 'LBL_CREATE_OBJECT_DELAY_MONTHS',
          ), 
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'create_object_delay_years',
            'label' => 'LBL_CREATE_OBJECT_DELAY_YEARS',
          ),
          1 => 
          array (
            'name' => 'assigned_user_id_create_object',
            'label' => 'LBL_ASSIGNED_TO',
          ), 
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'inherit_parent_data',
            'label' => 'LBL_INHERIT_PARENT_DATA',
          ),
          1 => 
          array (
            'name' => 'inherit_parent_relationships',
            'label' => 'LBL_INHERIT_PARENT_RELATIONSHIPS',
          ), 
        ),         
        6 => 
        array (
          0 => 
          array (
            'name' => 'relate_object_to_parent',
            'label' => 'LBL_RELATE_NEW_OBJECT_PARENT',
          ), 
         1 =>
        	array (
        		'name' => 'create_object_description',
        		'label' => 'LBL_CREATE_OBJECT_DESCRIPTION',
        	),

        ),           
      ),
     'MODIFY OBJECT FIELD DETAILS' => 
      array (
                 0 =>
    				array (
    						0 =>
    						array (
    								'name' => '',
    								'label' => '',
    						),
    						1 =>
    						array (
    								'name' => '',
    								'label' => '',
    						),
    				),
        1 => 
        array (
          0 => 
          array (
            'name' => 'process_object_modify_field',
            'label' => 'LBL_CHOOSE_FIELD_TO_MODIFY',
          ),
          1 => 
          array (
            'name' => 'process_object_modify_field_value',
            'label' => 'LBL_NEW_FIELD_VALUE',
          ),           
        ), 
       ), 
    	'MODIFY RELATED OBJECT FIELD DETAILS' =>
    		array (
         0 =>
    				array (
    						0 =>
    						array (
    								'name' => '',
    								'label' => '',
    						),
    						1 =>
    						array (
    								'name' => '',
    								'label' => '',
    						),
    				),
    				1 =>
    				array (
    						0 =>
    						array (
    								'name' => 'process_object_modify_related_object',
    								'label' => 'LBL_MODIFY_RELATED_OBJECT', 								
    						),
    						1 =>
    						array (
    								'name' => 'selected_related_object',
    								'label' => 'LBL_SELECTED_RELATED_OBJECT',
    						),
    				),
    				2 =>
    				array (
    						0 =>
    						array (
    								'name' => 'process_object_modify_related_field',
    								'label' => 'LBL_MODIFY_RELATED_FIELD',
    						),
    						1 =>
    						array (
    								'name' => 'process_object_modify_related_value',
    								'label' => 'LBL_MODIFY_RELATED_VALUE',
    						),
    				),    				
    	),    		
      'DIV_INFO' => 
      array (
       0 => 
        array (
          0 => 
          array (
            'name' => 'object_fields',
            'label' => 'LBL_PROJECT_FIELDS',
          ),
	        1 =>
	        array (
	        	'name' => 'parent_object',
	        	'label' => 'LBL_PROJECT_FIELDS',
	        ),        		

        ),
      ),
    		'RELATED_INFO' =>
    		array (
    				0 =>
    				array (
    						0 =>
    						array (
    								'name' => 'related_fields',
    								'label' => 'LBL_PROJECT_FIELDS',
    						),
    		
    				),
    		),
    		
    		'RELATED_FILTER_FIELDS_INFO' =>
    		array (
    				0 =>
    				array (
    						0 =>
    						array (
    								'name' => 'related_filter_fields',
    								'label' => 'LBL_PROJECT_FIELDS',
    						),
    						1 =>
    						array (
    								'name' => 'relatedObject',
    								'label' => 'LBL_PROJECT_FIELDS',
    						),
    		
    				),
    		),    		           
    ),
  ),
)
);
?>
