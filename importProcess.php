<?php
if(!defined('sugarEntry'))define('sugarEntry', true);
chdir(realpath(dirname(__FILE__)));
require_once('include/entryPoint.php');
global $db;
$target_file = basename($_FILES["processFile"]["name"]);
if (move_uploaded_file($_FILES["processFile"]["tmp_name"], $target_file)) {
    $sql = file_get_contents($target_file);
    //Parse the file row by row
    $lines = explode("\n", $sql);
    foreach($lines as $line){
        if(checkTables($line)) {
            $res = $db->query($line);
            //Grab the id so that we can direct after import
            $pos = strpos($line, "INSERT INTO pm_processmanager (");
            if ($pos !== false) {
                $pos = strpos($line, "VALUES");
                $pos = $pos + 9;
                $pid = substr($line, $pos, 36);
            }
        }
    }
    //If PID is not empty - redirect
    if(!empty($pid)){
        $queryParams = array(
            'module' => 'PM_ProcessManager',
            'action' => 'DetailView',
            'record' => $pid,
        );
        SugarApplication::redirect('index.php?' . http_build_query($queryParams));
    }else {
        echo("Process Successfully imported");
    }
} else {
    echo "Sorry, there was an error uploading your file.";
}

function checkTables($line){
    $tables = array(
        'pm_processmanager' => 'pm_processmanager',
        'pm_processmanagerstage' => 'pm_processmanagerstage',
        'pm_processmanagerstagetask' => 'pm_processmanagerstagetask',
        'pm_processmgerstagetask' => 'pm_processmgerstagetask',
        'pm_processmmanagerstage' => 'pm_processmmanagerstage',
        'pm_process_cancel_filter_table' => 'pm_process_cancel_filter_table',
        'pm_process_defs' => 'pm_process_defs',
        'pm_process_filter_table' => 'pm_process_filter_table',
        'pm_process_related_filter_table' => 'pm_process_related_filter_table',
        'pm_process_task_convert_lead_defs' => 'pm_process_task_convert_lead_defs',
        'pm_process_task_create_by_field_defs' => 'pm_process_task_create_by_field_defs',
        'pm_process_task_create_object_defs' => 'pm_process_task_create_object_defs',
        'pm_process_task_email_defs' => 'pm_process_task_email_defs',
        'pm_process_task_modify_field_defs' => 'pm_process_task_modify_field_defs',
        'pm_process_task_notification_defs' => 'pm_process_task_notification_defs',
        'pm_process_task_rest_defs' => 'pm_process_task_rest_defs',
        'pm_process_task_routing_defs' => 'pm_process_task_routing_defs',
        'pm_process_task_waiting_todo' => 'pm_process_task_waiting_todo',
        'pm_round_robin_tracking' => 'pm_round_robin_tracking',
    );
    foreach($tables as $key=>$value){
        $pos = strpos($line,$value);
        if($pos > 1){
            return TRUE;
        }
    }
    return FALSE;
}