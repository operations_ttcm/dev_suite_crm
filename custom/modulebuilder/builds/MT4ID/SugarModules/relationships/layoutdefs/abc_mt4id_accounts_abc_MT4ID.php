<?php
 // created: 2018-04-09 08:56:01
$layout_defs["abc_MT4ID"]["subpanel_setup"]['abc_mt4id_accounts'] = array (
  'order' => 100,
  'module' => 'Accounts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ABC_MT4ID_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'get_subpanel_data' => 'abc_mt4id_accounts',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
