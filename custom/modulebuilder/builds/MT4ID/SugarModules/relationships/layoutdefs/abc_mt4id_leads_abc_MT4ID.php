<?php
 // created: 2018-04-09 08:56:01
$layout_defs["abc_MT4ID"]["subpanel_setup"]['abc_mt4id_leads'] = array (
  'order' => 100,
  'module' => 'Leads',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ABC_MT4ID_LEADS_FROM_LEADS_TITLE',
  'get_subpanel_data' => 'abc_mt4id_leads',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
