<?php
// created: 2018-04-09 08:56:01
$dictionary["Lead"]["fields"]["abc_mt4id_leads"] = array (
  'name' => 'abc_mt4id_leads',
  'type' => 'link',
  'relationship' => 'abc_mt4id_leads',
  'source' => 'non-db',
  'module' => 'abc_MT4ID',
  'bean_name' => false,
  'vname' => 'LBL_ABC_MT4ID_LEADS_FROM_ABC_MT4ID_TITLE',
  'id_name' => 'abc_mt4id_leadsabc_mt4id_ida',
);
$dictionary["Lead"]["fields"]["abc_mt4id_leads_name"] = array (
  'name' => 'abc_mt4id_leads_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ABC_MT4ID_LEADS_FROM_ABC_MT4ID_TITLE',
  'save' => true,
  'id_name' => 'abc_mt4id_leadsabc_mt4id_ida',
  'link' => 'abc_mt4id_leads',
  'table' => 'abc_mt4id',
  'module' => 'abc_MT4ID',
  'rname' => 'name',
);
$dictionary["Lead"]["fields"]["abc_mt4id_leadsabc_mt4id_ida"] = array (
  'name' => 'abc_mt4id_leadsabc_mt4id_ida',
  'type' => 'link',
  'relationship' => 'abc_mt4id_leads',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ABC_MT4ID_LEADS_FROM_LEADS_TITLE',
);
