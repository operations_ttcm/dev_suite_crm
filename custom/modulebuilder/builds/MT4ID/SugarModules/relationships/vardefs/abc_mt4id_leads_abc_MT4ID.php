<?php
// created: 2018-04-09 08:56:01
$dictionary["abc_MT4ID"]["fields"]["abc_mt4id_leads"] = array (
  'name' => 'abc_mt4id_leads',
  'type' => 'link',
  'relationship' => 'abc_mt4id_leads',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'side' => 'right',
  'vname' => 'LBL_ABC_MT4ID_LEADS_FROM_LEADS_TITLE',
);
