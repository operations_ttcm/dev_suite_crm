<?php
// created: 2018-04-09 08:56:01
$dictionary["abc_MT4ID"]["fields"]["abc_mt4id_accounts"] = array (
  'name' => 'abc_mt4id_accounts',
  'type' => 'link',
  'relationship' => 'abc_mt4id_accounts',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ABC_MT4ID_ACCOUNTS_FROM_ACCOUNTS_TITLE',
);
