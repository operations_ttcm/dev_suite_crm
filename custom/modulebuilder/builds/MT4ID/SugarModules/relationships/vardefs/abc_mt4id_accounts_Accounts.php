<?php
// created: 2018-04-09 08:56:01
$dictionary["Account"]["fields"]["abc_mt4id_accounts"] = array (
  'name' => 'abc_mt4id_accounts',
  'type' => 'link',
  'relationship' => 'abc_mt4id_accounts',
  'source' => 'non-db',
  'module' => 'abc_MT4ID',
  'bean_name' => false,
  'vname' => 'LBL_ABC_MT4ID_ACCOUNTS_FROM_ABC_MT4ID_TITLE',
  'id_name' => 'abc_mt4id_accountsabc_mt4id_ida',
);
$dictionary["Account"]["fields"]["abc_mt4id_accounts_name"] = array (
  'name' => 'abc_mt4id_accounts_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ABC_MT4ID_ACCOUNTS_FROM_ABC_MT4ID_TITLE',
  'save' => true,
  'id_name' => 'abc_mt4id_accountsabc_mt4id_ida',
  'link' => 'abc_mt4id_accounts',
  'table' => 'abc_mt4id',
  'module' => 'abc_MT4ID',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["abc_mt4id_accountsabc_mt4id_ida"] = array (
  'name' => 'abc_mt4id_accountsabc_mt4id_ida',
  'type' => 'link',
  'relationship' => 'abc_mt4id_accounts',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ABC_MT4ID_ACCOUNTS_FROM_ACCOUNTS_TITLE',
);
