<?php
// created: 2018-04-09 08:56:01
$dictionary["abc_mt4id_accounts"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'abc_mt4id_accounts' => 
    array (
      'lhs_module' => 'abc_MT4ID',
      'lhs_table' => 'abc_mt4id',
      'lhs_key' => 'id',
      'rhs_module' => 'Accounts',
      'rhs_table' => 'accounts',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'abc_mt4id_accounts_c',
      'join_key_lhs' => 'abc_mt4id_accountsabc_mt4id_ida',
      'join_key_rhs' => 'abc_mt4id_accountsaccounts_idb',
    ),
  ),
  'table' => 'abc_mt4id_accounts_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'abc_mt4id_accountsabc_mt4id_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'abc_mt4id_accountsaccounts_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'abc_mt4id_accountsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'abc_mt4id_accounts_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'abc_mt4id_accountsabc_mt4id_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'abc_mt4id_accounts_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'abc_mt4id_accountsaccounts_idb',
      ),
    ),
  ),
);