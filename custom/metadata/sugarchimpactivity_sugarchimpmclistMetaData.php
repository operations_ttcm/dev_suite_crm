<?php






$dictionary["sugarchimpactivity_sugarchimpmclist"] = array(
    'relationships' => array(
        'sugarchimpactivity_sugarchimpmclist' => array(
            'lhs_module' => 'SugarChimpMCList',
            'lhs_table' => 'sugarchimp_mc_list',
            'lhs_key' => 'mailchimp_list_id',
            'rhs_module' => 'SugarChimpActivity',
            'rhs_table' => 'sugarchimpactivity',
            'rhs_key' => 'mailchimp_list_id',
            'relationship_type' => 'one-to-many',
        ),
    ),
);