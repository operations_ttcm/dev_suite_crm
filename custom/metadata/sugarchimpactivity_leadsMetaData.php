<?php






$dictionary["sugarchimpactivity_leads"] = array(
    'table' => 'sugarchimpactivity_leads',
    'fields' => array(
        'id'=>array(
            'name' => 'id',
            'type' => 'varchar',
            'len' => 36,
        ),
        'sugarchimpactivity_id'=>array(
            'name' => 'sugarchimpactivity_id',
            'type' => 'varchar',
            'len' => 36,
        ),
        'lead_id'=>array(
            'name' => 'lead_id',
            'type' => 'varchar',
            'len' => 36,
        ),	
        'date_created'=>array(
            'name' => 'date_created',
            'type' => 'datetime',
        ),
        'date_entered'=>array(
            'name' => 'date_entered',
            'type' => 'datetime',
        ),
        'date_modified'=>array(
            'name' => 'date_modified',
            'type' => 'datetime',
        ),
        'deleted'=>array(
            'name' => 'deleted',
            'type' => 'bool',
            'len' => '1',
            'default' => '0',
            'required' => true,
        ),
    ),
    'indices' => array(
        array(
            'name' => 'sugarchimpactivity_leadspk',
            'type' => 'primary',
            'fields' => array(
                0 => 'id',
            ),
        ),
        array(
            'name' => 'sugarchimpactivity_leads_idxa',
            'type' => 'index',
            'fields' => array(
                0 => 'sugarchimpactivity_id',
            ),
        ), 
        array(
            'name' => 'sugarchimpactivity_leads_idxb',
            'type' => 'index',
            'fields' => array(
                0 => 'lead_id',
            ),
        ),
        array(
            'name' => 'sugarchimpactivity_leads_idx',
            'type' => 'alternate_key',
            'fields' => array(
                0 => 'sugarchimpactivity_id',
                1 => 'lead_id',
            ),
        ),
    ),
    'relationships' => array(
        'sugarchimpactivity_leads' => array(
            'lhs_module' => 'SugarChimpActivity',
            'lhs_table' => 'sugarchimpactivity',
            'lhs_key' => 'id',
            'rhs_module' => 'Leads',
            'rhs_table' => 'leads',
            'rhs_key' => 'id',
            'relationship_type' => 'many-to-many',
            'join_table' => 'sugarchimpactivity_leads',
            'join_key_lhs' => 'sugarchimpactivity_id',
            'join_key_rhs' => 'lead_id',
        ),
    ),
);