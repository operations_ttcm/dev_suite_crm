<?php






$dictionary["sugarchimpactivity_accounts"] = array(
    'table' => 'sugarchimpactivity_accounts',
    'fields' => array(
        'id' => array(
            'name' => 'id',
            'type' => 'varchar',
            'len' => 36,
        ),
        'sugarchimpactivity_id'=>array(
            'name' => 'sugarchimpactivity_id',
            'type' => 'varchar',
            'len' => 36,
        ),
        'account_id'=>array(
            'name' => 'account_id',
            'type' => 'varchar',
            'len' => 36,
        ),	
        'date_created'=>array(
            'name' => 'date_created',
            'type' => 'datetime',
        ),
        'date_entered'=>array(
            'name' => 'date_entered',
            'type' => 'datetime',
        ),
        'date_modified'=>array(
            'name' => 'date_modified',
            'type' => 'datetime',
        ),
        'deleted'=>array(
            'name' => 'deleted',
            'type' => 'bool',
            'len' => '1',
            'default' => '0',
            'required' => true,
        ),
    ),
    'indices' => array(
        array(
            'name' => 'sugarchimpactivity_accountspk',
            'type' => 'primary',
            'fields' => array(
                0 => 'id',
            ),
        ),
        array(
            'name' => 'sugarchimpactivity_accounts_idxa',
            'type' => 'index',
            'fields' => array(
                0 => 'sugarchimpactivity_id',
            ),
        ), 
        array(
            'name' => 'sugarchimpactivity_accounts_idxb',
            'type' => 'index',
            'fields' => array(
                0 => 'account_id',
            ),
        ),
        array(
            'name' => 'sugarchimpactivity_accounts_idx',
            'type' => 'alternate_key',
            'fields' => array(
                0 => 'sugarchimpactivity_id',
                1 => 'account_id',
            ),
        ),
    ),
    'relationships' => array(
        'sugarchimpactivity_accounts' => array(
            'lhs_module' => 'SugarChimpActivity',
            'lhs_table' => 'sugarchimpactivity',
            'lhs_key' => 'id',
            'rhs_module' => 'Accounts',
            'rhs_table' => 'accounts',
            'rhs_key' => 'id',
            'relationship_type' => 'many-to-many',
            'join_table' => 'sugarchimpactivity_accounts',
            'join_key_lhs' => 'sugarchimpactivity_id',
            'join_key_rhs' => 'account_id',
        ),
    ),
);