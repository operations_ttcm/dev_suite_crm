<?php






$dictionary["sugarchimpactivity_contacts"] = array(
    'table' => 'sugarchimpactivity_contacts',
    'fields' => array(
        'id'=>array(
            'name' => 'id',
            'type' => 'varchar',
            'len' => 36,
        ),
        'sugarchimpactivity_id'=>array(
            'name' => 'sugarchimpactivity_id',
            'type' => 'varchar',
            'len' => 36,
        ),
        'contact_id'=>array(
            'name' => 'contact_id',
            'type' => 'varchar',
            'len' => 36,
        ),	
        'date_created'=>array(
            'name' => 'date_created',
            'type' => 'datetime',
        ),
        'date_entered'=>array(
            'name' => 'date_entered',
            'type' => 'datetime',
        ),
        'date_modified'=>array(
            'name' => 'date_modified',
            'type' => 'datetime',
        ),
        'deleted'=>array(
            'name' => 'deleted',
            'type' => 'bool',
            'len' => '1',
            'default' => '0',
            'required' => true,
        ),
    ),
    'indices' => array(
        array(
            'name' => 'sugarchimpactivity_contactspk',
            'type' => 'primary',
            'fields' => array(
                0 => 'id',
            ),
        ),
        array(
            'name' => 'sugarchimpactivity_contacts_idxa',
            'type' => 'index',
            'fields' => array(
                0 => 'sugarchimpactivity_id',
            ),
        ), 
        array(
            'name' => 'sugarchimpactivity_contacts_idxb',
            'type' => 'index',
            'fields' => array(
                0 => 'contact_id',
            ),
        ),
        array(
            'name' => 'sugarchimpactivity_contacts_idx',
            'type' => 'alternate_key',
            'fields' => array(
                0 => 'sugarchimpactivity_id',
                1 => 'contact_id',
            ),
        ),
    ),
    'relationships' => array(
        'sugarchimpactivity_contacts' => array(
            'lhs_module' => 'SugarChimpActivity',
            'lhs_table' => 'sugarchimpactivity',
            'lhs_key' => 'id',
            'rhs_module' => 'Contacts',
            'rhs_table' => 'contacts',
            'rhs_key' => 'id',
            'relationship_type' => 'many-to-many',
            'join_table' => 'sugarchimpactivity_contacts',
            'join_key_lhs' => 'sugarchimpactivity_id',
            'join_key_rhs' => 'contact_id',
        ),
    ),
);