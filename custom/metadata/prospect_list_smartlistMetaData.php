<?php







$dictionary["prospect_list_smartlist"] = array(
    'relationships' => array(
        'prospect_list_smartlist' => array(
            'lhs_module' => 'ProspectLists',
            'lhs_table' => 'prospect_lists',
            'lhs_key' => 'id',
            'rhs_module' => 'SmartList',
            'rhs_table' => 'smartlist',
            'rhs_key' => 'prospect_list_id',
            'relationship_type' => 'one-to-one',
        ),
    ),
);