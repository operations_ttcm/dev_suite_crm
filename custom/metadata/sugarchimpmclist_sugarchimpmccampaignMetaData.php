<?php






$dictionary["sugarchimpmclist_sugarchimpmccampaign"] = array(
    'relationships' => array(
        'sugarchimpmclist_sugarchimpmccampaign' => array(
            'lhs_module' => 'SugarChimpMCList',
            'lhs_table' => 'sugarchimp_mc_list',
            'lhs_key' => 'mailchimp_list_id',
            'rhs_module' => 'SugarChimpMCCampaign',
            'rhs_table' => 'sugarchimp_mc_campaign',
            'rhs_key' => 'mailchimp_list_id',
            'relationship_type' => 'one-to-many',
        ),
    ),
);