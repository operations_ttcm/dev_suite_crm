<?php






$dictionary["sugarchimpactivity_sugarchimpmccampaign"] = array(
    'relationships' => array(
        'sugarchimpactivity_sugarchimpmccampaign' => array(
            'lhs_module' => 'SugarChimpActivity',
            'lhs_table' => 'sugarchimpactivity',
            'lhs_key' => 'mailchimp_campaign_id',
            'rhs_module' => 'SugarChimpMCCampaign',
            'rhs_table' => 'sugarchimp_mc_campaign',
            'rhs_key' => 'mailchimp_campaign_id',
            'relationship_type' => 'one-to-many',
        ),
    ),
);