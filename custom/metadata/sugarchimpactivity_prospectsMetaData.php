<?php






$dictionary["sugarchimpactivity_prospects"] = array(
    'table' => 'sugarchimpactivity_prospects',
    'fields' => array(
        'id'=>array(
            'name' => 'id',
            'type' => 'varchar',
            'len' => 36,
        ),
        'sugarchimpactivity_id'=>array(
            'name' => 'sugarchimpactivity_id',
            'type' => 'varchar',
            'len' => 36,
        ),
        'prospect_id'=>array(
            'name' => 'prospect_id',
            'type' => 'varchar',
            'len' => 36,
        ),	
        'date_created'=>array(
            'name' => 'date_created',
            'type' => 'datetime',
        ),
        'date_entered'=>array(
            'name' => 'date_entered',
            'type' => 'datetime',
        ),
        'date_modified'=>array(
            'name' => 'date_modified',
            'type' => 'datetime',
        ),
        'deleted'=>array(
            'name' => 'deleted',
            'type' => 'bool',
            'len' => '1',
            'default' => '0',
            'required' => true,
        ),
    ),
    'indices' => array(
        array(
            'name' => 'sugarchimpactivity_prospectspk',
            'type' => 'primary',
            'fields' => array(
                0 => 'id',
            ),
        ),
        array(
            'name' => 'sugarchimpactivity_prospects_idxa',
            'type' => 'index',
            'fields' => array(
                0 => 'sugarchimpactivity_id',
            ),
        ), 
        array(
            'name' => 'sugarchimpactivity_prospects_idxb',
            'type' => 'index',
            'fields' => array(
                0 => 'prospect_id',
            ),
        ),
        array(
            'name' => 'sugarchimpactivity_prospects_idx',
            'type' => 'alternate_key',
            'fields' => array(
                0 => 'sugarchimpactivity_id',
                1 => 'prospect_id',
            ),
        ),
    ),
    'relationships' => array(
        'sugarchimpactivity_prospects' => array(
            'lhs_module' => 'SugarChimpActivity',
            'lhs_table' => 'sugarchimpactivity',
            'lhs_key' => 'id',
            'rhs_module' => 'Prospects',
            'rhs_table' => 'prospects',
            'rhs_key' => 'id',
            'relationship_type' => 'many-to-many',
            'join_table' => 'sugarchimpactivity_prospects',
            'join_key_lhs' => 'sugarchimpactivity_id',
            'join_key_rhs' => 'prospect_id',
        ),
    ),
);