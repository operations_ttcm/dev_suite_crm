<?php 





$app_list_strings['moduleList']['SugarChimp'] = 'SugarChimp';

$app_list_strings['moduleList']['SugarChimpMC'] = 'SugarChimpMC';
$app_list_strings['moduleList']['SugarChimpMCPeople'] = 'SugarChimpMCPeople';
$app_list_strings['moduleList']['SugarChimpMCCampaign'] = 'SugarChimpMCCampaign';
$app_list_strings['moduleList']['SugarChimpMCList'] = 'MailChimp Lists';
$app_list_strings['moduleList']['SugarChimpMCCampaign'] = 'MailChimp Campaigns';
$app_list_strings['moduleList']['SugarChimpMCCleanedEmail'] = 'SugarChimpMCCleanedEmail';
$app_list_strings['moduleList']['SugarChimpOptoutTracker'] = 'SugarChimpOptoutTracker';


$app_list_strings['moduleList']['SugarChimpMCActivity'] = 'SugarChimpMCActivity';
$app_list_strings['moduleList']['SugarChimpActivity'] = 'MailChimp Activities';
$app_list_strings['mailchimp_campaign_type_list']=array('regular'=>'Regular','automation'=>'Automation','absplit'=>'A/B Testing','rss'=>'RSS','variate'=>'Variate');
$app_list_strings['sugarchimpactivity_type_list']=array('open'=>'Open','click'=>'Click','send'=>'Send');

$app_strings['LBL_SMARTLIST_DASHLET_TITLE'] = 'MailChimp List Settings';
$app_strings['LBL_SMARTLIST_DASHLET_DESCRIPTION'] = 'Quick view of your synced MailChimp List settings.';


$app_strings['LBL_SUGARCHIMP_DASHLET_TITLE'] = 'MailChimp Activity';
$app_strings['LBL_SUGARCHIMP_DASHLET_DESCRIPTION'] = 'View activity data of your MailChimp subscribers right in SugarCRM.';

$app_strings['LBL_SUGARCHIMPACTIVITY_DASHLET_TITLE'] = 'MailChimp Campaign Summary';
$app_strings['LBL_SUGARCHIMPACTIVITY_DASHLET_DESCRIPTION'] = 'This dashboard will show you a detailed report summary for each of your MailChimp Campaigns.';