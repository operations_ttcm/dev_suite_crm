<?php







function getMailChimpLists()
{
    return SugarChimp::getMailChimpLists();
}
function getSugarChimpMCLists()
{
    return SugarChimp::getSugarChimpMCLists();
}

function getMailChimpCampaigns()
{
    return SugarChimp::getMailChimpCampaigns();
}

function getSugarChimpMCCampaigns()
{
    return SugarChimp::getSugarChimpMCCampaigns();
}

function getMailChimpDefaultModules()
{
    return SugarChimp::getMailChimpDefaultModules();
}