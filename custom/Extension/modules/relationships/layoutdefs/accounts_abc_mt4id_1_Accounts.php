<?php
 // created: 2018-04-13 10:47:56
$layout_defs["Accounts"]["subpanel_setup"]['accounts_abc_mt4id_1'] = array (
  'order' => 100,
  'module' => 'abc_MT4ID',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_ABC_MT4ID_1_FROM_ABC_MT4ID_TITLE',
  'get_subpanel_data' => 'accounts_abc_mt4id_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
