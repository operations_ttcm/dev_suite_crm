<?php
// created: 2018-04-13 10:47:56
$dictionary["Account"]["fields"]["accounts_abc_mt4id_1"] = array (
  'name' => 'accounts_abc_mt4id_1',
  'type' => 'link',
  'relationship' => 'accounts_abc_mt4id_1',
  'source' => 'non-db',
  'module' => 'abc_MT4ID',
  'bean_name' => 'abc_MT4ID',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_ABC_MT4ID_1_FROM_ABC_MT4ID_TITLE',
);
