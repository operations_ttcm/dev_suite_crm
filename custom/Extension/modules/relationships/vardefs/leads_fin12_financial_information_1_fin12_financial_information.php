<?php
// created: 2018-04-13 10:44:42
$dictionary["fin12_financial_information"]["fields"]["leads_fin12_financial_information_1"] = array (
  'name' => 'leads_fin12_financial_information_1',
  'type' => 'link',
  'relationship' => 'leads_fin12_financial_information_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_FIN12_FINANCIAL_INFORMATION_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_fin12_financial_information_1leads_ida',
);
$dictionary["fin12_financial_information"]["fields"]["leads_fin12_financial_information_1_name"] = array (
  'name' => 'leads_fin12_financial_information_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_FIN12_FINANCIAL_INFORMATION_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_fin12_financial_information_1leads_ida',
  'link' => 'leads_fin12_financial_information_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["fin12_financial_information"]["fields"]["leads_fin12_financial_information_1leads_ida"] = array (
  'name' => 'leads_fin12_financial_information_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_fin12_financial_information_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_FIN12_FINANCIAL_INFORMATION_1_FROM_FIN12_FINANCIAL_INFORMATION_TITLE',
);
