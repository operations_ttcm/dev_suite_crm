<?php
// created: 2018-04-13 10:45:21
$dictionary["Account"]["fields"]["accounts_fin12_financial_information_1"] = array (
  'name' => 'accounts_fin12_financial_information_1',
  'type' => 'link',
  'relationship' => 'accounts_fin12_financial_information_1',
  'source' => 'non-db',
  'module' => 'fin12_financial_information',
  'bean_name' => 'fin12_financial_information',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_1_FROM_FIN12_FINANCIAL_INFORMATION_TITLE',
);
