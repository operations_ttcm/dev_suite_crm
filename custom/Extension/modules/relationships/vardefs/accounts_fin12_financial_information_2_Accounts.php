<?php
// created: 2019-09-27 14:25:17
$dictionary["Account"]["fields"]["accounts_fin12_financial_information_2"] = array (
  'name' => 'accounts_fin12_financial_information_2',
  'type' => 'link',
  'relationship' => 'accounts_fin12_financial_information_2',
  'source' => 'non-db',
  'module' => 'fin12_financial_information',
  'bean_name' => 'fin12_financial_information',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_2_FROM_FIN12_FINANCIAL_INFORMATION_TITLE',
);
