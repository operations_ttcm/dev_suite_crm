<?php
// created: 2019-09-27 14:25:17
$dictionary["fin12_financial_information"]["fields"]["accounts_fin12_financial_information_2"] = array (
  'name' => 'accounts_fin12_financial_information_2',
  'type' => 'link',
  'relationship' => 'accounts_fin12_financial_information_2',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_2_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_fin12_financial_information_2accounts_ida',
);
$dictionary["fin12_financial_information"]["fields"]["accounts_fin12_financial_information_2_name"] = array (
  'name' => 'accounts_fin12_financial_information_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_2_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_fin12_financial_information_2accounts_ida',
  'link' => 'accounts_fin12_financial_information_2',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["fin12_financial_information"]["fields"]["accounts_fin12_financial_information_2accounts_ida"] = array (
  'name' => 'accounts_fin12_financial_information_2accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_fin12_financial_information_2',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_2_FROM_FIN12_FINANCIAL_INFORMATION_TITLE',
);
