<?php
// created: 2018-04-13 10:48:55
$dictionary["Lead"]["fields"]["leads_abc_mt4id_1"] = array (
  'name' => 'leads_abc_mt4id_1',
  'type' => 'link',
  'relationship' => 'leads_abc_mt4id_1',
  'source' => 'non-db',
  'module' => 'abc_MT4ID',
  'bean_name' => 'abc_MT4ID',
  'side' => 'right',
  'vname' => 'LBL_LEADS_ABC_MT4ID_1_FROM_ABC_MT4ID_TITLE',
);
