<?php






$layout_defs['SugarChimpActivity']['subpanel_setup']['accounts'] = array(
    'top_buttons' => array(array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => 'Accounts', 'mode' => 'MultiSelect'),),
    'order' => 900,
    'sort_by' => 'name',
    'sort_order' => 'asc',
    'module' => 'Accounts',
    'refresh_page'=>1,
    'subpanel_name' => 'default',
    'get_subpanel_data' => 'accounts',
    'add_subpanel_data' => 'account_id',
    'title_key' => 'LBL_ACCOUNTS_SUBPANEL_TITLE',
);

$layout_defs['SugarChimpActivity']['subpanel_setup']['contacts'] = array(
    'top_buttons' => array(array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => 'Contacts', 'mode' => 'MultiSelect'),),
    'order' => 900,
    'sort_by' => 'last_name',
    'sort_order' => 'asc',
    'module' => 'Contacts',
    'refresh_page'=>1,
    'subpanel_name' => 'default',
    'get_subpanel_data' => 'contacts',
    'add_subpanel_data' => 'contact_id',
    'title_key' => 'LBL_CONTACTS_SUBPANEL_TITLE',
);

$layout_defs['SugarChimpActivity']['subpanel_setup']['leads'] = array(
    'top_buttons' => array(array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => 'Leads', 'mode' => 'MultiSelect'),),
    'order' => 900,
    'sort_by' => 'last_name',
    'sort_order' => 'asc',
    'module' => 'Leads',
    'refresh_page'=>1,
    'subpanel_name' => 'default',
    'get_subpanel_data' => 'leads',
    'add_subpanel_data' => 'lead_id',
    'title_key' => 'LBL_LEADS_SUBPANEL_TITLE',
);

$layout_defs['SugarChimpActivity']['subpanel_setup']['prospects'] = array(
    'top_buttons' => array(array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => 'Prospects', 'mode' => 'MultiSelect'),),
    'order' => 900,
    'sort_by' => 'last_name',
    'sort_order' => 'asc',
    'module' => 'Prospects',
    'refresh_page'=>1,
    'subpanel_name' => 'default',
    'get_subpanel_data' => 'prospects',
    'add_subpanel_data' => 'prospect_id',
    'title_key' => 'LBL_PROSPECTS_SUBPANEL_TITLE',
);
