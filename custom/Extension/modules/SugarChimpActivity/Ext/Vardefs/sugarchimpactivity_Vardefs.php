<?php






$dictionary['SugarChimpActivity']['fields']['accounts'] = array(
    'name' => 'accounts',
    'type' => 'link',
    'relationship' => 'sugarchimpactivity_accounts',
    'module' => 'Accounts',
    'bean_name' => 'Accounts',
    'source' => 'non-db',
    'vname' => 'LBL_ACCOUNTS',
);

$dictionary['SugarChimpActivity']['fields']['contacts'] = array(
    'name' => 'contacts',
    'type' => 'link',
    'relationship' => 'sugarchimpactivity_contacts',
    'module' => 'Contacts',
    'bean_name' => 'Contacts',
    'source' => 'non-db',
    'vname' => 'LBL_CONTACTS',
);

$dictionary['SugarChimpActivity']['fields']['leads'] = array(
    'name' => 'leads',
    'type' => 'link',
    'relationship' => 'sugarchimpactivity_leads',
    'module' => 'Leads',
    'bean_name' => 'Leads',
    'source' => 'non-db',
    'vname' => 'LBL_LEADS',
);

$dictionary['SugarChimpActivity']['fields']['prospects'] = array(
    'name' => 'prospects',
    'type' => 'link',
    'relationship' => 'sugarchimpactivity_prospects',
    'module' => 'Prospects',
    'bean_name' => 'Prospects',
    'source' => 'non-db',
    'vname' => 'LBL_PROSPECTS',
);

$dictionary['SugarChimpActivity']['fields']['mailchimp_campaign_link'] = array(
    'name' => 'mailchimp_campaign_link',
    'type' => 'link',
    'relationship' => 'sugarchimpactivity_sugarchimpmccampaign',
    'module' => 'SugarChimpMCCampaign',
    'bean_name' => 'SugarChimpMCCampaign',
    'source' => 'non-db',
    'vname' => 'LBL_SUGARCHIMP_MC_CAMPAIGN_LINK',
);

$dictionary['SugarChimpActivity']['fields']['mailchimp_list_link'] = array(
    'name' => 'mailchimp_list_link',
    'type' => 'link',
    'relationship' => 'sugarchimpactivity_sugarchimpmclist',
    'module' => 'SugarChimpMCList',
    'bean_name' => 'SugarChimpMCList',
    'source' => 'non-db',
    'vname' => 'LBL_SUGARCHIMP_MC_LIST_LINK',
);

// $dictionary['SugarChimpActivity']['indices'][] = array(
//     'name' => 'sugarchimp_dup_check_idx',
//     'type' => 'index',
//     'fields' => array(
//         'name',
//         'activity',
//         'mailchimp_list_id',
//         'mailchimp_campaign_id',
//     )
// );
