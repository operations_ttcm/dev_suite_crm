<?php







$job_strings[] = 'SmartList';

require_once('modules/SmartList/includes/classes/SmartList/Scheduler.php');
require_once('modules/SmartList/includes/classes/SmartList/Logger.php');

// run by the smartlist scheduler
// handles all the queued actions for the smartlist module
function SmartList()
{
    // check if there are smartlist items that need to be checked
    // $result = SmartList_Scheduler::forge('SmartList')->start();
    SmartList_Logger::log('debug','Start SmartList_Scheduler');
    $result = SmartList_Scheduler::run();
    return true;
}