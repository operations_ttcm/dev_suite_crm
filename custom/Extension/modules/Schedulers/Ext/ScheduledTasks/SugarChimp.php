<?php





$job_strings[] = 'SugarChimp';

require_once('modules/SugarChimp/includes/classes/SugarChimp/Scheduler.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Helper.php');
require_once('modules/SugarChimp/includes/classes/SugarChimp/Setting.php');

// run by the sugarchimp scheduler
// handles all the queued actions either sugar to mailchimp or mailchimp to sugar
function SugarChimp()
{
    SugarChimp_Helper::log('debug','SugarChimp Scheduler Start');

    $apikey = SugarChimp_Setting::retrieve('apikey');

    if (empty($apikey))
    {
        SugarChimp_Helper::log('fatal','SugarChimp Scheduler: You do not have a MailChimp api key saved yet. You probably have not setup SugarChimp. Email support@sugarchimp.com if you need help.');
        return true;
    }

    // Cleanup Tables that need it before doing anything else
    $result = SugarChimp_Scheduler::forge('SugarChimpCleanup')->start();
    
    if ($result['elapsed_time'] === false || !preg_match("/^[0-9]+$/",$result['elapsed_time']))
    {
        // there's no time left to keep going
        // go ahead and return true so sugar scheduler keeps doing its thing
        // it'll pick up from here next time
        return true;
    }

    // process initial optout tracking tables
    // when SC runs for the first time it will import all of the optout and cleaned emails
    // this way we can better tracker who/why we send people to MC
    $result = SugarChimp_Scheduler::forge('MailchimpListToSugarCRM')->start();
    
    if ($result['elapsed_time'] === false || !preg_match("/^[0-9]+$/",$result['elapsed_time']))
    {
        // there's no time left to keep going
        // go ahead and return true so sugar scheduler keeps doing its thing
        // it'll pick up from here next time
        return true;
    }

    // check if there are backups that need to be run
    // backups are usually queued when field mappings are changed to ensure no data is lost on group syncing
    $result = SugarChimp_Scheduler::forge('SugarChimpBackups')->start();
    
    if ($result['elapsed_time'] === false || !preg_match("/^[0-9]+$/",$result['elapsed_time']))
    {
        // there's no time left to keep going
        // go ahead and return true so sugar scheduler keeps doing its thing
        // it'll pick up from here next time
        return true;
    }

    // check if there are queued updates to lists that have synced groups
    // if there are, we need to make sure all the possible sugar group values exist in MailChimp
    $result = SugarChimp_Scheduler::forge('SugarCRMGroupsToMailchimp')->start($result['elapsed_time']);
    
    if ($result['elapsed_time'] === false || !preg_match("/^[0-9]+$/",$result['elapsed_time']))
    {
        // there's no time left to keep going
        // go ahead and return true so sugar scheduler keeps doing its thing
        // it'll pick up from here next time
        return true;
    }

    $result = SugarChimp_Scheduler::forge('MailchimpToSugarCRM')->start($result['elapsed_time']);
    
    if (empty($result))
    {
        // there was an error, but it'll get caught/logged earlier
        // go ahead and return true so sugar scheduler keeps doing its thing
        return true;
    }
    
    if ($result['elapsed_time'] === false || !preg_match("/^[0-9]+$/",$result['elapsed_time']))
    {
        // there's no time left to keep going
        // go ahead and return true so sugar scheduler keeps doing its thing
        // it'll pick up from here next time
        return true;
    }
    
    // start running the sugar to mailchimp queue
    // start it with time elapsed to respect Sugar OD rules
    $result = SugarChimp_Scheduler::forge('SugarCRMToMailchimp')->start($result['elapsed_time']);
    
    if ($result['elapsed_time'] === false || !preg_match("/^[0-9]+$/",$result['elapsed_time']))
    {
        // there's no time left to keep going
        // go ahead and return true so sugar scheduler keeps doing its thing
        // it'll pick up from here next time
        return true;
    }
    
    
    
    // start running the mailchimp activity to sugar queue
    // start it with time elapsed to respect Sugar OD rules
    $result = SugarChimp_Scheduler::forge('MailchimpActivityToSugarCRM')->start($result['elapsed_time']);

    if ($result['elapsed_time'] === false || !preg_match("/^[0-9]+$/",$result['elapsed_time']))
    {
        // there's no time left to keep going
        // go ahead and return true so sugar scheduler keeps doing its thing
        // it'll pick up from here next time
        return true;
    }
    
    
    
    $result = SugarChimp_Scheduler::forge('UpdateSugarPersonFromMailchimp')->start($result['elapsed_time']);

    // always return true for sugar scheduler stuff
    return true;
}

// handles the initial import of mailchimp data into sugar
// uses the mailchimp export api to get the data as quickly as possible
// processed later by scheduler SugarChimp() above
class MailChimpToSugarCRM implements RunnableSchedulerJob
{
    public function setJob(SchedulersJob $job)
    {
        $this->job = $job;
    }

    //note: there is a chance of issues if someone queues up a job multiple times
    //we will do our best to ensure that something hasn't been synced already, but unsure how a conflict may occur yet
    //note 2: Just a reminder - if there is a weird error on a prod server and nothing is showing in the logs it may be a syntax error
    public function run($data)
    {
        $data = unserialize(base64_decode($data));
            
        try 
        {
            SugarChimp_Helper::log('debug',"MailChimpToSugarCRM->run() data: ".print_r($data,true));
            
            //if failure_count is 5 then email support...it will continue to try again after the 5th. Only want 1 email.
            //send current data and job->message
            if ($this->job->failure_count == 5)
            {
                //note: if we are seeing emails not sending frequently we may want to set one of the job columns as our "sent" column
                //then email if sent not set even if after 5
                SugarChimp_Helper::email_support($this->job->message,$data);
            }

            $finished = false;
        
            $admin = BeanFactory::getBean('Administration');
            $admin->retrieveSettings('sugarchimp');
            $api_key = $admin->settings['sugarchimp_apikey'];
            $error_message = '';
            
            //for tracking whether to requeue this list again
            $list_completed = false;
            $subscribers_completed = false;
            try {
                if ($data['create_list'] === true)
                {
                    $list_completed = SugarChimp_Helper::create_sugarcrm_list($data['list_id'], $data['default_module']);
                }
                else
                {
                    // don't create list, carry on
                    $list_completed = true;
                }
                if ($list_completed == true) {
                    $subscribers_completed = SugarChimp_Helper::import_subscribers_from_mailchimp($api_key, $data['list_id']);
                    if($subscribers_completed == false) 
                    {
                        $error_message .= ' Subscribers not completed for '.$data['list_id'];
                    }
                } 
                else 
                {
                    $error_message .= ' List not created for '.$data['list_id'].'|'.$data['default_module'];
                }
                
                if ($list_completed == true && $subscribers_completed == true) 
                {
                    SugarChimp_Helper::log('debug',"MailChimpToSugarCRM->run() list imported: ".$data['list_id']);
                    $finished = true;
                }
            } 
            catch (Exception $e) 
            {
                SugarChimp_Helper::log('fatal',"MailChimpToSugarCRM error when importing into SugarCRM: ".$e->getMessage());
                $error_message .= ' '.$e->getMessage();
                $finished = false;
            }
            
            if ($finished == false)
            {
                //todo: may want to use the SchedulersJob requeue instead as it tracks failure_count
                SugarChimp_Helper::log('fatal',"MailChimpToSugarCRM requeued as we are not quite finished.");
                $this->job->failJob('Failed to complete all lists.'.$error_message);
            }
        } 
        catch (Exception $e) 
        {
            $this->job->failJob($e->getMessage());
            SugarChimp_Helper::log('fatal',"MailChimpToSugarCRM error...requeuing the job: ".$e->getMessage());
            SugarChimp_Helper::queue_MailChimpToSugarCRM_job($data['list_id'],$data['default_module'],$data['create_list']);
            
            return false;
        }

        return true;
    }
}

// handles the import of cleaned emails for a mailchimp list
// uses the mailchimp export api to get the data as quickly as possible
// the queue is processed later by the mailchimp list scheduler 
class GetMailChimpCleanedEmails implements RunnableSchedulerJob
{
    public function setJob(SchedulersJob $job)
    {
        $this->job = $job;
    }

    public function run($data)
    {
        $data = unserialize(base64_decode($data));
            
        try 
        {
            SugarChimp_Helper::log('debug',"GetMailChimpCleanedEmails->run() - ".print_r($data,true));
            
            //if failure_count is 5 then email support...it will continue to try again after the 5th. Only want 1 email.
            //send current data and job->message
            if ($this->job->failure_count == 5)
            {
                //note: if we are seeing emails not sending frequently we may want to set one of the job columns as our "sent" column
                //then email if sent not set even if after 5
                SugarChimp_Helper::email_support("GetMailChimpCleanedEmails: ".$this->job->message,$data);
            }

            $admin = BeanFactory::getBean('Administration');
            $admin->retrieveSettings('sugarchimp');
            $api_key = $admin->settings['sugarchimp_apikey'];
            
            SugarChimp_Helper::import_cleaned_emails_from_mailchimp($api_key, $data['mailchimp_list_id'],$data['since']);
        } 
        catch (Exception $e) 
        {
            SugarChimp_Helper::log('fatal',"GetMailChimpCleanedEmails error...requeuing the job: ".$e->getMessage());
            $this->job->failJob($e->getMessage());
            
            return false;
        }

        return true;
    }
}



// Grabs all of the Campaigns from sugarchimp_activity_tracker
// This is the only way to grab old automations (get_campaigns will not)
// they are immediately added to the sugarchimp_mc_campaign table and dropdown
class GetMCCampaignsFromActivities implements RunnableSchedulerJob
{
    public function setJob(SchedulersJob $job)
    {
        $this->job = $job;
    }

    public function run($data)
    {
        global $db;
        $data = unserialize(base64_decode($data));
        try 
        {
            SugarChimp_Helper::log('debug',"GetMCCampaignsFromActivities->run()");
            //if failure_count is 5 then email support...it will continue to try again after the 5th. Only want 1 email.
            //send current data and job->message
            if ($this->job->failure_count == 5)
            {
                //note: if we are seeing emails not sending frequently we may want to set one of the job columns as our "sent" column
                //then email if sent not set even if after 5
                SugarChimp_Helper::email_support("GetMCCampaignsFromActivities: ".$this->job->message,$data);
            }

            // write sql to get all campaign id's from activity_tracker table
            // foreach campaign_id, call function to add it to table and dropdown
            $sql = "SELECT DISTINCT mailchimp_campaign_id
                FROM sugarchimp_activity_tracker
                WHERE 1=1";
            SugarChimp_Helper::log('debug','GetMCCampaignsFromActivities::run() sql: '.$sql);

            if (!$result = $db->query($sql))
            {
                SugarChimp_Helper::log('error','GetMCCampaignsFromActivities::run() sql query failed. sql: '.$sql);
                return false;
            }

            $campaign_ids = array();
            while ($row = $db->fetchByAssoc($result))
            {

                $campaign_ids []= $row['mailchimp_campaign_id'];
            }

            foreach($campaign_ids as $campaign_id)
            {
                // If not already there, add Campaign to sugarchimp_mc_campaign Table
                // also adds to the maintained dropdown menu
                SugarChimp_Helper::add_campaign_to_table($campaign_id);
            }
            // only run once
            // set config to true
            SugarChimp_Helper::log('debug','GetMCCampaignsFromActivities - set initial_campaign_table_fetched to true');

            SugarChimp_Setting::set('initial_campaign_table_fetched',true);
        } 
        catch (Exception $e) 
        {
            SugarChimp_Helper::log('error',"GetMCCampaignsFromActivities  - error...requeuing the job: ".$e->getMessage());
            $this->job->failJob($e->getMessage());
            
            return false;
        }

        return true;
    }
}

// handles the initial import of mailchimp campaign activity data into sugar
// uses the mailchimp export api to get the data as quickly as possible
// processed later by scheduler SugarChimp() above
class MailChimpToSugarCRMActivity implements RunnableSchedulerJob
{
    public function setJob(SchedulersJob $job)
    {
        $this->job = $job;
    }

    //note: there is a chance of issues if someone queues up a job multiple times
    //we will do our best to ensure that something hasn't been synced already, but unsure how a conflict may occur yet
    //note 2: Just a reminder - if there is a weird error on a prod server and nothing is showing in the logs it may be a syntax error
    public function run($data)
    {
        $activity_sync_enabled = SugarChimp_Setting::retrieve('activity_sync_enabled');
        if (empty($activity_sync_enabled))
        {
            SugarChimp_Helper::log('debug',"MailChimpToSugarCRMActivity->run() - activity syncing is disabled");
            return true;
        }
        
        $data = unserialize(base64_decode($data));
        
        if (empty($data['mailchimp_campaign_id']))
        {
            SugarChimp_Helper::log('fatal',"MailChimpToSugarCRMActivity->run() - mailchimp_campaign_id is required, but it is empty: ".print_r($data,true));
            return false;
        }
        
        if (empty($data['mailchimp_list_id']))
        {
            SugarChimp_Helper::log('fatal',"MailChimpToSugarCRMActivity->run() - mailchimp_list_id is required, but it is empty: ".print_r($data,true));
            return false;
        }
        
        $mailchimp_campaign_id = $data['mailchimp_campaign_id'];
        $mailchimp_list_id = $data['mailchimp_list_id'];

        if (empty($data['include_empty']))
        {
            $include_empty = false;
        }
        else
        {
            $include_send_activities = SugarChimp_Setting::retrieve('include_send_activities');
            SugarChimp_Helper::log('debug',"MailChimpToSugarCRMActivity->run() - check to include send activities - include_send_activities: ".print_r($include_send_activities,true));
            
            if(empty($include_send_activities))
            {
                $include_empty = false;
            }
            else
            {
                $include_empty = true;
            }
            
        }
        
        if (empty($data['since']))
        {
            $since = false;
        }
        else
        {
            $since = $data['since'];
        }
        
        try {
            SugarChimp_Helper::log('debug',"MailChimpToSugarCRMActivity->run() - ".print_r($data,true));
            
            //if failure_count is 5 then email support...it will continue to try again after the 5th. Only want 1 email.
            //send current data and job->message
            if ($this->job->failure_count == 5)
            {
                //note: if we are seeing emails not sending frequently we may want to set one of the job columns as our "sent" column
                //then email if sent not set even if after 5
                SugarChimp_Helper::email_support($this->job->message,$data);
            }

            $admin = BeanFactory::getBean('Administration');
            $admin->retrieveSettings('sugarchimp');
            $api_key = $admin->settings['sugarchimp_apikey'];
            $error_message = '';
            
            //for tracking whether to requeue this list again
            $acitivity_export_complete = false;
            try 
            {
                $acitivity_export_complete = SugarChimp_Helper::import_activities_from_mailchimp($api_key, $mailchimp_campaign_id, $mailchimp_list_id, $include_empty, $since);
            } 
            catch (Exception $e) 
            {
                SugarChimp_Helper::log('fatal',"MailChimpToSugarCRMActivity error when importing into SugarCRM: ".$e->getMessage());
                $error_message .= ' '.$e->getMessage();
            }

            if ($acitivity_export_complete == true) 
            {
                SugarChimp_Helper::log('debug',"MailChimpToSugarCRMActivity->run() campaign activity imported successfully: ".$mailchimp_campaign_id);
            }
            else
            {
                $error_message .= ' Campaign Activity import not completed for '.$mailchimp_campaign_id;
                SugarChimp_Helper::log('fatal',"MailChimpToSugarCRMActivity->run() error: ".$error_message);
                $this->job->failJob('Failed to complete all lists.'.$error_message);
            }
        } 
        catch (Exception $e) 
        {
            $this->job->failJob($e->getMessage());
            SugarChimp_Helper::log('fatal',"MailChimpToSugarCRMActivity error...requeuing the job: ".$e->getMessage());
            $data = base64_encode(serialize($data));
            SugarChimp_Helper::queue_MailChimpToSugarCRMActivity_job($data);
            
            return false;
        }

        return true;
    }
}

