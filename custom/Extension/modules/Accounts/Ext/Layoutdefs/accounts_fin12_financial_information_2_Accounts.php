<?php
 // created: 2019-09-27 14:25:17
$layout_defs["Accounts"]["subpanel_setup"]['accounts_fin12_financial_information_2'] = array (
  'order' => 100,
  'module' => 'fin12_financial_information',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_2_FROM_FIN12_FINANCIAL_INFORMATION_TITLE',
  'get_subpanel_data' => 'accounts_fin12_financial_information_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
