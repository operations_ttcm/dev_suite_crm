<?php
 // created: 2018-04-13 10:45:21
$layout_defs["Accounts"]["subpanel_setup"]['accounts_fin12_financial_information_1'] = array (
  'order' => 100,
  'module' => 'fin12_financial_information',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_1_FROM_FIN12_FINANCIAL_INFORMATION_TITLE',
  'get_subpanel_data' => 'accounts_fin12_financial_information_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
