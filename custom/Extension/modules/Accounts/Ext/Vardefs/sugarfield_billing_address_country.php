<?php
 // created: 2019-05-29 13:10:39
$dictionary['Account']['fields']['billing_address_country']['audited']=true;
$dictionary['Account']['fields']['billing_address_country']['inline_edit']=true;
$dictionary['Account']['fields']['billing_address_country']['comments']='The country used for the billing address';
$dictionary['Account']['fields']['billing_address_country']['merge_filter']='disabled';

 ?>