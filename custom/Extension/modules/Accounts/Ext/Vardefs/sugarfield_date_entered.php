<?php
 // created: 2018-07-11 10:29:26
$dictionary['Account']['fields']['date_entered']['inline_edit']=true;
$dictionary['Account']['fields']['date_entered']['comments']='Date record created';
$dictionary['Account']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Account']['fields']['date_entered']['massupdate']=0;
$dictionary['Account']['fields']['date_entered']['audited']=true;

 ?>