<?php
// created: 2018-04-13 10:45:21
$dictionary["fin12_financial_information"]["fields"]["accounts_fin12_financial_information_1"] = array (
  'name' => 'accounts_fin12_financial_information_1',
  'type' => 'link',
  'relationship' => 'accounts_fin12_financial_information_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_fin12_financial_information_1accounts_ida',
);
$dictionary["fin12_financial_information"]["fields"]["accounts_fin12_financial_information_1_name"] = array (
  'name' => 'accounts_fin12_financial_information_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_fin12_financial_information_1accounts_ida',
  'link' => 'accounts_fin12_financial_information_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["fin12_financial_information"]["fields"]["accounts_fin12_financial_information_1accounts_ida"] = array (
  'name' => 'accounts_fin12_financial_information_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_fin12_financial_information_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_1_FROM_FIN12_FINANCIAL_INFORMATION_TITLE',
);
