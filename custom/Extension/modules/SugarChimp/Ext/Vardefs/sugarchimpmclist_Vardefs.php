<?php






$dictionary['SugarChimpMCList']['fields']['sugarchimpactivity_link'] = array(
    'name' => 'sugarchimpactivity_link',
    'type' => 'link',
    'relationship' => 'sugarchimpactivity_sugarchimpmclist',
    'module' => 'SugarChimpMCList',
    'bean_name' => 'SugarChimpMCList',
    'source' => 'non-db',
    'vname' => 'LBL_SUGARCHIMP_ACTIVITY',
);

$dictionary['SugarChimpMCList']['fields']['sugarchimpmccampaign_link'] = array(
    'name' => 'sugarchimpmccampaign_link',
    'type' => 'link',
    'relationship' => 'sugarchimpmclist_sugarchimpmccampaign',
    'module' => 'SugarChimpMCList',
    'bean_name' => 'SugarChimpMCList',
    'source' => 'non-db',
    'vname' => 'LBL_SUGARCHIMP_MC_CAMPAIGN',
);

$dictionary['SugarChimpMCList']['indices'][] = array(
    'name' => 'idx_mailchimp_list_id',
    'type' => 'index',
    'fields' => array(
        'mailchimp_list_id',
    )
);