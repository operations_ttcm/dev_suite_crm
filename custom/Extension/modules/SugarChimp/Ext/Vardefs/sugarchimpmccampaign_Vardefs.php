<?php







$dictionary['SugarChimpMCCampaign']['fields']['sugarchimpactivity_link'] = array(
    'name' => 'sugarchimpactivity_link',
    'type' => 'link',
    'relationship' => 'sugarchimpactivity_sugarchimpmccampaign',
    'module' => 'SugarChimpActivity',
    'bean_name' => 'SugarChimpActivity',
    'source' => 'non-db',
    'vname' => 'LBL_SUGARCHIMP_ACTIVITY',
);

$dictionary['SugarChimpMCCampaign']['fields']['sugarchimpmclist_link'] = array(
    'name' => 'sugarchimpmclist_link',
    'type' => 'link',
    'relationship' => 'sugarchimpmclist_sugarchimpmccampaign',
    'module' => 'SugarChimpMCList',
    'bean_name' => 'SugarChimpMCList',
    'source' => 'non-db',
    'vname' => 'LBL_MAILCHIMP_LIST',
);