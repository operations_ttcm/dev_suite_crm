<?php







/**
if you edit this definition, also change /scripts/post_install.php
*/
$dictionary['ProspectList']['fields']['mailchimp_list_name_c'] = array(
    'name' => 'mailchimp_list_name_c',
    'label' => 'LBL_MAILCHIMP_LIST_NAME',
    'type' => 'enum',
    'function' => 'getMailChimpLists',
    'function_bean' => 'SugarChimp',
    'len' => 255,
);

/**
if you edit this definition, also change /scripts/post_install.php
*/
$dictionary['ProspectList']['fields']['mailchimp_default_module_c'] = array(
    'name' => 'mailchimp_default_module_c',
    'label' => 'LBL_MAILCHIMP_DEFAULT_MODULE',
    'type' => 'enum',
    'function' => 'getMailChimpDefaultModules',
    'function_bean' => 'SugarChimp',
    'len' => 255,
);