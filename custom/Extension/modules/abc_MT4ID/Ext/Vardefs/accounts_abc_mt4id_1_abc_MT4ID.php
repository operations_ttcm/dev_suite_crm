<?php
// created: 2018-04-13 10:47:56
$dictionary["abc_MT4ID"]["fields"]["accounts_abc_mt4id_1"] = array (
  'name' => 'accounts_abc_mt4id_1',
  'type' => 'link',
  'relationship' => 'accounts_abc_mt4id_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_ABC_MT4ID_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_abc_mt4id_1accounts_ida',
);
$dictionary["abc_MT4ID"]["fields"]["accounts_abc_mt4id_1_name"] = array (
  'name' => 'accounts_abc_mt4id_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_ABC_MT4ID_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_abc_mt4id_1accounts_ida',
  'link' => 'accounts_abc_mt4id_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["abc_MT4ID"]["fields"]["accounts_abc_mt4id_1accounts_ida"] = array (
  'name' => 'accounts_abc_mt4id_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_abc_mt4id_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_ABC_MT4ID_1_FROM_ABC_MT4ID_TITLE',
);
