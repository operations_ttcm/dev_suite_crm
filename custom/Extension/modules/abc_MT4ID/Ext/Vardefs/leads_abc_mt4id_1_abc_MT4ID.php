<?php
// created: 2018-04-13 10:48:55
$dictionary["abc_MT4ID"]["fields"]["leads_abc_mt4id_1"] = array (
  'name' => 'leads_abc_mt4id_1',
  'type' => 'link',
  'relationship' => 'leads_abc_mt4id_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_ABC_MT4ID_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_abc_mt4id_1leads_ida',
);
$dictionary["abc_MT4ID"]["fields"]["leads_abc_mt4id_1_name"] = array (
  'name' => 'leads_abc_mt4id_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_ABC_MT4ID_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_abc_mt4id_1leads_ida',
  'link' => 'leads_abc_mt4id_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["abc_MT4ID"]["fields"]["leads_abc_mt4id_1leads_ida"] = array (
  'name' => 'leads_abc_mt4id_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_abc_mt4id_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_ABC_MT4ID_1_FROM_ABC_MT4ID_TITLE',
);
