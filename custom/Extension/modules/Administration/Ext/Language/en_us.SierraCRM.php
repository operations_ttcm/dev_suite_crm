<?php

$mod_strings['LBL_PM_PROCESSMANAGER'] = 'Process Manager Add-on';
$mod_strings['LBL_PM_PROCESSMANAGER_LICENSE_TITLE'] = 'License Configuration';
$mod_strings['LBL_PM_PROCESSMANAGER_LICENSE'] = 'Manage and configure the license for Process Manager';
