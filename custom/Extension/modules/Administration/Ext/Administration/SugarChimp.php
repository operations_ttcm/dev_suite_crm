<?php 







global $sugar_version;

$admin_option_defs=array();

if(preg_match( "/^6.*/", $sugar_version) ) {
    $admin_option_defs['Administration']['sugarchimp_setup']= array('Import','LBL_SUGARCHIMP_SETUP_WIZARD_TITLE','LBL_SUGARCHIMP_SETUP_WIZARD','./index.php?module=SugarChimp&action=setup');
    $admin_option_defs['Administration']['sugarchimp_health']= array('Diagnostic','LBL_SUGARCHIMP_HEALTH_STATUS_TITLE','LBL_SUGARCHIMP_HEALTH_STATUS','./index.php?module=SugarChimp&action=health_status');
    $admin_option_defs['Administration']['sugarchimp_field_mapping']= array('Import','LBL_SUGARCHIMP_FIELD_MAPPING_TITLE','LBL_SUGARCHIMP_FIELD_MAPPING','./index.php?module=SugarChimp&action=field_mapping');
    $admin_option_defs['Administration']['sugarchimp_campaign_summary']= array('Import','LBL_SUGARCHIMP_CAMPAIGN_SUMMARY_TITLE','LBL_SUGARCHIMP_CAMPAIGN_SUMMARY','./index.php?module=SugarChimp&action=campaigns');
    $admin_option_defs['Administration']['sugarchimp_data_privacy']= array('Import','LBL_SUGARCHIMP_DATA_PRIVACY_TITLE','LBL_SUGARCHIMP_DATA_PRIVACY','./index.php?module=SugarChimp&action=data_privacy');
} else {
    $admin_option_defs['Administration']['sugarchimp_setup']= array('Import','LBL_SUGARCHIMP_SETUP_WIZARD_TITLE','LBL_SUGARCHIMP_SETUP_WIZARD','javascript:parent.SUGAR.App.router.navigate("SugarChimp/layout/setup", {trigger: true});');
    $admin_option_defs['Administration']['sugarchimp_health']= array('Diagnostic','LBL_SUGARCHIMP_HEALTH_STATUS_TITLE','LBL_SUGARCHIMP_HEALTH_STATUS','javascript:parent.SUGAR.App.router.navigate("SugarChimp/layout/health-status", {trigger: true});');
    $admin_option_defs['Administration']['sugarchimp_field_mapping']= array('Import','LBL_SUGARCHIMP_FIELD_MAPPING_TITLE','LBL_SUGARCHIMP_FIELD_MAPPING','javascript:parent.SUGAR.App.router.navigate("SugarChimp/layout/field-mapping", {trigger: true});');
    $admin_option_defs['Administration']['sugarchimpactivity_index']= array('Import','LBL_SUGARCHIMPACTIVITY_TITLE','LBL_SUGARCHIMPACTIVITY','javascript:parent.SUGAR.App.router.navigate("SugarChimpActivity", {trigger: true});');
    $admin_option_defs['Administration']['sugarchimp_data_privacy']= array('Import','LBL_SUGARCHIMP_DATA_PRIVACY_TITLE','LBL_SUGARCHIMP_DATA_PRIVACY','javascript:parent.SUGAR.App.router.navigate("SugarChimp/layout/data-privacy", {trigger: true});');
}

$admin_group_header[]= array('LBL_SUGARCHIMP','',false,$admin_option_defs, '');

