<?php






$dictionary['Lead']['fields']['sugarchimpactivity'] = array(
    'name' => 'sugarchimpactivity',
    'type' => 'link',
    'relationship' => 'sugarchimpactivity_leads',
    'module' => 'SugarChimpActivity',
    'bean_name' => 'SugarChimpActivity',
    'source' => 'non-db',
    'vname' => 'LBL_SUGARCHIMPACTIVITY',
);

$dictionary['Lead']['fields']['mailchimp_rating_c'] = array(
	'name' => 'mailchimp_rating_c',
	'vname' => 'LBL_MAILCHIMP_RATING',
	'type' => 'mailchimprating',
	'dbType' => 'int',
	'len' => '3',
	'comment' => 'MailChimp Rating',
	'required' => false,
	'studio' => true,
	'reportable' => true,
	'default' => 2,
	'readonly' => true,
);