<?php
// created: 2018-04-13 10:44:42
$dictionary["Lead"]["fields"]["leads_fin12_financial_information_1"] = array (
  'name' => 'leads_fin12_financial_information_1',
  'type' => 'link',
  'relationship' => 'leads_fin12_financial_information_1',
  'source' => 'non-db',
  'module' => 'fin12_financial_information',
  'bean_name' => 'fin12_financial_information',
  'side' => 'right',
  'vname' => 'LBL_LEADS_FIN12_FINANCIAL_INFORMATION_1_FROM_FIN12_FINANCIAL_INFORMATION_TITLE',
);
