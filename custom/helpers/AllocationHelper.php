<?php

class AllocationHelper {

    public static function assignAccountManager($bean, $user_id = null, $roundRobin = false) {
        global $db;
        $id = $bean->id;

        if ($user_id) {
            $bean->assigned_user_id = $user_id;
            $sql = "UPDATE leads SET assigned_user_id = '{$user_id}' WHERE  id='{$id}'";
            $res = $db->query($sql, true);
            $GLOBALS['log']->fatal('Aristos123 - records updated:' . $db->getAffectedRowCount($res));
            return TRUE;
        }

        return FALSE;
    }

}
