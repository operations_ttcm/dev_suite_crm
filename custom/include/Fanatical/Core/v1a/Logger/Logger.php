<?php







namespace Fanatical\Core\v1a;

class Logger
{
	// $format determines if you want to auto-format your log entry 
	// Gives detailed feedback about location of call,
	// Without the need to pass a ton of information
	// Keeps $message short to just the details...location not needed
	public static function log($level, $message = '', $format = true)
	{
		if($format === true)
		{	
			$message = self::format_message($message);
		}
        // if 'debug', all sugarchimp messages are logged at 'fatal' level
        // this allows you to run sugarchimp on debug mode and the rest of the system at any other level
        // if not 'debug', run on level provided in call
        
        switch ($level)
        {
            case 'debug':
                $GLOBALS['log']->debug($message);
                break;
            case 'info':
                $GLOBALS['log']->info($message);
                break;
            case 'deprecated':
                $GLOBALS['log']->deprecated($message);
                break;
            case 'warn':
                $GLOBALS['log']->warn($message);
                break;
            case 'error':
                $GLOBALS['log']->error($message);
                break;
            case 'fatal':
                $GLOBALS['log']->fatal($message);
                break;
            case 'security':
                $GLOBALS['log']->security($message);
                break;
            default:
                $GLOBALS['log']->debug($message);
                break;
        }
    }

    //Uses call_stack to get logging information
    //Example Stack:
    //Call 0: where we are
    //Call 1: log function
    //Call 2: SmartList_Logger
    //Call 3: Whatever called SmartList_Logger
    //
    //Example return: 'SmartList-SmartList_queue::queue(20): Job added to queue'
    public static function format_message($message)
    {
        /*
        76dev todo-jon: need to see if there is extra overhead to use this
        need to test on OD environment too

		//gets all the call_stack
		$calls = debug_backtrace(false);
		//Takes it back to AddonName_Logger
		$addon = $calls[2]['class'];
		//Takes off the "_Logger" piece of Class name
		$addon = substr($addon, 0, -7);
		//Goes back to the original Logger Call
		//Grabs all the information from there
		$original_call = $calls[3];
		$function = $original_call['function'];
		$class = $original_call['class'];
		$line = $original_call['line'];

		//Generic Log Idea, using the call stack to derive class::method
		//Could include line# if wanted...minimizing what has to be passed
		$message = $addon.'['.$level.']-'.$class.'::'.$function.'('.$line.'): '.$message;
        */
    	return $message;
    }
}