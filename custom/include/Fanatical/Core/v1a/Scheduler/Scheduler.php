<?php







namespace Fanatical\Core\v1a;

use BeanFactory as BeanFactory;
Loader::load('Core/Logger');

abstract class Scheduler
{
	public static $job_function = "Fanatical_Job";

	abstract public static function run();

    // if static::job_function matches the name of a Sugar Scheduler
    // this method will return some details about the status of the scheduler
    // is it active/inactive, when was the last time it run, how often is it set to run
	public static function getHealth()
    {
        global $db;
        $scheduler = BeanFactory::getBean('Schedulers');
        Logger::log('debug','Find Scheduler by job: ' . static::$job_function);

        $scheduler->retrieve_by_string_fields(array('job' => 'function::' . static::$job_function)); 

        if (!empty($scheduler->id))
        {
            $success = true;
            $found = true;
            $id = $scheduler->id;
            $status = $scheduler->status;
            $last_ran = $scheduler->last_run;
            Logger::log('debug','Scheduler found: id:'.$id);
            
            global $mod_strings, $current_language;
            $temp_mod_strings = $mod_strings;
            $mod_strings = return_module_language($current_language, 'Schedulers');  
            
            $scheduler->get_list_view_data(); //sets some vars we need
            $interval = $scheduler->intervalHumanReadable;

            $mod_strings = $temp_mod_strings;
            
            //format last_ran
            if (!empty($last_ran)) {
                global $current_user, $timedate;

                $last_ran = $timedate->to_display_date_time($last_ran, true, true, $current_user);
            }
            else{
                $last_ran = 'Has not run yet';
            }
            
        } 
        //Scheduler with that job function was not found
        else 
        {
            Logger::log('debug','Scheduler not found for job: '. static::$job_function);
            $success = false;
            $found = false;
            $last_ran = 'The '.static::$job_function.' scheduler job is missing.';
            $interval = '';
            $status = 'Inactive';
            $id = false;
        }
        
        return array(
            'success' => $success,
            'found' => $found,
            'interval' => $interval,
            'id' => $id,
            'last_ran' => $last_ran,
            'status' => $status,
        );
    }
}