<?php







namespace Fanatical\Core\v1a\Queue;

class TimesUpException extends \Exception {}


namespace Fanatical\Core\v1a;

Loader::load('Core/Logger');

use Fanatical\Core\v1a\Queue\TimesUpException as TimesUpException;

abstract class Queue
{
    protected $start_time = false;
    protected $last_time = false;
    protected $elapsed_time = false;

    protected $max_time_to_run = 300; // in seconds
    protected $time_buffer = 0.8; // ex. If set to .8, we're saying that we're going to stop doing work once we've used 80% of the max time
    protected $max_records_to_process = 500; // max number of job records to process in single call
    protected $queue_table_name = '';

    public static function forge($max_time_to_run=false, $max_records_to_process=false, $time_buffer=false, $queue_table_name=false)
    {
        return new static($max_time_to_run, $max_records_to_process, $time_buffer, $queue_table_name);
    }

    function __construct($max_time_to_run=false,$max_records_to_process=false, $time_buffer=false, $queue_table_name=false)
    {
        if (!empty($max_time_to_run))
        {
            $this->max_time_to_run = $max_time_to_run;
        }
        
        if (!empty($max_records_to_process))
        {
            $this->max_records_to_process = $max_records_to_process;
        }
        
        if (!empty($time_buffer))
        {
            $this->time_buffer = $time_buffer;
        }

        if (!empty($queue_table_name))
        {
            $this->queue_table_name = $queue_table_name;
        }
    }
    
    public function start($elapsed_time=false)
    {
        Logger::log('debug','start');

        $this->start_timer($elapsed_time);
        
        try 
        {
            $result = $this->process();
        }
        catch (TimesUpException $e)
        {
            // time ran out for this scheduler run
            // will pick up where we left off last time
            Logger::log('debug', "TimesUpException caught: ".$e->getMessage());
            return $this->end(array(
                'status' => 'success',
                'message' => $e->getMessage(),
            ));
        }
        catch (\Exception $e)
        {
            // error exception caught
            Logger::log('fatal', "Exception caught: ".$e->getMessage());
            return $this->end(array(
                'status' => 'error',
                'message' => $e->getMessage(),
            ));
        }
        
        return $result;
    }

    public function end($result)
    {
        Logger::log('debug','end');
        
        // if it's boolean true, we're good
        if ($result['status'] === 'success')
        {
            // ya! we're done!
            Logger::log('debug','end The scheduler ran successfully');
        }
        else
        {
            // do something about the error
            Logger::log('fatal','end The scheduler failed to finish: '.print_r($result,true));

            if (empty($result['error_type']) || $result['error_type'] != 'license')
            {
               // ERROR - Send email to admin
            }
        }

        $this->update_timer(false);

        // calculate and set the time remaining in seconds
        $time_used = $this->elapsed_time / $this->max_time_to_run;
        if ($time_used > $this->time_buffer)
        {
            // if we've crossed the time barrier, zero out the time remaining
            $result['elapsed_time'] = false;
        }
        else
        {
            $result['elapsed_time'] = $this->elapsed_time;
        }
        
        // always return true to signal the scheduler ran successfully
        // we'll handle error checking ourselves
        Logger::log('debug','end final result: '.print_r($result,true));

        return $result;
    }

    protected function start_timer($elapsed_time=false)
    {
        $this->start_time = time();
        $this->last_time = $this->start_time;

        Logger::log('debug',"start_timer timer started at {$this->start_time}");

        // if a $elapsed_time is provided, reset the timer to run for only the allotted time left
        if (empty($elapsed_time))
        {
            Logger::log('debug',"start_timer elapsed time is empty");
            $this->elapsed_time = 0;
        }
        else
        {
            Logger::log('debug',"start_timer elapsed time set to {$elapsed_time}");
            $this->elapsed_time = (int) $elapsed_time;
        }

        return true;
    }
    
    protected function update_timer($check_timer = true)
    {
        Logger::log('debug',"Original Start time {$this->start_time}");
        $current_time = time();
        Logger::log('debug',"Current time {$current_time}");
        $time_diff = $current_time - $this->last_time;
        Logger::log('debug',"Time since last check {$time_diff}");
        $this->elapsed_time += $time_diff;
        
        // set last_time to current_time
        $this->last_time = $current_time;
        
        if ($check_timer === true)
        {
            if ($this->check_timer() === false)
            {
                // we need to end the script
                Logger::log('debug','Ending the process due to time! Will pick up where we left off next time.');
                throw new TimesUpException('Everything ran well, but our time is up. Will continue on the next scheduler run.');
            }
        }
        
        return true;
    }
    
    protected function check_timer()
    {
        // calculate the percentage of time we've used 
        $time_used = $this->elapsed_time / $this->max_time_to_run;
        Logger::log('debug',"Elapsed time {$this->elapsed_time}");
        Logger::log('debug',"Time used {$time_used}");

        // if the percentage of time used is greater thant the allowed time before
        // return false to tell the script to finish up
        if ($time_used > $this->time_buffer)
        {
            return false;
        }
        
        // otherwise, let's keep going and do something else
        return true;
    }

    abstract public static function queue($data);
    abstract public function process();
}