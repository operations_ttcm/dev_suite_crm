<?php







namespace Fanatical\Core\v1a;

Loader::load('Core/Logger');

use Fanatical\Core\v1a\Logger as Logger;
use BeanFactory as BeanFactory;

class fList
{
    // parameters below follow the same signature as SugarBean->get_list(...)
    // order by - what field to order by
    // where - add a where clause to the sql query
    // row_offset - which record to start at
    // $limit - how many to query (defaults to max list count config variable)
    // max - max amount to query
    // show_deleted - also pull deleted target lists?
    // select_fields - fields to be selected during the query
    //
    // returns array(
    //     'row_count' => 5,
    //     'next_offset' => 20,
    //     'previous_offset' => -20,
    //     'current_offset' => 0,
    //     'list' => array(
    //         'asdf-asdf-asdf-asdf-asdf' => array(
    //             'id' => 'asdf-asdf-asdf-asdf-asdf',
    //             'name' => 'name of the list',
    //             'description' => 'description of the list',
    //             'mailchimp_list_name_c' => 'as6d5f7as' // a mailchimp list id
    //             'mailchimp_default_module_c' => 'Contacts', // the default module new MC subscribers are added as
    //         )
    //     )
    // )
    public static function get_sugar_lists($order_by='name',$where='',$row_offset=0,$limit=-1,$max=-1,$show_deleted=0,$single_select=false,$select_fields=array())
    {
        $prospect_list_bean = BeanFactory::getBean('ProspectLists');
        
        $prospect_lists = $prospect_list_bean->get_list($order_by,$where,$row_offset,$limit,$max,$show_deleted,$single_select,$select_fields); //Sugar will use list_max_entries_per_page if set to -1
        
        $data = array();
        $data['row_count'] = $prospect_lists['row_count'];
        $data['next_offset'] = $prospect_lists['next_offset'];
        $data['previous_offset'] = $prospect_lists['previous_offset'];
        $data['current_offset'] = $prospect_lists['current_offset'];

        $data['list'] = array();
        if (!empty($prospect_lists) and is_array($prospect_lists))
        {
            foreach($prospect_lists['list'] as $key => $list) 
            {
                if (empty($list->id))
                {
                    Logger::log('warning','SugarChimp_List::get_sugar_lists list->id is empty for key '.print_r($key,true));
                    continue;
                }

                $data['list'][$list->id] = static::prepare_sugar_list_for_client($list);
            }
        }

        return $data;
    }

    // create a new sugar target list
    // paramters: $data needs to be an array of the fields on the 
    // array(
    //     'name' => 'Master Target List', // required
    //     'list_type' => 'default' , // a required field to create the record, but automatically supplied if not supplied
    //     'mailchimp_list_name_c' => '3ff3g5q5', // mailchimp list id if you want to sync it
    //     'description' => 'This is a target list blah blah...', //
    // )
    public static function create_sugar_list($data)
    {
        if (empty($data) or !is_array($data))
        {
            Logger::log('warning','List::create_sugar_list data is empty or not an array');
            return false;
        }

        if (empty($data['list_type']))
        {
            $data['list_type'] = 'default';   
        }

        $required_fields = array('name','list_type');

        foreach ($required_fields as $field)
        {
            if (empty($data[$field]))
            {
                Logger::log('warning','List::create_sugar_list required field not available in data. field: '.print_r($field,true).' data: '.print_r($data,true));
                return false;
            }
        }

        $list = BeanFactory::newBean('ProspectLists');

        foreach ($data as $field => $value)
        {
            if (property_exists($list,$field))
            {
                $list->$field = $value;
            }
            else
            {
                Logger::log('warning','List::create_sugar_list failed to create list with data: '.print_r($data,true));
            }
        }

        $list->save();

        if (empty($list->id))
        {
            Logger::log('fatal','List::create_sugar_list failed to create list with data: '.print_r($data,true));
            return false;
        }

        return static::prepare_sugar_list_for_client($list);
    }

    public static function prepare_sugar_list_for_client($list,$include_subscriber_count = true)
    {
        if (empty($list))
        {
            return false;
        }

        $data = array(
            'list' => $list,
        );

        if ($include_subscriber_count === true)
        {
            $data['subscriber_count'] = false;
            if (method_exists($list,'get_entry_count'))
            {
                $data['subscriber_count'] = $list->get_entry_count();
            }
        }

        return $data;
    }
}