<?php







namespace Fanatical\Core\v1a;

require_once('custom/include/Fanatical/Loader.php');

use Fanatical\Loader as Fanatical_Loader;

//Core Loader called only whenever Other Core Classes need to load core library
class Loader extends Fanatical_Loader
{
	//THIS MUCH BE CHANGED WHEN A NEW CORE IS CREATED!!!
	protected static $core_version = 'v1a';
}