<?php







namespace Fanatical\Core\v1a;

use BeanFactory as BeanFactory;

Loader::load('Core/Logger');

class Field
{
    // get the available sugarcrm fields for a particular module
    // include_fields (array) - you can supply include filters based on keys in the field defs
    // exclude_fields (array) - you can supply exclude filters based on keys in the field defs
    // ignore_email1 - dont return ->email1 if it exists, defaults to true
    //
    // ex. Get all fields for the Contacts module that is of type varchar, enum or multienum AND source is empty or custom_stable
    //     but exclude those where the vname is empty:
    //      Field::get_module_fields('Contact'
    //          array(
    //              'type' => array('varchar','enum','multienum'),
    //              'source' => array('','custom_table')
    //          ),
    //          array(
    //              'vname' => array(''),
    //          )
    //      )
    public static function get_module_fields($module,$include_fields=array(),$exclude_fields=array(),$ignore_email1=true, $include_label_value=true, $include_options_array=true)
    {
        if (empty($module))
        {
            Logger::log('warning','Field::get_module_fields module is empty');
            return false;
        }
        
        $bean = BeanFactory::newBean($module);
        $fieldsData = array();

        if (empty($bean->field_defs))
        {
            Logger::log('warning','Field::get_module_fields there are no available fields for module '.$module);
            return false;
        }

        foreach($bean->field_defs as $def) 
        {
            if (!empty($include_fields) && is_array($include_fields))
            {
                $keep = true;
                foreach ($include_fields as $key => $values)
                {
                    if ((!isset($def[$key]) || $def[$key] == '') && in_array('',$values))
                    {
                        // empty case, we actually want those that have empty values
                        $keep = true;
                    }
                    else if (!isset($def[$key]))
                    {
                        // the key is not set
                        $keep = false;
                    }
                    else if (isset($def[$key]) && !in_array($def[$key],$values)) 
                    {
                        // key is there, but not in the array, so remove it
                        $keep = false;
                    } 
                    else
                    {
                        // still around so keep it
                        $keep = true;
                    }

                    if ($keep === false) break;
                }
                if ($keep === false) continue;
            }

            // exclude fields based on exclude_fields array
            if (!empty($exclude_fields) && is_array($exclude_fields))
            {
                $keep = true;
                foreach ($exclude_fields as $key => $values)
                {
                    if ((!isset($def[$key]) || $def[$key] == '') && in_array('',$values))
                    {
                        // excluding empties, so if here, do not keep it
                        $keep = false;
                    }
                    else if (!isset($def[$key]))
                    {
                        // key doesn't exist, so we keep it
                        $keep = true;
                    }
                    else if (isset($def[$key]) && in_array($def[$key],$values)) 
                    {
                        // it's in the array, so do not keep it
                        $keep = false;
                    }
                    else
                    {
                        // nothing excluded it, so keep it    
                        $keep = true;
                    }

                    if ($keep === false) break;
                }
                if ($keep === false) continue;
            }

            if ($ignore_email1 === true && $def['name'] == 'email1') continue;
            
            if ($include_options_array === true && ($def['type'] == 'enum' || $def['type'] == 'multienum'))
            {
                $def['options'] = self::get_enum_options($module,$def['name']);
            }

            if ($include_label_value === true)
            {
                $def['label'] = empty($def['vname']) ? $def['name'] : translate($def['vname'], $module);
            }
            
            $fieldsData[$def['name']] = $def;
        }
        
        // order by field name
        ksort($fieldsData);
        
        return $fieldsData;
    }

    public static function get_module_field($module,$name)
    {
        if (empty($module))
        {
            Logger::log('warning','Field::get_module_field module is empty');
            return false;
        }
        
        if (empty($name))
        {
            Logger::log('warning','Field::get_module_field name is empty');
            return false;
        }
        
        $bean = BeanFactory::newBean($module);
        $fieldsData = array();

        if (empty($bean->field_defs[$name]))
        {
            Logger::log('warning','Field::get_module_field there are no field defs available for module '.print_r($module,true).' and field '.print_r($field,true));
            return false;
        }
        
        return $bean->field_defs[$name];
    }

    // get all relationships where the $module passed in is the child
    // ex. if $module='Contacts', the 'Accounts' module would be a parent module 
    //     as Contacts can have exactly one Account parent
    // this will return the array of relationship definitons
    public static function get_parent_relationship_fields($module,$include_fields=array(),$exclude_fields=array())
    {
        if (empty($module))
        {
            Logger::log('warning','Field::get_parent_relationship_fields module is empty');
            return false;
        }

        $includes = array(
            'type' => array('link'),
            'link_type' => array('one'),
        );

        if (!empty($include_fields) && is_array($include_fields))
        {
            $includes = static::merge_field_filter_arrays($includes,$include_fields);
        }

        $excludes = array();

        if (!empty($exclude_fields) && is_array($exclude_fields))
        {
            $excludes = static::merge_field_filter_arrays($excludes,$exclude_fields);
        }

        $fields = self::get_module_fields($module,$includes,$excludes);

        if (empty($fields))
        {
            Logger::log('warning','Field::get_parent_relationship_fields there were no module fields for '.$module);
            return array();
        }

        return $fields;
    }

    // get the relationship definition of a single relationship
    public static function get_relationship_definition($relationship_name)
    {
    	if (empty($relationship_name))
    	{
    		Logger::log('warning','Field::get_relationship_definition - relationship name is empty');
    		return false;
    	}

    	if (empty($GLOBALS['relationships']))
    	{
    		static::get_relationship_definitions();
    	}

    	if (empty($GLOBALS['relationships'][$relationship_name]))
    	{
    		Logger::log('warning','Field::get_relationship_definition - there is no relationship definition for '.$relationship_name);
    		return false;
    	}

    	return $GLOBALS['relationships'][$relationship_name];
    }

    // return ALL relationship definitions loaded in Sugar
    public static function get_relationship_definitions()
    {
    	if (empty($GLOBALS['relationships']))
		{
			$rel = BeanFactory::newBean('Relationships');
			$rel->load_relationship_meta();
		}

		return $GLOBALS['relationships'];
    }

    // return all fields that are on the module that actually live in the database table
    // this will not return fields in the custom table
    public static function get_regular_database_fields($module,$include_fields=array(),$exclude_fields=array())
    {
        if (empty($module))
        {
            Logger::log('warning','Field::get_regular_database_fields module is empty');
            return false;
        }

        $includes = array(
            'source' => array(''),
        );

        if (!empty($include_fields) && is_array($include_fields))
        {
            $includes = static::merge_field_filter_arrays($includes,$include_fields);
        }

        $excludes = array(
            'dbType' => array('id'),
        );

        if (!empty($exclude_fields) && is_array($exclude_fields))
        {
            $excludes = static::merge_field_filter_arrays($excludes,$exclude_fields);
        }

        $fields = self::get_module_fields($module,$includes,$excludes);

        if (empty($fields))
        {
            Logger::log('warning','Field::get_regular_database_fields there were no module fields for '.$module);
            return array();
        }

        return $fields;
    }

    // return all fields that are in the modules custom table
    public static function get_custom_table_fields($module,$include_fields=array(),$exclude_fields=array())
    {
        if (empty($module))
        {
            Logger::log('warning','Field::get_custom_table_fields module is empty');
            return false;
        }

        $includes = array(
            'source' => array('custom_fields'),
        );

        if (!empty($include_fields) && is_array($include_fields))
        {
            $includes = static::merge_field_filter_arrays($includes,$include_fields);
        }

        $excludes = array();

        if (!empty($exclude_fields) && is_array($exclude_fields))
        {
            $excludes = static::merge_field_filter_arrays($excludes,$exclude_fields);
        }

        $fields = self::get_module_fields($module,$includes,$excludes);

        if (empty($fields))
        {
            Logger::log('warning','Field::get_custom_table_fields there were no module fields for '.$module);
            return array();
        }

        return $fields;    
    }

    public static function get_enum_options($module,$field_name)
    {
        if (empty($module) || empty($field_name))
        {
            return false;
        }

        global $app_list_strings;
        $module_bean = BeanFactory::newBean($module);

        if (empty($module_bean->field_defs[$field_name]['options']) or empty($app_list_strings[$module_bean->field_defs[$field_name]['options']]) or !is_array($app_list_strings[$module_bean->field_defs[$field_name]['options']]))
        {
            Logger::log('warning',"Field::get_enum_options could not find dropdown options for {$module} {$field_name}");
            return false;
        }

        return array(
            'name' => $module_bean->field_defs[$field_name]['options'],
            'options' => $app_list_strings[$module_bean->field_defs[$field_name]['options']],
        );
    }

    public static function get_all_available_fields($modules)
    {
        if (empty($modules) or !is_array($modules))
        {
            Logger::log('warning','Field::get_all_available_fields modules is empty or not an array');
            return false;
        }

        $fields = array();
        foreach ($modules as $module)
        {
            $fields[$module] = static::get_available_fields($module);
        }

        if (empty($fields))
        {
            Logger::log('warning','Field::get_all_available_fields modules is empty or not an array');
            return false;
        }

        return $fields;            
    }

    public static function get_available_fields($module,$get_parent_relationship_fields=true,$include_fields=array(),$exclude_fields=array(),$prefix_label='',$prefix_name='',$prefix_key='')
    {
        if (empty($module))
        {
            Logger::log('warning','Field::get_available_fields module is empty');
            return false;
        }

        Logger::log('debug','Field::get_available_fields: module '.print_r($module,true).' get_parent_relationship_fields '.print_r($get_parent_relationship_fields,true).' prefix_name '.print_r($prefix_name,true));

        $fields = array();

        $regular_fields = static::get_regular_database_fields($module,$include_fields,$exclude_fields);
        $regular_fields = static::prepare_fields_array($regular_fields,$prefix_label,$prefix_name,$prefix_key);

        $custom_fields = static::get_custom_table_fields($module,$include_fields,$exclude_fields);
        $custom_fields = static::prepare_fields_array($custom_fields,$prefix_label,$prefix_name,$prefix_key);

        $parent_fields = array();
        if ($get_parent_relationship_fields === true)
        {
            Logger::log('debug','Field::get_available_fields: get parent relationship fields for module '.print_r($module,true));
            $parent_relationship_fields = static::get_parent_relationship_fields($module,$include_fields,$exclude_fields);

            foreach ($parent_relationship_fields as $parent_relationship_field)
            {
                if (empty($parent_relationship_field['relationship']))
                {
                    Logger::log('debug','Field::get_available_fields: no relationship exists for parent_relationship_field '.print_r($parent_relationship_field,true));
                    continue;
                }

                $relationship = Field::get_relationship_definition($parent_relationship_field['relationship']);
                
                if (empty($relationship['lhs_module']))
                {
                    Logger::log('debug','Field::get_available_fields: no lhs_module exists for relationship '.print_r($relationship,true));
                    continue;
                }

                $parent_module = $relationship['lhs_module'];
                $prefix_label = empty($parent_relationship_field['label']) ? '' : $parent_relationship_field['label'];
                $prefix_name = empty($parent_relationship_field['name']) ? '' : $parent_relationship_field['name'].'.';
                $prefix_key = $prefix_name;

                $available_parent_fields = static::get_available_fields($parent_module,false,$include_fields,$exclude_fields,$prefix_label,$prefix_name,$prefix_key);

                if (empty($available_parent_fields) or !is_array($available_parent_fields)) $available_parent_fields = array();

                $parent_fields = array_merge($parent_fields,$available_parent_fields);
            }
        }        

        if (empty($regular_fields) or !is_array($regular_fields)) $regular_fields = array();
        if (empty($custom_fields) or !is_array($custom_fields)) $custom_fields = array();
        if (empty($parent_fields) or !is_array($parent_fields)) $parent_fields = array();

        $fields = array_merge($regular_fields,$custom_fields,$parent_fields);

        return $fields;
    }

    // do any processing on the fields gathered 
    // right now it just adds prefix to field name if a prefix is provided
    public static function prepare_fields_array($fields,$prefix_label='',$prefix_name='',$prefix_key='')
    {
        if (empty($fields) or !is_array($fields))
        {
            Logger::log('debug','Field prepare_fields_array: fields is empty or not an array');
            return $fields;
        }

        Logger::log('debug','Field prepare_fields_array: fields: '.print_r($fields,true));
        Logger::log('debug','Field prepare_fields_array: prefix_label: '.print_r($prefix_label,true));
        Logger::log('debug','Field prepare_fields_array: prefix_name: '.print_r($prefix_name,true));
        Logger::log('debug','Field prepare_fields_array: prefix_key: '.print_r($prefix_key,true));

        foreach ($fields as $key => $field)
        {
            if (!empty($prefix_name))
            {
                $fields[$key]['name'] = $prefix_name . $field['name'];
            }
            else
            {
                Logger::log('debug','Field prepare_fields_array: prefix_name is empty');
            }

            if (!empty($prefix_label))
            {
                $fields[$key]['label'] = $prefix_label . ' > ' . $field['label'];
            }
            else
            {
                Logger::log('debug','Field prepare_fields_array: prefix_label is empty');
            }

            if (!empty($prefix_key))
            {
                $new_key = $prefix_key . $key;
                $fields[$new_key] = $fields[$key];
                unset($fields[$key]);
            }
            else
            {
                Logger::log('debug','Field prepare_fields_array: prefix_key is empty');
            }
        }

        return $fields;
    }

    // check if field is a related field
    public static function is_related_field($field_name)
    {
        if (empty($field_name))
        {
            Logger::log('warning','Field is_related_field: field_name is empty');
            return false;
        }

        // if the name contains periods, it is linked to another module/table
        // strpos will return FALSE if not found
        $pos = strpos($field_name,'.');

        if ($pos === false)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    // take a related field name
    // return the important parts
    public static function get_related_field_parts($field_name)
    {
        if (empty($field_name))
        {
            Logger::log('warning','Field get_related_field_parts: field_name is empty');
            return false;
        }

        $field_parts = explode('.', $field_name);

        if (count($field_parts)<1)
        {
            // you got problems, you didn't pass a related field
            Logger::log('warning','Field get_related_field_parts: field_name does not contain periods, it probably is not a related field');
            return false;
        }

        $parts = array(
            'link_field_name' => $field_parts[0],
            'parent_field_name' => $field_parts[1],
        );

        return $parts;
    }

    // take two field filter arrays and merge them together
    // the second param array will be merged into the first param array
    // the merged array will be returned
    // given the follow arrays
    // $original = array(
    //     'type' => array('multienum','enum'),
    //     'source' => array('db'),
    // )
    // $additions = array(
    //     'type' => array('varchar'),
    //     'dbType' => array('id'),
    // )
    // they would return the following:
    // return array(
    //     'type' => array('multienum','enum','varchar'),
    //     'source' => array('db'),
    //     'dbType' => array('id'),
    // )
    public static function merge_field_filter_arrays($original,$additions)
    {
        if (empty($original) && !empty($additions) && is_array($additions))
        {
            Logger::log('debug','Field::merge_field_filter_arrays original is empty, take additions array');
            return $additions;
        }

        if (empty($additions) && !empty($original) && is_array($original))
        {
            Logger::log('debug','Field::merge_field_filter_arrays additions is empty, take origins array');
            return $original;
        }

        if (!is_array($original) || !is_array($additions))
        {
            Logger::log('warning','Field::merge_field_filter_arrays original and additions are not arrays, cannot continue');
            return false;
        }

        foreach ($additions as $key => $values)
        {
            // values must if an array
            if (!is_array($values)) continue;

            if (array_key_exists($key, $original))
            {
                // make sure original is an array
                if (!is_array($original[$key])) continue;

                // if the key exists, merge the arrays
                $new_values = array_merge($original[$key],$values);

                $original[$key] = array_unique($new_values);
            }
            else
            {
                // if the key does not exist, add the key and values to original
                $original[$key] = $values;
            }
        }

        return $original;
    }
}