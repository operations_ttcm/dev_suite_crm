<?php

namespace Fanatical;







class Loader
{
	protected static $core_version = 'v0a';
	
	// this does not need to include the custom/ folder prefix
	// it will be prepended later to core_path to get around
	// Sugar OD restriction on the use of file_exists
	public static $core_path = 'include/Fanatical/';
	
	// If any Core items are loaded, that core_class needs to also be loaded.
	public static $specific_version_loader = '';
	
	// this method will take our autoloader paths and actually require_once the
	// file that needs to be loaded, it's based on the core_version that should
	// be provided in the package class that extends Driver_Loader
	// ex. load('Core/Logger') would require_once Core/{version}/Logger/Logger.php
	// ex. load('Core/Logger/Whatever.php'); would require_once Core/{version}/Logger/Whatever.php
	// ex. load('Core/Logger/Special'); would require_once Core/{version}/Logger/Special/Special.php
	// ex. load('Email/Logger/Special/Whatever.php'); would require_once Email/{version}/Logger/Special/Whatever.php
	public static function load($path, $ext='php')
	{
		if (empty($path))
		{
			return false;
		}

		if (empty(static::$core_version))
		{
			$GLOBALS['warning']->debug("Fanatical_Loader failed: core version empty.");
			return false;
		}

		// we use DIRECTORY_SEPARATOR everywhere below, but in our code we always use forward slashes
		// if the DIRECTORY_SEPARATOR is not a forward slash, the imports will not work properly
		// so breakdown the path our code provides on the forward slash and use DIRECTORY_SEPARATOR everywhere else
		// ex. in windows the paths will look something like custom/include/Fanatical/Core\Setting\Setting.php
		//		because their DIRECTORY_SEPARATOR is a '\', windows accepts both '\' and '/' so we're good here
		$path_tokens = explode('/',$path);
		$absolute_path = static::$core_path;
		$number_tokens = count($path_tokens);

		// put the path back together
		// in the 2nd spot, add core_version
		for ($i=0; $i<$number_tokens; $i++)
		{
			if ($i == $number_tokens - 1)
			{
				// handles the last token separately
				$absolute_path .= self::handle_last_token($path_tokens[$i],$ext);
			}
			else
			{
				if ($i == 0)
				{
					$absolute_path .= $path_tokens[$i] . DIRECTORY_SEPARATOR . static::$core_version . DIRECTORY_SEPARATOR;
					// specify the core_version loader to be required
					// Should always be in the core_version directory
					// ex: Fanatical/Core/1.0/Loader.php
					$specific_version_loader = $absolute_path . 'Loader.php';
				}
				else
				{
					$absolute_path .= $path_tokens[$i] . DIRECTORY_SEPARATOR;
				}
			}
		}

		
		// since we can't do a file_exists check (it's a black-labeled function for OD)
		// we use this function provided by sugar which checks if a non-custom path 
		// exists in the custom folder, since our include folder happens to live in the
		// custom folder, we can use this
		$custom_path = get_custom_file_if_exists($absolute_path);		
		$custom_specific_version_loader = get_custom_file_if_exists($specific_version_loader);

		// if the file exists in the custom folder, custom_path will be prepended with custom/
		// if it is not, it will return what was provided, so if custom_path==absolute_path
		// we did not find the file, so we do NOT want to try to recover it
		if ($custom_specific_version_loader == $specific_version_loader || $custom_path == $absolute_path)
		{
			$GLOBALS['log']->fatal('Core Module or Core Loader not found.');
			return false;
		}
		
		// otherwise, require the file and core loader and keep on with it
		require_once($custom_specific_version_loader);	
		require_once($custom_path);

		return true;
	}

	// take the last token of the path and determine if it is a file (has .php ext)
	// or if it is not, if file, return the file name, it not file, return as Folder/Folder.php
	// ex. Logger would return Logger/Logger.php
	// ex. Logger.php would return Logger.php
	public static function handle_last_token($token,$ext = 'php')
	{
		if (empty($token))
		{
			return false;
		}

		if (strpos($token,'.') !== false)
		{
			// found a '.' in the token, assuming it's a file name
			return $token;
		}
		else
		{
			return $token . DIRECTORY_SEPARATOR . $token . '.' . $ext;
		}
	}
}