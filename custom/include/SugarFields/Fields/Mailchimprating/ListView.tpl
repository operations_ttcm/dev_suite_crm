



{capture name=getMailchimpRating assign=rating}{sugar_fetch object=$parentFieldArray key=$col}{/capture}

{if $rating == 5}
	<img src="modules/SugarChimp/includes/assets/img/rating/star-5.png" style="height:20px">
{elseif $rating == 4}
	<img src="modules/SugarChimp/includes/assets/img/rating/star-4.png" style="height:20px">
{elseif $rating == 3}
	<img src="modules/SugarChimp/includes/assets/img/rating/star-3.png" style="height:20px">
{elseif $rating == 2}
	<img src="modules/SugarChimp/includes/assets/img/rating/star-2.png" style="height:20px">
{elseif $rating == 1}
	<img src="modules/SugarChimp/includes/assets/img/rating/star-1.png" style="height:20px">
{else}
	<img src="modules/SugarChimp/includes/assets/img/rating/star-0.png" style="height:20px">
{/if}