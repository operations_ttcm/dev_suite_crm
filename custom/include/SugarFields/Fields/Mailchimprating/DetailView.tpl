



{assign var="value" value={{sugarvar key='value' string=true}} }
<span class="sugar_field" id="{{sugarvar key='name'}}">
	{if $value == 5}
		<img src="modules/SugarChimp/includes/assets/img/rating/star-5.png" style="height:20px">
	{elseif $value == 4}
		<img src="modules/SugarChimp/includes/assets/img/rating/star-4.png" style="height:20px">
	{elseif $value == 3}
		<img src="modules/SugarChimp/includes/assets/img/rating/star-3.png" style="height:20px">
	{elseif $value == 2}
		<img src="modules/SugarChimp/includes/assets/img/rating/star-2.png" style="height:20px">
	{elseif $value == 1}
		<img src="modules/SugarChimp/includes/assets/img/rating/star-1.png" style="height:20px">
	{else}
		<img src="modules/SugarChimp/includes/assets/img/rating/star-0.png" style="height:20px">
	{/if}
</span>