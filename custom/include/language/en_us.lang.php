<?php
$GLOBALS['app_list_strings']['invamount_list']=array (
  '' => '',
);
$GLOBALS['app_list_strings']['clientdepositspermonth_list']=array (
  '' => '',
  'onetofive' => '1-5',
  'sixtoten' => '6-10',
  'Morethanten' => '10+',
);
$GLOBALS['app_list_strings']['customersupport_list']=array (
  '' => '',
  'supportar' => 'Support AR',
  'supportcn' => 'Support CN',
  'supportde' => 'Support DE',
  'supportes' => 'Support ES',
  'supportfr' => 'Support FR',
  'supportit' => 'Support IT',
  'supportjp' => 'Support JP',
  'supportpl' => 'Support PL',
  'supportuk' => 'Support UK',
);
$GLOBALS['app_list_strings']['poi_list']=array (
  '' => '',
  'approved' => 'Approved',
  'expired' => 'Expired',
  'missing' => 'Missing',
  'new' => 'New',
  'pending' => 'Pending',
  'rejected' => 'Rejected',
);
$GLOBALS['app_list_strings']['riskoftranscactions_list']=array (
  '' => '',
  'no' => 'No',
  'yes' => 'Yes',
);
$GLOBALS['app_list_strings']['volumetradedpermonth_list']=array (
  '' => '',
  'zerofifty' => '0+50',
  'onehundreddonetotwohunderd' => '101-200',
  'morethantwohundred' => '200+',
  'fiftyonetoonehundred' => '51-100',
);

$GLOBALS['app_list_strings']['bankruptcy_list']=array (
  '' => '',
  'no' => 'No',
  'yes' => 'Yes',
);
$GLOBALS['app_list_strings']['pexposed_list']=array (
  '' => '',
  'no' => 'No',
  'yes' => 'Yes',
);
$GLOBALS['app_list_strings']['reason_list']=array (
  '' => '',
  'speculation' => 'Speculation',
  'hedging' => 'Hedging',
);
$GLOBALS['app_list_strings']['clientspermonth_list']=array (
  '' => '',
  'onetofive' => '1-5',
  'sixtoten' => '6-10',
  'morethanten' => '10+',
);
$GLOBALS['app_list_strings']['por_list']=array (
  '' => '',
  'approved' => 'Approved',
  'expired' => 'Expired',
  'missing' => 'Missing',
  'new' => 'New',
  'pending' => 'Pending',
  'rejected' => 'Rejected',
);
$GLOBALS['app_list_strings']['creditcard_c_list']=array (
  '' => '',
  'missing' => 'Missing',
  'pending' => 'Pending',
  'approved' => 'Approved',
  'new' => 'New',
  'expired' => 'Expired',
);
$GLOBALS['app_list_strings']['creditcard_list']=array (
  '' => '',
  'missing' => 'Missing',
  'pending' => 'Pending',
  'approved' => 'Approved',
  'new' => 'New',
  'expired' => 'Expired',
);
$GLOBALS['app_list_strings']['contactlanguage__list']=array (
  '' => '',
  'ar' => 'Arabic',
  'en' => 'English',
  'fr' => 'French',
  'de' => 'German',
  'gr' => 'Greek',
  'pl' => 'Polish',
  'es' => 'Spanish',
  'jp' => 'Japanese',
  'cn' => 'Chinese',
  'hu' => 'Hungarian',
);

$GLOBALS['app_list_strings']['securestatus_list']=array (
  '' => '',
  'New' => 'New',
  'NO_DOCUMENTS' => 'No Documents',
  'PendingDocs' => 'Pending Documents',
  'verified' => 'Verified',
  'PendingVerification' => 'Pending Verification',
);
$GLOBALS['app_list_strings']['accountstatus_list']=array (
  '' => '',
  'New' => 'New',
  'NO_DOCUMENTS' => 'No Documents',
  'PendingVerification' => 'Pending Verification',
  'PendingDocs' => 'Pending Documents',
  'verified' => 'Verified',
);
$GLOBALS['app_list_strings']['account_type_c_list']=array (
  'forex_account' => 'Forex Account',
  'stpecn' => 'STP-ECN',
  'islamic_account' => 'Islamic Account',
  'forex_islamic' => 'Forex Islamic',
  'allinclusive' => 'All Inclusive',
  'InstantRebate' => 'Instant Rebate',
);

$GLOBALS['app_list_strings']['typeofpartner_list']=array (
  '' => '',
  'affiliate' => 'Affiliate',
  'ib' => 'IB',
  'regional' => 'Regional',
  'whitelabel' => 'White Label',
  'signalproviders' => 'Signal Providers',
);
$GLOBALS['app_list_strings']['sms_module_list']['']='';
$GLOBALS['app_list_strings']['sms_module_list']['Accounts']='Accounts';
$GLOBALS['app_list_strings']['sms_module_list']['Contacts']='Contacts';
$GLOBALS['app_list_strings']['sms_module_list']['Leads']='Leads';
$GLOBALS['app_list_strings']['emailTemplates_type_list']['whatsapp']='WhatsApp';
$GLOBALS['app_list_strings']['call_status_dom']['Sent']='Sent';
$GLOBALS['app_list_strings']['call_status_dom']['Received']='Received';
$GLOBALS['app_list_strings']['call_status_dom']['Failure']='Failure';
$GLOBALS['app_list_strings']['emailTemplates_type_list_no_workflow']['whatsapp']='WhatsApp';
$app_list_strings['mailchimp_lists_list']['045248ebb1']='All VU Japanese IBs 15/06/2018';
$app_list_strings['mailchimp_lists_list']['1d74cca4bc']='Suite CRM Leads for Testing';
$app_list_strings['mailchimp_lists_list']['239c4726cd']='TTCM - VU JAPANESE (22/10/2018)';
$app_list_strings['mailchimp_lists_list']['27e0d9e25e']='TTCM-VU Clients (31/07/2018) |';
$app_list_strings['mailchimp_lists_list']['63d7d767dc']='ND Company Leads Vanuatu (MX)';
$app_list_strings['mailchimp_lists_list']['8ef1dc11c3']='All VU IBs (Non JP&ES)  (01/06/2018)';
$app_list_strings['mailchimp_lists_list']['91db8d9617']='TTCM -VU NON JAPANESE (22/10/2018)';
$app_list_strings['mailchimp_lists_list']['c11b37e3f5']='IT Test lists (includes gmail)';
$app_list_strings['mailchimp_lists_list']['e06a0a7ab8']='Uncomplete Registration JP - TT02';
$app_list_strings['mailchimp_lists_list']['f346f120e3']='TT- CY  (22/10/2018)';
$app_list_strings['mailchimp_lists_list']['3706f46eff']='Uncomplete Registration ES - TT02';
$app_list_strings['mailchimp_lists_list']['aabe740ec7']='Uncomplete Registration ES - TT02';
$app_list_strings['mailchimp_lists_list']['529960fb08']='Uncomplete Registration ES - TT02';
$app_list_strings['mailchimp_lists_list']['224604db22']='New Registration CySec';
$app_list_strings['mailchimp_lists_list']['df346b4311']='Master SugarChimp List';
$app_list_strings['mailchimp_lists_list']['67fa4a3a51']='Uncomplete Registrations Cysec';
$app_list_strings['mailchimp_lists_list']['81bd12c82d']='Uncomplete Registrations Cysec - EN';
$app_list_strings['mailchimp_lists_list']['c9e3db7a9e']='Uncomplete Registrations Cysec - English';
$app_list_strings['mailchimp_lists_list']['cb4c296f5c']='Uncomplete Registrations - ES 02';
$app_list_strings['mailchimp_lists_list']['a3617d9197']='Uncomplete Registrations ES-02';
$app_list_strings['mailchimp_lists_list']['a619782a58']='Uncomplete Registrations EN-02';
$app_list_strings['mailchimp_lists_list']['1dd138d8d8']='Uncomplete Registrations ES-02';
$app_list_strings['mailchimp_lists_list']['f88b2aed8c']='test list';
$app_list_strings['mailchimp_lists_list']['a58bbf5835']='IT TEST';
$app_list_strings['mailchimp_lists_list']['9e0b336703']='Uncomplete Registrations 02 - ES';
$app_list_strings['mailchimp_lists_list']['f87895353a']='MC-TEST01';
$app_list_strings['mailchimp_lists_list']['0f33dfa251']='mc test 02';
$app_list_strings['mailchimp_lists_list']['b5167d1541']='Uncomplete Registrations 02 - ES';
$app_list_strings['mailchimp_lists_list']['12c3f8b772']='Uncomplete Registrations 02 - EN';
$GLOBALS['app_list_strings']['progressstatus_list']=array (
  '' => '',
  'Funded' => 'Funded',
  'new' => 'New',
  'NoPhone' => 'No Phone',
  'InProgress' => 'In Progress',
  'NotInterested' => 'Not Interested',
  'Unrechable' => 'Unrechable',
  'NoLanguage' => 'No Language',
  'Timewaster' => 'Timewaster',
  'NO_ANSWER' => 'No Answer',
  'converted' => 'Converted',
);
$GLOBALS['app_list_strings']['brand_0']=array (
  'TT' => 'TT',
  'TT_VU' => 'TT-VU',
  'TFX' => 'TFX',
  'TFX_VU' => 'TFX-VU',
);
$GLOBALS['app_list_strings']['brand_1']=array (
  'TT' => 'TT',
  'TT_VU' => 'TT-VU',
  'TFX' => 'TFX',
  'TFX_VU' => 'TFX-VU',
);
$GLOBALS['app_list_strings']['language_list']=array (
  'en' => 'English',
  'es' => 'Spanish',
  'ja' => 'Japanese',
  'it' => 'Italian',
  'fr' => 'French',
  'cn' => 'Chinese',
);
$GLOBALS['app_list_strings']['invest_list']=array (
  '' => '',
  'first' => '500$',
  'second' => '500$-2500$',
  'third' => '2500$-5000$',
  'fourth' => '5k-10k',
  'fifth' => '10k-25k',
  'sixth' => '25k-50k',
  'seventh' => '50k-100k',
  'eighth' => 'Above 100k',
);
$GLOBALS['app_list_strings']['oftentrade_list']=array (
  '' => '',
  'none' => 'Never',
  'daily' => 'Daily',
  'weekly' => 'Weekly',
  'monthly' => 'Monthly',
);
$GLOBALS['app_list_strings']['fperyear_list']=array (
  '' => '',
  'one_two_years' => '1-2',
  'ten_plus' => '10+',
  'three_five_years' => '3-5',
  'five_ten_years' => '5-10',
  'none' => 'Never',
);
$GLOBALS['app_list_strings']['indicies_list']=array (
  '' => '',
  'one_two_years' => '1-2',
  'ten_plus' => '10+',
  'three_five_years' => '3-5',
  'five_ten_years' => '5-10',
  'none' => 'Never',
);
$GLOBALS['app_list_strings']['equities_list']=array (
  '' => '',
  'none' => 'Never',
  'one_two_years' => '1-2',
  'ten_plus' => '10+',
  'three_five_years' => '3-5',
  'five_ten_years' => '5-10',
);
$GLOBALS['app_list_strings']['commodoties_list']=array (
  '' => '',
  'none' => 'Never',
  'one_two_year' => '1-2',
  'ten_plus' => '10+',
  'three_five_years' => '3-5',
  'five_ten_years' => '5-10',
);
$GLOBALS['app_list_strings']['brand_c_list']=array (
  '' => '',
  'traderstrusteu' => 'TT-EU',
  'traderstrustvu' => 'TT-BM',
  'tradingforex' => 'TFX-EU',
  'tradingforexvu' => 'TFX-VU',
  'mt4account' => 'MT4 PLATFORM',
  'traderstrust' => 'TT-CY',
);

$GLOBALS['app_list_strings']['team_list']=array (
  '' => '',
  'latam' => 'Latam',
  'japan' => 'Japan',
  'inernational' => 'International',
  'europe' => 'Europe',
);

$GLOBALS['app_list_strings']['level_c_list']=array (
  'Level_L1' => 'CEO / Director',
  'Level_L2' => 'Manager',
  'Level_L3' => 'Team Leader',
  'Level_L4' => 'Agent',
);
$GLOBALS['app_list_strings']['internalaffiliate_list']=array (
  '' => '',
  'fraser' => 'Fraser N.',
  'giset' => 'Giset G.',
  'yoko' => 'Yoko M.',
  'james' => 'James C.',
  'nordine' => 'Nordine M.',
  'nicolas' => 'Nicolas L. ',
);
$GLOBALS['app_list_strings']['brand_list']=array (
  '' => '',
  'traderstrusteu' => 'TT-EU',
  'traderstrustvu' => 'TT-BM',
  'tradingforexvu' => 'TFX-VU',
  'tradingforex' => 'TFX-CY',
  'mt4platform' => 'MT4 PLATFORM',
  'traderstrust' => 'TT-CY',
  'axin.mx' => 'AXIN.MX',
  'accumforex' => 'ACCUMFOREX',
);

$GLOBALS['app_list_strings']['teammanager_list']=array (
  'nordine' => 'Nordine M',
  'yoko' => 'Yoko M. ',
  'james' => 'James C.',
  '' => '',
);

$GLOBALS['app_list_strings']['verifiedlevel_list']=array (
  'none' => 'None',
  'POIonly' => 'POI Only',
  'final' => 'Final',
);