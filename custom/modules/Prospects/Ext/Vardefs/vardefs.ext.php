<?php 
 //WARNING: The contents of this file are auto-generated








$dictionary['Prospect']['fields']['sugarchimpactivity'] = array(
    'name' => 'sugarchimpactivity',
    'type' => 'link',
    'relationship' => 'sugarchimpactivity_prospects',
    'module' => 'SugarChimpActivity',
    'bean_name' => 'SugarChimpActivity',
    'source' => 'non-db',
    'vname' => 'LBL_SUGARCHIMPACTIVITY',
);

$dictionary['Prospect']['fields']['mailchimp_rating_c'] = array(
	'name' => 'mailchimp_rating_c',
	'vname' => 'LBL_MAILCHIMP_RATING',
	'type' => 'mailchimprating',
	'dbType' => 'int',
	'len' => '3',
	'comment' => 'MailChimp Rating',
	'required' => false,
	'studio' => true,
	'reportable' => true,
	'default' => 2,
	'readonly' => true,
);

 // created: 2018-03-30 09:33:37
$dictionary['Prospect']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2018-03-30 09:33:37
$dictionary['Prospect']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2018-03-30 09:33:37
$dictionary['Prospect']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2018-03-30 09:33:37
$dictionary['Prospect']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 
?>