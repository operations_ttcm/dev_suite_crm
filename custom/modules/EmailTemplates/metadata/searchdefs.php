<?php
$searchdefs ['EmailTemplates'] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
      ),
      'type' => 
      array (
        'name' => 'type',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'EmailTemplate::getTypeOptionsForSearch',
        ),
        'default' => true,
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
      ),
      'type' => 
      array (
        'name' => 'type',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'EmailTemplate::getTypeOptionsForSearch',
        ),
        'default' => true,
      ),
      'subject' => 
      array (
        'name' => 'subject',
        'default' => true,
      ),
      'description' => 
      array (
        'name' => 'description',
        'default' => true,
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'type' => 'enum',
        'label' => 'LBL_ASSIGNED_TO',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '2',
    'maxColumnsBasic' => '2',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
