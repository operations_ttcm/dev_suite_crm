<?php
// Do not store anything in this file that is not part of the array or the hook version.  This file will	
// be automatically rebuilt in the future. 
 $hook_version = 1; 
$hook_array = Array(); 
// position, file, function 
$hook_array['before_save'] = Array(); 
$hook_array['before_save'][] = Array(77, 'updateGeocodeInfo', 'modules/Accounts/AccountsJjwg_MapsLogicHook.php','AccountsJjwg_MapsLogicHook', 'updateGeocodeInfo'); 
$hook_array['before_save'][] = Array(2, '', 'custom/modules/ExchangeSynchronization/SynchedFlagHook.php','SynchedFlagHook', 'synchFlag'); 
$hook_array['before_save'][] = Array(50, 'SugarChimp', 'modules/SugarChimp/includes/classes/SugarChimp/LogicHook.php','SugarChimp_LogicHook', 'PersonUpdateBeforeSave'); 
$hook_array['after_save'] = Array(); 
$hook_array['after_save'][] = Array(77, 'updateRelatedMeetingsGeocodeInfo', 'modules/Accounts/AccountsJjwg_MapsLogicHook.php','AccountsJjwg_MapsLogicHook', 'updateRelatedMeetingsGeocodeInfo'); 
$hook_array['after_save'][] = Array(78, 'updateRelatedProjectGeocodeInfo', 'modules/Accounts/AccountsJjwg_MapsLogicHook.php','AccountsJjwg_MapsLogicHook', 'updateRelatedProjectGeocodeInfo'); 
$hook_array['after_save'][] = Array(79, 'updateRelatedOpportunitiesGeocodeInfo', 'modules/Accounts/AccountsJjwg_MapsLogicHook.php','AccountsJjwg_MapsLogicHook', 'updateRelatedOpportunitiesGeocodeInfo'); 
$hook_array['after_save'][] = Array(80, 'updateRelatedCasesGeocodeInfo', 'modules/Accounts/AccountsJjwg_MapsLogicHook.php','AccountsJjwg_MapsLogicHook', 'updateRelatedCasesGeocodeInfo'); 
$hook_array['after_save'][] = Array(5, '', 'custom/modules/ExchangeSynchronization/ExchangeSyncHelperTable.php','ExchangeSyncHelperTable', 'updateLastSyncTime'); 
$hook_array['after_save'][] = Array(50, 'SugarChimp', 'modules/SugarChimp/includes/classes/SugarChimp/LogicHook.php','SugarChimp_LogicHook', 'PersonUpdateAfterSave'); 
$hook_array['after_relationship_add'] = Array(); 
$hook_array['after_relationship_add'][] = Array(77, 'addRelationship', 'modules/Accounts/AccountsJjwg_MapsLogicHook.php','AccountsJjwg_MapsLogicHook', 'addRelationship'); 
$hook_array['after_relationship_delete'] = Array(); 
$hook_array['after_relationship_delete'][] = Array(77, 'deleteRelationship', 'modules/Accounts/AccountsJjwg_MapsLogicHook.php','AccountsJjwg_MapsLogicHook', 'deleteRelationship'); 
$hook_array['after_delete'] = Array(); 
$hook_array['after_delete'][] = Array(1, '', 'custom/modules/ExchangeSynchronization/ExchangeSyncHelperTable.php','ExchangeSyncHelperTable', 'deleteItem'); 
$hook_array['after_delete'][] = Array(50, 'SugarChimp', 'modules/SugarChimp/includes/classes/SugarChimp/LogicHook.php','SugarChimp_LogicHook', 'PersonAfterDelete'); 
$hook_array['before_delete'] = Array(); 
$hook_array['before_delete'][] = Array(50, 'SugarChimp', 'modules/SugarChimp/includes/classes/SugarChimp/LogicHook.php','SugarChimp_LogicHook', 'PersonBeforeDelete'); 



?>