<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2019-01-14 10:38:50
$dictionary['Account']['fields']['fundedworkflow6_c']['inline_edit']='1';
$dictionary['Account']['fields']['fundedworkflow6_c']['labelValue']='Workflow 2.6';

 

 // created: 2018-04-17 06:29:40
$dictionary['Account']['fields']['accountrestrections_c']['inline_edit']='1';
$dictionary['Account']['fields']['accountrestrections_c']['labelValue']='Account Restrictions';

 

 // created: 2019-09-02 08:27:29
$dictionary['Account']['fields']['teammanagernew_c']['inline_edit']='1';
$dictionary['Account']['fields']['teammanagernew_c']['labelValue']='Team Manager';

 

 // created: 2018-04-17 06:31:41
$dictionary['Account']['fields']['poiissueddate_c']['inline_edit']='1';
$dictionary['Account']['fields']['poiissueddate_c']['labelValue']='ID Issued';

 

 // created: 2018-07-26 12:36:30
$dictionary['Account']['fields']['riskassessement_c']['inline_edit']='1';
$dictionary['Account']['fields']['riskassessement_c']['labelValue']='Risk Assessment';

 

 // created: 2018-04-17 06:34:23
$dictionary['Account']['fields']['subibs_c']['inline_edit']='1';
$dictionary['Account']['fields']['subibs_c']['labelValue']='Sub-IBs';

 

 // created: 2018-07-11 10:29:26
$dictionary['Account']['fields']['date_entered']['inline_edit']=true;
$dictionary['Account']['fields']['date_entered']['comments']='Date record created';
$dictionary['Account']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Account']['fields']['date_entered']['massupdate']=0;
$dictionary['Account']['fields']['date_entered']['audited']=true;

 

 // created: 2018-04-17 06:35:08
$dictionary['Account']['fields']['zulutrade_c']['inline_edit']='1';
$dictionary['Account']['fields']['zulutrade_c']['labelValue']='Zulu Trade';

 

 // created: 2018-07-30 13:44:08
$dictionary['Account']['fields']['clientdepositspermonth_c']['inline_edit']='1';
$dictionary['Account']['fields']['clientdepositspermonth_c']['labelValue']='Clients Deposits Per Month';

 

 // created: 2018-12-03 07:25:35
$dictionary['Account']['fields']['unsubscribe_c']['inline_edit']='1';
$dictionary['Account']['fields']['unsubscribe_c']['labelValue']='Unsubscribe';

 

 // created: 2019-05-29 13:10:39
$dictionary['Account']['fields']['billing_address_country']['audited']=true;
$dictionary['Account']['fields']['billing_address_country']['inline_edit']=true;
$dictionary['Account']['fields']['billing_address_country']['comments']='The country used for the billing address';
$dictionary['Account']['fields']['billing_address_country']['merge_filter']='disabled';

 

 // created: 2018-07-26 11:08:32
$dictionary['Account']['fields']['fundeddate_c']['inline_edit']='1';
$dictionary['Account']['fields']['fundeddate_c']['labelValue']='Last Deposit Date:';

 

 // created: 2018-07-30 13:40:54
$dictionary['Account']['fields']['equities_c']['inline_edit']='1';
$dictionary['Account']['fields']['equities_c']['labelValue']='Equities per Year';

 

 // created: 2019-07-09 10:48:17
$dictionary['Account']['fields']['teammanager_c']['inline_edit']='1';
$dictionary['Account']['fields']['teammanager_c']['labelValue']='Team Manager';

 

// created: 2018-04-13 10:45:21
$dictionary["Account"]["fields"]["accounts_fin12_financial_information_1"] = array (
  'name' => 'accounts_fin12_financial_information_1',
  'type' => 'link',
  'relationship' => 'accounts_fin12_financial_information_1',
  'source' => 'non-db',
  'module' => 'fin12_financial_information',
  'bean_name' => 'fin12_financial_information',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_1_FROM_FIN12_FINANCIAL_INFORMATION_TITLE',
);


 // created: 2019-08-27 08:48:02
$dictionary['Account']['fields']['utm_source_c']['inline_edit']='1';
$dictionary['Account']['fields']['utm_source_c']['labelValue']='Utm source';

 

 // created: 2019-10-04 10:39:03
$dictionary['Account']['fields']['brand_c']['inline_edit']='1';
$dictionary['Account']['fields']['brand_c']['labelValue']='Brand';

 

 // created: 2018-12-19 15:54:55
$dictionary['Account']['fields']['campaignname1_c']['inline_edit']='1';
$dictionary['Account']['fields']['campaignname1_c']['labelValue']='Campaign Name(1)';

 

 // created: 2019-07-03 10:19:57
$dictionary['Account']['fields']['internalaffiliate_c']['inline_edit']='1';
$dictionary['Account']['fields']['internalaffiliate_c']['labelValue']='Internal Affiliate';

 

 // created: 2019-05-24 06:45:46
$dictionary['Account']['fields']['businesscountry_c']['inline_edit']='1';
$dictionary['Account']['fields']['businesscountry_c']['labelValue']='Bussiness Country';

 

 // created: 2019-10-23 08:51:33
$dictionary['Account']['fields']['verifiedlevel_c']['inline_edit']='1';
$dictionary['Account']['fields']['verifiedlevel_c']['labelValue']='Verified Level';

 

 // created: 2019-01-14 10:35:14
$dictionary['Account']['fields']['fundedworkflow1_c']['inline_edit']='1';
$dictionary['Account']['fields']['fundedworkflow1_c']['labelValue']='Workflow 2.1';

 

 // created: 2018-04-17 06:39:56
$dictionary['Account']['fields']['idcomment_c']['inline_edit']='1';
$dictionary['Account']['fields']['idcomment_c']['labelValue']='ID Comment';

 

 // created: 2018-04-17 06:40:29
$dictionary['Account']['fields']['masterib_c']['inline_edit']='1';
$dictionary['Account']['fields']['masterib_c']['labelValue']='Master IB';

 

 // created: 2018-04-17 06:43:25
$dictionary['Account']['fields']['partnerwebsite1_c']['inline_edit']='1';
$dictionary['Account']['fields']['partnerwebsite1_c']['labelValue']='Site';

 

 // created: 2018-04-17 06:43:59
$dictionary['Account']['fields']['localdepositprovider_c']['inline_edit']='1';
$dictionary['Account']['fields']['localdepositprovider_c']['labelValue']='Local Deposit Provider';

 

 // created: 2018-04-17 06:56:41
$dictionary['Account']['fields']['newsletters_c']['inline_edit']='1';
$dictionary['Account']['fields']['newsletters_c']['labelValue']='Receive Newsletters';

 

 // created: 2019-02-28 14:19:13
$dictionary['Account']['fields']['porstatusreason_c']['inline_edit']='1';
$dictionary['Account']['fields']['porstatusreason_c']['labelValue']='POR Status Reason';

 

 // created: 2019-02-28 14:20:24
$dictionary['Account']['fields']['creditcardstatusreason_c']['inline_edit']='1';
$dictionary['Account']['fields']['creditcardstatusreason_c']['labelValue']='Credit Card Status Reason';

 

 // created: 2018-07-30 13:46:15
$dictionary['Account']['fields']['customersupport_c']['inline_edit']='1';
$dictionary['Account']['fields']['customersupport_c']['labelValue']='Customer Support';

 

 // created: 2018-04-17 08:19:47
$dictionary['Account']['fields']['birthdate_c']['inline_edit']='1';
$dictionary['Account']['fields']['birthdate_c']['labelValue']='Birthdate';

 

 // created: 2018-05-21 10:47:28
$dictionary['Account']['fields']['exchange_lastmodified_date_c']['inline_edit']=1;

 

 // created: 2018-07-26 13:12:46
$dictionary['Account']['fields']['http_referer_c']['inline_edit']='1';
$dictionary['Account']['fields']['http_referer_c']['labelValue']='Http referer:';

 

 // created: 2018-04-25 13:24:10
$dictionary['Account']['fields']['commodoties_c']['inline_edit']='1';
$dictionary['Account']['fields']['commodoties_c']['labelValue']='Commodoties per Year';

 

 // created: 2018-04-17 07:01:32
$dictionary['Account']['fields']['poi_c']['inline_edit']='1';
$dictionary['Account']['fields']['poi_c']['labelValue']='ID';

 

 // created: 2018-04-17 07:02:08
$dictionary['Account']['fields']['changeleverage_c']['inline_edit']='1';
$dictionary['Account']['fields']['changeleverage_c']['labelValue']='Change Leverage';

 

 // created: 2018-04-17 07:02:45
$dictionary['Account']['fields']['partnerwebsite3_c']['inline_edit']='1';
$dictionary['Account']['fields']['partnerwebsite3_c']['labelValue']='Site-3';

 

 // created: 2018-04-27 13:25:24
$dictionary['Account']['fields']['numbers_calls_c']['inline_edit']='1';
$dictionary['Account']['fields']['numbers_calls_c']['labelValue']='numbers calls';

 

 // created: 2018-04-17 07:04:46
$dictionary['Account']['fields']['riskoftranscactions_c']['inline_edit']='1';
$dictionary['Account']['fields']['riskoftranscactions_c']['labelValue']='Do you understand the nature and risks of margin transactions';

 

 // created: 2018-04-17 07:33:19
$dictionary['Account']['fields']['fpeyear_c']['inline_edit']='1';
$dictionary['Account']['fields']['fpeyear_c']['labelValue']='Forex Per Year';

 

 // created: 2018-04-17 07:05:48
$dictionary['Account']['fields']['oftentrade_c']['inline_edit']='1';
$dictionary['Account']['fields']['oftentrade_c']['labelValue']='How often do you trade';

 

// created: 2018-04-13 10:47:56
$dictionary["Account"]["fields"]["accounts_abc_mt4id_1"] = array (
  'name' => 'accounts_abc_mt4id_1',
  'type' => 'link',
  'relationship' => 'accounts_abc_mt4id_1',
  'source' => 'non-db',
  'module' => 'abc_MT4ID',
  'bean_name' => 'abc_MT4ID',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_ABC_MT4ID_1_FROM_ABC_MT4ID_TITLE',
);


 // created: 2018-04-17 07:06:31
$dictionary['Account']['fields']['how_did_you_hear_about_us_c']['inline_edit']='1';
$dictionary['Account']['fields']['how_did_you_hear_about_us_c']['labelValue']='How did you hear about us';

 

 // created: 2018-05-31 12:55:34
$dictionary['Account']['fields']['accounttype_c']['inline_edit']='1';
$dictionary['Account']['fields']['accounttype_c']['labelValue']='Account Type ';

 

 // created: 2018-04-17 07:07:08
$dictionary['Account']['fields']['fxstat_c']['inline_edit']='1';
$dictionary['Account']['fields']['fxstat_c']['labelValue']='Allow FX Stat';

 

 // created: 2018-04-17 07:11:27
$dictionary['Account']['fields']['porcomment_c']['inline_edit']='1';
$dictionary['Account']['fields']['porcomment_c']['labelValue']='POR Comment';

 

 // created: 2018-04-17 07:13:17
$dictionary['Account']['fields']['partnerwebsite2_c']['inline_edit']='1';
$dictionary['Account']['fields']['partnerwebsite2_c']['labelValue']='Site-2';

 

 // created: 2018-04-24 07:10:45
$dictionary['Account']['fields']['cardcomment_c']['inline_edit']='1';
$dictionary['Account']['fields']['cardcomment_c']['labelValue']='Card Comment ';

 

 // created: 2018-07-30 13:41:24
$dictionary['Account']['fields']['indicies_c']['inline_edit']='1';
$dictionary['Account']['fields']['indicies_c']['labelValue']='Indicies Per Year';

 

 // created: 2018-07-26 10:04:15
$dictionary['Account']['fields']['clienttype_c']['inline_edit']='1';
$dictionary['Account']['fields']['clienttype_c']['labelValue']='Client Type';

 

 // created: 2018-06-26 06:43:44
$dictionary['Account']['fields']['fundedfirstamount_c']['inline_edit']='1';
$dictionary['Account']['fields']['fundedfirstamount_c']['labelValue']='First Amount Funded';

 

 // created: 2018-08-02 11:14:02
$dictionary['Account']['fields']['accountstatus_c']['inline_edit']='1';
$dictionary['Account']['fields']['accountstatus_c']['labelValue']='Account Status / Secure Status:';

 

 // created: 2018-04-24 07:09:19
$dictionary['Account']['fields']['creditdebitlast4digits_c']['inline_edit']='1';
$dictionary['Account']['fields']['creditdebitlast4digits_c']['labelValue']='Last 4 Digits';

 

 // created: 2019-08-21 14:59:22
$dictionary['Account']['fields']['refered_by_c']['inline_edit']='1';
$dictionary['Account']['fields']['refered_by_c']['labelValue']='Referred by';

 

 // created: 2018-05-21 10:47:28
$dictionary['Account']['fields']['exchange_synch_c']['inline_edit']=1;

 

 // created: 2018-04-25 12:27:31
$dictionary['Account']['fields']['phone_mobile_c']['inline_edit']='1';
$dictionary['Account']['fields']['phone_mobile_c']['labelValue']='Mobile';

 

 // created: 2018-04-17 12:50:18
$dictionary['Account']['fields']['salutation_c']['inline_edit']='1';
$dictionary['Account']['fields']['salutation_c']['labelValue']='Salutation';

 

 // created: 2018-12-19 15:53:39
$dictionary['Account']['fields']['campaign_c']['inline_edit']='1';
$dictionary['Account']['fields']['campaign_c']['labelValue']='Campaign';

 

 // created: 2018-04-24 07:07:10
$dictionary['Account']['fields']['creditdebitexpiration_c']['inline_edit']='1';
$dictionary['Account']['fields']['creditdebitexpiration_c']['labelValue']='Credit/Debit Card Expiration';

 

 // created: 2018-07-30 13:45:12
$dictionary['Account']['fields']['contactlanguage_c']['inline_edit']='1';
$dictionary['Account']['fields']['contactlanguage_c']['labelValue']='Language/Desk';

 

 // created: 2018-07-26 10:44:10
$dictionary['Account']['fields']['email1']['inline_edit']=true;
$dictionary['Account']['fields']['email1']['merge_filter']='disabled';

 

 // created: 2018-04-17 07:16:14
$dictionary['Account']['fields']['promotionalemails_c']['inline_edit']='1';
$dictionary['Account']['fields']['promotionalemails_c']['labelValue']='Promotional Emails';

 

 // created: 2018-07-26 13:19:14
$dictionary['Account']['fields']['volumetradedpermonth_c']['inline_edit']='1';
$dictionary['Account']['fields']['volumetradedpermonth_c']['labelValue']='Volume Per Month';

 

 // created: 2018-04-17 07:18:19
$dictionary['Account']['fields']['poiexpiration_c']['inline_edit']='1';
$dictionary['Account']['fields']['poiexpiration_c']['labelValue']='ID Expiration';

 

 // created: 2019-01-14 10:39:08
$dictionary['Account']['fields']['fundedworkflow5_c']['inline_edit']='1';
$dictionary['Account']['fields']['fundedworkflow5_c']['labelValue']='Workflow 2.5';

 

 // created: 2019-02-28 14:18:12
$dictionary['Account']['fields']['poistatusreason_c']['inline_edit']='1';
$dictionary['Account']['fields']['poistatusreason_c']['labelValue']='POI Status Reason';

 

 // created: 2018-04-17 07:19:03
$dictionary['Account']['fields']['bankruptcy_c']['inline_edit']='1';
$dictionary['Account']['fields']['bankruptcy_c']['labelValue']='Have you declared bankruptcy';

 

 // created: 2018-03-30 09:33:35
$dictionary['Account']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2018-03-30 09:33:35
$dictionary['Account']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2019-01-14 10:36:24
$dictionary['Account']['fields']['fundedworkflow3_c']['inline_edit']='1';
$dictionary['Account']['fields']['fundedworkflow3_c']['labelValue']='Worfklow 2.3';

 

 // created: 2018-07-12 06:23:18
$dictionary['Account']['fields']['datecreatedcustom_c']['inline_edit']='1';
$dictionary['Account']['fields']['datecreatedcustom_c']['labelValue']='datecreatedcustom';

 

 // created: 2018-04-17 07:28:07
$dictionary['Account']['fields']['porissued_c']['inline_edit']='1';
$dictionary['Account']['fields']['porissued_c']['labelValue']='POR Issued Date';

 

 // created: 2018-04-17 07:29:08
$dictionary['Account']['fields']['pexposed_c']['inline_edit']='1';
$dictionary['Account']['fields']['pexposed_c']['labelValue']='Are you politically exposed person?';

 

 // created: 2018-07-30 13:42:19
$dictionary['Account']['fields']['typeofpartner_c']['inline_edit']='1';
$dictionary['Account']['fields']['typeofpartner_c']['labelValue']='Type of Partner';

 

 // created: 2018-03-30 09:33:35
$dictionary['Account']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2019-01-14 10:35:32
$dictionary['Account']['fields']['fundedworkflow2_c']['inline_edit']='1';
$dictionary['Account']['fields']['fundedworkflow2_c']['labelValue']='Workflow 2.2';

 

 // created: 2018-04-17 07:30:06
$dictionary['Account']['fields']['reason_c']['inline_edit']='1';
$dictionary['Account']['fields']['reason_c']['labelValue']='Reason to open an account';

 

// created: 2019-09-27 14:25:17
$dictionary["Account"]["fields"]["accounts_fin12_financial_information_2"] = array (
  'name' => 'accounts_fin12_financial_information_2',
  'type' => 'link',
  'relationship' => 'accounts_fin12_financial_information_2',
  'source' => 'non-db',
  'module' => 'fin12_financial_information',
  'bean_name' => 'fin12_financial_information',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_2_FROM_FIN12_FINANCIAL_INFORMATION_TITLE',
);


 // created: 2019-03-06 07:25:05
$dictionary['Account']['fields']['promotion_code_c']['inline_edit']='1';
$dictionary['Account']['fields']['promotion_code_c']['labelValue']='Promotion Code';

 

 // created: 2018-04-25 09:22:02
$dictionary['Account']['fields']['progressstatus_c']['inline_edit']='1';
$dictionary['Account']['fields']['progressstatus_c']['labelValue']='Progress';

 

 // created: 2018-04-17 12:57:22
$dictionary['Account']['fields']['verifieddate_c']['inline_edit']='1';
$dictionary['Account']['fields']['verifieddate_c']['labelValue']='Verified Date';

 

 // created: 2018-07-30 13:43:23
$dictionary['Account']['fields']['clientspermonth_c']['inline_edit']='1';
$dictionary['Account']['fields']['clientspermonth_c']['labelValue']='Clients Per Month';

 

 // created: 2019-08-27 08:48:48
$dictionary['Account']['fields']['utm_campaign_c']['inline_edit']='1';
$dictionary['Account']['fields']['utm_campaign_c']['labelValue']='Utm campaign';

 

 // created: 2019-01-14 10:39:48
$dictionary['Account']['fields']['fundedworkflow7_c']['inline_edit']='1';
$dictionary['Account']['fields']['fundedworkflow7_c']['labelValue']='Workflow 2.7';

 

 // created: 2018-03-30 09:33:35
$dictionary['Account']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2018-04-17 07:33:58
$dictionary['Account']['fields']['verifiedphone_c']['inline_edit']='1';
$dictionary['Account']['fields']['verifiedphone_c']['labelValue']='Phone is Verified';

 

 // created: 2018-04-17 07:34:29
$dictionary['Account']['fields']['age_c']['inline_edit']='1';
$dictionary['Account']['fields']['age_c']['labelValue']='I confirm that I am over 18 years of age';

 

 // created: 2018-05-04 06:46:17
$dictionary['Account']['fields']['por_c']['inline_edit']='1';
$dictionary['Account']['fields']['por_c']['labelValue']='POR';

 

 // created: 2018-05-03 07:31:45
$dictionary['Account']['fields']['funded_c']['inline_edit']='1';
$dictionary['Account']['fields']['funded_c']['labelValue']='Funded';

 

 // created: 2019-09-02 08:27:42
$dictionary['Account']['fields']['team_c']['inline_edit']='1';
$dictionary['Account']['fields']['team_c']['labelValue']='Team';

 

 // created: 2018-04-27 13:19:42
$dictionary['Account']['fields']['proof_of_address_status_c']['inline_edit']='1';
$dictionary['Account']['fields']['proof_of_address_status_c']['labelValue']='Proof Of Address Status';

 

 // created: 2018-05-21 10:47:28
$dictionary['Account']['fields']['exchange_id_c']['inline_edit']=1;

 







$dictionary['Account']['fields']['sugarchimpactivity'] = array(
    'name' => 'sugarchimpactivity',
    'type' => 'link',
    'relationship' => 'sugarchimpactivity_accounts',
    'module' => 'SugarChimpActivity',
    'bean_name' => 'SugarChimpActivity',
    'source' => 'non-db',
    'vname' => 'LBL_SUGARCHIMPACTIVITY',
);

$dictionary['Account']['fields']['mailchimp_rating_c'] = array(
	'name' => 'mailchimp_rating_c',
	'vname' => 'LBL_MAILCHIMP_RATING',
	'type' => 'mailchimprating',
	'dbType' => 'int',
	'len' => '3',
	'comment' => 'MailChimp Rating',
	'required' => false,
	'studio' => true,
	'reportable' => true,
	'default' => 2,
	'readonly' => true,
);

 // created: 2018-04-17 07:36:58
$dictionary['Account']['fields']['invamount_c']['inline_edit']='1';
$dictionary['Account']['fields']['invamount_c']['labelValue']='Inv.Ammount';

 

 // created: 2019-01-14 10:37:27
$dictionary['Account']['fields']['fundedworkflow4_c']['inline_edit']='1';
$dictionary['Account']['fields']['fundedworkflow4_c']['labelValue']='Workflow 2_4';

 

 // created: 2018-04-17 07:37:19
$dictionary['Account']['fields']['ib_c']['inline_edit']='1';
$dictionary['Account']['fields']['ib_c']['labelValue']='IB';

 

 // created: 2019-07-09 10:47:51
$dictionary['Account']['fields']['teamleader_c']['inline_edit']='1';
$dictionary['Account']['fields']['teamleader_c']['labelValue']='Team Leader';

 

 // created: 2018-05-03 06:53:06
$dictionary['Account']['fields']['creditcard_c']['inline_edit']='1';
$dictionary['Account']['fields']['creditcard_c']['labelValue']='Credit/Debit Card Status';

 

 // created: 2019-08-27 08:48:33
$dictionary['Account']['fields']['utm_medium_c']['inline_edit']='1';
$dictionary['Account']['fields']['utm_medium_c']['labelValue']='Utm medium';

 

 // created: 2018-05-21 10:47:28
$dictionary['Account']['fields']['exchange_synched_c']['inline_edit']=1;

 

 // created: 2018-04-17 06:37:34
$dictionary['Account']['fields']['strategyprovider_c']['inline_edit']='1';
$dictionary['Account']['fields']['strategyprovider_c']['labelValue']='Strategy Provider';

 

 // created: 2019-10-23 09:05:03
$dictionary['Account']['fields']['clicktocall_c']['inline_edit']='1';
$dictionary['Account']['fields']['clicktocall_c']['labelValue']='ClickToCall';

 
?>