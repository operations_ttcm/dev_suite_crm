<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2018-04-13 10:45:21
$layout_defs["Accounts"]["subpanel_setup"]['accounts_fin12_financial_information_1'] = array (
  'order' => 100,
  'module' => 'fin12_financial_information',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_1_FROM_FIN12_FINANCIAL_INFORMATION_TITLE',
  'get_subpanel_data' => 'accounts_fin12_financial_information_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2018-04-13 10:47:56
$layout_defs["Accounts"]["subpanel_setup"]['accounts_abc_mt4id_1'] = array (
  'order' => 100,
  'module' => 'abc_MT4ID',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_ABC_MT4ID_1_FROM_ABC_MT4ID_TITLE',
  'get_subpanel_data' => 'accounts_abc_mt4id_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2019-09-27 14:25:17
$layout_defs["Accounts"]["subpanel_setup"]['accounts_fin12_financial_information_2'] = array (
  'order' => 100,
  'module' => 'fin12_financial_information',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_2_FROM_FIN12_FINANCIAL_INFORMATION_TITLE',
  'get_subpanel_data' => 'accounts_fin12_financial_information_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['accounts_abc_mt4id_1']['override_subpanel_name'] = 'Account_subpanel_accounts_abc_mt4id_1';

?>