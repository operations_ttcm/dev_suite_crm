<?php
$viewdefs ['Accounts'] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'SAVE',
          1 => 'CANCEL',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'modules/Accounts/Account.js',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_ACCOUNT_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL5' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL6' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL7' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL8' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_ADVANCED' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_account_information' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'salutation_c',
            'studio' => 'visible',
            'label' => 'LBL_SALUTATION',
          ),
          1 => 
          array (
            'name' => 'name',
            'label' => 'LBL_NAME',
            'displayParams' => 
            array (
              'required' => true,
            ),
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'email1',
            'studio' => 'false',
            'label' => 'LBL_EMAIL',
          ),
          1 => 
          array (
            'name' => 'brand_c',
            'studio' => 'visible',
            'label' => 'LBL_BRAND',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'accountstatus_c',
            'studio' => 'visible',
            'label' => 'LBL_ACCOUNTSTATUS',
          ),
          1 => 
          array (
            'name' => 'verifieddate_c',
            'label' => 'LBL_VERIFIEDDATE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'progressstatus_c',
            'studio' => 'visible',
            'label' => 'LBL_PROGRESSSTATUS',
          ),
          1 => 
          array (
            'name' => 'billing_address_country',
            'comment' => 'The country used for the billing address',
            'label' => 'LBL_BILLING_ADDRESS_COUNTRY',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'billing_address_street',
            'hideLabel' => true,
            'type' => 'address',
            'displayParams' => 
            array (
              'key' => 'billing',
              'rows' => 2,
              'cols' => 30,
              'maxlength' => 150,
            ),
          ),
          1 => 
          array (
            'name' => 'billing_address_city',
            'comment' => 'The city used for billing address',
            'label' => 'LBL_BILLING_ADDRESS_CITY',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'phone_mobile_c',
            'label' => 'LBL_PHONE_MOBILE',
          ),
          1 => 
          array (
            'name' => 'phone_office',
            'label' => 'LBL_PHONE_OFFICE',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'verifiedphone_c',
            'label' => 'LBL_VERIFIEDPHONE',
          ),
          1 => 
          array (
            'name' => 'birthdate_c',
            'label' => 'LBL_BIRTHDATE',
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'invamount_c',
            'studio' => 'visible',
            'label' => 'LBL_INVAMOUNT',
          ),
          1 => 
          array (
            'name' => 'fpeyear_c',
            'studio' => 'visible',
            'label' => 'LBL_FPEYEAR',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'equities_c',
            'studio' => 'visible',
            'label' => 'LBL_EQUITIES',
          ),
          1 => 
          array (
            'name' => 'indicies_c',
            'studio' => 'visible',
            'label' => 'LBL_INDICIES_C',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'reason_c',
            'studio' => 'visible',
            'label' => 'LBL_REASON',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'oftentrade_c',
            'studio' => 'visible',
            'label' => 'LBL_OFTENTRADE',
          ),
          1 => 
          array (
            'name' => 'pexposed_c',
            'studio' => 'visible',
            'label' => 'LBL_PEXPOSED',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'riskoftranscactions_c',
            'studio' => 'visible',
            'label' => 'LBL_RISKOFTRANSCACTIONS',
          ),
          1 => 
          array (
            'name' => 'bankruptcy_c',
            'studio' => 'visible',
            'label' => 'LBL_BANKRUPTCY',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'age_c',
            'label' => 'LBL_AGE',
          ),
        ),
      ),
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'changeleverage_c',
            'label' => 'LBL_CHANGELEVERAGE',
          ),
          1 => 
          array (
            'name' => 'accountrestrections_c',
            'label' => 'LBL_ACCOUNTRESTRECTIONS',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'localdepositprovider_c',
            'label' => 'LBL_LOCALDEPOSITPROVIDER',
          ),
          1 => 
          array (
            'name' => 'riskassessement_c',
            'label' => 'LBL_RISKASSESSEMENT',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'fxstat_c',
            'label' => 'LBL_FXSTAT',
          ),
          1 => 
          array (
            'name' => 'strategyprovider_c',
            'label' => 'LBL_STRATEGYPROVIDER',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'zulutrade_c',
            'label' => 'LBL_ZULUTRADE',
          ),
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'poi_c',
            'studio' => 'visible',
            'label' => 'LBL_POI',
          ),
          1 => 
          array (
            'name' => 'poiissueddate_c',
            'label' => 'LBL_POIISSUEDDATE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'poiexpiration_c',
            'label' => 'LBL_POIEXPIRATION',
          ),
          1 => 
          array (
            'name' => 'idcomment_c',
            'label' => 'LBL_IDCOMMENT',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'por_c',
            'studio' => 'visible',
            'label' => 'LBL_POR',
          ),
          1 => 
          array (
            'name' => 'porissued_c',
            'label' => 'LBL_PORISSUED',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'porcomment_c',
            'label' => 'LBL_PORCOMMENT',
          ),
          1 => 
          array (
            'name' => 'creditcard_c',
            'studio' => 'visible',
            'label' => 'LBL_CREDITCARD',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'creditdebitexpiration_c',
            'label' => 'LBL_CREDITDEBITEXPIRATION',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'fundeddate_c',
            'label' => 'LBL_FUNDEDDATE',
          ),
          1 => 
          array (
            'name' => 'funded_c',
            'label' => 'LBL_FUNDED',
          ),
        ),
      ),
      'lbl_editview_panel5' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'typeofpartner_c',
            'studio' => 'visible',
            'label' => 'LBL_TYPEOFPARTNER',
          ),
          1 => 
          array (
            'name' => 'clientspermonth_c',
            'studio' => 'visible',
            'label' => 'LBL_CLIENTSPERMONTH',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'clientdepositspermonth_c',
            'studio' => 'visible',
            'label' => 'LBL_CLIENTDEPOSITSPERMONTH',
          ),
          1 => 
          array (
            'name' => 'volumetradedpermonth_c',
            'studio' => 'visible',
            'label' => 'LBL_VOLUMETRADEDPERMONTH',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'masterib_c',
            'label' => 'LBL_MASTERIB',
          ),
          1 => 
          array (
            'name' => 'ib_c',
            'label' => 'LBL_IB',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'subibs_c',
            'studio' => 'visible',
            'label' => 'LBL_SUBIBS',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel6' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'partnerwebsite1_c',
            'label' => 'LBL_PARTNERWEBSITE1',
          ),
          1 => 
          array (
            'name' => 'partnerwebsite2_c',
            'label' => 'LBL_PARTNERWEBSITE2',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'partnerwebsite3_c',
            'label' => 'LBL_PARTNERWEBSITE3',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel7' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'newsletters_c',
            'label' => 'LBL_NEWSLETTERS',
          ),
          1 => 
          array (
            'name' => 'promotionalemails_c',
            'label' => 'LBL_PROMOTIONALEMAILS',
          ),
        ),
      ),
      'lbl_editview_panel8' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'contactlanguage_c',
            'studio' => 'visible',
            'label' => 'LBL_CONTACTLANGUAGE',
          ),
          1 => 
          array (
            'name' => 'customersupport_c',
            'studio' => 'visible',
            'label' => 'LBL_CUSTOMERSUPPORT',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO',
          ),
          1 => '',
        ),
      ),
      'LBL_PANEL_ADVANCED' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'refered_by_c',
            'label' => 'LBL_REFERED_BY',
          ),
          1 => 'industry',
        ),
        1 => 
        array (
          0 => 'annual_revenue',
          1 => 'employees',
        ),
        2 => 
        array (
          0 => 'parent_name',
          1 => 'account_type',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'accounttype_c',
            'studio' => 'visible',
            'label' => 'LBL_ACCOUNTTYPE',
          ),
          1 => 
          array (
            'name' => 'http_referer_c',
            'label' => 'LBL_HTTP_REFERER',
          ),
        ),
        4 => 
        array (
          0 => 'campaign_name',
          1 => '',
        ),
      ),
    ),
  ),
);
;
?>
