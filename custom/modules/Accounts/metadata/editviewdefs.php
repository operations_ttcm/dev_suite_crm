<?php
$viewdefs ['Accounts'] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'SAVE',
          1 => 'CANCEL',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'modules/Accounts/Account.js',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_ACCOUNT_INFORMATION' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL9' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL10' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL11' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL12' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL13' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL8' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_account_information' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'salutation_c',
            'studio' => 'visible',
            'label' => 'LBL_SALUTATION',
          ),
          1 => 
          array (
            'name' => 'name',
            'label' => 'LBL_NAME',
            'displayParams' => 
            array (
              'required' => true,
            ),
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'email1',
            'studio' => 'false',
            'label' => 'LBL_EMAIL',
          ),
          1 => 
          array (
            'name' => 'brand_c',
            'studio' => 'visible',
            'label' => 'LBL_BRAND',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'accountstatus_c',
            'studio' => 'visible',
            'label' => 'LBL_ACCOUNTSTATUS',
          ),
          1 => 
          array (
            'name' => 'verifieddate_c',
            'label' => 'LBL_VERIFIEDDATE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'verifiedlevel_c',
            'studio' => 'visible',
            'label' => 'LBL_VERIFIEDLEVEL',
          ),
          1 => 
          array (
            'name' => 'refered_by_c',
            'label' => 'LBL_REFERED_BY',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'clienttype_c',
            'label' => 'LBL_CLIENTTYPE',
          ),
          1 => 
          array (
            'name' => 'progressstatus_c',
            'studio' => 'visible',
            'label' => 'LBL_PROGRESSSTATUS',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'billing_address_street',
            'hideLabel' => true,
            'type' => 'address',
            'displayParams' => 
            array (
              'key' => 'billing',
              'rows' => 2,
              'cols' => 30,
              'maxlength' => 150,
            ),
          ),
          1 => 
          array (
            'name' => 'billing_address_city',
            'comment' => 'The city used for billing address',
            'label' => 'LBL_BILLING_ADDRESS_CITY',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'billing_address_country',
            'comment' => 'The country used for the billing address',
            'label' => 'LBL_BILLING_ADDRESS_COUNTRY',
          ),
          1 => 
          array (
            'name' => 'verifiedphone_c',
            'label' => 'LBL_VERIFIEDPHONE',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'phone_mobile_c',
            'label' => 'LBL_PHONE_MOBILE',
          ),
          1 => 
          array (
            'name' => 'clicktocall_c',
            'label' => 'LBL_CLICKTOCALL',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'birthdate_c',
            'label' => 'LBL_BIRTHDATE',
          ),
          1 => '',
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'funded_c',
            'label' => 'LBL_FUNDED',
          ),
          1 => 
          array (
            'name' => 'fundedfirstamount_c',
            'label' => 'LBL_FUNDEDFIRSTAMOUNT',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'fundeddate_c',
            'label' => 'LBL_FUNDEDDATE',
          ),
          1 => 
          array (
            'name' => 'invamount_c',
            'studio' => 'visible',
            'label' => 'LBL_INVAMOUNT',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'promotion_code_c',
            'label' => 'LBL_PROMOTION_CODE',
          ),
          1 => '',
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'age_c',
            'label' => 'LBL_AGE',
          ),
          1 => 
          array (
            'name' => 'http_referer_c',
            'label' => 'LBL_HTTP_REFERER',
          ),
        ),
      ),
      'lbl_editview_panel9' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'poi_c',
            'studio' => 'visible',
            'label' => 'LBL_POI',
          ),
          1 => 
          array (
            'name' => 'poistatusreason_c',
            'label' => 'LBL_POISTATUSREASON',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'poiissueddate_c',
            'label' => 'LBL_POIISSUEDDATE',
          ),
          1 => 
          array (
            'name' => 'poiexpiration_c',
            'label' => 'LBL_POIEXPIRATION',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'idcomment_c',
            'label' => 'LBL_IDCOMMENT',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'por_c',
            'studio' => 'visible',
            'label' => 'LBL_POR',
          ),
          1 => 
          array (
            'name' => 'porstatusreason_c',
            'label' => 'LBL_PORSTATUSREASON',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'porissued_c',
            'label' => 'LBL_PORISSUED',
          ),
          1 => 
          array (
            'name' => 'porcomment_c',
            'label' => 'LBL_PORCOMMENT',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'creditcard_c',
            'studio' => 'visible',
            'label' => 'LBL_CREDITCARD',
          ),
          1 => 
          array (
            'name' => 'creditcardstatusreason_c',
            'label' => 'LBL_CREDITCARDSTATUSREASON',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'creditdebitexpiration_c',
            'label' => 'LBL_CREDITDEBITEXPIRATION',
          ),
          1 => 
          array (
            'name' => 'creditdebitlast4digits_c',
            'label' => 'LBL_CREDITDEBITLAST4DIGITS',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'cardcomment_c',
            'label' => 'LBL_CARDCOMMENT',
          ),
        ),
      ),
      'lbl_editview_panel10' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'fpeyear_c',
            'studio' => 'visible',
            'label' => 'LBL_FPEYEAR',
          ),
          1 => 
          array (
            'name' => 'indicies_c',
            'studio' => 'visible',
            'label' => 'LBL_INDICIES_C',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'equities_c',
            'studio' => 'visible',
            'label' => 'LBL_EQUITIES',
          ),
          1 => 
          array (
            'name' => 'commodoties_c',
            'studio' => 'visible',
            'label' => 'LBL_COMMODOTIES',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'riskoftranscactions_c',
            'studio' => 'visible',
            'label' => 'LBL_RISKOFTRANSCACTIONS',
          ),
          1 => 
          array (
            'name' => 'pexposed_c',
            'studio' => 'visible',
            'label' => 'LBL_PEXPOSED',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'bankruptcy_c',
            'studio' => 'visible',
            'label' => 'LBL_BANKRUPTCY',
          ),
          1 => 
          array (
            'name' => 'reason_c',
            'studio' => 'visible',
            'label' => 'LBL_REASON',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'oftentrade_c',
            'studio' => 'visible',
            'label' => 'LBL_OFTENTRADE',
          ),
        ),
      ),
      'lbl_editview_panel11' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'changeleverage_c',
            'label' => 'LBL_CHANGELEVERAGE',
          ),
          1 => 
          array (
            'name' => 'accountrestrections_c',
            'label' => 'LBL_ACCOUNTRESTRECTIONS',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'localdepositprovider_c',
            'label' => 'LBL_LOCALDEPOSITPROVIDER',
          ),
          1 => 
          array (
            'name' => 'riskassessement_c',
            'label' => 'LBL_RISKASSESSEMENT',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'fxstat_c',
            'label' => 'LBL_FXSTAT',
          ),
          1 => 
          array (
            'name' => 'strategyprovider_c',
            'label' => 'LBL_STRATEGYPROVIDER',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'zulutrade_c',
            'label' => 'LBL_ZULUTRADE',
          ),
        ),
      ),
      'lbl_editview_panel12' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'typeofpartner_c',
            'studio' => 'visible',
            'label' => 'LBL_TYPEOFPARTNER',
          ),
          1 => 
          array (
            'name' => 'clientspermonth_c',
            'studio' => 'visible',
            'label' => 'LBL_CLIENTSPERMONTH',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'clientdepositspermonth_c',
            'studio' => 'visible',
            'label' => 'LBL_CLIENTDEPOSITSPERMONTH',
          ),
          1 => 
          array (
            'name' => 'volumetradedpermonth_c',
            'studio' => 'visible',
            'label' => 'LBL_VOLUMETRADEDPERMONTH',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'masterib_c',
            'label' => 'LBL_MASTERIB',
          ),
          1 => 
          array (
            'name' => 'ib_c',
            'label' => 'LBL_IB',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'subibs_c',
            'studio' => 'visible',
            'label' => 'LBL_SUBIBS',
          ),
          1 => 
          array (
            'name' => 'partnerwebsite1_c',
            'label' => 'LBL_PARTNERWEBSITE1',
          ),
        ),
      ),
      'lbl_editview_panel13' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'unsubscribe_c',
            'label' => 'LBL_UNSUBSCRIBE',
          ),
          1 => 
          array (
            'name' => 'promotionalemails_c',
            'label' => 'LBL_PROMOTIONALEMAILS',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'newsletters_c',
            'label' => 'LBL_NEWSLETTERS',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'utm_source_c',
            'label' => 'LBL_UTM_SOURCE',
          ),
          1 => 
          array (
            'name' => 'utm_medium_c',
            'label' => 'LBL_UTM_MEDIUM',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'utm_campaign_c',
            'label' => 'LBL_UTM_CAMPAIGN',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel8' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'contactlanguage_c',
            'studio' => 'visible',
            'label' => 'LBL_CONTACTLANGUAGE',
          ),
          1 => 
          array (
            'name' => 'customersupport_c',
            'studio' => 'visible',
            'label' => 'LBL_CUSTOMERSUPPORT',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO',
          ),
          1 => 
          array (
            'name' => 'internalaffiliate_c',
            'studio' => 'visible',
            'label' => 'LBL_INTERNALAFFILIATE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'team_c',
            'studio' => 'visible',
            'label' => 'LBL_TEAM',
          ),
          1 => 
          array (
            'name' => 'teammanagernew_c',
            'studio' => 'visible',
            'label' => 'LBL_TEAMMANAGERNEW',
          ),
        ),
      ),
    ),
  ),
);
;
?>
