<?php
// Do not store anything in this file that is not part of the array or the hook version.  This file will	
// be automatically rebuilt in the future. 
 $hook_version = 1; 
$hook_array = Array(); 
// position, file, function 
$hook_array['after_save'] = Array(); 
$hook_array['after_save'][] = Array(1, 'AOD Index Changes', 'modules/AOD_Index/AOD_LogicHooks.php','AOD_LogicHooks', 'saveModuleChanges'); 
$hook_array['after_save'][] = Array(30, 'popup_select', 'modules/SecurityGroups/AssignGroups.php','AssignGroups', 'popup_select'); 
$hook_array['after_save'][] = Array(69, 'Process Manager Hook', 'modules/PM_ProcessManager/insertIntoPmEntryTable.php','insertIntoPmEntryTable', 'setPmEntryTable'); 
$hook_array['after_save'][] = Array(55, 'SugarChimp', 'modules/SugarChimp/includes/classes/SugarChimp/LogicHook.php','SugarChimp_LogicHook', 'RelatedRecordUpdateAfterSave'); 
$hook_array['after_save'][] = Array(60, 'SmartList', 'modules/SmartList/includes/classes/SmartList/LogicHook.php','SmartList_LogicHook', 'PersonUpdateAfterSave'); 
$hook_array['after_delete'] = Array(); 
$hook_array['after_delete'][] = Array(1, 'AOD Index changes', 'modules/AOD_Index/AOD_LogicHooks.php','AOD_LogicHooks', 'saveModuleDelete'); 
$hook_array['after_restore'] = Array(); 
$hook_array['after_restore'][] = Array(1, 'AOD Index changes', 'modules/AOD_Index/AOD_LogicHooks.php','AOD_LogicHooks', 'saveModuleRestore'); 
$hook_array['after_ui_footer'] = Array(); 
$hook_array['after_ui_footer'][] = Array(10, 'popup_onload', 'modules/SecurityGroups/AssignGroups.php','AssignGroups', 'popup_onload'); 
$hook_array['after_ui_frame'] = Array(); 
$hook_array['after_ui_frame'][] = Array(20, 'mass_assign', 'modules/SecurityGroups/AssignGroups.php','AssignGroups', 'mass_assign'); 
$hook_array['after_ui_frame'][] = Array(1, 'Load Social JS', 'include/social/hooks.php','hooks', 'load_js'); 
$hook_array['server_round_trip'] = Array(); 
$hook_array['server_round_trip'][] = Array(1, '', 'custom/modules/ExchangeSynchronization/license_job_hook.php','ValidateLicenseJob', 'validateLicense'); 
$hook_array['before_save'] = Array(); 
$hook_array['before_save'][] = Array(55, 'SugarChimp', 'modules/SugarChimp/includes/classes/SugarChimp/LogicHook.php','SugarChimp_LogicHook', 'RelatedRecordUpdateBeforeSave'); 
$hook_array['after_relationship_add'] = Array(); 
$hook_array['after_relationship_add'][] = Array(55, 'SugarChimp', 'modules/SugarChimp/includes/classes/SugarChimp/LogicHook.php','SugarChimp_LogicHook', 'RelatedRecordAfterRelationshipAdd'); 
$hook_array['after_relationship_add'][] = Array(60, 'SmartList', 'modules/SmartList/includes/classes/SmartList/LogicHook.php','SmartList_LogicHook', 'PersonAfterRelationshipAdd'); 
$hook_array['after_relationship_delete'] = Array(); 
$hook_array['after_relationship_delete'][] = Array(55, 'SugarChimp', 'modules/SugarChimp/includes/classes/SugarChimp/LogicHook.php','SugarChimp_LogicHook', 'RelatedRecordAfterRelationshipDelete'); 
$hook_array['after_relationship_delete'][] = Array(60, 'SmartList', 'modules/SmartList/includes/classes/SmartList/LogicHook.php','SmartList_LogicHook', 'PersonAfterRelationshipDelete'); 



?>