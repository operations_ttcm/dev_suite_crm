<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2019-09-27 14:25:17
$dictionary["fin12_financial_information"]["fields"]["accounts_fin12_financial_information_2"] = array (
  'name' => 'accounts_fin12_financial_information_2',
  'type' => 'link',
  'relationship' => 'accounts_fin12_financial_information_2',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_2_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_fin12_financial_information_2accounts_ida',
);
$dictionary["fin12_financial_information"]["fields"]["accounts_fin12_financial_information_2_name"] = array (
  'name' => 'accounts_fin12_financial_information_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_2_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_fin12_financial_information_2accounts_ida',
  'link' => 'accounts_fin12_financial_information_2',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["fin12_financial_information"]["fields"]["accounts_fin12_financial_information_2accounts_ida"] = array (
  'name' => 'accounts_fin12_financial_information_2accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_fin12_financial_information_2',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_2_FROM_FIN12_FINANCIAL_INFORMATION_TITLE',
);


 // created: 2018-04-13 14:35:58
$dictionary['fin12_financial_information']['fields']['fifteendayspl_c']['inline_edit']='1';
$dictionary['fin12_financial_information']['fields']['fifteendayspl_c']['labelValue']='Fifteen Days P/L';

 

 // created: 2018-04-13 14:27:36
$dictionary['fin12_financial_information']['fields']['totaldeposits_c']['inline_edit']='1';
$dictionary['fin12_financial_information']['fields']['totaldeposits_c']['labelValue']='Total Deposits';

 

 // created: 2018-04-13 14:28:33
$dictionary['fin12_financial_information']['fields']['totalwithdrawals_c']['inline_edit']='1';
$dictionary['fin12_financial_information']['fields']['totalwithdrawals_c']['labelValue']='Total Withdrawals';

 

// created: 2018-04-13 10:45:21
$dictionary["fin12_financial_information"]["fields"]["accounts_fin12_financial_information_1"] = array (
  'name' => 'accounts_fin12_financial_information_1',
  'type' => 'link',
  'relationship' => 'accounts_fin12_financial_information_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_fin12_financial_information_1accounts_ida',
);
$dictionary["fin12_financial_information"]["fields"]["accounts_fin12_financial_information_1_name"] = array (
  'name' => 'accounts_fin12_financial_information_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_fin12_financial_information_1accounts_ida',
  'link' => 'accounts_fin12_financial_information_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["fin12_financial_information"]["fields"]["accounts_fin12_financial_information_1accounts_ida"] = array (
  'name' => 'accounts_fin12_financial_information_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_fin12_financial_information_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_1_FROM_FIN12_FINANCIAL_INFORMATION_TITLE',
);


 // created: 2018-04-13 14:35:05
$dictionary['fin12_financial_information']['fields']['weeklytotalpl_c']['inline_edit']='1';
$dictionary['fin12_financial_information']['fields']['weeklytotalpl_c']['labelValue']='Weekly Total P/L';

 

 // created: 2018-04-13 14:33:31
$dictionary['fin12_financial_information']['fields']['daytotalpl_c']['inline_edit']='1';
$dictionary['fin12_financial_information']['fields']['daytotalpl_c']['labelValue']='Day Total P/L';

 

 // created: 2018-04-13 14:29:04
$dictionary['fin12_financial_information']['fields']['totalvolume_c']['inline_edit']='1';
$dictionary['fin12_financial_information']['fields']['totalvolume_c']['labelValue']='Total Volume';

 

 // created: 2018-04-13 14:36:35
$dictionary['fin12_financial_information']['fields']['monthlytotalpl_c']['inline_edit']='1';
$dictionary['fin12_financial_information']['fields']['monthlytotalpl_c']['labelValue']='Monthly Total P/L';

 

 // created: 2018-04-13 14:39:17
$dictionary['fin12_financial_information']['fields']['name']['required']=false;
$dictionary['fin12_financial_information']['fields']['name']['inline_edit']=true;
$dictionary['fin12_financial_information']['fields']['name']['duplicate_merge']='disabled';
$dictionary['fin12_financial_information']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['fin12_financial_information']['fields']['name']['merge_filter']='disabled';
$dictionary['fin12_financial_information']['fields']['name']['unified_search']=false;

 

// created: 2018-04-13 10:44:42
$dictionary["fin12_financial_information"]["fields"]["leads_fin12_financial_information_1"] = array (
  'name' => 'leads_fin12_financial_information_1',
  'type' => 'link',
  'relationship' => 'leads_fin12_financial_information_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_FIN12_FINANCIAL_INFORMATION_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_fin12_financial_information_1leads_ida',
);
$dictionary["fin12_financial_information"]["fields"]["leads_fin12_financial_information_1_name"] = array (
  'name' => 'leads_fin12_financial_information_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_FIN12_FINANCIAL_INFORMATION_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_fin12_financial_information_1leads_ida',
  'link' => 'leads_fin12_financial_information_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["fin12_financial_information"]["fields"]["leads_fin12_financial_information_1leads_ida"] = array (
  'name' => 'leads_fin12_financial_information_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_fin12_financial_information_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_FIN12_FINANCIAL_INFORMATION_1_FROM_FIN12_FINANCIAL_INFORMATION_TITLE',
);

?>