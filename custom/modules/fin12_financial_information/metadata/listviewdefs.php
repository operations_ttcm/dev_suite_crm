<?php
$module_name = 'fin12_financial_information';
$listViewDefs [$module_name] = 
array (
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'TOTALDEPOSITS_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_TOTALDEPOSITS',
    'width' => '10%',
  ),
  'TOTALWITHDRAWALS_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_TOTALWITHDRAWALS',
    'width' => '10%',
  ),
  'TOTALVOLUME_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_TOTALVOLUME',
    'width' => '10%',
  ),
  'DAYTOTALPL_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_DAYTOTALPL',
    'width' => '10%',
  ),
  'WEEKLYTOTALPL_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_WEEKLYTOTALPL',
    'width' => '10%',
  ),
  'FIFTEENDAYSPL_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_FIFTEENDAYSPL',
    'width' => '10%',
  ),
  'MONTHLYTOTALPL_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_MONTHLYTOTALPL',
    'width' => '10%',
  ),
  'ACCOUNTS_FIN12_FINANCIAL_INFORMATION_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_FIN12_FINANCIAL_INFORMATION_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_FIN12_FINANCIAL_INFORMATION_1ACCOUNTS_IDA',
    'width' => '10%',
    'default' => true,
  ),
);
;
?>
