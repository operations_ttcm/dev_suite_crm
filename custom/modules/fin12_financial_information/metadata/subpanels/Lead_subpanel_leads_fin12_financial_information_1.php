<?php
// created: 2018-04-13 14:59:24
$subpanel_layout['list_fields'] = array (
  'date_entered' => 
  array (
    'type' => 'datetime',
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'totaldeposits_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_TOTALDEPOSITS',
    'width' => '10%',
  ),
  'totalwithdrawals_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_TOTALWITHDRAWALS',
    'width' => '10%',
  ),
  'totalvolume_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_TOTALVOLUME',
    'width' => '10%',
  ),
  'daytotalpl_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_DAYTOTALPL',
    'width' => '10%',
  ),
  'weeklytotalpl_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_WEEKLYTOTALPL',
    'width' => '10%',
  ),
  'fifteendayspl_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_FIFTEENDAYSPL',
    'width' => '10%',
  ),
  'monthlytotalpl_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_MONTHLYTOTALPL',
    'width' => '10%',
  ),
);