<?php
$module_name = 'fin12_financial_information';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'totaldeposits_c',
            'label' => 'LBL_TOTALDEPOSITS',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'totalwithdrawals_c',
            'label' => 'LBL_TOTALWITHDRAWALS',
          ),
          1 => 
          array (
            'name' => 'totalvolume_c',
            'label' => 'LBL_TOTALVOLUME',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'daytotalpl_c',
            'label' => 'LBL_DAYTOTALPL',
          ),
          1 => 
          array (
            'name' => 'weeklytotalpl_c',
            'label' => 'LBL_WEEKLYTOTALPL',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'fifteendayspl_c',
            'label' => 'LBL_FIFTEENDAYSPL',
          ),
          1 => 
          array (
            'name' => 'monthlytotalpl_c',
            'label' => 'LBL_MONTHLYTOTALPL',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'date_entered',
            'comment' => 'Date record created',
            'label' => 'LBL_DATE_ENTERED',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'leads_fin12_financial_information_1_name',
          ),
          1 => 
          array (
            'name' => 'accounts_fin12_financial_information_1_name',
          ),
        ),
      ),
    ),
  ),
);
;
?>
