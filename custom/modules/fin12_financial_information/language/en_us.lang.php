<?php
// created: 2018-04-13 14:39:17
$mod_strings = array (
  'LBL_TOTALDEPOSITS' => 'Total Deposits',
  'LBL_TOTALWITHDRAWALS' => 'Total Withdrawals',
  'LBL_TOTALVOLUME' => 'Total Volume',
  'LBL_DAYTOTALPL' => 'Day Total P/L',
  'LBL_WEEKLYTOTALPL' => 'Weekly Total P/L',
  'LBL_FIFTEENDAYSPL' => 'Fifteen Days P/L',
  'LBL_MONTHLYTOTALPL' => 'Monthly Total P/L',
  'LBL_NAME' => 'Name',
);