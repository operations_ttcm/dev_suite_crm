<?php 
 //WARNING: The contents of this file are auto-generated



global $sugar_version;

$admin_option_defs=array();

if(preg_match( "/^6.*/", $sugar_version) ) {
    
	$admin_option_defs['Administration']['dt_whatsapp_config']= array('','LBL_DTWHATSAPP_LICENSE_WHATSAPP_CONFIGURATION','LBL_DTWHATSAPP_LICENSE_MESSAGE','./index.php?module=DT_Whatsapp&action=whatsapp_config');

    // $admin_option_defs['Administration']['dt_whatsapp_info']= array('','LBL_DTWHATSAPP_LICENSE_TITLE','LBL_DTWHATSAPP_LICENSE','./index.php?module=DT_Whatsapp&action=license');
    
} else {

	$admin_option_defs['Administration']['dt_whatsapp_config']= array('','LBL_DTWHATSAPP_LICENSE_WHATSAPP_CONFIGURATION','LBL_DTWHATSAPP_LICENSE_MESSAGE','#bwc/index.php?module=DT_Whatsapp&action=whatsapp_config');

    // $admin_option_defs['Administration']['dt_whatsapp_info']= array('','LBL_DTWHATSAPP_LICENSE_TITLE','LBL_DTWHATSAPP_LICENSE','javascript:parent.SUGAR.App.router.navigate("#bwc/index.php?module=DT_Whatsapp&action=license", {trigger: true});');
 
}

$admin_group_header[]= array('LBL_DTWHATSAPP','',false,$admin_option_defs, '');



global $sugar_version;

$admin_option_defs=array();

if(preg_match( "/^6.*/", $sugar_version) ) {
    $admin_option_defs['Administration']['gmsyncaddon_info']= array('helpInline','LBL_GMSYNCADDON_LICENSE_TITLE','LBL_GMSYNCADDON_LICENSE','./index.php?module=GMSyncAddon&action=license');
} else {
    $admin_option_defs['Administration']['gmsyncaddon_info']= array('helpInline','LBL_GMSYNCADDON_LICENSE_TITLE','LBL_GMSYNCADDON_LICENSE','javascript:parent.SUGAR.App.router.navigate("#bwc/index.php?module=GMSyncAddon&action=license", {trigger: true});');
}

$admin_group_header[]= array('LBL_GMSYNCADDON','',false,$admin_option_defs, '');

 







global $sugar_version;

$admin_option_defs=array();

if(preg_match( "/^6.*/", $sugar_version) ) {
    $admin_option_defs['Administration']['sugarchimp_setup']= array('Import','LBL_SUGARCHIMP_SETUP_WIZARD_TITLE','LBL_SUGARCHIMP_SETUP_WIZARD','./index.php?module=SugarChimp&action=setup');
    $admin_option_defs['Administration']['sugarchimp_health']= array('Diagnostic','LBL_SUGARCHIMP_HEALTH_STATUS_TITLE','LBL_SUGARCHIMP_HEALTH_STATUS','./index.php?module=SugarChimp&action=health_status');
    $admin_option_defs['Administration']['sugarchimp_field_mapping']= array('Import','LBL_SUGARCHIMP_FIELD_MAPPING_TITLE','LBL_SUGARCHIMP_FIELD_MAPPING','./index.php?module=SugarChimp&action=field_mapping');
    $admin_option_defs['Administration']['sugarchimp_campaign_summary']= array('Import','LBL_SUGARCHIMP_CAMPAIGN_SUMMARY_TITLE','LBL_SUGARCHIMP_CAMPAIGN_SUMMARY','./index.php?module=SugarChimp&action=campaigns');
    $admin_option_defs['Administration']['sugarchimp_data_privacy']= array('Import','LBL_SUGARCHIMP_DATA_PRIVACY_TITLE','LBL_SUGARCHIMP_DATA_PRIVACY','./index.php?module=SugarChimp&action=data_privacy');
} else {
    $admin_option_defs['Administration']['sugarchimp_setup']= array('Import','LBL_SUGARCHIMP_SETUP_WIZARD_TITLE','LBL_SUGARCHIMP_SETUP_WIZARD','javascript:parent.SUGAR.App.router.navigate("SugarChimp/layout/setup", {trigger: true});');
    $admin_option_defs['Administration']['sugarchimp_health']= array('Diagnostic','LBL_SUGARCHIMP_HEALTH_STATUS_TITLE','LBL_SUGARCHIMP_HEALTH_STATUS','javascript:parent.SUGAR.App.router.navigate("SugarChimp/layout/health-status", {trigger: true});');
    $admin_option_defs['Administration']['sugarchimp_field_mapping']= array('Import','LBL_SUGARCHIMP_FIELD_MAPPING_TITLE','LBL_SUGARCHIMP_FIELD_MAPPING','javascript:parent.SUGAR.App.router.navigate("SugarChimp/layout/field-mapping", {trigger: true});');
    $admin_option_defs['Administration']['sugarchimpactivity_index']= array('Import','LBL_SUGARCHIMPACTIVITY_TITLE','LBL_SUGARCHIMPACTIVITY','javascript:parent.SUGAR.App.router.navigate("SugarChimpActivity", {trigger: true});');
    $admin_option_defs['Administration']['sugarchimp_data_privacy']= array('Import','LBL_SUGARCHIMP_DATA_PRIVACY_TITLE','LBL_SUGARCHIMP_DATA_PRIVACY','javascript:parent.SUGAR.App.router.navigate("SugarChimp/layout/data-privacy", {trigger: true});');
}

$admin_group_header[]= array('LBL_SUGARCHIMP','',false,$admin_option_defs, '');




$admin_option_defs=array();
$admin_option_defs['Administration']['pm_processmanager_info']= array('helpInline','LBL_PM_PROCESSMANAGER_LICENSE_TITLE','LBL_PM_PROCESSMANAGER_LICENSE','./index.php?module=PM_ProcessManager&action=license');

$admin_group_header[]= array('LBL_PM_PROCESSMANAGER','',false,$admin_option_defs, '');


?>