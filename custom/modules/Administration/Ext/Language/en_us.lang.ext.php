<?php 
 //WARNING: The contents of this file are auto-generated









$mod_strings['LBL_SUGARCHIMP'] = 'SugarChimp';
$mod_strings['LBL_SUGARCHIMP_SETUP_WIZARD_TITLE'] = 'SugarChimp Configuration';
$mod_strings['LBL_SUGARCHIMP_SETUP_WIZARD'] = 'Configuration wizard for changing your API Key';
$mod_strings['LBL_SUGARCHIMP_HEALTH_STATUS_TITLE'] = 'Health Status';
$mod_strings['LBL_SUGARCHIMP_HEALTH_STATUS'] = 'Check on the current state of your MailChimp synchronization';
$mod_strings['LBL_SUGARCHIMP_LICENSE_TITLE'] = 'License Configuration';
$mod_strings['LBL_SUGARCHIMP_LICENSE'] = 'Change your license key';
$mod_strings['LBL_SUGARCHIMP_FIELD_MAPPING_TITLE'] = 'Field Mapping';
$mod_strings['LBL_SUGARCHIMP_FIELD_MAPPING'] = 'Map your MailChimp List fields with your SugarCRM Contact, Target and Lead fields.';
$mod_strings['LBL_SUGARCHIMP_DATA_PRIVACY_TITLE'] = 'Data Privacy';
$mod_strings['LBL_SUGARCHIMP_DATA_PRIVACY'] = 'Tool to manage data privacy requests.';
$mod_strings['LBL_SUGARCHIMP_CAMPAIGN_SUMMARY_TITLE'] = 'Campaign Summary Reports';
$mod_strings['LBL_SUGARCHIMP_CAMPAIGN_SUMMARY'] = 'See how your MailChimp Campaigns have performed.';
$mod_strings['LBL_SUGARCHIMP_SMARTLIST_TITLE'] = 'SmartList Setup';
$mod_strings['LBL_SUGARCHIMP_SMARTLIST_SUMMARY'] = 'Configure your automatic smart list filters.';
$mod_strings['LBL_SUGARCHIMPACTIVITY_TITLE'] = 'MailChimp Activities';
$mod_strings['LBL_SUGARCHIMPACTIVITY'] = 'View your Subscribers MailChimp Activities';


$mod_strings['LBL_DTWHATSAPP'] = 'Twilio WhatsApp License Add-on';
$mod_strings['LBL_DTWHATSAPP_LICENSE_TITLE'] = 'License Configuration';
$mod_strings['LBL_DTWHATSAPP_LICENSE'] = 'Manage and configure the license for this add-on';

$mod_strings['LBL_DTWHATSAPP_LICENSE_WHATSAPP_CONFIGURATION'] = 'Whatsapp API Settings';
$mod_strings['LBL_DTWHATSAPP_LICENSE_MESSAGE'] = 'Configure Twilio API credentials details';




$mod_strings['LBL_GMSYNCADDON'] = 'GrinMark Synchronizer Add-on';
$mod_strings['LBL_GMSYNCADDON_LICENSE_TITLE'] = 'License Configuration';
$mod_strings['LBL_GMSYNCADDON_LICENSE'] = 'Manage and configure the license for Outlook 365 AddOn';



$mod_strings['LBL_PM_PROCESSMANAGER'] = 'Process Manager Add-on';
$mod_strings['LBL_PM_PROCESSMANAGER_LICENSE_TITLE'] = 'License Configuration';
$mod_strings['LBL_PM_PROCESSMANAGER_LICENSE'] = 'Manage and configure the license for Process Manager';

?>