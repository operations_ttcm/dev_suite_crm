<?php
// created: 2018-11-26 07:32:36
$modules_sources = array (
  'Leads' => 
  array (
    'ext_rest_sugarchimp' => 'ext_rest_sugarchimp',
  ),
  'Accounts' => 
  array (
    'ext_rest_sugarchimp' => 'ext_rest_sugarchimp',
  ),
  'Emails' => 
  array (
    'ext_rest_sugarchimp' => 'ext_rest_sugarchimp',
  ),
  'ProspectLists' => 
  array (
    'ext_rest_sugarchimp' => 'ext_rest_sugarchimp',
    'ext_rest_sugarchimplist' => 'ext_rest_sugarchimplist',
  ),
);