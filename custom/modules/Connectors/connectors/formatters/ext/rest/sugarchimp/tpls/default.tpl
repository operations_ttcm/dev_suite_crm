




<link rel="stylesheet" href="modules/SugarChimp/includes/assets/css/sugarchimp-dashlet.css?8.1.1a" />
<script type="text/javascript" src="{sugar_getjspath file='include/connectors/formatters/default/company_detail.js'}"></script>

{literal}
<style type="text/css">
    .yui-panel .hd {
        background-color:#3D77CB;
        border-color:#FFFFFF #FFFFFF #000000;
        border-style:solid;
        border-width:1px;
        color:#000000;
        font-size:100%;
        font-weight:bold;
        line-height:100%;
        padding:4px;
        white-space:nowrap;
    }
</style>
{/literal}

<script type="text/javascript">
    function show_ext_rest_sugarchimp(event)
    {literal}
    {
        var xCoordinate = event.clientX;
        var yCoordinate = event.clientY;
        var isIE = document.all?true:false;

        if(isIE) {
            xCoordinate = xCoordinate + document.body.scrollLeft;
            yCoordinate = yCoordinate + document.body.scrollTop;
        }
    
        cd = new CompanyDetailsDialog("sugarchimp_popup_div", '<div id="sugarchimp_div"><p>Loading...</p></div>', xCoordinate, yCoordinate);
        cd.setHeader("MailChimp Activities");
        cd.display();
        
        
        if ('{/literal}{{$mapping_module}}{literal}' != 'Accounts'){
            // get email address
            $.ajax({
                url: 'index.php',
                method: 'post',
                dataType: 'json',
                data: {
                    module: 'SugarChimp',
                    action: 'get_email_address',
                    to_pdf: 1,
                    module_name: '{/literal}{{$mapping_module}}{literal}',
                    module_id: '{/literal}{$fields.id.value}{literal}',
                },
                success: function(data){
                    if (data.success === true){
                        $.ajax({
                            url: 'index.php',
                            method: 'post',
                            dataType: 'json',
                            data: {
                                module: 'SugarChimp',
                                action: 'get_dashlet_data',
                                to_pdf: 1,
                                module_email: data.email,
                                module_id: '{/literal}{$fields.id.value}{literal}',
                            },
                            success: function(data){
                                if (data.success === true){
                                    $('#sugarchimp_div').html(data.html);
                                } else {
                                    $('#sugarchimp_div').html('Could not load data: '+data.message);
                                }
                            }
                        });
                    } else {
                        alert('Could not load data: '+data.message);
                    }
                },
            });
        }else{
            //go to accounts ajax
            // get id
            $.ajax({
                url: 'index.php',
                method: 'post',
                dataType: 'json',
                data: {
                    module: 'SugarChimp',
                    action: 'get_accounts_dashlet_data',
                    to_pdf: 1,
                    module_id: '{/literal}{$fields.id.value}{literal}',
                },
                success: function(data){
                    if (data.success === true){
                        $('#sugarchimp_div').html(data.html);
                    } else {
                        $('#sugarchimp_div').html('Could not load data: '+data.message);
                    }
                }

            });
        }

    }
    {/literal}
</script>
