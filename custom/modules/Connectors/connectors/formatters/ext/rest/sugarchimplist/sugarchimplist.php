<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






require_once('include/connectors/formatters/default/formatter.php');

class ext_rest_sugarchimplist_formatter extends default_formatter
{
    public function getDetailViewFormat()
    {
       $this->_ss->assign('mapping_module', $this->_module);
       return $this->fetchSmarty();
    }

    public function getIconFilePath()
    {
       //icon for display
       return 'modules/SugarChimp/includes/assets/img/dashlet/freddie-mini.png';
    }
}