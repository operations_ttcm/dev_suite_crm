




var bigObj = this;

var sugarChimpSelf = {
    initDashlet: function() {
        Handlebars.registerHelper("eq",function(val1,val2,options)
        {
            return val1==val2?options.fn(this):options.inverse(this);
        });
        Handlebars.registerHelper("inc", function(number, options){
            if(typeof(number) === 'undefined' || number === null)
                return "0";
                
                // Increment by inc parameter if it exists or just by one
                return number + (options.hash.inc || 1);
        });
        Handlebars.registerHelper('toUpperCase', function(str) {
          if(typeof(str) === 'undefined' || str === null)
          {
            return "";
          }
          return str.toUpperCase();
        });
        Handlebars.registerHelper("check", function(str)
        {
            if(typeof(str) === 'undefined' || str === null)
            {
                return "";
            }
            if(str === "Prospects")
            {
                return "Targets";
            }
            return str;
        });
        sugarChimpSelf.loadData();
    },
    loadData: function (options) 
    {
        // get current list's id
        var list_id = sugarchimp_list_dashlet_list_id;
        var mailchimp_list_id = sugarchimp_list_dashlet_mailchimp_list_id;
        $.ajax({
            url: 'index.php',
            method: 'GET',
            dataType: 'json',
            data: {
                module: 'SugarChimp',
                action: 'smartlist_dashlet',
                mailchimp_list_id: mailchimp_list_id
            },
            success: function(data,response) {
                sugarChimpSelf.data = data;
                sugarChimpSelf.list_id = list_id;
                sugarChimpSelf.numbers_match = true;
                sugarChimpSelf.isListSynced = true;

                if (!sugarChimpSelf.data ||
                    !sugarChimpSelf.data.sugar_lists ||
                    !sugarChimpSelf.data.sugar_lists.list ||
                    !sugarChimpSelf.data.sugar_lists.list[list_id] ||
                    !sugarChimpSelf.data.sugar_lists.list[list_id].mailchimp_list_name_c){
                    
                    // this target list is not synced to a MailChimp list
                    sugarChimpSelf.isListSynced = false;
                    
                } else {
                    var mc_id = sugarChimpSelf.data.sugar_lists.list[list_id].mailchimp_list_name_c;
                    sugarChimpSelf.mc_list_name = sugarChimpSelf.data.mailchimp_lists.lists[mc_id].name;
                    // change the star rating to reflect the MC List
                    sugarChimpSelf.list_rating = Math.round(sugarChimpSelf.data.mailchimp_lists.lists[mc_id].list_rating);
                    
                    // set subscribers
                    sugarChimpSelf.mc_subscribers = sugarChimpSelf.data.mailchimp_lists.lists[mc_id].stats.member_count;
                    // set open rate
                    sugarChimpSelf.open_rate = (sugarChimpSelf.data.mailchimp_lists.lists[mc_id].stats.open_rate).toFixed(2) + '%';
                    
                    // set click rate
                    sugarChimpSelf.click_rate = (sugarChimpSelf.data.mailchimp_lists.lists[mc_id].stats.click_rate).toFixed(2) + '%';

                    //if MC subscribers differ from Target List Count
                    if(sugarChimpSelf.mc_subscribers != 
                            sugarChimpSelf.data.sugar_lists.list[list_id].subscriber_count)
                    {
                        sugarChimpSelf.numbers_match = false;
                    }
                    //if it has smartlist metadata
                    if(sugarChimpSelf.data.sugar_lists.list[list_id].smartlist && 
                            sugarChimpSelf.data.sugar_lists.list[list_id].smartlist.metadata){
                        sugarChimpSelf.supported_modules = [];
                        sugarChimpSelf.metadata = {};
                        


                        for(var key in sugarChimpSelf.data.available_fields){
                            sugarChimpSelf.supported_modules.push(key);
                        }
                        // foreach supported module
                        for(var i = 0; i < sugarChimpSelf.supported_modules.length; i++){
                            var module = sugarChimpSelf.supported_modules[i];
                            
                            if(sugarChimpSelf.data.sugar_lists.list[list_id].smartlist.metadata[module]){
                                //make it more accessible
                                sugarChimpSelf.metadata[module] = sugarChimpSelf.data.sugar_lists.list[list_id].smartlist.metadata[module];
                                // foreach smart filter
                                if(sugarChimpSelf.metadata[module].mode == 'smart'){
                                    //get field, operand, and value objects instead of their keys
                                    for(var j = 0; j < sugarChimpSelf.metadata[module].smart.filters.length; j++){
                                        var field_key = sugarChimpSelf.metadata[module].smart.filters[j].field;
                                        sugarChimpSelf.metadata[module].smart.filters[j].field = sugarChimpSelf.data.available_fields[module][field_key];
                                        
                                        var operand_key = sugarChimpSelf.metadata[module].smart.filters[j].operand;
                                        var field_type = sugarChimpSelf.data.available_fields[module][field_key].type;
                                        
                                        if(!sugarChimpSelf.data.fields_info[field_type])
                                            field_type = 'varchar';
                                        
                                        sugarChimpSelf.metadata[module].smart.filters[j].operand = sugarChimpSelf.data.fields_info[field_type]['operands'][operand_key];
                                        //if the value is an array, format the string accordingly
                                        if(sugarChimpSelf.metadata[module].smart.filters[j].value instanceof Array)
                                        {
                                           var values = "";
                                           for(var k = 0; k < sugarChimpSelf.metadata[module].smart.filters[j].value.length; k++)
                                           {
                                                if(k != 0){
                                                    values += ', ';
                                                }
                                                values += sugarChimpSelf.metadata[module].smart.filters[j].value[k];
                                           }
                                           sugarChimpSelf.metadata[module].smart.filters[j].value = values;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                sugarChimpSelf.renderDashlet();
            }
        });
    },
    renderDashlet: function(){
        var dashlet_html = sugarChimpSelf.renderViewData("hbs-render-smartlist-dashlet",null,sugarChimpSelf);
        $("#sugarchimplist_div").html(dashlet_html);
    },
    smartlistTabToggle: function(evt){
        $('#sugarchimplist_div .smartlist-tab').hide();
        var $el = bigObj.$(evt.currentTarget);
        $('#sugarchimplist_div .smartlist-nav li').removeClass('active');
        $el.parent().addClass('active');
        var divId = $el.attr('id');
        $('#sugarchimplist_div #smartlist-div-'+divId).show(); 
    },
    editSmartListSettings: function(evt){
        var $el;
        //$el = this.$(evt.currentTarget);
        var list_id = $el.attr('data-id');
        //implement going to smartlist page to edit the list
    },
    // left module in so that we can use the renderViewData Sugar7 calls
    renderViewData: function(template,module,data){
        if (!template || !data){ return false;}
        var source = $("#"+template).html();
        var template = Handlebars.compile(source);
        return template(data);
    }
};