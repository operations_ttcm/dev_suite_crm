




<link rel="stylesheet" type="text/css" href="modules/SugarChimp/includes/assets/css/ce/bootstrap.css?8.1.1a">
<link rel="stylesheet" type="text/css" href="modules/SugarChimp/includes/assets/css/ce/sugarchimp_sugar6.css?8.1.1a">
<div id="include-ce-css"></div>
<script type="text/javascript" src="{sugar_getjspath file='include/connectors/formatters/default/company_detail.js'}"></script>
<script type="text/javascript" src="modules/SugarChimp/includes/assets/js/ce/handlebars.js"></script>
<script type="text/javascript" src="custom/modules/Connectors/connectors/formatters/ext/rest/sugarchimplist/sugarchimplist.js"></script>
{literal}
<script type="text/javascript">

    var html = $('#hbs-render-smartlist-dashlet-primary').val();
    var temp_script = document.createElement('script');
    temp_script.setAttribute('type','text/x-handlebars-template');
    temp_script.setAttribute('id','hbs-render-smartlist-dashlet');
    document.head.appendChild(temp_script);
    $('#hbs-render-smartlist-dashlet').html(html);

    var sugarchimp_list_dashlet_list_id = '{/literal}{$fields.id.value}{literal}';
    var sugarchimp_list_dashlet_mailchimp_list_id = '{/literal}{$fields.mailchimp_list_name_c.value}{literal}';
    function show_ext_rest_sugarchimplist(event)
    {
        var xCoordinate = event.clientX;
        var yCoordinate = event.clientY;
        var isIE = document.all?true:false;

        if(isIE) {
            xCoordinate = xCoordinate + document.body.scrollLeft;
            yCoordinate = yCoordinate + document.body.scrollTop;
        }
    
        cd = new CompanyDetailsDialog("sugarchimplist_popup_div", '<div id="sugarchimplist_div"><p>Loading...</p></div>', xCoordinate, yCoordinate);
        cd.setHeader("SugarChimp List Properties");
        cd.display();
        
        $('#sugarchimplist_div').on('click','.smartlist-nav li a',function(evt){
            sugarChimpSelf.smartlistTabToggle(evt);
            return false;
        });
        $('#sugarchimplist_div').on('click','.edit-smartlist-settings',function(evt){
            sugarChimpSelf.editSmartListSettings(evt);
            return false;
        });
        
        sugarChimpSelf.initDashlet();
    }
</script>
{/literal}

<textarea id="hbs-render-smartlist-dashlet-primary" style="display:none">
    {include file="modules/SugarChimp/includes/assets/handlebars/smartlist-dashlet.hbs"}
</textarea>