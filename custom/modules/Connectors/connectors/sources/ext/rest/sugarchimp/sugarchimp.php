<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






require_once('include/connectors/sources/ext/rest/rest.php');

class ext_rest_sugarchimp extends ext_rest
{
    protected $_enable_in_hover = true;
    protected $_enable_in_admin_properties = false;
    protected $_enable_in_admin_mapping = true;
    protected $_enable_in_admin_search = false;
	protected $_has_testing_enabled = false;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function test()
    {
        return array();
    }

    public function getItem($args=array(), $module=null)
    {
        return array();
    }

    public function getList($args=array(), $module=null)
    {
        return array();
    }
}