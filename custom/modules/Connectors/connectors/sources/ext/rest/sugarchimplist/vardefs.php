<?php if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');






$dictionary['ext_rest_sugarchimplist'] = array(
    'comment' => 'Vardefs for SugarChimp List Connector',
    'fields' => array(
        'name' => array(
            'name' => 'name',
            'vname' => 'LBL_NAME',
            'type' => 'varchar',
            'comment' => 'Name',
            'hover' => true,
        ),
    )
);