<?php
// created: 2018-05-31 10:33:09
$mod_strings = array (
  'LBL_IP ' => 'IP',
  'LBL_EDITVIEW_PANEL1' => 'New Panel 1',
  'LBL_QUICKCREATE_PANEL1' => 'New Panel 1',
  'LBL_QUICKCREATE_PANEL2' => 'New Panel 2',
  'LBL_QUICKCREATE_PANEL3' => 'New Panel 3',
  'LBL_CURRENCY' => 'Currency',
  'LNK_NEW_RECORD' => 'Create MT4 Accounts',
  'LNK_LIST' => 'View MT4 Accounts',
  'LNK_IMPORT_ABC_MT4ID' => 'Import MT4 Accounts',
  'LBL_LIST_FORM_TITLE' => 'MT4ID List',
  'LBL_SEARCH_FORM_TITLE' => 'Search MT4ID',
  'LBL_HOMEPAGE_TITLE' => 'My MT4 Accounts',
  'LBL_BALANCE' => 'Balance',
  'LBL_EQUITY' => 'Equity',
  'LBL_VOLUME' => 'Volume',
  'LBL_LEVERAGE' => 'Leverage',
  'LBL_FLOATING' => 'Floating',
  'LBL_CREDIT' => 'Credit',
  'LBL_TYPE' => 'Type',
  'LBL_ACCOUNT_TYPE' => 'MT4 Type',
);