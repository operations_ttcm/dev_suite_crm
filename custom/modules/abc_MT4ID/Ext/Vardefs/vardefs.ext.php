<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2018-04-23 12:51:05
$dictionary['abc_MT4ID']['fields']['type_c']['inline_edit']='1';
$dictionary['abc_MT4ID']['fields']['type_c']['labelValue']='Type';

 

 // created: 2018-04-09 09:39:18
$dictionary['abc_MT4ID']['fields']['currency_c']['inline_edit']='1';
$dictionary['abc_MT4ID']['fields']['currency_c']['labelValue']='Currency';

 

 // created: 2018-04-09 09:22:20
$dictionary['abc_MT4ID']['fields']['ip_c']['inline_edit']='1';
$dictionary['abc_MT4ID']['fields']['ip_c']['labelValue']='IP';

 

 // created: 2018-04-12 12:54:17
$dictionary['abc_MT4ID']['fields']['volume_c']['inline_edit']='1';
$dictionary['abc_MT4ID']['fields']['volume_c']['labelValue']='Volume';

 

 // created: 2018-04-13 13:11:35
$dictionary['abc_MT4ID']['fields']['credit_c']['inline_edit']='1';
$dictionary['abc_MT4ID']['fields']['credit_c']['labelValue']='Credit';

 

 // created: 2018-04-13 13:10:26
$dictionary['abc_MT4ID']['fields']['floating_c']['inline_edit']='1';
$dictionary['abc_MT4ID']['fields']['floating_c']['labelValue']='Floating';

 

// created: 2018-04-13 10:47:56
$dictionary["abc_MT4ID"]["fields"]["accounts_abc_mt4id_1"] = array (
  'name' => 'accounts_abc_mt4id_1',
  'type' => 'link',
  'relationship' => 'accounts_abc_mt4id_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_ABC_MT4ID_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_abc_mt4id_1accounts_ida',
);
$dictionary["abc_MT4ID"]["fields"]["accounts_abc_mt4id_1_name"] = array (
  'name' => 'accounts_abc_mt4id_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_ABC_MT4ID_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_abc_mt4id_1accounts_ida',
  'link' => 'accounts_abc_mt4id_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["abc_MT4ID"]["fields"]["accounts_abc_mt4id_1accounts_ida"] = array (
  'name' => 'accounts_abc_mt4id_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_abc_mt4id_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_ABC_MT4ID_1_FROM_ABC_MT4ID_TITLE',
);


 // created: 2018-04-13 07:19:11
$dictionary['abc_MT4ID']['fields']['leverage_c']['inline_edit']='1';
$dictionary['abc_MT4ID']['fields']['leverage_c']['labelValue']='Leverage';

 

// created: 2018-04-13 10:48:55
$dictionary["abc_MT4ID"]["fields"]["leads_abc_mt4id_1"] = array (
  'name' => 'leads_abc_mt4id_1',
  'type' => 'link',
  'relationship' => 'leads_abc_mt4id_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_ABC_MT4ID_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_abc_mt4id_1leads_ida',
);
$dictionary["abc_MT4ID"]["fields"]["leads_abc_mt4id_1_name"] = array (
  'name' => 'leads_abc_mt4id_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_ABC_MT4ID_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_abc_mt4id_1leads_ida',
  'link' => 'leads_abc_mt4id_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["abc_MT4ID"]["fields"]["leads_abc_mt4id_1leads_ida"] = array (
  'name' => 'leads_abc_mt4id_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_abc_mt4id_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_ABC_MT4ID_1_FROM_ABC_MT4ID_TITLE',
);


 // created: 2018-04-12 12:53:57
$dictionary['abc_MT4ID']['fields']['equity_c']['inline_edit']='1';
$dictionary['abc_MT4ID']['fields']['equity_c']['labelValue']='Equity';

 

 // created: 2018-08-03 15:29:12
$dictionary['abc_MT4ID']['fields']['account_type_c']['inline_edit']='1';
$dictionary['abc_MT4ID']['fields']['account_type_c']['labelValue']='MT4 Type';

 

 // created: 2018-04-12 10:50:59
$dictionary['abc_MT4ID']['fields']['balance_c']['inline_edit']='1';
$dictionary['abc_MT4ID']['fields']['balance_c']['labelValue']='Balance';

 
?>