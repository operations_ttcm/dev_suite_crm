<?php
$module_name = 'abc_MT4ID';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'currency_c',
            'label' => 'LBL_CURRENCY',
          ),
          1 => 
          array (
            'name' => 'balance_c',
            'label' => 'LBL_BALANCE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'credit_c',
            'label' => 'LBL_CREDIT',
          ),
          1 => 
          array (
            'name' => 'floating_c',
            'label' => 'LBL_FLOATING',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'leverage_c',
            'label' => 'LBL_LEVERAGE',
          ),
          1 => 
          array (
            'name' => 'volume_c',
            'label' => 'LBL_VOLUME',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'equity_c',
            'label' => 'LBL_EQUITY',
          ),
          1 => 
          array (
            'name' => 'ip_c',
            'label' => 'LBL_IP ',
          ),
        ),
        5 => 
        array (
          0 => 'assigned_user_name',
        ),
      ),
    ),
  ),
);
;
?>
