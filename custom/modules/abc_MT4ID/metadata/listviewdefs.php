<?php
$module_name = 'abc_MT4ID';
$listViewDefs [$module_name] = 
array (
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'ACCOUNT_TYPE_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_ACCOUNT_TYPE',
    'width' => '10%',
  ),
  'TYPE_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_TYPE',
    'width' => '10%',
  ),
  'IP_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_IP ',
    'width' => '10%',
  ),
  'EQUITY_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_EQUITY',
    'width' => '10%',
  ),
  'BALANCE_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_BALANCE',
    'width' => '10%',
  ),
  'LEVERAGE_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_LEVERAGE',
    'width' => '10%',
  ),
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
);
;
?>
