<?php
// created: 2018-07-09 14:34:21
$subpanel_layout['list_fields'] = array (
  'date_entered' => 
  array (
    'type' => 'datetime',
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'type_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_TYPE',
    'width' => '10%',
  ),
  'account_type_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_ACCOUNT_TYPE',
    'width' => '10%',
  ),
  'description' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'currency_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_CURRENCY',
    'width' => '10%',
  ),
  'balance_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_BALANCE',
    'width' => '10%',
  ),
  'credit_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_CREDIT',
    'width' => '10%',
  ),
  'floating_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_FLOATING',
    'width' => '10%',
  ),
  'leverage_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_LEVERAGE',
    'width' => '10%',
  ),
  'volume_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_VOLUME',
    'width' => '10%',
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '45%',
    'default' => true,
  ),
  'edit_button' => 
  array (
    'widget_class' => 'SubPanelEditButton',
    'module' => 'abc_MT4ID',
    'width' => '4%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'abc_MT4ID',
    'width' => '5%',
    'default' => true,
  ),
);