<?php
$module_name = 'abc_MT4ID';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'account_type_c',
            'studio' => 'visible',
            'label' => 'LBL_ACCOUNT_TYPE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'currency_c',
            'label' => 'LBL_CURRENCY',
          ),
          1 => 'description',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'balance_c',
            'label' => 'LBL_BALANCE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'credit_c',
            'label' => 'LBL_CREDIT',
          ),
          1 => 
          array (
            'name' => 'floating_c',
            'label' => 'LBL_FLOATING',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'leverage_c',
            'label' => 'LBL_LEVERAGE',
          ),
          1 => 
          array (
            'name' => 'volume_c',
            'label' => 'LBL_VOLUME',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'equity_c',
            'label' => 'LBL_EQUITY',
          ),
          1 => 
          array (
            'name' => 'ip_c',
            'label' => 'LBL_IP ',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'date_entered',
            'comment' => 'Date record created',
            'label' => 'LBL_DATE_ENTERED',
          ),
          1 => 
          array (
            'name' => 'date_modified',
            'comment' => 'Date record last modified',
            'label' => 'LBL_DATE_MODIFIED',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'leads_abc_mt4id_1_name',
          ),
          1 => 
          array (
            'name' => 'accounts_abc_mt4id_1_name',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'type_c',
            'studio' => 'visible',
            'label' => 'LBL_TYPE',
          ),
          1 => 'assigned_user_name',
        ),
      ),
    ),
  ),
);
;
?>
