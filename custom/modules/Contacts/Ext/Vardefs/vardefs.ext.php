<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2019-01-25 07:53:49
$dictionary['Contact']['fields']['brand_c']['inline_edit']='1';
$dictionary['Contact']['fields']['brand_c']['labelValue']='Brand';

 







$dictionary['Contact']['fields']['sugarchimpactivity'] = array(
    'name' => 'sugarchimpactivity',
    'type' => 'link',
    'relationship' => 'sugarchimpactivity_contacts',
    'module' => 'SugarChimpActivity',
    'bean_name' => 'SugarChimpActivity',
    'source' => 'non-db',
    'vname' => 'LBL_SUGARCHIMPACTIVITY',
);

$dictionary['Contact']['fields']['mailchimp_rating_c'] = array(
	'name' => 'mailchimp_rating_c',
	'vname' => 'LBL_MAILCHIMP_RATING',
	'type' => 'mailchimprating',
	'dbType' => 'int',
	'len' => '3',
	'comment' => 'MailChimp Rating',
	'required' => false,
	'studio' => true,
	'reportable' => true,
	'default' => 2,
	'readonly' => true,
);

 // created: 2018-05-21 10:47:29
$dictionary['Contact']['fields']['exchange_lastmodified_date_c']['inline_edit']=1;

 

 // created: 2019-01-25 07:57:23
$dictionary['Contact']['fields']['converted_c']['inline_edit']='1';
$dictionary['Contact']['fields']['converted_c']['labelValue']='Converted to Account?';

 

 // created: 2018-05-21 10:47:29
$dictionary['Contact']['fields']['exchange_synch_c']['inline_edit']=1;

 

 // created: 2018-03-30 09:33:36
$dictionary['Contact']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2018-03-30 09:33:36
$dictionary['Contact']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2018-03-30 09:33:36
$dictionary['Contact']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2018-03-30 09:33:36
$dictionary['Contact']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2018-05-21 10:47:29
$dictionary['Contact']['fields']['exchange_id_c']['inline_edit']=1;

 

 // created: 2018-05-21 10:47:29
$dictionary['Contact']['fields']['exchange_synched_c']['inline_edit']=1;

 
?>