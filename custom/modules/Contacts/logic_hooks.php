<?php
// Do not store anything in this file that is not part of the array or the hook version.  This file will	
// be automatically rebuilt in the future. 
 $hook_version = 1; 
$hook_array = Array(); 
// position, file, function 
$hook_array['before_save'] = Array(); 
$hook_array['before_save'][] = Array(1, 'Contacts push feed', 'modules/Contacts/SugarFeeds/ContactFeed.php','ContactFeed', 'pushFeed'); 
$hook_array['before_save'][] = Array(77, 'updateGeocodeInfo', 'modules/Contacts/ContactsJjwg_MapsLogicHook.php','ContactsJjwg_MapsLogicHook', 'updateGeocodeInfo'); 
$hook_array['before_save'][] = Array(3, '', 'custom/modules/ExchangeSynchronization/SynchedFlagHook.php','SynchedFlagHook', 'synchFlag'); 
$hook_array['before_save'][] = Array(50, 'SugarChimp', 'modules/SugarChimp/includes/classes/SugarChimp/LogicHook.php','SugarChimp_LogicHook', 'PersonUpdateBeforeSave'); 
$hook_array['after_save'] = Array(); 
$hook_array['after_save'][] = Array(1, 'Update Portal', 'modules/Contacts/updatePortal.php','updatePortal', 'updateUser'); 
$hook_array['after_save'][] = Array(77, 'updateRelatedMeetingsGeocodeInfo', 'modules/Contacts/ContactsJjwg_MapsLogicHook.php','ContactsJjwg_MapsLogicHook', 'updateRelatedMeetingsGeocodeInfo'); 
$hook_array['after_save'][] = Array(3, '', 'custom/modules/ExchangeSynchronization/ExchangeSyncHelperTable.php','ExchangeSyncHelperTable', 'updateLastSyncTime'); 
$hook_array['after_save'][] = Array(50, 'SugarChimp', 'modules/SugarChimp/includes/classes/SugarChimp/LogicHook.php','SugarChimp_LogicHook', 'PersonUpdateAfterSave'); 
$hook_array['after_delete'] = Array(); 
$hook_array['after_delete'][] = Array(1, '', 'custom/modules/ExchangeSynchronization/ExchangeSyncHelperTable.php','ExchangeSyncHelperTable', 'deleteItem'); 
$hook_array['after_delete'][] = Array(50, 'SugarChimp', 'modules/SugarChimp/includes/classes/SugarChimp/LogicHook.php','SugarChimp_LogicHook', 'PersonAfterDelete'); 
$hook_array['before_delete'] = Array(); 
$hook_array['before_delete'][] = Array(50, 'SugarChimp', 'modules/SugarChimp/includes/classes/SugarChimp/LogicHook.php','SugarChimp_LogicHook', 'PersonBeforeDelete'); 



?>