<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

require_once 'custom/helpers/AllocationHelper.php';

/**
 *       this will save the call lead relationship whenever an unknown caller calls to this system
 * */
class LeadsAllocation {

    const DEFAULT_USER_ID = '780ddb1d-1702-5d51-b59c-520523390ff7'; //NORDINE

    function create_lead_hook($bean, $event, $arguments) {

        $GLOBALS['log']->fatal(__FUNCTION__ . ". Aristos123 - hook works. event=$event, bean fetched_row=" . $bean->fetched_row['id']);

        //lead create
        if (empty($bean->fetched_row['id'])) {
            $GLOBALS['log']->fatal('Aristos123 - new lead');

            //check sales_code
            //check country
            return;
        }
        //lead update
        else {
            $GLOBALS['log']->fatal('Aristos123 - existing lead');

            //todo 
            //1.remove to put live
            //2.move login to new lead

            $salesCodeAssign = $this->handleSalesCode($bean);
            if ($salesCodeAssign) {
                return;
            }

            //handle country
            $this->handleCountry($bean);

            return;
        }
    }

    public function handleSalesCode($bean) {
        $sales_code = $bean->sales_code_c;
        $GLOBALS['log']->fatal("Aristos123 - sales_code: $sales_code");
        if (empty($sales_code)) {
            $GLOBALS['log']->fatal("Aristos123 - no sales code found");
            return false;
        }

        $ib_partner_bean = BeanFactory::getBean('ibp_ib_partners');
        if (!$ib_partner_bean) {
            $GLOBALS['log']->fatal("Aristos123 - bean not found");
            return false;
        }
        $ib_partner = $ib_partner_bean->retrieve_by_string_fields(array('name' => $sales_code));

        if (is_null($ib_partner)) {
            $GLOBALS['log']->fatal('Aristos123 - no ib partner record found');
            return false;
        }

        $assigned_user_id = $ib_partner_bean->assigned_user_id;

        $result = AllocationHelper::assignAccountManager($bean, $assigned_user_id);

        return $result;
    }

    /**
     * primary_address_country : full format, comes from SA
     * countries_dom : full format
     * 
     * ATTENTION: SOME COUNTRIES ARE NOT THE SAME.
     */
    public function handleCountry($bean) {

        $GLOBALS['log']->fatal("Aristos123 - inside handle country");
        $user_bean = BeanFactory::getBean('Users');
        if (!$user_bean) {
            $GLOBALS['log']->fatal("Aristos123 - user bean not found");
            return false;
        }

        $allAgents = $user_bean->get_full_list();
        $GLOBALS['log']->fatal("Aristos123 - total agents:" . count($allAgents, true));
        $user_country = $bean->primary_address_country;
        //loop through all users
        foreach ($allAgents as $eachAgent) {
            $GLOBALS['log']->fatal("Aristos123 - id:$eachAgent->id, user_name:$eachAgent->user_name ");

            $agent_countries = unencodeMultienum($eachAgent->countries_c);

            if (in_array($user_country, $agent_countries)) {
                $GLOBALS['log']->fatal("Aristos123 - user_country:$user_country, agent:$eachAgent->name.  match with agent country");
                $agent_id = $eachAgent->id;
                return AllocationHelper::assignAccountManager($bean, $agent_id);
            }
        }
        //not found - assign to Nordine
        $GLOBALS['log']->fatal("Aristos123 - no country match found, assigning to default user");
        $agent_id = self::DEFAULT_USER_ID;
        return AllocationHelper::assignAccountManager($bean, $agent_id);
    }

}
