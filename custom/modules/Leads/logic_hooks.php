<?php

// Do not store anything in this file that is not part of the array or the hook version.  This file will
// be automatically rebuilt in the future.
$hook_version = 1;
$hook_array = Array();
// position, file, function
$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(1, 'Leads push feed', 'modules/Leads/SugarFeeds/LeadFeed.php', 'LeadFeed', 'pushFeed');
$hook_array['before_save'][] = Array(77, 'updateGeocodeInfo', 'modules/Leads/LeadsJjwg_MapsLogicHook.php', 'LeadsJjwg_MapsLogicHook', 'updateGeocodeInfo');
$hook_array['before_save'][] = Array(50, 'SugarChimp', 'modules/SugarChimp/includes/classes/SugarChimp/LogicHook.php', 'SugarChimp_LogicHook', 'PersonUpdateBeforeSave');
$hook_array['before_save'][] = Array(20, 'Leads Allocation', 'custom/modules/Leads/LeadsAllocation.php', 'LeadsAllocation', 'create_lead_hook');
$hook_array['after_save'] = Array();
$hook_array['after_save'][] = Array(77, 'updateRelatedMeetingsGeocodeInfo', 'modules/Leads/LeadsJjwg_MapsLogicHook.php', 'LeadsJjwg_MapsLogicHook', 'updateRelatedMeetingsGeocodeInfo');
$hook_array['after_save'][] = Array(50, 'SugarChimp', 'modules/SugarChimp/includes/classes/SugarChimp/LogicHook.php', 'SugarChimp_LogicHook', 'PersonUpdateAfterSave');
$hook_array['after_delete'] = Array();
$hook_array['after_delete'][] = Array(50, 'SugarChimp', 'modules/SugarChimp/includes/classes/SugarChimp/LogicHook.php', 'SugarChimp_LogicHook', 'PersonAfterDelete');
$hook_array['before_delete'] = Array();
$hook_array['before_delete'][] = Array(50, 'SugarChimp', 'modules/SugarChimp/includes/classes/SugarChimp/LogicHook.php', 'SugarChimp_LogicHook', 'PersonBeforeDelete');
$hook_array['after_ui_frame'] = Array();
