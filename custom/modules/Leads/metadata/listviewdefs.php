<?php
$listViewDefs ['Leads'] = 
array (
  'DATE_ENTERED' => 
  array (
    'width' => '10%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => true,
  ),
  'NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'orderBy' => 'name',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'first_name',
      1 => 'last_name',
      2 => 'salutation',
    ),
  ),
  'EMAIL1' => 
  array (
    'width' => '16%',
    'label' => 'LBL_LIST_EMAIL_ADDRESS',
    'sortable' => false,
    'customCode' => '{$EMAIL1_LINK}',
    'default' => true,
  ),
  'BRAND_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_BRAND',
    'width' => '10%',
  ),
  'PRIMARY_ADDRESS_COUNTRY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_PRIMARY_ADDRESS_COUNTRY',
    'default' => true,
  ),
  'PHONE_MOBILE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_MOBILE_PHONE',
    'default' => true,
  ),
  'CLICKTOCALL_C' => 
  array (
    'type' => 'url',
    'default' => true,
    'label' => 'LBL_CLICKTOCALL',
    'width' => '10%',
  ),
  'POR_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_POR',
    'width' => '10%',
  ),
  'POI_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_POI',
    'width' => '10%',
  ),
  'CLIENTTYPE_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_CLIENTTYPE',
    'width' => '10%',
  ),
  'INVAMOUNT_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_INVAMOUNT',
    'width' => '10%',
  ),
  'FUNDED_C' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'LBL_FUNDED',
    'width' => '10%',
  ),
  'SECURESTATUS_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_SECURESTATUS',
    'width' => '10%',
  ),
  'VERIFIEDLEVEL_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_VERIFIEDLEVEL',
    'width' => '10%',
  ),
  'REFERED_BY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_REFERED_BY',
    'default' => true,
  ),
  'PROMOTION_CODE_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_PROMOTION_CODE',
    'width' => '10%',
  ),
  'UTM_SOURCE_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_UTM_SOURCE',
    'width' => '10%',
  ),
  'TEAMMANAGERNEW_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_TEAMMANAGERNEW',
    'width' => '10%',
  ),
  'TEAM_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_TEAM',
    'width' => '10%',
  ),
  'INTERNALAFFILIATE_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_INTERNALAFFILIATE',
    'width' => '10%',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '5%',
    'label' => 'Responsible Agent',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'CONVERTED' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'LBL_CONVERTED',
    'width' => '10%',
  ),
  'CUSTOMERSUPPORT_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_CUSTOMERSUPPORT',
    'width' => '10%',
  ),
  'CLIENTSPERMONTH_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_CLIENTSPERMONTH',
    'width' => '10%',
  ),
  'VOLUMETRADEDPERMONTH_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_VOLUMETRADEDPERMONTH',
    'width' => '10%',
  ),
  'IB_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_IB',
    'width' => '10%',
  ),
  'MASTERIB_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_MASTERIB',
    'width' => '10%',
  ),
  'PROGRESSSTATUS_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_PROGRESSSTATUS',
    'width' => '10%',
  ),
  'VERIFIEDDATE_C' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'LBL_VERIFIEDDATE',
    'width' => '10%',
  ),
  'STATUS' => 
  array (
    'width' => '7%',
    'label' => 'LBL_LIST_STATUS',
    'default' => false,
  ),
  'TEAMAGENT_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_TEAMAGENT',
    'width' => '5%',
    'studio' => 'visible',
    'id' => 'TEAMAGENT_C',
  ),
  'TEAMLEADER_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_TEAMLEADER',
    'width' => '5%',
    'studio' => 'visible',
    'id' => 'TEAMLEADER_C',
  ),
);
;
?>
