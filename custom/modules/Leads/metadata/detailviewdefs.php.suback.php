<?php
$viewdefs ['Leads'] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          'SEND_CONFIRM_OPT_IN_EMAIL' => 
          array (
            'customCode' => '<input type="submit" class="button hidden" disabled="disabled" title="{$APP.LBL_SEND_CONFIRM_OPT_IN_EMAIL}" onclick="this.form.return_module.value=\'Leads\'; this.form.return_action.value=\'Leads\'; this.form.return_id.value=\'{$fields.id.value}\'; this.form.action.value=\'sendConfirmOptInEmail\'; this.form.module.value=\'Leads\'; this.form.module_tab.value=\'Leads\';" name="send_confirm_opt_in_email" value="{$APP.LBL_SEND_CONFIRM_OPT_IN_EMAIL}"/>',
            'sugar_html' => 
            array (
              'type' => 'submit',
              'value' => '{$APP.LBL_SEND_CONFIRM_OPT_IN_EMAIL}',
              'htmlOptions' => 
              array (
                'class' => 'button hidden',
                'id' => 'send_confirm_opt_in_email',
                'title' => '{$APP.LBL_SEND_CONFIRM_OPT_IN_EMAIL}',
                'onclick' => 'this.form.return_module.value=\'Leads\'; this.form.return_action.value=\'DetailView\'; this.form.return_id.value=\'{$fields.id.value}\'; this.form.action.value=\'sendConfirmOptInEmail\'; this.form.module.value=\'Leads\'; this.form.module_tab.value=\'Leads\';',
                'name' => 'send_confirm_opt_in_email',
                'disabled' => true,
              ),
            ),
          ),
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 
          array (
            'customCode' => '{if $bean->aclAccess("edit") && !$DISABLE_CONVERT_ACTION}<input title="{$MOD.LBL_CONVERTLEAD_TITLE}" accessKey="{$MOD.LBL_CONVERTLEAD_BUTTON_KEY}" type="button" class="button" onClick="document.location=\'index.php?module=Leads&action=ConvertLead&record={$fields.id.value}\'" name="convert" value="{$MOD.LBL_CONVERTLEAD}">{/if}',
            'sugar_html' => 
            array (
              'type' => 'button',
              'value' => '{$MOD.LBL_CONVERTLEAD}',
              'htmlOptions' => 
              array (
                'title' => '{$MOD.LBL_CONVERTLEAD_TITLE}',
                'accessKey' => '{$MOD.LBL_CONVERTLEAD_BUTTON_KEY}',
                'class' => 'button',
                'onClick' => 'document.location=\'index.php?module=Leads&action=ConvertLead&record={$fields.id.value}\'',
                'name' => 'convert',
                'id' => 'convert_lead_button',
              ),
              'template' => '{if $bean->aclAccess("edit") && !$DISABLE_CONVERT_ACTION}[CONTENT]{/if}',
            ),
          ),
          4 => 'FIND_DUPLICATES',
          5 => 
          array (
            'customCode' => '<input title="{$APP.LBL_MANAGE_SUBSCRIPTIONS}" class="button" onclick="this.form.return_module.value=\'Leads\'; this.form.return_action.value=\'DetailView\';this.form.return_id.value=\'{$fields.id.value}\'; this.form.action.value=\'Subscriptions\'; this.form.module.value=\'Campaigns\'; this.form.module_tab.value=\'Leads\';" type="submit" name="Manage Subscriptions" value="{$APP.LBL_MANAGE_SUBSCRIPTIONS}">',
            'sugar_html' => 
            array (
              'type' => 'submit',
              'value' => '{$APP.LBL_MANAGE_SUBSCRIPTIONS}',
              'htmlOptions' => 
              array (
                'title' => '{$APP.LBL_MANAGE_SUBSCRIPTIONS}',
                'class' => 'button',
                'id' => 'manage_subscriptions_button',
                'onclick' => 'this.form.return_module.value=\'Leads\'; this.form.return_action.value=\'DetailView\';this.form.return_id.value=\'{$fields.id.value}\'; this.form.action.value=\'Subscriptions\'; this.form.module.value=\'Campaigns\'; this.form.module_tab.value=\'Leads\';',
                'name' => '{$APP.LBL_MANAGE_SUBSCRIPTIONS}',
              ),
            ),
          ),
          'AOS_GENLET' => 
          array (
            'customCode' => '<input type="button" class="button" onClick="showPopup();" value="{$APP.LBL_PRINT_AS_PDF}">',
          ),
        ),
        'headerTpl' => 'modules/Leads/tpls/DetailViewHeader.tpl',
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'modules/Leads/Lead.js',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_CONTACT_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL6' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL5' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_ADVANCED' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL7' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_ASSIGNMENT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'LBL_CONTACT_INFORMATION' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'salutation',
            'comment' => 'Contact salutation (e.g., Mr, Ms)',
            'label' => 'LBL_SALUTATION',
          ),
          1 => 
          array (
            'name' => 'first_name',
            'comment' => 'First name of the contact',
            'label' => 'LBL_FIRST_NAME',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'last_name',
            'comment' => 'Last name of the contact',
            'label' => 'LBL_LAST_NAME',
          ),
          1 => 
          array (
            'name' => 'brand_c',
            'studio' => 'visible',
            'label' => 'LBL_BRAND',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'clienttype_c',
            'label' => 'LBL_CLIENTTYPE',
          ),
          1 => 
          array (
            'name' => 'account_name',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'securestatus_c',
            'studio' => 'visible',
            'label' => 'LBL_SECURESTATUS',
          ),
          1 => 
          array (
            'name' => 'verifieddate_c',
            'label' => 'LBL_VERIFIEDDATE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'funded_c',
            'label' => 'LBL_FUNDED',
          ),
          1 => 'phone_mobile',
        ),
        5 => 
        array (
          0 => 'email1',
          1 => 'phone_work',
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'verifiedphone_c',
            'label' => 'LBL_VERIFIEDPHONE',
          ),
          1 => 
          array (
            'name' => 'primary_address_street',
            'label' => 'LBL_PRIMARY_ADDRESS',
            'type' => 'address',
            'displayParams' => 
            array (
              'key' => 'primary',
            ),
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'alt_address_street',
            'label' => 'LBL_ALTERNATE_ADDRESS',
            'type' => 'address',
            'displayParams' => 
            array (
              'key' => 'alt',
            ),
          ),
          1 => 
          array (
            'name' => 'birthdate',
            'comment' => 'The birthdate of the contact',
            'label' => 'LBL_BIRTHDATE',
          ),
        ),
      ),
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'invamount_c',
            'studio' => 'visible',
            'label' => 'LBL_INVAMOUNT',
          ),
          1 => 
          array (
            'name' => 'fperyear_c',
            'studio' => 'visible',
            'label' => 'LBL_FPERYEAR',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'equities_c',
            'studio' => 'visible',
            'label' => 'LBL_EQUITIES',
          ),
          1 => 
          array (
            'name' => 'indicies_c',
            'studio' => 'visible',
            'label' => 'LBL_INDICIES',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'commodoties_c',
            'studio' => 'visible',
            'label' => 'LBL_COMMODOTIES',
          ),
          1 => 
          array (
            'name' => 'reason_c',
            'studio' => 'visible',
            'label' => 'LBL_REASON',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'oftentrade_c',
            'studio' => 'visible',
            'label' => 'LBL_OFTENTRADE',
          ),
          1 => 
          array (
            'name' => 'pexposed_c',
            'studio' => 'visible',
            'label' => 'LBL_PEXPOSED',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'riskoftranscactions_c',
            'studio' => 'visible',
            'label' => 'LBL_RISKOFTRANSCACTIONS',
          ),
          1 => 
          array (
            'name' => 'bankruptcy_c',
            'studio' => 'visible',
            'label' => 'LBL_BANKRUPTCY',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'age_c',
            'label' => 'LBL_AGE',
          ),
        ),
      ),
      'lbl_editview_panel6' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'changeleverage_c',
            'label' => 'LBL_CHANGELEVERAGE',
          ),
          1 => 
          array (
            'name' => 'accountrestrections_c',
            'label' => 'LBL_ACCOUNTRESTRECTIONS',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'localdepositprovider_c',
            'label' => 'LBL_LOCALDEPOSITPROVIDER',
          ),
          1 => 
          array (
            'name' => 'riskassessement_c',
            'label' => 'LBL_RISKASSESSEMENT',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'strategyprovider_c',
            'label' => 'LBL_STRATEGYPROVIDER',
          ),
          1 => 
          array (
            'name' => 'fxstat_c',
            'label' => 'LBL_FXSTAT',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'zulutrade_c',
            'label' => 'LBL_ZULUTRADE',
          ),
        ),
      ),
      'lbl_editview_panel5' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'poi_c',
            'studio' => 'visible',
            'label' => 'LBL_POI',
          ),
          1 => 
          array (
            'name' => 'poiissueddate_c',
            'label' => 'LBL_POIISSUEDDATE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'poiexpiration_c',
            'label' => 'LBL_POIEXPIRATION',
          ),
          1 => 
          array (
            'name' => 'idcomment_c',
            'label' => 'LBL_IDCOMMENT',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'por_c',
            'studio' => 'visible',
            'label' => 'LBL_POR',
          ),
          1 => 
          array (
            'name' => 'porissued_c',
            'label' => 'LBL_PORISSUED',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'porcomment_c',
            'label' => 'LBL_PORCOMMENT',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'creditcard_c',
            'studio' => 'visible',
            'label' => 'LBL_CREDITCARD',
          ),
          1 => 
          array (
            'name' => 'creditdebitexpiration_c',
            'label' => 'LBL_CREDITDEBITEXPIRATION',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'creditdebitlast4digits_c',
            'label' => 'LBL_CREDITDEBITLAST4DIGITS',
          ),
          1 => 
          array (
            'name' => 'cardcomment_c',
            'label' => 'CARD COMMENT',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'lastdepositdate_c',
            'label' => 'LBL_LASTDEPOSITDATE',
          ),
        ),
      ),
      'LBL_PANEL_ADVANCED' => 
      array (
        0 => 
        array (
          0 => 'refered_by',
          1 => 'lead_source',
        ),
        1 => 
        array (
          0 => 'status_description',
          1 => 'lead_source_description',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'http_referer_c',
            'label' => 'LBL_HTTP_REFERER',
          ),
          1 => 
          array (
            'name' => 'how_did_you_hear_about_us_c',
            'label' => 'LBL_HOW_DID_YOU_HEAR_ABOUT_US',
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'typeofpartner_c',
            'studio' => 'visible',
            'label' => 'LBL_TYPEOFPARTNER',
          ),
          1 => 
          array (
            'name' => 'clientspermonth_c',
            'studio' => 'visible',
            'label' => 'LBL_CLIENTSPERMONTH',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'clientdepositspermonth_c',
            'studio' => 'visible',
            'label' => 'LBL_CLIENTDEPOSITSPERMONTH',
          ),
          1 => 
          array (
            'name' => 'volumetradedpermonth_c',
            'studio' => 'visible',
            'label' => 'LBL_VOLUMETRADEDPERMONTH',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'masterib_c',
            'label' => 'LBL_MASTERIB',
          ),
          1 => 
          array (
            'name' => 'ib_c',
            'label' => 'LBL_IB',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'subibs_c',
            'studio' => 'visible',
            'label' => 'LBL_SUBIBS',
          ),
        ),
      ),
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'partnerwebsite1_c',
            'label' => 'LBL_PARTNERWEBSITE1',
          ),
          1 => 
          array (
            'name' => 'partnerwebsite2_c',
            'label' => 'LBL_PARTNERWEBSITE2',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'partnerwebsite3_c',
            'label' => 'LBL_PARTNERWEBSITE3',
          ),
        ),
      ),
      'lbl_editview_panel7' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'newsletters_c',
            'label' => 'LBL_NEWSLETTERS',
          ),
          1 => 
          array (
            'name' => 'promotionalemails_c',
            'label' => 'LBL_PROMOTIONALEMAILS',
          ),
        ),
      ),
      'LBL_PANEL_ASSIGNMENT' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'contactlanguage_c',
            'studio' => 'visible',
            'label' => 'LBL_CONTACTLANGUAGE',
          ),
          1 => 
          array (
            'name' => 'customersupport_c',
            'studio' => 'visible',
            'label' => 'LBL_CUSTOMERSUPPORT',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'progressstatus_c',
            'studio' => 'visible',
            'label' => 'LBL_PROGRESSSTATUS',
          ),
          1 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO',
          ),
        ),
        2 => 
        array (
          0 => 'status',
        ),
      ),
    ),
  ),
);
;
?>
