<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2018-04-13 07:01:52
$dictionary['Lead']['fields']['accountrestrections_c']['inline_edit']='1';
$dictionary['Lead']['fields']['accountrestrections_c']['labelValue']='Account Restrictions';

 

 // created: 2019-09-02 09:00:34
$dictionary['Lead']['fields']['teammanagernew_c']['inline_edit']='1';
$dictionary['Lead']['fields']['teammanagernew_c']['labelValue']='Team Manager';

 

 // created: 2018-04-24 08:02:14
$dictionary['Lead']['fields']['poiissueddate_c']['inline_edit']='1';
$dictionary['Lead']['fields']['poiissueddate_c']['labelValue']='ID Issued';

 

 // created: 2018-07-26 12:37:27
$dictionary['Lead']['fields']['riskassessement_c']['inline_edit']='1';
$dictionary['Lead']['fields']['riskassessement_c']['labelValue']='Risk Assessment';

 

 // created: 2018-04-13 06:41:30
$dictionary['Lead']['fields']['subibs_c']['inline_edit']='1';
$dictionary['Lead']['fields']['subibs_c']['labelValue']='Sub-IBs ';

 

 // created: 2018-07-09 14:57:48
$dictionary['Lead']['fields']['date_entered']['inline_edit']=true;
$dictionary['Lead']['fields']['date_entered']['comments']='Date record created';
$dictionary['Lead']['fields']['date_entered']['merge_filter']='disabled';

 







$dictionary['Lead']['fields']['sugarchimpactivity'] = array(
    'name' => 'sugarchimpactivity',
    'type' => 'link',
    'relationship' => 'sugarchimpactivity_leads',
    'module' => 'SugarChimpActivity',
    'bean_name' => 'SugarChimpActivity',
    'source' => 'non-db',
    'vname' => 'LBL_SUGARCHIMPACTIVITY',
);

$dictionary['Lead']['fields']['mailchimp_rating_c'] = array(
	'name' => 'mailchimp_rating_c',
	'vname' => 'LBL_MAILCHIMP_RATING',
	'type' => 'mailchimprating',
	'dbType' => 'int',
	'len' => '3',
	'comment' => 'MailChimp Rating',
	'required' => false,
	'studio' => true,
	'reportable' => true,
	'default' => 2,
	'readonly' => true,
);

 // created: 2019-01-14 10:30:17
$dictionary['Lead']['fields']['completereg4_c']['inline_edit']='1';
$dictionary['Lead']['fields']['completereg4_c']['labelValue']='Workflow 1.4';

 

 // created: 2018-04-13 07:00:17
$dictionary['Lead']['fields']['zulutrade_c']['inline_edit']='1';
$dictionary['Lead']['fields']['zulutrade_c']['labelValue']='Zulu Trade';

 

 // created: 2019-01-14 10:29:30
$dictionary['Lead']['fields']['completereg6_c']['inline_edit']='1';
$dictionary['Lead']['fields']['completereg6_c']['labelValue']='Workflow 1.6';

 

 // created: 2018-07-30 14:36:37
$dictionary['Lead']['fields']['clientdepositspermonth_c']['inline_edit']='1';
$dictionary['Lead']['fields']['clientdepositspermonth_c']['labelValue']='Clients Deposits Per Month';

 

 // created: 2018-12-21 15:02:56
$dictionary['Lead']['fields']['unsubscribe_c']['inline_edit']='1';
$dictionary['Lead']['fields']['unsubscribe_c']['labelValue']='Unsubscribe';

 

 // created: 2018-07-30 12:55:42
$dictionary['Lead']['fields']['equities_c']['inline_edit']='1';
$dictionary['Lead']['fields']['equities_c']['labelValue']='Equities per Year';

 

 // created: 2019-07-09 09:21:16
$dictionary['Lead']['fields']['teammanager_c']['inline_edit']='1';
$dictionary['Lead']['fields']['teammanager_c']['labelValue']='Team Manager';

 

 // created: 2019-09-26 09:11:49
$dictionary['Lead']['fields']['utm_source_c']['inline_edit']='1';
$dictionary['Lead']['fields']['utm_source_c']['labelValue']='Utm source';

 

 // created: 2019-07-17 09:08:50
$dictionary['Lead']['fields']['brand_c']['inline_edit']='1';
$dictionary['Lead']['fields']['brand_c']['labelValue']='Brand';

 

 // created: 2018-12-19 15:32:44
$dictionary['Lead']['fields']['campaignname1_c']['inline_edit']='1';
$dictionary['Lead']['fields']['campaignname1_c']['labelValue']='Campaign Name(1)';

 

 // created: 2019-07-03 11:14:01
$dictionary['Lead']['fields']['internalaffiliate_c']['inline_edit']='1';
$dictionary['Lead']['fields']['internalaffiliate_c']['labelValue']='Internal Affiliate';

 

 // created: 2019-05-24 06:45:08
$dictionary['Lead']['fields']['businesscountry_c']['inline_edit']='1';
$dictionary['Lead']['fields']['businesscountry_c']['labelValue']='Bussiness Country';

 

 // created: 2018-11-26 15:07:13
$dictionary['Lead']['fields']['dateworfklowreminder7_c']['inline_edit']='1';
$dictionary['Lead']['fields']['dateworfklowreminder7_c']['labelValue']='Date(7)';

 

 // created: 2019-10-23 08:50:40
$dictionary['Lead']['fields']['verifiedlevel_c']['inline_edit']='1';
$dictionary['Lead']['fields']['verifiedlevel_c']['labelValue']='Verified Level';

 

 // created: 2018-04-13 08:28:02
$dictionary['Lead']['fields']['idcomment_c']['inline_edit']='1';
$dictionary['Lead']['fields']['idcomment_c']['labelValue']='ID Comment';

 

 // created: 2018-04-12 07:03:40
$dictionary['Lead']['fields']['masterib_c']['inline_edit']='1';
$dictionary['Lead']['fields']['masterib_c']['labelValue']='Master ';

 

 // created: 2019-01-14 14:04:03
$dictionary['Lead']['fields']['completereg7_c']['inline_edit']='1';
$dictionary['Lead']['fields']['completereg7_c']['labelValue']='Incomplete Registration Reminder 7';

 

 // created: 2018-04-12 06:58:01
$dictionary['Lead']['fields']['partnerwebsite1_c']['inline_edit']='1';
$dictionary['Lead']['fields']['partnerwebsite1_c']['labelValue']='Site';

 

 // created: 2018-07-31 07:17:17
$dictionary['Lead']['fields']['securestatus_c']['inline_edit']='1';
$dictionary['Lead']['fields']['securestatus_c']['labelValue']='Secure Status';

 

 // created: 2018-04-24 09:38:25
$dictionary['Lead']['fields']['primary_address_country']['inline_edit']=true;
$dictionary['Lead']['fields']['primary_address_country']['comments']='Country for primary address';
$dictionary['Lead']['fields']['primary_address_country']['merge_filter']='disabled';

 

 // created: 2018-04-13 07:08:21
$dictionary['Lead']['fields']['localdepositprovider_c']['inline_edit']='1';
$dictionary['Lead']['fields']['localdepositprovider_c']['labelValue']='Local Deposit Provider';

 

 // created: 2018-04-13 07:16:26
$dictionary['Lead']['fields']['newsletters_c']['inline_edit']='1';
$dictionary['Lead']['fields']['newsletters_c']['labelValue']='Receive Newsletters';

 

 // created: 2019-02-28 14:19:11
$dictionary['Lead']['fields']['porstatusreason_c']['inline_edit']='1';
$dictionary['Lead']['fields']['porstatusreason_c']['labelValue']='POR Status Reason';

 

 // created: 2019-02-28 14:20:22
$dictionary['Lead']['fields']['creditcardstatusreason_c']['inline_edit']='1';
$dictionary['Lead']['fields']['creditcardstatusreason_c']['labelValue']='Credit Card Status Reason';

 

 // created: 2018-07-30 12:51:02
$dictionary['Lead']['fields']['customersupport_c']['inline_edit']='1';
$dictionary['Lead']['fields']['customersupport_c']['labelValue']='Customer Support';

 

 // created: 2018-07-26 13:13:20
$dictionary['Lead']['fields']['http_referer_c']['inline_edit']='1';
$dictionary['Lead']['fields']['http_referer_c']['labelValue']='Http referer:';

 

 // created: 2019-02-04 09:32:26
$dictionary['Lead']['fields']['commodoties_c']['inline_edit']='1';
$dictionary['Lead']['fields']['commodoties_c']['labelValue']='Commodities Per Year';

 

 // created: 2018-07-30 12:54:11
$dictionary['Lead']['fields']['poi_c']['inline_edit']='1';
$dictionary['Lead']['fields']['poi_c']['labelValue']='ID';

 

 // created: 2018-04-13 07:01:13
$dictionary['Lead']['fields']['changeleverage_c']['inline_edit']='1';
$dictionary['Lead']['fields']['changeleverage_c']['labelValue']='Change Leverage';

 

 // created: 2018-11-26 15:06:07
$dictionary['Lead']['fields']['dateworfklowreminder4_c']['inline_edit']='1';
$dictionary['Lead']['fields']['dateworfklowreminder4_c']['labelValue']='Date(4)';

 

 // created: 2018-04-12 06:59:23
$dictionary['Lead']['fields']['partnerwebsite3_c']['inline_edit']='1';
$dictionary['Lead']['fields']['partnerwebsite3_c']['labelValue']='SIte-3';

 

 // created: 2018-07-27 14:24:12
$dictionary['Lead']['fields']['refered_by']['audited']=true;
$dictionary['Lead']['fields']['refered_by']['inline_edit']=true;
$dictionary['Lead']['fields']['refered_by']['comments']='Identifies who refered the lead';
$dictionary['Lead']['fields']['refered_by']['merge_filter']='disabled';

 

 // created: 2018-07-30 12:56:29
$dictionary['Lead']['fields']['riskoftranscactions_c']['inline_edit']='1';
$dictionary['Lead']['fields']['riskoftranscactions_c']['labelValue']='Do you understand the nature and risks of margin transactions';

 

 // created: 2019-02-04 09:32:57
$dictionary['Lead']['fields']['oftentrade_c']['inline_edit']='1';
$dictionary['Lead']['fields']['oftentrade_c']['labelValue']='How often do you trade';

 

 // created: 2018-04-13 07:14:33
$dictionary['Lead']['fields']['how_did_you_hear_about_us_c']['inline_edit']='1';
$dictionary['Lead']['fields']['how_did_you_hear_about_us_c']['labelValue']='How did you hear about us';

 

 // created: 2019-01-14 10:31:03
$dictionary['Lead']['fields']['completereg2_c']['inline_edit']='1';
$dictionary['Lead']['fields']['completereg2_c']['labelValue']='Workflow 1.2';

 

 // created: 2018-05-31 09:30:32
$dictionary['Lead']['fields']['accounttype_c']['inline_edit']='1';
$dictionary['Lead']['fields']['accounttype_c']['labelValue']='Account Type';

 

 // created: 2018-04-13 07:11:39
$dictionary['Lead']['fields']['fxstat_c']['inline_edit']='1';
$dictionary['Lead']['fields']['fxstat_c']['labelValue']='Allow FX Stat';

 

 // created: 2018-04-13 06:49:48
$dictionary['Lead']['fields']['porcomment_c']['inline_edit']='1';
$dictionary['Lead']['fields']['porcomment_c']['labelValue']='POR Comment';

 

 // created: 2019-07-09 09:21:54
$dictionary['Lead']['fields']['teamagent_c']['inline_edit']='1';
$dictionary['Lead']['fields']['teamagent_c']['labelValue']='Team Agent';

 

 // created: 2018-11-26 15:04:41
$dictionary['Lead']['fields']['dateworfklowreminder2_c']['inline_edit']='1';
$dictionary['Lead']['fields']['dateworfklowreminder2_c']['labelValue']='Date(2)';

 

 // created: 2018-04-12 07:00:47
$dictionary['Lead']['fields']['partnerwebsite2_c']['inline_edit']='1';
$dictionary['Lead']['fields']['partnerwebsite2_c']['labelValue']='Site-2';

 

 // created: 2019-02-28 14:44:21
$dictionary['Lead']['fields']['cardcomment_c']['inline_edit']='1';
$dictionary['Lead']['fields']['cardcomment_c']['labelValue']='CC Comment ';

 

 // created: 2018-07-27 14:09:07
$dictionary['Lead']['fields']['status']['inline_edit']=true;
$dictionary['Lead']['fields']['status']['comments']='Status of the lead';
$dictionary['Lead']['fields']['status']['merge_filter']='disabled';

 

 // created: 2019-01-14 10:30:31
$dictionary['Lead']['fields']['completereg3_c']['inline_edit']='1';
$dictionary['Lead']['fields']['completereg3_c']['labelValue']='Workflow 1.3';

 

 // created: 2019-01-14 10:30:03
$dictionary['Lead']['fields']['completereg5_c']['inline_edit']='1';
$dictionary['Lead']['fields']['completereg5_c']['labelValue']='Workflow 1.5';

 

 // created: 2019-02-04 09:31:36
$dictionary['Lead']['fields']['indicies_c']['inline_edit']='1';
$dictionary['Lead']['fields']['indicies_c']['labelValue']='Indicies Per Year';

 

 // created: 2018-04-12 07:46:38
$dictionary['Lead']['fields']['clienttype_c']['inline_edit']='1';
$dictionary['Lead']['fields']['clienttype_c']['labelValue']='Client Type';

 

 // created: 2018-06-26 06:41:27
$dictionary['Lead']['fields']['fundedfirstamount_c']['inline_edit']='1';
$dictionary['Lead']['fields']['fundedfirstamount_c']['labelValue']='First Amount Funded';

 

 // created: 2018-11-26 15:05:29
$dictionary['Lead']['fields']['dateworfklowreminder3_c']['inline_edit']='1';
$dictionary['Lead']['fields']['dateworfklowreminder3_c']['labelValue']='Date(3)';

 

 // created: 2018-04-24 07:08:21
$dictionary['Lead']['fields']['creditdebitlast4digits_c']['inline_edit']='1';
$dictionary['Lead']['fields']['creditdebitlast4digits_c']['labelValue']='Last 4 Digits';

 

 // created: 2018-12-19 15:43:55
$dictionary['Lead']['fields']['campaign_c']['inline_edit']='1';
$dictionary['Lead']['fields']['campaign_c']['labelValue']='Campaign';

 

 // created: 2018-04-23 15:07:56
$dictionary['Lead']['fields']['creditdebitexpiration_c']['inline_edit']='1';
$dictionary['Lead']['fields']['creditdebitexpiration_c']['labelValue']='Credit/Debit Card Expiration';

 

 // created: 2018-07-30 13:04:20
$dictionary['Lead']['fields']['contactlanguage_c']['inline_edit']='1';
$dictionary['Lead']['fields']['contactlanguage_c']['labelValue']='Language/Desk';

 

 // created: 2018-04-13 09:13:19
$dictionary['Lead']['fields']['promotionalemails_c']['inline_edit']='1';
$dictionary['Lead']['fields']['promotionalemails_c']['labelValue']='Promotional Emails';

 

 // created: 2018-07-30 13:08:14
$dictionary['Lead']['fields']['volumetradedpermonth_c']['inline_edit']='1';
$dictionary['Lead']['fields']['volumetradedpermonth_c']['labelValue']='Volume Per Month';

 

 // created: 2018-04-24 07:50:24
$dictionary['Lead']['fields']['poiexpiration_c']['inline_edit']='1';
$dictionary['Lead']['fields']['poiexpiration_c']['labelValue']='ID Expiration';

 

 // created: 2019-02-28 14:18:15
$dictionary['Lead']['fields']['poistatusreason_c']['inline_edit']='1';
$dictionary['Lead']['fields']['poistatusreason_c']['labelValue']='POI Status Reason';

 

 // created: 2018-07-30 13:09:11
$dictionary['Lead']['fields']['bankruptcy_c']['inline_edit']='1';
$dictionary['Lead']['fields']['bankruptcy_c']['labelValue']='Have you declared bankruptcy';

 

 // created: 2018-03-30 09:33:36
$dictionary['Lead']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2018-03-30 09:33:36
$dictionary['Lead']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2018-11-26 15:06:28
$dictionary['Lead']['fields']['dateworfklowreminder5_c']['inline_edit']='1';
$dictionary['Lead']['fields']['dateworfklowreminder5_c']['labelValue']='Date(5)';

 

 // created: 2018-11-26 15:06:44
$dictionary['Lead']['fields']['dateworfklowreminder6_c']['inline_edit']='1';
$dictionary['Lead']['fields']['dateworfklowreminder6_c']['labelValue']='Date(6)';

 

 // created: 2018-07-12 06:22:30
$dictionary['Lead']['fields']['datecreatedcustom_c']['inline_edit']='1';
$dictionary['Lead']['fields']['datecreatedcustom_c']['labelValue']='Date Created Custom';

 

 // created: 2018-04-24 07:57:06
$dictionary['Lead']['fields']['porissued_c']['inline_edit']='1';
$dictionary['Lead']['fields']['porissued_c']['labelValue']='POR Issued';

 

 // created: 2018-07-30 13:10:02
$dictionary['Lead']['fields']['pexposed_c']['inline_edit']='1';
$dictionary['Lead']['fields']['pexposed_c']['labelValue']='Are you politically exposed person';

 

 // created: 2018-07-30 12:44:53
$dictionary['Lead']['fields']['typeofpartner_c']['inline_edit']='1';
$dictionary['Lead']['fields']['typeofpartner_c']['labelValue']='Type of Partner';

 

 // created: 2018-03-30 09:33:36
$dictionary['Lead']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2018-07-30 13:11:27
$dictionary['Lead']['fields']['reason_c']['inline_edit']='1';
$dictionary['Lead']['fields']['reason_c']['labelValue']='Reason to open an account';

 

 // created: 2019-03-06 07:24:36
$dictionary['Lead']['fields']['promotion_code_c']['inline_edit']='1';
$dictionary['Lead']['fields']['promotion_code_c']['labelValue']='Promotion  Code';

 

 // created: 2018-12-03 15:51:34
$dictionary['Lead']['fields']['progressstatus_c']['inline_edit']='1';
$dictionary['Lead']['fields']['progressstatus_c']['labelValue']='Progress';

 

 // created: 2019-03-15 13:51:56
$dictionary['Lead']['fields']['disableprofile_c']['inline_edit']='1';
$dictionary['Lead']['fields']['disableprofile_c']['labelValue']='Disable Profile';

 

 // created: 2018-04-17 12:58:10
$dictionary['Lead']['fields']['verifieddate_c']['inline_edit']='1';
$dictionary['Lead']['fields']['verifieddate_c']['labelValue']='Verified Date';

 

 // created: 2018-07-26 11:10:31
$dictionary['Lead']['fields']['lastdepositdate_c']['inline_edit']='1';
$dictionary['Lead']['fields']['lastdepositdate_c']['labelValue']='Last Deposit Date:';

 

 // created: 2018-07-30 13:12:23
$dictionary['Lead']['fields']['clientspermonth_c']['inline_edit']='1';
$dictionary['Lead']['fields']['clientspermonth_c']['labelValue']='Clients Per Month';

 

// created: 2018-04-13 10:44:42
$dictionary["Lead"]["fields"]["leads_fin12_financial_information_1"] = array (
  'name' => 'leads_fin12_financial_information_1',
  'type' => 'link',
  'relationship' => 'leads_fin12_financial_information_1',
  'source' => 'non-db',
  'module' => 'fin12_financial_information',
  'bean_name' => 'fin12_financial_information',
  'side' => 'right',
  'vname' => 'LBL_LEADS_FIN12_FINANCIAL_INFORMATION_1_FROM_FIN12_FINANCIAL_INFORMATION_TITLE',
);


 // created: 2019-08-27 08:42:01
$dictionary['Lead']['fields']['utm_campaign_c']['inline_edit']='1';
$dictionary['Lead']['fields']['utm_campaign_c']['labelValue']='Utm campaign';

 

 // created: 2018-11-26 14:42:14
$dictionary['Lead']['fields']['dateworkflowreminder1_c']['inline_edit']='1';
$dictionary['Lead']['fields']['dateworkflowreminder1_c']['labelValue']='Date -1';

 

 // created: 2018-03-30 09:33:36
$dictionary['Lead']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2019-02-04 09:31:10
$dictionary['Lead']['fields']['fperyear_c']['inline_edit']='1';
$dictionary['Lead']['fields']['fperyear_c']['labelValue']='Forex Per Year ';

 

 // created: 2018-04-13 06:53:45
$dictionary['Lead']['fields']['verifiedphone_c']['inline_edit']='1';
$dictionary['Lead']['fields']['verifiedphone_c']['labelValue']='Phone is Verified ';

 

 // created: 2018-04-17 07:34:23
$dictionary['Lead']['fields']['age_c']['inline_edit']='1';
$dictionary['Lead']['fields']['age_c']['labelValue']='I confirm that I am over 18 years of age';

 

 // created: 2018-07-30 13:13:39
$dictionary['Lead']['fields']['por_c']['inline_edit']='1';
$dictionary['Lead']['fields']['por_c']['labelValue']='POR';

 

 // created: 2018-05-03 07:31:05
$dictionary['Lead']['fields']['funded_c']['inline_edit']='1';
$dictionary['Lead']['fields']['funded_c']['labelValue']='Funded';

 

 // created: 2018-04-24 10:22:25
$dictionary['Lead']['fields']['account_name']['inline_edit']=true;
$dictionary['Lead']['fields']['account_name']['comments']='Account name for lead';
$dictionary['Lead']['fields']['account_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['account_name']['full_text_search']=NULL;
$dictionary['Lead']['fields']['account_name']['audited']=true;

 

 // created: 2019-05-29 13:04:38
$dictionary['Lead']['fields']['team_c']['inline_edit']='1';
$dictionary['Lead']['fields']['team_c']['labelValue']='Team';

 

 // created: 2018-07-30 13:16:44
$dictionary['Lead']['fields']['invamount_c']['inline_edit']='1';
$dictionary['Lead']['fields']['invamount_c']['labelValue']='Inv. Ammount';

 

 // created: 2018-04-12 07:44:43
$dictionary['Lead']['fields']['ib_c']['inline_edit']='1';
$dictionary['Lead']['fields']['ib_c']['labelValue']='IB';

 

 // created: 2019-07-09 09:22:29
$dictionary['Lead']['fields']['teamleader_c']['inline_edit']='1';
$dictionary['Lead']['fields']['teamleader_c']['labelValue']='Team Leader';

 

 // created: 2018-05-03 06:52:44
$dictionary['Lead']['fields']['creditcard_c']['inline_edit']='1';
$dictionary['Lead']['fields']['creditcard_c']['labelValue']='Credit/Debit Card Status';

 

 // created: 2019-08-27 08:41:44
$dictionary['Lead']['fields']['utm_medium_c']['inline_edit']='1';
$dictionary['Lead']['fields']['utm_medium_c']['labelValue']='Utm medium';

 

// created: 2018-04-13 10:48:55
$dictionary["Lead"]["fields"]["leads_abc_mt4id_1"] = array (
  'name' => 'leads_abc_mt4id_1',
  'type' => 'link',
  'relationship' => 'leads_abc_mt4id_1',
  'source' => 'non-db',
  'module' => 'abc_MT4ID',
  'bean_name' => 'abc_MT4ID',
  'side' => 'right',
  'vname' => 'LBL_LEADS_ABC_MT4ID_1_FROM_ABC_MT4ID_TITLE',
);


 // created: 2018-05-22 14:31:54
$dictionary['Lead']['fields']['strategyprovider_c']['inline_edit']='1';
$dictionary['Lead']['fields']['strategyprovider_c']['labelValue']='Strategy Provider';

 

 // created: 2019-10-23 08:55:01
$dictionary['Lead']['fields']['clicktocall_c']['inline_edit']='1';
$dictionary['Lead']['fields']['clicktocall_c']['labelValue']='ClickToCall';

 

 // created: 2019-01-14 10:31:12
$dictionary['Lead']['fields']['completereg1_c']['inline_edit']='1';
$dictionary['Lead']['fields']['completereg1_c']['labelValue']='Workflow 1.1';

 
?>